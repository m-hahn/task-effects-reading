\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Related Work}{4}
\contentsline {section}{\numberline {3}The Tradeoff Hypothesis}{5}
\contentsline {section}{\numberline {4}The Neural Attention Tradeoff Model}{6}
\contentsline {subsection}{\numberline {4.1}Architecture}{6}
\contentsline {subsection}{\numberline {4.2}Objective Function}{8}
\contentsline {subsection}{\numberline {4.3}Parameter Estimation}{8}
\contentsline {section}{\numberline {5}Modeling Study~1}{9}
\contentsline {subsection}{\numberline {5.1}Methods}{9}
\contentsline {subsubsection}{\numberline {5.1.1}Model Implementation}{9}
\contentsline {subsubsection}{\numberline {5.1.2}Dataset}{9}
\contentsline {subsection}{\numberline {5.2}Results and Discussion}{9}
\contentsline {subsubsection}{\numberline {5.2.1}Fixation Sequences}{10}
\contentsline {subsubsection}{\numberline {5.2.2}Reading Times}{12}
\contentsline {subsubsection}{\numberline {5.2.3}Qualitative Properties}{12}
\contentsline {section}{\numberline {6}Experiment~1}{14}
\contentsline {subsection}{\numberline {6.1}Methods}{15}
\contentsline {subsubsection}{\numberline {6.1.1}Participants}{15}
\contentsline {subsubsection}{\numberline {6.1.2}Materials}{15}
\contentsline {subsubsection}{\numberline {6.1.3}Procedure}{15}
\contentsline {subsubsection}{\numberline {6.1.4}Data Analysis}{16}
\contentsline {subsection}{\numberline {6.2}Results}{17}
\contentsline {subsubsection}{\numberline {6.2.1}Tradeoff between Accuracy and Economy}{17}
\contentsline {subsubsection}{\numberline {6.2.2}Mixed Effects Analyses}{18}
\contentsline {subsubsection}{\numberline {6.2.3}Task-independent Effects}{19}
\contentsline {subsubsection}{\numberline {6.2.4}Task-dependent Effects}{20}
\contentsline {subsection}{\numberline {6.3}Discussion}{23}
\contentsline {section}{\numberline {7}Modeling Task Effects in NEAT}{25}
\contentsline {subsection}{\numberline {7.1}Architecture}{25}
\contentsline {subsubsection}{\numberline {7.1.1}Attention Module}{26}
\contentsline {subsubsection}{\numberline {7.1.2}Task Module}{27}
\contentsline {subsection}{\numberline {7.2}Objective Function}{28}
\contentsline {subsection}{\numberline {7.3}Parameter Estimation}{28}
\contentsline {section}{\numberline {8}Modeling Study~2}{29}
\contentsline {subsection}{\numberline {8.1}Methods}{29}
\contentsline {subsubsection}{\numberline {8.1.1}Model Implementation}{29}
\contentsline {subsubsection}{\numberline {8.1.2}Dataset}{30}
\contentsline {subsection}{\numberline {8.2}Results}{30}
\contentsline {subsubsection}{\numberline {8.2.1}Tradeoff between Accuracy and Economy}{30}
\contentsline {subsubsection}{\numberline {8.2.2}Mixed Effects Analyses}{31}
\contentsline {subsubsection}{\numberline {8.2.3}Task-independent Effects}{32}
\contentsline {subsubsection}{\numberline {8.2.4}Task-dependent Effects}{34}
\contentsline {subsubsection}{\numberline {8.2.5}Reading Times}{34}
\contentsline {subsection}{\numberline {8.3}Discussion}{37}
\contentsline {section}{\numberline {9}General Discussion}{37}
