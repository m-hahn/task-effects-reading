\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Ba%
, Salakhutdinov%
, Grosse%
\BCBL {}\ \BBA {} Frey%
}{%
Ba%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2015}}%
}]{%
ba_learning_2015}
\APACinsertmetastar {%
ba_learning_2015}%
\begin{APACrefauthors}%
Ba, J.%
, Salakhutdinov, R\BPBI R.%
, Grosse, R\BPBI B.%
\BCBL {}\ \BBA {} Frey, B\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Learning wake-sleep recurrent attention models}
  {Learning wake-sleep recurrent attention models}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Advances in Neural Information Processing Systems}
  {Advances in neural information processing systems}\ (\BPGS\ 2575--2583).
\newblock
\begin{APACrefURL}
  [{2016-06-02}]\url{http://papers.nips.cc/paper/5861-learning-wake-sleep-recurrent-attention-models}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bahdanau%
, Cho%
\BCBL {}\ \BBA {} Bengio%
}{%
Bahdanau%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2015}}%
}]{%
bahdanau_neural_2015}
\APACinsertmetastar {%
bahdanau_neural_2015}%
\begin{APACrefauthors}%
Bahdanau, D.%
, Cho, K.%
\BCBL {}\ \BBA {} Bengio, Y.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Neural machine translation by jointly learning to align
  and translate} {Neural machine translation by jointly learning to align and
  translate}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the International Conference on Learning
  Representations.} {Proceedings of the international conference on learning
  representations.}
\newblock
\begin{APACrefURL} \url{http://arxiv.org/abs/1409.0473} \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Barrett%
, Agi{\' c}%
\BCBL {}\ \BBA {} S{\o}gaard%
}{%
Barrett%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2015}}%
}]{%
barrett_dundee_2015}
\APACinsertmetastar {%
barrett_dundee_2015}%
\begin{APACrefauthors}%
Barrett, M.%
, Agi{\' c}, {\v Z}.%
\BCBL {}\ \BBA {} S{\o}gaard, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The {Dundee} Treebank} {The {Dundee} treebank}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the 14th International Workshop on
  Treebanks and Linguistic Theories} {Proceedings of the 14th international
  workshop on treebanks and linguistic theories}\ (\BPGS\ 242--248).
\newblock
\begin{APACrefURL}
  [{2016-06-02}]\url{http://bib.irb.hr/datoteka/789398.barrett2015-dundee.pdf}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bates%
, M{\"a}chler%
, Bolker%
\BCBL {}\ \BBA {} Walker%
}{%
Bates%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2015}}%
}]{%
bates-fitting-2015-1}
\APACinsertmetastar {%
bates-fitting-2015-1}%
\begin{APACrefauthors}%
Bates, D.%
, M{\"a}chler, M.%
, Bolker, B.%
\BCBL {}\ \BBA {} Walker, S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Fitting {Linear} {Mixed}-{Effects} {Models} {Using}
  \textbf{lme4}} {Fitting {Linear} {Mixed}-{Effects} {Models} {Using}
  \textbf{lme4}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Statistical Software}{67}{1}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bicknell%
\ \BBA {} Levy%
}{%
Bicknell%
\ \BBA {} Levy%
}{%
{\protect \APACyear {2010}}%
}]{%
bicknell_rational_2010-1}
\APACinsertmetastar {%
bicknell_rational_2010-1}%
\begin{APACrefauthors}%
Bicknell, K.%
\BCBT {}\ \BBA {} Levy, R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A rational model of eye movement control in reading} {A
  rational model of eye movement control in reading}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the 48th Annual Meeting of the
  Association for Computational Linguistics} {Proceedings of the 48th annual
  meeting of the association for computational linguistics}\ (\BPGS\
  1168--1178).
\newblock
\begin{APACrefURL} \url{http://dl.acm.org/citation.cfm?id=1858800}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
B.~Carpenter%
\ \protect \BOthers {.}}{%
B.~Carpenter%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2017}}%
}]{%
carpenter-stan:-2017}
\APACinsertmetastar {%
carpenter-stan:-2017}%
\begin{APACrefauthors}%
Carpenter, B.%
, Gelman, A.%
, Hoffman, M\BPBI D.%
, Lee, D.%
, Goodrich, B.%
, Betancourt, M.%
\BDBL {}Riddell, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Stan: {A} {Probabilistic} {Programming} {Language}}
  {Stan: {A} {Probabilistic} {Programming} {Language}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Statistical Software}{76}{1}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
P\BPBI A.~Carpenter%
\ \BBA {} Just%
}{%
P\BPBI A.~Carpenter%
\ \BBA {} Just%
}{%
{\protect \APACyear {1983}}%
}]{%
carpenter_what_1983}
\APACinsertmetastar {%
carpenter_what_1983}%
\begin{APACrefauthors}%
Carpenter, P\BPBI A.%
\BCBT {}\ \BBA {} Just, M\BPBI A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1983}{}{}.
\newblock
{\BBOQ}\APACrefatitle {What your eyes do while your mind is reading} {What your
  eyes do while your mind is reading}.{\BBCQ}
\newblock
\BIn{} K.~Rayner\ (\BED), \APACrefbtitle {Eye Movements in Reading} {Eye
  movements in reading}\ (\BPGS\ 275--307).
\newblock
\APACaddressPublisher{New York}{Academic Press}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Chen%
, Bolton%
\BCBL {}\ \BBA {} Manning%
}{%
Chen%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2016}}%
}]{%
chen_thorough_2016}
\APACinsertmetastar {%
chen_thorough_2016}%
\begin{APACrefauthors}%
Chen, D.%
, Bolton, J.%
\BCBL {}\ \BBA {} Manning, C\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A thorough examination of the cnn/daily mail reading
  comprehension task} {A thorough examination of the cnn/daily mail reading
  comprehension task}.{\BBCQ}
\newblock
\APACjournalVolNumPages{arXiv preprint arXiv:1606.02858}{}{}{}.
\newblock
\begin{APACrefURL} [{2017-07-31}]\url{https://arxiv.org/abs/1606.02858}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Demberg%
\ \BBA {} Keller%
}{%
Demberg%
\ \BBA {} Keller%
}{%
{\protect \APACyear {2008}}%
}]{%
demberg_data_2008}
\APACinsertmetastar {%
demberg_data_2008}%
\begin{APACrefauthors}%
Demberg, V.%
\BCBT {}\ \BBA {} Keller, F.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2008}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Data from eye-tracking corpora as evidence for theories
  of syntactic processing complexity} {Data from eye-tracking corpora as
  evidence for theories of syntactic processing complexity}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Cognition}{109}{2}{193--210}.
\newblock
\begin{APACrefURL}
  \url{http://www.sciencedirect.com/science/article/pii/S0010027708001741}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Engbert%
, Longtin%
\BCBL {}\ \BBA {} Kliegl%
}{%
Engbert%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2002}}%
}]{%
engbert_dynamical_2002}
\APACinsertmetastar {%
engbert_dynamical_2002}%
\begin{APACrefauthors}%
Engbert, R.%
, Longtin, A.%
\BCBL {}\ \BBA {} Kliegl, R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2002}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A dynamical model of saccade generation in reading based
  on spatially distributed lexical processing} {A dynamical model of saccade
  generation in reading based on spatially distributed lexical
  processing}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Vision Research}{42}{5}{621--636}.
\newblock
\begin{APACrefURL}
  \url{http://www.sciencedirect.com/science/article/pii/S0042698901003017}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Engbert%
, Nuthmann%
, Richter%
\BCBL {}\ \BBA {} Kliegl%
}{%
Engbert%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2005}}%
}]{%
engbert_swift:_2005}
\APACinsertmetastar {%
engbert_swift:_2005}%
\begin{APACrefauthors}%
Engbert, R.%
, Nuthmann, A.%
, Richter, E\BPBI M.%
\BCBL {}\ \BBA {} Kliegl, R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2005}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{SWIFT}: A Dynamical Model of Saccade Generation During
  Reading} {{SWIFT}: A dynamical model of saccade generation during
  reading}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Psychological Review}{112}{4}{777--813}.
\newblock
\begin{APACrefURL}
  \url{http://doi.apa.org/getdoi.cfm?doi=10.1037/0033-295X.112.4.777}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Frank%
\ \BBA {} Bod%
}{%
Frank%
\ \BBA {} Bod%
}{%
{\protect \APACyear {2011}}%
}]{%
frank:bod:11}
\APACinsertmetastar {%
frank:bod:11}%
\begin{APACrefauthors}%
Frank, S.%
\BCBT {}\ \BBA {} Bod, R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Insensitivity of the human sentence-processing system to
  hierarchical structure} {Insensitivity of the human sentence-processing
  system to hierarchical structure}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Psychological Science}{22}{}{829--834}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Glorot%
\ \BBA {} Bengio%
}{%
Glorot%
\ \BBA {} Bengio%
}{%
{\protect \APACyear {2010}}%
}]{%
glorot-understanding-2010}
\APACinsertmetastar {%
glorot-understanding-2010}%
\begin{APACrefauthors}%
Glorot, X.%
\BCBT {}\ \BBA {} Bengio, Y.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{{\APACmonth{03}}}{}.
\newblock
{\BBOQ}\APACrefatitle {Understanding the difficulty of training deep
  feedforward neural networks} {Understanding the difficulty of training deep
  feedforward neural networks}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {{PMLR}} {{PMLR}}\ (\BPGS\ 249--256).
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Gordon%
, Plummer%
\BCBL {}\ \BBA {} Choi%
}{%
Gordon%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
gordon_see_2013}
\APACinsertmetastar {%
gordon_see_2013}%
\begin{APACrefauthors}%
Gordon, P\BPBI C.%
, Plummer, P.%
\BCBL {}\ \BBA {} Choi, W.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {See before you jump: Full recognition of parafoveal
  words precedes skips during reading.} {See before you jump: Full recognition
  of parafoveal words precedes skips during reading.}{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Experimental Psychology: Learning, Memory,
  and Cognition}{39}{2}{633}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hahn%
\ \BBA {} Keller%
}{%
Hahn%
\ \BBA {} Keller%
}{%
{\protect \APACyear {2016}}%
}]{%
Hahn:Keller:16}
\APACinsertmetastar {%
Hahn:Keller:16}%
\begin{APACrefauthors}%
Hahn, M.%
\BCBT {}\ \BBA {} Keller, F.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Modeling Human Reading with Neural Attention} {Modeling
  human reading with neural attention}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the Conference on Empirical Methods in
  Natural Language Processing} {Proceedings of the conference on empirical
  methods in natural language processing}\ (\BPGS\ 85--95).
\newblock
\APACaddressPublisher{Austin, TX}{}.
\newblock
\begin{APACrefURL}
  \url{http://homepages.inf.ed.ac.uk/keller/publications/emnlp16.pdf}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hale%
}{%
Hale%
}{%
{\protect \APACyear {2001}}%
}]{%
hale_probabilistic_2001}
\APACinsertmetastar {%
hale_probabilistic_2001}%
\begin{APACrefauthors}%
Hale, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2001}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A probabilistic {Earley} parser as a psycholinguistic
  model} {A probabilistic {Earley} parser as a psycholinguistic model}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of Conference of the North American Chapter
  of the Association for Computational Linguistics} {Proceedings of conference
  of the north american chapter of the association for computational
  linguistics}\ (\BVOL~2, \BPGS\ 159--166).
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hara%
, Kano%
\BCBL {}\ \BBA {} Aizawa%
}{%
Hara%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2012}}%
}]{%
hara_predicting_2012}
\APACinsertmetastar {%
hara_predicting_2012}%
\begin{APACrefauthors}%
Hara, T.%
, Kano, D\BPBI M\BPBI Y.%
\BCBL {}\ \BBA {} Aizawa, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Predicting word fixations in text with a {CRF} model for
  capturing general reading strategies among readers} {Predicting word
  fixations in text with a {CRF} model for capturing general reading strategies
  among readers}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the 1st Workshop on Eye-tracking and
  Natural Language Processing} {Proceedings of the 1st workshop on eye-tracking
  and natural language processing}\ (\BPGS\ 55--70).
\newblock
\begin{APACrefURL}
  [{2016-04-05}]\url{http://anthology.aclweb.org/W/W12/W12-49.pdf#page=65}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Henderson%
}{%
Henderson%
}{%
{\protect \APACyear {2003}}%
}]{%
Henderson:03}
\APACinsertmetastar {%
Henderson:03}%
\begin{APACrefauthors}%
Henderson, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Human Gaze Control in Real-world Scene Perception}
  {Human gaze control in real-world scene perception}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Trends in Cognitive Sciences}{7}{}{498--504}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hermann%
\ \protect \BOthers {.}}{%
Hermann%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2015}}%
}]{%
hermann_teaching_2015}
\APACinsertmetastar {%
hermann_teaching_2015}%
\begin{APACrefauthors}%
Hermann, K\BPBI M.%
, Ko{\v c}isk{\`y}, T.%
, Grefenstette, E.%
, Espeholt, L.%
, Kay, W.%
, Suleyman, M.%
\BCBL {}\ \BBA {} Blunsom, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Teaching machines to read and comprehend} {Teaching
  machines to read and comprehend}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {NIPS.} {Nips.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hochreiter%
\ \BBA {} Schmidhuber%
}{%
Hochreiter%
\ \BBA {} Schmidhuber%
}{%
{\protect \APACyear {1997}}%
}]{%
hochreiter_long_1997}
\APACinsertmetastar {%
hochreiter_long_1997}%
\begin{APACrefauthors}%
Hochreiter, S.%
\BCBT {}\ \BBA {} Schmidhuber, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1997}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Long short-term memory} {Long short-term memory}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Neural Computation}{9}{8}{1735--1780}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Islam%
, Henderson%
, Gomrokchi%
\BCBL {}\ \BBA {} Precup%
}{%
Islam%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2017}}%
}]{%
islam2017reproducibility}
\APACinsertmetastar {%
islam2017reproducibility}%
\begin{APACrefauthors}%
Islam, R.%
, Henderson, P.%
, Gomrokchi, M.%
\BCBL {}\ \BBA {} Precup, D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Reproducibility of Benchmarked Deep Reinforcement
  Learning Tasks for Continuous Control} {Reproducibility of benchmarked deep
  reinforcement learning tasks for continuous control}.{\BBCQ}
\newblock
\APACjournalVolNumPages{ICML 2017 Reproducibility in Machine Learning
  Workshop}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Kennedy%
\ \BBA {} Pynte%
}{%
Kennedy%
\ \BBA {} Pynte%
}{%
{\protect \APACyear {2005}}%
}]{%
kennedy_parafoveal--foveal_2005}
\APACinsertmetastar {%
kennedy_parafoveal--foveal_2005}%
\begin{APACrefauthors}%
Kennedy, A.%
\BCBT {}\ \BBA {} Pynte, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2005}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Parafoveal-on-foveal effects in normal reading}
  {Parafoveal-on-foveal effects in normal reading}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Vision Res.}{45}{2}{153--168}.
\newblock
\begin{APACrefURL}
  \url{http://linkinghub.elsevier.com/retrieve/pii/S0042698904003979}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Kim%
, Jernite%
, Sontag%
\BCBL {}\ \BBA {} Rush%
}{%
Kim%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2016}}%
}]{%
kim16}
\APACinsertmetastar {%
kim16}%
\begin{APACrefauthors}%
Kim, Y.%
, Jernite, Y.%
, Sontag, D.%
\BCBL {}\ \BBA {} Rush, A\BPBI M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Character-Aware Neural Language Models} {Character-aware
  neural language models}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the Thirtieth AAAI Conference on
  Artificial Intelligence} {Proceedings of the thirtieth aaai conference on
  artificial intelligence}\ (\BPGS\ 2741--2747).
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Levy%
}{%
Levy%
}{%
{\protect \APACyear {2008}}%
}]{%
levy_expectation-based_2008}
\APACinsertmetastar {%
levy_expectation-based_2008}%
\begin{APACrefauthors}%
Levy, R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2008}{March}{}.
\newblock
{\BBOQ}\APACrefatitle {Expectation-based syntactic comprehension}
  {Expectation-based syntactic comprehension}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Cognition}{106}{3}{1126--1177}.
\newblock
\begin{APACrefURL}
  \url{http://linkinghub.elsevier.com/retrieve/pii/S0010027707001436}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Matthies%
\ \BBA {} S{\o}gaard%
}{%
Matthies%
\ \BBA {} S{\o}gaard%
}{%
{\protect \APACyear {2013}}%
}]{%
matthies_blinkers_2013}
\APACinsertmetastar {%
matthies_blinkers_2013}%
\begin{APACrefauthors}%
Matthies, F.%
\BCBT {}\ \BBA {} S{\o}gaard, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {With Blinkers on: Robust Prediction of Eye Movements
  across Readers} {With blinkers on: Robust prediction of eye movements across
  readers}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the Conference on Empirical Methods in
  Natural Language Processing} {Proceedings of the conference on empirical
  methods in natural language processing}\ (\BPGS\ 803--807).
\newblock
\begin{APACrefURL}
  \url{http://www.aclweb.org/website/old_anthology/D/D13/D13-1075.pdf}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
McDonald%
\ \BBA {} Shillcock%
}{%
McDonald%
\ \BBA {} Shillcock%
}{%
{\protect \APACyear {2003}}%
{\protect \APACexlab {{\protect \BCnt {1}}}}}]{%
mcdonald_eye_2003}
\APACinsertmetastar {%
mcdonald_eye_2003}%
\begin{APACrefauthors}%
McDonald, S\BPBI A.%
\BCBT {}\ \BBA {} Shillcock, R\BPBI C.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003{\protect \BCnt {1}}}{November}{}.
\newblock
{\BBOQ}\APACrefatitle {Eye movements reveal the on-line computation of lexical
  probabilities during reading} {Eye movements reveal the on-line computation
  of lexical probabilities during reading}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Psychological Science}{14}{6}{648--652}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
McDonald%
\ \BBA {} Shillcock%
}{%
McDonald%
\ \BBA {} Shillcock%
}{%
{\protect \APACyear {2003}}%
{\protect \APACexlab {{\protect \BCnt {2}}}}}]{%
mcdonald_low-level_2003}
\APACinsertmetastar {%
mcdonald_low-level_2003}%
\begin{APACrefauthors}%
McDonald, S\BPBI A.%
\BCBT {}\ \BBA {} Shillcock, R\BPBI C.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003{\protect \BCnt {2}}}{July}{}.
\newblock
{\BBOQ}\APACrefatitle {Low-level predictive inference in reading: the influence
  of transitional probabilities on eye movements} {Low-level predictive
  inference in reading: the influence of transitional probabilities on eye
  movements}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Vision Research}{43}{16}{1735--1751}.
\newblock
\begin{APACrefURL}
  \url{http://www.sciencedirect.com/science/article/pii/S0042698903002372}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Mikolov%
, Karafi{\'a}t%
, Burget%
, {\v C}ernock{\'y}%
\BCBL {}\ \BBA {} Khudanpur%
}{%
Mikolov%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2010}}%
}]{%
mikolov_recurrent_2010}
\APACinsertmetastar {%
mikolov_recurrent_2010}%
\begin{APACrefauthors}%
Mikolov, T.%
, Karafi{\'a}t, M.%
, Burget, L.%
, {\v C}ernock{\'y}, J.%
\BCBL {}\ \BBA {} Khudanpur, S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Recurrent neural network based language model}
  {Recurrent neural network based language model}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of Interspeech} {Proceedings of
  interspeech}\ (\BPGS\ 1045--1048).
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Mnih%
, Heess%
, Graves%
\BCBL {}\ \BBA {} {others}%
}{%
Mnih%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2014}}%
}]{%
mnih_recurrent_2014}
\APACinsertmetastar {%
mnih_recurrent_2014}%
\begin{APACrefauthors}%
Mnih, V.%
, Heess, N.%
, Graves, A.%
\BCBL {}\ \BBA {} {others}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Recurrent models of visual attention} {Recurrent models
  of visual attention}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Advances in Neural Information Processing Systems}
  {Advances in neural information processing systems}\ (\BPGS\ 2204--2212).
\newblock
\begin{APACrefURL}
  \url{http://papers.nips.cc/paper/5542-recurrent-models-of-visual-attention}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Nilsson%
\ \BBA {} Nivre%
}{%
Nilsson%
\ \BBA {} Nivre%
}{%
{\protect \APACyear {2009}}%
}]{%
nilsson_learning_2009}
\APACinsertmetastar {%
nilsson_learning_2009}%
\begin{APACrefauthors}%
Nilsson, M.%
\BCBT {}\ \BBA {} Nivre, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Learning where to look: Modeling eye movements in
  reading} {Learning where to look: Modeling eye movements in reading}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the 13th Conference on Computational
  Natural Language Learning} {Proceedings of the 13th conference on
  computational natural language learning}\ (\BPGS\ 93--101).
\newblock
\begin{APACrefURL} \url{http://dl.acm.org/citation.cfm?id=1596392}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Nilsson%
\ \BBA {} Nivre%
}{%
Nilsson%
\ \BBA {} Nivre%
}{%
{\protect \APACyear {2010}}%
}]{%
nilsson_towards_2010}
\APACinsertmetastar {%
nilsson_towards_2010}%
\begin{APACrefauthors}%
Nilsson, M.%
\BCBT {}\ \BBA {} Nivre, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Towards a data-driven model of eye movement control in
  reading} {Towards a data-driven model of eye movement control in
  reading}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the Workshop on Cognitive Modeling and
  Computational Linguistics} {Proceedings of the workshop on cognitive modeling
  and computational linguistics}\ (\BPGS\ 63--71).
\newblock
\begin{APACrefURL} \url{http://dl.acm.org/citation.cfm?id=1870073}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pennington%
, Socher%
\BCBL {}\ \BBA {} Manning%
}{%
Pennington%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2014}}%
}]{%
pennington_glove_2014}
\APACinsertmetastar {%
pennington_glove_2014}%
\begin{APACrefauthors}%
Pennington, J.%
, Socher, R.%
\BCBL {}\ \BBA {} Manning, C\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Glove: Global Vectors for Word Representation.} {Glove:
  Global vectors for word representation.}{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {EMNLP} {Emnlp}\ (\BPGS\ 1532--1543).
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Petrov%
, Das%
\BCBL {}\ \BBA {} McDonald%
}{%
Petrov%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2012}}%
}]{%
petrov_universal_2012}
\APACinsertmetastar {%
petrov_universal_2012}%
\begin{APACrefauthors}%
Petrov, S.%
, Das, D.%
\BCBL {}\ \BBA {} McDonald, R\BPBI T.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A Universal Part-of-Speech Tagset} {A universal
  part-of-speech tagset}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the 8th International Conference on
  Language Resources and Evaluation} {Proceedings of the 8th international
  conference on language resources and evaluation}\ (\BPGS\ 2089--2096).
\newblock
\begin{APACrefURL}
  \url{http://www.lrec-conf.org/proceedings/lrec2012/summaries/274.html}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pinheiro%
\ \BBA {} Bates%
}{%
Pinheiro%
\ \BBA {} Bates%
}{%
{\protect \APACyear {2000}}%
}]{%
Pinheiro:Bates:00}
\APACinsertmetastar {%
Pinheiro:Bates:00}%
\begin{APACrefauthors}%
Pinheiro, J\BPBI C.%
\BCBT {}\ \BBA {} Bates, D\BPBI M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2000}.
\newblock
\APACrefbtitle {Mixed-Effects Models in {S} and {S-PLUS}} {Mixed-effects models
  in {S} and {S-PLUS}}.
\newblock
\APACaddressPublisher{New York}{Springer}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Rayner%
}{%
Rayner%
}{%
{\protect \APACyear {1998}}%
}]{%
rayner_eye_1998}
\APACinsertmetastar {%
rayner_eye_1998}%
\begin{APACrefauthors}%
Rayner, K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1998}{November}{}.
\newblock
{\BBOQ}\APACrefatitle {Eye movements in reading and information processing: 20
  years of research} {Eye movements in reading and information processing: 20
  years of research}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Psychological Bulletin}{124}{3}{372--422}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Rayner%
\ \BBA {} Pollatsek%
}{%
Rayner%
\ \BBA {} Pollatsek%
}{%
{\protect \APACyear {1989}}%
}]{%
Rayner:Pollatsek:89}
\APACinsertmetastar {%
Rayner:Pollatsek:89}%
\begin{APACrefauthors}%
Rayner, K.%
\BCBT {}\ \BBA {} Pollatsek, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{1989}.
\newblock
\APACrefbtitle {The Psychology of Reading} {The psychology of reading}.
\newblock
\APACaddressPublisher{Englewood Cliffs, NJ}{Prentice Hall}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Rayner%
\ \BBA {} Reichle%
}{%
Rayner%
\ \BBA {} Reichle%
}{%
{\protect \APACyear {2010}}%
}]{%
rayner_models_2010-1}
\APACinsertmetastar {%
rayner_models_2010-1}%
\begin{APACrefauthors}%
Rayner, K.%
\BCBT {}\ \BBA {} Reichle, E\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Models of the Reading Process} {Models of the reading
  process}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Wiley Interdisciplinary Reviews: Cognitive
  Science}{1}{6}{787--799}.
\newblock
\begin{APACrefURL} \url{http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3001687/}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Reichle%
, Pollatsek%
, Fisher%
\BCBL {}\ \BBA {} Rayner%
}{%
Reichle%
\ \protect \BOthers {.}}{%
{\protect \APACyear {1998}}%
}]{%
reichle_toward_1998}
\APACinsertmetastar {%
reichle_toward_1998}%
\begin{APACrefauthors}%
Reichle, E\BPBI D.%
, Pollatsek, A.%
, Fisher, D\BPBI L.%
\BCBL {}\ \BBA {} Rayner, K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1998}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Toward a model of eye movement control in reading}
  {Toward a model of eye movement control in reading}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Psychological Review}{105}{1}{125--157}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Reichle%
, Rayner%
\BCBL {}\ \BBA {} Pollatsek%
}{%
Reichle%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2003}}%
}]{%
reichle_ez_2003}
\APACinsertmetastar {%
reichle_ez_2003}%
\begin{APACrefauthors}%
Reichle, E\BPBI D.%
, Rayner, K.%
\BCBL {}\ \BBA {} Pollatsek, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The {EZ Reader} model of eye-movement control in
  reading: Comparisons to other models} {The {EZ Reader} model of eye-movement
  control in reading: Comparisons to other models}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Behavioral and Brain Sciences}{26}{04}{445--476}.
\newblock
\begin{APACrefURL}
  \url{http://journals.cambridge.org/abstract_S0140525X03000104}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Reichle%
, Warren%
\BCBL {}\ \BBA {} McConnell%
}{%
Reichle%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2009}}%
}]{%
reichle_using_2009}
\APACinsertmetastar {%
reichle_using_2009}%
\begin{APACrefauthors}%
Reichle, E\BPBI D.%
, Warren, T.%
\BCBL {}\ \BBA {} McConnell, K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{February}{}.
\newblock
{\BBOQ}\APACrefatitle {Using {EZ Reader} to model the effects of higher level
  language processing on eye movements during reading} {Using {EZ Reader} to
  model the effects of higher level language processing on eye movements during
  reading}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Psychonomic Bulletin \& Review}{16}{1}{1--21}.
\newblock
\begin{APACrefURL} \url{http://www.springerlink.com/index/10.3758/PBR.16.1.1}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Schotter%
, Bicknell%
, Howard%
, Levy%
\BCBL {}\ \BBA {} Rayner%
}{%
Schotter%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2014}}%
}]{%
Schotter:ea:14}
\APACinsertmetastar {%
Schotter:ea:14}%
\begin{APACrefauthors}%
Schotter, E\BPBI R.%
, Bicknell, K.%
, Howard, I.%
, Levy, R.%
\BCBL {}\ \BBA {} Rayner, K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Task effects reveal cognitive flexibility responding to
  frequency and predictability: Evidence from eye movements in reading and
  proofreading} {Task effects reveal cognitive flexibility responding to
  frequency and predictability: Evidence from eye movements in reading and
  proofreading}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Cognition}{131}{1}{1--27}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Smith%
\ \BBA {} Levy%
}{%
Smith%
\ \BBA {} Levy%
}{%
{\protect \APACyear {2013}}%
}]{%
smith_effect_2013}
\APACinsertmetastar {%
smith_effect_2013}%
\begin{APACrefauthors}%
Smith, N\BPBI J.%
\BCBT {}\ \BBA {} Levy, R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{September}{}.
\newblock
{\BBOQ}\APACrefatitle {The effect of word predictability on reading time is
  logarithmic} {The effect of word predictability on reading time is
  logarithmic}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Cognition}{128}{3}{302--319}.
\newblock
\begin{APACrefURL}
  \url{http://linkinghub.elsevier.com/retrieve/pii/S0010027713000413}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Sutskever%
, Vinyals%
\BCBL {}\ \BBA {} Le%
}{%
Sutskever%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2014}}%
}]{%
sutskever_sequence_2014}
\APACinsertmetastar {%
sutskever_sequence_2014}%
\begin{APACrefauthors}%
Sutskever, I.%
, Vinyals, O.%
\BCBL {}\ \BBA {} Le, Q\BPBI V.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Sequence to sequence learning with neural networks}
  {Sequence to sequence learning with neural networks}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Advances in Neural Information Processing Systems}
  {Advances in neural information processing systems}\ (\BPGS\ 3104--3112).
\newblock
\begin{APACrefURL}
  \url{http://papers.nips.cc/paper/5346-information-based-learning-by-agents-in-unbounded-state-spaces}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Van~Gompel%
\ \BBA {} Pickering%
}{%
Van~Gompel%
\ \BBA {} Pickering%
}{%
{\protect \APACyear {2007}}%
}]{%
van_gompel_syntactic_2007}
\APACinsertmetastar {%
van_gompel_syntactic_2007}%
\begin{APACrefauthors}%
Van~Gompel, R\BPBI P.%
\BCBT {}\ \BBA {} Pickering, M\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2007}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Syntactic Parsing} {Syntactic parsing}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {The Oxford Handbook of Psycholinguistics} {The oxford
  handbook of psycholinguistics}\ (\BPGS\ 289--307).
\newblock
\APACaddressPublisher{}{Oxford University Press}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Williams%
}{%
Williams%
}{%
{\protect \APACyear {1992}}%
}]{%
williams_simple_1992}
\APACinsertmetastar {%
williams_simple_1992}%
\begin{APACrefauthors}%
Williams, R\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1992}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Simple statistical gradient-following algorithms for
  connectionist reinforcement learning} {Simple statistical gradient-following
  algorithms for connectionist reinforcement learning}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Machine Learning}{8}{3-4}{229--256}.
\newblock
\begin{APACrefURL} \url{http://link.springer.com/article/10.1007/BF00992696}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Xu%
\ \protect \BOthers {.}}{%
Xu%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2015}}%
}]{%
xu_show_2015}
\APACinsertmetastar {%
xu_show_2015}%
\begin{APACrefauthors}%
Xu, K.%
, Ba, J.%
, Kiros, R.%
, Courville, A.%
, Salakhutdinov, R.%
, Zemel, R.%
\BCBL {}\ \BBA {} Bengio, Y.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
\APACrefbtitle {Show, attend and tell: Neural image caption generation with
  visual attention.} {Show, attend and tell: Neural image caption generation
  with visual attention.}
\newblock
\APACrefnote{arXiv:1502.03044}
\PrintBackRefs{\CurrentBib}

\end{thebibliography}
