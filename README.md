# task-effects-neural-attention

## Study 1

[Train Language Model](study1/python/surprisal)

[Train Decoder](study1/python/autoencoder)

[Train Attention](study1/python/attention)

[Accuracies](study1/dundee/analyze/compare_NEAT_Human/accuracy)

[Compute Surprisals](study1/python/surprisal)

[Reading Time Evaluation](study1/dundee/analyze/compare_NEAT_Human/reading-times)

## Experiment 1

[Descriptive statistics](study2/analysis/analysis/basic_stats/replication_descriptive.R)

[Reading times on the answer](study2/analysis/analysis/basic_stats/replication_answer_scores.R)

[Mixed Effects Models](study2/analysis/analysis/mixed_models)

[Heatmaps](study2/analysis/analysis_with_NEAT/heatmaps)

## Study 2

[Pretrain QA Model](study2/NEAT/pretrain_qa)

[Train NEAT](study2/NEAT/train_NEAT)

[Descriptive statistics](study2/NEAT/results/tradeoff.tsv)

[Mixed Effects Model](study2/analysis/analysis_with_NEAT/analyzing_neat)

[Predicting Reading Measures](study2/analysis/analysis_with_NEAT/neat_predicts_human)

[Heatmaps](study2/analysis/analysis_with_NEAT/heatmaps)
