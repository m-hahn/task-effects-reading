
import os

directoryIn = "/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training2/"


directoryOutTexts = "/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training4/"
fileCounter = 0
files = os.listdir(directoryIn)
for fileName in files:
         fileCounter = fileCounter + 1.0
         if fileCounter % 1000 == 0:
             print(" . "+fileName+" "+str(fileCounter / len(files)))
         f = open(directoryIn+"/"+fileName, 'r')      


         counter = 0

         fOut = open(directoryOutTexts+fileName+"-0", 'w')

         for line in f:
             counter = counter + 1
             fOut.write(line)
             if counter % 100 == 0:
                 fOut.close()
                 fOut = open(directoryOutTexts+fileName+"-"+str(counter/100), 'w')


f.close()

# Note at the end of a file, the last out handle will not be closed. this may generate problems downstream when they are opened and it is assumed the last one is still wellformed and complete


