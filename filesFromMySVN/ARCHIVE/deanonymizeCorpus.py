# September 2017, for reconstructing the original corpus for EMNLP experiments

import os 

dictionaryPath = "/afs/cs.stanford.edu/u/mhahn/scr/num2CharsWithEntities"
inPath = "/u/nlp/data/deepmind-qa/dailymail/train/"
fileNames = open("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/dailymail-files.txt", "r").read().strip().split("\n")
outDir = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/dailymail/"




with open(dictionaryPath, "r") as dictionaryFile:
   def reverseEntry(x):
     x = x.split(" ")
     assert len(x) == 2
     x = (x[1], x[0])
     return x
   dictionary = dict(map(reverseEntry, dictionaryFile.read().strip().split("\n")))

def getFromDict(word):
   word = word.lower()
   if word in dictionary:
     return dictionary[word]
   else:
     return "50000"


for j in range(0,len(fileNames)):
   fileName = fileNames[j]
   if j % 100 == 0:
      print (j*100.0)/len(fileNames)
   with open(inPath+fileName, "r") as inFile:
     doc = inFile.read().strip().split("\n")
     text = doc[2].split(" ")
     mapping = dict(map(lambda x:(x[0], x[1]), map(lambda x: x.split(":"), doc[8:])))
     with open(outDir+"/"+fileName, "w") as outFile:
      #print outDir+"/"+fileName
      for i in range(0,len(text)):
       if text[i] in mapping:
          text[i] = mapping[text[i]]
       text[i] = text[i].split(" ")
       for word in text[i]:
         print >> outFile, getFromDict(word)

