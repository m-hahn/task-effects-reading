-- neat-qa-SplitInput.lua was split from neat-qa-Unrolled-Attention.lua (11/25/2016)
-- This is for soft attention. Unrolled-Attention is for hard attention of the vanilla LSTM model, and also has soft atention functionality.
-- UNDO-CHANGES is for vanilla LSTM without hard attention.

neatQA = {}
neatQA.__name = "neat-qa-SplitInput.lua"

neatQA.number_of_LSTM_layers = 1

neatQA.ALSO_DO_LANGUAGE_MODELING = false

neatQA.ACCESS_MEMORY = true

neatQA.INITIALIZE_FROM_NEAT = false--true--false
neatQA.DO_BIDIRECTIONAL_MEMORY = true--true--false
neatQA.rewardBasedOnLogLikeLoss = true --false
neatQA.USE_ATTENTION_NETWORK = false
neatQA.USE_PRETRAINED_EMBEDDINGS = true
assert(neatQA.USE_PRETRAINED_EMBEDDINGS or DOING_DEBUGGING)
neatQA.GET_MORE_THAN_EMBEDDINGS_FROM_NEAT = false

QUESTION_LENGTH = 50

assert(neatQA.ACCESS_MEMORY)
assert(not(neatQA.GET_MORE_THAN_EMBEDDINGS_FROM_NEAT and (not neatQA.INITIALIZE_FROM_NEAT)))

assert( not (neatQA.USE_PRETRAINED_EMBEDDINGS and neatQA.INITIALIZE_FROM_NEAT))

neatQA.numberOfExamples = 0.0 -- in the cuny-split-predictors version, this is done in qaReinforce.lua

neatQA.attentionRateNoPreview = 0.5
neatQA.attentionRatePreview = 0.5
neatQA.attentionRate = 0.5

neatQA.accuracyPreview = 0.5
neatQA.accuracyNoPreview = 0.5
neatQA.accuracy = 0.5


neatQA.attentionRateNoPreviewSum = 0.0
neatQA.attentionRatePreviewSum = 0.0
neatQA.attentionRateSum = 0.0

neatQA.accuracyPreviewSum = 0.0
neatQA.accuracyNoPreviewSum = 0.0
neatQA.accuracySum = 0.0

neatQA.numberOfExamples = 0.0

neatQA.nllSumPreview = 0.0
neatQA.nllSumNoPreview = 0.0
neatQA.examplesPreview = 0.0000001
neatQA.examplesNoPreview = 0.0000001





print(neatQA)



require('auxiliary')
require('nn.RecursorMod')
require('nn.SequencerMod')
require('nn.PrintLayer')
require('nn.BlockGradientLayer')


require('qaAttentionAnswerer')
require('recurrentNetworkOnSequence')

require('qaReinforce')

function neatQA.createAnswerNetwork(param,gradparam)
    return qaAttentionAnswerer.createAnswerNetworkWithMemoryAndQuestionAttention(param,gradparam)
end


function neatQA.setup()
  print("Creating a RNN LSTM network.")

  print("Setting params.init_weight (60378)")
  params.init_weight=0.01


  -- initialize data structures
  model.dsR = {}
  model.dsR[1] = transfer_data(torch.zeros(params.rnn_size))
  model.dsR[2] = transfer_data(torch.zeros(params.rnn_size))
  if neatQA.ALSO_DO_LANGUAGE_MODELING then
    model.dsR[3] = transfer_data(torch.zeros(params.vocab_size)) -- NOTE actually will later have different size
  end
   

  reader_c ={}
  reader_h = {}

  reader_output = {}
  
  assert(neatQA.number_of_LSTM_layers == 1)

    reader_c[0] = torch.CudaTensor(params.batch_size,params.rnn_size):zero() 
    reader_h[0] = torch.CudaTensor(params.batch_size,params.rnn_size):zero()



  neatQA.criterionDerivative = torch.DoubleTensor(params.batch_size, NUMBER_OF_ANSWER_OPTIONS) 


  attention_decisions = {}
  attention_scores = {}
  baseline_scores = {}
  attended_input_tensors = {}
  attention_probabilities = {}
  for i=1, params.seq_length do
     attention_decisions[i] = torch.CudaTensor(params.batch_size)
     attention_scores[i] = torch.CudaTensor(params.batch_size,1)
     baseline_scores[i] = torch.CudaTensor(params.batch_size,1)
     attended_input_tensors[i] = torch.CudaTensor(params.batch_size,1)
     attention_probabilities[i] = torch.CudaTensor(params.batch_size,1)
  end

  probabilityOfChoices = torch.FloatTensor(params.batch_size)
  totalAttentions = torch.FloatTensor(params.batch_size) -- apparently using CudaTensor would cause a noticeable slowdown...?!
  nll = torch.FloatTensor(params.batch_size)

  attention_inputTensors = {}


  ones = transfer_data(torch.ones(params.batch_size))
  rewardBaseline = 0


  local embeddings = nil
  local embeddingsGrad = nil
  if neatQA.USE_PRETRAINED_EMBEDDINGS then
--        local parameters, _ = reader_core_network:parameters()
        embeddings = torch.CudaTensor(params.vocab_size+1,params.embeddings_dimensionality)
        embeddingsGrad = torch.CudaTensor(params.vocab_size+1,params.embeddings_dimensionality):zero()
        readDict.setToPretrainedEmbeddings(embeddings)
    if true then
     print(embeddings[readDict.word2Num("beer")+1]*embeddings[readDict.word2Num("wine")+1])
     print(embeddings[readDict.word2Num("computer")+1]*embeddings[readDict.word2Num("wine")+1])
     print(embeddings[readDict.word2Num("paper")+1]*embeddings[readDict.word2Num("wine")+1])
     print(embeddings[readDict.word2Num("drink")+1]*embeddings[readDict.word2Num("wine")+1])
     --print(embeddings[readDict.word2Num("towel")+1]*embeddings[readDict.word2Num("paper")+1])
     print(embeddings[readDict.word2Num("paper")+1]*embeddings[readDict.word2Num("article")+1])
     print(embeddings[readDict.word2Num("paper")+1]*embeddings[readDict.word2Num("stone")+1])
     print(embeddings[readDict.word2Num("paper")+1]*embeddings[readDict.word2Num("elephant")+1])
     print(embeddings[readDict.word2Num("paper")+1]*embeddings[readDict.word2Num("bicycle")+1])
     print(embeddings[readDict.word2Num("@entity5")+1]*embeddings[readDict.word2Num("@entity7")+1])
    end
  end

local params2, sentencesRead ,SparamxForward, SparamdxForward, SparamxBackward, SparamdxBackward, SparamxQForward, SparamdxQForward, SparamxQBackward, SparamdxQBackward, SparamxA, SparamdxA, SparamxRA, SparamdxRA
if LOAD then

     print("LOADING MODEL AT "..BASE_DIRECTORY.."/model-"..fileToBeLoaded)
     local storedModel = torch.load(BASE_DIRECTORY.."/model-"..fileToBeLoaded, "binary")
--     params2, sentencesRead ,SparamxForward, SparamdxForward, SparamxBackward, SparamdxBackward, SparamxQForward, SparamdxQForward, SparamxQBackward, SparamdxQBackward, SparamxA, SparamdxA, SparamxRA, SparamdxRA = unpack(storedModel)
print("Parameters from the stored Model:")
print(storedModel)

params2 = storedModel.params
sentencesRead       = storedModel.readWords
SparamxForward      = storedModel.SparamxForward
SparamdxForward      = storedModel.SparamdxForward
SparamxBackward      = storedModel.SparamxBackward
SparamdxBackward      = storedModel.SparamdxBackward
SparamxQForward      = storedModel.SparamxQForward
SparamdxQForward      = storedModel.SparamdxQForward
SparamxQBackward      = storedModel.SparamxQBackward
SparamdxQBackward      = storedModel.SparamdxQBackward
SparamxA      = storedModel.SparamxA
SparamdxA      = storedModel.SparamdxA
SparamxRA      = storedModel.SparamxRA
SparamdxRA      = storedModel.SparamdxRA

--           modelsArray = {params,(numberOfWords/params.seq_length),SparamxForward, SparamdxForward, SparamxBackward, SparamdxBackward, SparamxQForward, SparamdxQForward, SparamxQBackward, SparamdxQBackward, SparamxA, SparamdxA, SparamxRA, SparamdxRA}

    print(params2)
--crash()
else
  SparamxForward = {embeddings}
  SparamdxForward = {embeddingsGrad}
  SparamxBackward = {embeddings}
  SparamdxBackward = {embeddingsGrad}
  SparamxQForward = {embeddings}
  SparamdxQForward = {embeddingsGrad}
  SparamxQBackward = {embeddings}
  SparamdxQBackward = {embeddingsGrad}
  SparamxA = {}
  SparamdxA = {}
end


     forward_network = RecurrentNetworkOnSequence.new(params.rnn_size,SparamxForward,SparamdxForward,params.seq_length)
     backward_network = RecurrentNetworkOnSequence.new(params.rnn_size, SparamxBackward,SparamdxBackward,params.seq_length)
     question_forward_network = RecurrentNetworkOnSequence.new(params.rnn_size, SparamxQForward,SparamdxQForward, QUESTION_LENGTH)
     question_backward_network =  RecurrentNetworkOnSequence.new(params.rnn_size, SparamxQBackward,SparamdxQBackward, QUESTION_LENGTH)

   -- II execute getParameters()
   actor_core_network = neatQA.createAnswerNetwork(SparamxA, SparamdxA)
   paramxA, paramdxA = actor_core_network:getParameters()
   paramdxA:zero()

   -- III build clones
   if neatQA.USE_ATTENTION_NETWORK then
      attentionNetwork = attention.createAttentionNetwork() 
      paramxRA, paramdxRA = attentionNetwork:getParameters()
      paramdxRA:zero()
      attentionNetworks = {}
      auxiliary.buildClones(params.seq_length,attentionNetworks,attentionNetwork)
   end


   vectorOfLengths = torch.LongTensor(params.batch_size)
   neatQA.maximalLengthOccurringInInput = {0}
   neatQA.maximalLengthOccurringInInputQuestion = {0}
end


function neatQA.parameters()
   local parameters = {}
   local gradParameters = {}
   local modules = {forward_network, backward_network, question_forward_network, question_backward_network}
   for q=1, #modules do
     local p, dp = modules[1]:parameters()
     table.insert(parameters,p)
     table.insert(gradParameters,dp)
   end
   return parameters, gradParameters
end




function neatQA.fp(corpus, startIndex, endIndex)


   neatQA.maxLengthsPerItem = torch.LongTensor(params.batch_size)

    neatQA.inputTensors, neatQA.inputTensorsQuestion = auxiliary.buildSeparateInputTensorsQA(corpus,startIndex,endIndex,neatQA.maxLengthsPerItem,neatQA.maximalLengthOccurringInInput, neatQA.maximalLengthOccurringInInputQuestion)

  neatQA.answerTensors =  qa.buildAnswerTensor(corpus, startIndex, endIndex)


-----------------------
-----------------------

attended_input_tensors = auxiliary.shallowCopyTable(neatQA.inputTensors)


------------------------
------------------------
--print("170 TEXTS")
--print(neatQA.inputTensors[1])
--print(neatQA.inputTensors[2])
--print(neatQA.inputTensors[3])
--print(neatQA.inputTensors[4])
--print("... QUESTIONS")
--print(neatQA.inputTensorsQuestion[1])
--print(neatQA.answerTensors)

  for i=1, neatQA.maximalLengthOccurringInInput[1] do
         attention_decisions[i] = attention_decisions[i]:view(-1)
         attended_input_tensors[i], _ = hardAttention.makeAttentionDecisions(i, neatQA.inputTensors[i])
         attention_decisions[i] = attention_decisions[i]:view(params.batch_size,1)
  end

--print("Input Tensors")
--for i=1,4 do
--print(attended_input_tensors[i])
--print(auxiliary.reverseTable(attended_input_tensors, neatQA.maximalLengthOccurringInInput[1])[i])
--end

  print("40  "..neatQA.maximalLengthOccurringInInput[1])

  question_forward_cs, question_forward_hs = question_forward_network:fp(neatQA.inputTensorsQuestion,neatQA.maximalLengthOccurringInInputQuestion[1])
  question_backward_cs, question_backward_hs = question_backward_network:fp(auxiliary.reverseTable(neatQA.inputTensorsQuestion,neatQA.maximalLengthOccurringInInputQuestion[1]),neatQA.maximalLengthOccurringInInputQuestion[1])
  forward_cs, forward_hs = forward_network:fp(attended_input_tensors,neatQA.maximalLengthOccurringInInput[1])
  backward_cs, backward_hs = backward_network:fp(auxiliary.reverseTable(attended_input_tensors, neatQA.maximalLengthOccurringInInput[1]),neatQA.maximalLengthOccurringInInput[1])

--print("19811 forward and backward") 
--print(forward_hs[1])
--print(auxiliary.reverseTable(backward_hs, neatQA.maximalLengthOccurringInInput[1])[1])
 
--print("1887 QUESTION FORWARD")
--print(question_forward_hs[neatQA.maximalLengthOccurringInInputQuestion[1]])
  neatQA.actorInput ={forward_hs, auxiliary.reverseTable(backward_hs, neatQA.maximalLengthOccurringInInput[1]), question_forward_hs[neatQA.maximalLengthOccurringInInputQuestion[1]], question_backward_hs[neatQA.maximalLengthOccurringInInputQuestion[1]]}
--print(neatQA.actorInput)

  actor_output = actor_core_network:forward(neatQA.actorInput):float()
  --print("Q19411")
  --print(neatQA.inputTensorsQuestion[1])
  --print("19410 ACTOR OUTPUT")
  --print(actor_output)
  --print((actor_output[1]-actor_output[2]):norm())
--crash()
---------------------------------
----------------------------------

  for i=1, params.batch_size do
    nll[i] = - actor_output[i][neatQA.answerTensors[i]]
  end

  meanNLL = 0.95 * meanNLL + 0.05 * nll:mean()

  return nll, actor_output
end


function neatQA.bp(corpus, startIndex, endIndex)
  auxiliary.prepareMomentum(paramdxA)

if false then
  reset_ds()
end

if false then  
  TRAIN_LANGMOD = true
  TRAIN_AUTOENCODER = true
end

  if params.lr > 0 and (true or train_autoencoding) then --hrhr
------------ CRITERION
    derivativeFromCriterion = neatQA.criterionDerivative
    derivativeFromCriterion:zero()
    for i=1, params.batch_size do
      derivativeFromCriterion[i][neatQA.answerTensors[i]] = -1
    end

   assert(neatQA.DO_BIDIRECTIONAL_MEMORY)

-----------------------------------------------
------------ ACTOR ----------------------------
-----------------------------------------------
if not true then
 print("Setting to zero 26825")
 paramdxA:zero()
end



    local actorGradient = actor_core_network:backward(neatQA.actorInput, transfer_data(derivativeFromCriterion))

--[[for i = 1,4 do
    print(i)
    print(actorGradient[1][i])
    print(actorGradient[2][i])
end
    print(actorGradient[3])
    print(actorGradient[4])
    print("...24215")]]

    auxiliary.clipGradients(paramdxA)
    auxiliary.updateParametersWithMomentum(paramxA,paramdxA)

--   local paramsA, paramdsA = actor_core_network:parameters()
--   print("25913 Bilinear Parameters")
  -- print(paramsA[1])
   --print(paramsA[2])
   


--------------------------------------------------------
------------ BACKWARD PASS FOR FORWARD READER ----------
--------------------------------------------------------

   forward_network:bp(attended_input_tensors,neatQA.maximalLengthOccurringInInput[1], {nil, nil,nil,actorGradient[1]})
   backward_network:bp(auxiliary.reverseTable(attended_input_tensors, neatQA.maximalLengthOccurringInInput[1]),neatQA.maximalLengthOccurringInInput[1], {nil, nil, nil, auxiliary.reverseTable(actorGradient[2], neatQA.maximalLengthOccurringInInput[1])})
   question_forward_network:bp(neatQA.inputTensorsQuestion,neatQA.maximalLengthOccurringInInputQuestion[1], {nil,actorGradient[3],nil,nil})
   question_backward_network:bp(auxiliary.reverseTable(neatQA.inputTensorsQuestion,   neatQA.maximalLengthOccurringInInputQuestion[1] ),neatQA.maximalLengthOccurringInInputQuestion[1], {nil,actorGradient[4],nil,nil})



  end
  neatQA.doBackwardForAttention()
end


function neatQA.printStuff(perp, actor_output, since_beginning, epoch, numberOfWords)

            print("+++++++ "..perp[1]..'  '..meanNLL)
             print(epoch.."  "..readChunks.corpusReading.currentFile..
               '   since beginning = ' .. since_beginning .. ' mins.')  
            print(experimentNameOut)
            print(params) 

            local correct = 0.0
            local incorrect = 0.0  
            for l = 1, params.batch_size do
               print("batch index "..l)
               --print("PERP "..perp[l])
               --print(neatQA.answerTensors[l])
               --print(qa.getFromAnswer(readChunks.corpus,l,1))
               --print("todo make sure the right numbers are used for the answers (numbers vs. numbersToEntityIDs)")
               
               local answerID = qa.getFromAnswer(readChunks.corpus,l,1)
               if answerID == nil then
                    print("463: answerID == nil")
                    answerID = 1
               end
               if  (DOING_DEBUGGING or false or math.random() < 0.006) then
--                  auxiliary.deepPrint(neatQA.inputTensors, function (tens) return tens[l] end)
                  print(45825)

                  for j=1,neatQA.maximalLengthOccurringInInput[1] do
if false then
print("...")
print(j)
print(l)
print(neatQA.inputTensors[j])
print(neatQA.inputTensors[j][l])
end

                    if neatQA.inputTensors[j][l] == 0 then
                       break
                    end
  --                  local wordProbabilities = neatQA.fullOutput[1][1][j-1][3][l]
--                    local predictedScoresLM, predictedTokensLM = torch.min( wordProbabilities,1)
    --                io.write((readDict.chars[neatQA.inputTensors[j][l]]))--..'\n')
      --              io.write(" \t "..readDict.chars[predictedTokensLM[1]])
        --            io.write("  "..math.exp(-predictedScoresLM[1]))
          --          io.write("  "..math.exp(-wordProbabilities[neatQA.inputTensors[j][l]]))

--                  local predictedScores, predictedTokens = torch.min(actor_output[j][l],1)
--print(attention_decisions[j])
auxiliary.write((readDict.chars[neatQA.inputTensors[j][l]]))
auxiliary.write(attention_decisions[j][l][1])
auxiliary.write(attention_scores[j][l][1])
if neatQA.ACCESS_MEMORY then
--print(globalForExpOutput.softAttentionsContainer.output)
--crash()
--print(j)
--print(l)
--print("66310")
   auxiliary.write(globalForExpOutput.softAttentionsContainer.output[l][1][j])
end
--auxiliary.write(attention_probabilities[j][l][1])
io.write("\n")


--attention_scores[i], attention_decisions[i]
--"\n")
                  end


               end
--               print(readChunks.corpus[l].text)
               print("ANSW       "..answerID)
               print("PROB       "..actor_output[l][answerID])
               local predictedScore,predictedAnswer = torch.max(actor_output[l],1)
--               print(predictedAnswer)
  --             print(predictedScore)
               print("PREDICTED  "..predictedAnswer[1].." # "..predictedScore[1])
--               local negSample = math.random(math.min(10, actor_output[l]:size()[1]))
  --             print("NEGATIVE EX PROB "..actor_output[l][negSample].." ("..negSample..")")
    --           if (math.abs(actor_output[l][answerID]) <= math.abs(actor_output[l][negSample])) then
               if (answerID == predictedAnswer[1]) then
                 correct = correct + 1.0
               else
                 incorrect = incorrect + 1.0
               end
               --print(actor_output[l])
               --print("PERP "..math.exp(-actor_output[l][answerID]))

            end
            fileStats:write((numberOfWords/params.seq_length)..'\t'..perp[1]..'\n')
            fileStats:flush()
            print("APPROX PERFORMANCE  "..(correct / (correct + incorrect)))
            globalForExpOutput.accuracy = 0.95 * globalForExpOutput.accuracy + 0.05 * (correct / (correct+incorrect))

            print("Avg performance     "..(globalForExpOutput.accuracy))
--            globalForExpOutput.correct = globalForExpOutput.correct * 0.9
  --          globalForExpOutput.incorrect = globalForExpOutput.incorrect * 0.9
end




