from subprocess import call
from subprocess import Popen, PIPE
import sys

# July 2017, modeled on the scripts in cuny-split-predictors

print("WARNING: Have you made sure CREATE_RECORDED_FILES is set to true?")

LOOK_AT_ORIGINAL_CONDITION = True

seq_length = int(sys.argv[1])
gpu_number = str(int(sys.argv[2]))

CORPUS_NUMBER = 18

CONDITION = "UNDEFINED"
if len(sys.argv) > 1:
  CONDITION = sys.argv[1] #"nopreview"

##############
with open("modelsForPredictions.txt","r") as myfile:
   data = myfile.read().split('\n')
############

# constraining to the conditions listed here
doConditions = ["mixed"] #"fullpreview","fullnopreview"]

OFFSET = 6

for model in data:
  if len(model) == 0:
    continue
  if model[0] == "#":
    continue
#  if LOOK_AT_ORIGINAL_CONDITION:
 #   if "nopreview" in model and CONDITION == "preview":
  #    continue
   # elif (not ("nopreview" in model)) and CONDITION == "nopreview":
    #  continue
  while "  " in model:
    model = model.replace("  "," ")
  command = model.split(">")
  if len(command) > 1:
    assert len(command) == 2
    originalOutputFile = command[1].rstrip().lstrip()
    assert (not (" " in originalOutputFile))
  else:
    #originalOutputFile = "Output-For-"+model[1]+".txt"
    assert False

  resu = Popen(["grep","Printing stuff",originalOutputFile],stdout=PIPE)
  nameOfModel = resu.communicate()
  nameOfModel = nameOfModel[0].rstrip().replace("Printing stuff to ","")
  if nameOfModel == "":
    print("ERROR: No model file")
    print(model)
    continue

  command = command[0].split(" ")
  print(command)

  CONDITION = command[OFFSET+25][:-1]
  #if not (CONDITION in doConditions):
  #   continue
  command[4] = "run-stats-output.json"
  command[OFFSET+2] = "true"
  command[OFFSET+3] = "true"
  command[OFFSET+5] = str(seq_length)
  command[OFFSET+9] = "false" # use attention network?
  command[OFFSET+14] = "true"
  command[OFFSET+15] = nameOfModel
  command[OFFSET+16] = "OUTPUT-DEV-"+CONDITION+"-"+nameOfModel
  command[OFFSET+18] = "false"
  command[OFFSET+19] = str(CORPUS_NUMBER)
  command[OFFSET+23] = "false"
  command[OFFSET+24] = "fixed"
#  if len(command) < OFFSET+25+1:
#    command.append("")
#    command[OFFSET+24] = command[OFFSET+24][:-1]
  command[OFFSET+25] = CONDITION # don't change the condition
  parametersFileNumber = command[OFFSET+1]

  originalPath = originalOutputFile.split("/")
  originalPrefix = originalPath[:-1]
  originalFileName = originalPath[-1]



  command = ["CUDA_VISIBLE_DEVICES="+gpu_number]+command[5:]
  command[1] = command[1][1:]
 

  command = command + [">", "/jagupard10/scr1/mhahn/OUT-DEV-"+CONDITION+"-"+str(CORPUS_NUMBER)+"-"+str(seq_length)+"-"+parametersFileNumber+"-"+originalFileName]
  print(command)
  print("TO EXECUTE")
  print(" ".join(command))
  call(" ".join(command), shell=True)
