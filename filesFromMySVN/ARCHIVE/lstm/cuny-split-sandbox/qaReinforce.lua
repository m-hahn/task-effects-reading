-- extracted from neat-qa-Unrolled-Attention.lua (25/11/2016)

assert(adam == nil)

function neatQA.initializeAdam()

  if adam == nil then
    print("Initializing Adam")
    adam = {}
    adam.__name = "Adam"
    adam.movingAverageOfGradients = paramdxRA:clone():zero()
    adam.movingAverageOfSquareGradients = paramdxRA:clone():zero()
    adam.temp = paramdxRA:clone():zero()
--    adam.square_gradient = paramdxRA:clone():zero()
    paramdxRA:zero()
    adam.adam_step = 0
    print(adam)
  end
end


function neatQA.doBackwardForAttention(attentionObjects) 
 local gradientsAttention = {}

if neatQA.USE_ATTENTION_NETWORK then
  if neatQA.USE_ADAM_FOR_REINFORCE then
    neatQA.initializeAdam()
  else 
    auxiliary.prepareMomentum(paramdxRA)
  end

  if params.lr_att > 0 and (true and train_attention_network) then
     -- compute reward
     local reward = torch.CudaTensor(params.batch_size,1):zero()
     if neatQA.USE_GOLD_LIKELIHOODS_AS_BASELINE then
        assert(false)
        reward:add(neatQA.baselineTensor) -- this will make it negative
        --print("37412")
        --print(reward)
     end
     local gradientWRTAttentionScore

     -------------------
     -- Minimizing fixations
     -------------------
     if neatQA.ANALYTICAL_MINIMIZATION_OF_FIXATIONS then
        gradientWRTAttentionScore = torch.CudaTensor(params.batch_size,1):zero()

        for i=1,neatQA.maximalLengthOccurringInInput[1] do
          gradientWRTAttentionScore:add(attention_scores[i])
        end

        if neatQA.scaleAttentionWithLength then
          gradientWRTAttentionScore:div( neatQA.maximalLengthOccurringInInput[1] )
        end

        fileStatsFixations:write((neatQA.sessionCounter*params.batch_size)..'\t'..(gradientWRTAttentionScore:mean())..'\n')
        fileStatsFixations:flush()

        if neatQA.CUT_OFF_BELOW_BASELINE then
          gradientWRTAttentionScore:add(params.ATTENTION_VALUES_BASELINE)
          gradientWRTAttentionScore = neatQA.reluLayer:forward(gradientWRTAttentionScore)
          if true and torch.uniform() > 0.98 then
            print("3412b: KL divergence gradient")
            print(gradientWRTAttentionScore)
          end
        end

        gradientWRTAttentionScore:mul(params.TOTAL_ATTENTIONS_WEIGHT)

    else --minimize fixations via REINFORCE

       for i=1,neatQA.maximalLengthOccurringInInput[1] do
           reward:add(attention_decisions[i])
       end

       -- now scale with the length of the sequences
       if neatQA.scaleAttentionWithLength then
         reward:div( neatQA.maximalLengthOccurringInInput[1] )
       end

       fileStatsFixations:write((neatQA.sessionCounter*params.batch_size)..'\t'..(reward:mean())..'\n')
       fileStatsFixations:flush()



       if neatQA.CUT_OFF_BELOW_BASELINE then
         reward:add(params.ATTENTION_VALUES_BASELINE)
         reward = neatQA.reluLayer:forward(reward)
         if true and torch.uniform() > 0.98 then
           print("3412: REWARD")
           print(reward)
         end
       end
       reward:mul(params.TOTAL_ATTENTIONS_WEIGHT)
    end
    -------------------
    -- Done minimizing fixations
    -------------------






if neatQA.rewardBasedOnLogLikeLoss then
     reward:add(transfer_data(nll))
--     print("10711")
  --   print(reward)
     -- as of 21/11/2016, these numbers are positive
else
     local lossTensor = torch.FloatTensor(params.batch_size):zero()
     for l=1,params.batch_size do
       local answerID = qa.getFromAnswer(readChunks.corpus,l,1)
       local predictedScore,predictedAnswer = torch.max(actor_output[l],1)
       if answerID == predictedAnswer[1] then
         lossTensor[l] = -1
       end
     end
    reward:add(transfer_data(lossTensor))
end

if torch.uniform()>0.999 then
 print("----38714")
 print(reward)
end

fileStatsReward:write((neatQA.sessionCounter*params.batch_size)..'\t'..(reward:mean())..'\n')
fileStatsReward:flush()



-- as of 19/11/2016: higher reward is worse

     local rewardDifference = reward:clone():add(-rewardBaseline, ones) 

     local baselineScores
     if USE_BIDIR_BASELINE then
        bidir_baseline_gradparams:zero()
        local inputsWithActions = {}
        for i=1,  neatQA.maximalLengthOccurringInInput[1] do
            inputsWithActions[i] = joinTable[i]:forward({neatQA.inputTensors[i]:view(-1,1), attention_decisions[i]:view(-1,1)})
        end
        baselineScores = bidir_baseline:forward(inputsWithActions)
        for i=1, neatQA.maximalLengthOccurringInInput[1]  do
          local target
          if i == neatQA.maximalLengthOccurringInInput[1]  then
             target = reward:view(params.batch_size,1)
         else
             target = baselineScores[i+1]:view(params.batch_size,1)
          end

             if i == neatQA.maximalLengthOccurringInInput[1] and torch.uniform() > 0.93 then
               print("BIDIR "..i)
               print("40624 TARGET ")
               print(target)

               print("SCORES ")
               print(baselineScores[i])
             end
          bidirBaseline.criterion_gradients[i]:copy(baseline_criterion:backward(baselineScores[i]:view(-1,1), target))
          if i == params.seq_length then
             --print(baseline_criterion:backward(baselineScores[i]:view(-1,1), target))
          end
       end
       bidir_baseline:backward(inputsWithActions, auxiliary.shortenTable(bidirBaseline.criterion_gradients,  neatQA.maximalLengthOccurringInInput[1]))

       auxiliary.clipGradients(bidir_baseline_gradparams)
       bidir_baseline_params:add(-0.0008, bidir_baseline_gradparams) -- this is changed from the NEAT version, which first multiplied in place
     end

     -- this mean is not actually used, what is used is the bidirectional predictor
     rewardBaseline = 0.95 * rewardBaseline + 0.05 * (torch.sum(reward) * 1.0/params.batch_size)
     for i = neatQA.maximalLengthOccurringInInput[1], 1, -1 do
        rewardDifferenceForI = rewardDifference
        if USE_BIDIR_BASELINE then
            if i>1 then
              rewardDifferenceForI = baselineScores[i-1]:clone():add(-1,reward):mul(-1)
if true and torch.uniform() > 0.9 and i == 100 then
 print("4349  "..i.."   "..(torch.mean(torch.abs(rewardDifference) - torch.abs(rewardDifferenceForI))))
end
            end
        end

        local whatToMultiplyToTheFinalDerivative
        if (not neatQA.better_logsigmoid_gradients) then
           whatToMultiplyToTheFinalDerivative = torch.CudaTensor(params.batch_size)
        end
        local attentionEntropyFactor =  torch.CudaTensor(params.batch_size)
        for j=1,params.batch_size do
-- it is important to take the probabilities and not the scores, since it will all be fed into the probabilities port of the attention moduke
           if (neatQA.better_logsigmoid_gradients) then
              -- note that entropy is positive, so there is a double - here and the term should enter positively
              attentionEntropyFactor[j] = params.ENTROPY_PENALTY * (math.exp(attentionObjects.probabilities[i][j][1]) *(attentionObjects.probabilities[i][j][1] - math.log(1 - math.exp(attentionObjects.probabilities[i][j][1]))))
--              print("..11824")
  --            print(attentionEntropyFactor[j])
    --          print(math.exp(attentionObjects.probabilities[i][j][1]))
           else
              --assert(false)
              attentionEntropyFactor[j] =  params.ENTROPY_PENALTY * (math.log(attentionObjects.probabilities[i][j][1]) - math.log(1 - attentionObjects.probabilities[i][j][1]))
           end
--           print("161")
  --         print(params.ENTROPY_PENALTY)
    --       print(attentionEntropyFactor[j])
      --     print(attentionObjects.probabilities[i][j][1])
        --   print(attentionObjects.scores[i][j][1])
          -- print(attentionObjects.decisions[i][j][1])
           if (not neatQA.better_logsigmoid_gradients) then
              --assert(false)

              whatToMultiplyToTheFinalDerivative[j] = 1 / (attentionObjects.probabilities[i][j][1]) -- THIS is an important part of REINFORCE!
           end
        end

        local factorsForTheDerivatives =  rewardDifferenceForI:clone() -- positive is worse?!
       if (not neatQA.better_logsigmoid_gradients) then
        --      assert(false)

             factorsForTheDerivatives:cmul(whatToMultiplyToTheFinalDerivative)
       end
        factorsForTheDerivatives:add(attentionEntropyFactor)
       local attentionNetwork = attentionObjects.attentionNetworks[i]
       local originalInputTensor = attentionObjects.originalInputTensors[i]
       local forwardHidden = attentionObjects.forwardHidden[i-1]
       local forwardCell = attentionObjects.forwardCell[i-1]
       local attentionArguments = {originalInputTensor}
       if neatQA.CONDITION == "preview" then
        if neatQA.ATTENTION_DOES_Q_ATTENTION then
           table.insert(attentionArguments, neatQA.questionEmbeddings)
        elseif params.useBackwardQForAttention then
          assert(false)
          table.insert(attentionArguments,attentionObjects.questionBackward)
        else
          assert(false)
          table.insert(attentionArguments,attentionObjects.questionForward)
        end
        if neatQA.USE_INNOVATIVE_ATTENTION then
             table.insert(attentionArguments, attentionObjects.questionBackward)
        end
       end
       table.insert(attentionArguments,forwardHidden)
       table.insert(attentionArguments,forwardCell)
       local firstGradient -- the attention score
       local secondGradient = torch.CudaTensor() -- the decision
       local thirdGradient = factorsForTheDerivatives -- the log probability
       local fourthGradient = torch.CudaTensor()
       if neatQA.ANALYTICAL_MINIMIZATION_OF_FIXATIONS then
         firstGradient = gradientWRTAttentionScore
       else
         firstGradient = torch.CudaTensor()
       end

       local gradientFactors = {firstGradient, secondGradient, thirdGradient, fourthGradient}
       if false then
         table.insert(gradientFactors, torch.CudaTensor())
         table.insert(gradientFactors, torch.CudaTensor())
       end
       local gradientAttention = attentionNetworks[i]:backward(attentionArguments,gradientFactors)
       gradientsAttention[i] = {questionForward = gradientAttention[2], forwardHidden = gradientAttention[3], forwardCell = gradientAttention[4]}
     end
     assert(norm_dwRA == norm_dwRA)


if neatQA.APPLY_L2_REGULARIZATION_TO_ATT then
  ap, gap = attentionNetworks[1]:parameters()
  for i=1,#gap do
    if gap[i]:norm()>0 then -- exclude parameters that are not trained (e.g., embeddings)
      gap[i]:add(params.l2_regularization, ap[i])
    end
  end
end


if false then
      auxiliary.normalizeGradients(paramdxRA)
else
      auxiliary.clipGradients(paramdxRA)
end

if true then
      print("23615")
      print(paramdxRA:norm())
end


if true and torch.uniform() > 0.6 then
  ap, gap = attentionNetworks[1]:parameters()
  print("--24113")
  print(ap)
  print(gap)
  for i=1,#gap do
    print(i.."  "..gap[i]:norm().."  "..ap[i]:norm())
  end
end


if neatQA.USE_ADAM_FOR_REINFORCE then
    adam.adam_step = adam.adam_step + 1
    local beta1 = 0.9
    local beta2 = 0.999
    local epsilon = 0.00000001
    local alpha = params.lr_att
--    print("...")
--    print(paramdxRA[paramdxRA:storage():size()-1])
    adam.movingAverageOfGradients:mul(beta1):add(1-beta1,paramdxRA)
--    print(adam.movingAverageOfGradients[paramdxRA:storage():size()-1])

    paramdxRA:pow(2)
--    print(adam.square_gradient[paramdxRA:storage():size()-1])

    adam.movingAverageOfSquareGradients:mul(beta2):add(1-beta2,paramdxRA)
--    print(adam.movingAverageOfSquareGradients[paramdxRA:storage():size()-1])

    torch.sqrt(adam.temp, adam.movingAverageOfSquareGradients)

--    print(adam.temp[paramdxRA:storage():size()-1])

    adam.temp:add(epsilon)
    paramdxRA:zero()
    paramxRA:addcdiv(- math.sqrt(1-math.pow(beta2,adam.adam_step)) * alpha /(1-math.pow(beta1,adam.adam_step)), adam.movingAverageOfGradients, adam.temp)
--    print(paramxRA[paramdxRA:storage():size()-1])

else 
      auxiliary.updateParametersWithMomentum(paramxRA,paramdxRA,params.lr_att)
end

  end
end  

return gradientsAttention
  
end






