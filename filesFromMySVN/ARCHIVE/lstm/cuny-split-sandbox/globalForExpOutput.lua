assert(globalForExpOutput == nil)

globalForExpOutput = {}

globalForExpOutput.accuracy = 0.0

globalForExpOutput.softAttentionsContainer = {}
globalForExpOutput.attRelativeToQ = {name = "ATT_RELATIVE_TO_Q"}
globalForExpOutput.fromInput = {name = "fromInput"}
globalForExpOutput.gateFromInput = {name = "gateFromInput"}
globalForExpOutput.dotproductAffine  = {name = "dotproductAffine"}
