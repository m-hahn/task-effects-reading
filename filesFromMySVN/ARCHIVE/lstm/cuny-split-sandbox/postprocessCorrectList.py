with open("/jagupard11/scr1/mhahn/deepmind-qa/cnn/training/noentities/correctlyAnswered-500-scores.txt") as inFile:
  correctList = inFile.read().replace("\n","").replace("[torch.FloatTensor of size 1]","\n").replace(".question ",".question\t").split("\n")
for item in correctList:
   item = item.split("\t")
   if len(item) == 0:
     continue
   if len(item[0]) == 0:
     assert(len(item) == 1)
     continue
   assert(len(item) == 2), str(item)
   item[1] = item[1].split("*")
   if len(item[1]) == 2:
      item[1] = str(float(item[1][0]) * float(item[1][1]))
   else:
      assert(len(item[1]) == 1), str(item)
      item[1] = str(float(item[1][0]))
   print("\t".join(item))
