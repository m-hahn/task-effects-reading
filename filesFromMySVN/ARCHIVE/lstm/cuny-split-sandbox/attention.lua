attention = {}

require('nn.BernoulliLayer')
require('nn.ReplicateDynamic')
require('nn.BernoulliLayerSigmoidWithLogProb')
require('nn.StoreInContainerLayer')

attention.ABLATE_INPUT = false
attention.ABLATE_STATE = false
attention.ABLATE_SURPRISAL = false


if string.match(params.ablation, 'i') then
  attention.ABLATE_INPUT = true
end
if string.match(params.ablation, 'r') then
  attention.ABLATE_STATE = true
end
if string.match(params.ablation, 's') then
  attention.ABLATE_SURPRISAL = true
end

--[[print(string.match(params.ablation, 's'))
print(string.match(params.ablation, 'r'))
print(string.match(params.ablation, 'i'))]]


print("ABLATION INP STATE SURP")
print(attention.ABLATE_INPUT)
print(attention.ABLATE_STATE)
print(attention.ABLATE_SURPRISAL)



function attention.createAttentionNewCombinationReduceCollinearity(param,gradparam)
assert(neatQA.CENTER_PREDICTORS)
 print("Doing biaffine with reduced collinearity at 3754")
 if neatQA.USE_INNOVATIVE_ATTENTION then
   print("Usng innovative attention!")
   return attention.createAttentionBilinearEXPERIMENTINGDirect(param,gradparam)
 end
assert(neatQA.NEW_COMBINATION)
   assert(neatQA.ATTENTION_DOES_Q_ATTENTION)

   assert(params.TASK == 'neat-qa')


   print("Doing training on attention embeddings?")
   print(neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS)

APPLY_TRAFO_ALSO_TO_Q = false
print("APPLY_TRAFO_ALSO_TO_Q   "..tostring(APPLY_TRAFO_ALSO_TO_Q))
assert(not(neatQA.STRETCH_Q_EMBEDDINGS) or not(APPLY_TRAFO_ALSO_TO_Q))
assert(not(APPLY_TRAFO_ALSO_TO_Q) or not (QUESTION_ATTENTION_BILINEAR)) 


   local x = nn.Identity()()
   local xemb
   if neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS then
     assert(false)
     xemb = nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x)
   else
     xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   end

   local y = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()

------------
print("At 628: Warning: Doing ablation")
local z2 = nn.MulConstant(0.0)(nn.Linear(params.rnn_size,1)(z))
local u2 = nn.MulConstant(0.0)(nn.Linear(params.rnn_size,1)(u))

-----------
xembOrig = xemb



local dimensionOfConvolution

local number_of_inputs = nil
assert(neatQA.CONDITION == "preview")
y0 = y

   questionTokensEmbeddings = y0
local inputReplicated

  inputReplicated = nn.ReplicateDynamic()({questionTokensEmbeddings,xembOrig})
  inputReplicated = nn.View(-1,params.embeddings_dimensionality)(inputReplicated)
  questionTokensEmbeddings = nn.View(-1,params.embeddings_dimensionality)(questionTokensEmbeddings)

QUESTION_ATTENTION_BILINEAR = false --true --false --true--true --false --true --false --true
print("Question Attention bilinear? ")
print(QUESTION_ATTENTION_BILINEAR)

   dimensionOfConvolution = 1
     questionTokensEmbeddings = nn.DotProduct()({(inputReplicated), (questionTokensEmbeddings)})
assert(dimensionOfConvolution == 1)
questionTokensEmbeddingsFlat = nn.View(params.batch_size,-1,dimensionOfConvolution)(questionTokensEmbeddings) -- batch * length * conv
   questionEmbeddingMax = nn.Max(2,3)(questionTokensEmbeddingsFlat)
   questionEmbeddingMax = nn.View(params.batch_size,dimensionOfConvolution)(questionEmbeddingMax) -- batch * conv
fromInput = nn.JoinTable(1,1)({xemb,(z2),(u2)})


DOING_VARIANT = 59
--DOING_SIGMOID_ON_GATE = true
print("VARIANT NUMBER "..DOING_VARIANT)
if DOING_VARIANT == 54 or DOING_VARIANT == 58 then --54, 58
  print("DOING 54 58 at 10521")
  fromInput = nn.Linear(30,1)(nn.Dropout(0.1)(nn.ReLU()(nn.Linear(102,30)(fromInput)))) -- attention score from the input
  gateFromInput = (nn.Linear(30,1)(nn.Dropout(0.1)(nn.ReLU()(nn.Linear(100,30)(xemb))))) -- gating score from the input
 if DOING_VARIANT == 54 then -- 54
   gateFromInput = nn.Sigmoid()(gateFromInput)
 elseif DOING_VARIANT == 58 then
   print("No sigmoid for gate.")
 else
   assert(false)
 end
else --55, 56, 57, 59
  assert(DOING_VARIANT == 55 or DOING_VARIANT == 59)
  print("DOING 55 56 57 at 10927")
  fromInput = nn.Linear(102,1)(fromInput) -- attention score from the input
  gateFromInput = (nn.Linear(100,1)(xemb)) -- gating score from the input
 if DOING_VARIANT == 55 then -- 55,56,57
   gateFromInput = nn.Sigmoid()(gateFromInput)
 else
  assert(DOING_VARIANT == 59)
   print("No sigmoid for gate.")
 end
end

if neatQA.STORE_Q_ATTENTION then
   questionEmbeddingMax = nn.StoreInContainerLayer("attRelativeToQ", true)(questionEmbeddingMax)
   fromInput = nn.StoreInContainerLayer("fromInput", true)(fromInput)
   gateFromInput = nn.StoreInContainerLayer("gateFromInput", true)(gateFromInput)
end

if false then
  dotproductAffine = nn.AddConstant(-0.66)(questionEmbeddingMax)
else
  print("Taking binary variable")
  dotproductAffine = nn.AddConstant(-0.166)(nn.Threshold(0.99,0.0)(questionEmbeddingMax))
--  dotproductAffine = nn.Threshold(0.99,0.0)(questionEmbeddingMax)
end
if neatQA.STORE_Q_ATTENTION then
   dotproductAffine = nn.StoreInContainerLayer("dotproductAffine", true)(dotproductAffine)
end



gatedDotProduct = nn.CMulTable()({gateFromInput, dotproductAffine})



--fromInput = nn.PrintLayer("3",1.0,true)(fromInput)

assert(dimensionOfConvolution == 1)
print(dimensionOfConvolution)


local bilinear = nn.CAddTable()({gatedDotProduct, fromInput})



local attentionBias = 0.0
if neatQA.CONDITION == "nopreview" then
 attentionBias = 2.0
end
print("attention bias"..attentionBias)
bilinear = nn.AddConstant(attentionBias)(bilinear)


local decisionsAndProbs
local attention
if neatQA.better_logsigmoid_gradients then
   decisionsAndProbs = nn.BernoulliLayerSigmoidWithLogProb(params.batch_size,1)(bilinear)
   attention = nn.SelectTable(3)(decisionsAndProbs)
else
   attention = nn.Sigmoid()(bilinear)
   decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
end

   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
--probs = nn.PrintLayer("probs",1.0,true)(probs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention
if neatQA.ANALYTICAL_MINIMIZATION_OF_FIXATIONS then
  blockedAttention = attention
else
  blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
end
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


if false then
 local blockedQ = nn.BlockGradientLayer(params.batch_size,QUESTION_LENGTH)(questionTokensEmbeddingsFlat)
 local blockedQMax = nn.BlockGradientLayer(params.batch_size,1)(questionEmbeddingMax)
end

   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, y)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})


  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
    parameters[i]:uniform(-epsilon, epsilon)
    gradparameters[i]:zero()
    if parameters[i]:storage():size() == 1 then
      print("Note: singleton parameters initialized to zero")
      parameters[i]:zero()
    end
    print(i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("   "..parameters[i][1]:norm())
    end
    if false and (i==4) then
      assert(DOING_DEBUGGING)
      parameters[i]:zero()
    end

  end
  if false and #param == 1 then
    param = {"PLACEHOLDER","PLACEHOLDER",param[1]}
    gradparam = {"PLACEHOLDER","PLACEHOLDER",gradparam[1]}
  end


  if #param ~= #parameters then
    print("WARNING: attention parameters have different numbers")
    print("Loaded:")
    print(param)
    print("Own:")
    print(parameters)
    print("===")
    if (#param == 10) then
      for j=4,9 do
         param[j] = param[j+1]
         gradparam[j] = gradparam[j+1]
      end
      param[10] = nil
      gradparam[10] = nil
      print("Have removed constant offset by removing element 4")
      print("Loaded:")
      print(param)
    end
  end
  for i=1,#param do
    if param[i] ~= "PLACEHOLDER" then
      parameters[i]:copy(param[i])
      if false then
        print("Not copying gradparam at 17342")
        gradparameters[i]:copy(gradparam[i]):zero()
      end
if false then
   print("WARNING: Setting singleton parameters of loaded model to zero!")
    if parameters[i]:storage():size() == 1 then
        parameters[i]:zero()
    end
    print(parameters[i]:storage():size().."  "..parameters[i]:norm())
end
    end
  end

  for i=1, #parameters do
    print("# "..i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("#   "..parameters[i][1]:norm())
    end
  end


  return transfer_data(module)
end











function attention.createAttentionNewCombination(param,gradparam)
if neatQA.CENTER_PREDICTORS then
  return attention.createAttentionNewCombinationReduceCollinearity(param,gradparam)
end
assert(not(neatQA.CENTER_PREDICTORS))
 print("Doing biaffine at 3627")
 if neatQA.USE_INNOVATIVE_ATTENTION then
   print("Usng innovative attention!")
   return attention.createAttentionBilinearEXPERIMENTINGDirect(param,gradparam)
 end
assert(neatQA.NEW_COMBINATION)
   assert(neatQA.ATTENTION_DOES_Q_ATTENTION)

   assert(params.TASK == 'neat-qa')


   print("Doing training on attention embeddings?")
   print(neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS)

APPLY_TRAFO_ALSO_TO_Q = false
print("APPLY_TRAFO_ALSO_TO_Q   "..tostring(APPLY_TRAFO_ALSO_TO_Q))
assert(not(neatQA.STRETCH_Q_EMBEDDINGS) or not(APPLY_TRAFO_ALSO_TO_Q))
assert(not(APPLY_TRAFO_ALSO_TO_Q) or not (QUESTION_ATTENTION_BILINEAR)) 


   local x = nn.Identity()()
   local xemb
   if neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS then
     assert(false)
     xemb = nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x)
   else
     xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   end

   local y = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()

------------
print("At 628: Warning: Doing ablation")
local z2 = nn.MulConstant(0.0)(nn.Linear(params.rnn_size,1)(z))
local u2 = nn.MulConstant(0.0)(nn.Linear(params.rnn_size,1)(u))

-----------
xembOrig = xemb



local dimensionOfConvolution

local number_of_inputs = nil
assert(neatQA.CONDITION == "preview")
y0 = y

   questionTokensEmbeddings = y0
local inputReplicated

  inputReplicated = nn.ReplicateDynamic()({questionTokensEmbeddings,xembOrig})
  inputReplicated = nn.View(-1,params.embeddings_dimensionality)(inputReplicated)
  questionTokensEmbeddings = nn.View(-1,params.embeddings_dimensionality)(questionTokensEmbeddings)

QUESTION_ATTENTION_BILINEAR = false --true --false --true--true --false --true --false --true
print("Question Attention bilinear? ")
print(QUESTION_ATTENTION_BILINEAR)

   dimensionOfConvolution = 1
     questionTokensEmbeddings = nn.DotProduct()({(inputReplicated), (questionTokensEmbeddings)})
assert(dimensionOfConvolution == 1)
questionTokensEmbeddingsFlat = nn.View(params.batch_size,-1,dimensionOfConvolution)(questionTokensEmbeddings) -- batch * length * conv
   questionEmbeddingMax = nn.Max(2,3)(questionTokensEmbeddingsFlat)
   questionEmbeddingMax = nn.View(params.batch_size,dimensionOfConvolution)(questionEmbeddingMax) -- batch * conv
fromInput = nn.JoinTable(1,1)({xemb,(z2),(u2)})


DOING_VARIANT = 55
--DOING_SIGMOID_ON_GATE = true

if DOING_VARIANT == 54 or DOING_VARIANT == 58 then --54, 58
  print("DOING 54 58 at 10521")
  fromInput = nn.Linear(30,1)(nn.Dropout(0.1)(nn.ReLU()(nn.Linear(102,30)(fromInput)))) -- attention score from the input
  gateFromInput = (nn.Linear(30,1)(nn.Dropout(0.1)(nn.ReLU()(nn.Linear(100,30)(xemb))))) -- gating score from the input
 if DOING_VARIANT == 54 then -- 54
   gateFromInput = nn.Sigmoid()(gateFromInput)
 elseif DOING_VARIANT == 58 then
   print("No sigmoid for gate.")
 else
   assert(false)
 end
else --55, 56, 57
  assert(DOING_VARIANT == 55)
  print("DOING 55 56 57 at 10927")
  fromInput = nn.Linear(102,1)(fromInput) -- attention score from the input
  gateFromInput = (nn.Linear(100,1)(xemb)) -- gating score from the input
 if DOING_VARIANT == 55 then -- 55,56,57
   gateFromInput = nn.Sigmoid()(gateFromInput)
 else
  assert(false)
   print("No sigmoid for gate.")
 end
end

if neatQA.STORE_Q_ATTENTION then
   questionEmbeddingMax = nn.StoreInContainerLayer("attRelativeToQ", true)(questionEmbeddingMax)
   fromInput = nn.StoreInContainerLayer("fromInput", true)(fromInput)
   gateFromInput = nn.StoreInContainerLayer("gateFromInput", true)(gateFromInput)
end


dotproductAffine = nn.Add(1)(questionEmbeddingMax)

if neatQA.STORE_Q_ATTENTION then
   dotproductAffine = nn.StoreInContainerLayer("dotproductAffine", true)(dotproductAffine)
end



gatedDotProduct = nn.CMulTable()({gateFromInput, dotproductAffine})



--fromInput = nn.PrintLayer("3",1.0,true)(fromInput)

assert(dimensionOfConvolution == 1)
print(dimensionOfConvolution)


local bilinear = nn.CAddTable()({gatedDotProduct, fromInput})



local attentionBias = 0.0
if neatQA.CONDITION == "nopreview" then
 attentionBias = 2.0
end
print("attention bias"..attentionBias)
bilinear = nn.AddConstant(attentionBias)(bilinear)


local decisionsAndProbs
local attention
if neatQA.better_logsigmoid_gradients then
   decisionsAndProbs = nn.BernoulliLayerSigmoidWithLogProb(params.batch_size,1)(bilinear)
   attention = nn.SelectTable(3)(decisionsAndProbs)
else
   attention = nn.Sigmoid()(bilinear)
   decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
end

   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
--probs = nn.PrintLayer("probs",1.0,true)(probs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention
if neatQA.ANALYTICAL_MINIMIZATION_OF_FIXATIONS then
  blockedAttention = attention
else
  blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
end
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


if false then
 local blockedQ = nn.BlockGradientLayer(params.batch_size,QUESTION_LENGTH)(questionTokensEmbeddingsFlat)
 local blockedQMax = nn.BlockGradientLayer(params.batch_size,1)(questionEmbeddingMax)
end

   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, y)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})


  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
    parameters[i]:uniform(-epsilon, epsilon)
    gradparameters[i]:zero()
    if parameters[i]:storage():size() == 1 then
      print("Note: singleton parameters initialized to zero")
      parameters[i]:zero()
    end
    print(i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("   "..parameters[i][1]:norm())
    end
    if false and (i==4) then
      assert(DOING_DEBUGGING)
      parameters[i]:zero()
    end

  end
  if false and #param == 1 then
    param = {"PLACEHOLDER","PLACEHOLDER",param[1]}
    gradparam = {"PLACEHOLDER","PLACEHOLDER",gradparam[1]}
  end

  if #param ~= #parameters then
    print("WARNING: attention parameters have different numbers")
    print("Loaded:")
    print(param)
    print("Own:")
    print(parameters)
    print("===")
  end
  for i=1,#param do
    if param[i] ~= "PLACEHOLDER" then
      parameters[i]:copy(param[i])
      if false then
        print("Not copying gradparam at 17342")
        gradparameters[i]:copy(gradparam[i]):zero()
      end
if false then
   print("WARNING: Setting singleton parameters of loaded model to zero!")
    if parameters[i]:storage():size() == 1 then
        parameters[i]:zero()
    end
    print(parameters[i]:storage():size().."  "..parameters[i]:norm())
end
    end
  end

  for i=1, #parameters do
    print("# "..i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("#   "..parameters[i][1]:norm())
    end
  end


  return transfer_data(module)
end
























function attention.createAttentionBiaffine(param,gradparam)
 print("Doing biaffine at 3627")
 if neatQA.USE_INNOVATIVE_ATTENTION then
   print("Usng innovative attention!")
   return attention.createAttentionBilinearEXPERIMENTINGDirect(param,gradparam)
 end
assert(neatQA.BIAFFINE_ATTENTION)
   assert(neatQA.ATTENTION_DOES_Q_ATTENTION)

   assert(params.TASK == 'neat-qa')


   print("Doing training on attention embeddings?")
   print(neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS)

APPLY_TRAFO_ALSO_TO_Q = false
print("APPLY_TRAFO_ALSO_TO_Q   "..tostring(APPLY_TRAFO_ALSO_TO_Q))
assert(not(neatQA.STRETCH_Q_EMBEDDINGS) or not(APPLY_TRAFO_ALSO_TO_Q))
assert(not(APPLY_TRAFO_ALSO_TO_Q) or not (QUESTION_ATTENTION_BILINEAR)) 


   local x = nn.Identity()()
   local xemb
   if neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS then
     assert(false)
     xemb = nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x)
   else
     xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   end

   local y = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()

------------
print("At 628: Warning: Doing ablation")
local z2 = nn.MulConstant(0.0)(nn.Linear(params.rnn_size,1)(z))
local u2 = nn.MulConstant(0.0)(nn.Linear(params.rnn_size,1)(u))

-----------
xembOrig = xemb
--xemb = nn.PrintLayer("1",1.0,true)(xemb)
xemb = nn.ELU(0.1)(nn.Linear(params.embeddings_dimensionality,30)(nn.Dropout(0.2)(xemb)))
--xemb = nn.PrintLayer("1",1.0,true)(xemb)



local dimensionOfConvolution

local number_of_inputs = nil
assert(neatQA.CONDITION == "preview")
y0 = y

   questionTokensEmbeddings = y0
local inputReplicated
if APPLY_TRAFO_ALSO_TO_Q then
  inputReplicated = nn.ReplicateDynamic()({questionTokensEmbeddings,xemb})
  inputReplicated = nn.View(-1,30)(inputReplicated)

  questionTokensEmbeddings = nn.View(-1,params.embeddings_dimensionality)(questionTokensEmbeddings)
   questionTokensEmbeddings = nn.ELU(0.1)(nn.Linear(params.embeddings_dimensionality,30)(questionTokensEmbeddings))
else
  inputReplicated = nn.ReplicateDynamic()({questionTokensEmbeddings,xembOrig})
  inputReplicated = nn.View(-1,params.embeddings_dimensionality)(inputReplicated)
  questionTokensEmbeddings = nn.View(-1,params.embeddings_dimensionality)(questionTokensEmbeddings)
end

QUESTION_ATTENTION_BILINEAR = false --true --false --true--true --false --true --false --true
print("Question Attention bilinear? ")
print(QUESTION_ATTENTION_BILINEAR)

if QUESTION_ATTENTION_BILINEAR then
assert(false)
else
   dimensionOfConvolution = 1
   if APPLY_TRAFO_ALSO_TO_Q then
     questionTokensEmbeddings = nn.DotProduct()({nn.Dropout(0.35)(inputReplicated), nn.Dropout(0.35)(questionTokensEmbeddings)})
   else
     questionTokensEmbeddings = nn.DotProduct()({(inputReplicated), (questionTokensEmbeddings)})
   end
end
assert(dimensionOfConvolution == 1)

questionTokensEmbeddingsFlat = nn.View(params.batch_size,-1,dimensionOfConvolution)(questionTokensEmbeddings) -- batch * length * conv
   questionEmbeddingMax = nn.Max(2,3)(questionTokensEmbeddingsFlat)
   questionEmbeddingMax = nn.View(params.batch_size,dimensionOfConvolution)(questionEmbeddingMax) -- batch * conv

fromInput = nn.JoinTable(1,1)({xemb,(z2),(u2)})
--fromInput = nn.PrintLayer("3",1.0,true)(fromInput)

fromInput = nn.Linear(32,1)(fromInput)
--fromInput = nn.PrintLayer("3",1.0,true)(fromInput)

if neatQA.STORE_Q_ATTENTION then
   questionEmbeddingMax = nn.StoreInContainerLayer("attRelativeToQ", true)(questionEmbeddingMax)
   fromInput = nn.StoreInContainerLayer("fromInput", true)(fromInput)
end

assert(dimensionOfConvolution == 1)
print(dimensionOfConvolution)


local bilinear = nn.CAddTable()({questionEmbeddingMax, fromInput})



local attentionBias = 1.0
if neatQA.CONDITION == "nopreview" then
 attentionBias = 2.0
end
print("attention bias"..attentionBias)
bilinear = nn.AddConstant(attentionBias)(bilinear)


local decisionsAndProbs
local attention
if neatQA.better_logsigmoid_gradients then
   decisionsAndProbs = nn.BernoulliLayerSigmoidWithLogProb(params.batch_size,1)(bilinear)
   attention = nn.SelectTable(3)(decisionsAndProbs)
else
   attention = nn.Sigmoid()(bilinear)
   decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
end

   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
--probs = nn.PrintLayer("probs",1.0,true)(probs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


if false then
 local blockedQ = nn.BlockGradientLayer(params.batch_size,QUESTION_LENGTH)(questionTokensEmbeddingsFlat)
 local blockedQMax = nn.BlockGradientLayer(params.batch_size,1)(questionEmbeddingMax)
end

   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, y)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})


  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
    parameters[i]:uniform(-epsilon, epsilon)
    gradparameters[i]:zero()
    print(i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("   "..parameters[i][1]:norm())
    end
    if false and (i==4) then
      assert(DOING_DEBUGGING)
      parameters[i]:zero()
    end

  end
  if false and #param == 1 then
    param = {"PLACEHOLDER","PLACEHOLDER",param[1]}
    gradparam = {"PLACEHOLDER","PLACEHOLDER",gradparam[1]}
  end

  if #param ~= #parameters then
    print("WARNING: attention parameters have different numbers")
    print(param)
    print(parameters)
    print("===")
  end
  for i=1,#param do
    if param[i] ~= "PLACEHOLDER" then
      parameters[i]:copy(param[i])
      if false then
        print("Not copying gradparam at 17342")
        gradparameters[i]:copy(gradparam[i]):zero()
      end
    end
  end

  for i=1, #parameters do
    print("# "..i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("#   "..parameters[i][1]:norm())
    end
  end


  return transfer_data(module)
end

---------------------------

---------------------------

---------------------------

---------------------------

function attention.createAttentionBilinearSimple(param,gradparam)
 print("23911 simple bilinear (55)")
 print("Doing simple version only depending on the word")
 if neatQA.USE_INNOVATIVE_ATTENTION then
   print("Usng innovative attention!")
   return attention.createAttentionBilinearEXPERIMENTINGDirect(param,gradparam)
 end
   assert(neatQA.ATTENTION_DOES_Q_ATTENTION)

   assert(params.TASK == 'neat-qa')

   local x = nn.Identity()()

   print("Doing training on attention embeddings?")
   print(neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS)

   local xemb
   if neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS then
     assert(false)
     xemb = nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x)
   else
     xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   end

   local y = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()
   xemb = nn.Dropout(0.1)(xemb)
assert(not(neatQA.APPLY_MLP_TO_ATTENTION_EMBEDDING))

------------
print("At 628: Warning: Doing ablation")
local z2 = nn.MulConstant(0.0)(nn.Linear(params.rnn_size,1)(z))
local u2 = nn.MulConstant(0.0)(nn.Linear(params.rnn_size,1)(u))

-----------
local dimensionOfConvolution

local number_of_inputs = nil
assert(neatQA.CONDITION == "preview")
   y0 = y
   questionTokensEmbeddings = y0
local inputReplicated = nn.ReplicateDynamic()({questionTokensEmbeddings,xemb})
inputReplicated = nn.View(-1,params.embeddings_dimensionality)(inputReplicated)
questionTokensEmbeddings = nn.View(-1,params.embeddings_dimensionality)(questionTokensEmbeddings)

-- maybe some kind of gating or attention instead of Bilinear would be better

QUESTION_ATTENTION_BILINEAR = true --true --false --true --false --true--true --false --true --false --true
print("Question Attention bilinear = true ")
assert(QUESTION_ATTENTION_BILINEAR)

   dimensionOfConvolution = 1
assert(dimensionOfConvolution == 1)

   questionTokensEmbeddings = nn.Bilinear(params.embeddings_dimensionality, params.embeddings_dimensionality, dimensionOfConvolution)({questionTokensEmbeddings, inputReplicated})

questionTokensEmbeddingsFlat = nn.View(params.batch_size,-1,dimensionOfConvolution)(questionTokensEmbeddings) -- batch * length * conv
print("MAX OR SUM OR MEAN?")
print("Sum")
--   questionEmbeddingMax = nn.Max(2,3)(questionTokensEmbeddingsFlat)
   questionEmbeddingMax = nn.Sum(2,3,false)(questionTokensEmbeddingsFlat)
local attentionBias = -1.0 --1.0 -- 0.0

print("attention bias"..attentionBias)



   questionEmbeddingMax = nn.View(params.batch_size,dimensionOfConvolution)(questionEmbeddingMax) -- batch * conv

if neatQA.STORE_Q_ATTENTION then
   questionEmbeddingMax = nn.StoreInContainerLayer("attRelativeToQ", true)(questionEmbeddingMax)
end

assert(dimensionOfConvolution == 1)
print(dimensionOfConvolution)



   y2 = nn.JoinTable(1,1)({xemb,nn.Dropout(0.4)(z2),nn.Dropout(0.4)(u2)})
number_of_inputs = 2 + params.embeddings_dimensionality

print(dimensionOfConvolution)
assert(dimensionOfConvolution == 1)

   
 print("WARNING: only using the input-question attention")
local bilinear = nn.Linear(number_of_inputs,1)(y2)
 bilinear = nn.MulConstant(0.0)(bilinear)
-- batch * 1
assert(dimensionOfConvolution == 1)

  bilinear = nn.CAddTable()({bilinear,questionEmbeddingMax})


bilinear = nn.AddConstant(attentionBias)(bilinear)


local decisionsAndProbs
local attention
if neatQA.better_logsigmoid_gradients then
   decisionsAndProbs = nn.BernoulliLayerSigmoidWithLogProb(params.batch_size,1)(bilinear)
   attention = nn.SelectTable(3)(decisionsAndProbs)
else
   attention = nn.Sigmoid()(bilinear)
   decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
end

   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
--probs = nn.PrintLayer("probs",1.0,true)(probs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


if false then
 local blockedQ = nn.BlockGradientLayer(params.batch_size,QUESTION_LENGTH)(questionTokensEmbeddingsFlat)
 local blockedQMax = nn.BlockGradientLayer(params.batch_size,1)(questionEmbeddingMax)
end

   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, y)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})
--  module:getParameters():uniform(-params.init_weight, params.init_weight)



  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
    parameters[i]:uniform(-epsilon, epsilon)
    gradparameters[i]:zero()
    print(i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("   "..parameters[i][1]:norm())
    end
    if false and (i==4) then
      assert(DOING_DEBUGGING)
      parameters[i]:zero()
    end
    if false and (i==8) then
      print("INITIALIZING TO DIAGONAL MATRIX!")
      parameters[i]:copy(torch.eye(params.embeddings_dimensionality))
    end
    if false and (i==9) then
      print("INITIALIZING BIAS!")
      parameters[i]:add(0.7)
    end
  end
  if #param ~= #parameters then
    print("WARNING: attention parameters have different numbers")
    print(param)
    print(parameters)
    print("===")
  end
  for i=1,#param do
    if param[i] ~= "PLACEHOLDER" then
      parameters[i]:copy(param[i])
      if false then
        print("Not copying gradparam at 17342")
        gradparameters[i]:copy(gradparam[i]):zero()
      end
    end
  end

  for i=1, #parameters do
    print("# "..i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("#   "..parameters[i][1]:norm())
    end
  end


  return transfer_data(module)
end




-----------------------------------------------
-----------------------------------------------
-----------------------------------------------







-----------------------------------------------
-----------------------------------------------
-----------------------------------------------

-- the 51, 53 version
function attention.createAttentionBilinearSimpleInUse(param,gradparam)
 print("4658")
 print("Doing simple version only depending on the word")
 if neatQA.USE_INNOVATIVE_ATTENTION then
   print("Usng innovative attention!")
   return attention.createAttentionBilinearEXPERIMENTINGDirect(param,gradparam)
 end
   assert(neatQA.ATTENTION_DOES_Q_ATTENTION)

   assert(params.TASK == 'neat-qa')

   local x = nn.Identity()()
   print("Doing training on attention embeddings?")

   neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS = false --true
   print(neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS)
   local xemb
   if neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS then
     xemb = nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x)
   else
     xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   end

   local y = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()
   xemb = nn.Dropout(0.1)(xemb)

------------
print("At 628: Warning: Doing ablation")
local z2 = nn.MulConstant(0.0)(nn.Linear(params.rnn_size,1)(z))
local u2 = nn.MulConstant(0.0)(nn.Linear(params.rnn_size,1)(u))

-----------

local number_of_inputs = nil
if neatQA.CONDITION == "preview" then
   y0 = y
   questionTokensEmbeddings = y0
local inputReplicated = nn.ReplicateDynamic()({questionTokensEmbeddings,xemb})
inputReplicated = nn.View(-1,params.embeddings_dimensionality)(inputReplicated)
questionTokensEmbeddings = nn.View(-1,params.embeddings_dimensionality)(questionTokensEmbeddings)

-- maybe some kind of gating or attention instead of Bilinear would be better

QUESTION_ATTENTION_BILINEAR = false --true --false --true --false --true--true --false --true --false --true
print("Question Attention bilinear? ")
print(QUESTION_ATTENTION_BILINEAR)
local dimensionOfConvolution
if QUESTION_ATTENTION_BILINEAR then
   dimensionOfConvolution = 1
   questionTokensEmbeddings = nn.Bilinear(params.embeddings_dimensionality, params.embeddings_dimensionality, dimensionOfConvolution)({questionTokensEmbeddings, inputReplicated})
else
   dimensionOfConvolution = 1
   questionTokensEmbeddings = nn.DotProduct()({inputReplicated, questionTokensEmbeddings})
end


questionTokensEmbeddingsFlat = nn.View(params.batch_size,-1,dimensionOfConvolution)(questionTokensEmbeddings) -- batch * length * conv
   questionEmbeddingMax = nn.Max(2,3)(questionTokensEmbeddingsFlat)
   questionEmbeddingMax = nn.View(params.batch_size,dimensionOfConvolution)(questionEmbeddingMax) -- batch * conv


if neatQA.STORE_Q_ATTENTION then
   questionEmbeddingMax = nn.StoreInContainerLayer("attRelativeToQ", true)(questionEmbeddingMax)
end


if false then
   print("WARNING ablating question!")
   questionEmbeddingMax = nn.MulConstant(0.0)(questionEmbeddingMax)
end
   y2 = nn.JoinTable(1,1)({xemb,questionEmbeddingMax,nn.Dropout(0.4)(z2),nn.Dropout(0.4)(u2)})
number_of_inputs = 2 + dimensionOfConvolution + params.embeddings_dimensionality
else
   assert(false)
   y2 = nn.JoinTable(1,1)({nn.Dropout(0.4)(z2),nn.Dropout(0.4)(u2)})
number_of_inputs = 2*params.rnn_size + 1
number_of_inputs = 2
end
   local bilinear = nn.Linear(number_of_inputs,1)(y2)
local attentionBias = 2.0
if neatQA.CONDITION == "nopreview" then
 attentionBias = 2.0
end
print("attention bias"..attentionBias)
bilinear = nn.AddConstant(attentionBias)(bilinear)


local decisionsAndProbs
local attention
if neatQA.better_logsigmoid_gradients then
   decisionsAndProbs = nn.BernoulliLayerSigmoidWithLogProb(params.batch_size,1)(bilinear)
   attention = nn.SelectTable(3)(decisionsAndProbs)
else
   attention = nn.Sigmoid()(bilinear)
   decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
end

   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
--probs = nn.PrintLayer("probs",1.0,true)(probs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


if false then
 local blockedQ = nn.BlockGradientLayer(params.batch_size,QUESTION_LENGTH)(questionTokensEmbeddingsFlat)
 local blockedQMax = nn.BlockGradientLayer(params.batch_size,1)(questionEmbeddingMax)
end

   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, y)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})
--  module:getParameters():uniform(-params.init_weight, params.init_weight)



  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
    parameters[i]:uniform(-epsilon, epsilon)
    gradparameters[i]:zero()
    print(i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("   "..parameters[i][1]:norm())
    end
    if false and (i==4) then
      assert(DOING_DEBUGGING)
      parameters[i]:zero()
    end
  end
  if #param ~= #parameters then
    print("WARNING: attention parameters have different numbers")
    print(param)
    print(parameters)
    print("===")
  end
  for i=1,#param do
    if param[i] ~= "PLACEHOLDER" then
      parameters[i]:copy(param[i])
      if false then
        print("Not copying gradparam at 17342")
        gradparameters[i]:copy(gradparam[i]):zero()
      end
    end
  end

  for i=1, #parameters do
    print("# "..i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("#   "..parameters[i][1]:norm())
    end
  end


  return transfer_data(module)
end

--------------------------------------------------

--------------------------------------------------

--------------------------------------------------

--------------------------------------------------

--------------------------------------------------

--------------------------------------------------

--------------------------------------------------







function attention.createAttentionBilinear(param,gradparam)
if neatQA.NEW_COMBINATION then
  return attention.createAttentionNewCombination(param,gradparam)
elseif neatQA.BIAFFINE_ATTENTION then
  return attention.createAttentionBiaffine(param,gradparam)
elseif neatQA.USE_BILINEAR_TERM_IN_HARD_ATTENTION then
  return attention.createAttentionBilinearSimple(param,gradparam)
else 
  return attention.createAttentionBilinearSimpleInUse(param,gradparam)
end

 if neatQA.USE_INNOVATIVE_ATTENTION then
   print("Usng innovative attention!")
   return attention.createAttentionBilinearEXPERIMENTINGDirect(param,gradparam)
 end
   assert(neatQA.ATTENTION_DOES_Q_ATTENTION)

   assert(params.TASK == 'neat-qa')

   local x = nn.Identity()()
   print("Doing training on attention embeddings?")
   neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS = false
   print(neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS)
   local xemb
   if neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS then
     xemb = nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x)
   else
     xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   end

   local y = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()
   xemb = nn.Dropout(0.1)(xemb)

------------
--print("At 628: Warning: Doing ablation")
--local z2 = nn.MulConstant(0.0)(z)
--local u2 = nn.MulConstant(0.0)(u)
-----------

local number_of_inputs = nil
if neatQA.CONDITION == "preview" then
   y0 = y
   -- y0 consists of the one-hot vectors of the questions

  -- questionTokensReshaped =  nn.JoinTable(1)(y0)
  -- questionTokensEmbeddings = nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(questionTokensReshaped)
--   questionTokensEmbeddings = nn.View(-1,params.batch_size,params.embeddings_dimensionality)(questionTokensEmbeddings)

   questionTokensEmbeddings = y0

   local inputReplicated = nn.ReplicateDynamic()({questionTokensEmbeddings,xemb})

--   inputReplicated = nn.PrintLayer("",1.0,false)(inputReplicated)
  -- questionTokensEmbeddings = nn.PrintLayer("",1.0,false)(questionTokensEmbeddings)

inputReplicated = nn.View(-1,params.embeddings_dimensionality)(inputReplicated)
questionTokensEmbeddings = nn.View(-1,params.embeddings_dimensionality)(questionTokensEmbeddings)

QUESTION_ATTENTION_BILINEAR = true --false --true --false --true--true --false --true --false --true
print("Question Attention bilinear? ")
print(QUESTION_ATTENTION_BILINEAR)
local dimensionOfConvolution
if QUESTION_ATTENTION_BILINEAR then
   dimensionOfConvolution = 10
   questionTokensEmbeddings = nn.Bilinear(params.embeddings_dimensionality, params.embeddings_dimensionality,dimensionOfConvolution)({inputReplicated, questionTokensEmbeddings})
else
   dimensionOfConvolution = 1
   questionTokensEmbeddings = nn.DotProduct()({inputReplicated, questionTokensEmbeddings})
   -- batch * length * conv
end
--questionTokensEmbeddings = nn.PrintLayer("questionTokensEmbeddings",1.0,false)(questionTokensEmbeddings)


questionTokensEmbeddingsFlat = nn.View(params.batch_size,-1,dimensionOfConvolution)(questionTokensEmbeddings) -- batch * length * conv
--questionTokensEmbeddingsFlat = nn.PrintLayer("questionTokensEmbeddingsFlat",1.0,true)(questionTokensEmbeddingsFlat)

   questionEmbeddingMax = nn.Max(2,3)(questionTokensEmbeddingsFlat)
  -- questionEmbeddingMax = nn.PrintLayer("questionEmbeddingMax",1.0,false)(questionEmbeddingMax)
   questionEmbeddingMax = nn.View(params.batch_size,dimensionOfConvolution)(questionEmbeddingMax) -- batch * conv
   --questionEmbeddingMax = nn.PrintLayer("questionEmbeddingMax",1.0,false)(questionEmbeddingMax)
--   y0 = nn.PrintLayer("y",1.0,true)(y0)
   y2 = nn.JoinTable(1,1)({questionEmbeddingMax,nn.Dropout(0.4)(z2),nn.Dropout(0.4)(u2)})
number_of_inputs = 2*params.rnn_size + dimensionOfConvolution
else
   assert(false)
   y2 = nn.JoinTable(1,1)({nn.Dropout(0.4)(z2),nn.Dropout(0.4)(u2)})
number_of_inputs = 2*params.rnn_size + 1
number_of_inputs = 2
end

--   y2 = nn.PrintLayer("",1.0,true)(y2)

   local intermediateMLP = nn.Linear(number_of_inputs,100)(y2)
if true then
   intermediateMLP = nn.Tanh()(intermediateMLP)
else
   intermediateMLP = nn.ReLU()(intermediateMLP)
end
   intermediateMLP = nn.Dropout(0.1)(intermediateMLP)
--   intermediateMLP = nn.PrintLayer("intermediate",1.0,true)(intermediateMLP)
--   local bilinear = nn.Bilinear(params.embeddings_dimensionality, 100, 1)({xemb,intermediateMLP})
   local bilinear = nn.Linear(100, 1)(intermediateMLP)
--   bilinear = nn.PrintLayer("attention",1.0,true)(bilinear)

--bilinear = nn.AddConstant(3.0)(bilinear)
local attentionBias = 2.0
if neatQA.CONDITION == "nopreview" then
 attentionBias = 2.0
end
print("attention bias"..attentionBias)
bilinear = nn.AddConstant(attentionBias)(bilinear)

   local decisionsAndProbs = nn.BernoulliLayerSigmoidWithLogProb(params.batch_size,1)(bilinear)
   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
   local attention = nn.SelectTable(3)(decisionsAndProbs)
--probs = nn.PrintLayer("probs",1.0,true)(probs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


if false then
 local blockedQ = nn.BlockGradientLayer(params.batch_size,QUESTION_LENGTH)(questionTokensEmbeddingsFlat)
 local blockedQMax = nn.BlockGradientLayer(params.batch_size,1)(questionEmbeddingMax)
end

   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, y)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})
--  module:getParameters():uniform(-params.init_weight, params.init_weight)



  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
    parameters[i]:uniform(-epsilon, epsilon)
    gradparameters[i]:zero()
    print(i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("   "..parameters[i][1]:norm())
    end
    if false and (i==4) then
      assert(DOING_DEBUGGING)
      parameters[i]:zero()
    end
  end
  if #param ~= #parameters then
    print("WARNING: attention parameters have different numbers")
    print(param)
    print(parameters)
    print("===")
  end
  for i=1,#param do
    if param[i] ~= "PLACEHOLDER" then
      parameters[i]:copy(param[i])
      if false then
        print("Not copying gradparam at 17342")
        gradparameters[i]:copy(gradparam[i]):zero()
      end
    end
  end

  for i=1, #parameters do
    print("# "..i..parameters[i]:norm().." at 16743 ")
    if parameters[i]:dim() > 1 then
        print("#   "..parameters[i][1]:norm())
    end
  end


  return transfer_data(module)
end

























function attention.createAttentionBilinearPrevious(param,gradparam)
 assert(false)
 if neatQA.USE_INNOVATIVE_ATTENTION then
   print("Usng innovative attention!")
   return attention.createAttentionBilinearEXPERIMENTINGDirect(param,gradparam)
 end
   assert(neatQA.ATTENTION_DOES_Q_ATTENTION)

   assert(params.TASK == 'neat-qa')

   local x = nn.Identity()()
   print("Doing training on attention embeddings?")
   neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS = false
   print(neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS)
   local xemb
   if neatQA.DO_TRAINING_ON_ATTENTION_EMBEDDINGS then
     xemb = nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x)
   else
     xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   end

   local y = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()
   xemb = nn.Dropout(0.1)(xemb)

local number_of_inputs = nil
if neatQA.CONDITION == "preview" then
   y0 = y
   -- y0 consists of the one-hot vectors of the questions

  -- questionTokensReshaped =  nn.JoinTable(1)(y0)
  -- questionTokensEmbeddings = nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(questionTokensReshaped)
--   questionTokensEmbeddings = nn.View(-1,params.batch_size,params.embeddings_dimensionality)(questionTokensEmbeddings)

   questionTokensEmbeddings = y0

   local inputReplicated = nn.ReplicateDynamic()({questionTokensEmbeddings,xemb})

--   inputReplicated = nn.PrintLayer("",1.0,false)(inputReplicated)
  -- questionTokensEmbeddings = nn.PrintLayer("",1.0,false)(questionTokensEmbeddings)

inputReplicated = nn.View(-1,params.embeddings_dimensionality)(inputReplicated)
questionTokensEmbeddings = nn.View(-1,params.embeddings_dimensionality)(questionTokensEmbeddings)

QUESTION_ATTENTION_BILINEAR = true --false --true --false --true--true --false --true --false --true
print("Question Attention bilinear? ")
print(QUESTION_ATTENTION_BILINEAR)
local dimensionOfConvolution
if QUESTION_ATTENTION_BILINEAR then
   dimensionOfConvolution = 10
   questionTokensEmbeddings = nn.Bilinear(params.embeddings_dimensionality, params.embeddings_dimensionality,dimensionOfConvolution)({inputReplicated, questionTokensEmbeddings})
else
   dimensionOfConvolution = 1
   questionTokensEmbeddings = nn.DotProduct()({inputReplicated, questionTokensEmbeddings})
   -- batch * length * conv
end
--questionTokensEmbeddings = nn.PrintLayer("questionTokensEmbeddings",1.0,false)(questionTokensEmbeddings)


questionTokensEmbeddingsFlat = nn.View(params.batch_size,-1,dimensionOfConvolution)(questionTokensEmbeddings) -- batch * length * conv
--questionTokensEmbeddingsFlat = nn.PrintLayer("questionTokensEmbeddingsFlat",1.0,true)(questionTokensEmbeddingsFlat)

   questionEmbeddingMax = nn.Max(2,3)(questionTokensEmbeddingsFlat)
  -- questionEmbeddingMax = nn.PrintLayer("questionEmbeddingMax",1.0,false)(questionEmbeddingMax)
   questionEmbeddingMax = nn.View(params.batch_size,dimensionOfConvolution)(questionEmbeddingMax) -- batch * conv
   --questionEmbeddingMax = nn.PrintLayer("questionEmbeddingMax",1.0,false)(questionEmbeddingMax)
--   y0 = nn.PrintLayer("y",1.0,true)(y0)
   y2 = nn.JoinTable(1,1)({questionEmbeddingMax,nn.Dropout(0.4)(z),nn.Dropout(0.4)(u)})
number_of_inputs = 2*params.rnn_size + dimensionOfConvolution
else
   y2 = nn.JoinTable(1,1)({nn.Dropout(0.4)(z),nn.Dropout(0.4)(u)})
number_of_inputs = 2*params.rnn_size + 1
number_of_inputs = 2
end

--   y2 = nn.PrintLayer("",1.0,true)(y2)

   local intermediateMLP = nn.Linear(number_of_inputs,100)(y2)
if false then
   intermediateMLP = nn.Tanh()(intermediateMLP)
else
   intermediateMLP = nn.ReLU()(intermediateMLP)
end
   intermediateMLP = nn.Dropout(0.1)(intermediateMLP)
--   intermediateMLP = nn.PrintLayer("intermediate",1.0,true)(intermediateMLP)
   local bilinear = nn.Bilinear(params.embeddings_dimensionality, 100, 1)({xemb,intermediateMLP})

--   bilinear = nn.PrintLayer("attention",1.0,true)(bilinear)

--bilinear = nn.AddConstant(3.0)(bilinear)
local attentionBias = 2.0
if neatQA.CONDITION == "nopreview" then
 attentionBias = 2.0
end
print("attention bias"..attentionBias)
bilinear = nn.AddConstant(attentionBias)(bilinear)

   local attention = nn.Sigmoid()(bilinear)
   local decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
--probs = nn.PrintLayer("probs",1.0,true)(probs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


if false then
 local blockedQ = nn.BlockGradientLayer(params.batch_size,QUESTION_LENGTH)(questionTokensEmbeddingsFlat)
 local blockedQMax = nn.BlockGradientLayer(params.batch_size,1)(questionEmbeddingMax)
end

   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, y)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})
--  module:getParameters():uniform(-params.init_weight, params.init_weight)



  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
    parameters[i]:uniform(-epsilon, epsilon)
    gradparameters[i]:zero()
    if false and (i==6 or i ==7) then
      assert(DOING_DEBUGGING)
      parameters[i]:zero()
    end
  end
  if #param ~= #parameters then
    print("WARNING: attention parameters have different numbers")
    print(param)
    print(parameters)
    print("===")
  end
  for i=1,#param do
    if param[i] ~= "PLACEHOLDER" then
      parameters[i]:copy(param[i])
      if false then
        print("Not copying gradparam at 17342")
        gradparameters[i]:copy(gradparam[i]):zero()
      end
    end
  end



  return transfer_data(module)
end



