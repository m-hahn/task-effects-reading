attention = {}

require('nn.BernoulliLayer')

function attention.createAttentionNetworkOneHot()
   assert(false)
   local x = nn.Identity()()
   local y = nn.Identity()()
   local x2h = nn.LookupTable(params.vocab_size, params.rnn_size)(x)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)
   local hidden
   local surprisal
      hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module
      module = nn.gModule({x, y},
                                      {attention})
   module:getParameters():uniform(-params.init_weight, params.init_weight)
   return transfer_data(module)
end


attention.ABLATE_INPUT = false
attention.ABLATE_STATE = false
attention.ABLATE_SURPRISAL = false


if string.match(params.ablation, 'i') then
  attention.ABLATE_INPUT = true
end
if string.match(params.ablation, 'r') then
  attention.ABLATE_STATE = true
end
if string.match(params.ablation, 's') then
  attention.ABLATE_SURPRISAL = true
end

--[[print(string.match(params.ablation, 's'))
print(string.match(params.ablation, 'r'))
print(string.match(params.ablation, 'i'))]]


print("ABLATION INP STATE SURP")
print(attention.ABLATE_INPUT)
print(attention.ABLATE_STATE)
print(attention.ABLATE_SURPRISAL)



function attention.createAttentionNetworkEmbeddingsSurprisalWithBaseline()
   assert(false)
   if not (params.TASK == 'combined') then
      crash()
   end
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTable(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local surprisal = nn.Identity()()

   

   -- ABLATION OF INPUT
   if attention.ABLATE_INPUT then
     xemb = nn.MulConstant(0)(xemb)
   end

   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)

   -- ABLATION OF STATE
   if attention.ABLATE_STATE then
      y2h = nn.MulConstant(0)(y2h)
   end

   local z2h = nn.Linear(1, params.rnn_size)(surprisal)

   -- ABLATION OF SURPRISAL
   if attention.ABLATE_SURPRISAL then
      z2h = nn.MulConstant(0)(z2h)
   end

   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h, z2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()

   local baseline = nn.Linear(params.rnn_size, 1)(hidden)


   local module = nn.gModule({x, y, surprisal},
                                      {attention, baseline})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end



function attention.createAttentionNetworkEmbeddingsSurprisal()
   assert(false)
   if not (params.TASK == 'combined') then
      crash()
   end
   if USE_BASELINE_NETWORK then
      return attention.createAttentionNetworkEmbeddingsSurprisalWithBaseline()
   end
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTable(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local surprisal = nn.Identity()()

   

   -- ABLATION OF INPUT
   if attention.ABLATE_INPUT then
     xemb = nn.MulConstant(0)(xemb)
   end

   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)

   -- ABLATION OF STATE
   if attention.ABLATE_STATE then
      y2h = nn.MulConstant(0)(y2h)
   end

   local z2h = nn.Linear(1, params.rnn_size)(surprisal)

   -- ABLATION OF SURPRISAL
   if attention.ABLATE_SURPRISAL then
      z2h = nn.MulConstant(0)(z2h)
   end

   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h, z2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module = nn.gModule({x, y, surprisal},
                                      {attention})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end

function attention.createAttentionBilinearEXPERIMENTINGDirect(param,gradparam)
   assert(params.TASK == 'neat-qa')
--assert(DOING_DEBUGGING)
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   local q1 = nn.Identity()()
   local q2 = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()

-- the question 
q = nn.JoinTable(1,1)({nn.Dropout(0.1)(q1),nn.Dropout(0.1)(q2)})


-- the recurrent state of the reader
y2 = nn.JoinTable(1,1)({nn.Dropout(0.1)(z),nn.Dropout(0.1)(u)})
local intermediateMLP = y2
--local intermediateMLP = nn.Linear(2*params.rnn_size,100)(y2)
--intermediateMLP = nn.ReLU()(intermediateMLP)
--intermediateMLP = nn.Dropout(0.1)(intermediateMLP)
xemb = nn.Dropout(0.1)(xemb)

wordCombinedWithContext = nn.Bilinear(params.embeddings_dimensionality, 2*params.rnn_size, 1)({xemb,intermediateMLP})

local preAttentionLayer = nil
local number_of_inputs = nil
if neatQA.CONDITION == "preview" then
--   y0 = y
--   y0 = nn.PrintLayer("y",1.0,true)(y0)
--   y2 = nn.JoinTable(1,1)({nn.Dropout(0.1)(y0),nn.Dropout(0.1)(z),nn.Dropout(0.1)(u)})
  contextCombinedWithQuestion = nn.Bilinear(2*params.rnn_size, 2*params.rnn_size, 1)({q,intermediateMLP})
  wordCombinedWithQuestion = nn.Bilinear(params.embeddings_dimensionality, 2*params.rnn_size, 1)({xemb, q})
  preAttentionLayer = nn.CAddTable()({nn.MulConstant(0)(wordCombinedWithContext), nn.MulConstant(0)(contextCombinedWithQuestion), wordCombinedWithQuestion})
number_of_inputs = 3
else
 preAttentionLayer = wordCombinedWithContext
number_of_inputs = 2
end

--preAttentionLayer = nn.Linear(10,1)(preAttentionLayer)

--bilinear = nn.AddConstant(3.0)(bilinear)
local attentionBias = 3.0
if neatQA.CONDITION == "nopreview" then
 attentionBias = 1.0
end
if neatQA.PRETRAINED_ATTENTION then
 assert(neatQA.CONDITION == "preview")
 attentionBias = 10.0
end
   preAttentionLayer = nn.AddConstant(attentionBias)(preAttentionLayer)

   local attention = nn.Sigmoid()(preAttentionLayer)
   local decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, q1)
     table.insert(inputNodes, q2)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})
--  module:getParameters():uniform(-params.init_weight, params.init_weight)



  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    if false then
      parameters[i]:zero()
    else
      local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
      parameters[i]:uniform(-epsilon, epsilon)
    end
    gradparameters[i]:zero()
  end
  if #param ~= #parameters then
    print("WARNING: attention parameters have different numbers")
    print(param)
    print(parameters)
    print("===")
  end
  for i=1,#param do
    parameters[i]:copy(param[i])
    gradparameters[i]:copy(gradparam[i])
  end



  return transfer_data(module)
end



function attention.createAttentionBilinearEXPERIMENTING(param,gradparam)
   assert(params.TASK == 'neat-qa')
--assert(DOING_DEBUGGING)
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   local q1 = nn.Identity()()
   local q2 = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()

-- the question 
q = nn.JoinTable(1,1)({nn.Dropout(0.1)(q1),nn.Dropout(0.1)(q2)})


-- the recurrent state of the reader
y2 = nn.JoinTable(1,1)({nn.Dropout(0.1)(z),nn.Dropout(0.1)(u)})
local intermediateMLP = nn.Linear(2*params.rnn_size,100)(y2)
intermediateMLP = nn.ReLU()(intermediateMLP)
--intermediateMLP = nn.Dropout(0.1)(intermediateMLP)
xemb = nn.Dropout(0.1)(xemb)

wordCombinedWithContext = nn.Bilinear(params.embeddings_dimensionality, 100, 10)({xemb,intermediateMLP})

local preAttentionLayer = nil
local number_of_inputs = nil
if neatQA.CONDITION == "preview" then
--   y0 = y
--   y0 = nn.PrintLayer("y",1.0,true)(y0)
--   y2 = nn.JoinTable(1,1)({nn.Dropout(0.1)(y0),nn.Dropout(0.1)(z),nn.Dropout(0.1)(u)})
  contextCombinedWithQuestion = nn.Bilinear(2*params.rnn_size, 100, 10)({q,intermediateMLP})
  wordCombinedWithQuestion = nn.Bilinear(params.embeddings_dimensionality, 2*params.rnn_size, 10)({xemb, q})
  preAttentionLayer = nn.CAddTable()({wordCombinedWithContext, contextCombinedWithQuestion, wordCombinedWithQuestion})
number_of_inputs = 3
else
 preAttentionLayer = wordCombinedWithContext
number_of_inputs = 2
end

preAttentionLayer = nn.Linear(10,1)(preAttentionLayer)

--bilinear = nn.AddConstant(3.0)(bilinear)
local attentionBias = 3.0
if neatQA.CONDITION == "nopreview" then
 attentionBias = 3.0
end
preAttentionLayer = nn.AddConstant(attentionBias)(preAttentionLayer)

   local attention = nn.Sigmoid()(preAttentionLayer)
   local decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, q1)
     table.insert(inputNodes, q2)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})
--  module:getParameters():uniform(-params.init_weight, params.init_weight)



  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
    parameters[i]:uniform(-epsilon, epsilon)
    gradparameters[i]:zero()
  end
  if #param ~= #parameters then
    print("WARNING: attention parameters have different numbers")
    print(param)
    print(parameters)
    print("===")
  end
  for i=1,#param do
    parameters[i]:copy(param[i])
    gradparameters[i]:copy(gradparam[i])
  end



  return transfer_data(module)
end



function attention.createAttentionBilinear(param,gradparam)
 if neatQA.USE_INNOVATIVE_ATTENTION then
   print("Usng innovative attention!")
   return attention.createAttentionBilinearEXPERIMENTINGDirect(param,gradparam)
 end

   assert(params.TASK == 'neat-qa')

   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()

local number_of_inputs = nil
if neatQA.CONDITION == "preview" then
   y0 = y
--   y0 = nn.PrintLayer("y",1.0,true)(y0)
   y2 = nn.JoinTable(1,1)({nn.Dropout(0.1)(y0),nn.Dropout(0.1)(z),nn.Dropout(0.1)(u)})
number_of_inputs = 3
else
   y2 = nn.JoinTable(1,1)({nn.Dropout(0.1)(z),nn.Dropout(0.1)(u)})
number_of_inputs = 2
end

   xemb = nn.Dropout(0.1)(xemb)
   local intermediateMLP = nn.Linear(number_of_inputs*params.rnn_size,100)(y2)
   intermediateMLP = nn.ReLU()(intermediateMLP)
   intermediateMLP = nn.Dropout(0.1)(intermediateMLP)
   local bilinear = nn.Bilinear(params.embeddings_dimensionality, 100, 1)({xemb,intermediateMLP})

--bilinear = nn.AddConstant(3.0)(bilinear)
local attentionBias = 3.0
if neatQA.CONDITION == "nopreview" then
 attentionBias = 2.0
end
bilinear = nn.AddConstant(attentionBias)(bilinear)

   local attention = nn.Sigmoid()(bilinear)
   local decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, y)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})
--  module:getParameters():uniform(-params.init_weight, params.init_weight)



  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
    parameters[i]:uniform(-epsilon, epsilon)
    gradparameters[i]:zero()
  end
  if #param ~= #parameters then
    print("WARNING: attention parameters have different numbers")
    print(param)
    print(parameters)
    print("===")
  end
  for i=1,#param do
    parameters[i]:copy(param[i])
    gradparameters[i]:copy(gradparam[i])
  end



  return transfer_data(module)
end




function attention.createAttentionNetworkEmbeddings()
   assert(false)
   if params.TASK == 'combined' then
      return attention.createAttentionNetworkEmbeddingsSurprisal()
   elseif params.TASK == 'neat-qa' then
      return attention.createAttentionBilinear()
   end
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTable(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)
   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module = nn.gModule({x, y},
                                      {attention})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end



function attention.createAttentionNetwork()
  assert(false)
  if params.ATTENTION_WITH_EMBEDDINGS then
          return attention.createAttentionNetworkEmbeddings()
  else
       return attention.createAttentionNetworkOneHot()
  end
end




