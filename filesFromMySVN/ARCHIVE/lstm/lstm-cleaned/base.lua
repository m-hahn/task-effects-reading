
function clone_network(network)
  local temp = torch.MemoryFile("w"):binary()
  temp:writeObject(network)

  local reader = torch.MemoryFile(temp:storage(), "r"):binary()
  local clonedNetwork = reader:readObject()
  reader:close()
  local cloneParams, cloneGradParams = clonedNetwork:parameters()
  local params, gradParams = network:parameters()
  for i = 1, #params do
    cloneParams[i]:set(params[i])
    cloneGradParams[i]:set(gradParams[i])
  end
  collectgarbage()
  temp:close()
  return clonedNetwork
end


