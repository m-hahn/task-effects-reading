langmod = {}

langmod.TRAIN_LANGMOD = false

if langmod.TRAIN_LANGMOD then
  langmod.INTERVAL = 40
  langmod.gpu = 2
else
  langmod.INTERVAL = 40
  langmod.gpu = 1
end


langmod.nll_average = 0.0

-- separate in saving and loading models?
-- if set to true, then
--- * only save things other than language model
--- * load language model from the path given here
--- * load other things from the model specified in the command line
langmod.SEPARATE_LANGMOD_FROM_NEAT = true
langmod.LANGMOD_MODEL_TO_LOAD = "pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-133-nll"
assert(not(langmod.TRAIN_LANGMOD and langmod.SEPARATE_LANGMOD_FROM_NEAT))


-- these things ought to be stored with the attention module
    langmod.surprisalAverage = 8.0
    langmod.surprisalAveragePreview = 8.0
    langmod.surprisalAverageNoPreview = 8.0



langmod.__name = "langmod"

print(langmod)

function langmod.setupLanguageModelling(SparamF,SparamdF, surprisalAverages)
 if surprisalAverages ~= nil then
    langmod.surprisalAverage = surprisalAverages.surprisalAverage
    langmod.surprisalAveragePreview = surprisalAverages.surprisalAveragePreview
    langmod.surprisalAverageNoPreview = surprisalAverages.surprisalAverageNoPreview
 end
 local  local_rnn_size = 512
  cutorch.setDevice(langmod.gpu)
  print("Creating the language model.")
  langmod.dsA = {}
  langmod.dsA[1] = transfer_data(torch.zeros(params.batch_size,local_rnn_size))
  langmod.dsA[2] = transfer_data(torch.zeros(params.batch_size,local_rnn_size))
  langmod.dsA[3] = transfer_data(torch.zeros(local_rnn_size)) -- NOTE actually will later have different size

  langmod.actor_c ={[0] = torch.CudaTensor(local_rnn_size)}
  langmod.actor_h = {[0] = torch.CudaTensor(local_rnn_size)}

  langmod.actor_c[0] = torch.CudaTensor(params.batch_size,local_rnn_size):zero() --TODO they have to be intiialized decently
  langmod.actor_h[0] = torch.CudaTensor(params.batch_size,local_rnn_size):zero()

  langmod.actor_output = {}

  langmod.nll = torch.FloatTensor(params.batch_size)

  langmod.ones = torch.ones(params.batch_size):cuda()

  langmod.criterion = nn.ClassNLLCriterion():cuda()

     -- ACTOR
     local actor_core_network = autoencoding.create_network(true, true, false, local_rnn_size)
     langmod.actor_network_params, langmod.actor_network_gradparams = actor_core_network:parameters()
     for j=1, #SparamF do
           langmod.actor_network_params[j]:set(SparamF[j])
           langmod.actor_network_gradparams[j]:set(SparamdF[j])
     end
     paramxLM, paramdxLM = actor_core_network:getParameters()
     langmod.actorRNNs = {}
     for i=1,langmod.INTERVAL + 5 do
        print("L "..tostring(i))
        langmod.actorRNNs[i] = g_clone(actor_core_network)
     end

langmod.inputTensorHere = {}
for i=1, params.seq_length do
   langmod.inputTensorHere[i] = torch.CudaTensor(params.batch_size)
end


     langmod.startOfCurrentSequence = 0


langmod.lastSurprisal = torch.CudaTensor(params.batch_size,1):zero()

     cutorch.setDevice(1)
end

function langmod.fpLanguageModellingOneStep(i, inputTensor, decisionsAtTheStep)
  cutorch.setDevice(langmod.gpu)
--  if i == 1 then
--     langmod.nll:zero()
--     langmod.actor_output = {}
--  end
 local realInputTensors = neatQA.original_input_tensors
 local maxLength = math.min(params.seq_length, neatQA.maximalLengthOccurringInInput[1])

-- print(i)
 if i == 1 then
    -- printing?
    langmod.printingThisOne = (math.random() < 0.0501) and true or false
    if langmod.printingThisOne then
           print("NLL "..langmod.nll_average)
           print(math.exp(langmod.nll_average))
           print(".... LANGMOD")
    end

    -- indices
    langmod.startOfCurrentSequence = 1

    -- restart LM
    langmod.actor_c[0]:zero()
    langmod.actor_h[0]:zero()

    -- set gradients to zero
    paramdxLM:mul(params.lr_momentum / (1-params.lr_momentum)) --better to put this inside this if condition
--    paramdxLM:zero()


--    self.paramd:mul(params.lr_momentum / (1-params.lr_momentum)) --better to put this inside this if condition

 end

 local positionInCurrentSequence = i - langmod.startOfCurrentSequence + 1
 --print("  "..positionInCurrentSequence)

-- print(langmod.actor_h)
-- print("10610: ")
-- print("STATE COMES FROM  "..(positionInCurrentSequence-1))
-- print("INPUT COMES FROM  "..(positionInCurrentSequence))
 langmod.inputTensorHere[positionInCurrentSequence]:copy(inputTensor)
 langmod.actor_c[positionInCurrentSequence], langmod.actor_h[positionInCurrentSequence], langmod.actor_output[positionInCurrentSequence] = unpack(langmod.actorRNNs[positionInCurrentSequence]:forward({langmod.inputTensorHere[positionInCurrentSequence], langmod.actor_c[positionInCurrentSequence-1], langmod.actor_h[positionInCurrentSequence-1]}))
-- print("Done forward for "..i.."  "..positionInCurrentSequence)


if i % langmod.INTERVAL == 0 or i == maxLength then
   --print(i.."  backprop")
   if langmod.TRAIN_LANGMOD then
     -- backprop in time
     langmod.bpLanguageModelling(realInputTensors, langmod.startOfCurrentSequence, i-1)

     -- perform gradient update at the end
     if i == maxLength then
      auxiliary.clipGradients(paramdxLM)
      auxiliary.updateParametersWithMomentum(paramxLM, paramdxLM)
     end
   end

   -- which position exactly?
   langmod.actor_c[0] = langmod.actor_c[i - langmod.startOfCurrentSequence + 1]
   langmod.actor_h[0] = langmod.actor_h[i - langmod.startOfCurrentSequence + 1]

   langmod.startOfCurrentSequence = i+1
    if langmod.printingThisOne then
        print("stitching")
    end
end



if langmod.printingThisOne then
   if i < maxLength then
   local l = 1
--  print( realInputTensors)
--print(i)
--print(maxLength)
   if(realInputTensors[i+1] ~= nil and realInputTensors[i+1][l] > 0) then
   local predictedScores, predictedTokens = torch.min(langmod.actor_output[positionInCurrentSequence][l],1)
auxiliary.write((readDict.chars[realInputTensors[i+1][l]]))
auxiliary.write(decisionsAtTheStep[l][1])
auxiliary.write(readDict.chars[predictedTokens[1]])
auxiliary.write(math.exp(-predictedScores[1]))
auxiliary.write(math.exp(-langmod.actor_output[positionInCurrentSequence][l][realInputTensors[i+1][l]]))
io.write("\n")
   end
   end
end

  if (not langmod.TRAIN_LANGMOD) and realInputTensors[i+1] ~= nil then
    cutorch.synchronize()
    -- what is the surprisal when there is a padded element?
    local realInputTensorForThisItem = realInputTensors[i+1]
    realInputTensorForThisItem = torch.cmax(realInputTensorForThisItem,1) -- for padded elements, get rid of the zero
--    print(realInputTensorForThisItem:view(params.batch_size,1))
  --  print(langmod.actor_output[positionInCurrentSequence]:size())
    langmod.lastSurprisal:gather(langmod.actor_output[positionInCurrentSequence],2,realInputTensorForThisItem:view(params.batch_size,1))


    local conditionMask = torch.CudaTensor(params.batch_size,1)
    conditionMask:copy(neatQA.condition_mask)


    local surprisalAverage = langmod.lastSurprisal:sum()
    local previewFraction = conditionMask:sum()


    local surprisalTimesFraction = torch.cmul(langmod.lastSurprisal, conditionMask):sum()
    
    local surprisalAveragePreview = surprisalTimesFraction / (previewFraction + 0.000001)
    local surprisalAverageNoPreview = (surprisalAverage - surprisalTimesFraction) / (params.batch_size - previewFraction + 0.000001)

    local surprisalMean = surprisalAverage / params.batch_size


    -- center surprisal, scale to [-1,1]
    langmod.lastSurprisal:add(-langmod.surprisalAverage)
    langmod.lastSurprisal:add(-( langmod.surprisalAveragePreview- langmod.surprisalAverage), conditionMask)
    langmod.lastSurprisal:mul(1/15)

    -- adjust running average
    if (not DOING_EVALUATION_OUTPUT) then
      local decayRate = 0.99
      langmod.surprisalAverage = decayRate * langmod.surprisalAverage + (1-decayRate) * surprisalMean
      langmod.surprisalAveragePreview = decayRate * langmod.surprisalAveragePreview + (1-decayRate) * surprisalAveragePreview
      langmod.surprisalAverageNoPreview = decayRate * langmod.surprisalAverageNoPreview + (1-decayRate) * surprisalAverageNoPreview
    end
  end



  cutorch.setDevice(1)
end

function langmod.bpLanguageModelling(realInputTensors, startOfSequence, endOfSequence, maxLength)
  assert(langmod.TRAIN_LANGMOD)

  langmod.dsA[1]:zero()
  langmod.dsA[2]:zero()

  local lengthOfSequence = endOfSequence - startOfSequence

      for i = lengthOfSequence+2, 1, -1 do
        if  i+startOfSequence <= math.min(neatQA.maximalLengthOccurringInInput[1],params.seq_length) then
          local targetInputTensor = realInputTensors[i+startOfSequence]:cuda()
          local nll = - langmod.criterion:forward(langmod.actor_output[i],  targetInputTensor)
          langmod.nll_average = 0.9999 * langmod.nll_average + 0.0001 * nll
          langmod.dsA[3] = langmod.criterion:backward(langmod.actor_output[i],  targetInputTensor):mul(-1)
          local prior_c = langmod.actor_c[i-1]
          local prior_h = langmod.actor_h[i-1]
          local derr = transfer_data(torch.ones(1))
          local tmp = langmod.actorRNNs[i]:backward({langmod.inputTensorHere[i], prior_c, prior_h},langmod.dsA)
          langmod.dsA[1]:copy(tmp[2])
          langmod.dsA[2]:copy(tmp[3])
          cutorch.synchronize()
        end
      end
end





