from subprocess import call
from subprocess import Popen, PIPE
import sys


print("WARNING: Have you made sure CREATE_RECORDED_FILES is set to true?")

LOOK_AT_ORIGINAL_CONDITION = False

seq_length = 500

DO_IT_ON_TESTSET = True #False
if DO_IT_ON_TESTSET:
   CORPUS_NUMBER = 12
else:
   CORPUS_NUMBER = 15

CONDITION = sys.argv[1] #"nopreview"

##############
with open("modelsForPredictions.txt","r") as myfile:
   data = myfile.read().split('\n')
############

OFFSET = 6

for model in data:
  if len(model) == 0:
    continue
  if model[0] == "#":
    continue
  if LOOK_AT_ORIGINAL_CONDITION:
    if "nopreview" in model and CONDITION == "preview":
      continue
    elif (not ("nopreview" in model)) and CONDITION == "nopreview":
      continue
  command = model.split(">")
  if len(command) > 1:
    assert len(command) == 2
    originalOutputFile = command[1].rstrip().lstrip()
    assert (not (" " in originalOutputFile))
  else:
    #originalOutputFile = "Output-For-"+model[1]+".txt"
    assert False

  resu = Popen(["grep","Printing stuff",originalOutputFile],stdout=PIPE)
  nameOfModel = resu.communicate()
  nameOfModel = nameOfModel[0].rstrip().replace("Printing stuff to ","")
  if nameOfModel == "":
    print("ERROR: No model file")
    print(model)
    continue

  command = command[0].split(" ")
  print(command)
  command[4] = "run-stats-output.json"
  command[OFFSET+2] = "true"
  command[OFFSET+3] = "true"
  command[OFFSET+5] = str(seq_length)
  command[OFFSET+9] = "true"
  command[OFFSET+14] = "true"
  command[OFFSET+15] = nameOfModel
  command[OFFSET+16] = "OUTPUT-"+CONDITION+"-"+nameOfModel
  command[OFFSET+18] = "false"
  command[OFFSET+19] = str(CORPUS_NUMBER)
  command[OFFSET+23] = "false"
  command[OFFSET+24] = "fixed"
#  if len(command) < OFFSET+25+1:
#    command.append("")
#    command[OFFSET+24] = command[OFFSET+24][:-1]
  command[OFFSET+25] = CONDITION+'"'
  originalPath = originalOutputFile.split("/")
  originalPrefix = originalPath[:-1]
  originalFileName = originalPath[-1]
  command = command + [">", "/".join(originalPrefix)+"/OUT-"+CONDITION+"-"+str(CORPUS_NUMBER)+"-"+originalFileName]
  print(command)
  print("TO EXECUTE")
  print(" ".join(command))
  call(" ".join(command), shell=True)
