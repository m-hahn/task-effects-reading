-- extracted from neat-qa-Unrolled-Attention.lua (25/11/2016)



function neatQA.doBackwardForAttention(attentionObjects) 
 local gradientsAttention = {}


if neatQA.USE_ATTENTION_NETWORK then
  auxiliary.prepareMomentum(paramdxRA)

  if params.lr_att > 0 and (true and train_attention_network) then
     --assert(false,"reward")


     -- compute reward
     local reward = torch.CudaTensor(params.batch_size,1)

     reward:fill(params.ATTENTION_VALUES_BASELINE)

     for i=1,neatQA.maximalLengthOccurringInInput[1] do
         reward:add(attention_decisions[i])
     end
--print(reward)

if false then
print("PRINTING REWARDS 39348")
print("Attention sums")
print(reward)
end


-- now scale with the length of the sequences
if neatQA.scaleAttentionWithLength then
  reward:div( neatQA.maximalLengthOccurringInInput[1] )
end

if false then
print("PRINTING REWARDS 3927")
print("Attention scaled")
print(reward)
end



reward:mul(params.TOTAL_ATTENTIONS_WEIGHT)

if false then
print("PRINTING REWARDS 3927")
print("Attention multiplied  "..params.TOTAL_ATTENTIONS_WEIGHT)
print(reward)
end






if neatQA.rewardBasedOnLogLikeLoss then
if false then
     print("LogLoss 19822")
     print(nll)
end
     reward:add(transfer_data(nll))
     -- as of 21/11/2016, these numbers are positive
else
     local lossTensor = torch.FloatTensor(params.batch_size):zero()
     for l=1,params.batch_size do
       local answerID = qa.getFromAnswer(readChunks.corpus,l,1)
       local predictedScore,predictedAnswer = torch.max(actor_output[l],1)
       if answerID == predictedAnswer[1] then
         lossTensor[l] = -1
       end
     end
    if false then
      print("ACCURACY LOSS")
      print(lossTensor)
      print("40413 "..lossTensor:mean())
    end
    reward:add(transfer_data(lossTensor))
end

if torch.uniform()>0.999 then
 print("----38714")
 print(reward)
end

if false then
print("Total reward 42014")
print(reward)
end

-- as of 19/11/2016: higher reward is worse

     local rewardDifference = reward:clone():add(-rewardBaseline, ones) 

     local baselineScores
     if USE_BIDIR_BASELINE then
        bidir_baseline_gradparams:zero()
        local inputsWithActions = {}
        for i=1,  neatQA.maximalLengthOccurringInInput[1] do
            inputsWithActions[i] = joinTable[i]:forward({neatQA.inputTensors[i]:view(-1,1), attention_decisions[i]:view(-1,1)})
        end
        baselineScores = bidir_baseline:forward(inputsWithActions)
        for i=1, neatQA.maximalLengthOccurringInInput[1]  do
          local target
          if i == neatQA.maximalLengthOccurringInInput[1]  then
             target = reward:view(params.batch_size,1)
         else
             target = baselineScores[i+1]:view(params.batch_size,1)
          end

             if i == neatQA.maximalLengthOccurringInInput[1] and torch.uniform() > 0.93 then
               print("BIDIR "..i)
               print("40624 TARGET ")
               print(target)

               print("SCORES ")
               print(baselineScores[i])
             end
          bidirBaseline.criterion_gradients[i]:copy(baseline_criterion:backward(baselineScores[i]:view(-1,1), target))
          if i == params.seq_length then
             --print(baseline_criterion:backward(baselineScores[i]:view(-1,1), target))
          end
       end
       bidir_baseline:backward(inputsWithActions, auxiliary.shortenTable(bidirBaseline.criterion_gradients,  neatQA.maximalLengthOccurringInInput[1]))

       auxiliary.clipGradients(bidir_baseline_gradparams)
       bidir_baseline_params:add(-0.0008, bidir_baseline_gradparams) -- this is changed from the NEAT version, which first multiplied in place
     end

if false then
print("BASELINE 46218")
print(baselineScores[neatQA.maximalLengthOccurringInInput[1]])
end

     -- this mean is not actually used, what is used is the bidirectional predictor
     rewardBaseline = 0.95 * rewardBaseline + 0.05 * (torch.sum(reward) * 1.0/params.batch_size)
     for i = neatQA.maximalLengthOccurringInInput[1], 1, -1 do
        rewardDifferenceForI = rewardDifference
        if USE_BIDIR_BASELINE then
            if i>1 then
--print("43910")
--print(baselineScores[i-1])
--print(reward)
              rewardDifferenceForI = baselineScores[i-1]:clone():add(-1,reward):mul(-1)
--print(rewardDifferenceForI)
if true and torch.uniform() > 0.9 and i == 100 then
 print("4349  "..i.."   "..(torch.mean(torch.abs(rewardDifference) - torch.abs(rewardDifferenceForI))))
end

            end
        end

        local whatToMultiplyToTheFinalDerivative = torch.CudaTensor(params.batch_size)
        local attentionEntropyFactor =  torch.CudaTensor(params.batch_size)
        for j=1,params.batch_size do
-- it is important to take the probabilities and not the scores, since it will all be fed into the probabilities port of the attention moduke
           attentionEntropyFactor[j] =  params.ENTROPY_PENALTY * (math.log(attentionObjects.probabilities[i][j][1]) - math.log(1 - attentionObjects.probabilities[i][j][1]))
--           print("161")
  --         print(params.ENTROPY_PENALTY)
    --       print(attentionEntropyFactor[j])
      --     print(attentionObjects.probabilities[i][j][1])
           whatToMultiplyToTheFinalDerivative[j] = 1 / (attentionObjects.probabilities[i][j][1]) -- THIS is an important part of REINFORCE!
        end

if false and i==100 then

       print("REWARD DIFFERENCE 48334")
       print(rewardDifferenceForI)


print("PROBS")
print(attention_probabilities[i])
print("ENTROPY 49446")
print(attentionEntropyFactor)

print("WHATTOMULTIPLY 49722")
print(whatToMultiplyToTheFinalDerivative)
end

if false and torch.uniform() > 0.98 then
print("4818")
print(attentionEntropyFactor)
end
        local factorsForTheDerivatives =  rewardDifferenceForI:clone():cmul(whatToMultiplyToTheFinalDerivative)
        factorsForTheDerivatives:add(attentionEntropyFactor)
if false and i==100 then
print("DERIVATIVE 50618")
print(factorsForTheDerivatives)
end





       local attentionNetwork = attentionObjects.attentionNetworks[i]
       local originalInputTensor = attentionObjects.originalInputTensors[i]
       local forwardHidden = attentionObjects.forwardHidden[i-1]
       local forwardCell = attentionObjects.forwardCell[i-1]
       local attentionArguments = {originalInputTensor}
       if neatQA.CONDITION == "preview" then
        table.insert(attentionArguments,attentionObjects.questionForward)
       end
       table.insert(attentionArguments,forwardHidden)
       table.insert(attentionArguments,forwardCell)
       local gradientAttention = attentionNetworks[i]:backward(attentionArguments,{torch.CudaTensor(), torch.CudaTensor(), factorsForTheDerivatives, torch.CudaTensor()})
       gradientsAttention[i] = {questionForward = gradientAttention[2], forwardHidden = gradientAttention[3], forwardCell = gradientAttention[4]}

--print(gradientC)

--print(gradientsContext)

     end
     assert(norm_dwRA == norm_dwRA)
 
      auxiliary.clipGradients(paramdxRA)
      auxiliary.updateParametersWithMomentum(paramxRA,paramdxRA,params.lr_att)


  end
end  

return gradientAttention
  
end






