attention = {}

require('nn.BernoulliLayer')

function attention.createAttentionNetworkOneHot()
   assert(false)
   local x = nn.Identity()()
   local y = nn.Identity()()
   local x2h = nn.LookupTable(params.vocab_size, params.rnn_size)(x)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)
   local hidden
   local surprisal
      hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module
      module = nn.gModule({x, y},
                                      {attention})
   module:getParameters():uniform(-params.init_weight, params.init_weight)
   return transfer_data(module)
end


attention.ABLATE_INPUT = false
attention.ABLATE_STATE = false
attention.ABLATE_SURPRISAL = false


if string.match(params.ablation, 'i') then
  attention.ABLATE_INPUT = true
end
if string.match(params.ablation, 'r') then
  attention.ABLATE_STATE = true
end
if string.match(params.ablation, 's') then
  attention.ABLATE_SURPRISAL = true
end

--[[print(string.match(params.ablation, 's'))
print(string.match(params.ablation, 'r'))
print(string.match(params.ablation, 'i'))]]


print("ABLATION INP STATE SURP")
print(attention.ABLATE_INPUT)
print(attention.ABLATE_STATE)
print(attention.ABLATE_SURPRISAL)



function attention.createAttentionNetworkEmbeddingsSurprisalWithBaseline()
   assert(false)
   if not (params.TASK == 'combined') then
      crash()
   end
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTable(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local surprisal = nn.Identity()()

   

   -- ABLATION OF INPUT
   if attention.ABLATE_INPUT then
     xemb = nn.MulConstant(0)(xemb)
   end

   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)

   -- ABLATION OF STATE
   if attention.ABLATE_STATE then
      y2h = nn.MulConstant(0)(y2h)
   end

   local z2h = nn.Linear(1, params.rnn_size)(surprisal)

   -- ABLATION OF SURPRISAL
   if attention.ABLATE_SURPRISAL then
      z2h = nn.MulConstant(0)(z2h)
   end

   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h, z2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()

   local baseline = nn.Linear(params.rnn_size, 1)(hidden)


   local module = nn.gModule({x, y, surprisal},
                                      {attention, baseline})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end



function attention.createAttentionNetworkEmbeddingsSurprisal()
   assert(false)
   if not (params.TASK == 'combined') then
      crash()
   end
   if USE_BASELINE_NETWORK then
      return attention.createAttentionNetworkEmbeddingsSurprisalWithBaseline()
   end
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTable(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local surprisal = nn.Identity()()

   

   -- ABLATION OF INPUT
   if attention.ABLATE_INPUT then
     xemb = nn.MulConstant(0)(xemb)
   end

   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)

   -- ABLATION OF STATE
   if attention.ABLATE_STATE then
      y2h = nn.MulConstant(0)(y2h)
   end

   local z2h = nn.Linear(1, params.rnn_size)(surprisal)

   -- ABLATION OF SURPRISAL
   if attention.ABLATE_SURPRISAL then
      z2h = nn.MulConstant(0)(z2h)
   end

   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h, z2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module = nn.gModule({x, y, surprisal},
                                      {attention})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end




function attention.createAttentionBilinear(param,gradparam)
   assert(params.TASK == 'neat-qa')

   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local z = nn.Identity()()
   local u = nn.Identity()()

local number_of_inputs = nil
if neatQA.CONDITION == "preview" then
   y2 = nn.JoinTable(1,1)({nn.Dropout(0.1)(y),nn.Dropout(0.1)(z),nn.Dropout(0.1)(u)})
number_of_inputs = 3
else
   y2 = nn.JoinTable(1,1)({nn.Dropout(0.1)(z),nn.Dropout(0.1)(u)})
number_of_inputs = 2
end

   xemb = nn.Dropout(0.1)(xemb)
   local intermediateMLP = nn.Linear(number_of_inputs*params.rnn_size,100)(y2)
   intermediateMLP = nn.ReLU()(intermediateMLP)
   intermediateMLP = nn.Dropout(0.1)(intermediateMLP)
   local bilinear = nn.Bilinear(params.embeddings_dimensionality, 100, 1)({xemb,intermediateMLP})

--bilinear = nn.AddConstant(3.0)(bilinear)
local attentionBias = 3.0
if neatQA.CONDITION == "nopreview" then
 attentionBias = 2.0
end
bilinear = nn.AddConstant(attentionBias)(bilinear)

   local attention = nn.Sigmoid()(bilinear)
   local decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(nn.View(params.batch_size,1)(decisions))
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


   local inputNodes = {x}
   if neatQA.CONDITION == "preview" then
     table.insert(inputNodes, y)
   end
   table.insert(inputNodes,z)
   table.insert(inputNodes, u)
   local module = nn.gModule(inputNodes,{blockedAttention,blockedDecisions,probs,blockedInput})
--  module:getParameters():uniform(-params.init_weight, params.init_weight)



  parameters, gradparameters = module:parameters()
  for i=1, #parameters do
    local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
    parameters[i]:uniform(-epsilon, epsilon)
    gradparameters[i]:zero()
  end
  for i=1,#param do
    parameters[i]:copy(param[i])
    gradparameters[i]:copy(gradparam[i])
  end



  return transfer_data(module)
end




function attention.createAttentionNetworkEmbeddings()
   assert(false)
   if params.TASK == 'combined' then
      return attention.createAttentionNetworkEmbeddingsSurprisal()
   elseif params.TASK == 'neat-qa' then
      return attention.createAttentionBilinear()
   end
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTable(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)
   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module = nn.gModule({x, y},
                                      {attention})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end



function attention.createAttentionNetwork()
  assert(false)
  if params.ATTENTION_WITH_EMBEDDINGS then
          return attention.createAttentionNetworkEmbeddings()
  else
       return attention.createAttentionNetworkOneHot()
  end
end




