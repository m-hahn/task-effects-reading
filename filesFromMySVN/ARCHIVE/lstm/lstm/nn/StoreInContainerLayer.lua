require 'nn'

local StoreInContainerLayer, _ = torch.class('nn.StoreInContainerLayer', 'nn.Module')

-- note does NOT try to infer minibatch size
function StoreInContainerLayer:__init(container)
   assert(container ~= nil)
   self.result_container = container
end


function StoreInContainerLayer:updateOutput(input)
   self.output = input
   self.result_container.output = input
   return self.output
end

-- NOTE assumes input is a tensor, not a table
function StoreInContainerLayer:updateGradInput(input, gradOutput)
   self.gradInput = gradOutput
   self.result_container.grad = gradOutput
   return self.gradInput
end


function StoreInContainerLayer:clearState()
   self.result_container.output = nil
   self.result_container.grad = nil
   -- don't call set because it might reset referenced tensors
   local function clear(f)
      if self[f] then
         if torch.isTensor(self[f]) then
            self[f] = self[f].new()
         elseif type(self[f]) == 'table' then
            self[f] = {}
         else
            self[f] = nil
         end
      end
   end
   clear('output')
   clear('gradInput')
   return self
end

