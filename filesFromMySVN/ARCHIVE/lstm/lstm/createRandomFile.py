path = "/disk/scratch/s1582047/corpus7/input.txt"

f = open(path, 'w')

f.write("\n\n\n")

for i in range(0,100000):
   if random() > 0.8:
      f.write("b ")
   else if random() > 0.5:
      f.write("c ")
   else:
      f.write("a ")

f.write("\n")

f.close()
