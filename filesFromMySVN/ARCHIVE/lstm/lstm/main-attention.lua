

BASE_DIRECTORY = "/afs/cs.stanford.edu/u/mhahn/scr/"


BlockGradientLayer = require('nn.BlockGradientLayer')

require('globalForExpOutput')


require('phono')
require('linearization')
require('rnn')
require('soft-attention')
require('q-attention')
require('bidirBaseline')
require('qa')


require('cunn')

require('nngraph')
require('base')
require('data') --ptb

-- SET THE PARAMETERS
require('setParameters')
--

require('readDict')



require('datasets')


------------------------

require 'lfs'


require('storeAnnotation')



assert(not ((not DOING_EVALUATION_OUTPUT) and (not DO_TRAINING)))

------------------------


-- READ THE DICTIONARY


require('readChunks')

require('numericalValues')

require('readFiles')

require('lstm')

require('attention')

require('auxiliary')

require('autoencoding')

require('combined')

require('langmod')

--require('neat-qa-UNDO-THE-CHANGES')
--require('neat-qa')
--require('neat-qa-Unrolled-Attention')
--require('neat-qa-SplitInput')

require('binning')

assert(params.TASK == 'combined' or params.TASK == 'langmod')


local function setup()
   if params.TASK == 'autoencoding' or params.TASK == 'combined' then
      return autoencoding.setupAutoencoding()
   elseif params.TASK == 'langmod' then
      return setupLanguageModelling()
   elseif params.TASK == 'qa' then
      return setupQA()
   elseif params.TASK == 'phono' then
      return setupPhono()
   elseif params.TASK == 'linearization' then
      return setupLinearization()
   elseif params.TASK == 'combined-soft' then
      return setupCombinedSoft()
   elseif params.TASK == 'combined-q' then
      return setupCombinedQ()
   elseif params.TASK == 'neat-qa' then
      return neatQA.setup()
   else
      print(TASK)
      crash()
   end
end



local function reset_state(state)

end

-- TODO make more efficient by filling zeros in place with :zero? but note that these things are from inside some neural network
function reset_ds()



   if params.TASK == 'autoencoding' then
    assert(false)
    model.dsR[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    model.dsR[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h


    model.dsA[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    model.dsA[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h
    model.dsA[3] = torch.zeros(params.batch_size,params.vocab_size):cuda() --h
  elseif params.TASK == 'neat-qa' then
    model.dsR[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    model.dsR[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h
    if neatQA.ALSO_DO_LANGUAGE_MODELING then
      model.dsR[3] = torch.zeros(params.batch_size,params.vocab_size):cuda() --h
    end

    --model.dsA[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    --model.dsA[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h
    --model.dsA[3] = torch.zeros(params.batch_size,params.vocab_size):cuda() --h


--   elseif params.TASK == "combined-q" then
-- no need to do anything
   elseif params.TASK == 'combined' or params.TASK == 'combined-soft' or params.TASK == 'combined-q' then
    assert(params.TASK == 'combined' or params.TASK == 'combined-soft')
    model.dsR[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    model.dsR[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h
    model.dsR[3] = torch.zeros(params.batch_size,params.vocab_size):cuda() --h


    model.dsA[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    model.dsA[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h
    model.dsA[3] = torch.zeros(params.batch_size,params.vocab_size):cuda() --h

   elseif params.TASK == 'langmod' then

    model.dsA[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    model.dsA[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h
    model.dsA[3] = torch.zeros(params.batch_size,params.vocab_size):cuda() --h

   elseif params.TASK == 'qa' then
assert(false)
  model.dsRT[1] = transfer_data(torch.zeros(params.batch_size,params.rnn_size))
  model.dsRT[2] = transfer_data(torch.zeros(params.batch_size,params.rnn_size))

  model.dsRQ[1] = transfer_data(torch.zeros(params.batch_size,params.rnn_size))
  model.dsRQ[2] = transfer_data(torch.zeros(params.batch_size,params.rnn_size))

  model.dsAt[1] = transfer_data(torch.zeros(params.batch_size,params.rnn_size))
  model.dsAt[2] = transfer_data(torch.zeros(params.batch_size,params.rnn_size))
  model.dsAt[3] = transfer_data(torch.zeros(params.batch_size,params.rnn_size)) -- NOTE actually will later have different size

  model.dsAn[1] = transfer_data(torch.zeros(params.batch_size,NUMBER_OF_ANSWER_OPTIONS))
   else
      print(TASK)
      crash()
   end



end

--------------------------------------
--------------------------------------

local function fp(corpus, startIndex, endIndex)
   if params.TASK == 'autoencoding' or params.TASK == 'combined' then
      return autoencoding.fpAutoencoding(corpus, startIndex, endIndex)
   elseif params.TASK == 'langmod' then
      return fpLanguageModelling(corpus, startIndex, endIndex)
   elseif params.TASK == 'qa' then
assert(false)
      return fpQA(corpus, startIndex, endIndex)
   elseif params.TASK == 'phono' then
assert(false)
      return fpPhono(corpus, startIndex, endIndex)
   elseif params.TASK == 'linearization' then
assert(false)
      return fpLinearization(corpus, startIndex, endIndex)
   elseif params.TASK == 'combined-soft' then
      return fpCombinedSoft(corpus, startIndex, endIndex)
   elseif params.TASK == 'combined-q' then
assert(false)
      return combined.fpCombinedQ(corpus, startIndex, endIndex)
   elseif params.TASK == 'neat-qa' then
assert(false)
      return neatQA.fp(corpus, startIndex, endIndex)
   else
assert(false)
      print(params.TASK)
      crash()
   end
end


---------------------------------------
---------------------------------------



local function bp(corpus, startIndex, endIndex)
   if params.TASK == 'autoencoding' then
      return autoencoding.bpAutoencoding(corpus, startIndex, endIndex)
   elseif params.TASK == 'combined' then
      if USE_BASELINE_NETWORK then
         return combined.bpCombined(corpus, startIndex, endIndex)
      else
         return combined.bpCombinedNoBaselineNetwork(corpus, startIndex, endIndex)
      end
   elseif params.TASK == 'langmod' then
      return bpLanguageModelling(corpus, startIndex, endIndex)
   elseif params.TASK == 'qa' then
assert(false)
      return bpQA(corpus, startIndex, endIndex)
   elseif params.TASK == 'phono' then
 assert(false)
     return bpPhono(corpus, startIndex, endIndex)
   elseif params.TASK == 'linearization' then
  assert(false)
    return bpLinearization(corpus, startIndex, endIndex)
   elseif params.TASK == 'combined-soft' then
      return combined.bpCombinedSoft(corpus, startIndex, endIndex)
   elseif params.TASK == 'combined-q' then
 assert(false)
     return combined.bpCombinedQ(corpus, startIndex, endIndex)
   elseif params.TASK == 'neat-qa' then
 assert(false)
     return neatQA.bp(corpus, startIndex, endIndex)
   else
      print(TASK)
  assert(false)
   end
end

require('getParams')

require('nn.UniformLayer')

require('nn.ScalarMult')

require('nn.BlockGradientLayer')


local function tryReadParam(func)
if not pcall(func) then
print("ERROR ")
print(func)
end
end




local function main()




  g_init_gpu({params.gpu_number})

  if params.TASK == 'neat-qa' then
        PRINTING_PERIOD = 11--21 -- 51 -- 51 --5 --21 --21
  else
        PRINTING_PERIOD = 51 --51 --51
  end
  if DOING_EVALUATION_OUTPUT then
     PRINTING_PERIOD = 1
  end
  
  readDict.readDictionary()

  if params.TASK == 'neat-qa' then
     readDict.createNumbersToEntityIDsIndex()
  end


  print("Network parameters:")
  print(params)

  print("setup")
  setup()
  print("setup done")



  local beginning_time = torch.tic()
  local start_time = torch.tic()
  print("Starting training.")
  local numberOfWords = 0
  local counter = 0


  tryReadParam(getLearningRateFromFile)
  tryReadParam(getAttentionFromFile)
  tryReadParam(getAttentionLearningRateFromFile)
  if params.TASK == 'combined-soft' or params.TASK == "combined-q" then
      tryReadParam(getTotalAttentionWeightFromFile)
  end



  print(FIXED_ATTENTION)
  print(params.lr)
  print(params.lr_att)



  for epoch = 1,EPOCHS_NUMBER do
--     print("--------"..epoch)
     if (not DO_TRAINING) and epoch > 1 then
       print("BREAK 301. Not doing training, so only doing the first epoch.")
       break
     end

     readChunks.resetFileIterator()

     epochCounter = epoch

--     print("31316")
     --[[print(files)
     print(#files)
     print(params.batch_size)
     print(#files-params.batch_size+1)]]
     --print(4756)
     --print(hasNextFile())
      --print("316  "..tostring(readChunks.hasNextFile()))

     while readChunks.hasNextFile() do
  
        if ( PERCENTAGE_OF_DATA < (100.0 * (readChunks.corpusReading.currentFile+0.0) / #readChunks.files)) then
          print("AT 313")
          print( (100.0 * (readChunks.corpusReading.currentFile+0.0) / #readChunks.files))
          break
        end


       
     --print(i)
  
      for l = 1, params.batch_size do
         --corpus[l] = readAFile(files[i+l-1])
         if params.TASK == 'langmod' or params.TASK == 'autoencoding' or params.TASK == 'combined' or params.TASK == 'combined-soft' or params.TASK == "combined-q" then
               --print(l)
               --print("339")

             if binning.do_binning then
               assert(params.TASK == "neat-qa", "Do you really want to call binning when doing autoencoding? I'm not sure whether this thing here is intended or not.")
               readChunks.corpus[l] = binning.getNext(readChunks.readNextChunkForBatchItem,l,     (function(x) return (#(x)) end)   )
             else
               readChunks.corpus[l] = readChunks.readNextChunkForBatchItem(l)
             end
             if params.INCLUDE_NUMERICAL_VALUES then -- huhu
                if binning.do_binning then
                  print("THIS PROBABLY DOES NOT WORK CORRECTLY. 344")
                  assert(false)
                end
                  numericalValues.getNumericalValuesForBatchItem(l)
             end
         elseif params.TASK == 'neat-qa' then
           assert(false)
           if binning.do_binning then
             readChunks.corpus[l] = binning.getNext(readFiles.readNextQAItem,l)
           else
             readChunks.corpus[l] = readFiles.readNextQAItem(l)
           end
         else
             crash()
         end
      end



--      print("BEFORE FORWARD")
      --print(readChunks.corpus)
      local perp, actor_output = fp(readChunks.corpus, 1, params.batch_size)
  --    print("DONE FORWARD")
      if params.TASK == 'neat-qa' then
         assert(false)
         numberOfWords = numberOfWords + params.batch_size * neatQA.maximalLengthOccurringInInput[1]
      else
         numberOfWords = numberOfWords + params.batch_size * params.seq_length
      end



      if MAKE_SKIPPING_STATISTICS then
           updateSkippingStatistics(readChunks.corpus)
      end

      if STORE_ATTENTION_ANNOTATION then
         if params.TASK == "neat-qa" then
          assert(false)
          storeAttentionAnnotationQA(readChunks.corpus)
         else
          storeAttentionAnnotation(readChunks.corpus)
         end
      end

      if WRITE_SURPRISAL_SCORES then
          storeSurprisalScores(readChunks.corpus)
      end


      if DOING_EVALUATION_OUTPUT then
          for  l=1, params.batch_size do
              if readChunks.corpus[l][1] == 2 and readChunks.corpus[l][2] == 2 and readChunks.corpus[l][3] == 2 then
                 print(readChunks.corpus[l])
              else
                 evaluationAccumulators.reconstruction_loglikelihood = evaluationAccumulators.reconstruction_loglikelihood + perp[l]
                 if params.TASK == "combined" or params.TASK == "combined-soft"or params.TASK == "combined-q" then
                     evaluationAccumulators.lm_loglikelihood = evaluationAccumulators.lm_loglikelihood + nll_reader[l]
                 end
--                 print("3342   "..files[readChunks.corpusReading.locationsOfLastBatchChunks[l].file])
                 evaluationAccumulators.numberOfTokens = evaluationAccumulators.numberOfTokens + params.seq_length
              end
          end
      end

      if params.TASK == 'autoencoding' or params.TASK == 'neat-qa' then
           meanTotalAtt = 0.8 * meanTotalAtt + 0.2 * torch.sum(totalAttentions) * 1/params.batch_size
      end

      if params.TASK == 'autoencoding' or params.TASK == 'langmod' or params.TASK == 'qa' then
           meanNLL = 0.8 * meanNLL + 0.2 * torch.sum(perp) * 1/params.batch_size
      end

      --print(meanNLL)
      counter = counter + 1

      if  counter % 100 == 0 and TASK == 'qa' then
         qaCorrect = 0
         qaIncorrect = 0
      end


      if counter % PRINTING_PERIOD == 0 then
         print('WORDS '..numberOfWords..'  EPOCH '..epoch..'  '..(100.0 * (readChunks.corpusReading.currentFile+0.0) / #readChunks.files))
         print("WORDS/SEC   "..numberOfWords / torch.toc(start_time))
         local since_beginning = g_d(torch.toc(beginning_time) / 60)


         if params.TASK == 'autoencoding' then
            print("+++++++ "..perp[1]..'  '..meanNLL..'  '..meanTotalAtt..'  '..(params.TOTAL_ATTENTIONS_WEIGHT * meanTotalAtt + meanNLL))
             print(epoch.."  "..corpusReading.currentFile..
               '   since beginning = ' .. since_beginning .. ' mins.')  
            print(probabilityOfChoices[1]..'  '..totalAttentions[1])
            print(experimentNameOut)
            print(params) 
            if probabilityOfChoices[1] ~= probabilityOfChoices[1]  then
               crash()
            end
   
            if MAKE_SKIPPING_STATISTICS then
                printSkippingStatistics()
            end
   
   
            for l = 1, 1 do
               print("....")
               print(perp[l])
               for j=1,params.seq_length do
                  local predictedScores, predictedTokens = torch.min(actor_output[j][l],1)
                  io.write((readDict.chars[getFromData(readChunks.corpus,l,j)]))--..'\n')
                  io.write(" ~ "..readDict.chars[predictedTokens[1]].."  "..math.exp(-predictedScores[1]).."  "..math.exp(-actor_output[j][l][getFromData(readChunks.corpus,l,j)]).."  "..attention_decisions[j][l].."  "..attention_scores[j][l][1])
                  if params.INCLUDE_SURPRISAL_VALUES then
                     io.write("  "..numericalValues.numericalValuesImporter.values[1][j])
                  end
                  io.write("\n")
               end
            end
            --io.output(fileStats)
            fileStats:write((numberOfWords/params.seq_length)..'\t'..perp[1]..'\n')
            fileStats:flush()
            --io.output(stdout)
         elseif params.TASK == 'combined' then
            combined.printStuffForCombined(perp, actor_output, since_beginning, epoch, numberOfWords)
         elseif params.TASK == 'combined-soft' then
            printStuffForCombinedSoft(perp, actor_output, since_beginning, epoch, numberOfWords)
         elseif params.TASK == 'combined-q' then
assert(false)
            printStuffForCombinedQ(perp, actor_output, since_beginning, epoch, numberOfWords)
         elseif params.TASK == 'langmod' then
            printStuffForLangmod(perp, actor_output, since_beginning, epoch, numberOfWords)
         elseif params.TASK == 'qa' then
assert(false)

            printStuffForQA(perp, actor_output, since_beginning, epoch, numberOfWords)
         elseif params.TASK == 'phono' then
assert(false)
            printStuffForPhono(perp, actor_output, since_beginning, epoch, numberOfWords)
         elseif params.TASK == 'linearization' then
assert(false)
           printStuffForLinearization(perp, actor_output, since_beginning, epoch, numberOfWords)
         elseif params.TASK == 'neat-qa' then
assert(false)
           neatQA.printStuff(perp, actor_output, since_beginning, epoch, numberOfWords)
         else
            print(params.TASK)
            crash()
         end


--         if not use_attention_network then
  --          if not pcall(getAttentionFromFile) then
    --           print("ERROR IN FILE ATT")
      --      end
        -- end

         tryReadParam(getLearningRateFromFile)
         tryReadParam(getAttentionFromFile)
         tryReadParam(getAttentionLearningRateFromFile)
         if params.TASK == 'combined-soft' or params.TASK == "combined-q" then
            tryReadParam(getTotalAttentionWeightFromFile)
         end
--            print("ERROR IN FILE LR")
  --       end
    --     if not pcall(getAttentionLearningRateFromFile) then
      --      print("ERROR IN FILE LRATT")
        -- end

         print(FIXED_ATTENTION)
         print(params.lr)
         print(params.lr_att)


      end

      if DO_TRAINING then
--         print("BEFORE TRAINING")
  --       print(readChunks.corpus)
         bp(readChunks.corpus, 1, params.batch_size)
         --checkBackprop(corpus)
      end
      if counter % 33 == 0 then
        cutorch.synchronize()
        collectgarbage()
      end


      if DO_TRAINING and counter % PRINT_MODEL_PERIOD == 0 then
        print("WRITING MODEL...")
        local modelsArray
        if params.TASK == 'autoencoding' or params.TASK == 'combined' then
           local uR, udR = readerRNNs[1]:parameters()
           local uA, udA = actorRNNs[1]:parameters()
           local uRA, udRA = attentionNetworks[1]:parameters()
           modelsArray = {params,(numberOfWords/params.seq_length),uR, udR, uA, udA, uRA, udRA, reader_c[0], reader_h[0]}
           if USE_BIDIR_BASELINE and bidir_baseline ~= nil then
              local uB, udB = bidir_baseline:parameters()
--              print(uB)
              table.insert(modelsArray, uB)
              table.insert(modelsArray, udB)
           end
         elseif params.TASK == 'autoencoding' or params.TASK == 'combined-soft' or params.TASK == 'combined-q' then
           local uR, udR = readerRNNs[1]:parameters()
           local uA, udA = actorRNNs[1]:parameters()
           modelsArray = {params,(numberOfWords/params.seq_length),uR, udR, uA, udA, reader_c[0], reader_h[0]}
         elseif params.TASK == 'langmod' then
           local uA, udA = actorRNNs[1]:parameters()
           modelsArray = {params,(numberOfWords/params.seq_length),uA, udA, actor_c[0], actor_h[0]}
         elseif params.TASK == 'phono' then
           savePhono(numberOfWords)
         elseif params.TASK == 'linearization' then
           saveLinearization(numberOfWords)
         elseif params.TASK == 'qa' then
           local uAn, udAn = answerNetwork:parameters()
           local uAt, udAt = attentionNetworks[1]:parameters()
           local uRT, udRT = readerTRNNs[1]:parameters()
           local uRQ, udRQ = readerQRNNs[1]:parameters()

  -- NOTE when changing the model also change which initial vectors we store

  --TODO why are we storing the derivatives?
           modelsArray = {params,(numberOfWords/params.seq_length),uAn, udAn, uAt, udAt, uRT, udRT, uRQ, udRQ, readerQ_c[0], readerQ_h[0]}
         elseif params.TASK == 'neat-qa' then
          --     local params2, sentencesRead, = unpack(torch.load(BASE_DIRECTORY.."/model-"..fileToBeLoaded, "binary"))
if neatQA.fullModel then

           local SparamxF, SparamdxF = neatQA.fullModel:parameters()
--           local readerCStart = reader_c[0]
  --         local readerHStart = reader_h[0]
           
           modelsArray = {params,(numberOfWords/params.seq_length),SparamxF, SparamdxF}   

elseif forward_network then

           modelsArray = {params,(numberOfWords/params.seq_length)}

else


           local SparamxR, SparamdxR = readerRNNs[1]:parameters()
           local SparamxA, SparamdxA = actor_core_network:parameters()
           local SparamxRA, SparamdxRA = attentionNetwork:parameters()
           local readerCStart = reader_c[0]
           local readerHStart = reader_h[0]
           
           modelsArray = {params,(numberOfWords/params.seq_length),SparamxR, SparamdxR, SparamxA, SparamdxA, SparamxRA, SparamdxRA, readerCStart, readerHStart}


end

           if USE_BIDIR_BASELINE and bidir_baseline ~= nil then
             --crash()
             local uB, udB = bidir_baseline:parameters()

             table.insert(modelsArray, uB)
             table.insert(modelsArray, udB)
           end
         else
           crash()
         end
         if modelsArray ~= nil then
            torch.save(BASE_DIRECTORY..'/model-'..experimentNameOut, modelsArray, "binary")
         end
--print(BASE_DIRECTORY..'/model-'..experimentNameOut)
--crash()
      end

      if (not use_attention_network) and meanNLL < NLL_TO_CHANGE_ATTENTION then -- or (numberOfWords/(params.batch_size * params.seq_length) % 5000 == 0)
         print("CHANGE ATTENTION")
         meanNLL = 10 * NLL_TO_CHANGE_ATTENTION -- to prevent the attention from going down to the base immediately
         FIXED_ATTENTION = BASE_ATTENTION + 0.9 * (FIXED_ATTENTION-BASE_ATTENTION)
      end


      if DOING_EVALUATION_OUTPUT then
            print(evaluationAccumulators.reconstruction_loglikelihood.."&\n"..evaluationAccumulators.lm_loglikelihood.."\n") 
      end


      --if numberOfWords/(5*params.batch_size) % 50000 == 0 then
      --   use_attention_network = true
      --   train_attention_network = true
      --end

    end
  end
  --run_test()
  print("Training is over.")


    if DOING_EVALUATION_OUTPUT then
             PERP_ANNOTATION_FILE = DATASET_DIR.."/annotation/perp-"..experimentNameOut..".txt"

             local fileOut = io.open(PERP_ANNOTATION_FILE, "w")
             print(PERP_ANNOTATION_FILE)
             tokenCountForLM = 49/50 * evaluationAccumulators.numberOfTokens
             fileOut:write("REC "..(evaluationAccumulators.reconstruction_loglikelihood) .."\n".."LM  "..(evaluationAccumulators.lm_loglikelihood).."\n".."REC "..(evaluationAccumulators.reconstruction_loglikelihood/evaluationAccumulators.numberOfTokens) .."\n".."LM  "..(evaluationAccumulators.lm_loglikelihood/tokenCountForLM) .."\n".."REC "..math.exp(evaluationAccumulators.reconstruction_loglikelihood/evaluationAccumulators.numberOfTokens) .."\n".."LM  "..math.exp(evaluationAccumulators.lm_loglikelihood/tokenCountForLM).."\n") 
             fileOut:close()
      end




  fileStats:close()



end

if (not OVERWRITE_MODEL) and  (lfs.attributes(BASE_DIRECTORY..'/model-'..experimentNameOut) ~= nil) then
   print("MODEL EXISTS, ABORTING")
else
  if (lfs.attributes(BASE_DIRECTORY..'/model-'..experimentNameOut) ~= nil) and OVERWRITE_MODEL then
     print("OVERWRITE MODEL")
  end
   main()
end
