attention = {}

require('nn.BernoulliLayer')

function attention.createAttentionNetworkOneHot()
   local x = nn.Identity()()
   local y = nn.Identity()()
   local x2h = nn.LookupTable(params.vocab_size, params.rnn_size)(x)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)
   local hidden
   local surprisal
      hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module
      module = nn.gModule({x, y},
                                      {attention})
   module:getParameters():uniform(-params.init_weight, params.init_weight)
   return transfer_data(module)
end


attention.ABLATE_INPUT = false
attention.ABLATE_STATE = false
attention.ABLATE_SURPRISAL = false


if string.match(params.ablation, 'i') then
  attention.ABLATE_INPUT = true
end
if string.match(params.ablation, 'r') then
  attention.ABLATE_STATE = true
end
if string.match(params.ablation, 's') then
  attention.ABLATE_SURPRISAL = true
end

--[[print(string.match(params.ablation, 's'))
print(string.match(params.ablation, 'r'))
print(string.match(params.ablation, 'i'))]]


print("ABLATION INP STATE SURP")
print(attention.ABLATE_INPUT)
print(attention.ABLATE_STATE)
print(attention.ABLATE_SURPRISAL)



function attention.createAttentionNetworkEmbeddingsSurprisalWithBaseline()
   assert(false)
   if not (params.TASK == 'combined') then
      crash()
   end
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTable(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local surprisal = nn.Identity()()

   

   -- ABLATION OF INPUT
   if attention.ABLATE_INPUT then
     xemb = nn.MulConstant(0)(xemb)
   end

   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)

   -- ABLATION OF STATE
   if attention.ABLATE_STATE then
      y2h = nn.MulConstant(0)(y2h)
   end

   local z2h = nn.Linear(1, params.rnn_size)(surprisal)

   -- ABLATION OF SURPRISAL
   if attention.ABLATE_SURPRISAL then
      z2h = nn.MulConstant(0)(z2h)
   end

   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h, z2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()

   local baseline = nn.Linear(params.rnn_size, 1)(hidden)


   local module = nn.gModule({x, y, surprisal},
                                      {attention, baseline})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end


-- this is the function used in the EMNLP experiments
function attention.createAttentionNetworkEmbeddingsSurprisal()
   print('attention.createAttentionNetworkEmbeddingsSurprisal')
   if not (params.TASK == 'combined') then
      crash()
   end
   if USE_BASELINE_NETWORK then
      return attention.createAttentionNetworkEmbeddingsSurprisalWithBaseline()
   end
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local surprisal = nn.Identity()()

   

   -- ABLATION OF INPUT
   if attention.ABLATE_INPUT then
     xemb = nn.MulConstant(0)(xemb)
   end

   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)

   -- ABLATION OF STATE
   if attention.ABLATE_STATE then
      y2h = nn.MulConstant(0)(y2h)
   end

   local z2h = nn.Linear(1, params.rnn_size)(surprisal)

   -- ABLATION OF SURPRISAL
   if attention.ABLATE_SURPRISAL then
      z2h = nn.MulConstant(0)(z2h)
   end

   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h, z2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module = nn.gModule({x, y, surprisal},
                                      {attention})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end


-- this is a bilinear version of the function used in the EMNLP experiments, written in Feb 2017
function attention.createAttentionNetworkEmbeddingsSurprisalBilinear()
   print('attention.createAttentionNetworkEmbeddingsSurprisalBilinear')
   if not (params.TASK == 'combined') then
      crash()
   end
   if USE_BASELINE_NETWORK then
      assert(false)
      return attention.createAttentionNetworkEmbeddingsSurprisalWithBaseline()
   end
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   xemb = nn.Dropout(0.1)(xemb)
   local y = nn.Identity()()
   local surprisal = nn.Identity()()
  

   -- ABLATION OF INPUT
   if attention.ABLATE_INPUT then
     xemb = nn.MulConstant(0)(xemb)
   end

   -- surprisal
   local z2h = nn.Linear(1, 100)(surprisal)
   -- ABLATION OF SURPRISAL
   if attention.ABLATE_SURPRISAL then
      z2h = nn.MulConstant(0)(z2h)
   end

   -- recurrent state
   local y2h = nn.Linear(params.rnn_size, 100)(y)
   -- ABLATION OF STATE
   if attention.ABLATE_STATE then
      y2h = nn.MulConstant(0)(y2h)
   end


   local hidden = nn.ReLU()(nn.CAddTable()({y2h, z2h}))
   hidden = nn.Dropout(0.1)(hidden)
 

--   local x2h = nn.Linear(params.embeddings_dimensionality, 100)(xemb)

   -- biaffine
   local bilinear = nn.Bilinear(params.embeddings_dimensionality, 100, 1)({xemb,hidden})
   local affineInput = nn.Linear(params.embeddings_dimensionality, 1)(xemb)
   local affineHidden = nn.Linear(100, 1)(hidden)

   bilinear = nn.CAddTable()({bilinear, affineInput, affineHidden})

   local attention = nn.Sigmoid()(bilinear) --nn.Log()
   local module = nn.gModule({x, y, surprisal},
                                      {attention})
  --module:getParameters():uniform(-params.init_weight, params.init_weight)
-- Xavier initialization
  parameters, gradparameters = module:parameters()
   for i=1, #parameters do
     local epsilon = math.sqrt(6.0/torch.LongTensor(parameters[i]:size()):sum())
     parameters[i]:uniform(-epsilon, epsilon)
     gradparameters[i]:zero()
   end
  return transfer_data(module)
end








function attention.createAttentionBilinear()
   assert(params.TASK == 'neat-qa')
   assert(false)
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTableMaskZero(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()

   xemb = nn.Dropout(0.1)(xemb)
   y2 = nn.Dropout(0.1)(y)
   local intermediateMLP = nn.Linear(params.rnn_size,100)(y2)
   intermediateMLP = nn.ReLU()(intermediateMLP)
   intermediateMLP = nn.Dropout(0.1)(intermediateMLP)
   local bilinear = nn.Bilinear(params.embeddings_dimensionality, 100, 1)({xemb,intermediateMLP})

bilinear = nn.AddConstant(3.0)(bilinear)

   local attention = nn.Sigmoid()(bilinear)
--attention = nn.PrintLayer("bilinear",1,true)(bilinear)


   local decisionsAndProbs = nn.BernoulliLayer(params.batch_size,1)(attention)
--   local attendedInput = nn.CMulTable()(decisions,
   local decisions = nn.SelectTable(1)(decisionsAndProbs)
   local probs = nn.SelectTable(2)(decisionsAndProbs)
   local attendedInputTensors = nn.CMulTable()({x,decisions})

local blockedAttention = nn.BlockGradientLayer(params.batch_size,1)(attention)
local blockedDecisions = nn.BlockGradientLayer(params.batch_size,1)(decisions)
local blockedInput = nn.BlockGradientLayer(params.batch_size,1)(attendedInputTensors)


   local module = nn.gModule({x, y},{blockedAttention,blockedDecisions,probs,blockedInput})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end




function attention.createAttentionNetworkEmbeddings()
   if params.TASK == 'combined' then
      if(false) then
        -- replication September 2017
        return attention.createAttentionNetworkEmbeddingsSurprisalBilinear()
      else
        -- EMNLP experiments
        return attention.createAttentionNetworkEmbeddingsSurprisal()
      end
   elseif params.TASK == 'neat-qa' then
      assert(false)
      return attention.createAttentionBilinear()
   end
   local x = nn.Identity()()
   local xemb = nn.BlockGradientLayer(params.batch_size, params.embeddings_dimensionality)(nn.LookupTable(params.vocab_size,params.embeddings_dimensionality)(x))
   local y = nn.Identity()()
   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)
   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module = nn.gModule({x, y},
                                      {attention})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end



function attention.createAttentionNetwork()
  if params.ATTENTION_WITH_EMBEDDINGS then
          return attention.createAttentionNetworkEmbeddings()
  else
       assert(false)
       return attention.createAttentionNetworkOneHot()
  end
end




