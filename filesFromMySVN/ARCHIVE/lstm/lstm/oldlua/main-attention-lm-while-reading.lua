--- derived from mini-batch-back-flexible-apparentlyslow-but-everything, derived from Zaremba's LSTM code



--[[

main-attention.lua 2 pg50665.txt false 20 30 100 10000 3 false


-]]




--[[ TODO

-- train also to autoencode prefixes for warming up

--- maybe word-embeddings better be shared between attention and reader

-- train initial c, h

-- TODO word-embeddings dimension s.t. different


* ???are there different parameters for different indices within a batch?


+++ GOOD MODELS

.. ~/local/bin/th main-attention.lua 1 pg50665.txt false 30 50 500 10000 3 false 0.2 500: attention at 0.91

.. ~/local/bin/th main-attention.lua 3 pg50665.txt false 20 100 1000 10000 3 false 0.2 200 (not so great)
]]



local ok,cunn = pcall(require, 'fbcunn')
if not ok then
    ok,cunn = pcall(require,'cunn')
    if ok then
        print("warning: fbcunn not found. Falling back to cunn") 
        LookupTable = nn.LookupTable
    else
        print("Could not find cunn or fbcunn. Either is required")
        os.exit()
    end
else
    deviceParams = cutorch.getDeviceProperties(1)
    cudaComputeCapability = deviceParams.major + deviceParams.minor/10
    LookupTable = nn.LookupTable
end
require('nngraph')
require('base')
local ptb = require('data')


local NLL_TO_CHANGE_ATTENTION = 30

local meanNLL = 10000
local meanNLLReader = 10000
local meanTotalAtt = 0

gpu_number = {arg[1]+0}

local corpus_name = arg[2] --"pg50665.txt" --"pg50665.txt" --"hlm.txt"

if arg[3] == 'false' then
   arg[3] = false
end
local LOAD = arg[3] and true



local REWARD_DIFFERENCE_SCALING = 1

local FIXED_ATTENTION = 0.95
local BASE_ATTENTION = 0.6


-- 1 pg50665.txt 20 30 100 8000
local params = {batch_size=arg[4]+0,
                seq_length=arg[5]+0,
                --layers=2,
                --decay=2,
                rnn_size=arg[6]+0,
                --dropout=0,
                init_weight=0.05,
                lr=((arg[10]+0) + 0.0), -- 0.01
                vocab_size=arg[7]+0,
                --max_epoch=4,
                --max_max_epoch=13,
                max_grad_norm=5,
                lr_att = 0.01,
                lr_momentum = 0.9,
                embeddings_dimensionality = arg[11] + 0}

local TOTAL_ATTENTIONS_WEIGHT = arg[8]+0



local use_attention_network
local train_attention_network
local train_autoencoding

if arg[9] == 'false' then
   arg[9] = false
end
if arg[9] then
  use_attention_network = true
  train_attention_network = true
  train_autoencoding = false
else
  use_attention_network = false
  train_attention_network = false
  train_autoencoding = true
end   
if train_attention_network and (not use_attention_network) then
   crash()
end

-- arg[10] is learning rate
-- embeddings_dimensionality = arg[11]

local experimentName = "pg-test-"..params.seq_length.."-"..params.rnn_size.."-"..params.lr.."-"..params.embeddings_dimensionality
local experimentNameOut = experimentName

if LOAD then
  experimentNameOut = experimentNameOut.."-R-"..TOTAL_ATTENTIONS_WEIGHT
end

local fileStats = io.open(experimentNameOut..'-stats', 'w')

print(fileStats)

-- Trains 1h and gives test 115 perplexity.
--[[local params = {batch_size=20,
                seq_length=20,
                layers=2,
                decay=2,
                rnn_size=10,
                dropout=0,
                init_weight=0.5,
                lr=0.01,
                vocab_size=10,
                max_epoch=4,
                max_max_epoch=13,
                max_grad_norm=5}]]

--[[local params = {batch_size=20,
                seq_length=20,
                layers=2,
                decay=2,
                rnn_size=200,
                dropout=0,
                init_weight=0.1,
                lr=1,
                vocab_size=10000,
                max_epoch=4,
                max_max_epoch=13,
                max_grad_norm=5}]]

local function transfer_data(x)
  return x:cuda()
end

local state_train, state_valid, state_test
local model = {}
local paramx, paramdx



local corpusDir = "/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training/" --"/disk/scratch2/s1582047/gutenberg/out/" --"/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training/"


function createListOfFiles()
    local files = {}
    io.input("/disk/scratch2/s1582047/listOfFilesToRead.txt") --gutenberg
    t = io.read("*all")
    for line in string.gmatch(t, "[^\n]+") do
       table.insert(files, line)
    end
    return files
end


-- READ THE DICTIONARY

function readDictionary()
   io.input("/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/num2Chars") --"disk/scratch2/s1582047/gutenberg/num2Chars") --)
   t = io.read("*all")
   for line in string.gmatch(t, "[^\n]+") do
     local isInSecond = false
     local character
     if line:len() > 1 then
       for x in string.gmatch(line, "[^ ]+") do
          --print(x)
          character = x
          if isInSecond == true then
            break
          end
          isInSecond = true
       end
       --print(character.."..."..(#chars).."   "..line)
       table.insert(chars, character)
     else
       print("IGNORED "..line)
     end
   end

   -- HULLU
   for i = 1, 15000 do
      chars[i] = i..""
   end
   


   if params.vocab_size > #chars then
      print(#chars)
      crash()
   end
end


corpus = {}



function readAFile(fileName)
   local dataPointFromFile = {}
   io.input(corpusDir..fileName)
   t = io.read("*all")
   local counter = 0
   for line in string.gmatch(t, "[^\n]+") do
     counter = counter+1
     lineList = {}
     for token in string.gmatch(line, "[0-9]+") do
        table.insert(dataPointFromFile, math.min(token+1, params.vocab_size))
     end
   end
   --print("....")
   --print(fileName)
   --for y = 1, 10 do
   --   print(chars[dataPointFromFile[y]].."  "..dataPointFromFile[y].."  "..chars[dataPointFromFile[y]-1].."  "..chars[dataPointFromFile[y]+1])
   --end

   return dataPointFromFile
end



chars = {}



-- NOTE THAT THE FILES COME FROM PYTHON AND WILL BE ZERO-INDEXED
function readCorpus() --as a double storage
   io.input(corpus_name..".num.b")
   t = io.read("*all")
   local counter = 0
   for line in string.gmatch(t, "[^\n]+") do
     counter = counter+1
     lineList = {}
     table.insert(corpus, lineList)
     for token in string.gmatch(line, "[0-9]+") do
        table.insert(lineList, math.min(token+1.0, params.vocab_size))
     end

    --print(line)

   end
   --corpus = torch.Tensor(corpusList)

   io.input(corpus_name..".charnums.txt")
   t = io.read("*all")

   for line in string.gmatch(t, "[^\n]+") do
     table.insert(chars, line)
   end

   if params.vocab_size > #chars then
      print(#chars)
      crash()
   end

--print(chars)

end



--readCorpus()

print("DONE READING CORPUS")






local function lstm(x, prev_c, prev_h)


  --[[local a = nn.Linear(params.rnn_size, params.rnn_size)(x)
  local b = nn.Linear(params.rnn_size, params.rnn_size)(prev_c)
  local c = nn.Linear(params.rnn_size, params.rnn_size)(prev_h)

  local next_c = nn.Sigmoid()(nn.CAddTable()({a, b, c}))
  local next_h = nn.Sigmoid()(nn.CAddTable()({a, b, c}))
return next_c, next_h]]



  -- Calculate all four gates in one go
  local i2h = nn.Linear(params.embeddings_dimensionality, 4*params.rnn_size)(x)
  local h2h = nn.Linear(params.rnn_size, 4*params.rnn_size)(prev_h)
  local gates = nn.CAddTable()({i2h, h2h})
  
  -- Reshape to (batch_size, n_gates, hid_size)
  -- Then slize the n_gates dimension, i.e dimension 2
  local reshaped_gates =  nn.Reshape(4,params.rnn_size)(gates)
  local sliced_gates = nn.SplitTable(2)(reshaped_gates)
  
  -- Use select gate to fetch each gate and apply nonlinearity
  local in_gate          = nn.Sigmoid()(nn.SelectTable(1)(sliced_gates))
  local in_transform     = nn.Tanh()(nn.SelectTable(2)(sliced_gates))
  local forget_gate      = nn.Sigmoid()(nn.SelectTable(3)(sliced_gates))
  local out_gate         = nn.Sigmoid()(nn.SelectTable(4)(sliced_gates))

  local next_c           = nn.CAddTable()({
      nn.CMulTable()({forget_gate, prev_c}),
      nn.CMulTable()({in_gate,     in_transform})
  })
  local next_h           = nn.CMulTable()({out_gate, nn.Tanh()(next_c)})

  return next_c, next_h
end


local function createAttentionNetwork()
   local x = nn.Identity()()
   local xemb = LookupTable(params.vocab_size,params.embeddings_dimensionality)(x)
   local y = nn.Identity()()
   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)
   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module = nn.gModule({x, y},
                                      {attention})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end


local function create_network(withOutput)
  local x                = nn.Identity()()
  local prev_c           = nn.Identity()()
  local prev_h           = nn.Identity()()
  local i                = LookupTable(params.vocab_size,params.embeddings_dimensionality)(x)
  local next_s           = {}
  local next_c, next_h = lstm(i, prev_c, prev_h)
  local module
  if withOutput  then
        local h2y              = nn.Linear(params.rnn_size, params.vocab_size)(next_c)
        local output = nn.MulConstant(-1)(nn.LogSoftMax()(h2y))
-- TODO softmax
      module = nn.gModule({x, prev_c, prev_h},
                                      {next_c, next_h, output})
  else
      module = nn.gModule({x, prev_c, prev_h},
                                      {next_c, next_h})
  end
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end


--[[ why do we have to clone stuff???]]
local function setup()
  print("Creating a RNN LSTM network.")



  -- initialize data structures

  model.sR = {}
  model.dsR = {}
  model.dsA = {}
  model.start_sR = {}
  for j = 0, params.seq_length do
    model.sR[j] = transfer_data(torch.zeros(params.batch_size, params.rnn_size))
  end


  model.dsR[1] = transfer_data(torch.zeros(params.rnn_size))
  model.dsR[2] = transfer_data(torch.zeros(params.rnn_size))
  model.dsR[3] = transfer_data(torch.zeros(params.rnn_size)) -- NOTE actually will later have different size

  model.dsA[1] = transfer_data(torch.zeros(params.rnn_size))
  model.dsA[2] = transfer_data(torch.zeros(params.rnn_size))
 model.dsA[3] = transfer_data(torch.zeros(params.rnn_size)) -- NOTE actually will later have different size



  reader_c ={}
  reader_h = {}

  actor_c ={[0] = torch.CudaTensor(params.rnn_size)}
  actor_h = {[0] = torch.CudaTensor(params.rnn_size)}

  reader_c[0] = torch.CudaTensor(params.batch_size,params.rnn_size):zero() --TODO they have to be intiialized decently
  reader_h[0] = torch.CudaTensor(params.batch_size,params.rnn_size):zero()

  attention_decisions = {}
  attention_scores = {}
  for i=1, params.seq_length do
     attention_decisions[i] = torch.CudaTensor(params.batch_size)
     attention_scores[i] = torch.CudaTensor(params.batch_size,1)
  end

  probabilityOfChoices = torch.FloatTensor(params.batch_size)
  totalAttentions = torch.FloatTensor(params.batch_size) -- apparently using CudaTensor would cause a noticeable slowdown...?!
   nll = torch.FloatTensor(params.batch_size)
   nllReader = torch.FloatTensor(params.batch_size)



  ones = torch.ones(params.batch_size):cuda()
  rewardBaseline = 0


   if not LOAD then
     -- READER
     local reader_core_network = create_network(true)
     paramxR, paramdxR = reader_core_network:getParameters()

     readerRNNs = {}

     for i=1,params.seq_length do
        readerRNNs[i] = g_clone(reader_core_network)
     end

     -- ACTOR
     local actor_core_network = create_network(true)
     paramxA, paramdxA = actor_core_network:getParameters()

     actorRNNs = {}

     for i=1,params.seq_length do
        actorRNNs[i] = g_clone(actor_core_network)
     end

     -- ATTENTION

     local attentionNetwork = createAttentionNetwork()
     paramxRA, paramdxRA = attentionNetwork:getParameters()

     attentionNetworks = {}

     for i=1,params.seq_length do
        attentionNetworks[i] = g_clone(attentionNetwork)
     end
  elseif true then


-- TODO add params
     local params2, sentencesRead, SparamxR, SparamdxR, SparamxA, SparamdxA, SparamxRA, SparamdxRA, readerCStart, readerHStart = unpack(torch.load("/disk/scratch2/s1582047/model-"..experimentName, "binary"))

    print(params2)


------
     local reader_core_network = create_network(false)
     local network_params, network_gradparams = reader_core_network:parameters()
     for j=1, #SparamxR do
           --print(reader_core_network:parameters()[j])
           --print(SparamxR[j])
           network_params[j]:set(SparamxR[j])
           network_gradparams[j]:set(SparamxR[j])
     end

     paramxR, paramdxR = reader_core_network:getParameters()

     readerRNNs = {}

     for i=1,params.seq_length do
        readerRNNs[i] = g_clone(reader_core_network)
     end

------
     -- ACTOR
     local actor_core_network = create_network(true)
     network_params, network_gradparams = actor_core_network:parameters()
     for j=1, #SparamxA do
           network_params[j]:set(SparamxA[j])
           network_gradparams[j]:set(SparamdxA[j])
     end


     paramxA, paramdxA = actor_core_network:getParameters()

     actorRNNs = {}

     for i=1,params.seq_length do
        actorRNNs[i] = g_clone(actor_core_network)
     end

     -- ATTENTION

     local attentionNetwork = createAttentionNetwork()
     --print(SparamxRA)
     --print(#SparamxRA)

     network_params, network_gradparams = attentionNetwork:parameters()

     for j=1, #SparamxRA do
           --print(j)
           network_params[j]:set(SparamxRA[j])
           network_gradparams[j]:set(SparamdxRA[j])
           --print(attentionNetwork:parameters()[j])
           --print(SparamxRA[j])
     end
--attentionNetwork:parameters()[1]:set(SparamxRA[1])
     --print(SparamxRA[1])
     --print(attentionNetwork:parameters()[1])
     --crash()

attentionNetwork:getParameters():uniform(-params.init_weight, params.init_weight) -- hullu huhu

     paramxRA, paramdxRA = attentionNetwork:getParameters()

  --print(paramxRA)
  --crash()

     
     attentionNetworks = {}

     for i=1,params.seq_length do
        attentionNetworks[i] = g_clone(attentionNetwork)
     end


------

     -- for safety zero initialization when later using momentum
     paramdxRA:zero()





     print("Sequences read by model "..sentencesRead)

     reader_c[0] = readerCStart
     reader_h[0] = readerHStart

     --attentionNetworks[1]:parameters()[1][1][1] = 7.654 --hullu huhu

     --print(attentionNetworks[1]:getParameters())
     --print(attentionNetworks[2]:getParameters())
     --crash()



  --paramxR2, paramdxR2 = reader_core_network:getParameters()
  --print(paramxR - paramxR2)
  --print(paramdxR - paramdxR2)
  --paramxR, paramdxR = reader_core_network:getParameters()
  --print()
  --print()
--crash()
  --paramxA, paramdxA = actor_core_network:getParameters()
  --paramxRA, paramdxRA = attentionNetwork:getParameters()




  else
     print(paramxA)
     local sentencesRead, SparamxA, SparamdxA, SparamxR, SparamdxR, SparamxRA, SparamdxRA, readerCStart, readerHStart = unpack(torch.load("/disk/scratch2/s1582047/model-"..experimentName, "binary"))
     
     print("Sequences read by model "..sentencesRead)
     for i=1,params.seq_length do
         local cloneParams, cloneGradParams = readerRNNs[i]:parameters()
         --print(SparamxA)
         print(cloneParams)
         for j=1, #SparamxA do
           cloneParams[j]:set(SparamxA[j])
           cloneGradParams[j]:set(SparamdxA[j])
         end
     end
     
     for i=1,params.seq_length do
         local cloneParams, cloneGradParams = actorRNNs[i]:parameters()
         for j=1, #SparamxR do
           cloneParams[j]:set(SparamxR[j])
           cloneGradParams[j]:set(SparamdxR[j])
         end
     end
     
     for i=1,params.seq_length do
         local cloneParams, cloneGradParams = attentionNetworks[i]:parameters()
         for j=1, #SparamxRA do
           cloneParams[j]:set(SparamxRA[j])
           cloneGradParams[j]:set(SparamdxRA[j])
         end
     end
     reader_c[0] = readerCStart
     reader_h[0] = readerHStart

     attentionNetworks[1]:parameters()[1][1][1] = 7.654 --hullu huhu

     print(attentionNetworks[1]:getParameters())
     print(attentionNetworks[2]:getParameters())
     crash()



  --paramxR2, paramdxR2 = reader_core_network:getParameters()
  --print(paramxR - paramxR2)
  --print(paramdxR - paramdxR2)
  paramxR, paramdxR = reader_core_network:getParameters()
  --print()
  --print()
--crash()
  paramxA, paramdxA = actor_core_network:getParameters()

  paramxRA, paramdxRA = attentionNetwork:getParameters()




   end

   updatesR = torch.zeros(paramxR:size())
   updatesA = torch.zeros(paramxA:size())
   updatesRA = torch.zeros(paramxRA:size())


end





local function reset_state(state)

end

-- TODO make more efficient by filling zeros in place with :zero?
local function reset_ds()
    model.dsR[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    model.dsR[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h
    model.dsR[3] = torch.zeros(params.batch_size,params.vocab_size):cuda() --h


    model.dsA[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    model.dsA[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h
    model.dsA[3] = torch.zeros(params.batch_size,params.vocab_size):cuda() --h



end

local function getFromData(data, index, token)
              if #(data[index]) >= token then
                  return data[index][token]
              else
                  return 1
              end
end


local function buildInputTensors(data, startIndex, endIndex)
    local inputTensors = {}
    for token=0, params.seq_length do
      local inputTensor = torch.CudaTensor(params.batch_size)
      for index=startIndex,endIndex do
           if token==0 then
              inputTensor[index-startIndex+1] = 1
           else
              inputTensor[index-startIndex+1] = getFromData(data,index,token)
           end
      end
      inputTensors[token] = inputTensor
    end
    --print(inputTensors)
    return inputTensors
end


local function makeAttentionDecisions(i, inputTensor)
   local attendedInputTensor = torch.CudaTensor(params.batch_size):zero()
      --attention_scores[i] = torch.CudaTensor(params.batch_size)
      if use_attention_network then 
           attention_scores[i] = attentionNetworks[i]:forward({inputTensor, reader_c[i-1]})
           --print(attention_scores[i])
      else
           attention_scores[i]:fill(FIXED_ATTENTION)
      end
   for item=1, params.batch_size do
      local dice = torch.uniform()
      --print(attention_scores[item][i])
      if dice > attention_scores[i][item][1] then
         attention_decisions[i][item] = 0
         --print(probabilityOfChoices)
         probabilityOfChoices[item] = probabilityOfChoices[item] * (1-attention_scores[i][item][1])
      else
         attention_decisions[i][item] = 1
         attendedInputTensor[item] = inputTensor[item]
         probabilityOfChoices[item] = probabilityOfChoices[item] * attention_scores[i][item][1]
      end
      totalAttentions[item] = totalAttentions[item] + attention_decisions[i][item]
   end
   --print(attendedInputTensor)
   --print(inputTensor)
   --print(attention_scores)
   return attendedInputTensor, probability
end


local function fp(corpus, startIndex, endIndex)
  --data[0] = 1
  -- Reader
  probabilityOfChoices:fill(1)
  totalAttentions:fill(0)
  reader_output = {}
  nllReader:zero()
  local inputTensors = buildInputTensors(corpus, startIndex, endIndex)
  for i=1, params.seq_length do
     inputTensor = inputTensors[i]

     -- make attention decisions
     local attendedInputTensor, probability = makeAttentionDecisions(i, inputTensor)
     --print(attendedInputTensor)
--attendedInputTensor = torch.CudaTensor(params.batch_size):zero() --hullu
     --print(inputTensor)
      
      reader_c[i], reader_h[i], reader_output[i] = unpack(readerRNNs[i]:forward({attendedInputTensor, reader_c[i-1], reader_h[i-1]}))
      if i < params.seq_length then
         for item=1, params.batch_size do
           nllReader[item] = nllReader[item] + reader_output[i][item][getFromData(corpus,startIndex+ item - 1,i+1)] -- TODO
         end
      end
  end


 
  actor_c[0] = reader_c[params.seq_length] 
  actor_h[0] = reader_h[params.seq_length] 

  --print(reader_h[20])

  nll:zero()
  actor_output = {}
  for i=1, params.seq_length do
     inputTensor = inputTensors[i-1]
     --inputTensor = torch.CudaTensor(params.batch_size):zero() --hullu
     actor_c[i], actor_h[i], actor_output[i] = unpack(actorRNNs[i]:forward({inputTensor, actor_c[i-1], actor_h[i-1]}))
     for item=1, params.batch_size do
        nll[item] = nll[item] + actor_output[i][item][getFromData(corpus,startIndex+ item - 1,i)] -- TODO
     end
    --print(actor_c[i])
    --print(actor_h[i])
  end

  return nll, actor_output
end


local function checkBackprop(data)
   --local loss, _ = fp(data)
   --print("LOSS "..loss)
   for i = 1,paramxA:size(1) do
       print("A "..i)
--      for j = 1,paramdxA[i]:dim() do
          paramxA[i] = paramxA[i] + 0.0001
          local lossNew, _ = fp(data)
          local deriv = (lossNew - loss) * 1/0.0001
          print((deriv - paramdxA[i]).." :: "..deriv.."  "..paramdxA[i])
          paramxA[i] = paramxA[i] - 0.0001
   end

assert(paramxR:size(1) == paramdxR:size(1), "..")
assert(paramxA:size(1) == paramdxA:size(1), "..")
   for i = 1,paramxR:size(1) do
       print("R "..i)
--      for j = 1,paramdxA[i]:dim() do
          paramxR[i] = paramxR[i] + 0.0001
          local lossNew, _ = fp(data)
          local deriv = (lossNew - loss) * 1/0.0001
          print((deriv - paramdxR[i]).." :: "..deriv.."  "..paramdxR[i])
          paramxR[i] = paramxR[i] - 0.0001
   end

end


local function buildGradientsOfProbOutputs(dsAThird, corpus, startIndex, endIndex, tokenIndex)
      for index=startIndex,endIndex do
           if tokenIndex==0 then
               dsAThird[index - startIndex + 1][1] = 1
           else
               dsAThird[index - startIndex + 1][getFromData(corpus,index,tokenIndex)] = 1
           end
      end
    --print(dsAThird)
end

local function bp(corpus, startIndex, endIndex)
   --local loss, _ = fp(data)
   --print("LOSS1 "..loss)


  paramdxR:zero()
  paramdxA:zero()
  --paramdxRA:zero() -- will be dealt with by momentum stuff

  -- MOMENTUM
  --print(paramdxRA)
  paramdxRA:mul(params.lr_momentum / (1-params.lr_momentum))
  --print(paramdxRA)
  --print("...")


  reset_ds()
  --print(model.dsA)



  buildGradientsOfProbOutputs(model.dsA[3], corpus, startIndex, endIndex, params.seq_length)

    --[[print("***********")
   print(startIndex)
   print(endIndex)
   print(model.dsA[3])]]

  --model.dsA[3][1][corpus[startIndex][params.seq_length]] = 1 --TODO
  --model.dsA[3][2][corpus[startIndex+1][params.seq_length]] = 1 --TODO


   --local loss, _ = fp(data)
   --print("LOSS2 "..loss)

  local inputTensors = buildInputTensors(corpus, startIndex, endIndex)


  if train_autoencoding then
   -- do it for actor network
      for i = params.seq_length, 1, -1 do
          inputTensor = inputTensors[i-1]
          local prior_c = actor_c[i-1]
          local prior_h = actor_h[i-1]
          local derr = transfer_data(torch.ones(1))

   --print(model.dsA[1])
   --print(model.dsA[2])
   --print(model.dsA[3])

          --inputTensor = torch.CudaTensor(params.batch_size):zero() -- hullu
          local tmp = actorRNNs[i]:backward({inputTensor, prior_c, prior_h},
                                       model.dsA)
          --print(tmp)
          model.dsA[1]:copy(tmp[2])
          model.dsA[2]:copy(tmp[3])
          model.dsA[3]:zero()


          --model.dsA[3][1][corpus[startIndex][i-1]] = 1 --TODO
          --model.dsA[3][2][corpus[startIndex+1][i-1]] = 1 --TODO
          buildGradientsOfProbOutputs(model.dsA[3], corpus, startIndex, endIndex, i-1) -- NOTE i-1 because it is for the next round!!!
          --print(model.dsA[1])
          cutorch.synchronize()
      end

      model.dsR[1]:copy(model.dsA[1])
      model.dsR[2]:copy(model.dsA[2])
      model.dsR[3]:zero()


 --         if model.dsR[3]:size(2) < 10000 then
 --  print(model.dsR[3]:size(2))
 --            crash()
 --         end

      -- TODO first c, h are not trained
      -- do it for reader network
      for i = params.seq_length, 1, -1 do
    
          inputTensor= torch.cmul(inputTensors[i], attention_decisions[i])

          local prior_c = reader_c[i-1]
          local prior_h = reader_h[i-1]
          local derr = transfer_data(torch.ones(1))

          --if model.dsR[3]:size(2) < 10000 then
          --     print(model.dsR[3]:size(2))
          --   crash()
          --end
          if epochCounter < 10 and i < params.seq_length then
               buildGradientsOfProbOutputs(model.dsR[3], corpus, startIndex, endIndex, i+1) -- NOTE i because it is for the next round!!!
          end


          local tmp = readerRNNs[i]:backward({inputTensor, prior_c, prior_h},
                                        model.dsR)
   --print(model.dsR[1])
   --print(model.dsR[2])
          model.dsR[1]:copy(tmp[2])
          model.dsR[2]:copy(tmp[3])
          cutorch.synchronize()
          --print(paramdxR:norm())
      end

      model.norm_dwR = paramdxR:norm()
      if model.norm_dwR > params.max_grad_norm then
          local shrink_factor = params.max_grad_norm / model.norm_dwR
          paramdxR:mul(shrink_factor)
      end

      model.norm_dwA = paramdxA:norm()
      if model.norm_dwA > params.max_grad_norm then
          local shrink_factor = params.max_grad_norm / model.norm_dwA
          paramdxA:mul(shrink_factor)
      end

  --print(paramdxR)
      momentum = 0.8

      -- MOMENTUM
      --[[updatesR:mul(momentum)
      updatesR:add(paramdxR:mul(1-momentum))

      updatesA:mul(momentum)
      updatesA:add(paramdxA:mul(1-momentum))

      paramxR:add(updatesR:mul(-params.lr))
      paramxA:add(updatesA:mul(-params.lr))]]

      paramxR:add(paramdxR:mul(-params.lr))
      paramxA:add(paramdxA:mul(-params.lr))
      

  end

  -- reward = totalAttentions + negloglikelihood
  -- TODO
  if true and train_attention_network then
     
--local uparamxRA, uparamdxRA = attentionNetworks[2]:getParameters() -- TODO somehow after loading it gets messed up



     --print(uparamxRA)
--crash()


     --[[print("---")
     print("BASE "..rewardBaseline)
     print("NLL "..nll[1])
     print("ATT "..totalAttentions[1])]]
     local reward = (nll:add(TOTAL_ATTENTIONS_WEIGHT,totalAttentions)) -- gives the reward for each batch item
     --print(reward)
     --print("R "..reward[1])
     local rewardDifference = reward:cuda():add(-rewardBaseline, ones)
     rewardBaseline = 0.8 * rewardBaseline + 0.2 * torch.sum(reward) * 1/params.batch_size
     --print("NEW BASELINE "..rewardBaseline)
     --[[print('RD '..rewardDifference[1])
     print(totalAttentions)
     print(rewardDifference)]]
     rewardDifference:mul(REWARD_DIFFERENCE_SCALING)


     for i = params.seq_length, 1, -1 do
        local whatToMultiplyToTheFinalDerivative = torch.CudaTensor(params.batch_size)
        --print(whatToMultiplyToTheFinalDerivative)
        for j=1,params.batch_size do
           if attention_decisions[i][j] == 0 then
               whatToMultiplyToTheFinalDerivative[j] = -1 / (1 - attention_scores[i][j][1])
               --[[print(attention_decisions[i][j])
               print(1 - attention_scores[i][j][1])
               print(whatToMultiplyToTheFinalDerivative[j])]]
           else
               whatToMultiplyToTheFinalDerivative[j] = 1 / (attention_scores[i][j][1])

               --[[print(attention_decisions[i][j])
               print(attention_scores[i][j][1])
               print(whatToMultiplyToTheFinalDerivative[j])]]

           end
        end
--[[           print(attention_decisions[i])
           print(attention_scores[i])
           print(whatToMultiplyToTheFinalDerivative)]]
        --print(rewardDifference)
        local tmp = attentionNetworks[i]:backward({inputTensors[i], reader_c[i-1]},
                                                   rewardDifference:clone():cmul(whatToMultiplyToTheFinalDerivative))
        --print(rewardDifference:clone():cmul(whatToMultiplyToTheFinalDerivative))
        --print(tmp[2])
     end
-- TODO now throw in different factors to get different weights on the gradients
     --print(paramdxRA)
     local norm_dwRA = paramdxRA:norm()
     if norm_dwRA > params.max_grad_norm then
        local shrink_factor = params.max_grad_norm / norm_dwRA
        paramdxRA:mul(shrink_factor)
     end
     --print(norm_dwRA)
     --print(paramdxRA)
     if norm_dwRA ~= norm_dwRA then
       local cloneParams, cloneGradParams = attentionNetworks[1]:parameters()
       print(cloneParams)
       print(cloneGradParams)
       print(cloneParams[1])
       print(cloneGradParams[1])
       print(cloneParams[2])
       print(cloneGradParams[2])
       print(cloneParams[3])
       print(cloneGradParams[3])
       print(cloneParams[4])
       print(cloneGradParams[4])
       print(cloneParams[5])
       print(cloneGradParams[5])
       print(cloneParams[6])
       print(cloneGradParams[6])
       print(cloneParams[7])
       print(cloneGradParams[7])
       REWARD_DIFFERENCE_SCALING = 0.1 * REWARD_DIFFERENCE_SCALING
       return
     end
     --print("...")

     -- MOMENTUM
     paramdxRA:mul((1-params.lr_momentum))

     --[[print(paramxRA)
     print(paramdxRA)
     print("###############")]]

     paramxRA:add(paramdxRA:mul(- 1 * params.lr_att))
     paramdxRA:mul(1 / (- 1 * params.lr_att)) -- is this really better than cloning before multiplying?
     --print("Shouldnt you have cloned it before multiplying, or undo the multiplication?")
     --crash()
  end


end









local function main()
  g_init_gpu(gpu_number)

  
  readDictionary()
  files = createListOfFiles()

  print("Network parameters:")
  print(params)

  print("setup")
  setup()
  print("setup done")



  local beginning_time = torch.tic()
  local start_time = torch.tic()
  print("Starting training.")
  local numberOfWords = 0
  local counter = 0
  for epoch = 1,100000000 do

   epochCounter = epoch

   --[[print(files)
   print(#files)
   print(params.batch_size)
   print(#files-params.batch_size+1)]]
   for i = 1,#files-params.batch_size+1,params.batch_size do
     --print(i)
    numberOfWords = numberOfWords + params.batch_size * params.seq_length

    for l = 1, params.batch_size do
       corpus[l] = readAFile(files[i+l-1])
    end

    local perp, actor_output = fp(corpus, 1, params.batch_size)

    meanTotalAtt = 0.8 * meanTotalAtt + 0.2 * torch.sum(totalAttentions) * 1/params.batch_size
    meanNLL = 0.8 * meanNLL + 0.2 * torch.sum(perp) * 1/params.batch_size

    meanNLLReader = 0.8 * meanNLLReader + 0.2 * torch.sum(nllReader) * 1/params.batch_size


    --print(meanNLL)
    counter = counter + 1
    if counter % 51 == 0 then
       print(numberOfWords..'  '..epoch..'  '..(100.0 * (i+0.0) / #files))
       print(numberOfWords / torch.toc(start_time))
       local since_beginning = g_d(torch.toc(beginning_time) / 60)
       print("+++++++ "..perp[1]..'  '..meanNLL..'  '..meanTotalAtt..'  '..(TOTAL_ATTENTIONS_WEIGHT * meanTotalAtt + meanNLL).. '  '..meanNLLReader)
        print(epoch.."  "..i..
            '   since beginning = ' .. since_beginning .. ' mins.')
       print(probabilityOfChoices[1]..'  '..totalAttentions[1])

       if probabilityOfChoices[1] ~= probabilityOfChoices[1]  then
          crash()
       end

       for l = 1, 1 do
          print("....")
  --      print(perp)
          print(perp[l])
          for j=1,params.seq_length do
             local predictedScores, predictedTokens = torch.min(actor_output[j][l],1)



             local predictedScoresR, predictedTokensR = torch.min(reader_output[j][l],1)
  --         print(attention_scores)
           --print(corpus[i+l-1][j])
           --print(chars[corpus[i+l-1][j]-1])
            --print(corpus[i+l-1][j]..'  '..chars[corpus[i+l-1][j]])
           --print(chars[corpus[i+l-1][j]])
           --print(chars[corpus[i+l-1][j]+1])

             --[[print((chars[getFromData(corpus,l ,j)-1]))
             print((chars[getFromData(corpus,l ,j)]))
             print((chars[getFromData(corpus,l ,j)+1]))]]


             io.write((chars[getFromData(corpus,l,j)]))--..'\n')
             io.write(" ~ "..chars[predictedTokens[1]].."  "..math.exp(-predictedScores[1]).."  "..math.exp(-actor_output[j][l][getFromData(corpus,l,j)]).."  "..attention_decisions[j][l].."  "..attention_scores[j][l][1].."\n")
             --io.write(chars[predictedTokensR[1]]..'  '..getFromData(corpus,l,j+1).."\n")


            --print(chars[getFromData(corpus,i + l - 1,j)])
           --print(corpus[i + l - 1][j].."  "..predictedTokens[1].."  "..(-predictedScores[1]).."  "..(-actor_output[j][l][corpus[i + l - 1][j]]).."  "..attention_decisions[j][l].."  "..attention_scores[j][l][1])
          end
       end
       --io.output(fileStats)
       fileStats:write((numberOfWords/params.seq_length)..'\t'..perp[1]..'\n')
       fileStats:flush()
       --io.output(stdout)
    end

    bp(corpus, 1, params.batch_size)
    if i % 33 == 0 then
      cutorch.synchronize()
      collectgarbage()
    end


    if counter % 1000 == 0 then
        print("WRITING MODEL...")

         local uR, udR = readerRNNs[1]:parameters()
         local uA, udA = actorRNNs[1]:parameters()
         local uRA, udRA = attentionNetworks[1]:parameters()


         local modelsArray = {params,(numberOfWords/params.seq_length),uR, udR, uA, udA, uRA, udRA, reader_c[0], reader_h[0]}
         torch.save('/disk/scratch2/s1582047/model-'..experimentNameOut, modelsArray, "binary") 
         --crash()
    end

    if (not use_attention_network) and meanNLL < NLL_TO_CHANGE_ATTENTION then -- or (numberOfWords/(params.batch_size * params.seq_length) % 5000 == 0)
         print("CHANGE ATTENTION")
         meanNLL = 10 * NLL_TO_CHANGE_ATTENTION -- to prevent the attention from going down to the base immediately
         FIXED_ATTENTION = BASE_ATTENTION + 0.9 * (FIXED_ATTENTION-BASE_ATTENTION)
    end



    --if numberOfWords/(5*params.batch_size) % 50000 == 0 then
    --   use_attention_network = true
    --   train_attention_network = true
    --end

  end
  end
  run_test()
  print("Training is over.")
end



--[[local function mainForOneCorpusInMemory()
  g_init_gpu(gpu_number)

  readCorpus()

  print("Network parameters:")
  print(params)

  print("setup")
  setup()
  print("setup done")



  local beginning_time = torch.tic()
  local start_time = torch.tic()
  print("Starting training.")
  local numberOfWords = 0
  local counter = 0
  for epoch = 1,100000000 do
   for i = 1,#corpus-params.batch_size,params.batch_size do
    numberOfWords = numberOfWords + params.batch_size * params.seq_length
    local perp, actor_output = fp(corpus, i, i+params.batch_size-1)

    meanTotalAtt = 0.8 * meanTotalAtt + 0.2 * torch.sum(totalAttentions) * 1/params.batch_size
    meanNLL = 0.8 * meanNLL + 0.2 * torch.sum(perp) * 1/params.batch_size
    --print(meanNLL)
    counter = counter + 1
    if counter % 50 == 0 then
       print(torch.toc(start_time))
       print(numberOfWords)
       print(numberOfWords / torch.toc(start_time))
       local since_beginning = g_d(torch.toc(beginning_time) / 60)
       print("+++++++ "..perp[1]..'  '..meanNLL..'  '..meanTotalAtt..'  '..(TOTAL_ATTENTIONS_WEIGHT * meanTotalAtt + meanNLL))
        print(epoch.."  "..i..
            '   since beginning = ' .. since_beginning .. ' mins.')
       print(probabilityOfChoices[1]..'  '..totalAttentions[1])

       if probabilityOfChoices[1] ~= probabilityOfChoices[1]  then
          crash()
       end

       for l = 1, 1 do
          print("....")
  --      print(perp)
          print(perp[l])
          for j=1,params.seq_length do
             local predictedScores, predictedTokens = torch.min(actor_output[j][l],1)
  --         print(attention_scores)
           --print(corpus[i+l-1][j])
           --print(chars[corpus[i+l-1][j]-1])
            --print(corpus[i+l-1][j]..'  '..chars[corpus[i+l-1][j] ])
           --print(chars[corpus[i+l-1][j] ])
           --print(chars[corpus[i+l-1][j]+1])
             io.write((chars[getFromData(corpus,i + l - 1,j)]))--..'\n')
             io.write(" ~ "..chars[predictedTokens[1] ].."  "..math.exp(-predictedScores[1]).."  "..math.exp(-actor_output[j][l][getFromData(corpus,i + l - 1,j)]).."  "..attention_decisions[j][l].."  "..attention_scores[j][l][1].."\n")
            --print(chars[getFromData(corpus,i + l - 1,j)])
           --print(corpus[i + l - 1][j].."  "..predictedTokens[1].."  "..(-predictedScores[1]).."  "..(-actor_output[j][l][corpus[i + l - 1][j] ]).."  "..attention_decisions[j][l].."  "..attention_scores[j][l][1])
          end
       end
       --io.output(fileStats)
       fileStats:write((numberOfWords/params.seq_length)..'\t'..perp[1]..'\n')
       fileStats:flush()
       --io.output(stdout)
    end

    bp(corpus, i, i+params.batch_size-1)
    if i % 33 == 0 then
      cutorch.synchronize()
      collectgarbage()
    end


    if counter % 1000 == 0 then
        print("WRITING MODEL...")

         local uR, udR = readerRNNs[1]:parameters()
         local uA, udA = actorRNNs[1]:parameters()
         local uRA, udRA = attentionNetworks[1]:parameters()


         local modelsArray = {params,(numberOfWords/params.seq_length),uR, udR, uA, udA, uRA, udRA, reader_c[0], reader_h[0]}
         torch.save('/disk/scratch2/s1582047/model-'..experimentNameOut, modelsArray, "binary") 
         --crash()
    end

    if (not use_attention_network) and meanNLL < NLL_TO_CHANGE_ATTENTION then -- or (numberOfWords/(params.batch_size * params.seq_length) % 5000 == 0)
         print("CHANGE ATTENTION")
         meanNLL = 10 * NLL_TO_CHANGE_ATTENTION -- to prevent the attention from going down to the base immediately
         FIXED_ATTENTION = BASE_ATTENTION + 0.9 * (FIXED_ATTENTION-BASE_ATTENTION)
    end



    --if numberOfWords/(5*params.batch_size) % 50000 == 0 then
    --   use_attention_network = true
    --   train_attention_network = true
    --end

  end
  end
  run_test()
  print("Training is over.")
end]]

main()
--mainForOneCorpusInMemory()
