--
----  Copyright (c) 2014, Facebook, Inc.
----  All rights reserved.
----
----  This source code is licensed under the Apache 2 license found in the
----  LICENSE file in the root directory of this source tree. 
----
local ok,cunn = pcall(require, 'fbcunn')
if not ok then
    ok,cunn = pcall(require,'cunn')
    if ok then
        print("warning: fbcunn not found. Falling back to cunn") 
        LookupTable = nn.LookupTable
    else
        print("Could not find cunn or fbcunn. Either is required")
        os.exit()
    end
else
    deviceParams = cutorch.getDeviceProperties(1)
    cudaComputeCapability = deviceParams.major + deviceParams.minor/10
    LookupTable = nn.LookupTable
end
require('nngraph')
require('base')
local ptb = require('data')

-- Train 1 day and gives 82 perplexity.

local params = {batch_size=2,
                seq_length=20,
                layers=2,
                decay=2,
                rnn_size=10,
                dropout=0,
                init_weight=0.1,
                lr=1,
                vocab_size=10000,
                max_epoch=4,
                max_max_epoch=13,
                max_grad_norm=5}
        

-- Trains 1h and gives test 115 perplexity.
--[[local params = {batch_size=20,
                seq_length=20,
                layers=2,
                decay=2,
                rnn_size=10,
                dropout=0,
                init_weight=0.5,
                lr=0.01,
                vocab_size=10,
                max_epoch=4,
                max_max_epoch=13,
                max_grad_norm=5}]]

--[[local params = {batch_size=20,
                seq_length=20,
                layers=2,
                decay=2,
                rnn_size=200,
                dropout=0,
                init_weight=0.1,
                lr=1,
                vocab_size=10000,
                max_epoch=4,
                max_max_epoch=13,
                max_grad_norm=5}]]

local function transfer_data(x)
  return x:cuda()
end

local state_train, state_valid, state_test
local model = {}
local paramx, paramdx




local corpus_name = "corpus5.txt"






corpus = {}
-- NOTE THAT THE FILES COME FROM PYTHON AND WILL BE ZERO-INDEXED
function readCorpus() --as a double storage
   io.input(corpus_name..".num.b")
   t = io.read("*all")
   local counter = 0
   for line in string.gmatch(t, "[^\n]+") do
     counter = counter+1
     lineList = {}
     table.insert(corpus, lineList)
     for token in string.gmatch(line, "[0-9]+") do
        table.insert(lineList, math.min(token+1.0, params.vocab_size))
     end

   end
   --corpus = torch.Tensor(corpusList)
end

readCorpus()

print("DONE READING CORPUS")











local function lstm(x, prev_c, prev_h)


  local a = nn.Linear(params.rnn_size, params.rnn_size)(x)
  local b = nn.Linear(params.rnn_size, params.rnn_size)(prev_c)
  local c = nn.Linear(params.rnn_size, params.rnn_size)(prev_h)

  local next_c = nn.CAddTable()({a, b, c})
  local next_h = nn.CAddTable()({a, b, c})
return next_c, next_h



--[[  -- Calculate all four gates in one go
  local i2h = nn.Linear(params.rnn_size, 4*params.rnn_size)(x)
  local h2h = nn.Linear(params.rnn_size, 4*params.rnn_size)(prev_h)
  local gates = nn.CAddTable()({i2h, h2h})
  
  -- Reshape to (batch_size, n_gates, hid_size)
  -- Then slize the n_gates dimension, i.e dimension 2
  local reshaped_gates =  nn.Reshape(4,params.rnn_size)(gates)
  local sliced_gates = nn.SplitTable(2)(reshaped_gates)
  
  -- Use select gate to fetch each gate and apply nonlinearity
  local in_gate          = nn.Sigmoid()(nn.SelectTable(1)(sliced_gates))
  local in_transform     = nn.Tanh()(nn.SelectTable(2)(sliced_gates))
  local forget_gate      = nn.Sigmoid()(nn.SelectTable(3)(sliced_gates))
  local out_gate         = nn.Sigmoid()(nn.SelectTable(4)(sliced_gates))

  local next_c           = nn.CAddTable()({
      nn.CMulTable()({forget_gate, prev_c}),
      nn.CMulTable()({in_gate,     in_transform})
  })
  local next_h           = nn.CMulTable()({out_gate, nn.Tanh()(next_c)})

  return next_c, next_h]]
end

local function create_network(withOutput)
  local x                = nn.Identity()()
  local prev_c           = nn.Identity()()
  local prev_h           = nn.Identity()()
  local i                = LookupTable(params.vocab_size,params.rnn_size)(x)
  local next_s           = {}
  local next_c, next_h = lstm(i, prev_c, prev_h)
  local module
  if withOutput  then
        local h2y              = nn.Linear(params.rnn_size, params.vocab_size)(next_c)
        local output = nn.MulConstant(-1)(nn.LogSoftMax()(h2y))
-- TODO softmax
      module = nn.gModule({x, prev_c, prev_h},
                                      {next_c, next_h, output})
  else
      module = nn.gModule({x, prev_c, prev_h},
                                      {next_c, next_h})
  end
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end

local function setup()
  print("Creating a RNN LSTM network.")
  local reader_core_network = create_network(false)
  paramxR, paramdxR = reader_core_network:getParameters()

  readerRNNs = {}

  for i=1,params.seq_length do
     readerRNNs[i] = g_clone(reader_core_network)
  end


  local actor_core_network = create_network(true)
  paramxA, paramdxA = actor_core_network:getParameters()

  actorRNNs = {}

  for i=1,params.seq_length do
     actorRNNs[i] = g_clone(actor_core_network)
  end



  model.sR = {}
  model.dsR = {}
  model.dsA = {}
  model.start_sR = {}
  for j = 0, params.seq_length do
    model.sR[j] = transfer_data(torch.zeros(params.batch_size, params.rnn_size))
  end
  model.dsR[1] = transfer_data(torch.zeros(params.rnn_size))
  model.dsR[2] = transfer_data(torch.zeros(params.rnn_size))

  model.dsA[1] = transfer_data(torch.zeros(params.rnn_size))
  model.dsA[2] = transfer_data(torch.zeros(params.rnn_size))
 model.dsA[3] = transfer_data(torch.zeros(params.rnn_size))



  reader_c ={}
  reader_h = {}

  actor_c ={[0] = torch.CudaTensor(params.rnn_size)}
  actor_h = {[0] = torch.CudaTensor(params.rnn_size)}

  reader_c[0] = torch.CudaTensor(params.batch_size,params.rnn_size):zero() --TODO they have to be intiialized decently
  reader_h[0] = torch.CudaTensor(params.batch_size,params.rnn_size):zero()



end





local function reset_state(state)

end

-- TODO make more efficient by filling zeros in place with :zero?
local function reset_ds()
    model.dsR[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    model.dsR[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h


    model.dsA[1] = torch.zeros(params.batch_size,params.rnn_size):cuda() --c
    model.dsA[2] = torch.zeros(params.batch_size,params.rnn_size):cuda() --h
    model.dsA[3] = torch.zeros(params.batch_size,params.vocab_size):cuda() --h
end




local function buildInputTensors(data, startIndex, endIndex)
    local inputTensors = {}
    for token=0, params.seq_length do
      local inputTensor = torch.CudaTensor(params.batch_size)
      for index=startIndex,endIndex do
           if token==0 then
              inputTensor[index-startIndex+1] = 1
           else
              inputTensor[index-startIndex+1] = data[index][token]
           end
      end
      inputTensors[token] = inputTensor
    end
    --print(inputTensors)
    return inputTensors
end


local function fp(corpus, startIndex, endIndex)
  --data[0] = 1
  -- Reader
  local inputTensors = buildInputTensors(corpus, startIndex, endIndex)
  for i=1, params.seq_length do
     inputTensor = inputTensors[i]
     --print(inputTensor)
     reader_c[i], reader_h[i] = unpack(readerRNNs[i]:forward({inputTensor, reader_c[i-1], reader_h[i-1]}))
  end

  actor_c[0] = reader_c[params.seq_length]
  actor_h[0] = reader_h[params.seq_length]

  --print(reader_h[20])

  local nll = 0
  actor_output = {}
  for i=1, params.seq_length do
     inputTensor = inputTensors[i-1]    
     actor_c[i], actor_h[i], actor_output[i] = unpack(actorRNNs[i]:forward({inputTensor, actor_c[i-1], actor_h[i-1]}))
     nll = nll + actor_output[i][1][corpus[startIndex][i]] -- TODO
    --print(actor_c[i])
    --print(actor_h[i])
  end

  return nll, actor_output
end


local function checkBackprop(data)
   --local loss, _ = fp(data)
   --print("LOSS "..loss)
   for i = 1,paramxA:size(1) do
       print("A "..i)
--      for j = 1,paramdxA[i]:dim() do
          paramxA[i] = paramxA[i] + 0.0001
          local lossNew, _ = fp(data)
          local deriv = (lossNew - loss) * 1/0.0001
          print((deriv - paramdxA[i]).." :: "..deriv.."  "..paramdxA[i])
          paramxA[i] = paramxA[i] - 0.0001
   end

assert(paramxR:size(1) == paramdxR:size(1), "..")
assert(paramxA:size(1) == paramdxA:size(1), "..")
   for i = 1,paramxR:size(1) do
       print("R "..i)
--      for j = 1,paramdxA[i]:dim() do
          paramxR[i] = paramxR[i] + 0.0001
          local lossNew, _ = fp(data)
          local deriv = (lossNew - loss) * 1/0.0001
          print((deriv - paramdxR[i]).." :: "..deriv.."  "..paramdxR[i])
          paramxR[i] = paramxR[i] - 0.0001
   end

end


local function buildGradientsOfProbOutputs(dsAThird, corpus, startIndex, endIndex, tokenIndex)
      for index=startIndex,endIndex do
           if tokenIndex==0 then
               dsAThird[index - startIndex + 1][1] = 1
           else
               dsAThird[index - startIndex + 1][corpus[index][tokenIndex]] = 1
           end
      end
    --print(dsAThird)
end

local function bp(corpus, startIndex, endIndex)
   --local loss, _ = fp(data)
   --print("LOSS1 "..loss)


  paramdxR:zero()
  paramdxA:zero()
  reset_ds()
  --print(model.dsA)


  buildGradientsOfProbOutputs(model.dsA[3], corpus, startIndex, endIndex, params.seq_length)

  --model.dsA[3][1][corpus[startIndex][params.seq_length]] = 1 --TODO
  --model.dsA[3][2][corpus[startIndex+1][params.seq_length]] = 1 --TODO


   --local loss, _ = fp(data)
   --print("LOSS2 "..loss)

  local inputTensors = buildInputTensors(corpus, startIndex, endIndex)

  -- do it for actor network
  for i = params.seq_length, 1, -1 do
    inputTensor = inputTensors[i-1]

    local prior_c = actor_c[i-1]
    local prior_h = actor_h[i-1]
    local derr = transfer_data(torch.ones(1))


    --print(model.dsA[1])
    --print(prior_c)
    --print(prior_h)
    --print(inputTensor)
    local tmp = actorRNNs[i]:backward({inputTensor, prior_c, prior_h},
                                       model.dsA)
    --print(tmp)
    model.dsA[1]:copy(tmp[2])
    model.dsA[2]:copy(tmp[3])
    model.dsA[3]:zero()

    --model.dsA[3][1][corpus[startIndex][i-1]] = 1 --TODO
    --model.dsA[3][2][corpus[startIndex+1][i-1]] = 1 --TODO
    buildGradientsOfProbOutputs(model.dsA[3], corpus, startIndex, endIndex, i-1)
    --print(model.dsA[1])
    cutorch.synchronize()
  end

    model.dsR[1]:copy(model.dsA[1])
    model.dsR[2]:copy(model.dsA[2])


  -- TODO first c, h are not trained


  -- do it for reader network
  for i = params.seq_length, 1, -1 do
    inputTensor = inputTensors[i]

    local prior_c = reader_c[i-1]
    local prior_h = reader_h[i-1]
    local derr = transfer_data(torch.ones(1))

    local tmp = readerRNNs[i]:backward({inputTensor, prior_c, prior_h},
                                       model.dsR)

    model.dsR[1]:copy(tmp[2])
    model.dsR[2]:copy(tmp[3])
    cutorch.synchronize()
    --print(paramdxR:norm())
  end



  --checkBackprop(data)



  model.norm_dwR = paramdxR:norm()
  if model.norm_dwR > params.max_grad_norm then
    local shrink_factor = params.max_grad_norm / model.norm_dwR
    paramdxR:mul(shrink_factor)
  end

  model.norm_dwA = paramdxA:norm()
  if model.norm_dwA > params.max_grad_norm then
    local shrink_factor = params.max_grad_norm / model.norm_dwA
    paramdxA:mul(shrink_factor)
  end

  --print(paramxA)

  -- training





  --paramxR:add(paramdxR:mul(-params.lr))
  paramxA:add(paramdxA:mul(-params.lr))
end


local function main()
  g_init_gpu(arg)
  state_train = {data=transfer_data(ptb.traindataset(params.batch_size))}
  state_valid =  {data=transfer_data(ptb.validdataset(params.batch_size))}
  state_test =  {data=transfer_data(ptb.testdataset(params.batch_size))}
  print("Network parameters:")
  print(params)

  print("setup")
  setup()
  print("setup done")



  local beginning_time = torch.tic()
  local start_time = torch.tic()
  print("Starting training.")

  for epoch = 1,1000 do
   for i = 1,#corpus-params.batch_size,params.batch_size do
    local perp, actor_output = fp(corpus, i, i+params.batch_size-1)
    
    if i % 50 == 0 then
       print(torch.toc(start_time))
       print(i * params.batch_size)
       print(i * params.batch_size / torch.toc(start_time))
       local since_beginning = g_d(torch.toc(beginning_time) / 60)
       print("+++++++ "..perp)
        print(epoch.."  "..i..
            '   since beginning = ' .. since_beginning .. ' mins.')
       for i=1,params.seq_length do
           local predictedScores, predictedTokens = torch.min(actor_output[i][1],1)
           print(corpus[i][i].."  "..predictedTokens[1].."  "..math.exp(-predictedScores[1]).."  "..math.exp(-actor_output[i][1][corpus[i][i]]))
       end
    end

    bp(corpus, i, i+params.batch_size-1)
    if i % 33 == 0 then
      cutorch.synchronize()
      collectgarbage()
    end
  end
  end
  run_test()
  print("Training is over.")
end

main()
