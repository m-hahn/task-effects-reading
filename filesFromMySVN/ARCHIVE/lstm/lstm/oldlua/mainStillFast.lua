--
----  Copyright (c) 2014, Facebook, Inc.
----  All rights reserved.
----
----  This source code is licensed under the Apache 2 license found in the
----  LICENSE file in the root directory of this source tree. 
----
local ok,cunn = pcall(require, 'fbcunn')
if not ok then
    ok,cunn = pcall(require,'cunn')
    if ok then
        print("warning: fbcunn not found. Falling back to cunn") 
        LookupTable = nn.LookupTable
    else
        print("Could not find cunn or fbcunn. Either is required")
        os.exit()
    end
else
    deviceParams = cutorch.getDeviceProperties(1)
    cudaComputeCapability = deviceParams.major + deviceParams.minor/10
    LookupTable = nn.LookupTable
end
require('nngraph')
require('base')
local ptb = require('data')

-- Train 1 day and gives 82 perplexity.
--[[
local params = {batch_size=20,
                seq_length=35,
                layers=2,
                decay=1.15,
                rnn_size=1500,
                dropout=0.65,
                init_weight=0.04,
                lr=1,
                vocab_size=10000,
                max_epoch=14,
                max_max_epoch=55,
                max_grad_norm=10}
               ]]--

-- Trains 1h and gives test 115 perplexity.
local params = {batch_size=20,
                seq_length=20,
                layers=2,
                decay=2,
                rnn_size=200,
                dropout=0,
                init_weight=0.1,
                lr=1,
                vocab_size=10000,
                max_epoch=4,
                max_max_epoch=13,
                max_grad_norm=5}

local function transfer_data(x)
  return x:cuda()
end

local state_train, state_valid, state_test
local model = {}
local paramx, paramdx

local function lstm(x, prev_c, prev_h)
  -- Calculate all four gates in one go
  local i2h = nn.Linear(params.rnn_size, 4*params.rnn_size)(x)
  local h2h = nn.Linear(params.rnn_size, 4*params.rnn_size)(prev_h)
  local gates = nn.CAddTable()({i2h, h2h})
  
  -- Reshape to (batch_size, n_gates, hid_size)
  -- Then slize the n_gates dimension, i.e dimension 2
  local reshaped_gates =  nn.Reshape(4,params.rnn_size)(gates)
  local sliced_gates = nn.SplitTable(2)(reshaped_gates)
  
  -- Use select gate to fetch each gate and apply nonlinearity
  local in_gate          = nn.Sigmoid()(nn.SelectTable(1)(sliced_gates))
  local in_transform     = nn.Tanh()(nn.SelectTable(2)(sliced_gates))
  local forget_gate      = nn.Sigmoid()(nn.SelectTable(3)(sliced_gates))
  local out_gate         = nn.Sigmoid()(nn.SelectTable(4)(sliced_gates))

  local next_c           = nn.CAddTable()({
      nn.CMulTable()({forget_gate, prev_c}),
      nn.CMulTable()({in_gate,     in_transform})
  })
  local next_h           = nn.CMulTable()({out_gate, nn.Tanh()(next_c)})

  return next_c, next_h
end

local function create_network(withOutput)
  local x                = nn.Identity()()
  local prev_c           = nn.Identity()()
  local prev_h           = nn.Identity()()
  local i                = LookupTable(params.vocab_size,params.rnn_size)(x)
  local next_s           = {}
  local next_c, next_h = lstm(i, prev_c, prev_h)
  local module
  if withOutput  then
        local h2y              = nn.Linear(params.rnn_size, params.vocab_size)(next_c)
        local output = nn.MulConstant(-1)(nn.LogSoftMax()(h2y))
-- TODO softmax
      module = nn.gModule({x, prev_c, prev_h},
                                      {next_c, next_h, output})
  else
      module = nn.gModule({x, prev_c, prev_h},
                                      {next_c, next_h})
  end
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end

local function setup()
  print("Creating a RNN LSTM network.")
  local reader_core_network = create_network(false)
  paramxR, paramdxR = reader_core_network:getParameters()

  readerRNNs = {}

  for i=1,20 do
     readerRNNs[i] = g_clone(reader_core_network)
  end



  local actor_core_network = create_network(true)
  paramxA, paramdxA = actor_core_network:getParameters()

  actorRNNs = {}

  for i=1,20 do
     actorRNNs[i] = g_clone(actor_core_network)
  end



  model.sR = {}
  model.dsR = {}
  model.dsA = {}
  model.start_sR = {}
  for j = 0, params.seq_length do
    model.sR[j] = transfer_data(torch.zeros(params.batch_size, params.rnn_size))
  end
  model.dsR[1] = transfer_data(torch.zeros(params.rnn_size))
  model.dsR[2] = transfer_data(torch.zeros(params.rnn_size))

  model.dsA[1] = transfer_data(torch.zeros(params.rnn_size))
  model.dsA[2] = transfer_data(torch.zeros(params.rnn_size))
 model.dsA[3] = transfer_data(torch.zeros(params.rnn_size))



  reader_c ={}
  reader_h = {}

  actor_c ={[0] = torch.CudaTensor(params.rnn_size)}
  actor_h = {[0] = torch.CudaTensor(params.rnn_size)}

  reader_c[0] = torch.CudaTensor(params.rnn_size):uniform() --TODO they have to be intiialized decently
  reader_h[0] = torch.CudaTensor(params.rnn_size):uniform()



end





local function reset_state(state)

end

local function reset_ds()
    model.dsR[1] = torch.ones(params.rnn_size):cuda() --c
    model.dsR[2] = torch.ones(params.rnn_size):cuda() --h


    model.dsA[1] = torch.ones(params.rnn_size):cuda() --c
    model.dsA[2] = torch.ones(params.rnn_size):cuda() --h
    model.dsA[3] = torch.zeros(params.vocab_size):cuda() --h
end


local function fp(data)
  data[0] = 1000
  -- Reader
  local inputTensor = torch.CudaTensor(1)
  for i=1, 20 do
     inputTensor[1] = data[i]     
     reader_c[i], reader_h[i] = unpack(readerRNNs[i]:forward({inputTensor, reader_c[i-1], reader_h[i-1]}))
  end

  actor_c[0] = reader_c[20]
  actor_h[0] = reader_h[20]

  local nll = 0
  actor_output = {}
  for i=1, 20 do
     inputTensor[1] = data[i-1]
     actor_c[i], actor_h[i], actor_output[i] = unpack(actorRNNs[i]:forward({inputTensor, actor_c[i-1], actor_h[i-1]}))
     nll = nll + actor_output[i][1][data[i]]
  end

  return nll, actor_output
end

local function bp(data)
  paramdxR:zero()
  paramdxA:zero()
  reset_ds()
  data[0] = 1000
  local inputTensor = torch.CudaTensor(1)

  -- do it for actor network
  for i = 20, 1, -1 do
    local x = data[i]
    inputTensor[1] = data[i-1]

    local prior_c = actor_c[i-1]
    local prior_h = actor_h[i-1]
    local derr = transfer_data(torch.ones(1))

    local tmp = actorRNNs[i]:backward({inputTensor, prior_c, prior_h},
                                       model.dsA)
    --print(tmp)
    model.dsA[1]:copy(tmp[2])
    model.dsA[2]:copy(tmp[3])
    model.dsA[3][data[i-1]] = 1
    --print(model.dsA[1])
    cutorch.synchronize()
  end

    model.dsR[1]:copy(model.dsA[1])
    model.dsR[2]:copy(model.dsA[2])

  -- do it for reader network
  for i = 20, 1, -1 do
    local x = data[i]
    inputTensor[1] = data[i-1]

    local prior_c = reader_c[i-1]
    local prior_h = reader_h[i-1]
    local derr = transfer_data(torch.ones(1))

    local tmp = readerRNNs[i]:backward({inputTensor, prior_c, prior_h},
                                       model.dsR)

    model.dsR[1]:copy(tmp[2])
    model.dsR[2]:copy(tmp[3])
    cutorch.synchronize()
    --print(paramdxR:norm())
  end

  model.norm_dwR = paramdxR:norm()
  if model.norm_dwR > params.max_grad_norm then
    local shrink_factor = params.max_grad_norm / model.norm_dwR
    paramdxR:mul(shrink_factor)
  end

  model.norm_dwA = paramdxA:norm()
  if model.norm_dwA > params.max_grad_norm then
    local shrink_factor = params.max_grad_norm / model.norm_dwA
    paramdxA:mul(shrink_factor)
  end

  -- training


  paramxR:add(paramdxR:mul(-params.lr))
  paramxA:add(paramdxA:mul(-params.lr))
end


local function main()
  g_init_gpu(arg)
  state_train = {data=transfer_data(ptb.traindataset(params.batch_size))}
  state_valid =  {data=transfer_data(ptb.validdataset(params.batch_size))}
  state_test =  {data=transfer_data(ptb.testdataset(params.batch_size))}
  print("Network parameters:")
  print(params)
  local states = {state_train, state_valid, state_test}
  for _, state in pairs(states) do
    reset_state(state)
  end
  print("setup")
  setup()
  print("setup done")
  local step = 0
  local epoch = 0
  local total_cases = 0
  local beginning_time = torch.tic()
  local start_time = torch.tic()
  print("Starting training.")
  local words_per_step = params.seq_length * params.batch_size
  local epoch_size = torch.floor(state_train.data:size(1) / params.seq_length)
  local perps
  local data = {1,2,5,2,3,2,2,7,2,2,4,2,2,2,2,6,2,8,1,2,2,6,8,1,3,2,3,5,6,7,4,2,4,5,3,3,2,4,6,7,1,2,4,5,6,4}
  while epoch < params.max_max_epoch do
    local perp, actor_output = fp(data)
    



    if perps == nil then
      perps = torch.zeros(epoch_size):add(perp)
    end
    perps[step % epoch_size + 1] = perp
    step = step + 1
    bp(data)
    total_cases = total_cases + params.seq_length * params.batch_size
    epoch = step / epoch_size
    if step % torch.round(epoch_size / 10) == 10 then
      local wps = torch.floor(total_cases / torch.toc(start_time))
      local since_beginning = g_d(torch.toc(beginning_time) / 60)
      print('epoch = ' .. g_f3(epoch) ..
            ', train perp. = ' .. g_f3(torch.exp(perps:mean())) ..
            ', wps = ' .. wps ..
            ', dw:norm() = ' .. g_f3(model.norm_dwR) ..
            ', lr = ' ..  g_f3(params.lr) ..
            ', since beginning = ' .. since_beginning .. ' mins.')
    end
    if step % epoch_size == 0 then
      run_valid()
      if epoch > params.max_epoch then
          params.lr = params.lr / params.decay
      end
    end
    if step % 33 == 0 then
      cutorch.synchronize()
      collectgarbage()
    end
  end
  run_test()
  print("Training is over.")
end

main()
