--- derived from mini-batch-back-flexible-apparentlyslow-but-everything, derived from Zaremba's LSTM code



--[[

main-attention.lua 2 pg50665.txt false 20 30 100 10000 3 false


-]]




--[[ TODO

-- train also to autoencode prefixes for warming up

--- maybe word-embeddings better be shared between attention and reader

-- train initial c, h

-- TODO word-embeddings dimension s.t. different


* ???are there different parameters for different indices within a batch?


+++ GOOD MODELS

.. ~/local/bin/th main-attention.lua 1 pg50665.txt false 30 50 500 10000 3 false 0.2 500: attention at 0.91

.. ~/local/bin/th main-attention.lua 3 pg50665.txt false 20 100 1000 10000 3 false 0.2 200 (not so great)

+++++++

.. ~/local/bin/th main-attention-lm-while-reading.lua 4 pg50665.txt false 30 50 701 10000 3 false 0.2 500
is done with lm, the one with 700 is done without
]]



local ok,cunn = pcall(require, 'fbcunn')
if not ok then
    ok,cunn = pcall(require,'cunn')
    if ok then
        print("warning: fbcunn not found. Falling back to cunn") 
        LookupTable = nn.LookupTable
    else
        print("Could not find cunn or fbcunn. Either is required")
        os.exit()
    end
else
    deviceParams = cutorch.getDeviceProperties(1)
    cudaComputeCapability = deviceParams.major + deviceParams.minor/10
    LookupTable = nn.LookupTable
end
require('nngraph')
require('base')
local ptb = require('data')


local NLL_TO_CHANGE_ATTENTION = 1

local meanNLL = 10000
local meanTotalAtt = 0




TASK = 'qa'--'autoencoding' --'qa'


----------------

gpu_number = {arg[1]+0}

local corpus_name = arg[2] --"pg50665.txt" --"pg50665.txt" --"hlm.txt"

if arg[3] == 'false' then
   arg[3] = false
end
local LOAD = arg[3] and true



local REWARD_DIFFERENCE_SCALING = 1

local FIXED_ATTENTION = 0.95
local BASE_ATTENTION = 0.6


-- 1 pg50665.txt 20 30 100 8000
local params = {batch_size=arg[4]+0,
                seq_length=arg[5]+0,
                --layers=2,
                --decay=2,
                rnn_size=arg[6]+0,
                --dropout=0,
                init_weight=0.05,
                lr=((arg[10]+0) + 0.0), -- 0.01
                vocab_size=arg[7]+0,
                --max_epoch=4,
                --max_max_epoch=13,
                max_grad_norm=5,
                lr_att = 0.01,
                lr_momentum = 0.9,
                embeddings_dimensionality = arg[11] + 0,
                question_length = 20}

local TOTAL_ATTENTIONS_WEIGHT = arg[8]+0



local use_attention_network
local train_attention_network
local train_autoencoding

if arg[9] == 'false' then
   arg[9] = false
end
if arg[9] then
  use_attention_network = true
  train_attention_network = true
  train_autoencoding = false
else
  use_attention_network = false
  train_attention_network = false
  train_autoencoding = true
end   
if train_attention_network and (not use_attention_network) then
   crash()
end

-- arg[10] is learning rate
-- embeddings_dimensionality = arg[11]

local experimentName = "pg-test-"..TASK..'-'..params.seq_length.."-"..params.rnn_size.."-"..params.lr.."-"..params.embeddings_dimensionality
local experimentNameOut = experimentName

if LOAD then
  experimentNameOut = experimentNameOut.."-R-"..TOTAL_ATTENTIONS_WEIGHT
end

local fileStats = io.open(experimentNameOut..'-stats', 'w')


print("Printing stuff to "..experimentNameOut)


local function transfer_data(x)
  return x:cuda()
end

local state_train, state_valid, state_test
local model = {}
local paramx, paramdx



local corpusDir --"/disk/scratch2/s1582047/gutenberg/out/" --"/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training/"
local dictLocation


if false then
  corpusDir = "/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questionsChars/training/"
  dictLocation = "/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questionsChars/num2Chars"
else
  corpusDir = "/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training3/"
  dictLocation = "/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training3/num2Chars"
end

function createListOfFiles()
    local files = {}
    io.input("/disk/scratch2/s1582047/listOfFilesToRead.txt") --gutenberg
    t = io.read("*all")
    for line in string.gmatch(t, "[^\n]+") do
       table.insert(files, line)
    end
    return files
end


-- READ THE DICTIONARY

function readDictionary()
   io.input(dictLocation) --"disk/scratch2/s1582047/gutenberg/num2Chars") --)
   t = io.read("*all")
   for line in string.gmatch(t, "[^\n]+") do
     local isInSecond = false
     local character
     if line:len() > 1 then
       for x in string.gmatch(line, "[^ ]+") do
          --print(x)
          character = x
          if isInSecond == true then
            break
          end
          isInSecond = true
       end
       --print(character.."..."..(#chars).."   "..line)
       table.insert(chars, character)
     else
       print("IGNORED "..line)
     end
   end

   -- HULLU
   --for i = 1, 15000 do
   --   chars[i] = i..""
   --end
   


   if params.vocab_size > #chars then
      print(#chars)
      crash()
   end
end


corpus = {}



function readAFile(subDir,fileName)
   local dataPointFromFile = {}
   io.input(corpusDir..subDir..fileName)
   t = io.read("*all")
   local counter = 0
   for line in string.gmatch(t, "[^\n]+") do
     counter = counter+1
     lineList = {}
     for token in string.gmatch(line, "[0-9]+") do
        table.insert(dataPointFromFile, math.min(token+1, params.vocab_size))
     end
   end
   --print("....")
   --print(fileName)
   --for y = 1, 10 do
   --   print(chars[dataPointFromFile[y]].."  "..dataPointFromFile[y].."  "..chars[dataPointFromFile[y]-1].."  "..chars[dataPointFromFile[y]+1])
   --end

   return dataPointFromFile
end





chars = {}



-- NOTE THAT THE FILES COME FROM PYTHON AND WILL BE ZERO-INDEXED
function readCorpus() --as a double storage
   io.input(corpus_name..".num.b")
   t = io.read("*all")
   local counter = 0
   for line in string.gmatch(t, "[^\n]+") do
     counter = counter+1
     lineList = {}
     table.insert(corpus, lineList)
     for token in string.gmatch(line, "[0-9]+") do
        table.insert(lineList, math.min(token+1.0, params.vocab_size))
     end

    --print(line)

   end
   --corpus = torch.Tensor(corpusList)

   io.input(corpus_name..".charnums.txt")
   t = io.read("*all")

   for line in string.gmatch(t, "[^\n]+") do
     table.insert(chars, line)
   end

   if params.vocab_size > #chars then
      print(#chars)
      crash()
   end

--print(chars)

end



--readCorpus()

print("DONE READING CORPUS")






local function lstm(x, prev_c, prev_h)


  --[[local a = nn.Linear(params.rnn_size, params.rnn_size)(x)
  local b = nn.Linear(params.rnn_size, params.rnn_size)(prev_c)
  local c = nn.Linear(params.rnn_size, params.rnn_size)(prev_h)

  local next_c = nn.Sigmoid()(nn.CAddTable()({a, b, c}))
  local next_h = nn.Sigmoid()(nn.CAddTable()({a, b, c}))
return next_c, next_h]]



  -- Calculate all four gates in one go
  local i2h = nn.Linear(params.embeddings_dimensionality, 4*params.rnn_size)(x)
  local h2h = nn.Linear(params.rnn_size, 4*params.rnn_size)(prev_h)
  local gates = nn.CAddTable()({i2h, h2h})
  
  -- Reshape to (batch_size, n_gates, hid_size)
  -- Then slize the n_gates dimension, i.e dimension 2
  local reshaped_gates =  nn.Reshape(4,params.rnn_size)(gates)
  local sliced_gates = nn.SplitTable(2)(reshaped_gates)
  
  -- Use select gate to fetch each gate and apply nonlinearity
  local in_gate          = nn.Sigmoid()(nn.SelectTable(1)(sliced_gates))
  local in_transform     = nn.Tanh()(nn.SelectTable(2)(sliced_gates))
  local forget_gate      = nn.Sigmoid()(nn.SelectTable(3)(sliced_gates))
  local out_gate         = nn.Sigmoid()(nn.SelectTable(4)(sliced_gates))

  local next_c           = nn.CAddTable()({
      nn.CMulTable()({forget_gate, prev_c}),
      nn.CMulTable()({in_gate,     in_transform})
  })
  local next_h           = nn.CMulTable()({out_gate, nn.Tanh()(next_c)})

  return next_c, next_h
end


local function createAttentionNetwork()
   local x = nn.Identity()()
   local xemb = LookupTable(params.vocab_size,params.embeddings_dimensionality)(x)
   local y = nn.Identity()()
   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)
   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module = nn.gModule({x, y},
                                      {attention})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end


local function create_network(withOutput)
  local x                = nn.Identity()()
  local prev_c           = nn.Identity()()
  local prev_h           = nn.Identity()()
  local i                = LookupTable(params.vocab_size,params.embeddings_dimensionality)(x)
  local next_s           = {}
  local next_c, next_h = lstm(i, prev_c, prev_h)
  local module
  if withOutput  then
        local h2y              = nn.Linear(params.rnn_size, params.vocab_size)(next_c)
        local output = nn.MulConstant(-1)(nn.LogSoftMax()(h2y))
-- TODO softmax
      module = nn.gModule({x, prev_c, prev_h},
                                      {next_c, next_h, output})
  else
      module = nn.gModule({x, prev_c, prev_h},
                                      {next_c, next_h})
  end
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end


local function getFromAnswer(data, index, token)
              if #(data[index].a) >= token then
                  return ((data[index]).a)[token]
              else
                  return 1
              end
end

local function getFromText(data, index, token)
              if #(data[index].t) >= token then
                  return ((data[index]).t)[token]
              else
                  return 1
              end
end

local function getFromQuestion(data, index, token)
              if #(data[index].q) >= token then
                  return ((data[index]).q)[token]
              else
                  return 1
              end
end

-- assumes that each datapoint is a sequence of ints, there is no additional structure
local function getFromData(data, index, token)
              if #(data[index]) >= token then
                  return data[index][token]
              else
                  return 1
              end
end



local function buildGradientsOfProbOutputs(dsAThird, corpus, startIndex, endIndex, tokenIndex, getFunction)
      for index=startIndex,endIndex do
           if tokenIndex==0 then
               dsAThird[index - startIndex + 1][1] = 1
           else
               dsAThird[index - startIndex + 1][getFunction(corpus,index,tokenIndex)] = 1
           end
      end
    --print(dsAThird)
end


local function prepareInput(index)
    if TASK == 'autoencoding' then
       return readAFile("texts/",files[index])
    elseif TASK == 'qa' then
       local text = readAFile("texts/",files[index])
    end
end

