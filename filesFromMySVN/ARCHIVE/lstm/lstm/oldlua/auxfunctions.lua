function transfer_data(x)
  return x:cuda()
end

function makeBoolean(string)
   if string == "false" then
     return false
   elseif string == "true" then
     return true
   else
     print(string)
     crash()
   end
end

function createListOfFiles()
    local files = {}
    io.input(LIST_OF_FILES_TO_READ)
    t = io.read("*all")
    for line in string.gmatch(t, "[^\n]+") do
       table.insert(files, line)
    end
    io.input():close()
    return files
end


function readDictionary()
   io.input(dictLocation)
   t = io.read("*all")
   for line in string.gmatch(t, "[^\n]+") do
     local isInSecond = false
     local character
       for x in string.gmatch(line, "[^ ]+") do
          character = x
          if isInSecond == true then
            break
          end
          isInSecond = true
       end
       table.insert(chars, character)
   end
   io.input():close()
   assert(params.vocab_size <= #chars)
end



function createNumbersToEntityIDsIndex()
    numbersToEntityIDs = {}
    local maxEntities = 0
    for num, char in pairs(chars) do
        if char:sub(1,7) == "@entity" then
            entityNumber = char:sub(8)+1 --better add 1 here because the entities start at 0
            numbersToEntityIDs[num+0] = entityNumber
            maxEntities = math.max(maxEntities, entityNumber)
        end
    end
end


function resetFileIterator()
    corpusReading = {lastPosition = 1, currentFileContents = {}, currentFile = 0, locationsOfLastBatchChunks = {}}
end

function hasNextFile()
    return corpusReading.currentFile + params.batch_size - 1 < #files
end


--- NOTE TODO this will crash when some of the last files are shorter than params.seq_length
function readNextChunk()
   local dataPointFromFile = {}
   while corpusReading.lastPosition + params.seq_length >= #corpusReading.currentFileContents do
       corpusReading.lastPosition = 0
       corpusReading.currentFile = corpusReading.currentFile + 1
       corpusReading.currentFileContents = readAFile(files[corpusReading.currentFile])
   end
   local startPosition = corpusReading.lastPosition+1
   for i=corpusReading.lastPosition+1, corpusReading.lastPosition +params.seq_length do
       table.insert(dataPointFromFile, corpusReading.currentFileContents[i])
   end
   corpusReading.lastPosition = corpusReading.lastPosition +params.seq_length
   return dataPointFromFile, corpusReading.currentFile, startPosition, params.seq_length
end

function readNextChunkForBatchItem(batchIndex)
    local dataPointFromFile, fileNumber, startPosition, lengthOfChunk = readNextChunk()
    corpusReading.locationsOfLastBatchChunks[batchIndex] = {file = fileNumber, offset = startPosition, length = lengthOfChunk}
    return dataPointFromFile
end


function readNumericalValuesFile(fileName)
   local values = {}
   print(surprisalAnnotationDir..fileName..".surp.anno")
   io.input(surprisalAnnotationDir..fileName..".surp.anno")
   t = io.read("*all")
   local counter = 0
   for line in string.gmatch(t, "[^\n]+") do
     counter = 0
     local lineList = {}
     for token in string.gmatch(line, "[^ ]+") do
        counter = counter+1
        table.insert(lineList, token)
     end
     if #lineList == 3 then
        values[lineList[1]+0] = lineList[3]+0.0
     end
   end
   io.input():close()
   return values
end

function getSurprisalValuesForBatchItem(batchIndex)
    local numericalValuesCurrent = {}
    local start = corpusReading.locationsOfLastBatchChunks[batchIndex].offset
    if start == 1 then
        numericalValuesImporter.currentNumericalValuesContents = readNumericalValuesFile(files[corpusReading.currentFile])
    end
    for i=start, start+corpusReading.locationsOfLastBatchChunks[batchIndex].length-1 do
       local value = numericalValuesImporter.currentNumericalValuesContents[i]
       if value == nil then
            value = -1
       end
       table.insert(numericalValuesCurrent, value)
    end
    numericalValuesImporter.values[batchIndex] = numericalValuesCurrent
end







function readAFile(fileName,intermediateDir, boundByMaxChar)
   local dataPointFromFile = {}
   if intermediateDir == nil then
      intermediateDir = ""
   end
   if boundByMaxChar == nil then
      boundByMaxChar = true
   end
   io.input(corpusDir..intermediateDir..fileName)
   t = io.read("*all")
   local counter = 0
   for line in string.gmatch(t, "[^\n]+") do
     counter = counter+1
     lineList = {}
     for token in string.gmatch(line, "[0-9]+") do
        if boundByMaxChar then
           token = math.min(token+1, params.vocab_size)
        else
           token = token + 1
        end
        table.insert(dataPointFromFile, token)
     end
   end
   io.input():close()
   return dataPointFromFile
end






function readNextQAItem(batchIndex)
    ::getNext::
    corpusReading.currentFile = corpusReading.currentFile + 1
    local q = readAFile(files[corpusReading.currentFile], "/questions/", true)
    local a = readAFile(files[corpusReading.currentFile], "/answers/", false)
    local t = readAFile(files[corpusReading.currentFile], "/texts/", true)


-- some deepmind-corpus-specific things
-- NOTE the next three lines might be sensitive to subtle changes in the input format
    if #a > 1 then
       a = {a[2]}
    end

-- TODO NOTE goto could lead to crash at the end of the corpus, since a new eleemnt will be retrieved without asking hasNext
    if #q > MAX_LENGTH_Q_FOR_QA then
       print(476)
       print(#q)
       --goto getNext
    end
    if #t > MAX_LENGTH_T_FOR_QA then
       print(480)
       print(#t)
       --goto getNext
    end



    return {question = q, answer = a, text = t}
end



chars = {}



-- NOTE THAT THE FILES COME FROM PYTHON AND WILL BE ZERO-INDEXED
function readCorpus() --as a double storage
   io.input(corpus_name..".num.b")
   t = io.read("*all")
   local counter = 0
   for line in string.gmatch(t, "[^\n]+") do
     counter = counter+1
     lineList = {}
     table.insert(corpus, lineList)
     for token in string.gmatch(line, "[0-9]+") do
        table.insert(lineList, math.min(token+1.0, params.vocab_size))
     end

    --print(line)

   end
   --corpus = torch.Tensor(corpusList)
   io.input():close()


   io.input(corpus_name..".charnums.txt")
   t = io.read("*all")

   for line in string.gmatch(t, "[^\n]+") do
     table.insert(chars, line)
   end
   
   if params.vocab_size > #chars then
      print(#chars)
      crash()
   end
   io.input():close()
--print(chars)

end


function getFromData(data, index, token)
              if #(data[index]) >= token then
                  return data[index][token]
              else
                  return 1
              end
end


function getFromAnswer(data, index,token)
   local answer = data[index].answer
   if #answer >= token then
       return answer[token]
   else
       return 1
   end
end

function getFromText(data, index,token)
   local text = data[index].text
   if #text >= token then
       return text[token]
   else
       return 1
   end
end

function getFromQuestion(data, index,token)
   local question = data[index].question
   if #question >= token then
       return question[token]
   else
       return 1
   end
end


function buildInputTensorsForSubcorpus(data, startIndex, endIndex, getFromFunction, maxLength)
    local inputTensors = {}
    for token=0, maxLength  do
      local inputTensor = torch.CudaTensor(params.batch_size)
      -- the batch elements
      for index=startIndex,endIndex do
           if token==0 then
              inputTensor[index-startIndex+1] = 1
           else
              inputTensor[index-startIndex+1] = getFromFunction(data,index,token)
           end
      end
      inputTensors[token] = inputTensor
    end
    return inputTensors
end

function buildInputTensors(data, startIndex, endIndex)
    return buildInputTensorsForSubcorpus(data, startIndex, endIndex, getFromData, params.seq_length)
end

function getAttentionFromFile()
            local f = assert(io.open("attention-"..arg[1], 'r'))
            local t = f:read("*all")
            f:close()
            for line in string.gmatch(t, "[^\n]+") do
              if line:len() > 1 then
                FIXED_ATTENTION = line+0.0
              end
            end
end

function getLearningRateFromFile()
            local f2 = assert(io.open("lr-"..arg[1], 'r'))
            local t2 = f2:read("*all")
            f2:close()
            for line in string.gmatch(t2, "[^\n]+") do
              if line:len() > 1 then
                params.lr = line+0.0
              end
            end
end

function getAttentionLearningRateFromFile()
            local f2 = assert(io.open("lr-att-"..arg[1], 'r'))
            local t2 = f2:read("*all")
            f2:close()
            for line in string.gmatch(t2, "[^\n]+") do
              if line:len() > 1 then
                params.lr_att = line+0.0
              end
            end

end



function printSkippingStatistics(corpus)
   --[[table.sort(total_encountered_statistics,  function(a1, a2)
                                                  return (a1[2]/a1[1] < a2[2]/a2[1])
                                              end
              )]]
   local skippingStatisticsTable = {}
   for i=1,#has_seen_in_skipping_statistics do
     if has_seen_in_skipping_statistics[i] ~= nil then
        if total_encountered_statistics[has_seen_in_skipping_statistics[i]][1] > 100 then
           --print(chars[i].."  "..((100.0 * total_encountered_statistics[has_seen_in_skipping_statistics[i]][2]) / total_encountered_statistics[has_seen_in_skipping_statistics[i]][1]))
           table.insert(skippingStatisticsTable,{chars[i], ((100.0 * total_encountered_statistics[has_seen_in_skipping_statistics[i]][2]) / total_encountered_statistics[has_seen_in_skipping_statistics[i]][1])})
        end
     end
   end
   table.sort(skippingStatisticsTable,  function(a1, a2)
                                                  return (a1[2] < a2[2])
                                              end
             )
   for i=1, #skippingStatisticsTable do
        print(skippingStatisticsTable[i][1].."  "..skippingStatisticsTable[i][2])
   end
end


 
function updateSkippingStatistics(corpus)
  for i=1, params.seq_length do
    for item = 1, params.batch_size do
      local token = getFromData(corpus,item,i)
      if has_seen_in_skipping_statistics[token] == nil then
           has_seen_in_skipping_statistics[token] = (#total_encountered_statistics)+1
           table.insert(total_encountered_statistics, {1,0})
      else
           total_encountered_statistics[has_seen_in_skipping_statistics[token]][1] = total_encountered_statistics[has_seen_in_skipping_statistics[token]][1] +1
      end
      if attention_decisions[i][item] == 0 then
           total_encountered_statistics[has_seen_in_skipping_statistics[token]][2] = total_encountered_statistics[has_seen_in_skipping_statistics[token]][2] + 1
      end
    end
  end
end


function storeAttentionAnnotation(corpus)
   for batchIndex = 1, params.batch_size do
       --corpusReading.locationsOfLastBatchChunks[batchIndex] = {file = fileNumber, offset = startPosition, length = lengthOfChunk}
       local fileOut = io.open(ATTENTION_ANNOTATION_DIR.."/"..files[corpusReading.locationsOfLastBatchChunks[batchIndex].file]..".att.anno", "a")
       print(ATTENTION_ANNOTATION_DIR.."/"..files[corpusReading.locationsOfLastBatchChunks[batchIndex].file]..".att.anno")
       fileOut:write("++\n")
       for j=1, params.seq_length do
          fileOut:write((corpusReading.locationsOfLastBatchChunks[batchIndex].offset + j - 1).."  "..getFromData(corpus,batchIndex,j).."  "..attention_decisions[j][batchIndex].."  "..attention_scores[j][batchIndex][1].."\n")
       end
       fileOut:close()
   end
end

function storeSurprisalScores(corpus)
   for batchIndex = 1, params.batch_size do
       local fileOut = io.open(SURPRISAL_ANNOTATION_DIR.."/"..files[corpusReading.locationsOfLastBatchChunks[batchIndex].file]..".surp.anno", "a")
       if torch.uniform() > 0.99 then
            print(SURPRISAL_ANNOTATION_DIR.."/"..files[corpusReading.locationsOfLastBatchChunks[batchIndex].file]..".surp.anno")
        end
       fileOut:write("++\n")
       for j=1, params.seq_length do
          fileOut:write((corpusReading.locationsOfLastBatchChunks[batchIndex].offset + j - 1).."  "..getFromData(corpus,batchIndex,j).."  "..(actor_output[j][batchIndex][getFromData(corpus,batchIndex,j)]).."\n")
       end
       fileOut:close()
   end
end


function buildGradientsOfProbOutputs(dsAThird, corpus, startIndex, endIndex, tokenIndex)
      for index=startIndex,endIndex do
           if tokenIndex==0 then
               dsAThird[index - startIndex + 1][1] = 1
           else
               dsAThird[index - startIndex + 1][getFromData(corpus,index,tokenIndex)] = 1
           end
      end
end



































