function createAttentionNetworkOneHot()
   local x = nn.Identity()()
   local y = nn.Identity()()
   local x2h = LookupTable(params.vocab_size, params.rnn_size)(x)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)
   local hidden
   local surprisal
   if params.INCLUDE_SURPRISAL_VALUES then
       surprisal = nn.Identity()
       hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h, surprisal}))
   else
       hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h}))
   end
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module
   if params.INCLUDE_SURPRISAL_VALUES then
       module = nn.gModule({x, y, surprisal},
                                      {attention})
   else
       module = nn.gModule({x, y},
                                      {attention})
   end
   module:getParameters():uniform(-params.init_weight, params.init_weight)
   return transfer_data(module)
end


function createAttentionNetworkEmbeddingsSurprisal()
   if not params.INCLUDE_SURPRISAL_VALUES then
      crash()
   end
   local x = nn.Identity()()
   local xemb = LookupTable(params.vocab_size,params.embeddings_dimensionality)(x)
   local y = nn.Identity()()
   local surprisal = nn.Identity()()

   -- hqhq
   xemb = nn.MulConstant(0)(xemb)


   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)

   -- hqhq
   y2h = nn.MulConstant(0)(y2h)


   local z2h = nn.Linear(1, params.rnn_size)(surprisal)





   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h, z2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module = nn.gModule({x, y, surprisal},
                                      {attention})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end


function createAttentionNetworkEmbeddings()
   if params.INCLUDE_SURPRISAL_VALUES then
      return createAttentionNetworkEmbeddingsSurprisal()
   end
   local x = nn.Identity()()
   local xemb = LookupTable(params.vocab_size,params.embeddings_dimensionality)(x)
   local y = nn.Identity()()
   local x2h = nn.Linear(params.embeddings_dimensionality, params.rnn_size)(xemb)
   local y2h = nn.Linear(params.rnn_size, params.rnn_size)(y)
   local hidden = nn.Sigmoid()(nn.CAddTable()({x2h, y2h}))
   local attention = (nn.Sigmoid()(nn.Linear(params.rnn_size, 1)(hidden))) --nn.Log()
   local module = nn.gModule({x, y},
                                      {attention})
  module:getParameters():uniform(-params.init_weight, params.init_weight)
  return transfer_data(module)
end

