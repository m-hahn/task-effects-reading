import os.path

jags = map(str,[9, 10, 11])

for num in range(1700,1800):
  for jagNum in jags:
   outFile = '/jagupard'+jagNum+'/scr1/mhahn/cuny-split-bcontinuing-2-pilotattention-'+str(num)+'-l1-nll-sandbox-QATT.txt'
   if os.path.isfile(outFile):
    with open(outFile,"r") as stream:
      stream = stream.read()
      stream = stream[int(len(stream) * 0.98):]
      if "Training is over." in stream:
        if 'fullpreview' in stream:
          condition = 'fullpreview'
        elif 'fullnopreview' in stream:
          condition = 'fullnopreview'
        else:
          condition = 'mixed'
        os.chmod(outFile, 0444)
        command = '/u/nlp/bin/stake.py -g 11.5g -s run-stats-cuny-300.json "/afs/cs.stanford.edu/u/mhahn/torch/install/bin/th main-attention.lua 57 false true 64 500 128 50000 6.0 true 0.0 100 0.0001 0.6 true pg-test-neat-qa-1800-128-0.7-100-R-5cuny-split-bcontinuing-2   cuny-split-bcontinuing-2-pilotattention-'+str(num)+'-l1-nll neat-qa true 5 true 0.01 full true fixed '+condition+'" > /jagupard'+jagNum+'/scr1/mhahn/cuny-split-bcontinuing-2-pilotattention-'+str(num)+'-l1-nll-sandbox-QATT.txt'
        print(command+"\n")
    break

