import os

bestP = 100
bestNP = 100

aggregateP = []
aggregateNP = []
aggregateTotal = []


make_r_table = True

if make_r_table:
  modelsToLookOutFor = map(str,[300, 301, 302, 306, 307, 308, 310, 311, 312, 313, 316, 317, 318, 319, 810, 850, 851, 852, 860, 861, 870, 880, 884]+range(1400,1500)+range(1700,1800)) #, 990]) # 990 is different because the QA system was trained together with the attention
  rOut = open("/afs/cs.stanford.edu/u/mhahn/scr/collect-tradeoff-for-r.txt","w")
  print >> rOut, "\t".join(["model","Condition","Att","Acc","Nll"])

# for 1700's
directories = ["/jagupard9/scr1/mhahn/", "/jagupard11/scr1/mhahn/"]
# for 300's, 500's, 800's, 1400's
#directories = ["/jagupard10/scr1/mhahn/"]

for directory in directories:
  files = os.listdir(directory) 
  files = filter(lambda x:"OUT-DEV-" in x, files)
  for fileName in files:
    with open(directory+"/"+fileName,"r") as inFile:
      data = inFile.read().split("\n")
      indices = [i for i, x in enumerate(data) if "Total NLLs" in x]
      if len(indices) > 0:
        print(fileName)
        if (not ("-l1-nll" in fileName)):
          print("Skipping this one")
          continue
        condition = fileName[8:fileName.index("-18-")]
  
  
        #print(condition)
        index = indices[-1]
        nllNoPreview = data[index+1]
        nllPreview = data[index+2]
        assert("G No Preview" in nllNoPreview)
        assert("G Preview" in nllPreview)
  
        attNoPreview = data[index+7]
        attPreview = data[index+8]
  
        accNoPreview = data[index+14]
        accPreview = data[index+15]
  #      print(accNoPreview)
   #     print(attNoPreview)
        
        nllNoPreview = float(nllNoPreview.replace("G Preview","").replace("G No Preview",""))
        nllPreview = float(nllPreview.replace("G Preview","").replace("G No Preview",""))
        attNoPreview = float(attNoPreview.replace("G Preview","").replace("G No Preview",""))
        attPreview = float(attPreview.replace("G Preview","").replace("G No Preview",""))
        accNoPreview = float(accNoPreview.replace("G Preview","").replace("G No Preview",""))
        accPreview = float(accPreview.replace("G Preview","").replace("G No Preview",""))
  
  #      print(nllNoPreview)
   #     print(nllPreview)
    #    print(attNoPreview)
     #   print(attPreview)
        weight = 2.25 #25
        rewardNP = nllNoPreview+weight*attNoPreview
        rewardP = nllPreview+weight*attPreview
        
  
        reward = 0
        acc = 0
        if (not condition == "fullpreview"):
          aggregateNP.append((fileName,rewardNP))
          print("NP "+str(rewardNP)+"  \t"+str(accNoPreview)+"  \t"+str(attNoPreview))
          reward = reward + rewardNP
          acc = acc + accNoPreview
          if rewardNP <  bestNP:
             bestNP = rewardNP
        if (not condition == "fullnopreview"):
          aggregateP.append((fileName,rewardP))
          print("P  "+str(rewardP)+"  \t"+str(accPreview)+"  \t"+str(attPreview))
          reward = reward + rewardP
          acc = acc + accPreview
          if rewardP <  bestP:
             bestP = rewardP
        if (condition == "mixed"):
          aggregateTotal.append((fileName,reward/2.0))
          reward = reward/2.0
          acc = acc/2.0
        print("#  "+str(reward)+"  \t"+str(acc))
        if make_r_table:
          conditionNumber = (fileName[0:fileName.index("-l1-nll")])
          conditionNumber = conditionNumber[conditionNumber.rfind("-")+1:]
          #print(conditionNumber)
          #print(conditionNumber in modelsToLookOutFor)
          if conditionNumber in modelsToLookOutFor:
            print >> rOut, "\t".join(map(str,[conditionNumber,"preview",attPreview,accPreview,nllPreview]))
            print >> rOut, "\t".join(map(str,[conditionNumber,"nopreview",attNoPreview,accNoPreview,nllNoPreview]))

if make_r_table:
 rOut.close()

print("BEST")
print(bestP)
print(bestNP)

aggregateP = sorted(aggregateP, key=lambda x:x[1])
aggregateNP = sorted(aggregateNP, key=lambda x:x[1])
aggregateTotal = sorted(aggregateTotal, key=lambda x:x[1])

print("================\nP")
print("\n".join(map(str,aggregateP)))
print("================\nNP")
print("\n".join(map(str,aggregateNP)))
print("================\nTotal")
print("\n".join(map(str,aggregateTotal)))


