from subprocess import call
from subprocess import Popen, PIPE
import sys

# was used to create 1200, 1400

start = int(sys.argv[1])
end = int(sys.argv[2])
gpu = int(sys.argv[3])

jagNumber = 10

print("Note: you have to set L1 in the Lua code!")

# /u/nlp/bin/stake.py -g 11.5g -s run-stats-pretrain6543.json "
for number in range(start,end):
#  call("kinit", shell=True)
  basicCommand = 'CUDA_VISIBLE_DEVICES='+str(gpu)+'  /afs/cs.stanford.edu/u/mhahn/torch/install/bin/th main-attention.lua 57 false true 64 500 128 50000 6.0 true 0.0 100 0.0001 0.6 true pg-test-neat-qa-1800-128-0.7-100-R-5cuny-split-bcontinuing-2   cuny-split-bcontinuing-2-pilotattention-'+str(number)+'-l1-nll neat-qa true 5 true 0.01 full true fixed mixed > /jagupard'+str(jagNumber)+'/scr1/mhahn/cuny-split-bcontinuing-2-pilotattention-'+str(number)+'-l1-nll-sandbox-QATT.txt'

  print("TO EXECUTE")
  print(basicCommand)
  call(basicCommand, shell=True)
