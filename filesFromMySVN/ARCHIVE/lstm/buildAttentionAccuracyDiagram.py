conditions = ["preview", "nopreview"]
steps = [x * 0.3 for x in range(-6,6)]
models = ["cuny-split-bcontinuing-2-pilotattention-112-nll-sandbox-QATT.txt", "cuny-split-bcontinuing-2-pilotattention-113-nll-sandbox-QATT.txt"]

results = {}

taskPerformanceDiff = {}

for model in models:
  taskPerformanceDiff[model] = {}
  for condition in conditions:
    taskPerformanceDiff[model][condition] = {}
    results[(condition,model)] = []
    for step in steps:
      if (not (step in taskPerformanceDiff )):
         taskPerformanceDiff[model][step] = {}
      fileName = "/jagupard12/scr1/mhahn/OUT-"+condition+"-5-"+str(step)+"-"+model
      with open(fileName,"r") as inputFile:
        data = inputFile.read().split("\n")
        relevant = zip(range(-30,-1),data[-30:-1])
        assert ("G No Preview" in data[-20])
        assert ("G No Preview" in data[-13])
        assert ("G Preview" in data[-19])
        assert ("G Preview" in data[-12])
        fixations = map(lambda x : float(x.rstrip().split(" ")[-1]), data[-20:-18])
        accuracies = map(lambda x : float(x.rstrip().split(" ")[-1]), data[-13:-11])
        if fixations[0] != 0 and fixations[1] != 0:
          print("Problem")
        if accuracies[0] != 0 and accuracies[1] != 0:
          print("Problem")
        conditionNumber = {"nopreview" : 0, "preview" : 1}[condition]
        fixations = fixations[conditionNumber]
        accuracies = accuracies[conditionNumber]
        results[(condition,model)].append("("+str(int(fixations*100))+","+str(int(accuracies*100))+")  +=(0,20) -=(0,17) ")
        print(condition+" "+model)
        print(fixations)
        print(accuracies)
        taskPerformanceDiff[model][condition][step] = (fixations,accuracies)
for combo in results:
   print(combo)
   print(" ".join(results[combo]))
for model in models:
   for step in steps:
      print(step)
      print(taskPerformanceDiff[model]["preview"][step][1] - taskPerformanceDiff[model]["nopreview"][step][1])
      print(taskPerformanceDiff[model]["preview"][step])
      print(taskPerformanceDiff[model]["nopreview"][step])
   print(taskPerformanceDiff[model])
   


#OUT-preview-5-0.3-cuny-split-bcontinuing-2-pilotattention-112-nll-sandbox-QATT.txt


