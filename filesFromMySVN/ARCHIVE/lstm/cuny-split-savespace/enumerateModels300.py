import os.path

for num in range(300,355):
  outFile = '/jagupard11/scr1/mhahn/cuny-split-bcontinuing-2-pilotattention-'+str(num)+'-l1-nll-sandbox-QATT.txt'
  if os.path.isfile(outFile):
    with open(outFile,"r") as stream:
      stream = stream.read()
      if "Training is over." in stream:
        os.chmod(outFile, 0444)
        command = '/u/nlp/bin/stake.py -g 11.5g -s run-stats-cuny-300.json "/afs/cs.stanford.edu/u/mhahn/torch/install/bin/th main-attention.lua 57 false true 64 500 128 50000 6.0 true 0.0 100 0.0001 0.6 true pg-test-neat-qa-1800-128-0.7-100-R-5cuny-split-bcontinuing-2   cuny-split-bcontinuing-2-pilotattention-'+str(num)+'-l1-nll neat-qa true 5 true 0.01 full true fixed mixed" > /jagupard11/scr1/mhahn/cuny-split-bcontinuing-2-pilotattention-'+str(num)+'-l1-nll-sandbox-QATT.txt'
        print(command+"\n")
