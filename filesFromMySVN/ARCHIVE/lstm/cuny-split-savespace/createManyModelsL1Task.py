from subprocess import call
from subprocess import Popen, PIPE
import sys

start = int(sys.argv[1])
end = int(sys.argv[2])
gpu = int(sys.argv[3])
task = sys.argv[4]

assert task in ["preview","nopreview"]

print("Note: you have to set L1 in the Lua code!")

# /u/nlp/bin/stake.py -g 11.5g -s run-stats-pretrain6543.json "
for number in range(start,end):
  basicCommand = 'CUDA_VISIBLE_DEVICES='+str(gpu)+'  /afs/cs.stanford.edu/u/mhahn/torch/install/bin/th main-attention.lua 57 false true 64 500 128 50000 6.0 true 0.0 100 0.0001 0.6 true pg-test-neat-qa-1800-128-0.7-100-R-5cuny-split-bcontinuing-2   cuny-split-bcontinuing-2-pilotattention-'+str(number)+'-l1-nll neat-qa true 5 true 0.01 full true fixed '+'full'+task+' > /jagupard11/scr1/mhahn/cuny-split-bcontinuing-2-pilotattention-'+str(number)+'-l1-nll-sandbox-QATT.txt'

  print("TO EXECUTE")
  print(basicCommand)
  call(basicCommand, shell=True)
