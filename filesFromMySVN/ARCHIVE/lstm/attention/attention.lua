require 'nn';
require 'rnn';

corpus = {}

function readCorpus() --as a double storage
   io.input("hlm.txt.num")
   t = io.read("*all")
   counter = 1
   for token in string.gmatch(t, "[0-9]+") do
      corpus[counter] = token+0.0
      counter = counter+1
      if counter>100 then
         break
      end
   end
   --corpus = torch.Tensor(corpusList)
end

readCorpus()

print("DONE READING CORPUS")




-------------------




opt = {hiddenSize = 10, nIndex=30, uniform = 0.01}

rho = 100

-- autoencoder without attention

--- this one reads a sentence. each forward call will mean feeding a new timestep
model = nn.Sequential()


reader = nn.Recurrent(10, nn.Linear(3, opt.hiddenSize), nn.Linear(opt.hiddenSize, opt.hiddenSize), nn.Sigmoid(), 99999)




model:add(reader)
--model:add(nn.Sequencer(reconstructor))
model:add(nn.Linear(opt.hiddenSize, opt.nIndex))
model:add(nn.LogSoftMax())



--

locator = nn.Sequential()
locator:add(nn.Linear(opt.hiddenSize, 2))
locator:add(nn.HardTanh()) -- bounds mean between -1 and 1
locator:add(nn.ReinforceBernoulli(true)) -- sample from normal, uses REINFORCE learning rule
locator:add(nn.HardTanh()) -- bounds sample between -1 and 1
--locator:add(nn.MulConstant(opt.unitPixels*2/ds:imageSize("h")))




attention = nn.RecurrentAttention(reader, locator, 100, {opt.hiddenSize})

---------

-- model is a reinforcement learning agent
agent = nn.Sequential()
--agent:add(nn.Convert(ds:ioShapes(), 'bchw'))
agent:add(attention)

-- classifier :
agent:add(nn.SelectTable(-1))
agent:add(nn.Linear(opt.hiddenSize, 1000))
agent:add(nn.LogSoftMax())



for k,param in ipairs(agent:parameters()) do
      param:uniform(-opt.uniform, opt.uniform)
end



criterion = nn.ClassNLLCriterion()



--reconstructor = nn.Recurrent(opt.hiddenSize, nn.Identity()(), nn.Linear(opt.hiddenSize, opt.hiddenSize), nn.Sigmoid(), 99999)






------------------------------------------------------------------------
--[[ Sequencer ]]--
-- Encapsulates a Module. 
-- Input is a sequence (a table) of tensors.
-- Output is a sequence (a table) of tensors of the same length.
-- Applies the module to each element in the sequence.
-- Handles both recurrent modules and non-recurrent modules.
-- The sequences in a batch must have the same size.
-- But the sequence length of each batch can vary.
------------------------------------------------------------------------

------------------------------------------------------------------------
--[[ SequencerCriterion ]]--
-- Applies a criterion to each of the inputs and targets in the 
-- corresponding input and target Tables.
-- Useful for nn.Repeater and nn.Sequencer.
-- WARNING : assumes that the decorated criterion is stateless, i.e. 
-- the backward doesn't need to be preceded by a commensurate forward.
------------------------------------------------------------------------

function gradientUpgrade(model, x, y, criterion, learningRate, i)
   local prediction = model:forward(x)
   local err = criterion:forward(prediction, y)
   local gradOutputs = criterion:backward(prediction, y)
   -- the Recurrent layer is memorizing its gradOutputs (up to memSize)
   model:backward(x, gradOutputs)

   if i % 100 == 0 then
      print('error for iteration ' .. i  .. ' is ' .. err/rho)
   end

   if i % 3 == 0 then
      -- backpropagates through time (BPTT) :
      -- 1. backward through feedback and input layers,
      -- 2. updates parameters
      model:backwardThroughTime()
      model:updateParameters(learningRate)
      model:zeroGradParameters()
   end
end


input = torch.Tensor(torch.DoubleStorage({3,6,1}))
dataset={{input,1}};


function one_hot(num)
   local vec = torch.Tensor(5000):zero()
   vec[num] = 1
   return vec
end


for i = 1, #corpus do
   local inputs, targets = {}, {}

   inputs[i] = one_hot(corpus[i])
   targets[i] = i

   i = gradientUpgrade(model, inputs[i], targets[i], criterion, 0.01, i)
end



--------------






locationSensor = nn.Sequential()
locationSensor:add(nn.SelectTable(2))
locationSensor:add(nn.Linear(2, opt.locatorHiddenSize))
locationSensor:add(nn[opt.transfer]())


glimpseSensor = nn.Sequential()
glimpseSensor:add(nn.DontCast(nn.SpatialGlimpse(opt.glimpsePatchSize, opt.glimpseDepth, opt.glimpseScale):float(),true))
glimpseSensor:add(nn.Collapse(3))
glimpseSensor:add(nn.Linear(ds:imageSize('c')*(opt.glimpsePatchSize^2)*opt.glimpseDepth, opt.glimpseHiddenSize))
glimpseSensor:add(nn[opt.transfer]())




glimpse = nn.Sequential()
glimpse:add(nn.ConcatTable():add(locationSensor):add(glimpseSensor))
glimpse:add(nn.JoinTable(1,1))
glimpse:add(nn.Linear(opt.glimpseHiddenSize+opt.locatorHiddenSize, opt.imageHiddenSize))
glimpse:add(nn[opt.transfer]())
glimpse:add(nn.Linear(opt.imageHiddenSize, opt.hiddenSize))




-- recurrent neural network
rnn = nn.Recurrent(opt.hiddenSize, glimpse, recurrent, nn.Sigmoid(), 99999)

--transfer: a non-linear Module used to process the element-wise sum of the input and feedback module outputs




-- model is a reinforcement learning agent
agent = nn.Sequential()
agent:add(nn.Convert(ds:ioShapes(), 'bchw'))
agent:add(attention)

-- classifier :
agent:add(nn.SelectTable(-1))
agent:add(nn.Linear(opt.hiddenSize, #ds:classes()))
agent:add(nn.LogSoftMax())




---

---




-- next steps

--- recurrent, two networks

--- LSTM



dataset={};
function dataset:size() return 100 end -- 100 examples
for i=1,dataset:size() do 
  local input = torch.randn(2);     -- normally distributed example in 2d
  local output = torch.Tensor(1);
  if input[1]*input[2]>0 then     -- calculate label for XOR function
    output[1] = -1;
  else
    output[1] = 1
  end
  dataset[i] = {input, output+1}
end



net = nn.Sequential()
net:add(nn.Linear(2, 5))             -- fully connected layer (matrix multiplication between input and weights)
net:add(nn.Tanh())
net:add(nn.Linear(5, 7))
net:add(nn.Tanh())
net:add(nn.Linear(7, 1))                   -- 10 is the number of outputs of the network (in this case, 10 digits)
net:add(nn.Tanh())
--net:add(nn.LogSoftMax())                     -- converts the output to a log-probability. Useful for classification problems

print('Lenet5\n' .. net:__tostring());


criterion = nn.MSECriterion() 
trainer = nn.StochasticGradient(net, criterion)
trainer.learningRate = 0.01
trainer:train(dataset)

print("END")





-------

trainset =  {{torch.Tensor(10),1},{torch.Tensor(10),2},{torch.Tensor(10),1}}
testset =  {{torch.Tensor(10),1}}
-- ignore setmetatable for now, it is a feature beyond the scope of this tutorial. It sets the index operator.
--setmetatable(trainset, 
--    {__index = function(t, i) 
--                    return t[i]
--                end}
--);
--trainset.data = trainset.data:double() -- convert the data from a ByteTensor to a DoubleTensor.

function trainset:size() 
    return self:size(1) 
end


input = torch.rand(10) -- pass a random tensor as input to the network
output = net:forward(input)
print(output)
net:zeroGradParameters() -- zero the internal gradient buffers of the network (will come to this later)
gradInput = net:backward(input, torch.rand(10))
print(#gradInput)



criterion = nn.ClassNLLCriterion() -- a negative log-likelihood criterion for multi-class classification
criterion:forward(output, 3) -- let's say the groundtruth was class number: 3
gradients = criterion:backward(output, 3)

gradInput = net:backward(input, gradients)

print(53)

trainer = nn.StochasticGradient(net, criterion)
trainer.learningRate = 0.001
trainer.maxIteration = 1 -- just do 5 epochs of training.

print(59)

trainer:train(dataset)

print(63)




