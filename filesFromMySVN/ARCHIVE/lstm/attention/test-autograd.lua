    t = require 'torch'
    grad = require 'autograd'
    
    params = {
       a = t.randn(1,1)
    }

    randomNumbers = {0.1, 0.5, 0.3}


    f = function(params, x)
       local result = 1 + t.sum(x * params.a)
       return (result) --math.log
    end

    df = grad(f)

    x = t.randn(1,1)

    print(f(params, x))

    print(df(params, x))
