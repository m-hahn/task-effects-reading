-- Import libraries 
local grad = require 'autograd'
local util = require 'autograd.util'
local t = require 'torch'
  
  
inputSize = 10
recurrentSize = 5
  
corpus = {}

settings = {stepsize = 0.01, range = 0.01}
  
-- NOTE THAT THE FILES COME FROM PYTHON AND WILL BE ZERO-INDEXED
function readCorpus() --as a double storage
   io.input("hlm.txt.num.b")
   t = io.read("*all")
   counter = 0
   for line in string.gmatch(t, "[^\n]+") do
     counter = counter+1
     lineList = {}
     table.insert(corpus, lineList)
     for token in string.gmatch(line, "[0-9]+") do
        table.insert(lineList, math.min(token+1.0, inputSize))
     end
     if counter>100 then
        break
     end
   end
   --corpus = torch.Tensor(corpusList)
end
  
readCorpus()
  
print("DONE READING CORPUS")
  
  
  
  
function one_hot(num)
   local vec = torch.FloatTensor(inputSize):zero()
   vec[math.min(num, inputSize)] = 1
   return vec
end
  
  
local function read(params, inputs)
   local hiddenState = params.initialHidden
   --print(inputs)
   for _,input in pairs(inputs) do
      input = (one_hot(input))
      --print(params.Wrih)
      --print(input)
      --print(params.Wrhh)
      --print(hiddenState)
  
      local part1 = params.Wrih * input
      local part2 = params.Wrhh * hiddenState
      --print(part1)
      --print(part2)
      hiddenState = util.sigmoid(part1 + part2)
   end
   return hiddenState,1
end
--where the second thing will be the probability of the glimpse sequence
  
local function reconstruct(params, inputs, hiddenState)
   prediction = {}
   lastInput = params.startOfSentence
   nll = 0
   for _,input in pairs(inputs) do
      input = one_hot(input)
      output = util.logSoftMax(params.Waio * lastInput + params.Waho * hiddenState)
      hiddenState = util.sigmoid(params.Waih * lastInput + params.Wahh * hiddenState)
      lastInput = input
      nll = nll - input * output
      table.insert(prediction, output)
   end
   return nll, prediction
end
  
local function apply(params, inputs)
    local hiddenState, prob = read(params, inputs)
    local nll, _ = reconstruct(params, inputs, hiddenState)
    return nll, prob
end


local function applyAndPrint(params, inputs)
    local hiddenState, prob = read(params, inputs)
    local nll, prediction = reconstruct(params, inputs, hiddenState)
    print("+++++++")
    print(nll.."  "..prob)
    for i=1, #inputs do
        --print(torch.max(prediction[i]),1)
        local predictedScores, predictedTokens = torch.max(prediction[i],1)
        --print(inputs[i])
        --print(predictedTokens)
        --print(predictedScores)
        --print(prediction)
        --print(prediction[i])
        --print(inputs[i])
        --print(prediction[i][inputs[i]])
        print(inputs[i].."  "..predictedTokens[1].."  "..math.exp(predictedScores[1]).."  "..math.exp(prediction[i][inputs[i]]))
    end
end
  
local function costNLL(params, inputs)
    local nll, prob = apply(params, inputs)
    return nll
end
  
-- Get a function that will automatically compute gradients
-- of our loss function
local dcostNLL = grad(costNLL)
  
-- Note: at this point, weâre completely done writing our model.
-- All gradients will be taken care of automatically.
  
  
  
  
  
-- Define our parameters
local params = {
   initialHidden = torch.FloatTensor(recurrentSize),
   Wrih = torch.FloatTensor(recurrentSize,inputSize),
   Wrhh  = torch.FloatTensor(recurrentSize,recurrentSize),
   startOfSentence  = torch.FloatTensor(inputSize),
   Waio  = torch.FloatTensor(inputSize,inputSize),
   Waho  = torch.FloatTensor(inputSize,recurrentSize),
   Waih  = torch.FloatTensor(recurrentSize,inputSize),
   Wahh  = torch.FloatTensor(recurrentSize,recurrentSize)
}
  
  
  
-- Now, we can train with e.g. stochastic gradient descent
for epoch = 1,100 do
   for i = 1,#corpus do
      --if #corpus[i] < 20 then
      --   break
      --end
       
      -- Get a data sample:
      -- TODO
      local x = corpus[i]
  
      applyAndPrint(params,x)
  
  
      -- Calculate gradients (this is the function we defined above,
      -- and where all of autogradâs magic happens)
      local grads, loss = dcostNLL(params,x)
  
      print(loss)
      print(grads)


      -- Update weights and biases
      params.initialHidden = params.initialHidden - grads.initialHidden * settings.stepsize
      params.Wrih = params.Wrih - grads.Wrih * settings.stepsize
      params.Wrhh  = params.Wrhh - grads.Wrhh * settings.stepsize
      params.startOfSentence  = params.startOfSentence - grads.startOfSentence * settings.stepsize
      params.Waio  = params.Waio - grads.Waio * settings.stepsize
      params.Waho  = params.Waho - grads.Waho * settings.stepsize
      params.Waih  = params.Waih - grads.Waih * settings.stepsize
      params.Wahh  = params.Wahh - grads.Wahh * settings.stepsize
   end
end
