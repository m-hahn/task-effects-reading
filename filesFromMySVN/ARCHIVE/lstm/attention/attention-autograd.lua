-- TODO
-- . momentum
-- . differentiate loss and attention stuff together?

-- Import libraries 
local grad = require 'autograd'
local util = require 'autograd.util'
local t = require 'torch'
require "cutorch"

cutorch.setDevice(1)


tensorType = torch.FloatTensor --CudaTensor --FloatTensor

torch.manualSeed(123)

n_emb = 200
n_ex_in = 4000
n_rec = 300
n_attention_hidden = 50
corpus_name = "hlm.txt"
attention_reg = 0.1

settings = {stepsize = 0.01, range = 0.01, stepsize_reinforce = 0.04, momentum = 0.2}



local actor = "LSTM"

local TRAIN_READER = true
local TRAIN_ATTENTION_NETWORK = true
local USE_ATTENTION_NETWORK = true

------------------


-- Define our parameters

local paramsReaderRNN = {   
        initialHidden = tensorType(n_rec),
        Wrih = tensorType(n_rec,n_emb),
        Wrhh  = tensorType(n_rec,n_rec),
        r_W_emb = tensorType(n_emb, n_ex_in)
}

local paramsReader = {         l_r_W_xf = tensorType(n_rec, n_emb),
         l_r_W_xi = tensorType(n_rec, n_emb),
         l_r_W_xc = tensorType(n_rec, n_emb),
         l_r_W_xo = tensorType(n_rec, n_emb),
         

         l_r_W_hi = tensorType(n_rec, n_rec),
         l_r_W_ci = tensorType(n_rec, n_rec),



         l_r_W_hf = tensorType(n_rec, n_rec),
         l_r_W_cf = tensorType(n_rec, n_rec),



         l_r_W_hc = tensorType(n_rec, n_rec),

         l_r_W_ho = tensorType(n_rec, n_rec),

         l_r_W_co = tensorType(n_rec, n_rec),

         l_r_b_i = tensorType(n_rec),
         l_r_b_f = tensorType(n_rec),
 	 l_r_b_c = tensorType(n_rec),
 	 l_r_b_o = tensorType(n_rec),

         l_r_initialRecurrentState =  tensorType(n_rec),
         l_r_initialCell =  tensorType(n_rec),

         r_W_emb = tensorType(n_emb, n_ex_in)

}


local paramsActorRNN =    {startOfSentence = tensorType(n_ex_in),
        Waio  = tensorType(n_ex_in,n_ex_in),
        Waho  = tensorType(n_ex_in,n_rec),
        Waih  = tensorType(n_rec,n_ex_in),
        Wahh  = tensorType(n_rec,n_rec)}





local paramsActorLSTM = {
         startOfSentence = tensorType(n_ex_in),
         l_a_W_ce = tensorType(n_ex_in, n_rec),
         l_a_W_xf = tensorType(n_rec, n_ex_in),
         l_a_W_xi = tensorType(n_rec, n_ex_in),
         l_a_W_xc = tensorType(n_rec, n_ex_in),
         l_a_W_xo = tensorType(n_rec, n_ex_in),
         

         l_a_W_hi = tensorType(n_rec, n_rec),
         l_a_W_ci = tensorType(n_rec, n_rec),



         l_a_W_hf = tensorType(n_rec, n_rec),
         l_a_W_cf = tensorType(n_rec, n_rec),



         l_a_W_hc = tensorType(n_rec, n_rec),

         l_a_W_ho = tensorType(n_rec, n_rec),

         l_a_W_co = tensorType(n_rec, n_rec),

         l_a_b_i = tensorType(n_rec),
         l_a_b_f = tensorType(n_rec),
 	 l_a_b_c = tensorType(n_rec),
 	 l_a_b_o = tensorType(n_rec),

         l_a_initialCell =  tensorType(n_rec)
}

local paramsAttention = {
         W_att_rh = tensorType(n_attention_hidden, n_rec),
         W_att_ih = tensorType(n_attention_hidden, n_emb),
         W_att_ho = tensorType(1, n_attention_hidden)
}


if actor == "RNN" then
    paramsActor = paramsActorRNN
else
    paramsActor = paramsActorLSTM
end



local params = {}
for k,v in pairs(paramsReader) do params[k] = v end
for k,v in pairs(paramsActor) do params[k] = v end
for k,v in pairs(paramsAttention) do params[k] = v end


local paramsForAutoencoder = {}
for k,v in pairs(paramsReader) do paramsForAutoencoder[k] = v end
for k,v in pairs(paramsActor) do paramsForAutoencoder[k] = v end


for _, param in pairs(params) do
    param:uniform(-settings.range,settings.range)
end





updates = {}
for name, param in pairs(params) do
     updates[name] = 0 * params[name]
end



-------------


corpus = {}
-- NOTE THAT THE FILES COME FROM PYTHON AND WILL BE ZERO-INDEXED
function readCorpus() --as a double storage
   io.input(corpus_name..".num.b")
   t = io.read("*all")
   counter = 0
   for line in string.gmatch(t, "[^\n]+") do
     counter = counter+1
     lineList = {}
     table.insert(corpus, lineList)
     for token in string.gmatch(line, "[0-9]+") do
        table.insert(lineList, math.min(token+1.0, n_ex_in))
     end
     --if counter>100 then
     --   break
     --end
   end
   --corpus = torch.Tensor(corpusList)
end

readCorpus()

print("DONE READING CORPUS")






function one_hot(num)
   local vec = tensorType(n_ex_in):zero()
   vec[num] = 1
   return vec
end


defaultInput = n_ex_in



----------------------------------------
------- ATTENTION NETWORK --------------
----------------------------------------

local function computeAttention(params, inputEmbedding, currentHiddenVector)
     --print(params.W_att_rh)
     --print(currentHiddenVector)
     local attention
     if USE_ATTENTION_NETWORK then
          attention = util.sigmoid(params.W_att_ho * torch.tanh(params.W_att_rh * currentHiddenVector + params.W_att_ih * inputEmbedding))
     else
          attention = tensorType({0.98})
     end
     return attention
end


local function makeAttentionDecision(attentionScore, randomNumber)
    if torch.sum(attentionScore) > randomNumber then
        return 1
    else
        return 0
    end
end


----------------------------------------
------------ READER (LSTM) -------------
----------------------------------------

local function readLSTM(params, inputs, randomSequence, attentionScores)
    --print(randomSequence)
    local hiddenCurrent = params.l_r_initialRecurrentState
    local cellCurrent = params.l_r_initialCell
    local cellCurrentNew, hiddenCurrentNew
    local totalProb = tensorType(1)
    totalProb[1] = 1
    local attentions = {}
    local totalAttentionScore = 0
    for i,input in pairs(inputs) do
        local externalInput = params.r_W_emb * (one_hot(input))
-- TODO could make this more efficient by first checking decision ==? 0



        local decision = makeAttentionDecision(attentionScores[i], randomSequence[i])
        externalInput = externalInput * decision

        --print("+++++++++++++++")
        --print(params.l_r_W_xi)
        --print(externalInput)
        --print(params.l_r_W_hi)
        --print("***")
        --print(hiddenCurrent)
        --print(params.l_r_W_ci)
        --print(cellCurrent)
        --print(params.l_r_b_i)
        --print("=================")
        inputCurrent = util.sigmoid(params.l_r_W_xi * externalInput + params.l_r_W_hi * hiddenCurrent + params.l_r_W_ci * cellCurrent + params.l_r_b_i)
        forgetCurrent = util.sigmoid(params.l_r_W_xf * externalInput + params.l_r_W_hf * hiddenCurrent + params.l_r_W_cf * cellCurrent + params.l_r_b_f)
        cellCurrentNew = torch.cmul(forgetCurrent , cellCurrent) + torch.cmul(inputCurrent, torch.tanh(params.l_r_W_xc * externalInput + params.l_r_W_hc * hiddenCurrent + params.l_r_b_c))
        outputCurrent = util.sigmoid(params.l_r_W_xo * externalInput + params.l_r_W_ho * hiddenCurrent + params.l_r_W_co * cellCurrentNew + params.l_r_b_o)
        hiddenCurrentNew = torch.cmul(outputCurrent, torch.tanh(cellCurrentNew))







        -- we know attentionScores[i], but that doesn't help much...
        local attentionScore = computeAttention(params, externalInput, hiddenCurrent)
        local prob
        --print(attentionScore)
        --print(1-attentionScore)
        if decision == 1 then
            prob = attentionScore
        else
            prob = 1 - attentionScore
        end
        totalAttentionScore = totalAttentionScore + decision

        hiddenCurrent = hiddenCurrentNew
        cellCurrent = cellCurrentNew


        totalProb = torch.cmul(prob, totalProb)

        table.insert(attentions, {attentionScores[i], decision})

    end
    --print("TOTAL "..torch.sum(totalProb))
    return params.l_r_initialCell,totalProb, attentions, totalAttentionScore
end

--[[ NOTE THIS IS THIS ISSUE: https://github.com/twitter/torch-autograd/issues/73]]


local function readToComputeAttentionScores(params, inputs, randomSequence)
    local hiddenCurrent = params.l_r_initialRecurrentState
    local cellCurrent = params.l_r_initialCell
    local cellCurrentNew, hiddenCurrentNew
    local attentionScores = {}


    local totalAttentionScore = 0
    for i,input in pairs(inputs) do
        local externalInput = params.r_W_emb * (one_hot(input))

        local attention = computeAttention(params, externalInput, hiddenCurrent)
        local decision = makeAttentionDecision(attention, randomSequence[i])
        externalInput = externalInput * decision

        --print("+++++++++++++++")
        --print(params.l_r_W_xi)
        --print(externalInput)
        --print(params.l_r_W_hi)
        --print("***")
        --print(hiddenCurrent)
        --print(params.l_r_W_ci)
        --print(cellCurrent)
        --print(params.l_r_b_i)
        --print("=================")
        inputCurrent = util.sigmoid(params.l_r_W_xi * externalInput + params.l_r_W_hi * hiddenCurrent + params.l_r_W_ci * cellCurrent + params.l_r_b_i)
        forgetCurrent = util.sigmoid(params.l_r_W_xf * externalInput + params.l_r_W_hf * hiddenCurrent + params.l_r_W_cf * cellCurrent + params.l_r_b_f)
        cellCurrentNew = torch.cmul(forgetCurrent , cellCurrent) + torch.cmul(inputCurrent, torch.tanh(params.l_r_W_xc * externalInput + params.l_r_W_hc * hiddenCurrent + params.l_r_b_c))
        outputCurrent = util.sigmoid(params.l_r_W_xo * externalInput + params.l_r_W_ho * hiddenCurrent + params.l_r_W_co * cellCurrentNew + params.l_r_b_o)


        hiddenCurrentNew = torch.cmul(outputCurrent, torch.tanh(cellCurrentNew))



        hiddenCurrent = hiddenCurrentNew
        cellCurrent = cellCurrentNew

        --- the attention scores, so that they can later be recalled without trouble with differentiating comparisons
        table.insert(attentionScores, attention)

        totalAttentionScore = totalAttentionScore + decision


    end
    return attentionScores, totalAttentionScore
end


----------------------------------------
------------ READER (RNN) --------------
----------------------------------------

local function readRNN(params, inputs, randomSequence, attentionScores)
   local hiddenState = params.initialHidden
   local totalProb = 1
   local attentions = {}
   for _,input in pairs(inputs) do
      input = (one_hot(input))
      local prob, attention = makeAttentionDecision(attentionScores[i], randomSequence[i])
      input = input * attention
      --print(params.Wrih)
      --print(input)
      --print(params.Wrhh)
      --print(hiddenState)

      local part1 = params.Wrih * input
      local part2 = params.Wrhh * hiddenState
      --print(part1)
      --print(part2)
      --print(hiddenState)
      --print(input)
      hiddenState = util.sigmoid(part1 + part2)
        totalProb = prob * totalProb

        table.insert(attentions, {attentionScores[i], attention})
   end
   return hiddenState,totalProb, attentions
end
--where the second thing will be the probability of the glimpse sequence


----------------------------------------
--------------- READER -----------------
----------------------------------------


local function read(params, inputs, randomSequence, attentionScores)
    --print(randomSequence)
   return readLSTM(params, inputs, randomSequence, attentionScores)
end










----------------------------------------
----------- RECONSTRUCTOR --------------
----------------------------------------



local function reconstructLSTM(params, inputs, hiddenState)

    local prediction = {}
    local nll = 0
    local lastInput = params.startOfSentence


    local hiddenCurrent = hiddenState
    local cellCurrent = params.l_a_initialCell
    local cellCurrentNew, hiddenCurrentNew
    for _,input in pairs(inputs) do

       -- print("+++++++++++++++")
       -- print(params.l_a_W_xi)
       -- print(externalInput)
       -- print(params.l_a_W_hi)
       -- print("***")
       -- print(hiddenCurrent)
       -- print(params.l_a_W_ci)
       -- print(cellCurrent)
       -- print(params.l_a_b_i)
       -- print("=================")
       -- print(params.l_a_initialRecurrentState)

        inputCurrent = util.sigmoid(params.l_a_W_xi * lastInput + params.l_a_W_hi * hiddenCurrent + params.l_a_W_ci * cellCurrent + params.l_a_b_i)
        forgetCurrent = util.sigmoid(params.l_a_W_xf * lastInput + params.l_a_W_hf * hiddenCurrent + params.l_a_W_cf * cellCurrent + params.l_a_b_f)
        cellCurrentNew = torch.cmul(forgetCurrent , cellCurrent) + torch.cmul(inputCurrent, torch.tanh(params.l_a_W_xc * lastInput + params.l_a_W_hc * hiddenCurrent + params.l_a_b_c))
        outputCurrent = util.sigmoid(params.l_a_W_xo * lastInput + params.l_a_W_ho * hiddenCurrent + params.l_a_W_co * cellCurrentNew + params.l_a_b_o)
        hiddenCurrentNew = torch.cmul(outputCurrent, torch.tanh(cellCurrentNew))


        local externalInput = (one_hot(input))
        hiddenCurrent = hiddenCurrentNew
        cellCurrent = cellCurrentNew


      externalOutput = util.logSoftMax(params.l_a_W_ce * cellCurrent)



      lastInput = externalInput
      nll = nll - externalInput * externalOutput
      table.insert(prediction, externalOutput)
   end
   return nll, prediction
end





local function reconstructRNN(params, inputs, hiddenState)
   local prediction = {}
   local lastInput = params.startOfSentence
   nll = 0
   for _,input in pairs(inputs) do
      input = one_hot(input)
      --print(hiddenState)
      --print(params.Who)
      --print(lastInput)
      --print(params.Wio)
      --print(params.Waio * lastInput)
      --print(params.Waho * hiddenState)
      output = util.logSoftMax(params.Waio * lastInput + params.Waho * hiddenState)
      hiddenState = util.sigmoid(params.Waih * lastInput + params.Wahh * hiddenState)
      lastInput = input
      nll = nll - input * output
      table.insert(prediction, output)
   end
   return nll, prediction
end

local function reconstruct(params, inputs, hiddenState)
    if actor == "RNN" then
       return reconstructRNN(params, inputs, hiddenState)
    else
       return reconstructLSTM(params, inputs, hiddenState)
    end
end


----------------------------------------
--------------- APPLY ------------------
----------------------------------------

local function apply(params, inputs, randomSequence, attentionScores)
    local hiddenState, prob, attentions, totalAttentionScore = read(params, inputs, randomSequence, attentionScores)
    local nll, _ = reconstruct(params, inputs, hiddenState)
    return nll, prob, totalAttentionScore
end



local function applyToComputeAttentionScores(params,x, randomSequence)
    return readToComputeAttentionScores(params, x, randomSequence)
end

local function applyAndPrint(params, inputs, randomSequence, attentionScores)
    local hiddenState, prob, attentions, totalAttentionScore = read(params, inputs, randomSequence, attentionScores)
    local nll, prediction = reconstruct(params, inputs, hiddenState)
    print("+++++++")
    print(nll.."  "..torch.sum(prob).."  "..totalAttentionScore)
    for i=1, #inputs do
        local predictedScores, predictedTokens = torch.max(prediction[i],1)
        --print(inputs[i])
        --print(predictedTokens)
        --print(predictedScores)
        --print(prediction)
        --print(prediction[i])
        --print(inputs[i])
        --print(prediction[i][inputs[i]])
        print(inputs[i].."  "..predictedTokens[1].."  "..math.exp(predictedScores[1]).."  "..math.exp(prediction[i][inputs[i]]).."  "..attentions[i][2].."  "..torch.sum(attentions[i][1]))
    end
end

local function probOfGlimpses(params, inputs, randomSequence, attentionScores)
    local _, prob, _ = read(params, inputs, randomSequence, attentionScores)
    return (torch.sum(torch.log(prob)))
end



local function costNLL(params, inputs, randomSequence, attentionScores)
    local nll, _, _ = apply(params, inputs, randomSequence, attentionScores)
    return nll
end



----------------------------------------
-------------- TRAINING ----------------
----------------------------------------



local dcostNLL = grad(costNLL)
local dprobOfGlimpses = grad(probOfGlimpses)
 




local moving_average = 0


for epoch = 1,1000 do
   for i = 1,#corpus do
      --if #corpus[i] < 20 then
      --   break
      --end
      
      -- Get a data sample:
      -- TODO
      local x = corpus[i]
 
      ---
      local totalAttention
      local randomSequence = {}
      for i= 1,#x do
         table.insert(randomSequence,torch.uniform())
      end
      attentionScores, totalAttention, _ = applyToComputeAttentionScores(params,x, randomSequence)
      --print("...")
      --print(totalAttention)
      ---

      if i % 100 == 0 then
        print(i)
        applyAndPrint(params,x, randomSequence, attentionScores)
      end

      --probOfGlimpses(params,x, randomSequence, attentionScores)

      --exit()
      -- Calculate gradients (this is the function we defined above,
      -- and where all of autograd’s magic happens)

      local grads, loss 
      if TRAIN_READER then
           grads, loss = dcostNLL(params,x, randomSequence, attentionScores)
      end

      local gradsProb, probOfGlimpses
      if TRAIN_ATTENTION_NETWORK then
           gradsProb, probOfGlimpses = dprobOfGlimpses(params,x, randomSequence, attentionScores)
           --print("PROB "..probOfGlimpses)
      end


      local reward
      if TRAIN_ATTENTION_NETWORK then
          --print(loss)
          --print(totalAttention)
          reward = (loss + attention_reg * totalAttention)
          moving_average  = 0.9 * moving_average + 0.1 * reward

          --print(reward)
          --print(moving_average)
      end
--(reward-moving_average)*T.grad(theano.gradient.grad_clip(T.log(reader.probabilityOfChoicesSoFar), -1, 1), param))) 

      --print(grads)


      --print("NOW UPDATING")
 
      -- Update weights and biases
      if TRAIN_READER then
        for name, param in pairs(paramsForAutoencoder) do
            --print({name,param})
            --print(grads[name])
            --print(params[name])
            if grads[name] == nil then
               print("Warning: derivative for "..name.." is nil")
            else
               updates[name] = settings.momentum * updates[name] + (1-settings.momentum) * grads[name]
               params[name]:add(- settings.stepsize * updates[name])
            end
        end
      end

      if TRAIN_ATTENTION_NETWORK then
          for name, param in pairs(paramsAttention) do
            --print({name,param})
            --print(grads[name])
            --print(params[name])
            if gradsProb[name] == nil then
               print("Warning: prob derivative for "..name.." is nil")
            else
               --print(name)
               --print(gradsProb[name])
               --print(updates[name])
               updates[name] = settings.momentum * updates[name] + (1-settings.momentum) * gradsProb[name] * (reward-moving_average)
               --print(settings.stepsize_reinforce)
               --print(updates[name])
               --print(params[name])
               params[name]:add(- settings.stepsize_reinforce * updates[name])
            end
          end
      end


      --print("DONE UPDATING")

   end
end




