import math
import os
import sys

# TODO note that we check modulo case here.
# In the end it will have to be made sure that all tokenization and formatting things will have to be done consistently.


# TODO somehow the frequency list contains duplicates

# assumes that the token is in a column indexed "Token"

PART_NUM = sys.argv[2]

statFile = open("/disk/scratch2/s1582047/dundeetreebank/parts/PART"+PART_NUM+"Statistics/statistics/att-surp-"+sys.argv[1]+".csv", 'r')

statFileOut = open("/disk/scratch2/s1582047/dundeetreebank/parts/PART"+PART_NUM+"Statistics/statisticsFinal/att-surp-uni-"+sys.argv[1]+".csv", 'w')

lineCounter = 0

freqListFile = open("/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training3/freqList", 'r')

freqList = {}

for line in freqListFile:
     line = line.split("\t")
     if line[0] in freqList:
        #print("PROBLEM "+line[0]+" "+line[2].rstrip()+" "+str(freqList[line[0]]))
        freqList[line[0]] += float(line[2])
     else:
        freqList[line[0]] = float(line[2])


tokenIndex = 1

for line in statFile:
     line = line.rstrip("\n")
     lineList = line.split("\t")
     if lineCounter == 0:
         for i in range(0,len(lineList)):
             print(lineList[i])
             if lineList[i] == "\"Token\"":
                  tokenIndex =i
                  break
         statFileOut.write(line+"\t\"WordFreq\"\n")
     else:
         token = lineList[tokenIndex][1:-1].lower()
         if token in freqList:
               unigramSurp = -math.log( freqList[token] )
         else:
               print("ERROR token not in freqList  "+token)
               print(line)
               unigramSurp = -1
         statFileOut.write(line+"\t"+str(unigramSurp)+"\n")

     lineCounter += 1

statFile.close()
statFileOut.close()
