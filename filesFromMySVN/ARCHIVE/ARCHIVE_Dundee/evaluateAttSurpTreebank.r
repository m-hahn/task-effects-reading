DO_LOGISTIC = FALSE #TRUE
DO_LINEAR = TRUE

models = list("full"="Full", "s" = "No Surprisal", "rs" = "No Context", "ir" = "Only Surprisal")

centerColumn = function(data,columnName) {
  newName = (paste(columnName,"Centered",sep="."))
  data[,newName] <- data[,columnName] - mean(data[,columnName], na.rm = TRUE)
  data[,newName] <- data[,newName] / sd(data[,columnName], na.rm = TRUE)
  return(data)
}



replicate2017 = function() {
# data = read.csv("/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/unified_data_replication_1.csv", sep="\t")
# data = read.csv("/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/unified_data_50_s.csv", sep="\t")

# data = read.csv("/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/unified_data_50.csv", sep="\t")
 data = read.csv("/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/unified_data_50_PART2.csv", sep="\t")

 dataWithRandomSurprisal = read.csv("/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART2Statistics/statistics/att-surp-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-RANDOM_SURP_JAN_2018.csv", sep="\t")



library(dplyr)

 dataWithRandomSurprisalProc = dataWithRandomSurprisal %>% select(fileName, position, Surprisal, LuaID, C.FILE, C.WNUM, Participant) %>% rename(Random.Surprisal = Surprisal)

data = merge(data, dataWithRandomSurprisalProc, by=c("fileName", "position", "LuaID", "C.FILE", "C.WNUM", "Participant"))

# data = rbind(data, data2)
#/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1Statistics/statistics/att-surp-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50.csv

 data$Fixated = (data$C.FPASSD > 0)
 library(lme4)
  data$First.pass.dur = NULL # is just zeros
	data$C.ITEM <- 10000*data$C.FILE+data$C.WNUM

data = data[data$Anno.LogWordFreq < 12,]
data$position = as.numeric(as.character(data$position))

data$Anno.LogWordFreq = -data$Anno.LogWordFreq


# remove things at the beginning of sequences
data = data[data$position > 3,]


 dataN = data[data$Fixated,]

centerColumn = function(data,columnName) {
newName = (paste(columnName,"Centered",sep="."))
data[,newName] <- data[,columnName] - mean(data[,columnName], na.rm = TRUE)
data[,newName] <- data[,newName] / sd(data[,columnName], na.rm = TRUE)
return(data)
}


data = centerColumn(data, "Surprisal")
data = centerColumn(data, "Anno.WordLength")
data = centerColumn(data, "Anno.LogWordFreq")
data = centerColumn(data, "LuaID")

  data$Surprisal.Resid = resid(lm(Surprisal ~ Anno.LogWordFreq, data = data, na.action=na.exclude))
  data$Anno.WordLength.Resid = resid(lm(Anno.WordLength ~ Anno.LogWordFreq, data = data, na.action=na.exclude))
  data$Anno.Surprisal.Resid = resid(lm(Anno.Surprisal ~ Anno.LogWordFreq, data = data, na.action=na.exclude))
  data$Random.Surprisal.Resid = resid(lm(Random.Surprisal ~ Anno.LogWordFreq, data = data, na.action=na.exclude))



dataN = centerColumn(dataN, "Surprisal")
dataN = centerColumn(dataN, "Anno.WordLength")
dataN = centerColumn(dataN, "Anno.LogWordFreq")
dataN = centerColumn(dataN, "LuaID")

  dataN$Surprisal.Resid = resid(lm(Surprisal ~ Anno.LogWordFreq, data = dataN, na.action=na.exclude))
  dataN$Anno.WordLength.Resid = resid(lm(Anno.WordLength ~ Anno.LogWordFreq, data = dataN, na.action=na.exclude))
  dataN$Anno.Surprisal.Resid = resid(lm(Anno.Surprisal ~ Anno.LogWordFreq, data = dataN, na.action=na.exclude))
  dataN$Random.Surprisal.Resid = resid(lm(Random.Surprisal ~ Anno.LogWordFreq, data = dataN, na.action=na.exclude))



createUniqueItemIDs = function(data, columnName) {
   newName = (paste(columnName,"Renumbered",sep="."))
   data[,newName] = as.integer(factor(data[,columnName]))
   return (data)
}



data$Entered.Surprisal.Resid = data$Random.Surprisal.Resid
data.Complete = data[complete.cases(data[,c("Entered.Surprisal.Resid","Anno.WordLength.Resid","Anno.LogWordFreq.Centered","LuaID.Centered","Participant","C.ITEM","Fixated")]),]
data.Complete = createUniqueItemIDs(data.Complete, "Participant")
data.Complete = createUniqueItemIDs(data.Complete, "C.ITEM")
data.Complete = centerColumn(data.Complete,"Entered.Surprisal.Resid")
data.Complete = centerColumn(data.Complete,"Anno.WordLength.Resid")
data.Complete = centerColumn(data.Complete,"Anno.LogWordFreq")
data.Complete = centerColumn(data.Complete,"LuaID")
stanDat <- list(EnteredSurprisal = data.Complete$Entered.Surprisal.Resid , AnnoWordLength = data.Complete$Anno.WordLength.Resid , AnnoLogWordFreq = data.Complete$Anno.LogWordFreq.Centered , LuaID = data.Complete$LuaID.Centered , fix = data.Complete$Fixated,Participant = data.Complete$Participant.Renumbered , CITEM = data.Complete$C.ITEM.Renumbered , N = nrow(data.Complete) , M = max(data.Complete$Participant.Renumbered) , L = max(data.Complete$C.ITEM.Renumbered))
library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
fixMCMC <- stan(file = "model-fix-dundee.stan", data = stanDat, iter = 2000, warmup=1000,chains = 2, control=list(adapt_delta=0.7) )




data.Complete = data[complete.cases(data[,c("Anno.WordLength.Resid","Anno.LogWordFreq.Centered","LuaID.Centered","Participant","C.ITEM","Fixated")]),]
data.Complete = createUniqueItemIDs(data.Complete, "Participant")
data.Complete = createUniqueItemIDs(data.Complete, "C.ITEM")
data.Complete = centerColumn(data.Complete,"Anno.WordLength.Resid")
data.Complete = centerColumn(data.Complete,"Anno.LogWordFreq")
data.Complete = centerColumn(data.Complete,"LuaID")
stanDat <- list(AnnoWordLength = data.Complete$Anno.WordLength.Resid , AnnoLogWordFreq = data.Complete$Anno.LogWordFreq.Centered , LuaID = data.Complete$LuaID.Centered , fix = data.Complete$Fixated,Participant = data.Complete$Participant.Renumbered , CITEM = data.Complete$C.ITEM.Renumbered , N = nrow(data.Complete) , M = max(data.Complete$Participant.Renumbered) , L = max(data.Complete$C.ITEM.Renumbered))
library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
fixMCMC <- stan(file = "model-fix-dundee-nosurp.stan", data = stanDat, iter = 2000, warmup=1000,chains = 2, control=list(adapt_delta=0.7) )





predictors = 'Anno.WordLength.Resid + Anno.WordLength.Resid*Anno.LogWordFreq.Centered + LuaID.Centered*Anno.LogWordFreq.Centered + LuaID.Centered*Anno.WordLength.Resid + (1|Participant) + (1|C.ITEM)'
 
m_ff_0 = (lmer(paste('C.FFIXDUR ~ ',predictors,sep="") , data=dataN, REML=FALSE))
m_fp_0 = (lmer(paste('C.FPASSD ~ ',predictors,sep="") , data=dataN, REML=FALSE))
m_tt_0 = (lmer(paste('C.TOTDUR ~ ',predictors,sep="") , data=dataN, REML=FALSE))
#m_fix_0 = (glmer(paste('Fixated ~ ',predictors,sep="") , data=data, family="binomial"))

  library('texreg')
  texreg(c(m_ff_0, m_fp_0, m_tt_0, m_fix_0) ,single.row = TRUE)



predictors = 'Surprisal.Resid+Anno.WordLength.Resid + Anno.WordLength.Resid*Anno.LogWordFreq.Centered + LuaID.Centered*Anno.LogWordFreq.Centered + LuaID.Centered*Anno.WordLength.Resid + (1|Participant) + (1|C.ITEM)'
 
m_ff = (lmer(paste('C.FFIXDUR ~ ',predictors,sep="") , data=dataN, REML=FALSE))
m_fp = (lmer(paste('C.FPASSD ~ ',predictors,sep="") , data=dataN, REML=FALSE))
m_tt = (lmer(paste('C.TOTDUR ~ ',predictors,sep="") , data=dataN, REML=FALSE))
#m_fix = (glmer(paste('Fixated ~ ',predictors,sep="") , data=data, family="binomial"))

  library('texreg')
  texreg(c(m_ff, m_fp, m_tt, m_fix) ,single.row = TRUE)

anova(m_ff, m_ff_0)
anova(m_fp, m_fp_0)
anova(m_tt, m_tt_0)
anova(m_fix, m_fix_0)

AIC(m_ff)-AIC(m_ff_0)
AIC(m_fp)-AIC(m_fp_0)
AIC(m_tt)-AIC(m_tt_0)

BIC(m_ff)-BIC(m_ff_0)
BIC(m_fp)-BIC(m_fp_0)
BIC(m_tt)-BIC(m_tt_0)

deviance(m_ff)-deviance(m_ff_0)
deviance(m_fp)-deviance(m_fp_0)
deviance(m_tt)-deviance(m_tt_0)




predictors = 'Anno.Surprisal.Resid+Anno.WordLength.Resid + Anno.WordLength.Resid*Anno.LogWordFreq.Centered + LuaID.Centered*Anno.LogWordFreq.Centered + LuaID.Centered*Anno.WordLength.Resid + (1|Participant) + (1|C.ITEM)'
 
m_ff = (lmer(paste('C.FFIXDUR ~ ',predictors,sep="") , data=dataN, REML=FALSE))
m_fp = (lmer(paste('C.FPASSD ~ ',predictors,sep="") , data=dataN, REML=FALSE))
m_tt = (lmer(paste('C.TOTDUR ~ ',predictors,sep="") , data=dataN, REML=FALSE))
#m_fix = (glmer(paste('Fixated ~ ',predictors,sep="") , data=data, family="binomial"))

  library('texreg')
  texreg(c(m_ff, m_fp, m_tt, m_fix) ,single.row = TRUE)

anova(m_ff, m_ff_0)
anova(m_fp, m_fp_0)
anova(m_tt, m_tt_0)
anova(m_fix, m_fix_0)

AIC(m_ff)-AIC(m_ff_0)
AIC(m_fp)-AIC(m_fp_0)
AIC(m_tt)-AIC(m_tt_0)

BIC(m_ff)-BIC(m_ff_0)
BIC(m_fp)-BIC(m_fp_0)
BIC(m_tt)-BIC(m_tt_0)

deviance(m_ff)-deviance(m_ff_0)
deviance(m_fp)-deviance(m_fp_0)
deviance(m_tt)-deviance(m_tt_0)

predictors = 'Random.Surprisal.Resid+Anno.WordLength.Resid + Anno.WordLength.Resid*Anno.LogWordFreq.Centered + LuaID.Centered*Anno.LogWordFreq.Centered + LuaID.Centered*Anno.WordLength.Resid + (1|Participant) + (1|C.ITEM)'
 
m_ff = (lmer(paste('C.FFIXDUR ~ ',predictors,sep="") , data=dataN, REML=FALSE))
m_fp = (lmer(paste('C.FPASSD ~ ',predictors,sep="") , data=dataN, REML=FALSE))
m_tt = (lmer(paste('C.TOTDUR ~ ',predictors,sep="") , data=dataN, REML=FALSE))
#m_fix = (glmer(paste('Fixated ~ ',predictors,sep="") , data=data, family="binomial"))

  library('texreg')
  texreg(c(m_ff, m_fp, m_tt, m_fix) ,single.row = TRUE)

anova(m_ff, m_ff_0)
anova(m_fp, m_fp_0)
anova(m_tt, m_tt_0)
anova(m_fix, m_fix_0)

AIC(m_ff)-AIC(m_ff_0)
AIC(m_fp)-AIC(m_fp_0)
AIC(m_tt)-AIC(m_tt_0)

BIC(m_ff)-BIC(m_ff_0)
BIC(m_fp)-BIC(m_fp_0)
BIC(m_tt)-BIC(m_tt_0)

deviance(m_ff)-deviance(m_ff_0)
deviance(m_fp)-deviance(m_fp_0)
deviance(m_tt)-deviance(m_tt_0)





cor(dataN$Anno.LogWordFreq, dataN$Attention, use="complete")

 summary(lmer(C.FPASSD ~ Attention + Anno.Surprisal + Anno.WordLength + Anno.LogWordFreq + (1|Participant) + (1|C.ITEM), data=dataN))
 summary(lmer(C.TOTDUR ~ Attention + Anno.Surprisal + Anno.WordLength + Anno.LogWordFreq + (1|Participant) + (1|C.ITEM), data=dataN))
 summary(lmer(First.fixation.duration.on.every.word ~ Attention + Anno.Surprisal + Anno.WordLength + Anno.LogWordFreq + (1|Participant) + (1|C.ITEM), data=dataN))

 summary(glmer(Fixated ~ Attention + Anno.Surprisal + Anno.WordLength + Anno.LogWordFreq + (1|Participant) + (1|C.ITEM), data=data, family="binomial"))


library(ggplot2)
ggplot(data, aes(x=Anno.LogWordFreq,y=Attention)) +
  geom_smooth() 


library(ggplot2)
ggplot(data, aes(x=position,y=Attention)) +
  geom_smooth() 



threshold = (sort(data$Attention)[floor((1-mean(data$Fixated, na.rm=TRUE)) * length(data$Fixated))])
 data$Attended.Threshold = (data$Attention > 0.5653681)
 mean(data$Attended.Threshold == data$Fixated,na.rm=TRUE)

}


##########################
##########################


mse = function(a,b) {
   return(sum((a-b)^2) / length(a))
}

##########################
##########################



lastOnesVector = function(vec) {
	return(append(-1, vec[-c(length(vec))]))

}



##########################
##########################


performDiscretizedEvaluation =function(data) {
cat("\nA\n")
  predA = getThresholdPredictor(data, data$NotCentered.Attention)
  doDiscreteEvaluation(predA, data$C.FIXATED)
cat("\nS\n")
  predS = getThresholdPredictor(data, data$NotCentered.Surprisal)
  doDiscreteEvaluation(predS, data$C.FIXATED)


}



splitIntoReaders = function(data) {

	data$C.FIXATED = pmin(data$C.FIXNO, 1)

		da = data[data$Participant == "sa",]
		db = data[data$Participant == "sb",]
		dc = data[data$Participant == "sc",]
		dd = data[data$Participant == "sd",]
		de = data[data$Participant == "se",]
		df = data[data$Participant == "sf",]
		dg = data[data$Participant == "sg",]
		dh = data[data$Participant == "sh",]
		di = data[data$Participant == "si",]
		dj = data[data$Participant == "sj",]

		da = da[order(da$tokenID),]
		db = db[order(db$tokenID),]
		dc = dc[order(dc$tokenID),]
		dd = dd[order(dd$tokenID),]
		de = de[order(de$tokenID),]
		df = df[order(df$tokenID),]
		dg = dg[order(dg$tokenID),]
		dh = dh[order(dh$tokenID),]
		di = di[order(di$tokenID),]
		dj = dj[order(dj$tokenID),]

		datasets = list(da, db, dc, dd, de, df, dg, dh, di, dj)

		for(j in (1:length(datasets))) {
			dataset = datasets[[j]]
				for(i in 1:ncol(dataset)){
					dataset[is.na(dataset[,i]), i] <- mean(dataset[,i], na.rm = TRUE)
				}
			datasets[[j]] <- dataset
		}



	return(datasets)
}



##########################
##########################



# NOTE data will be altered
# Assumes -1s are already taken out
discreteEvaluationForLogisticModel = function(readers, trainer, tester) {

	da = readers[[trainer]]
		db = readers[[tester]]
		return(discreteEvaluationForLogisticModelOnOtherData(db,db))
}

discreteEvaluationForLogisticModelOnOtherData = function(da, db) {

	lm = glm(formula = C.FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST +   WordFreq + Attention, family = binomial("logit"), data=da)


		for(i in 1:ncol(db)){
			db[is.na(db[,i]), i] <- mean(db[,i], na.rm = TRUE)
		}


	resu = predict(lm, type="response", newdata =data.frame(Attention=db$Attention,  C.WLEN=db$C.WLEN, C.FREQBIN=db$C.FREQBIN , C.FRQLAST=db$C.FRQLAST ,    WordFreq=db$WordFreq), na.action=na.pass)


		doDiscreteEvaluation(db$C.FIXATED, getThresholdPredictor(db, resu))


		resu = (sign(resu - 0.5) + 1)/2
		doDiscreteEvaluation(db$C.FIXATED, resu)
		return(lm)
}



##########################
##########################




markLastFix = function(data) {
	attended = data$Attended
		data$PrevAttended <- append(-1, attended[-c(length(attended))])


		condProb = sum(data$Attended * data$PrevAttended) / sum(data$PrevAttended)
		marginalProb = mean(data$Attended)
		cat(condProb,"  ",marginalProb,"\n")

}



##########################
##########################



readStatisticalNumbersFromFiles = function(descriptor, partNum) {
	numberOfDataPoints = 6
		datapoints = list()

		files = system(paste(" ls /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "/annotation/perp-pg-test-",descriptor,sep=""), intern=TRUE)
		print(files)
		for(i in (1:length(files))) {
			data = read.table(files[[i]], header=FALSE, sep="\t")
				for(j in (1:numberOfDataPoints)) {
					if(i == 1) {
						datapoints = append(datapoints, data[j,][1])
					} else {
						datapoints[[j]] = append(datapoints[[j]], data[j,][1])
					}
				}
		}
	print(datapoints)
		for(j in (1:numberOfDataPoints)) {
			cat("--",j,"\n")
				evalList(datapoints[[j]], 0)
		}
}



##########################
##########################



getGaussianVector = function(length) {
	gauvector = c(rnorm(length-1, mean=0, sd = 0.2), length)
		gauvector = pmax(pmin(gauvector, 1), -1)
		return(gauvector)
}


##########################
##########################



evaluateLanguageModelRuns = function() {


}



##########################
##########################



discreteEvaluationForRandom = function(fixationrate) {


	truePositives = fixationrate * fixationrate
		trueNegatives = (1-fixationrate) * (1-fixationrate)


		correctPredictions = truePositives+trueNegatives


		totalRealPositives = fixationrate
		totalRealNegatives = 1-fixationrate
		totalItems = 1

		accuracy = correctPredictions / totalItems
		fixPrecision = truePositives / fixationrate
		fixRecall = truePositives / fixationrate
		skipPrecision = trueNegatives / (1-fixationrate)
		skipRecall = trueNegatives / (1-fixationrate)

		f1Fix = 2 * (fixPrecision * fixRecall) / (fixPrecision + fixRecall)
		f1Skip = 2 * (skipPrecision * skipRecall) / (skipPrecision + skipRecall)


		cat("Accuracy   ", accuracy,"\n")
		cat("F1 Fix     ", f1Fix, "\n")
		cat("F1 Skip    ", f1Skip, "\n")

}


##########################
##########################




findThreshold = function(data, predictor) {
#	return(sort(predictor)[floor((1-mean(data$C.FIXATED)) * length(data$C.FIXATED))])
# 2017
	return(sort(predictor)[floor((1-mean(data$Fixated, na.rm=TRUE)) * length(data$Fixated))])

}


##########################
##########################




predictWithThreshold = function(predictor, threshold) {
	return(sign(sign(predictor - threshold) +1))
}


##########################
##########################



# NOTE assumes C.FIXATED has not been centered
getThresholdPredictor = function(data, predictor) {
	threshold = findThreshold(data, predictor)
		binarizedPredictor = predictWithThreshold(predictor, threshold)
		return(binarizedPredictor)
}



##########################
##########################



compareHumans = function(data) {
	data$C.FIXATED = pmin(data$C.FIXNO, 1)

		da = data[data$Participant == "sa",]
		db = data[data$Participant == "sb",]
		dc = data[data$Participant == "sc",]
		dd = data[data$Participant == "sd",]
		de = data[data$Participant == "se",]
		df = data[data$Participant == "sf",]
		dg = data[data$Participant == "sg",]
		dh = data[data$Participant == "sh",]
		di = data[data$Participant == "si",]
		dj = data[data$Participant == "sj",]

		da = da[order(da$tokenID),]
		db = db[order(db$tokenID),]
		dc = dc[order(dc$tokenID),]
		dd = dd[order(dd$tokenID),]
		de = de[order(de$tokenID),]
		df = df[order(df$tokenID),]
		dg = dg[order(dg$tokenID),]
		dh = dh[order(dh$tokenID),]
		di = di[order(di$tokenID),]
		dj = dj[order(dj$tokenID),]

		ppa = getThresholdPredictor(da, (db$FIXATED + dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*da$FIXATED
		ppb = getThresholdPredictor(db, (da$FIXATED + dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*db$FIXATED
		ppc = getThresholdPredictor(dc, (da$FIXATED + db$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dc$FIXATED
		ppd = getThresholdPredictor(dd, (da$FIXATED + db$FIXATED+dc$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dd$FIXATED
		ppe = getThresholdPredictor(de, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*de$FIXATED
		ppf = getThresholdPredictor(df, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*df$FIXATED
		ppg = getThresholdPredictor(dg, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dg$FIXATED
		pph = getThresholdPredictor(dh, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dh$FIXATED
		ppi = getThresholdPredictor(di, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+dj$FIXATED)) + 0*di$FIXATED
		ppj = getThresholdPredictor(dj, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED)) + 0*dj$FIXATED


		ta = da$FIXATED
		tb = db$FIXATED
		tc = dc$FIXATED
		td = dd$FIXATED
		te = de$FIXATED
		tf = df$FIXATED
		tg = dg$FIXATED
		th = dh$FIXATED
		ti = di$FIXATED
		tj = dj$FIXATED

		ta = ta[!is.na(ppa)]
		tb = tb[!is.na(ppb)]
		tc = tc[!is.na(ppc)]
		td = td[!is.na(ppd)]
		te = te[!is.na(ppe)]
		tf = tf[!is.na(ppf)]
		tg = tg[!is.na(ppg)]
		th = th[!is.na(pph)]
		ti = ti[!is.na(ppi)]
		tj = tj[!is.na(ppj)]



		ppa = ppa[!is.na(ppa)]
		ppb = ppb[!is.na(ppb)]
		ppc = ppc[!is.na(ppc)]
		ppd = ppd[!is.na(ppd)]
		ppe = ppe[!is.na(ppe)]
		ppf = ppf[!is.na(ppf)]
		ppg = ppg[!is.na(ppg)]
		pph = pph[!is.na(pph)]
		ppi = ppi[!is.na(ppi)]
		ppj = ppj[!is.na(ppj)]

		doDiscreteEvaluation(c(ta,tb,tc,td,te,tf,tg,th,ti,tj), c(ppa, ppb, ppc, ppd, ppe, ppf, ppg, pph, ppi, ppj))


		cat(mean(da$FIXATED == db$FIXATED, na.rm=TRUE),"\n")
		cat(mean(db$FIXATED == dc$FIXATED, na.rm=TRUE),"\n")
		cat(mean(dc$FIXATED == dd$FIXATED, na.rm=TRUE),"\n")
		cat(mean(dd$FIXATED == de$FIXATED, na.rm=TRUE),"\n")
		cat(mean(de$FIXATED == df$FIXATED, na.rm=TRUE),"\n")
		cat(mean(df$FIXATED == dg$FIXATED, na.rm=TRUE),"\n")
		cat(mean(dg$FIXATED == dh$FIXATED, na.rm=TRUE),"\n")


		runEvaluationOfHumanOnOther(da$FIXATED, db$FIXATED)
		runEvaluationOfHumanOnOther(db$FIXATED, dc$FIXATED)
		runEvaluationOfHumanOnOther(dc$FIXATED, dd$FIXATED)
		runEvaluationOfHumanOnOther(dd$FIXATED, de$FIXATED)
		runEvaluationOfHumanOnOther(de$FIXATED, df$FIXATED)
		runEvaluationOfHumanOnOther(df$FIXATED, dg$FIXATED)
		runEvaluationOfHumanOnOther(dg$FIXATED, dh$FIXATED)
		runEvaluationOfHumanOnOther(dh$FIXATED, di$FIXATED)
		runEvaluationOfHumanOnOther(di$FIXATED, dj$FIXATED)
		runEvaluationOfHumanOnOther(dj$FIXATED, da$FIXATED)



}



##########################
##########################




runEvaluationOfHumanOnOther = function(target, predictor) {
	target2 = target * sign(predictor+1)
		predictor2 = predictor * sign(target+1)

		target2 = target2[!is.na(target2)]
		predictor2 = predictor2[!is.na(predictor2)]


		doDiscreteEvaluation(target2, predictor2)
}


##########################
##########################

getHumanSplit = function(data) {


	data$C.ITEM <- 10000*data$C.FILE+data$C.WNUM



		data$C.FIXATED = pmin(data$C.FIXNO, 1)


		da = data[data$Participant == "sa",]
		db = data[data$Participant == "sb",]
		dc = data[data$Participant == "sc",]
		dd = data[data$Participant == "sd",]
		de = data[data$Participant == "se",]
		df = data[data$Participant == "sf",]
		dg = data[data$Participant == "sg",]
		dh = data[data$Participant == "sh",]
		di = data[data$Participant == "si",]
		dj = data[data$Participant == "sj",]

                da = da[order(da$tokenID),]
                db = db[order(db$tokenID),]
                dc = dc[order(dc$tokenID),]
                dd = dd[order(dd$tokenID),]
                de = de[order(de$tokenID),]
                df = df[order(df$tokenID),]
                dg = dg[order(dg$tokenID),]
                dh = dh[order(dh$tokenID),]
                di = di[order(di$tokenID),]
                dj = dj[order(dj$tokenID),]


FIXATED_COLUMN = 92 #90

		da[is.na(da[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(da[,FIXATED_COLUMN], na.rm = TRUE)
		db[is.na(db[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(db[,FIXATED_COLUMN], na.rm = TRUE)
		dc[is.na(dc[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dc[,FIXATED_COLUMN], na.rm = TRUE)
		dd[is.na(dd[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dd[,FIXATED_COLUMN], na.rm = TRUE)
		de[is.na(de[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(de[,FIXATED_COLUMN], na.rm = TRUE)
		df[is.na(df[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(df[,FIXATED_COLUMN], na.rm = TRUE)
		dg[is.na(dg[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dg[,FIXATED_COLUMN], na.rm = TRUE)
		dh[is.na(dh[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dh[,FIXATED_COLUMN], na.rm = TRUE)
		di[is.na(di[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(di[,FIXATED_COLUMN], na.rm = TRUE)
		dj[is.na(dj[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dj[,FIXATED_COLUMN], na.rm = TRUE)


		da$Attention = (da$C.FIXATED + db$C.FIXATED + dc$C.FIXATED + dd$C.FIXATED + de$C.FIXATED + df$C.FIXATED + dg$C.FIXATED + dh$C.FIXATED + di$C.FIXATED + dj$C.FIXATED)/10

return(da)

}


getSkippingRatesHumanForOneTest = function(data) {


	data$C.ITEM <- 10000*data$C.FILE+data$C.WNUM



		data$C.FIXATED = pmin(data$C.FIXNO, 1)


		da = data[data$Participant == "sa" & data$Itemno==3,]
		db = data[data$Participant == "sb" & data$Itemno==3,]
		dc = data[data$Participant == "sc" & data$Itemno==3,]
		dd = data[data$Participant == "sd" & data$Itemno==3,]
		de = data[data$Participant == "se" & data$Itemno==3,]
		df = data[data$Participant == "sf" & data$Itemno==3,]
		dg = data[data$Participant == "sg" & data$Itemno==3,]
		dh = data[data$Participant == "sh" & data$Itemno==3,]
		di = data[data$Participant == "si" & data$Itemno==3,]
		dj = data[data$Participant == "sj" & data$Itemno==3,]


FIXATED_COLUMN = 90

		da[is.na(da[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(da[,FIXATED_COLUMN], na.rm = TRUE)
		db[is.na(db[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(db[,FIXATED_COLUMN], na.rm = TRUE)
		dc[is.na(dc[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dc[,FIXATED_COLUMN], na.rm = TRUE)
		dd[is.na(dd[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dd[,FIXATED_COLUMN], na.rm = TRUE)
		de[is.na(de[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(de[,FIXATED_COLUMN], na.rm = TRUE)
		df[is.na(df[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(df[,FIXATED_COLUMN], na.rm = TRUE)
		dg[is.na(dg[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dg[,FIXATED_COLUMN], na.rm = TRUE)
		dh[is.na(dh[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dh[,FIXATED_COLUMN], na.rm = TRUE)
		di[is.na(di[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(di[,FIXATED_COLUMN], na.rm = TRUE)
		dj[is.na(dj[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dj[,FIXATED_COLUMN], na.rm = TRUE)


		da$Attention = (da$C.FIXATED + db$C.FIXATED + dc$C.FIXATED + dd$C.FIXATED + de$C.FIXATED + df$C.FIXATED + dg$C.FIXATED + dh$C.FIXATED + di$C.FIXATED + dj$C.FIXATED)/10

return(da)







}



##########################
##########################



printHeatmap = function(data, low, high, exclude, end) {
	itemno = data$Itemno
		attention = data$Attention
		words = data$WORD
		wordfreq = data$WordFreq
		position = data$position 

		initializeColors()
		cat("\\definecolor{none}{rgb}{0.99,0.99,0.99}","\n",sep="")
		for(i in (1:end)) {
#cat(i,sep="\n")
			if(itemno[i] > 0 && itemno[i] != 3) {
				break
			}
			if(words[i] == -1) {
				next
			}

			if(exclude && (wordfreq[i] > 12 || position[i] > 47 || position[i] < 3)){
#    cat(" ",as.character(words[i]),sep="")
				cat(" \\colorbox{none}{",as.character(words[i]),"}",sep="")
			} else {
				colorID = min(18, max(3, floor((attention[i] - low) * 20 / (high - low))))
					cat(" \\colorbox{color", colorID,     "}{",as.character(words[i]),"}",sep="")
			}
			if(i %% 10 == 0) {
				cat("\n")
			}

		}

}



##########################
##########################



getRGB = function(i, lower, upper) {
	i = min(20, max(0, (i - lower) * 20 / (upper-lower)))

         if (FALSE) {
		blue = max(0,min(1, 1-i/20.0))
		green = max(0,min(1, 1- i / 20.0))
		red = 1
		return(c(red,green,blue))
} else if(TRUE) {
		blue = max(0,min(1, 2- i/10))
		red = max(0,min(1, 0+i/10))
		return(c(red,0.3,blue))
} else {

		blue = max(0,min(1,2 - i/5.0))
		if(i < 10) {
			green = max(0,min(1,0+i/5.0))
		} else {
			green = max(0,min(1, 4-i/5.0))
		}
	red = max(0,min(1,-2+i/5.0))
		return(c(red, green, blue))
}
}


##########################
##########################




initializeColors = function(data) {
	for(i in (0:20)) {
		color = getRGB(i, 0, 20)
			cat("\\definecolor{color",i,"}{rgb}{", color[1], ",", color[2], ",", color[3],  "}","\n",sep="")
	}

}


##########################
##########################



FULL_SURPRISAL_MODEL = "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full"


##########################
##########################




getFullSurprisalData = function(partNum) {
	name = FULL_SURPRISAL_MODEL
		CSV_FILE = paste("att-surp-uni-dundee-",name,"_",name,".csv",sep="")
		if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE,sep=""))) {
			return(processDataForEvaluation(getData(CSV_FILE2), "2"))
		}
}



##########################
##########################



pos = list("ADJ" = c("JJ", "JJR", "JJS"),
		"ADP" = c("IN"),
		"ADV" = c("RB", "RBR", "RBS", "WRB"),
		"CONJ" = c("CC"),
		"DET" = c("DT","EX","PDT","WDT"),
		"NOUN" = c("NN","NNP","NNPS","NNS"),
		"NUM" = c("CD"),
		"PRON" = c("PRP", "PRP$", "WP", "WP$"),
		"PRT" = c("POS", "RP","TO"),
		"VERB" = c("MD", "VB", "VBD", "VBG", "VBN", "VBP", "VBZ"),
		"X" = c("FW", "LS", "SYM", "UH")
	  )


##########################
##########################




retrieveValuesPerPOS = function() {
	findValuesPerPOS("full","5.0")
		findValuesPerPOS("s","5.0")
		findValuesPerPOS("rs","5.0")
		findValuesPerPOS("ir","5.0")

		findValuesPerPOS("full","4.5")
		findValuesPerPOS("s","4.5")
		findValuesPerPOS("rs","4.5")
		findValuesPerPOS("ir","4.5")

}


##########################
##########################



findValuesPerPOSInDundee = function(CSV_FILE, partNum) {
	scores = list("ADJ" = {}, "ADP" = {}, "ADV" = {}, "CONJ" = {}, "DET" = {}, "NOUN"= {}, "NUM" = {}, "PRON"={}, "PRT"={}, "VERB"={}, "X"={})
		cat(paste("X: /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE,sep=""),"\n")
		data = processDataForEvaluation(getData(CSV_FILE), "1")
		for(tag in names(pos)) {
			scores[[tag]] = append(scores[[tag]], mean((data[data$CPOS %in% pos[[tag]],]$FIXATED)))
		}
	for(tag in names(pos)) {
		cat("---\n",tag,"\n")
			cat(scores[[tag]],"\n")
			evalList(scores[[tag]], 0)
			cat("\n")
	}
}


##########################
##########################


collectValuesPerPOSForColumn = function(data, scores, column) {
	for(tag in names(pos)) {
		scores[[tag]] = append(scores[[tag]], mean((column[data$CPOS %in% pos[[tag]]])))
	}
	return(unlist(scores))
}

collectValuesPerPOS = function(data, scores) {
	for(tag in names(pos)) {
		scores[[tag]] = append(scores[[tag]], mean((data[data$CPOS %in% pos[[tag]],]$NotCentered.Attention)))
	}
	return(unlist(scores))
}



##########################
##########################




findValuesPerPOS = function(abl, weight, encoderName, partNum) {
	cat("@@@@@@@@@@@\n",abl,"   ",weight,"\n")
		scores = list("ADJ" = {}, "ADP" = {}, "ADV" = {}, "CONJ" = {}, "DET" = {}, "NOUN"= {}, "NUM" = {}, "PRON"={}, "PRT"={}, "VERB"={}, "X"={})
		for(num in (1:10)) {
			name = getNameFromAblAndWeight(abl, weight,num, encoderName)
				CSV_FILE = paste("att-surp-uni-dundee-",name,"_",name,".csv",sep="")
				cat(paste("X: /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE,sep=""),"\n")
				if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE,sep=""))) {
					data = processDataForEvaluation(getData(CSV_FILE), "1")
						if(is.null(data)) {
							cat("FAIL\n")
								next
						}
					scores = collectValuesPerPOS(data, scores)

				}
		}
	for(tag in names(pos)) {
		cat("---\n",tag,"\n")
			cat(scores[[tag]],"\n")
			evalList(scores[[tag]], 0)
			cat("\n")
	}
}



##########################
##########################



percentageOfSignificant = function(xlist, threshold)
{
	return(sum(xlist > threshold) / length(xlist))
}



##########################
##########################



evalList = function(xlist, threshold)
{
	cat(percentageOfSignificant(xlist, threshold), mean(xlist),sd(xlist),min(xlist), which.min(xlist), " -- ",max(xlist),which.max(xlist),"\n", sep=" ")
}



##########################
##########################



logisticFitWithBaselines = function(data)
{


	lm = glm(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST +  C.PREVFIX +  C.OBJLPOS + WordFreq +  NotCentered.Attention, family = binomial("logit"), data=data)
		resu = predict(lm, type="response", newdata =data.frame(NotCentered.Attention=data$NotCentered.Attention,  C.WLEN=data$C.WLEN, C.FREQBIN=data$C.FREQBIN , C.FRQLAST=data$C.FRQLAST ,  C.PREVFIX=data$C.PREVFIX ,  C.OBJLPOS=data$C.OBJLPOS , WordFreq=data$WordFreq))
		return(resu)

}



##########################
##########################


maxLikelihoodInterpolatorWithBaselines = function(data)
{
	resu = logisticFitWithBaselines(data)
		resu = (sign(resu - 0.5) + 1)/2
		return(resu)
}



##########################
##########################





logisticFit = function(data)
{
	lm = glm(formula = FIXATED ~  NotCentered.Attention, family = binomial("logit"), data=data)
		resu = predict(lm, type="response", newdata =data.frame(NotCentered.Attention=data$NotCentered.Attention))
		return(resu)

}


##########################
##########################



maxLikelihoodInterpolator = function(data)
{
	resu = logisticFit(data)
		resu = (sign(resu - 0.5) + 1)/2
		return(resu)
}



##########################
##########################




getNozeros = function(data)
{

# remove where FDUR is NAN
	data.Nozeros <- data[data$C.FPASSD > 0,]


		data.Nozeros <- data.Nozeros[complete.cases(data.Nozeros[,1]), ]
		data.Nozeros <- data.Nozeros[data.Nozeros$C.FPASSD < 1000,]
		data.Nozeros <- data.Nozeros[data.Nozeros$C.FPASSD > 80,]


		data.Nozeros$C.WLEN <- data.Nozeros$C.WLEN - mean(data.Nozeros$C.WLEN, na.rm = TRUE)
		data.Nozeros$PREVFIX <- data.Nozeros$PREVFIX - mean(data.Nozeros$PREVFIX, na.rm = TRUE)
		data.Nozeros$OBLP <- data.Nozeros$WDLP - mean(data.Nozeros$WDLP, na.rm = TRUE)
		data.Nozeros$ID <- data.Nozeros$ID - mean(data.Nozeros$ID, na.rm = TRUE)
		data.Nozeros$WordFreq <- data.Nozeros$WordFreq - mean(data.Nozeros$WordFreq, na.rm = TRUE)
		data.Nozeros$FRQLAST <- data.Nozeros$FRQLAST - mean(data.Nozeros$FRQLAST, na.rm = TRUE)
#data.Nozeros$NBIGR <- data.Nozeros$NBIGR - mean(data.Nozeros$NBIGR, na.rm = TRUE)
#data.Nozeros$BACKTR <- data.Nozeros$BACKTR - mean(data.Nozeros$BACKTR, na.rm = TRUE)
		data.Nozeros$LASTFIX <- data.Nozeros$LASTFIX - mean(data.Nozeros$LASTFIX, na.rm = TRUE)
		data.Nozeros$Surprisal <- data.Nozeros$Surprisal - mean(data.Nozeros$Surprisal, na.rm = TRUE)
		data.Nozeros$Attention <- data.Nozeros$Attention - mean(data.Nozeros$Attention, na.rm = TRUE)


		return(data.Nozeros)
}



##########################
##########################



doEvaluationForAllGroups = function()
{
	if(TRUE) {
		doEvaluationForGroup("ir","5.0", "-combined-real")
			doEvaluationForGroup("rs","5.0", "-combined-real")
			doEvaluationForGroup("s","5.0", "-combined-real")
			doEvaluationForGroup("full","5.0", "-combined-real")

			doEvaluationForGroup("ir","4.5", "-combined-real")
			doEvaluationForGroup("rs","4.5", "-combined-real")
			doEvaluationForGroup("full","4.5", "-combined-real")
			doEvaluationForGroup("s","4.5", "-combined-real")
	} else {

		doEvaluationForGroup("ir","5.0", "encoder-1")
			doEvaluationForGroup("rs","5.0", "encoder-1")
			doEvaluationForGroup("s","5.0", "encoder-1")
			doEvaluationForGroup("full","5.0", "encoder-1")

	}


	if(FALSE) {
		doEvalForPairs()
	}
}


doEvalForPairs = function() {
	compareTwoGroups("ir", "5.0", "rs", "5.0", "-combined-real")
		compareTwoGroups("ir", "5.0", "s", "5.0", "-combined-real")
		compareTwoGroups("ir", "5.0", "full", "5.0", "-combined-real")

#compareTwoGroups("rs", "5.0", "ir", "5.0", "-combined-real")
		compareTwoGroups("rs", "5.0", "s", "5.0", "-combined-real")
		compareTwoGroups("rs", "5.0", "full", "5.0", "-combined-real")

#compareTwoGroups("s", "5.0", "ir", "5.0", "-combined-real")
#compareTwoGroups("s", "5.0", "rs", "5.0", "-combined-real")
		compareTwoGroups("s", "5.0", "full", "5.0", "-combined-real")

#compareTwoGroups("full", "5.0", "ir", "5.0", "-combined-real")
#compareTwoGroups("full", "5.0", "rs", "5.0", "-combined-real")
#compareTwoGroups("full", "5.0", "s", "5.0", "-combined-real")



}



##########################
##########################



getProcessedData = function(CSV_FILE) {
  return(processDataForEvaluation(getData(CSV_FILE), 1))
}


#assumes that processDataForEvaluation has been run on them
compareTwoModelsLogistic = function(data1, data2)
{


	data = merge(data1, data2, by=c("C.ITEM", "Participant"))

		data$resid.Attention.NoSurp.x.Against = residuals(lm(Attention.x ~ C.WLEN.x + C.FREQBIN.x +   WordFreq.x + Attention.y, data=data, na.action=na.exclude))
		data$resid.Attention.NoSurp.y.Against = residuals(lm(Attention.y ~ C.WLEN.y + C.FREQBIN.y +   WordFreq.y + Attention.x, data=data, na.action=na.exclude))

		data$resid.Attention.x.Against = residuals(lm(Attention.x ~ C.WLEN.x + C.FREQBIN.x +   WordFreq.x + Surprisal.x + Attention.y, data=data, na.action=na.exclude))
		data$resid.Attention.y.Against = residuals(lm(Attention.y ~ C.WLEN.y + C.FREQBIN.y +   WordFreq.y + Surprisal.y + Attention.x, data=data, na.action=na.exclude))


#log.Full.x = try(glmer(formula = FIXATED.x ~ C.WLEN.x+ C.FREQBIN.x + C.FRQLAST.x + C.PREVFIX.x +  C.OBJLPOS.x + WordFreq.x  + Surprisal.x + Attention.y + resid.Attention.x.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))
		log.Att.x  = try(glmer(formula = FIXATED.x ~ C.WLEN.x+ C.FREQBIN.x + C.FRQLAST.x + C.PREVFIX.x +  C.OBJLPOS.x + WordFreq.x  +               Attention.y + resid.Attention.NoSurp.x.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))

#log.Full.y = try(glmer(formula = FIXATED.y ~ C.WLEN.y+ C.FREQBIN.y + C.FRQLAST.y + C.PREVFIX.y +  C.OBJLPOS.y + WordFreq.y  + Surprisal.y + Attention.x + resid.Attention.y.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))
		log.Att.y  = try(glmer(formula = FIXATED.y ~ C.WLEN.y+ C.FREQBIN.y + C.FRQLAST.y + C.PREVFIX.y +  C.OBJLPOS.y + WordFreq.y  +               Attention.x + resid.Attention.NoSurp.y.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))



		return(list("att-x" = log.Att.x, "att-y" = log.Att.y))


}
##########################
##########################


compareTwoModelsLinear = function(data1, data2)
{
	data.Nozeros = merge(getNozeros(data1), getNozeros(data2), by=c("C.ITEM", "Participant"))

		data.Nozeros$residSurprisal.x.Against = residuals(lm(Surprisal.x ~ C.WLEN.x + WordFreq.x +  FRQLAST.x +    WordFreq.x + Surprisal.y, data=data.Nozeros, na.action=na.exclude))
		data.Nozeros$residSurprisal.y.Against = residuals(lm(Surprisal.y ~ C.WLEN.y + WordFreq.y +  FRQLAST.y +    WordFreq.y + Surprisal.x, data=data.Nozeros, na.action=na.exclude))

		linear.Surp.x = lmer(C.FPASSD.x ~ C.WLEN.x+ C.FREQBIN.x + C.FRQLAST.x + C.TRACEIC.x + C.BACKTR.x + C.NBIGR.x + C.PREVFIX.x + C.LDIST.x + C.OBJLPOS.x + WordFreq.x + Surprisal.y + residSurprisal.x.Against + (residSurprisal.x.Against|C.ITEM) + (1|Participant) + (1|C.ITEM), data=data.Nozeros)
		linear.Surp.y = lmer(C.FPASSD.y ~ C.WLEN.y+ C.FREQBIN.y + C.FRQLAST.y + C.TRACEIC.y + C.BACKTR.y + C.NBIGR.y + C.PREVFIX.y + C.LDIST.y + C.OBJLPOS.y + WordFreq.y + Surprisal.x + residSurprisal.y.Against + (residSurprisal.y.Against|C.ITEM) + (1|Participant) + (1|C.ITEM), data=data.Nozeros)

		return(list("x" = linear.Surp.x, "y" = linear.Surp.y))
}

##########################
##########################



compareTwoGroups = function(abl1, weight1, abl2, weight2, encoderName, partNum)
{
	x_over_y = {}
	y_over_x = {}

	library(lme4)
		for(num1 in (1:10)) {
			name1 = getNameFromAblAndWeight(abl1, weight1,num1, encoderName)
				CSV_FILE1 = paste("att-surp-uni-dundee-",name1,"_",name1,".csv",sep="")
				cat(paste("X: /disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE1,sep=""),"\n")
				if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE1,sep=""))) {
					data1 = processDataForEvaluation(getData(CSV_FILE1), "1")
						if(is.null(data1)) {
							next
						}

					if(runif(1,0,1) > 0.5){
						next
					}

					for(num2 in (1:10)) {
						name2 = getNameFromAblAndWeight(abl2, weight2,num2, encoderName)
							CSV_FILE2 = paste("att-surp-uni-dundee-",name2,"_",name2,".csv",sep="")
							cat("\n","----",paste("X: /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE1,sep=""),"\n")
							cat(paste("Y: /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE2,sep=""),"\n")
							if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE2,sep=""))) {
								data2 = processDataForEvaluation(getData(CSV_FILE2), "2")
									if(is.null(data2)) {
										next
									}
								if(runif(1,0,1) > 0.5) {
									next
								}

								compareLog = compareTwoModelsLogistic(data1, data2)
									compareLin = compareTwoModelsLinear(data1, data2)


# cat(capture.output(summary(compareLog$"full-x")), sep="\n")
									cat(capture.output(summary(compareLog$"att-x")), sep="\n")
# cat(capture.output(summary(compareLog$"full-y")), sep="\n")
									cat(capture.output(summary(compareLog$"att-y")), sep="\n")

									cat(capture.output(summary(compareLin$"x")), sep="\n")
									cat(capture.output(summary(compareLin$"y")), sep="\n")

									x_over_y = append(x_over_y, coef(summary(compareLog$"att-x"))["resid.Attention.NoSurp.x.Against","z value"])
									y_over_x = append(y_over_x, coef(summary(compareLog$"att-y"))["resid.Attention.NoSurp.y.Against","z value"])



							}
					}
				}
		}
	cat("X OVER Y for ","X",abl1,weight1,"Y",abl2,weight2,"\n",sep=" ")
		cat(x_over_y,"\n")
		cat(evalList(x_over_y, 2),"\n")

		cat("Y OVER X for ","Y",abl2,weight2,"X",abl1,weight1,"\n",sep=" ")
		cat(y_over_x,"\n")
		cat(evalList(y_over_x, 2),"\n")


}

##########################
##########################

getNameFromAblAndWeight = function(abl, weight, num, encoderName, soft)
{
	if(as.numeric(weight) == floor(as.numeric(weight))) {
		weightR = floor(as.numeric(weight))
	} else {
		weightR = weight
	}
	if (! soft){
		return(paste("pg-test-combined-50-1000-0.7-100-R-",weightR,"a0-", weight, "-entities-entropy5.0-1.0",encoderName,"-",abl,"-re", num, sep=""))
	} else {
		crash()
	}
}

##########################
##########################



doEvaluationForGroup = function(abl, weight, encoderName, partNum)
{
	library(lme4)
		cat("\n\n",abl,weight,"---------","\n",sep="\n")
		l.tval.fix.full =      {}
	l.tval.fix.surp =      {}
	l.tval.fix.att =       {}

	l.cor.att.surp =      {}
	l.cor.att.wordfreq =  {}
	l.cor.att.wordlength = {}

	l.tval.fp.full =      {}
	l.tval.fp.surp =     {}
	l.tval.fp.att =        {}

	l.meansurp =          {}

	l.loglikelihood =    {}

	l.ratio =           {}

	disc.att.acc = {}
	disc.att.f1f = {}
	disc.att.f1s = {}

	disc.surp.acc = {} 
	disc.surp.f1f = {} 
	disc.surp.f1s = {} 


	for(num in (1:10)) {
		name = getNameFromAblAndWeight(abl, weight, num, encoderName)
			CSV_FILE = paste("att-surp-uni-dundee-",name,"_",name,".csv",sep="")
			cat(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE,sep=""),"\n")
			if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE,sep=""))) {
				results = evaluate.Treebank(CSV_FILE)
					if(is.null(results)) {
						next
					}
				if(DO_LOGISTIC) {
					tval.fix.full =      coef(summary(results$"log.Full"))["C.residAttention","z value"]
						tval.fix.surp =      coef(summary(results$"log.Surp"))["C.residSurprisal","z value"]
						tval.fix.att =       coef(summary(results$"log.Att"))["C.residAttentionNoSurp","z value"]

						l.tval.fix.full  =   append(l.tval.fix.full    , tval.fix.full   )
						l.tval.fix.surp  =   append(l.tval.fix.surp    ,  tval.fix.surp  )
						l.tval.fix.att  =   append(l.tval.fix.att    , tval.fix.att   )


				}

				cor.att.surp =       results$"Att-Surp"
					cor.att.wordfreq =   results$"Att-log(WordFreq)"
					cor.att.wordlength = results$"Att-WordLength"

					if(DO_LINEAR) {
						tval.fp.full =       coef(summary(results$"FP.Full"))["residAttentionWithSurp","t value"]
							tval.fp.surp =       coef(summary(results$"FP.Surprisal"))["residSurprisal","t value"]
							tval.fp.att =        coef(summary(results$"FP.Att"))["residAttentionNoSurp","t value"]


							l.tval.fp.full  =   append(l.tval.fp.full    ,  tval.fp.full  )
							l.tval.fp.surp  =   append(l.tval.fp.surp    ,  tval.fp.surp  )
							l.tval.fp.att  =   append(l.tval.fp.att    ,  tval.fp.att  )


					}

				meansurp =           results$"meanSurp"

					loglikelihood =      results$"approxPerp"$"model"

					ratio =              results$"approxPerp"$"ratio"


					l.cor.att.surp  =   append(l.cor.att.surp    ,  cor.att.surp  )
					l.cor.att.wordfreq  =   append(l.cor.att.wordfreq    ,  cor.att.wordfreq  )
					l.cor.att.wordlength  =   append(l.cor.att.wordlength    ,  cor.att.wordlength  )
					l.meansurp  =   append(l.meansurp    ,  meansurp  )
					l.loglikelihood  =   append(l.loglikelihood    ,  loglikelihood  )
					l.ratio  =   append(l.ratio    ,  ratio  )


					disc.att = results$"Disc.Att"
					disc.surp = results$"Disc.Surp"
					disc.att.acc = append(disc.att.acc, disc.att$"Acc")
					disc.att.f1f = append(disc.att.f1f, disc.att$"F1 Fix")
					disc.att.f1s = append(disc.att.f1s, disc.att$"F1 Skip")

					disc.surp.acc = append(disc.surp.acc, disc.surp$"Acc")
					disc.surp.f1f = append(disc.surp.f1f, disc.surp$"F1 Fix")
					disc.surp.f1s = append(disc.surp.f1s, disc.surp$"F1 Skip")




#   return(list("Acc" = accuracy, "F1 Fix" = f1Fix, "F1 Skip" = f1Skip))



			}
	}
	cat("EVAL FOR ",abl,"  ",weight," ",encoderName,"\n")

		ModelName = getModelName(abl)

		buildRow(paste(ModelName," (Prob)"), list(100*disc.att.acc, 100*disc.att.f1f, 100*disc.att.f1s))


		buildRow(paste(ModelName," (Surp)"), list(100*disc.surp.acc, 100*disc.surp.f1f, 100*disc.surp.f1s))



		printEvalFeature(disc.att.acc, "disc.att.acc", 0.52)
		printEvalFeature(disc.att.f1f, "disc.att.f1f", 0.6)
		printEvalFeature(disc.att.f1s, "disc.att.f1s", 0.4)

		printEvalFeature(disc.surp.acc, "disc.surp.acc", 0.52)
		printEvalFeature(disc.surp.f1f, "disc.surp.f1f", 0.6)
		printEvalFeature(disc.surp.f1s, "disc.surp.f1s", 0.4)


		cat("         l.tval.fix.full ","\n")
		cat(         l.tval.fix.full ,"\n")
		cat(   evalList(l.tval.fix.full, 1.96 ))
		cat("\n")
		cat( "    l.tval.fix.surp ","\n")
		cat(     l.tval.fix.surp ,"\n")
		cat(  evalList(l.tval.fix.surp , 1.96))
		cat("\n")
		cat(  "       l.tval.fix.att ","\n")
		cat(         l.tval.fix.att ,"\n")
		cat(evalList(l.tval.fix.att ,1.96))
		cat("\n")
		cat(   "    l.cor.att.surp","\n")
		cat(       l.cor.att.surp,"\n")
		cat(  evalList(       l.cor.att.surp,0))
		cat("\n")
		cat(    " l.cor.att.wordfreq ","\n")
		cat(     l.cor.att.wordfreq ,"\n")
		cat(  evalList(   l.cor.att.wordfreq ,0))
		cat("\n")
		cat(     "    l.cor.att.wordlength ","\n")
		cat(         l.cor.att.wordlength ,"\n")
		cat(   evalList(      l.cor.att.wordlength ,0))
		cat("\n")
		cat(      "   l.tval.fp.full" ,"\n")
		cat(         l.tval.fp.full ,"\n")      
		cat(evalList(         l.tval.fp.full,2.0 ))
		cat("\n")
		cat("     l.tval.fp.surp  ","\n")
		cat(     l.tval.fp.surp  ,"\n")    
		cat(evalList(     l.tval.fp.surp ,0 ))
		cat("\n")
		cat( "        l.tval.fp.att    ","\n")
		cat(         l.tval.fp.att   ,"\n" )   
		cat(evalList(         l.tval.fp.att   ,0 ))
		cat("\n")
		cat(  "       l.meansurp      "  ,"\n")
		cat(         l.meansurp       ,"\n" )  
		cat(evalList(         l.meansurp      ,0  ))
		cat("\n")
		cat(   "      l.loglikelihood" ,"\n")
		cat(         l.loglikelihood ,"\n")
		cat( evalList(        l.loglikelihood,0 ))
		cat("\n")
		cat(    "     l.ratio       "  ,"\n" )
		cat(         l.ratio         ,"\n" )  
		cat(  evalList(       l.ratio          ,3))
		cat("\n")
}


##########################
##########################



printEvalFeature = function(feature, name, threshold) {


	cat("\t\t",name,"\t\t\n")
		cat(       feature ,"\n")
		cat(   evalList(feature, threshold ))
		cat("\n")
}


##########################
##########################



getModelName = function(ABL) {
	return(models[[ABL]])
}


##########################
##########################




buildRow = function(RowName, data) {
#buildRow(paste(ModelName," (Prob)"), c(disc.att.acc, disc.att.f1f, disc.att.f1s))
	cat(RowName,"  ")
		for(i in (1:length(data))) {
			datapoint = data[[i]]
				cat("&   ",round(mean(datapoint),2)," (",round(sd(datapoint),2),")  ",sep="")
		}
	cat("\\\\\n")
}



##########################
##########################



bootstrapping = function(CSV_FILE) {
	library(boot)
		data = processDataForEvaluation(getData(CSV_FILE), CSV_FILE)
		if(! is.data.frame(data)) {
			return(NULL)
		}
	results <- boot(data = data, statistic = getStatisticsForData, R = 10)
		return(results)

}



##########################
##########################



getStatisticsForData = function(data, indices){

	d <- data[indices,]
		return(computeStatisticsOnData(d,TRUE))
}


#models <- list("data" = data,               "log.Full" = log.Full,               "log.Surp" = log.Surp,               "log.Att" = log.Att,"Att-Surp" =cor(data$Surprisal, data$Attention), "Att-log(WordFreq)" = cor(data$Attention, data$WordFreq),"Att-WordLength" = cor(data$Attention,data$C.WLEN), "FP.Surprisal" = linear.Surp, "FP.Att" = linear.Att= "FP.Full" = linear.Full, "meanSurp" = mean(data.Nozeros$NotCentered.Surprisal), "approxPerp" = approxPerp)
#return list("model" = perpModel, "random" = perpRandom, "ratio" = (perpRandom - perpModel))


##########################
##########################



evaluatePerplexity = function(CSV_FILE) 
{
	data = evaluate.Treebank(CSV_FILE)
		computeApproxPerplexity(data)
		return(data)
}



##########################
##########################



computeApproxPerplexity = function(data)
{

perpModel = -(sum(data$C.FIXATED * log(data$NotCentered.Attention)) + sum((1-data$C.FIXATED) * log(1-data$NotCentered.Attention)))
# todo remove unknown words from this formula

	lexicalAttentions = aggregate(data["NotCentered.Attention"],by=data["Token"], mean, na.rm=TRUE)
		perpModel = 0
		perpRandom = 0
		perpLexical = 0
		attention = data$NotCentered.Attention
		wordfreq = data$WordFreq
		fixated = data$C.FIXNO
		tokens = data$Token
		randomProb = mean(data$NotCentered.Attention)
		numberOfItems = 0




		for(i in (1:nrow(data))) {
#cat(i,sep="\n")
			if(!is.na(fixated[i]) & fixated[i] > -1){
				if(wordfreq[i] > 0 && wordfreq[i] < 12) { # deal with unknown words (NOTE hard-coded that ~12 is the threshold)
					numberOfItems = numberOfItems + 1
#lexicalAttention = wordatts[wordatts[,1]==tokens[i],]$NotCentered.Attention
						if(fixated[i] > 0){
							perpModel = perpModel - log(attention[i])
								perpRandom = perpRandom - log(randomProb)
#perpLexical = perpLexical - log(lexicalAttention)
#cat(attention[i], wordfreq[i], sep = "\n")
						}else{
#cat("\n")
							perpModel = perpModel - log(1-attention[i])
								perpRandom = perpRandom - log(1-randomProb)
#perpLexical = perpLexical - log(1-lexicalAttention)
						}
				}
			}
		}



	cat("LogLikelihoods:","\n","MODEL      ",perpModel,"\nLEXICAL   ",perpLexical,"\nRANDOM    ",perpRandom,"\nRATIO      ",(perpRandom - perpModel),"\nAVG MODEL  ",(perpModel/numberOfItems),"\nAVG RAND   ",(perpRandom/numberOfItems),sep="")
		return(list("model" = perpModel, "random" = perpRandom, "ratio" = (perpRandom - perpModel)))
}




##########################
##########################



getData = function(CSV_FILE, partNum) {
	data = read.table(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE, sep=""), header = TRUE, sep = "\t")
		data$tokenID = 10000 * data$Itemno + data$WNUM
		return(data)
}





##########################
##########################




evaluateAll.Treebank = function(partNum)
{
	library(lme4)
#library(ggplot2)
		files = list.files(path="/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/")
		for(q in (1:length(files))) {
#sink()
#         cat(files[q], sep="\n")
			try(evaluate.Treebank(files[q]))
		}

}



##########################
##########################



#result = merge(data.Treebank, data.Corpus, by=0, all=TRUE)

buildRowNames.T = function(dataT)
{
	rowNames = row.names(dataT)
		for(q in (1:length(dataT$Surprisal))) {
			print(q)
				rowNames[q] <- paste(dataT$C.SUBJ[q], dataT$C.ITEM[q])
		}
	row.names(dataT) <- rowNames
		return(dataT)
}


##########################
##########################



buildRowNames.C = function(dataC)
{
	rowNames = row.names(dataC)
		for(q in (1:length(dataC$Surprisal))) {
			print(q)
				rowNames[q] <- paste(dataC$SUBJ[q], dataC$ITEM[q])
		}
	row.names(dataC) <- rowNames
		return(dataC)
}



##########################
##########################



randomlySampleData.Treebank = function(data, prob) {
	column = data$Surprisal
		for(q in (1:length(column))) {
			dice = runif(1,0.0,1.0)
				column[q] = dice - prob
		}

	data2 = data[column < 0,]


		m1 = try(glm(formula = FIXATED ~ C.WLEN+ C.PREVFIX + C.OBJLPOS + WordFreq+ C.FRQLAST +  Surprisal + Attention, family = binomial("logit"), data=data2))

		m2 = try(glm(formula = FIXATED ~  C.WLEN+ C.PREVFIX + C.OBJLPOS  + WordFreq + C.FRQLAST +   Surprisal, family = binomial("logit"), data=data2))

		m3 = try(glm(formula = FIXATED ~  C.WLEN+ C.PREVFIX + C.OBJLPOS +  WordFreq + C.FRQLAST,  family = binomial("logit"), data=data2))

		m4 = try(glm(formula = FIXATED ~  C.WLEN+ C.PREVFIX + C.OBJLPOS +  WordFreq + C.FRQLAST + Attention,  family = binomial("logit"), data=data2))



		cat("--- nFixations, RESID, LOGISTIC ---",sep="\n")
		try(cat(capture.output(m1), "\n", sep="\n"))

		cat("--- nFixations, RESID, LOGISTIC BASELINE ---",sep="\n")
		try(cat(capture.output(m2), "\n", sep="\n"))

		cat("--- nFixations, RESID, LOGISTIC BASELINE NO SURP ---",sep="\n")
		try(cat(capture.output(m3), "\n", sep="\n"))

		cat("--- nFixations, RESID, NO SURP ---",sep="\n")
		try(cat(capture.output(m4), "\n", sep="\n"))


		cat("--- LIKELIHOOD RATIO ---",sep="\n")
		try(cat(capture.output(lrtest(m1,m2)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m1,m3)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m2,m3)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m1,m4)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m2,m4)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m3,m4)), "\n", sep="\n"))

}



##########################
##########################




test.Treebank = function()
{
	library(lme4)
#library(ggplot2)
		evaluate.Treebank('att-surp-uni-dundee-pg-test-autoencoding-20-700-0.7-100-R-4.6a0-emb_pg-test-langmod-20-200-0.7-100-R-4.6e1.csv')
}

#data <- read.table("/disk/scratch2/s1582047/dundee/PART1Statistics/statisticsFinal//att-surp-uni-dundee-pg-test-autoencoding-20-700-0.7-100-R-4.6a0-emb_pg-test-langmod-20-200-0.7-100-R-4.6e1.csv", header = TRUE, sep = "\t")


##########################
##########################




evaluate.Treebank = function(CSV_FILE)
{
	data = processDataForEvaluation(getData(CSV_FILE), CSV_FILE)
		return(computeStatisticsOnData(data, FALSE))
}



##########################
##########################



doBasicPreprocessing = function(data) {
# ADDING SOME COLUMNS
	data$NotCentered.Attention = data$Attention 
		data$NotCentered.Surprisal = data$Surprisal
		data$FIXATED = pmin(data$C.FIXNO, 1)
		data$C.FIXATED = pmin(data$C.FIXNO, 1)


##########################################
# Merging together the -1 rows to undo the tokenization

		auxColumn.Surp = data$Surprisal
		auxColumn.Att = data$Surprisal

		accumulator.Surp = 0
		accumulator.Att = 0
		lastRealOne = -1
		numberOfTokensInCurrentToken = 0
		for(i in (1:length(data$Surprisal))) {
#cat(i, sep="\n")
			if(data$CorpusID[i] == -1){
#cat(i, sep="\n")
				accumulator.Surp = data$Surprisal[i] + accumulator.Surp
					accumulator.Att = data$Attention[i] + accumulator.Att
					numberOfTokensInCurrentToken = numberOfTokensInCurrentToken + 1
#data$Surprisal[lastRealOne] = data$Surprisal[i] + data$Surprisal[lastRealOne]
#data$Attention[lastRealOne] = data$Attention[i] + data$Attention[lastRealOne]
			} else {
				if(accumulator.Surp > 0){
					auxColumn.Surp[lastRealOne] = accumulator.Surp# + data$Surprisal[lastRealOne]
						auxColumn.Att[lastRealOne] = accumulator.Att / numberOfTokensInCurrentToken #+ data$Attention[lastRealOne]

				}
				lastRealOne = i
					accumulator.Surp = data$Surprisal[i]
					accumulator.Att = data$Attention[i]
					numberOfTokensInCurrentToken = 1
			}
		}

	data$Surprisal = auxColumn.Surp
		data$Attention = auxColumn.Att

		data = data[data$CorpusID > -1,]


}


##########################
##########################



processDataForEvaluation = function(data, CSV_FILE, partNum)
{
# GET THE DATA
	cat(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE, sep=""), sep="\n")

#sink(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART1Statistics/statisticsResults/statistics-",CSV_FILE,".txt",sep=""))

#data <- read.table(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART1Statistics/statisticsFinal/",CSV_FILE, sep=""), header = TRUE, sep = "\t")

########################################
#       PREPROCESSING
########################################

		if(length(data$position) == 0) {
#sink()
			return(NULL)
		}



	data = doBasicPreprocessing(data)

# is 0 for skipped elements
		data$WLEN <- NULL




#########################################


#######################
# remove stuff that is not in the Edinburgh version
		data = data[!is.na(data$C.WLEN),]


#######################
# now should be almost identical to the Edinburgh data
#######################

#####################################
# model does not really deal with rare words


		data <- data[data$WordFreq > 0,]
		data <- data[data$WordFreq < 12,]


		if(FALSE){
			data = data[data$WordFreq < 12,]
				data = data[data$WordFreq > 0,]
		} else if(FALSE){
			data = data[data$WordFreq < 6,]
				data = data[data$WordFreq > 0,]
		}
##########################################
# Create some additional measures





# PREVFIX: previous word fixated or not = ?TODO CREATE
#linear.Surp = lmer(C.FPASSD ~ C.WLEN+ C.FREQBIN + C.FRQLAST + C.TRACEIC + C.BACKTR + C.NBIGR + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + residSurprisal + (residSurprisal|C.ITEM) + (1|Participant) + (1|C.ITEM), data=data.Nozeros)# FRQLAST: frequency of previous word. TODO CREATE
# LASTFIX: distance of previous fixation in number of words. TODO CREATE

	columnPREVFIX = data$Surprisal
		columnFRQLAST = data$Surprisal
		columnLASTFIX = data$Surprisal

		columnPREVFIX[1] = 0
		columnFRQLAST[1] = NA
		columnLASTFIX[1] = NA
		lastFix = NA
		for(i in (2:length(data$Surprisal))) {
			if((!is.na(data$Itemno[i])) && data$Itemno[i] == data$Itemno[i-1]) {
				if(data$FIXATED[i-1] == 1){
					columnPREVFIX[i] = 1
				} else {
					columnPREVFIX[i] = 0
				}
				columnFRQLAST[i] = data$WordFreq[i-1]
					columnLASTFIX[i] = i-lastFix
					if(!is.na(i-lastFix) && (i-lastFix > 3)) { # this would count as track loss
						columnLASTFIX[i] = NA
					}
			} else {
				columnPREVFIX[i] = NA
					lastFix = NA
					columnFRQLAST[i] = NA
					columnLASTFIX[i] = NA
			}
			if(data$FIXATED[i] == 1){
				lastFix = i
			}
		}
	data$PREVFIX = columnPREVFIX
		data$FRQLAST = columnFRQLAST
		data$LASTFIX = columnLASTFIX



#########
#should still be like the Edinburgh version

##########################################
# Removing regions with track loss (four successive non-fixated words)
		if(FALSE){
			column = data$Surprisal
				for(i in (1:length(column))) {
					`[`(column, i) = 1
				}
			for(i in (1:(length(column)-3))) {
				if(`[`(data$nFixations, i) == 0 && `[`(data$nFixations, i+1) == 0 && `[`(data$nFixations, i+2) == 0 && `[`(data$nFixations, i+3) == 0) {
					`[`(column, i)   = 0
						`[`(column, i+1) = 0
						`[`(column, i+2) = 0
						`[`(column, i+3) = 0
				}
			}
			data <- data[column == 1,]
		} else{
			data <- data
		}







#########################################


	maxPosition = max(data$position)

		cat(CSV_FILE)
		cat("\n")

#cat("Full Data:\n")
#cat(capture.output(summary(data)), sep="\n")
#cat("\n\n")
#cat("Restricted Positions: \n")

		data <- data[ which(data$position> 3 & data$position < maxPosition-2), ]

#cat(capture.output(summary(data)), sep="\n")
#cat("\n\n")

		data$C.ITEM <- 10000*data$C.FILE+data$C.WNUM

###### CLEANUP

		data <- data[data$C.GOPAST < 2000,]
		data <- data[!is.na(data$C.FRQLAST),]
		data <- data[data$C.FPASSD < 1000,]
		data <- data[data$C.FPASSD == 0 || data$C.FPASSD > 80,]


		return(data)

}



##########################
##########################



doDiscreteEvaluation = function(target, predicted) {



	correctPredictions = sum(abs(predicted == target))
		truePositives = sum(predicted * target)
		trueNegatives = sum((1-predicted) * (1-target))
		totalRealPositives = sum(target)
		totalRealNegatives = sum(1-target)
		totalItems = length(predicted)

		accuracy = correctPredictions / totalItems
		fixPrecision = truePositives / sum(predicted)
		fixRecall = truePositives / sum(target)
		skipPrecision = trueNegatives / sum(1-predicted)
		skipRecall = trueNegatives / sum(1-target)

		f1Fix = 2 * (fixPrecision * fixRecall) / (fixPrecision + fixRecall)
		f1Skip = 2 * (skipPrecision * skipRecall) / (skipPrecision + skipRecall)

		cat("RATES      ", mean(target),"  ", mean(predicted),"\n")
		cat("Accuracy   ", accuracy,"\n")
		cat("F1 Fix     ", f1Fix, "\n")
		cat("F1 Skip    ", f1Skip, "\n")
		return(list("Acc" = accuracy, "F1 Fix" = f1Fix, "F1 Skip" = f1Skip))
}





##########################
##########################



computeStatisticsOnData = function(data, returnStatistics) {

	if(! is.data.frame(data)) {
		return(NULL)
	}

#################### DATA CLEANUP




#LDIST = LAUN
#OBLP=OBLP
#WDLP= WDLP
#FILE, Participant (encoded via which file from the original corpus is used)

#PREVFIX  = whether the previous word was fixated
#FIXNO = number of fixations on this word
#TOTDUR = total fixation duration
#FFIXDUR = first fixation duration
#FDUR = first pass times
#CPOS = CPOS tag according to charniak parser
#MPOS = CPOS tag according to MINIPAR parser
#WordFreq = frequency, log-transformed per billion words based on gigaword corpus
#FRQLAST= log frequency of previous word
#NBIGR = bigram forward transitional probability, also estimated from gigaword, log
#BACKTR = bigram backward transitional probability, also estimated from gigaword, log
#GOPAST = go-past times = ?
#SACCWD = saccade length measured in words (this is mainly as an additional cue to identify errors in the dundee corpus like the one described above)



#################### MIXED MODELS

# dependent variables:
# FDUR
# FIXATED: number of fixations

# independent variables:
## OLEN: object length (includes punctuation)	= .
# C.WLEN: word length (excludes punctuation) =.
# // LDIST: launch distance = ?
# PREVFIX: previous word fixated or not = ?TODO CREATE
# OBJLPOS: landing position on object = OBLP
## WDLPOS: landing position on word = WDLP
## POS: part of speech = CPOS (what's the difference to FPOS?)
# WNOS: word number	= ID
# FREQBIN: lexical frequency. replaced by WordFreq
# FRQLAST: frequency of previous word. TODO CREATE
# // NBIGR: bigram probability = ?
# // BACKTR: backwards transitional probability	 = ?
# LASTFIX: distance of previous fixation in number of words. TODO CREATE



####################  CENTERING ###########


	data$C.WLEN <- data$C.WLEN - mean(data$C.WLEN, na.rm = TRUE)
		data$C.LDIST <- data$C.LDIST - mean(data$C.LDIST, na.rm = TRUE)
		data$C.PREVFIX <- data$C.PREVFIX - mean(data$C.PREVFIX, na.rm = TRUE)
		data$C.OBJLPOS <- data$C.WDLPOS - mean(data$C.WDLPOS, na.rm = TRUE)
		data$C.WNOS <- data$C.WNOS - mean(data$C.WNOS, na.rm = TRUE)
		data$C.FREQBIN <- data$C.FREQBIN - mean(data$C.FREQBIN, na.rm = TRUE)
		data$C.FRQLAST <- data$C.FRQLAST - mean(data$C.FRQLAST, na.rm = TRUE)
		data$C.TRACEIC <- data$C.TRACEIC - mean(data$C.TRACEIC, na.rm = TRUE)
		data$C.NBIGR <- data$C.NBIGR - mean(data$C.NBIGR, na.rm = TRUE)
		data$C.BACKTR <- data$C.BACKTR - mean(data$C.BACKTR, na.rm = TRUE)
		data$C.LASTFIX <- data$C.LASTFIX - mean(data$C.LASTFIX, na.rm = TRUE)

		data$Surprisal <- data$Surprisal - mean(data$Surprisal, na.rm = TRUE)
		data$Attention <- data$Attention - mean(data$Attention, na.rm = TRUE)



		data$C.FIXNO <- data$C.FIXNO - mean(data$C.FIXNO, na.rm = TRUE)




		if(length(data$Surprisal) == 0) {
			return(NULL)
		}


	data$C.FIXATED = pmin(data$C.FIXNO, 1)


########### COMPUTE RESIDUALS ##############

		data$C.residAttention       = residuals(lm(Attention ~ C.WLEN + C.FREQBIN +  WordFreq + Surprisal, data=data, na.action=na.exclude))
		data$C.residSurprisal       = residuals(lm(Surprisal ~ C.WLEN + C.FREQBIN +   WordFreq, data=data, na.action=na.exclude))
		data$C.residAttentionNoSurp = residuals(lm(Attention ~ C.WLEN + C.FREQBIN +   WordFreq, data=data, na.action=na.exclude))




		cat("Logistic Models","\n")




# Logistic Models
# - full
		if(DO_LOGISTIC) {


			log.Baseline = try(glmer(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST + C.PREVFIX +  C.OBJLPOS + WordFreq  + (1|C.SUBJ) + (1|C.ITEM), family = binomial("logit"), data=data))


				log.Full = try(glmer(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST + C.PREVFIX +  C.OBJLPOS + WordFreq  + C.residSurprisal + C.residAttention + (1|C.SUBJ) + (1|C.ITEM), family = binomial("logit"), data=data))
#

				cat(1,"\n")

#cat(capture.output(n1), sep="\n")

				log.Surp = try(glmer(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST + C.PREVFIX +  C.OBJLPOS + WordFreq  + C.residSurprisal + (1|C.SUBJ) + (1|C.ITEM), family = binomial("logit"), data=data))

				cat(2,"\n")

# - only Attention
				log.Att  = try(glmer(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST +  C.PREVFIX +  C.OBJLPOS + WordFreq + C.residAttentionNoSurp + (1|C.SUBJ) + (1|C.ITEM), family = binomial("logit"), data=data))

#cat(capture.output(n2), sep="\n")

				cat(3,"\n")
		} else {
			log.Baseline = NULL
				log.Full = NULL
				log.Surp = NULL
				log.Att  = NULL
		}





############## Predicting FPASSDUR

######### CENTERING FOR FIXATED ITEMS




# remove where FDUR is NAN
	data.Nozeros <- data[data$C.FPASSD > 0,]


		data.Nozeros <- data.Nozeros[complete.cases(data.Nozeros[,1]), ]
		data.Nozeros <- data.Nozeros[data.Nozeros$C.FPASSD < 1000,]
		data.Nozeros <- data.Nozeros[data.Nozeros$C.FPASSD > 80,]


		data.Nozeros$C.WLEN <- data.Nozeros$C.WLEN - mean(data.Nozeros$C.WLEN, na.rm = TRUE)
		data.Nozeros$PREVFIX <- data.Nozeros$PREVFIX - mean(data.Nozeros$PREVFIX, na.rm = TRUE)
		data.Nozeros$OBLP <- data.Nozeros$WDLP - mean(data.Nozeros$WDLP, na.rm = TRUE)
		data.Nozeros$ID <- data.Nozeros$ID - mean(data.Nozeros$ID, na.rm = TRUE)
		data.Nozeros$WordFreq <- data.Nozeros$WordFreq - mean(data.Nozeros$WordFreq, na.rm = TRUE)
		data.Nozeros$FRQLAST <- data.Nozeros$FRQLAST - mean(data.Nozeros$FRQLAST, na.rm = TRUE)
#data.Nozeros$NBIGR <- data.Nozeros$NBIGR - mean(data.Nozeros$NBIGR, na.rm = TRUE)
#data.Nozeros$BACKTR <- data.Nozeros$BACKTR - mean(data.Nozeros$BACKTR, na.rm = TRUE)
		data.Nozeros$LASTFIX <- data.Nozeros$LASTFIX - mean(data.Nozeros$LASTFIX, na.rm = TRUE)
		data.Nozeros$Surprisal <- data.Nozeros$Surprisal - mean(data.Nozeros$Surprisal, na.rm = TRUE)
		data.Nozeros$Attention <- data.Nozeros$Attention - mean(data.Nozeros$Attention, na.rm = TRUE)


#return(data)


# TODO
# C.WLEN should now be Token.Length, should be be taken care of when removing the -1 things




# Demberg:
# C.WLEN .
# wfreq .
# prevwordfreq .
# prevwordfix .
# launchdist
# landingpos .
# wordnoinsentence .
# resodbigramprob
# wordlen:wordfreq
# wordlen:landpos


# LASTFIX, LDIST
# mixed model with all factors

		cat("FPASSDUR","\n")

		data.Nozeros$residAttentionWithSurp = residuals(lm(Attention ~ C.WLEN + C.FRQLAST +    WordFreq + Surprisal, data=data.Nozeros, na.action=na.exclude))

		data.Nozeros$residSurprisal = residuals(lm(Surprisal ~ C.WLEN +   C.FRQLAST +    WordFreq, data=data.Nozeros, na.action=na.exclude))

		data.Nozeros$residAttentionNoSurp = residuals(lm(Attention ~ C.WLEN +  C.FRQLAST +    WordFreq, data=data.Nozeros, na.action=na.exclude))



		if(DO_LINEAR){

# with more predictors
#linear.Baseline = lmer(C.FPASSD ~ C.WNOS + C.WLEN+ C.FREQBIN + C.FRQLAST + C.TRACEIC + C.BACKTR + C.NBIGR + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + (1|Participant) + (1|C.ITEM), data=data.Nozeros)
#linear.Surp = lmer(C.FPASSD ~ C.WNOS + C.WLEN+ C.FREQBIN + C.FRQLAST + C.TRACEIC + C.BACKTR + C.NBIGR + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + residSurprisal +  (1|Participant) + (1|C.ITEM), data=data.Nozeros)

	linear.Baseline = lmer(C.FPASSD ~ C.WNOS + C.WLEN+ C.FRQLAST + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + (1|Participant) + (1|C.ITEM), data=data.Nozeros)

	linear.Surp = lmer(C.FPASSD ~ C.WNOS + C.WLEN+  C.FRQLAST +  C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + residSurprisal +  (1|Participant) + (1|C.ITEM), data=data.Nozeros)

if(sd(data$Attention) > 0) {
				linear.Att = lmer(C.FPASSD ~ C.WNOS + C.WLEN+ C.FREQBIN + C.FRQLAST + C.TRACEIC + C.BACKTR + C.NBIGR + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + residAttentionNoSurp + (1|Participant) + (1|C.ITEM), data=data.Nozeros)

				linear.Full = lmer(C.FPASSD ~ C.WNOS + C.WLEN+ C.FREQBIN + C.FRQLAST + C.TRACEIC + C.BACKTR + C.NBIGR + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + Surprisal + residAttentionWithSurp  + (1|Participant) + (1|C.ITEM), data=data.Nozeros)
} else {
   linear.Att = NULL
   linear.Full = NULL
}
		} else {
			linear.Surp = NULL
				linear.Att = NULL
				linear.Full = NULL
                      l9inear.Baseline = NULL
		}



	cat("Starting output","\n")
################### EVALUATION OUTPUT ######################

		cat("Mean Surprisal:", mean(data.Nozeros$NotCentered.Surprisal), sep="\n")

		approxPerp = computeApproxPerplexity(data)

# Significance for Fixations

		if(DO_LOGISTIC) {
			cat("\n", "Baseline",capture.output(summary(log.Baseline)), sep="\n")
				cat("\n","Full",capture.output(summary(log.Full)), sep="\n")

				cat("\n","Surprisal",capture.output(summary(log.Surp)), sep="\n")

				cat("\n","Attention",capture.output(summary(log.Att)), sep="\n")
		}
# Correlations

	cat("Att-Surp          ",cor(data$Surprisal, data$Attention),"\n")
		cat("Att-log(WordFreq) ", cor(data$Attention, data$WordFreq),"\n")
		cat("Att-WordLength    ", cor(data$Attention,data$C.WLEN),"\n")

#return(data)


# Significance for FPASSDUR

		if(DO_LINEAR){
cat("\n", "Baseline", capture.output(summary(linear.Baseline)), sep="\n")
			cat("\n","Surprisal",capture.output(summary(linear.Surp)), sep="\n")

				cat("\n","Attention",capture.output(summary(linear.Att)), sep="\n")

				cat("\n","Full",capture.output(summary(linear.Full)), sep="\n")
		}


# COMPARISON OF MAXIMUM-LIKELIHOOD SEQUENCE

	cat("\nATTENTION\n")
		predicted = getThresholdPredictor(data, data$NotCentered.Attention)
		resuAttDiscrete = doDiscreteEvaluation(data$FIXATED, predicted)


		cat("\nSURPRISAL\n")
		predicted = getThresholdPredictor(data, data$NotCentered.Surprisal)
		resuSurpDiscrete = doDiscreteEvaluation(data$FIXATED, predicted)


		cat("\nModel Fixation Rate  ",mean(data$Attended),"\n\n")
#sink()


#cat(".....")

		models <- list("data" = data,
				"log.Full" = log.Full, 
				"log.Surp" = log.Surp,
				"log.Att" = log.Att,
				"Att-Surp" = cor(data$Surprisal, data$Attention), 
				"Att-log(WordFreq)" = cor(data$Attention, data$WordFreq),
				"Att-WordLength" = cor(data$Attention,data$C.WLEN), 
				"FP.Surprisal" = linear.Surp, 
				"FP.Att" = linear.Att, "FP.Full" = linear.Full, "meanSurp" = mean(data.Nozeros$NotCentered.Surprisal), "approxPerp" = approxPerp,
				"Disc.Surp" = resuSurpDiscrete, "Disc.Att" = resuAttDiscrete)

#return(models)


		if(FALSE){
			statisticsOfInterest <- c(
					coef(summary(models$log.Full))["C.residAttention","z value"],
					coef(summary(models$log.Surp))["C.residSurprisal","z value"],
					coef(summary(models$log.Att))["C.residAttentionNoSurp","z value"],

					models$"Att-Surp",
					models$"Att-log(WordFreq)",
					models$"Att-WordLength",

					coef(summary(models$"FP.Full"))["residAttentionWithSurp","t value"],
					coef(summary(models$"FP.Att"))["residAttentionNoSurp","t value"],
					coef(summary(models$"FP.Surprisal"))["residSurprisal","t value"],

					models$"meanSurp",
					models$"approxPerp"$model,
					models$"approxPerp"$ratio
					)
		}


	if(!returnStatistics){
		return(models)
	}else{
		return(statisticsOfInterest)
	}

	postags = unique(data[,c("CPOS")])


		for(i in (1:length(postags)))
		{
			posdata <- data[ which(data$CPOS == postags[i]), ]
				if(length(posdata$CPOS) > 300 && max(posdata$WordFreq) != min(posdata$WordFreq) && length(unique(posdata[,c("Token")])) > 50) {
					att = posdata$Attention
						fpassdur = posdata$FDUR
						fixated = posdata$C.FIXNO
						surp = posdata$Surprisal
						cat("\n")
						cat("+++++++++\n")
						cat(capture.output(postags[i])[1])
						cat("\n")
						cat(capture.output(summary(posdata)), sep="\n")
						cat("Correlation between attention and surprisal:  ")
						cat(cor(att, surp), sep="\n")
						cat("Correlation between attention and duration:  ")
						cat(cor(att, fpassdur), sep="\n")
						cat("Correlation between attention and fixation:  ")
						cat(cor(att, fixated), sep="\n")
						cat("Correlation between surprisal and duration:  ")
						cat(cor(surp, fpassdur), sep="\n")
						cat("Correlation between surprisal and fixation:  ")
						cat(cor(surp, fixated), sep="\n")
						cat("Correlation between wordfreq and duration:  ")
						cat(cor(fpassdur, posdata$WordFreq), sep="\n")
						cat("Correlation between wordfreq and surprisal:  ")
						cat(cor(surp, posdata$WordFreq), sep="\n")
						cat("Correlation between wordfreq and attention:  ")
						cat(cor(att, posdata$WordFreq), sep="\n")

						if(sd(surp, na.rm = TRUE) * sd(att, na.rm = TRUE) * sd(fixated, na.rm = TRUE) * sd(fpassdur, na.rm = TRUE) == 0) {

						} else {

							posdata$residAttentionPOS = residuals(lm(Attention ~ C.WLEN + WordFreq +  FRQLAST +    WordFreq + Surprisal, data=posdata))

								posdata$residSurprisalPOS = residuals(lm(Surprisal ~ C.WLEN + WordFreq +  FRQLAST +    WordFreq, data=posdata))

								try(cat(capture.output(summary(lmer(FDUR ~ C.WLEN +  PREVFIX + OBLP + ID + WordFreq + FRQLAST +     WordFreq + Surprisal + residAttentionPOS + (1|Participant) + (1|C.ITEM), data=posdata))), sep="\n"))

								try(cat(capture.output(summary(lmer(FDUR ~ C.WLEN +  PREVFIX + OBLP + ID + WordFreq + FRQLAST +     WordFreq + residSurprisalPOS + (1|Participant) + (1|C.ITEM), data=posdata))), sep="\n"))

								try(cat(capture.output(summary(lmer(C.FIXNO ~ C.WLEN +  PREVFIX + OBLP + ID + WordFreq + FRQLAST +     WordFreq + Surprisal + residAttentionPOS + (1|Participant) + (1|C.ITEM), data=posdata))), sep="\n"))

# removed C.WLEN
								m1 = try(glm(formula = FIXATED ~   PREVFIX + OBLP + ID + WordFreq + FRQLAST +     WordFreq + Surprisal + residAttentionPOS, family = binomial("logit"), data=posdata))

								m2 = try(glm(formula = FIXATED ~   PREVFIX + OBLP + ID + WordFreq + FRQLAST +     WordFreq + Surprisal, family = binomial("logit"), data=posdata))

								cat("--- nFixations, RESID, LOGISTIC ---",sep="\n")
								try(cat(capture.output(m1), sep="\n"))

								cat("--- nFixations, RESID, LOGISTIC BASELINE ---",sep="\n")
								try(cat(capture.output(m2), sep="\n"))

								cat("--- LIKELIHOOD RATIO ---",sep="\n")
								try(cat(capture.output(lrtest(m1,m2)), sep="\n"))



						}

				}
		}
#sink()

#print("..")

#tokens = matrix(unique(data[,c("Token")]))



## tokens$Correlation <- apply(tokens, 1, function(token)
## {


##    tokenData = data[ which(data$Token == token), ]
##    #print(summary(tokenData))
##    if(length(tokenData$CPOS) > 100) {
##       correlation = cor(tokenData$Attention, tokenData$Surprisal)
##       cat(token, sep="\t")
##       cat(" \t  ")
##       cat(length(tokenData$CPOS), sep="\t")
##       cat(" \t  ")
##       cat(correlation, sep="\n")
##       return(correlation)
##    } else {
##       return(0.0)
##    }
## } )


}




outputSkippingPattern = function(CSV_FILE, fileName, partNum){
	sink(fileName)
		data <- read.table(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE, sep=""), header = TRUE, sep = "\t")
		data$C.FIXATED = pmin(data$C.FIXNO, 1)

		fixated = data$C.FIXATED
		fileName = data$C.FILE
		previousFile = -1
		for(i in (1:length(fixated))) {
			if(! is.na(fileName[i]) &( fileName[i] == previousFile)) {
				if(is.na(fixated[i])) {
					cat(-1, sep="\n")
				}else{
					cat(fixated[i], sep="\n")
				}
			}else{
				cat(-1, sep="\n")
			}
			if(!is.na(fileName[i])) {
				previousFile = fileName[i]
			}
		}

	sink()

}


outputSkippingPatternWithWords = function(CSV_FILE, fileName, column_name, partNum){
	sink(fileName)
		data <- read.table(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE, sep=""), header = TRUE, sep = "\t")
		fixated = pmin(data[, column_name], 1)

		fileName = data$C.FILE
		words = data$Word
		previousFile = -1
		for(i in (1:length(fixated))) {
			if(! is.na(fileName[i]) &( fileName[i] == previousFile)) {
				if(is.na(fixated[i])) {
					cat(paste(-1, words[i], sep=" "), sep="\n")
				}else{
					cat(paste(fixated[i], words[i],  sep=" "), sep="\n")
				}
			}else{
				cat(paste(-1, words[i], sep=" "), sep="\n")
			}
			if(!is.na(fileName[i])) {
				previousFile = fileName[i]
			}
		}

	sink()
}

