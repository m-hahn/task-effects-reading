from subprocess import call, Popen, PIPE
import os


# preprocess gigaword treebank
dirIn = "/disk/scratch2/s1582047/gigaword/"
dirOut = "/disk/scratch2/s1582047/gigawordProcessed/"

files = os.listdir(dirIn)


process = Popen(['java','-jar','lib/pennconverter.jar'], stdin=PIPE, stdout=PIPE,  bufsize=0)


for fileName in files:
         f = open(dirIn+"/"+fileName, 'r')

         fOut = open(dirOut+"/"+fileName, 'w')

         for line in f:
             if line.startswith("( ("):
               fOut.write(line)
               print(line)
               while 1:
                  try:
                     process.stdin.write(line)
                     while 1:

                       parsegraph = process.stdout.readline()
                       fOut.write(parsegraph)
                       if len(parsegraph) < 3:
                          break
                     break
                  except IOError:
                     print("AAA")
                     process = Popen(['java','-jar','lib/pennconverter.jar'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
         f.close()
         fOut.close()

process.stdin.close()


