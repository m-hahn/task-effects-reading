# TODO NOTE that lua outputs always with +1 (both the indices of positions and the numbers for the characters. Here, some ad hoc conversion is done. Better always check when running.)

from nltk.tag.mapping import map_tag

import os

import sys


sys.path.insert(0, 'neuratt/')
from langmod import readDictionary

print(sys.argv)


DATASET_DIR = sys.argv[1] #"/disk/scratch2/s1582047/ptb/annotation/"
ATTENTION_ANNOTATION_DIR = DATASET_DIR+"/"+sys.argv[2]+"/"
SURPRISAL_ANNOTATION_DIR = DATASET_DIR+"/"+sys.argv[3]+"/"

_, num2Chars = readDictionary("/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/num2Chars")


CORPUS_DIR = sys.argv[4] #"/disk/scratch2/s1582047/ptb/ptb2.0AnonymizedWithTags/"

print(CORPUS_DIR)

files = os.listdir(CORPUS_DIR)





statFile = open("/disk/scratch2/s1582047/dundee/PART1Statistics/statistics/att-surp-"+sys.argv[5]+".csv", 'w')

INPUT_LENGTH = int(sys.argv[6])


totalCount = 0
totalAttention = INPUT_LENGTH * [0]
totalSurprisal = INPUT_LENGTH * [0]


totalCountPerPos = {}
totalAttentionPerPos = {}

addOtherTable = True
OTHER_TABLE_DIR= "/disk/scratch2/s1582047/dundee/PART1ReTokenized/"
lookAtTheText = False



#head = "\"position\"\t\"Token\"\t\"POS\"\t\"Attention\"\t\"Surprisal\""
#columns = ["position", "Attention", "Surprisal"]
head = "\"position\"\t\"Attention\"\t\"Surprisal\""
if addOtherTable:
   isText = [True, False,True,False,False,False,False,False,False,False,False,False,False,False,False,False,True,True,False,False,False,False,True,False,False,False,True,False,False,False,False,False,False,False,False]
   headings = ["Token", "FILE", "SUBJ", "WNUM","REGOUTN","OLEN","WLEN","LDIST","PREVFIX","OBJLPOS","WDLPOS","TXFR","FIXNO","TOTDUR","FFIXDUR","FPASSD","POS","MPOS","BIGRP","BACKTRANS","WNOS","MITIC","DEP","FREQBIN","LEXSURP","ULXSPR","WORD","FRQLAST","TRACEIC","NBIGR","BACKTR","REGRESS","ULXSPRS","GOPAST","LASTFIX"]
   for heading in headings:
       head = head+"\t\""+heading+"\""

statFile.write(head+"\n")

for fileName in files:
         try:
            print(ATTENTION_ANNOTATION_DIR+fileName+".att.anno")
            print(SURPRISAL_ANNOTATION_DIR+fileName+".surp.anno")
            fAtt = open(ATTENTION_ANNOTATION_DIR+fileName+".att.anno", 'r')      


            fSurp = open(SURPRISAL_ANNOTATION_DIR+fileName+".surp.anno", 'r')
            if lookAtTheText:
               fText = open(CORPUS_DIR+fileName, 'r') 

            if addOtherTable:
                fOtherTable = open(OTHER_TABLE_DIR+fileName, 'r')
         except:
            continue
         att = {}
         surp = {}
         text = []
         pos = []

         otherTable = []
         if addOtherTable:
             for line in fOtherTable:
                line = line.rstrip()
                if len(line) > 5:
                    otherTable.append(line.split("\t"))
         if lookAtTheText:
           for line in fText:
             line = line.split()
             for token in line:
               splitToken = token.split("/")

               if len(splitToken[0]) > 0:
                  text.append(token)
                  
                  posTag = token.split("/")[1]
                  if posTag.endswith("0"):
                     posTag = posTag[:-1]
                  try:
                     posTag = map_tag('en-ptb', 'universal', posTag)
                  except:
                     print(posTag)
                  pos.append( posTag)
           fText.close()

         counter = 0
         for line in fAtt:
             line = line.split()
             if len(line) > 1:
                att[line[0]] = line[3]
                totalAttention[counter] += float(line[3])
                counter = counter+1
             else:
                counter = 0
                totalCount = totalCount+1
         fAtt.close()

         counter = 0
         for line in fSurp:
             line = line.split()
             if len(line) > 1:
                surp[line[0]] = line[2]
                totalSurprisal[counter] += float(line[2])
                counter = counter+1
                if int(line[1]) in num2Chars:
                    if lookAtTheText:
                       whatIsInPTB = text[int(line[0]) - 1].split("/")[0]
                       whatLUASays = num2Chars[int(line[1])-1]
                       if (whatIsInPTB.lower() != whatLUASays and int(line[1])-1 < 1000):
                          print("ERROR")
                          print(fileName)
                          print(line[0]+"  "+(line[1])+"  "+num2Chars[int(line[1])-1]+"  "+att[line[0]]+"  "+surp[line[0]])
                          print(text[int(line[0]) - 1])
                          crash()
                       #toPrint = str(counter)+"\t\""+whatIsInPTB+"\"\t\""+pos[int(line[0]) - 1]+"\"\t"+att[line[0]]+"\t"+surp[line[0]]
                       toPrint = str(counter)+"\t"+att[line[0]]+"\t"+surp[line[0]]
                    else:
                       toPrint = str(counter)+"\t"+att[line[0]]+"\t"+surp[line[0]]
                    if addOtherTable:
                         otherTableLine = otherTable[int(line[0]) - 1]
                         for q in range(0,len(otherTableLine)):
                             if isText[q]:
                                  otherTableLine[q] = "\""+otherTableLine[q]+"\""
                             toPrint = toPrint+"\t"+otherTableLine[q]
                    statFile.write(toPrint+"\n")
                else:
                    fytiuh = 0
                    print("ERROR "+(line[1])+" "+line[0])
             else:
                #print(line)
                counter = 0
         fSurp.close()

for i in range(0,20):
    print(i)
    if totalCount == 0:
      totalCount = 0.000001
    print(totalAttention[i] / totalCount)
    print(totalSurprisal[i] / totalCount)

print("NUMBER "+str(totalCount))
statFile.close()

