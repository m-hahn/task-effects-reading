# TODO
# (1) why the difference in significance?
# (2) what is it about WLEN?


evaluateAll.Corpus = function()
{
library(lme4)
#library(ggplot2)
    files = list.files(path="/disk/scratch2/s1582047/dundee/PART1Statistics/statisticsFinal/")
    for(q in (1:length(files))) {
         sink()
         cat(files[q], sep="\n")
         evaluate(files[q])
    }

}


randomlySampleData.Corpus = function(data, prob) {
    column = data$Surprisal
    for(q in (1:length(column))) {
         dice = runif(1,0.0,1.0)
         column[q] = dice - prob
    }

    data2 = data[column < 0,]


m1 = try(glm(formula = FIXATED ~ WLEN+ PREVFIX + OBJLPOS +  WordFreq + FRQLAST +     WordFreq  + Attention, family = binomial("logit"), data=data2))

m2 = try(glm(formula = FIXATED ~  WLEN+ PREVFIX + OBJLPOS +  WordFreq + FRQLAST +     WordFreq + Surprisal, family = binomial("logit"), data=data2))


cat("--- nFixations, RESID, LOGISTIC ---",sep="\n")
try(cat(capture.output(m1), sep="\n"))

cat("\n\n--- nFixations, RESID, LOGISTIC BASELINE ---",sep="\n")
try(cat(capture.output(m2), sep="\n"))

cat("\n\n--- LIKELIHOOD RATIO ---",sep="\n")
try(cat(capture.output(lrtest(m1,m2)), sep="\n"))

try(cat(capture.output(lrtest(m2,m3)), sep="\n"))

    
}


test.Corpus = function()
{
library(lme4)
#library(ggplot2)
evaluate('att-surp-uni-dundee-pg-test-autoencoding-20-700-0.7-100-R-4.6a0-emb_pg-test-langmod-20-200-0.7-100-R-4.6e1.csv')
}




evaluate.Corpus = function(CSV_FILE)
{

#sink(paste("/disk/scratch2/s1582047/dundee/PART1Statistics/statisticsResults/statistics-",CSV_FILE,".txt",sep=""))

data <- read.table(paste("/disk/scratch2/s1582047/dundee/PART1Statistics/statisticsFinal/",CSV_FILE, sep=""), header = TRUE, sep = "\t")


# check whether there actually are data


if(length(data$position) == 0) {
   sink()
   return(NULL)
}


#column = data$Surprisal

#for(i in (1:length(column))) {
#   `[`(column, i) = 1
#}

#for(i in (1:(length(column)-3))) {
#   if(`[`(data$FIXNO, i) == 0 && `[`(data$FIXNO, i+1) == 0 && `[`(data$FIXNO, i+2) == 0 && `[`(data$FIXNO, i+3) == 0) {
#      `[`(column, i)   = 0
#      `[`(column, i+1) = 0
#      `[`(column, i+2) = 0
#      `[`(column, i+3) = 0
#   }
#}
#
#data <- data[column == 1,]




##############################
##############################

# model does not really deal with rare words
if(FALSE){
   data = data[data$WordFreq < 12,]
   data = data[data$WordFreq > 0,]
} else if(FALSE){
   data = data[data$WordFreq < 6,]
   data = data[data$WordFreq > 0,]
}



maxPosition = max(data$position)

cat(CSV_FILE)
cat("\n")

cat("Full Data:\n")
cat(capture.output(summary(data)), sep="\n")
cat("\n\n")
cat("Restricted Positions: \n")
data <- data[ which(data$position> 3 & data$position < maxPosition-2), ]

cat(capture.output(summary(data)), sep="\n")
cat("\n\n")


##### add item ID

data$ITEM <- 10000*data$FILE+data$WNUM

#################### DATA CLEANUP

# TODO data loss




# remove cases where the reading time in Dundee has been calculated
# erroneously (has to do with line lengths during presentation)
data <- data[data$GOPAST < 2000,]

# remove cases with missing launch distance value
#data <- data[!is.na(data$LDIST),]
#data <- data[data$LDIST != -99,]

# remove cases with large launch distance values
#data <- data[(data$LDIST < 20 & data$LDIST > -30),]

# remove cases with missing FRQLAST value
data <- data[!is.na(data$FRQLAST),]


# outlier removal: 2 SDs
# doesn't work so well, there's still outliers left (go-past is
# particularly prone to outliers, may work better for other measures)
#data <- data[data$FPASSD < mean(data$FPASSD) + 2*sd(data$FPASSD),]
#data <- data[data$FPASSD > mean(data$FPASSD) - 2*sd(data$FPASSD),]

# alternative: simple cut-offs
 data <- data[data$FPASSD < 1000,]
 data <- data[data$FPASSD == 0 || data$FPASSD > 80,]










#################### MIXED MODELS

# dependent variables:
# FPASSD
# FIXATED: number of fixations

# independent variables:
## OLEN: object length (includes punctuation)	
# WLEN: word length (excludes punctuation)
# LDIST: launch distance
# PREVFIX: previous word fixated or not
# OBJLPOS: landing position on object	
## WDLPOS: landing position on word
## TXFR: unclear, probably a word frequency in a different corpus	
## POS: part of speech
## MPOS: part of speech computed by Minipar
## BIGRP: bigram probability (old)
## BACKTRANS: backward transitional probability (old)
# WNOS: word number	
## DEP: list of dependencies of the word
# FREQBIN: lexical frequency
## LEXSURP: lexicalized suprisal
## ULXSPR: unlexicalized suprisal
## WORD: word form
# FRQLAST: frequency of previous word
# TRACEIC: integration cost, different version
# NBIGR: bigram probability
# BACKTR: backwards transitional probability	
## ULXSPRS: unlexicalized suprisal (old)
# LASTFIX: distance of previous fixation in number of words

# center predictors
data$WLEN <- data$WLEN - mean(data$WLEN)
data$LDIST <- data$LDIST - mean(data$LDIST, na.rm = TRUE)
data$PREVFIX <- data$PREVFIX - mean(data$PREVFIX)
data$OBJLPOS <- data$WDLPOS - mean(data$WDLPOS)
data$WNOS <- data$WNOS - mean(data$WNOS)
data$FREQBIN <- data$FREQBIN - mean(data$FREQBIN)
data$FRQLAST <- data$FRQLAST - mean(data$FRQLAST, na.rm = TRUE)
data$TRACEIC <- data$TRACEIC - mean(data$TRACEIC)
data$NBIGR <- data$NBIGR - mean(data$NBIGR)
data$BACKTR <- data$BACKTR - mean(data$BACKTR)
data$LASTFIX <- data$LASTFIX - mean(data$LASTFIX, na.rm = TRUE)

data$Surprisal <- data$Surprisal - mean(data$Surprisal, na.rm = TRUE)
data$Attention <- data$Attention - mean(data$Attention, na.rm = TRUE)

if(sd(data$Surprisal, na.rm = TRUE) * sd(data$Attention, na.rm = TRUE) == 0) {
  return(NULL)
}

cat(capture.output(cor.test(data$Surprisal, data$FPASSD)), sep="\n")
cat(capture.output(cor.test(data$Attention, data$FPASSD)), sep="\n")
cat(capture.output(cor.test(data$Attention, data$FIXNO)), sep="\n")


data$residAttention = residuals(lm(Attention ~ WLEN + FREQBIN + NBIGR + FRQLAST + TRACEIC + NBIGR + BACKTR + WordFreq + Surprisal, data=data, na.action=na.exclude))
data$residSurprisal = residuals(lm(Surprisal ~ WLEN + FREQBIN + NBIGR + FRQLAST + TRACEIC + NBIGR + BACKTR + WordFreq, data=data, na.action=na.exclude))


cat("--- FIXNO, RESID ---",sep="\n")

try(cat(capture.output(summary(lmer(FIXNO ~ WLEN +  PREVFIX + OBJLPOS + WNOS + FREQBIN + FRQLAST + TRACEIC + NBIGR + BACKTR +  WordFreq + Surprisal + residAttention + (residAttention|SUBJ) + (residAttention|ITEM) + (1|SUBJ) + (1|ITEM), data=data))), sep="\n"))


data$FIXATED = pmin(data$FIXNO, 1)


# SHOULD IT REALLY BE WNUM???
m1 = try(glm(formula = FIXATED ~ WLEN+ PREVFIX + OBJLPOS +  WordFreq + FRQLAST +     WordFreq + Surprisal + Attention, family = binomial("logit"), data=data))

m2 = try(glm(formula = FIXATED ~  WLEN+ PREVFIX + OBJLPOS +  WordFreq + FRQLAST +     WordFreq + Surprisal, family = binomial("logit"), data=data))

cat("--- nFixations, RESID, LOGISTIC ---",sep="\n")
try(cat(capture.output(m1), sep="\n"))

cat("--- nFixations, RESID, LOGISTIC BASELINE ---",sep="\n")
try(cat(capture.output(m2), sep="\n"))

cat("--- LIKELIHOOD RATIO ---",sep="\n")
try(cat(capture.output(lrtest(m1,m2)), sep="\n"))




#######################################




data.Nozeros <- data[data$FPASSD != 0,]
data.Nozeros$WLEN <- data.Nozeros$WLEN - mean(data.Nozeros$WLEN, na.rm = TRUE)
data.Nozeros$LDIST <- data.Nozeros$LDIST - mean(data.Nozeros$LDIST, na.rm = TRUE)
data.Nozeros$PREVFIX <- data.Nozeros$PREVFIX - mean(data.Nozeros$PREVFIX, na.rm = TRUE)
data.Nozeros$OBJLPOS <- data.Nozeros$WDLPOS - mean(data.Nozeros$WDLPOS, na.rm = TRUE)
data.Nozeros$WNOS <- data.Nozeros$WNOS - mean(data.Nozeros$WNOS, na.rm = TRUE)
data.Nozeros$FREQBIN <- data.Nozeros$FREQBIN - mean(data.Nozeros$FREQBIN, na.rm = TRUE)
data.Nozeros$FRQLAST <- data.Nozeros$FRQLAST - mean(data.Nozeros$FRQLAST, na.rm = TRUE)
data.Nozeros$TRACEIC <- data.Nozeros$TRACEIC - mean(data.Nozeros$TRACEIC, na.rm = TRUE)
data.Nozeros$NBIGR <- data.Nozeros$NBIGR - mean(data.Nozeros$NBIGR, na.rm = TRUE)
data.Nozeros$BACKTR <- data.Nozeros$BACKTR - mean(data.Nozeros$BACKTR, na.rm = TRUE)
data.Nozeros$LASTFIX <- data.Nozeros$LASTFIX - mean(data.Nozeros$LASTFIX, na.rm = TRUE)

data.Nozeros$Surprisal <- data.Nozeros$Surprisal - mean(data.Nozeros$Surprisal, na.rm = TRUE)
data.Nozeros$Attention <- data.Nozeros$Attention - mean(data.Nozeros$Attention, na.rm = TRUE)

data.Nozeros$residAttention = residuals(lm(Attention ~ WLEN + FREQBIN + NBIGR + FRQLAST + TRACEIC + NBIGR + BACKTR + WordFreq + Surprisal, data=data.Nozeros, na.action=na.exclude))
data.Nozeros$residSurprisal = residuals(lm(Surprisal ~ WLEN + FREQBIN + NBIGR + FRQLAST + TRACEIC + NBIGR + BACKTR + WordFreq, data=data.Nozeros, na.action=na.exclude))

cat("--- FPASSD, Resid ---",sep="\n")

try(cat(capture.output(summary(lmer(FPASSD ~ WLEN +  PREVFIX + OBJLPOS + WNOS + FREQBIN + FRQLAST + TRACEIC + NBIGR + BACKTR +  WordFreq + Surprisal + residAttention + (residAttention|SUBJ) + (residAttention|ITEM) + (1|SUBJ) + (1|ITEM), data=data.Nozeros))), sep="\n"))



# LASTFIX, LDIST
# mixed model with all factors

#cat("--- FPASSD ---",sep="\n")

#cat(capture.output(summary(lmer(FPASSD ~ WLEN +  PREVFIX + OBJLPOS + WNOS + FREQBIN + FRQLAST + TRACEIC + NBIGR + BACKTR + WordFreq + Surprisal + Attention + (1|SUBJ) + (1|ITEM), data=data.Nozeros))), sep="\n")

#cat("--- FIXNO ---",sep="\n")

#cat(capture.output(summary(lmer(FIXNO ~ WLEN +  PREVFIX + OBJLPOS + WNOS + FREQBIN + FRQLAST + TRACEIC + NBIGR + BACKTR +  WordFreq + Surprisal + Attention + (1|SUBJ) + (1|ITEM), data=data))), sep="\n")







sink()
return(data)



postags = unique(data[,c("MPOS")])


for(i in (1:length(postags)))
{
   posdata <- data[ which(data$MPOS == postags[i]), ]
   if(length(posdata$MPOS) > 300 && max(posdata$WordFreq) != min(posdata$WordFreq) && length(unique(posdata[,c("Token")])) > 50) {
      att = posdata$Attention
      fpassdur = posdata$FPASSD
      fixated = posdata$FIXNO
      surp = posdata$Surprisal
      cat("\n")
      cat("+++++++++\n")
      cat(capture.output(postags[i])[1])
      cat("\n")
      cat(capture.output(summary(posdata)), sep="\n")
      cat("Correlation between attention and surprisal:  ")
      cat(cor(att, surp), sep="\n")
      cat("Correlation between attention and duration:  ")
      cat(cor(att, fpassdur), sep="\n")
      cat("Correlation between attention and fixation:  ")
      cat(cor(att, fixated), sep="\n")
      cat("Correlation between surprisal and duration:  ")
      cat(cor(surp, fpassdur), sep="\n")
      cat("Correlation between surprisal and fixation:  ")
      cat(cor(surp, fixated), sep="\n")
      cat("Correlation between wordfreq and duration:  ")
      cat(cor(fpassdur, posdata$WordFreq), sep="\n")
      cat("Correlation between wordfreq and surprisal:  ")
      cat(cor(surp, posdata$WordFreq), sep="\n")
      cat("Correlation between wordfreq and attention:  ")
      cat(cor(att, posdata$WordFreq), sep="\n")

if(sd(surp, na.rm = TRUE) * sd(att, na.rm = TRUE) * sd(fixated, na.rm = TRUE) * sd(fpassdur, na.rm = TRUE) == 0) {
   
} else {

   posdata$residAttentionPOS = residuals(lm(Attention ~ WLEN + FREQBIN + NBIGR + FRQLAST + TRACEIC + NBIGR + BACKTR + WordFreq + Surprisal, data=posdata))

   posdata$residSurprisalPOS = residuals(lm(Surprisal ~ WLEN + FREQBIN + NBIGR + FRQLAST + TRACEIC + NBIGR + BACKTR + WordFreq, data=posdata))

   try(cat(capture.output(summary(lmer(FPASSD ~ WLEN +  PREVFIX + OBJLPOS + WNOS + FREQBIN + FRQLAST + TRACEIC + NBIGR + BACKTR +  WordFreq + residSurprisalPOS + residAttentionPOS + (1|SUBJ) + (1|ITEM), data=posdata))), sep="\n"))
}

   }
}
sink()

#print("..")

#tokens = matrix(unique(data[,c("Token")]))



## tokens$Correlation <- apply(tokens, 1, function(token)
## {
   

##    tokenData = data[ which(data$Token == token), ]
##    #print(summary(tokenData))
##    if(length(tokenData$POS) > 100) {
##       correlation = cor(tokenData$Attention, tokenData$Surprisal)
##       cat(token, sep="\t")
##       cat(" \t  ")
##       cat(length(tokenData$POS), sep="\t")
##       cat(" \t  ")
##       cat(correlation, sep="\n")
##       return(correlation)
##    } else {
##       return(0.0)
##    }
## } )


}




