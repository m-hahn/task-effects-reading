from nltk.tag.stanford import NERTagger

import os

directoryIn = "/disk/scratch2/s1582047/ptb/ptb2.0PureWords/"

directoryOut = "/disk/scratch2/s1582047/ptb/ptb2.0Anonymized/"


st = NERTagger('/disk/scratch2/s1582047/resources/stanford-ner-2012-05-22/classifiers/english.all.3class.distsim.crf.ser.gz', '/disk/scratch2/s1582047/resources/stanford-ner-2012-05-22/stanford-ner-2012-05-22.jar') 

files = os.listdir("/disk/scratch2/s1582047/ptb/ptb2.0/")

print("STARTING")





for fileName in files:
         entityMapping = {}
         f = open(directoryIn+"/"+fileName, 'r')      
         print(directoryOut+"/"+fileName)
         fOut = open(directoryOut+"/"+fileName, 'w')      

         fOut.write("\n\n")

         for line in f:
             line = line.split()
             if len(line) > 0:
                 tagged = st.tag(line)
                 lastToken = ""
                 print(tagged)
                 for tagTuple in tagged:
                     if tagTuple[1] == "O":
                        if len(lastToken) > 0:
                           if not (lastToken in entityMapping):
                              entityMapping[lastToken] = len(entityMapping)
                           fOut.write("@entity"+str(entityMapping[lastToken])+" ")
                           lastToken = ""
                        fOut.write(tagTuple[0]+" ")
                     else:
                        lastToken = lastToken+" "

# NOTE assumes the last token is tagged O

         f.close()
         fOut.write("\n\n")


