# This script preserves the positions of tokens

# BUT it converts everything to LOWERCASE
# and removes final "\" from the tokens

import os

directoryOut = "/disk/scratch2/s1582047/ptb/ptb2.0PureWordsAnonymized/"

directoryIn = "/disk/scratch2/s1582047/ptb/ptb2.0AnonymizedWithTags/"



files = os.listdir("/disk/scratch2/s1582047/ptb/ptb2.0/")

for fileName in files:
         f = open(directoryIn+"/"+fileName, 'r')      
         fOut = open(directoryOut+"/"+fileName, 'w')      

         for line in f:
             line = line.split()
             for token in line:
                 word = token.split("/")[0]
                 if word.endswith("\\"):
                     word = word[:-1]
                 fOut.write(word.lower()+" ")
         f.close()
         fOut.write("\n")


