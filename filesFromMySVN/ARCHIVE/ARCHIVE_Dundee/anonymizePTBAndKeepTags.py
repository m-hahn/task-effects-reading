import os

# This script collapses named entities, not preserving the positions of tokens

# Also, it splits words by "-"

directoryIn = "/disk/scratch2/s1582047/ptb/ptb2.0Processed/"

directoryOut = "/disk/scratch2/s1582047/ptb/ptb2.0AnonymizedWithTags/"


files = os.listdir("/disk/scratch2/s1582047/ptb/ptb2.0/")



for fileName in files:
         entityMapping = {}
         f = open(directoryIn+"/"+fileName, 'r')      
         print(directoryOut+"/"+fileName)
         fOut = open(directoryOut+"/"+fileName, 'w')      

         fOut.write("\n\n")

         for line in f:
             line = line.split()
             if len(line) > 0:
                 lastToken = ""
                 for tokenWithTag in line:
                     tagTuple = tokenWithTag.split("/")
                     if tagTuple[1] != "NNP":
                        if len(lastToken) > 0:
                           if not (lastToken in entityMapping):
                              entityMapping[lastToken] = len(entityMapping)
                           fOut.write("@entity"+str(entityMapping[lastToken])+"/NNP ")
                           lastToken = ""
                        
                        tokensSeparatedByHyphen = tagTuple[0].split("-")
                        for i in range(0, len(tokensSeparatedByHyphen)):
                           tag = tagTuple[1]
                           if i < len(tokensSeparatedByHyphen) - 1:
                               tag = tag+"0"
                           fOut.write(tokensSeparatedByHyphen[i]+"/"+tag+" ")
                           if i < len(tokensSeparatedByHyphen) - 1:
                               fOut.write("-/- ")
                     else:
                        lastToken = lastToken+" "

# NOTE assumes the last token is tagged O

         f.close()
         fOut.write("\n\n")


