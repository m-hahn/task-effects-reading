# TODO NOTE that lua outputs always with +1 (both the indices of positions and the numbers for the characters. Here, some ad hoc conversion is done. Better always check when running.)

# September 2017
#python correlateAttAndSurprisalTreebank.py /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/annotation att-pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-1 surp-pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-1 /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/ pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-1 50 1
#python correlateAttAndSurprisalTreebank.py /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/annotation att-pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-2 surp-pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-2 /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/ pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-2 50 1
#python correlateAttAndSurprisalTreebank.py /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/annotation att-pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-3 surp-pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-3 /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/ pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-3 50 1
#python correlateAttAndSurprisalTreebank.py /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/annotation att-pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-4 surp-pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-4 /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/ pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-4 50 1

#python correlateAttAndSurprisalTreebank.py /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/annotation att-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-s surp-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-s /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/ pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-s 50 1

#python correlateAttAndSurprisalTreebank.py /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/annotation att-pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re5 surp-pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re5 /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/ pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re5 50 1

# December 2017
# python correlateAttAndSurprisalTreebank.py /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/annotation att-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50 surp-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50 /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1/ pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50 50 1
# same for PART2



# python correlateAttAndSurprisalTreebank.py /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART2/annotation att-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50 surp-pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-random-att /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART2/ pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-RANDOM_SURP_JAN_2018 50 2

# python correlateAttAndSurprisalTreebank.py /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART2/annotation att-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50 surp-pg-test-combined-50-1000-0.7-100encoder-1-full-att /afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART2/ pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-FULL_SURP_JAN_2018 50 2



#att-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50
#att-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-ir
#att-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-rs
#att-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-s


from nltk.tag.mapping import map_tag

import os

import sys


sys.path.insert(0, 'neuratt/')
from langmod import readDictionary

print(sys.argv)


DATASET_DIR = sys.argv[1] #"/disk/scratch2/s1582047/ptb/annotation/"
ATTENTION_ANNOTATION_DIR = DATASET_DIR+"/"+sys.argv[2]+"/"
SURPRISAL_ANNOTATION_DIR = DATASET_DIR+"/"+sys.argv[3]+"/"

_, num2Chars = readDictionary("/afs/cs.stanford.edu/u/mhahn/scr/num2CharsWithEntities")


CORPUS_DIR = sys.argv[4] #"/disk/scratch2/s1582047/ptb/ptb2.0AnonymizedWithTags/"


PART_NUMBER = sys.argv[7]

print(CORPUS_DIR)

files = os.listdir(CORPUS_DIR)


#print("...")
#print(sys.argv)
#print(PART_NUMBER)

#for i in range(1,10000000):
#   a = 1+i


#crash()


statFilePath = "/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART"+PART_NUMBER+"Statistics/statistics/att-surp-"+sys.argv[5]+".csv"
statFile = open(statFilePath, 'w')

INPUT_LENGTH = int(sys.argv[6])


totalCount = 0
totalAttention = INPUT_LENGTH * [0]
totalSurprisal = INPUT_LENGTH * [0]


totalCountPerPos = {}
totalAttentionPerPos = {}

addOtherTable = True
OTHER_TABLE_DIR= "/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART"+PART_NUMBER+"ReTokenized/"
lookAtTheText = False


# TOKEN will come from the table as the second column

#head = "\"position\"\t\"Token\"\t\"POS\"\t\"Attention\"\t\"Surprisal\""
#columns = ["position", "Attention", "Surprisal"]


# headings are as in prepareNumericalValuesFiles.py, should be synchronized


head = "\"position\"\t\"Attention\"\t\"Surprisal\"\t\"Attended\""
if addOtherTable:
   isText = [False, True,False, False,False,False,True,True,True,False,True,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,True,False,False,False,True,False,False,False,False,False,False,False, False, False, False, False,                   False,True,False,False,False,False,False,False,False,False,False,False,False,False,False,True,True,False,False,False,False,True,False,False,False,True,False,False,False,False,False,False,False,False, True]
   headings = ["LuaID", "Token", "CorpusID","Itemno","SentenceID","ID","Word","CPOS","FPOS","Head","DepRel","nFixations","Mean fixation duration per word","Total fixation duration per word","First fixation duration on every word","Fixation prob","nRefixations","nRegressions to","nRegressions from","Total regression from duration","Total regresssion to duration","nLong regressions from","nLong regressions to","n-1 fix prob","n+1 fix prob","n-2 fix prob","n+2 fix prob","Re-read prob","n-1 fix dur","n+1 fix dur","n+2 fix dur","n-2 fix dur","Participant","Frequency","First pass dur","Word length","WORD","TEXT","LINE","OLEN","WLEN","XPOS","WNUM","FDUR","OBLP","WDLP","FXNO","TXFR",         "C FILE", "C SUBJ", "C WNUM","C REGOUTN","C OLEN","C WLEN","C LDIST","C PREVFIX","C OBJLPOS","C WDLPOS","C TXFR","C FIXNO","C TOTDUR","C FFIXDUR","C FPASSD","C POS","C MPOS","C BIGRP","C BACKTRANS","C WNOS","C MITIC","C DEP","C FREQBIN","C LEXSURP","C ULXSPR","C WORD","C FRQLAST","C TRACEIC","C NBIGR","C BACKTR","C REGRESS","C ULXSPRS","C GOPAST","C LASTFIX", "fileName"]
# TODO apparently R doesn't care about strings not enclosed in "...", so can remove isText



   for heading in headings:
       head = head+"\t\""+heading+"\""

statFile.write(head+"\n")

for fileName in files:
         try:
           print(ATTENTION_ANNOTATION_DIR+fileName+".att.anno")
           print(SURPRISAL_ANNOTATION_DIR+fileName+".surp.anno")
           fAtt = open(ATTENTION_ANNOTATION_DIR+fileName+".att.anno", 'r')      
         except:
            print("Continue at 99")
            continue

         try: 
            fSurp = open(SURPRISAL_ANNOTATION_DIR+fileName+".surp.anno", 'r')
         except:
            print("File doesn't exist: "+SURPRISAL_ANNOTATION_DIR+fileName+".surp.anno")
            continue
         if lookAtTheText:
            fText = open(CORPUS_DIR+fileName, 'r') 

         if addOtherTable:
             fOtherTable = open(OTHER_TABLE_DIR+fileName, 'r')
         att = {}
         surp = {}
         text = []
         pos = []
         attended = {}

         otherTable = []
         if addOtherTable:
             for line in fOtherTable:
                line = line.rstrip("\n") # important that \t is not stripped away
                if len(line) > 5:
                    otherTable.append(line.split("\t"))
         if lookAtTheText:
           for line in fText:
             line = line.split()
             for token in line:
               splitToken = token.split("/")

               if len(splitToken[0]) > 0:
                  text.append(token)
                  
                  posTag = token.split("/")[1]
                  if posTag.endswith("0"):
                     posTag = posTag[:-1]
                  try:
                     posTag = map_tag('en-ptb', 'universal', posTag)
                  except:
                     print(posTag)
                  pos.append( posTag)
           fText.close()

         counter = 0
         for line in fAtt:
             line = line.split()
             if len(line) > 1:
                att[line[0]] = line[3]
                attended[line[0]] = line[2]
                totalAttention[counter] += float(line[3])
                counter = counter+1
             else:
                counter = 0
                totalCount = totalCount+1
         fAtt.close()

         counter = 0
         for line in fSurp:
             line = line.split()
             if len(line) > 1:
                #print(line)
                surp[line[0]] = line[2]
                totalSurprisal[counter] += float(line[2])
                counter = counter+1
                if int(line[1]) in num2Chars:
                    if lookAtTheText:
                       whatIsInPTB = text[int(line[0]) - 1].split("/")[0]
                       whatLUASays = num2Chars[int(line[1])-1]
                       if (whatIsInPTB.lower() != whatLUASays and int(line[1])-1 < 1000):
                          print("ERROR")
                          print(fileName)
                          print(line[0]+"  "+(line[1])+"  "+num2Chars[int(line[1])-1]+"  "+att[line[0]]+"  "+surp[line[0]])
                          print(text[int(line[0]) - 1])

                          print("IS NOT THE SAME "+whatLUASays)
#                          crash()
                       else:
                          print("IS THE SAME "+whatLUASays)
                       #toPrint = str(counter)+"\t\""+whatIsInPTB+"\"\t\""+pos[int(line[0]) - 1]+"\"\t"+att[line[0]]+"\t"+surp[line[0]]
                       toPrint = str(counter)+"\t"+att[line[0]]+"\t"+surp[line[0]]+"\t"+attended[line[0]]
                    else:
                       toPrint = str(counter)+"\t"+att[line[0]]+"\t"+surp[line[0]]+"\t"+attended[line[0]]
                    if addOtherTable:
                       if int(line[0]) -1 >= len(otherTable): # September 2017
                         toPrint = toPrint+"\t"+"\t".join(["NA"] * len(otherTable[0]))
                       else:
                         otherTableLine = otherTable[int(line[0]) - 1]
                         for q in range(0,len(otherTableLine)):
                             if isText[q]:
                                  otherTableLine[q] = "\""+otherTableLine[q]+"\""
                             toPrint = toPrint+"\t"+otherTableLine[q]
                    toPrint += "\t"+'"'+fileName+'"'
                    statFile.write(toPrint+"\n")
                else:
                    fytiuh = 0
                    print("ERROR "+(line[1])+" "+line[0])
             else:
                #print(line)
                counter = 0
         fSurp.close()

for i in range(0,20):
    print(i)
    if totalCount == 0:
      totalCount = 0.000001
    print(totalAttention[i] / totalCount)
    print(totalSurprisal[i] / totalCount)

print("NUMBER "+str(totalCount))
statFile.close()
print(statFilePath)
