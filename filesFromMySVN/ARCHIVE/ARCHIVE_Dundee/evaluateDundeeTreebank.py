from subprocess import call


DO_IT_ON_TESTSET = 1




## without entities
modelsAttAnonymized = [
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 20 700 10000 4.6 true 0.7 100 0.0001 5 false pg-test-20-700-0.7-100-R-2.3 a0-emb autoencoding true 3 true",	"pg-test-autoencoding-20-700-0.7-100-R-4.6a0-emb"], #[STOPPED AT 24]
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 20 700 10000 4.6 true 0.7 100 0.0001 5 false pg-test-20-700-0.7-100-R-2.3 a0-0.0001-emb autoencoding true 3 true",	"pg-test-autoencoding-20-700-0.7-100-R-4.6a0-0.0001-emb"],
["pg-test-20-700-0.7-100-R-4.6-R2a1"],
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 20 700 10000 4.6 true 0.7 100 0.01 5 false pg-test-20-700-0.7-100-R-2.3 a1-emb autoencoding true 3",	"pg-test-autoencoding-20-700-0.7-100-R-4.6a1-emb"], #[STOPPED at 24]
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 20 700 10000 4.6 true 0.7 100 0.0001 5 true pg-test-20-700-0.7-100-R-2.3 a2 autoencoding false",	"pg-test-autoencoding-20-700-0.7-100-R-4.6-R2a2"], #[NOT FINISHED]
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 20 700 10000 4.6 true 0.7 100 0.01 5 true pg-test-20-700-0.7-100-R-2.3 a1",	"pg-test-20-700-0.7-100-R-4.3-R2a1"],
["~/local/bin/th main-attention.lua 4 pg50665.txt true 60 20 700 10000 4.6 true 0.7 100 0.01 8 true pg-test-20-700-0.7-100-R-2.3 a1",	"pg-test-20-700-0.7-100-R-4.6-R2a1"],
["~/local/bin/th main-attention.lua 1 pg50665.txt false 60 50 1000 10000 4.6 false 0.7 100 0.0001 5 false NONE e0 autoencoding true 3 false",	"pg-test-autoencoding-50-1000-0.7-100e0"],
["~/local/bin/th main-attention.lua 1 pg50665.txt true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 true pg-test-autoencoding-50-1000-0.7-100-R-4.1a0 a1 autoencoding true 3",	"pg-test-autoencoding-50-1000-0.7-100-R-4.1-R2a1"],#resumed: [STOPPED at 78 avg loglikelihood]
["~/local/bin/th main-attention.lua 1 pg50665.txt true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100e0 a0-emb autoencoding true 3 true",	"pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb"], #[STOPPED at 75]
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100e0 a0-emb-0.0001 autoencoding true 3 true",	"pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb-0.0001"], #[RUNNING]
["~/local/bin/th main-attention.lua 1 pg50665.txt true 60 30 1000 10000 4.6 true 0.7 100 0.0001 5 false pg-test-30-1000-0.7-100e0 a0 autoencoding",	"pg-test-autoencoding-30-1000-0.7-100-R-4.6a0"],
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 30 1000 10000 4.3 true 0.7 100 0.0001 5 false pg-test-30-1000-0.7-100e0 a1 autoencoding true",	"pg-test-autoencoding-30-1000-0.7-100-R-4.3a1"],#[NOT FINISHED]
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 30 1000 10000 4.45 true 0.7 100 0.0001 10 false pg-test-30-1000-0.7-100e0 a1 autoencoding true 3 false", "pg-test-autoencoding-30-1000-0.7-100-R-4.45a1"],
["~/local/bin/th main-attention.lua 1 pg50665.txt true 60 30 1000 10000 4.0 true 0.7 100 0.0001 5 false pg-test-30-1000-0.7-100e0 a0 autoencoding",	"pg-test-autoencoding-30-1000-0.7-100-R-4.0a0"],#[only 84 min]
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 40 1000 10000 4.6 true 0.7 100 0.0001 5 false pg-test-40-1000-0.7-100e0 a0 autoencoding",	"pg-test-autoencoding-40-1000-0.7-100-R-4.6a0"],
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 40 1000 10000 4.0 true 0.7 100 0.0001 5 false pg-test-40-1000-0.7-100e0 a0 autoencoding"],#[only 84 min]
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 30 1000 10000 4.5 true 0.7 100 0.0001 10 false pg-test-30-1000-0.7-100e0 a1 autoencoding true 3",	"pg-test-autoencoding-30-1000-0.7-100-R-4.5a1"],
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 30 1000 10000 4.5 true 0.7 100 0.0001 10 true pg-test-autoencoding-30-1000-0.7-100-R-4.5a1 a2 autoencoding true 3",	"pg-test-autoencoding-30-1000-0.7-100-R-4.5-R2a2"],
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 30 1000 10000 4.5 true 0.7 100 0.0001 10 false pg-test-30-1000-0.7-100e0 a0emb autoencoding true 3",	"pg-test-autoencoding-30-1000-0.7-100-R-4.5a0emb"],
["~/local/bin/th main-attention.lua 3 pg50665.txt true 60 40 1000 10000 4.7 true 0.7 100 0.0001 10 false pg-test-40-1000-0.7-100e0 a0 autoencoding",	"pg-test-autoencoding-40-1000-0.7-100-R-4.7a0"], #[1.8 loglikelihood per word]


["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100e0 a0-emb-retry-entrop autoencoding true 3 true 0.01", "pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb-retry-entrop"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb a0-emb-retry-entrop-0.1 autoencoding true 3 true 0.1","pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb-retry-entrop-0.1"],
["~/local/bin/th main-attention.lua 4 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb a0-emb-retry-entrop-2.0 autoencoding true 3 true 2.0","pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb-retry-entrop-2.0"],
["~/local/bin/th main-attention.lua 3 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100e0 a0-emb-retry-entrop-1.0 autoencoding true 3 true 1.0","pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb-retry-entrop-1.0"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb a0-emb-retry-entrop-0.5 autoencoding true 3 true 0.5","pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb-retry-entrop-0.5"]
]

modelsAttEntities = [
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.6e0-entities-re a0-4.1-entities-entropy-1.0 autoencoding true 8 true 2.0", "pg-test-autoencoding-50-1000-0.7-100-R-3.8a0-4.1-entities-entropy-2.0"],
["~/local/bin/th main-attention.lua 4 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.6e0-entities-re a0-4.1-entities-entropy-0.1-real autoencoding true 8 true 0.1", "pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-0.1-real"],
["~/local/bin/th main-attention.lua 3 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.6e0-entities-re a0-4.1-entities-entropy-2.0-real autoencoding true 8 true 2.0", "pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-2.0-real"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.0 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.6e0-entities-re a0-4.1-entities-entropy-0.1-real autoencoding true 8 true 0.1", "pg-test-autoencoding-50-1000-0.7-100-R-4a0-4.1-entities-entropy-0.1-real"],
["~/local/bin/th main-attention.lua 3 false true 60 50 1000 10000 3.6 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.6e0-entities-re a0-3.6-entities-entropy-2.0-real autoencoding true 8 true 2.0", "pg-test-autoencoding-50-1000-0.7-100-R-3.6a0-3.6-entities-entropy-2.0-real"],
["~/local/bin/th main-attention.lua 4 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.6e0-entities-re a0-4.1-entities-entropy-0.1-real-new autoencoding true 8 true 0.1", "pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-0.1-real-new"],
["~/local/bin/th main-attention.lua 3 false true 60 50 1000 10000 4.6 true 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.6e0-entities-re a0-4.6-entities-entropy-2.0-real autoencoding true 8 true 2.0", "pg-test-autoencoding-50-1000-0.7-100-R-4.6a0-4.6-entities-entropy-2.0-real"]
]


# CSV_FILES SO FAR:
#['att-surp-uni-dundee-pg-test-autoencoding-50-1000-0.7-100-R-3.8a0-4.1-entities-entropy-2.0_pg-test-langmod-50-500-0.7-100langmod-entities.csv', 'att-surp-uni-dundee-pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-0.1-real_pg-test-langmod-50-500-0.7-100langmod-entities.csv', 'att-surp-uni-dundee-pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-2.0-real_pg-test-langmod-50-500-0.7-100langmod-entities.csv', 'att-surp-uni-dundee-pg-test-autoencoding-50-1000-0.7-100-R-4a0-4.1-entities-entropy-0.1-real_pg-test-langmod-50-500-0.7-100langmod-entities.csv']

modelsAtt = []  #modelsAttEntities + modelsAttAnonymized

modelsSurpAnonymized = [
["~/local/bin/th main-attention.lua 4 pg50665.txt false 60 20 200 10000 4.6 false 0.7 100 0.0001 5 false NONE e0 langmod",	"pg-test-langmod-20-200-0.7-100e0"],
["~/local/bin/th main-attention.lua 4 pg50665.txt true 60 20 200 10000 4.6 false 0.7 100 0.0001 5 false pg-test-langmod-20-200-0.7-100e0 e1 langmod true",	"pg-test-langmod-20-200-0.7-100-R-4.6e1"], #[DONE, perplexity is exp(88/20)]
["~/local/bin/th main-attention.lua 4 pg50665.txt false 60 20 700 10000 4.6 false 0.7 100 0.0001 5 false NONE e1 langmod true 3",	"pg-test-langmod-20-700-0.7-100e1"], # [DONE] perplexity ~66
["~/local/bin/th main-attention.lua 4 pg50665.txt false 60 30 700 10000 4.6 false 0.7 100 0.0001 5 false NONE e0 langmod",	"pg-test-langmod-30-700-0.7-100e0"],
["~/local/bin/th main-attention.lua 4 pg50665.txt false 60 40 700 10000 4.6 false 0.7 100 0.0001 5 false NONE e0 langmod",	"pg-test-langmod-40-700-0.7-100e0"],#(perplexity < 118)
["~/local/bin/th main-attention.lua 4 pg50665.txt false 60 50 700 10000 4.6 false 0.7 100 0.0001 5 false NONE e0 langmod",	"pg-test-langmod-50-700-0.7-100e0"],#(perplexity 76)
[]
]

# this should be encoder 1 (the one that was used for building the models containing the baseline) instead of what is here right now
# the annotation used should belong to full surprisal
modelsCombinedForOtherExperiments = [["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 full-surp combined false 9 true 5.0 full false WLEN", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-full-WLEN"],
["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 full-FIXNO combined false 9 true 5.0 full false NUMERICAL_FILE3", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-full-FIXNO"],
["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 full-WordFreq combined false 9 true 5.0 full false NUMERICAL_FILE4", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-full-WordFreq"],
["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 full-Surp combined false 9 true 5.0 full false NUMERICAL_FILE9", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-full-Surp"],

["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 random-surp combined false 9 true 5.0 full false fixed", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-random-Surp"],


[]
]



modelsCombinedPost = [
#["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 full-WLEN combined false 9 true 5.0 full false WLEN", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-full-WLEN"],
#["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 full-FIXNO combined false 9 true 5.0 full false NUMERICAL_FILE3", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-full-FIXNO"],
#["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 full-WordFreq combined false 9 true 5.0 full false NUMERICAL_FILE4", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-full-WordFreq"],
#["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 full-Surp combined false 9 true 5.0 full false NUMERICAL_FILE9", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-full-Surp"],

["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 random-0.58 combined false 9 true 5.0 full false fixed", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-random-0.58", 0.58],
#["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 random-0.62 combined false 9 true 5.0 full false fixed", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-random-0.62", 0.62],
#["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 random-0.7 combined false 9 true 5.0 full false fixed", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-random-0.7", 0.7],
#["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 random-0.8 combined false 9 true 5.0 full false fixed", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-random-0.8", 0.8],
#["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 random-0.9 combined false 9 true 5.0 full false fixed", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-random-0.9", 0.9],
#["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 random-1.0 combined false 9 true 5.0 full false fixed", "pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45-random-1.0", 1.0],

##["~/local/bin/th main-attention.lua 1 true true 60 50 1000 10000 5.0 false 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100encoder-1-full-att full-surp combined false 8 true 5.0 none false", "pg-test-combined-50-1000-0.7-100encoder-1-full-att"],

[]
]




#HAE anonymized models operate on a different lexicon and should be useless?

# the attention network is set to FALSE so it computes with fixed attention
modelsCombinedForFullAttention = [

#["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-full-att full-surp combined false 9 true 5.0 full false", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-full-att"],

["~/local/bin/th main-attention.lua 1 true true 60 50 1000 10000 5.0 false 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100encoder-1-full-att full-surp combined false 8 true 5.0 none false", "pg-test-combined-50-1000-0.7-100encoder-1-full-att"],

[]
]







modelsCombinedForRandomAttention = [["~/local/bin/th main-attention.lua 4 true true 60 50 1000 10000 4.5 false 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-random-att random-surp combined false 9 true 5.0 full false", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-random-att"]]


modelsSurpEntities = [
["~/local/bin/th main-attention.lua 4 false true 60 50 500 10000 4.1 false 0.7 100 0.0001 20 false NONE langmod-entities langmod true 8 true 1.0", "pg-test-langmod-50-500-0.7-100langmod-entities"]
]

modelsSoft = [
["/afs/inf.ed.ac.uk/user/s15/s1582047/local/bin/th main-attention.lua 15 false true 60 50 1000 10000 5.0 false 0.7 100 0.0001 20 false pg-test-combined-soft-50-1000-0.7-100first-rs first-rs-att-15 combined-soft true 8 true 5.0 rs false", "pg-test-combined-soft-50-1000-0.7-100-R-5first-rs-att-15"],

["/afs/inf.ed.ac.uk/user/s15/s1582047/local/bin/th main-attention.lua 16 false true 60 50 1000 10000 5.0 false 0.7 100 0.0001 20 false pg-test-combined-soft-50-1000-0.7-100first-rs first-s-att-16 combined-soft true 8 true 5.0 s false", "pg-test-combined-soft-50-1000-0.7-100-R-5first-s-att-16"],

["/afs/inf.ed.ac.uk/user/s15/s1582047/local/bin/th main-attention.lua 15 false true 60 50 1000 10000 5.0 false 0.7 100 0.0001 20 false pg-test-combined-soft-50-1000-0.7-100-R-0first-R-15 first-R-15 combined-soft true 8 true 5.0 full false", "pg-test-combined-soft-50-1000-0.7-100-R-0first-R-15"],

["/afs/inf.ed.ac.uk/user/s15/s1582047/local/bin/th main-attention.lua 16 false true 60 50 1000 10000 5.0 false 0.7 100 0.0001 20 false pg-test-combined-soft-50-1000-0.7-100-R-0first-R-16 first-R-16 combined-soft true 8 true 5.0 full false", "pg-test-combined-soft-50-1000-0.7-100-R-0first-R-16"],
[]
]


modelsQ = [

[]
]

#that is, models trained using a baseline
modelsBaseline = [
["/afs/inf.ed.ac.uk/user/s15/s1582047/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 0 true pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 baseline-50 combined true 8 true 5.0 full true", "pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50"],
["/afs/inf.ed.ac.uk/user/s15/s1582047/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 0 true pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 baseline-50-rs combined true 8 true 5.0 rs true", "pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-rs"],
["/afs/inf.ed.ac.uk/user/s15/s1582047/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 0 true pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 baseline-50-s combined true 8 true 5.0 s true", "pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-s"],
["/afs/inf.ed.ac.uk/user/s15/s1582047/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 0 true pg-test-combined-45-1000-0.7-100-R-5-R2baseline-45 baseline-50-ir combined true 8 true 5.0 ir true", "pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-ir"],
[]
]
#





modelsSurp = modelsSurpEntities #+ modelsSurpAnonymized

modelsCombinedEntities = [
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 1.0 false 0.7 100 0.0001 20 false pg-test-autoencoding-50-1000-0.7-100-R-4.6e0-entities-re a0-4.1-entities-entropy-1.0-combined combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-1a0-4.1-entities-entropy-1.0-combined"], #ablation: attention entirely based on surprisal
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 true pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined a0-4.1-entities-entropy-1.0-combined combined true 8 true 2.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined"], #ablation: attention entirely based on surprisal
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.1 false 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined a0-4.1-entities-entropy-1.0-combined-retry combined true 8 true 2.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined-retry"], #ablation: attention entirely based on surprisal
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined a0-4.1-entities-entropy-1.0-combined-ablation-of-context combined true 8 true 2.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined-ablation-of-context"],
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.6 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined a0-4.6-entities-entropy-1.0-combined-ablation-of-context combined true 8 true 2.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.6a0-4.6-entities-entropy-1.0-combined-ablation-of-context"],
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.1 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined a0-4.1-entities-entropy-1.0-combined-full combined true 8 true 2.0 none", "pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined-full"],
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 6.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined-ablation-of-context a0-6.0-entities-entropy-1.0-combined-ablation-of-context combined true 8 true 2.0 rs", "pg-test-combined-50-1000-0.7-100-R-6a0-6.0-entities-entropy-1.0-combined-ablation-of-context"], # NOTE since ablation ios not a parameter, maybe this didnt actually work
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined-ablation-of-context a0-5.0-entities-entropy-1.0-combined-ablation-of-context combined true 8 true 2.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy-1.0-combined-ablation-of-context"], # run for 12%
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined a0-5.0-entities-entropy-1.0-combined-full combined true 8 true 2.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy-1.0-combined-full"], #run for 7%
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined-ablation-of-context a0-5.0-entities-entropy5.0-1.0-combined-ablation-of-context combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ablation-of-context"],
#["~/local/bin/th main-attention.lua 3 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined a0-5.0-entities-entropy5.0-1.0-combined-real-full combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full"],
#["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 1.0 false 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100combined-secondtry combined-secondtry-cont combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-1combined-secondtry-cont"],
#["~/local/bin/th main-attention.lua 3 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-4.1a0-4.1-entities-entropy-1.0-combined a0-5.0-entities-entropy5.0-1.0-combined-onlysurp-real combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-onlysurp-real"],

#full, 5.0
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-full-re1 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-full-re2 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-full-re3 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-full-re4 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-full-re5 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-full-re6 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-full-re7 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-full-re8 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-full-re9 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-full-re10 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-re10"],


["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re1 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re2 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re3 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re4 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re5 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re6 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re7 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re8 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re9 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re10 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re10"],






#full,4.5
["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-full-re1 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-full-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-full-re2 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-full-re2"],
["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-full-re3 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-full-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-full-re4 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-full-re4"],
["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-full-re5 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-full-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-full-re6 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-full-re6"],
["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-full-re7 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-full-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-full-re8 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-full-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-full-re9 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-full-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-full-re10 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-full-re10"],



["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-full-re1 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-full-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-full-re2 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-full-re2"],
["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-full-re3 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-full-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-full-re4 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-full-re4"],
["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-full-re5 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-full-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-full-re6 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-full-re6"],
["~/local/bin/th main-attention.lua 2 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-full-re7 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-full-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-full-re8 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-full-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-full-re9 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-full-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-full-re10 combined true 8 true 5.0 none", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-full-re10"],






#ir,4.5
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re1 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re2 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re3 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re4 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re5 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re6 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re7 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re8 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re9 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re10 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-ir-re10"],



["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re1 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re2 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re3 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re4 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re5 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re6 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re7 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re8 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re9 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re10 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-ir-re10"],








#rs,4.5
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re1 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re2 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re3 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re4 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re5 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re6 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re7 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re8 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re9 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re10 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-rs-re10"],


["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re1 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re2 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re3 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re4 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re5 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re6 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re7 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re8 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re9 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re10 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-rs-re10"],

#ir,5.0 START
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re1 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re2 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re3 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re4 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re5 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re6 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re7 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re8 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re9 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re10 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-ir-re10"],


["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re1 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re2 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re3 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re4 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re5 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re6 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re7 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re8 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re9 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re10 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re10"],




#rs,5.0
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re1 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re2 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re3 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re4 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re5 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re6 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re7 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re8 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re9 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re10 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re10"],


["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re1 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re2 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re3 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re4 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re5 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re6 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re7 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re8 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re9 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re10 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re10"],



#s,5.0
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-s-re1 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-s-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-s-re2 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-s-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-s-re3 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-s-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-s-re4 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-s-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-s-re5 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-s-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-s-re6 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-s-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-s-re7 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-s-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-s-re8 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-s-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-s-re9 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-s-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-s-re10 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-s-re10"],


["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re1 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re2 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re3 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re4 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re5 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re6 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re7 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re8 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re9 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re10 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re10"],





#s,4.5
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-s-re1 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-s-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-s-re2 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-s-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-s-re3 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-s-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-s-re4 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-s-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-s-re5 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-s-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-s-re6 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-s-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-s-re7 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-s-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-s-re8 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-s-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-s-re9 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-s-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-4.5-entities-entropy5.0-1.0-combined-real-s-re10 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0-combined-real-s-re10"],


["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-s-re1 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-s-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-s-re2 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-s-re2"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-s-re3 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-s-re3"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-s-re4 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-s-re4"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-s-re5 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-s-re5"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-s-re6 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-s-re6"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-s-re7 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-s-re7"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-s-re8 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-s-re8"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-s-re9 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-s-re9"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 4.5 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full a0-4.5-entities-entropy5.0-1.0encoder-1-s-re10 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-4.5a0-4.5-entities-entropy5.0-1.0encoder-1-s-re10"],



["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re1 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-rs-re1"],



[]
]


modelsWithPredictionForSurp = [
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0encoder-1-full-re1 combined true 8 true 5.0 full", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-full-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0encoder-1-s-re1 combined true 8 true 5.0 s", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-s-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re1 combined true 8 true 5.0 rs", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-rs-re1"],
["~/local/bin/th main-attention.lua 1 false true 60 50 1000 10000 5.0 true 0.7 100 0.0001 20 false pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re1 combined true 8 true 5.0 ir", "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0encoder-1-ir-re1"],
[]
]

modelsCombined = modelsCombinedEntities

modelsQA = [["~/local/bin/th main-attention.lua 1 pg50665.txt false 5 3 500 10001 4.6 false 0.7 99 0.0001 5 false NONE e00 qa true 5 false"]]

# CREATE THE ATTENTION AND SURPRISAL ANNOTATIONS



if DO_IT_ON_TESTSET:
   CORPUS_NUMBER = "10"
   PART_NUMBER = "2"
else:
   CORPUS_NUMBER = "9"
   PART_NUMBER = "1"



PREDICTION_FOR_ATTENTION = False

def createAttentionAndSurprisalAnnotations():


   modelNames = []
   GPU_NUMBER = "3"

   if PREDICTION_FOR_ATTENTION:
     modelList = modelsWithPredictionForSurp
   else:
     modelList =  modelsAtt+modelsSurp+modelsCombined+modelsCombinedForFullAttention+modelsCombinedForRandomAttention

   ##modelList =  modelsCombinedForFullAttention#+modelsCombinedForRandomAttention
   ##modelList = modelsCombinedForOtherExperiments 
   #modelList = modelsBaseline +modelsCombinedForOtherExperiments + modelsCombinedForRandomAttention + modelsCombinedForFullAttention
   modelList = modelsBaseline +modelsCombinedForOtherExperiments + modelsCombinedForRandomAttention + modelsCombinedForFullAttention
   modelList = modelsCombinedPost
   #modelList = modelsBaseline

   #e.g. for evaluating surprisal resulting from some special attention
   # here it is assumed that the code of main-attention.lua is modified accordingly when running


   for model in modelList: 

     print(len(model))


     if len(model) >= 2:
       if "  " in model[0]:
         print("WARNING 228 double whitespace")

       command = model[0].split(" ")
       # in 1-based lua: (has to do +1)
       # - 1 GPU
       # - 2 doing evaluation utput?
       # - 3 LOAD?
       # - 14 attention to be reloaded?
       # - 15 file to be loaded
       # - 16 suffix
       # - 18 do training?
       # - 19 corpus id
       identifier = model[1]
       # DO NOT DO elseif, THEY WILL BE DONE SEQUENTIALLY
       if len(command) == 16+2:
           command.append("autoencoding")
       if len(command) == 17+2:
           command.append("false")
       if len(command) == 18+2:
           command.append("7")
       if len(command) == 19+2:
           command.append("false")
       if len(command) == 20+2:
           command.append("0.0")
       if len(command) == 21+2:
           command.append("full")
       if len(command) == 22+2:
           command.append("true")

       print("*********")
       print(command)
       print(len(command))

       # now make sure all the parameters have the right values
       if len(model)> 2:
          file = open("/disk/scratch2/s1582047/attention-"+GPU_NUMBER, 'w')
          file.write(str(model[2])+"\n")
          file.close()
          print("NOTE THAT THE ANNOTATION OF OTHER MODELS MIGHT BE OVERWRITTEN")
       elif model in modelsCombinedForFullAttention:
          file = open("/disk/scratch2/s1582047/attention-"+GPU_NUMBER, 'w')
          file.write("1.0\n")
          file.close()
          print("NOTE THAT THE ANNOTATION OF OTHER MODELS MIGHT BE OVERWRITTEN")
       elif model in modelsCombinedForRandomAttention:
          file = open("/disk/scratch2/s1582047/attention-"+GPU_NUMBER, 'w')
          file.write("0.62\n")
          file.close()
          print("NOTE THAT THE ANNOTATION OF OTHER MODELS MIGHT BE OVERWRITTEN")
         #command[9+1] = "false"

       command[0] = "/afs/inf.ed.ac.uk/user/s15/s1582047/local/bin/th"
       command[1] = "main-attention.lua"
       command[1+1] = GPU_NUMBER
       command[2+1 ] = "true"
       command[3+1] = "true"
       command[14+1] = "true"
       command[15+1] = identifier
       command[16+1] = "dundee"+command[16+1]
       command[18+1] = "false"
       command[19+1] = CORPUS_NUMBER
       

       

       print("+++++++")
       print(command)
       #command.insert(0,"CUDA_VISIBLE_DEVICES=0")
       print(" ".join(command))
       call(command)
       print("~~~~~~")
       modelNames.append(command[16+1])


   print("MODELS")
   for model in modelList:
     if len(model)>1:
       print(model[1])
   print("PERP ANNOTATIONS GENERATED:")
   for modelName in modelNames:
       print(modelName)
       print("     /disk/scratch2/s1582047//dundeetreebank/parts/PART"+PART_NUMBER+"//annotation/perp-*"+modelName+".txt")


def createJointTables():
   listOfTablesCreated = []

   if PREDICTION_FOR_ATTENTION:
     modelAttList = modelsWithPredictionForSurp
     modelSurpList = modelsWithPredictionForSurp
   else:
     modelAttList = modelsAtt+modelsCombined+modelsCombinedForFullAttention+modelsCombinedForRandomAttention
     modelSurpList = modelsSurp+modelsCombined+modelsCombinedForFullAttention+modelsCombinedForRandomAttention



   modelAttList = modelsCombinedForFullAttention#+modelsCombinedForRandomAttention
   modelSurpList = modelsCombinedForFullAttention#+modelsCombinedForRandomAttention

   modelAttList  = modelsBaseline +modelsCombinedForOtherExperiments + modelsCombinedForRandomAttention + modelsCombinedForFullAttention
   modelSurpList = modelsBaseline +modelsCombinedForOtherExperiments + modelsCombinedForRandomAttention + modelsCombinedForFullAttention


   modelAttList = modelsCombinedPost
   modelSurpList = modelsCombinedPost

   for modelAtt in modelAttList:
     if len(modelAtt) >= 2:
       commandAtt = modelAtt[0].split(" ")
       seqLengthAtt = commandAtt[5+1]
       for modelSurp in modelSurpList:
          if len(modelSurp) >= 2:

            # if the surprisal one is a combined model, make sure they are the same
            if "combined" in modelSurp[1] and (modelAtt != modelSurp):
               continue


            commandSurp = modelSurp[0].split(" ")
            seqLengthSurp = commandSurp[5+1]
            if seqLengthSurp == seqLengthAtt:
               nameForTable = "dundee-"+modelAtt[1]+"_"+modelSurp[1]
               call(["python","correlateAttAndSurprisalTreebank.py", "/disk/scratch2/s1582047/dundeetreebank/parts/PART"+PART_NUMBER+"/annotation/", "att-"+modelAtt[1]+"/", "surp-"+modelSurp[1]+"/", "/disk/scratch2/s1582047/dundeetreebank/parts/PART"+PART_NUMBER+"TextTagged/", nameForTable,  str(seqLengthSurp), PART_NUMBER])
               call(["python","addUnigramProbabilityAnnotationTreebank.py", nameForTable, PART_NUMBER])
               print("Have generated "+"statisticsFinal/att-surp-uni-"+nameForTable+".csv")
               listOfTablesCreated.append("att-surp-uni-"+nameForTable+".csv")
               print("/disk/scratch2/s1582047/dundeetreebank/parts/PART"+PART_NUMBER+"/annotation/ att-"+modelAtt[1]+"/  surp-"+modelSurp[1]+"/   /disk/scratch2/s1582047/dundeetreebank/parts/PART"+PART_NUMBER+"TextTagged/")
   print("CSV_FILES:")
   print("\n".join(listOfTablesCreated))

#createAttentionAndSurprisalAnnotations() # run it in lstm/lstm
createJointTables()

# FOR EACH SUITABLE PAIR, create a joint table

#python correlateAttAndSurprisal.py /disk/scratch2/s1582047/dundee/PART1/annotation/ att-pg-test-20-700-0.7-100-R-4.6-R2a1/ surp-pg-test-langmod-20-200-0.7-100e0/ /disk/scratch2/s1582047/dundee/PART1TextTagged/ dundee1-20-700-0.7-100-R-4.6-R2a1-lm-200 20

#python addUnigramProbabilityAnnotation.py dundee1-20-700-0.7-100-R-4.6-R2a1-lm-200

#GENERATES:
#att-surp-uni-dundee1-20-700-0.7-100-R-4.6-R2a1-lm-200.csv



