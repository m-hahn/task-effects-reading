
GOLD = "/disk/scratch2/s1582047/dundeetreebank/parts/PART1/patterns/GOLD.txt"

MODEL = "/disk/scratch2/s1582047/dundeetreebank/parts/PART1/patterns/pattern-att-pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb-0.0001"


# /disk/scratch2/s1582047/dundeetreebank/parts/PART1Statistics/statisticsFinal/att-surp-uni-dundee-pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb-0.0001_pg-test-langmod-50-700-0.7-100e0.csv


def findBigramStatistics(PATTERN_INPUT):
   bigramsWithPatterns = {}
   fPattern = open(PATTERN_INPUT, 'r')
   lastSeen = "-1"
   lastWord = "-1"
   for line in fPattern:
      line = line.rstrip().split(" ")
      seen = line[0]
      word = line[1]
      if lastSeen != "-1" and seen != "-1":
         if (lastWord+" "+word) in bigramsWithPatterns:
            entry = bigramsWithPatterns[lastWord+" "+word]
         else:
            entry = {"0 0" : 0, "1 0" : 0, "0 1" : 0, "1 1" : 0, "COUNT" : 0}
            bigramsWithPatterns[lastWord+" "+word] = entry
         entry[lastSeen+" "+seen] = entry[lastSeen+" "+seen] + 1
         entry["COUNT"] = entry["COUNT"] + 1
      lastSeen = seen
      lastWord = word
   return bigramsWithPatterns


bigramsGOLD = findBigramStatistics(GOLD)
bigramsModel = findBigramStatistics(MODEL)

for bigram in bigramsGOLD:
      entry = bigramsGOLD[bigram]
      if entry["COUNT"] > 50:
         print(bigram+"  "+str(entry)+"  "+str(bigramsModel[bigram]))
  


def getPatternDistributions(PATTERN_INPUT, END_FOR_ALTERNATION_BLOCKS = 1, ALTERNATION_LENGTH = 3, LENGTH_OF_PATTERN_SEQUENCE = 10):
   numberOfAlternationSequencesFound = 0
   alternationSequenceCounts = {}
   counts = {}
   pattern = []
   numberOfSequencesFound = 0
   fPattern = open(PATTERN_INPUT, 'r')

   def findZeroOneAlternations():
       if len(pattern)>2:
         if pattern[len(pattern) - 2] == END_FOR_ALTERNATION_BLOCKS  and pattern[len(pattern) - 1] != pattern[len(pattern) - 2]:
           alternations = [0] * ALTERNATION_LENGTH
           alternationsSoFar = 0
           lengthOfCurrentBlock = 0
           for i in range(len(pattern) - 2, 0, -1):
              if pattern[i] != pattern[i+1] and i < len(pattern) - 2:
                  alternations[ALTERNATION_LENGTH - alternationsSoFar - 1] = lengthOfCurrentBlock
                  alternationsSoFar = alternationsSoFar + 1
                  lengthOfCurrentBlock = 1
              else:
                  lengthOfCurrentBlock = lengthOfCurrentBlock + 1
              if alternationsSoFar == ALTERNATION_LENGTH:
                 if str(alternations) in alternationSequenceCounts:
                    alternationSequenceCounts[str(alternations)] = alternationSequenceCounts[str(alternations)] + 1
                 else:
                    alternationSequenceCounts[str(alternations)] = 1
                 return 1
       return 0
  
   def processPatterns():
       newAlternationSequencesFound = findZeroOneAlternations()
       if len(pattern) > LENGTH_OF_PATTERN_SEQUENCE:
          subpattern = pattern[(len(pattern)-LENGTH_OF_PATTERN_SEQUENCE):]
          if not(str(subpattern) in counts):
              counts[str(subpattern)] = 1
          else:
              counts[str(subpattern)] = counts[str(subpattern)] + 1
          return 1,newAlternationSequencesFound
       else:
          return 0,newAlternationSequencesFound

   for line in fPattern:
      line = line.rstrip().split(" ")[0]
      if line == '-1':
         pattern = []
      else:
         pattern.append(int(line))
         newPatterns, newAltSeqs = processPatterns()
         numberOfSequencesFound = numberOfSequencesFound + newPatterns
         numberOfAlternationSequencesFound = numberOfAlternationSequencesFound + newAltSeqs
   fPattern.close()
   return alternationSequenceCounts, counts, numberOfAlternationSequencesFound, numberOfSequencesFound

alternationSequenceCountsGold, _, numberOfAlternationSequencesFoundGold, _ = getPatternDistributions(GOLD, 1, 1, 10)

print(alternationSequenceCountsGold)
alternationSequenceCountsModel, _, numberOfAlternationSequencesFoundModel, _ = getPatternDistributions(MODEL, 1, 1, 10)
print(alternationSequenceCountsModel)



