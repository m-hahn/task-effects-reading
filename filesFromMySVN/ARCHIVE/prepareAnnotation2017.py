import os

# attention, and Dundee
#annotation = "/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1Statistics/statistics/att-surp-pg-test-combined-50-1000-0.7-100-R-5replication-attention-3-1.csv"
#annotation = "/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1Statistics/statistics/att-surp-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50-s.csv"
#annotation = "/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART1Statistics/statistics/att-surp-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50.csv"
annotation = "/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART2Statistics/statistics/att-surp-pg-test-combined-50-1000-0.7-100-R-5-R2baseline-50.csv"

surprisalPath = "/afs/cs.stanford.edu/u/mhahn/scr/NEAT_EMNLP_DATA/PART2PredefinedNumericalWithSurp"


data = map(lambda x:x.split("\t"), open(annotation, "r").read().split("\n"))

header = data[0]
#print(header)

def lookup(x,y):
 #  print y
   return x[header.index(y)]

data = data[1:]

files = os.listdir(surprisalPath)
annotations = {}
for fileName in files:
   annotations[fileName] = map(lambda x:x.split("  "), open(surprisalPath+"/"+fileName, "r").read().split("\n"))
   #annotations[fileName] = dict(map(lambda x: (int(x[4]), x), annotations[fileName])

textsFound = {}
lastText = None
position = 0
surprisalForFile = None

print("\t".join(header + ['"Anno.Position"', '"Anno.Token"', '"Anno.FIXNO"', '"Anno.LogWordFreq"', '"Anno.WordLength"', '"Anno.UNKNOWN"', '"Anno.UNKNOWN2"', '"Anno.Position2"', '"Anno.Surprisal"'])) 


for datapoint in data:
   if len(datapoint) < 2:
     continue
   text = lookup(datapoint, '"TEXT"').replace('"','')
   participant = lookup(datapoint, '"Participant"').replace('"','')
   if text == "-1":
     continue
   fileName = "dundee-"+text+"-"+participant
   fileName = lookup(datapoint, '"fileName"').replace('"','')
   if not (fileName in textsFound):
     textsFound[fileName] = 1
     print(fileName)
   if fileName != lastText:
     position = 0
     try:
       surprisalForFile = annotations[fileName]
     except KeyError:
       continue
       print("Not found")
     print("NEW TEXT")
     lastText = fileName
  # print(".............")
   #print(text)
 #  print(datapoint)
   positionForFile = int(datapoint[4])-2
   numericalAnnotation = surprisalForFile[positionForFile]
   #print(numericalAnnotation)
   if len(numericalAnnotation) < 2:
     continue
   position +=1
   word = datapoint[5].replace('"','')
   
   word2 = numericalAnnotation[1]
   #if word != word2:
   #  print("ERROR "+word+" "+word2)
   print("\t".join(datapoint+numericalAnnotation))
     #print("# "+fileName)
#   print([len(surprisalForFile), lookup(datapoint, '"position"')])
   
