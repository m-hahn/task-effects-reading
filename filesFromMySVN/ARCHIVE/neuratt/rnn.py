__docformat__ = 'restructedtext en'


import os
import sys
import timeit

import numpy

import theano
import theano.tensor as T

from theano import config
exception_verbosity='high'
optimizer=None

from theano import *


import langmod





def initWeightMatrix(rng, name, columnsNum, rowsNum, rangeOfRand):
        W_values = numpy.asarray(rng.uniform(low=-rangeOfRand, high=rangeOfRand,size=(columnsNum, rowsNum)),dtype=theano.config.floatX)
        #    if activation == theano.tensor.nnet.sigmoid:
        W_values *= 4
        return theano.shared(value=W_values, name=name, borrow=True)



class RecurrentModule(object):

    def __init__(self, rng, externalInput, recurrentInput, n_ex_in, n_rec, n_ex_out, W=None, b=None,
                 activation=T.tanh):

        n_in = n_ex_in + n_rec
        n_out = n_rec + n_ex_out

        [self.W_re, self.W_er, self.W_ee, self.W_rr] = W
        [self.b_e, self.b_r] = b


        lin_output_rec = self.b_r + T.dot(self.W_er, externalInput) + T.dot(self.W_rr, recurrentInput)
        lin_output_ext = self.b_e + T.dot(self.W_re, recurrentInput) + T.dot(self.W_ee, externalInput)

        self.externalOutput = T.nnet.softmax(lin_output_ext.T).T
        self.recurrentOutput = activation(lin_output_rec)





class RecurrentNetwork(object):

    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, W=None, b=None):

        self.currentOutput_ext = initWeightMatrix(rng, 'out-cur-ext', n_ex_out, 1, 0.24)
        self.currentRecurrentNodes = initialRecurrentState
        self.previousOutput_ext = initWeightMatrix(rng, 'out-prev', n_ex_out, 1, 0.24)


        self.n_ex_in = n_ex_in
        self.n_ex_out = n_ex_out
        self.n_rec = n_rec
        self.time = 0
        self.rng = rng
        n_in = n_ex_in + n_rec
        n_out = n_ex_out + n_rec

        self.W_er = initWeightMatrix(rng,'W_er',n_rec, n_ex_out, numpy.sqrt(6. / 100.))
        self.W_re = initWeightMatrix(rng,'W_re',n_ex_in,n_rec, numpy.sqrt(6. / 100.))
        self.W_ee = initWeightMatrix(rng,'W_ee',n_ex_in,n_ex_out, numpy.sqrt(6. / 100.))
        self.W_rr = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(6. / 100.))

        self.b_e = initWeightMatrix(rng,'b_e', n_ex_out, 1, 0) #
        self.b_r = initWeightMatrix(rng,'b_r', n_rec, 1, 0) #

        self.W = [self.W_re, self.W_er, self.W_ee, self.W_rr]
        self.b = [self.b_e, self.b_r]


        # L1 norm ; one regularization option is to enforce L1 norm to
        # be small
        self.L1 = (
            abs(self.W_re).sum() +
            abs(self.W_er).sum() +
            abs(self.W_ee).sum() +
            abs(self.W_rr).sum()
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            (self.W_re ** 2).sum() +
            (self.W_er ** 2).sum() +
            (self.W_ee ** 2).sum() +
            (self.W_rr ** 2).sum()
        )
        self.params = [self.W_re, self.W_er, self.W_ee, self.W_rr]


        temp1 = theano.function(inputs=[],outputs=self.W_re )
        temp2 = theano.function(inputs=[],outputs= self.currentRecurrentNodes)

        


    def addTimeStep(self, externalInput):
        newReccurentModule = RecurrentModule(self.rng, externalInput, self.currentRecurrentNodes, self.n_ex_in, self.n_rec, self.n_ex_out, W=self.W, b=self.b,activation=T.tanh)
        self.currentRecurrentNodes = newReccurentModule.recurrentOutput
        self.previousOutput_ext = self.currentOutput_ext
        self.currentOutput_ext = newReccurentModule.externalOutput


def pushToQueue(queueArray, element):
        for i in range(-len(queueArray)+1, 0):
               queueArray[-i].set_value(queueArray[-i-1].get_value())
        queueArray[0].set_value(element)





def test_rnn(learning_rate=0.001,
             L1_reg=0.00,
             L2_reg=0.000,
             n_rec = 100,
             DEPTH_IN_TIME = 5):

	print '... building the model'

	rng = numpy.random.RandomState(1234)

        

        initialRecurrentState =  initWeightMatrix(rng, 'rec', n_rec, 1, 0.24) 

	classifier = RecurrentNetwork(
		rng=rng,
                initialRecurrentState=initialRecurrentState,
		n_ex_in = langmod.NUM_OF_CHARS, 
		n_ex_out = langmod.NUM_OF_CHARS, 
		n_rec = n_rec,
	)


        inputLists = range(0,DEPTH_IN_TIME)

        SOFInputList = numpy.zeros((langmod.NUM_OF_CHARS,1), dtype=theano.config.floatX)
	SOFInputList[langmod.NUM_OF_CHARS-1] = 1
        for i in range(0,DEPTH_IN_TIME):
             newInputList = shared(SOFInputList, name='inp '+str(i)) 
             classifier.addTimeStep(newInputList)
             inputLists[DEPTH_IN_TIME-1-i] = newInputList

        negative_log_likelihood_at_current_timestep = - T.mean(T.log(T.dot(classifier.previousOutput_ext.T, inputLists[0])))


	cost = (
	    negative_log_likelihood_at_current_timestep
	    + L1_reg * classifier.L1
	    + L2_reg * classifier.L2_sqr
	)

	gparams = [T.grad(cost, param) for param in classifier.params]


	updates = [
            (param, param - learning_rate *  gparam)
	    # clipping the gradients seems to make theano slower
            #(param, param - learning_rate * T.maximum(-1,T.minimum(1, gparam)))
	    for param, gparam in zip(classifier.params, gparams)
	]


	train_model = theano.function(
	    inputs=[],
	    outputs=cost,
	    updates=updates
	    #givens={
		#x: train_set_x[index * batch_size: (index + 1) * batch_size],
		#y: train_set_y[index * batch_size: (index + 1) * batch_size]
	    #}
	)

        currentRecurrentOutput = classifier.currentRecurrentNodes

        recurrentOutput = theano.function(
            inputs=[],
            outputs=currentRecurrentOutput
        )


        temp = theano.function(
              inputs=[],
              outputs=classifier.currentOutput_ext
        )
        #temp2 = theano.function(
        #      inputs=[],
        #      outputs=T.log(T.dot(classifier.previousOutput_ext.T, inputLists[0]))
        #)
        #temp3 = theano.function(
        #      inputs=[],
        #      outputs=T.dot(classifier.previousOutput_ext.T, inputLists[0])
        #)
        #temp2 = theano.function(
        #      inputs=[],
        #      outputs=T.mean(T.log(T.dot(classifier.previousOutput_ext.T, inputLists[0])))
        #)
    ###############
    # TRAIN MODEL #
    ###############
	print '... training'

	(corpus, chars, counts, maxCount) = langmod.read_corpus()

        log_likelihood_f = theano.function(inputs=[], outputs=negative_log_likelihood_at_current_timestep)

        lastRecurrentStates = []
        for i in range(0,DEPTH_IN_TIME):
                newRecList = initWeightMatrix(rng, 'rec', n_rec, 1, 0.24)
                lastRecurrentStates.append(newRecList)


	for i in range(0, len(corpus)-1):
                newInputList = numpy.zeros((langmod.NUM_OF_CHARS,1), dtype=theano.config.floatX)
		newInputList[min(langmod.NUM_OF_CHARS-1, corpus[i])] = 1
                pushToQueue(inputLists, newInputList)
                pushToQueue(lastRecurrentStates, recurrentOutput())
		cost = train_model()
		print(str(i) + ' '+chars[corpus[i]]+' '+str(corpus[i])+' '+str(cost))
                #print('External '+str(temp()))
		#print(temp2())
                #print(temp3())
		initialRecurrentState.set_value(lastRecurrentStates[DEPTH_IN_TIME-1].get_value())



