__docformat__ = 'restructedtext en'


import os
import sys
import timeit

import numpy

import theano
import theano.tensor as T

from theano import config
exception_verbosity='high'
optimizer=None

from theano import *


import langmod





def initWeightMatrix(rng, name, columnsNum, rowsNum, rangeOfRand):
        W_values = numpy.asarray(rng.uniform(low=-rangeOfRand, high=rangeOfRand,size=(columnsNum, rowsNum)),dtype=theano.config.floatX)
        #    if activation == theano.tensor.nnet.sigmoid:
        W_values *= 4
        return theano.shared(value=W_values, name=name, borrow=True)



class RecurrentModule(object):

    def __init__(self, rng, externalInput, recurrentInput, n_ex_in, n_rec, n_ex_out, W=None, b=None,
                 activation=T.tanh):

        n_in = n_ex_in + n_rec
        n_out = n_rec + n_ex_out

        [self.W_re, self.W_er, self.W_ee, self.W_rr] = W
        [self.b_e, self.b_r] = b







class RecurrentNetwork(object):

    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, W=None, b=None):

        self.currentOutput_ext = initWeightMatrix(rng, 'out-cur-ext', n_ex_out, 1, 0.24)
        self.currentRecurrentNodes = initialRecurrentState
        self.previousOutput_ext = initWeightMatrix(rng, 'out-prev', n_ex_out, 1, 0.24)


        self.n_ex_in = n_ex_in
        self.n_ex_out = n_ex_out
        self.n_rec = n_rec
        self.time = 0
        self.rng = rng
        n_in = n_ex_in + n_rec
        n_out = n_ex_out + n_rec

        self.W_er = initWeightMatrix(rng,'W_er',n_rec, n_ex_out, numpy.sqrt(6. / 100.)) # bei initWeightMatrix: first rownumber, then colnumber
        self.W_rr = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(6. / 100.))

        self.b_e = initWeightMatrix(rng,'b_e', n_ex_out, 1, 0) #
        self.b_r = initWeightMatrix(rng,'b_r', n_rec, 1, 0) #

        
        self.params = [self.W_er, self.W_rr]


        #temp1 = theano.function(inputs=[],outputs=self.W_re )
        #temp2 = theano.function(inputs=[],outputs= self.currentRecurrentNodes)

        recurrentNodes = T.fmatrix()
        externalInput = T.fmatrix()

################

class RecurrentNetworkReconstructor(RecurrentNetwork):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, W=None, b=None):
        super(RecurrentNetworkReconstructor, self).__init__(rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec,W,b)
        self.W_re = initWeightMatrix(rng,'W_re',n_ex_in,n_rec, numpy.sqrt(6. / 100.))
        self.W_ee = initWeightMatrix(rng,'W_ee',n_ex_in,n_ex_out, numpy.sqrt(6. / 100.))
        self.params = [self.W_re, self.W_er, self.W_ee, self.W_rr]
        self.L1 = (
            abs(self.W_re).sum() +
            abs(self.W_er).sum() +
            abs(self.W_ee).sum() +
            abs(self.W_rr).sum()
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            (self.W_re ** 2).sum() +
            (self.W_er ** 2).sum() +
            (self.W_ee ** 2).sum() +
            (self.W_rr ** 2).sum()
        )
        self.output_ext = []
        self.output_ext_funcs = []


    def addTimeStep(self, externalInput, i):
        lin_output_rec = self.b_r + T.dot(self.W_er, externalInput) + T.dot(self.W_rr, self.currentRecurrentNodes)
        lin_output_ext = self.b_e + T.dot(self.W_re, self.currentRecurrentNodes) + T.dot(self.W_ee, externalInput)
        externalOutput = T.nnet.softmax(lin_output_ext.T).T
        recurrentOutput = T.tanh(lin_output_rec)

         #newReccurentModule = RecurrentModule(self.rng, externalInput, , self.n_ex_in, self.n_rec, self.n_ex_out, W=self.W, b=self.b,activation=T.tanh)

        self.currentRecurrentNodes = recurrentOutput
        self.previousOutput_ext = self.currentOutput_ext
        #self.currentOutput_ext = externalOutput
        self.output_ext.append(externalOutput)
        self.output_ext_funcs.append(theano.function(inputs=[],outputs=externalOutput))

################

class RecurrentNetworkReader(RecurrentNetwork):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, W=None, b=None):
        super(RecurrentNetworkReader, self).__init__(rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec,W,b)
        self.attentionSoFar = shared(0)
        self.W_att = initWeightMatrix(rng,'W_att',n_rec, n_ex_in, numpy.sqrt(6. / 100.))
        self.params = [self.W_er, self.W_rr, self.W_att]
        self.L1 = (
            abs(self.W_er).sum() +
            abs(self.W_rr).sum()
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            (self.W_er ** 2).sum() +
            (self.W_rr ** 2).sum()
        )

        # only for debugging/inspecting
        self.attention_scores = []

    def addTimeStep(self, externalInput, i):
        attention = T.mean(T.nnet.sigmoid(T.dot(self.currentRecurrentNodes.T, T.dot(self.W_att, externalInput))))
        self.attention_scores.append(theano.function(inputs=[],outputs=attention))

        externalInput = attention * externalInput

	lin_output_rec = self.b_r + T.dot(self.W_er, externalInput) + T.dot(self.W_rr, self.currentRecurrentNodes)
        recurrentOutput = T.tanh(lin_output_rec)


        #newReccurentModule = RecurrentModule(self.rng, T.mean(attention) * externalInput, self.currentRecurrentNodes, self.n_ex_in, self.n_rec, self.n_ex_out, W=self.W, b=self.b,activation=T.tanh)


        self.currentRecurrentNodes = recurrentOutput
        self.attentionSoFar += attention


def pushToQueue(queueArray, element):
        for i in range(-len(queueArray)+1, 0):
               queueArray[-i].set_value(queueArray[-i-1].get_value())
        queueArray[0].set_value(element)





def test_rnn(corpus='corpus7.txt',
             learning_rate=0.001,
             L1_reg=0.00,
             L2_reg=0.00001,
             attention_reg=0.1,
             n_rec = 10,
             TIME_LENGTH = 10,
             NUM_OF_CHARS = 10):

	print '... building the model'

	rng = numpy.random.RandomState(1234)

        initialRecurrentStateReader =  initWeightMatrix(rng, 'rec', n_rec, 1, 0.24) 

	reader = RecurrentNetworkReader(
		rng=rng,
                initialRecurrentState=initialRecurrentStateReader,
		n_ex_in = NUM_OF_CHARS, 
		n_ex_out = NUM_OF_CHARS, 
		n_rec = n_rec,
	)
        


        # the input list variables
        inputLists = []
        SOFInputList = numpy.zeros((NUM_OF_CHARS,1), dtype=theano.config.floatX)
	SOFInputList[NUM_OF_CHARS-1] = 1
        for i in range(0,TIME_LENGTH):
             newInputList = shared(SOFInputList, name='inp '+str(i)) 
             reader.addTimeStep(newInputList, i)
             inputLists.append(newInputList)



        reconstructor = RecurrentNetworkReconstructor(
                rng=rng,
                initialRecurrentState=reader.currentRecurrentNodes,
		n_ex_in = NUM_OF_CHARS, 
		n_ex_out = NUM_OF_CHARS, 
		n_rec = n_rec,
        )

        for i in range(0,TIME_LENGTH):
             reconstructor.addTimeStep(inputLists[i], i)



        overall_negative_log_likelihood = shared(0)
        for i in range(0, TIME_LENGTH):
              overall_negative_log_likelihood += - T.mean(T.log(T.dot(reconstructor.output_ext[i].T, inputLists[i])))

	cost = (
	    overall_negative_log_likelihood
	    + L1_reg * reader.L1
            + L1_reg * reconstructor.L1
            + L2_reg * reader.L2_sqr
	    + L2_reg * reconstructor.L2_sqr
            + attention_reg * reader.attentionSoFar
	)

	gparams = [T.grad(cost, param) for param in reader.params + reconstructor.params]


	updates = [
            (param, param - learning_rate *  gparam)
	    # clipping the gradients seems to make theano slower
            #(param, param - learning_rate * T.maximum(-1,T.minimum(1, gparam)))
	    for param, gparam in zip(reader.params + reconstructor.params, gparams)
	] #+ [(initialRecurrentStateReader, reader.currentRecurrentNodes)]


	train_model = theano.function(
	    inputs=[],
	    outputs=cost,
	    updates=updates
	    #givens={
		#x: train_set_x[index * batch_size: (index + 1) * batch_size],
		#y: train_set_y[index * batch_size: (index + 1) * batch_size]
	    #}
	)

        

        #temp2 = theano.function(
        #      inputs=[],
        #      outputs=T.log(T.dot(classifier.previousOutput_ext.T, inputLists[0]))
        #)
        #temp3 = theano.function(
        #      inputs=[],
        #      outputs=T.dot(classifier.previousOutput_ext.T, inputLists[0])
        #)
        #temp2 = theano.function(
        #      inputs=[],
        #      outputs=T.mean(T.log(T.dot(classifier.previousOutput_ext.T, inputLists[0])))
        #)
        attention = theano.function(inputs=[], outputs=reader.attentionSoFar)
    ###############
    # TRAIN MODEL #
    ###############
	print '... training'

	(sentences, chars) = langmod.read_corpus_into_batches_no_counting(corpus, TIME_LENGTH)

        iteration = -1

        for epoch in range(0,8):
   	        for sent in sentences:
		    if len(sent)>=TIME_LENGTH:

                        iteration+=1

			# Build Input Vector Sequence
			for i in range(0,TIME_LENGTH):
			     newInputList = numpy.zeros((NUM_OF_CHARS,1), dtype=theano.config.floatX)
			     newInputList[min(NUM_OF_CHARS-1, sent[i])] = 1
			     inputLists[i].set_value(newInputList)

			# Predict, and calculate error gradients
			cost = train_model()

#TODO previous output becomes new input

                        # NOTE all the things are recomputed for printing here...

                        if iteration%100 == 0:
				print('-- '+str(iteration)+' --')
				print('Input '+str([chars[num] for num in sent]))
				print('Cost '+str(cost))

				print('Total Attention '+str(attention()))

				# Attention scores
				print('Attention '+str([(chars[sent[i]], (reader.attention_scores[i])().item()) for i in range(0,TIME_LENGTH)]))
				# Predicted
				print('Prediction '+str([chars[numpy.argmax(reconstructor.output_ext_funcs[i]())] for i in range(0,TIME_LENGTH)]))
				for i in range(0,TIME_LENGTH):
				     print("  "+chars[min(NUM_OF_CHARS-1, sent[i])]+"  "+chars[min(NUM_OF_CHARS-1, numpy.argmax(reconstructor.output_ext_funcs[i]()))]+"  "+str(reader.attention_scores[i]()))

