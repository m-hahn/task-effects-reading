from shutil import copyfile, copy2

with open("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-sample-dev.txt") as stream:
  fileList = stream.read().rstrip().split("\n")
for filePath in fileList:
  copy2(filePath.replace("_","/"), "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint-sample/training/original/"+filePath)

