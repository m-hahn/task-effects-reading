__docformat__ = 'restructedtext en'


import os
import sys
import timeit

import numpy

import theano
import theano.tensor as T

from theano import config
exception_verbosity='high'
optimizer=None

from theano import *


import langmod


from theano.ifelse import ifelse



def initWeightMatrix(rng, name, columnsNum, rowsNum, rangeOfRand):
        W_values = numpy.asarray(rng.uniform(low=-rangeOfRand, high=rangeOfRand,size=(columnsNum, rowsNum)),dtype=theano.config.floatX)
        #    if activation == theano.tensor.nnet.sigmoid:
        W_values *= 4
        return theano.shared(value=W_values, name=name, borrow=True)



class RecurrentModule(object):

    def __init__(self, rng, externalInput, recurrentInput, n_ex_in, n_rec, n_ex_out, W=None, b=None,
                 activation=T.tanh):

        n_in = n_ex_in + n_rec
        n_out = n_rec + n_ex_out

        [self.W_re, self.W_er, self.W_ee, self.W_rr] = W
        [self.b_e, self.b_r] = b







class RecurrentNetwork(object):

    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, W=None, b=None):

        self.currentOutput_ext = initWeightMatrix(rng, 'out-cur-ext', n_ex_out, 1, 0.24)
        self.currentRecurrentNodes = initialRecurrentState
        self.previousOutput_ext = initWeightMatrix(rng, 'out-prev', n_ex_out, 1, 0.24)


        self.n_ex_in = n_ex_in
        self.n_ex_out = n_ex_out
        self.n_rec = n_rec
        self.time = 0
        self.rng = rng
        n_in = n_ex_in + n_rec
        n_out = n_ex_out + n_rec

        self.W_er = initWeightMatrix(rng,'W_er',n_rec, n_ex_out, numpy.sqrt(6. / 100.)) # bei initWeightMatrix: first rownumber, then colnumber
        self.W_rr = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(6. / 100.))

        self.b_e = initWeightMatrix(rng,'b_e', n_ex_out, 1, 0) #
        self.b_r = initWeightMatrix(rng,'b_r', n_rec, 1, 0) #

        
        self.params = [self.W_er, self.W_rr]


        #temp1 = theano.function(inputs=[],outputs=self.W_re )
        #temp2 = theano.function(inputs=[],outputs= self.currentRecurrentNodes)

        recurrentNodes = T.fmatrix()
        externalInput = T.fmatrix()



################

class RecurrentNetworkReader(RecurrentNetwork):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, defaultInput, b_r, W_er, W_rr):
        super(RecurrentNetworkReader, self).__init__(rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec)

        self.b_r = b_r
        self.W_er = W_er
        self.W_rr = W_rr

        self.totalLikelihoodOfChoices = shared(1)

        self.W_att = initWeightMatrix(rng,'W_att',n_rec, n_ex_in, numpy.sqrt(6. / 100.))
        self.params = [self.W_att]
        self.L1 = (
            abs(self.W_att).sum() +
            abs(self.W_att).sum()
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            (self.W_att ** 2).sum() +
            (self.W_att ** 2).sum()
        )

        self.defaultExternalInput = defaultInput

        # only for debugging/inspecting
        self.attention_scores = []
        self.attentionSoFar = shared(0)

    def addTimeStep(self, externalInput, i): # should change W_att: should separately estimate rewards for viewing and non-viewing. Also, the hidden representation should better be posttrained...?
        attention = T.mean(T.nnet.sigmoid(T.dot(self.currentRecurrentNodes.T, T.dot(self.W_att, externalInput))))
        self.attention_scores.append(theano.function(inputs=[],outputs=attention))

        rollDice = self.rng.uniform(size=(), low=0.0, high=1.0)

        externalInput = ifelse(T.lt(rollDice, attention),  externalInput, self.defaultExternalInput)
        likelihoodOfChoice = ifelse(T.lt(rollDice,attention), attention, 1-attention)

	lin_output_rec = self.b_r + T.dot(self.W_er, externalInput) + T.dot(self.W_rr, self.currentRecurrentNodes)
        recurrentOutput = T.tanh(lin_output_rec)

        likelihoodsOfPreviousChoicesSoFar.append(self.totalLikelihoodOfChoices * likelihoodOfChoice)

        #newReccurentModule = RecurrentModule(self.rng, T.mean(attention) * externalInput, self.currentRecurrentNodes, self.n_ex_in, self.n_rec, self.n_ex_out, W=self.W, b=self.b,activation=T.tanh)

        self.attentionSoFar += attention

        self.currentRecurrentNodes = recurrentOutput
        self.totalLikelihoodOfChoices *= likelihoodOfChoice


def pushToQueue(queueArray, element):
        for i in range(-len(queueArray)+1, 0):
               queueArray[-i].set_value(queueArray[-i-1].get_value())
        queueArray[0].set_value(element)


#def test_rnn(corpus='corpus7.txt',
#             learning_rate=0.001,
#             L1_reg=0.00,
#             L2_reg=0.00001,
#             attention_reg=0.1,
#             n_rec = 10,
#             TIME_LENGTH = 10,
#             NUM_OF_CHARS = 10):

# TODO read in serialized reader and reconstructor


def trainAttentionREINFORCE(corpus='corpus7.txt',
             learning_rate=0.001,
             L1_reg=0.00,
             L2_reg=0.00001,
             attention_reg=0.1,
             n_rec = 10,
             TIME_LENGTH = 10,
             NUM_OF_CHARS = 10, trainedReader=None, trainedReconstructor=None, inputLists=None, inputWordIdentifiers=None):


        assert trainedReader.n_rec == n_rec



	print '... building the model'

	rng = numpy.random.RandomState(1234)
        trng = T.shared_randomstreams.RandomStreams(1234)

        initialRecurrentStateReader =  initWeightMatrix(rng, 'rec', n_rec, 1, 0.24) 

        defaultInput = numpy.zeros((NUM_OF_CHARS,1), dtype=theano.config.floatX)
	defaultInput[NUM_OF_CHARS-1] = 1

	reader = RecurrentNetworkReader(
		rng=rng,
                initialRecurrentState=initialRecurrentStateReader,
		n_ex_in = NUM_OF_CHARS, 
		n_ex_out = NUM_OF_CHARS, 
		n_rec = n_rec,
                defaultInput = shared(defaultInput), b_r = trainedReader.b_r, W_er = trainedReader.W_er, W_rr = trainedReader.W_rr
	)
        

        for i in range(0,TIME_LENGTH):
             reader.addTimeStep(inputLists[i], i)
             

# NOTE will have to replace reader.currentRecurrentNodes in the computational graph for the reconstructor!


        # the overall negative log likelihood assigned by the model to the input (should be small)
        overall_negative_log_likelihood = shared(0)
        for i in range(0, TIME_LENGTH):
              overall_negative_log_likelihood += - T.mean(T.log(T.dot(reconstructor.output_ext[i].T, inputLists[i])))

	final_reward = - (
	    overall_negative_log_likelihood
	    + L1_reg * reader.L1
            + L2_reg * reader.L2_sqr
	)



 
	gparams = [((T.grad(T.log(reader.totalLikelihoodOfChoices) , param) * (-final_reward)) for param in reader.params]


	updates = [
            (param, param - learning_rate * gparam)
	    for param, gparam in zip(reader.params, gparams)
	] #+ [(initialRecurrentStateReader, reader.currentRecurrentNodes)]

# TODO T.grad_clip does the job

	train_model = theano.function(
	    inputs=[],
	    outputs=cost,
	    updates=updates
	)

 
        attention = theano.function(inputs=[], outputs=reader.attentionSoFar)
        likelihoodReaderChoices = theano.function(inputs=[], outputs=reader.totalLikelihoodOfChoices)
	rewardf = theano.function(inputs=[], outputs=reward)
        #inputList = theano.function(inputs=[], outputs=inputLists[0])
    ###############
    # TRAIN MODEL #
    ###############
	print '... training'

	(sentences, chars) = langmod.read_corpus_into_batches_no_counting(corpus, TIME_LENGTH)

        iteration = -1

        for epoch in range(0,8):
   	        for sent in sentences:
		    if len(sent)>=TIME_LENGTH:

                        iteration+=1

			# Build Input Vector Sequence
			for i in range(0,TIME_LENGTH):
                             inputWordIdentifiers[i].set_value(min(NUM_OF_CHARS-1, sent[i]))



			# Predict, and calculate error gradients
			cost = train_model()

#TODO previous output becomes new input

                        # NOTE some of the things are recomputed for printing here...

                        if iteration%100 == 0:
				print('-- '+str(iteration)+' --')
				print('Input '+str([chars[num] for num in sent]))
				print('Cost '+str(cost))
				print('Reward '+str(rewardf()))

				print('Total Attention '+str(attention()))
				print('Likelihood Reader '+str(likelihoodReaderChoices()))

				# Attention scores
				print('Attention '+str([(chars[sent[i]], (reader.attention_scores[i])().item()) for i in range(0,TIME_LENGTH)]))
				# Predicted
				print('Prediction '+str([chars[numpy.argmax(reconstructor.output_ext_funcs[i]())] for i in range(0,TIME_LENGTH)]))
				for i in range(0,TIME_LENGTH):
				     print("  "+chars[min(NUM_OF_CHARS-1, sent[i])]+"  "+chars[min(NUM_OF_CHARS-1, numpy.argmax(reconstructor.output_ext_funcs[i]()))]+"  "+str(reader.attention_scores[i]()))

