

import os
import sys
import timeit

import numpy

import theano
import theano.tensor as T

from theano import config
exception_verbosity='high'
optimizer=None

from theano import *


import langmod


from theano.ifelse import ifelse


import theano
import numpy
from theano import tensor as T

import stochasticReader

rng = numpy.random.RandomState(1234) 
trng = T.shared_randomstreams.RandomStreams(1234)



W = stochasticReader.initWeightMatrix(rng, 'a', 1,1,0.5)+0.5

bvis = stochasticReader.initWeightMatrix(rng, 'a', 1,1,0.5)+0.5
bhid = stochasticReader.initWeightMatrix(rng, 'a', 1,1,0.5)+0.5



def OneStep(vsample) :
    hmean = T.nnet.sigmoid(theano.dot(vsample, W) + bhid)
    hsample = trng.binomial(size=hmean.shape, n=1, p=hmean)
    vmean = T.nnet.sigmoid(theano.dot(hsample, W.T) + bvis)
    return trng.binomial(size=vsample.shape, n=1, p=vmean,
                         dtype=theano.config.floatX)

sample = theano.tensor.vector()

values, updates = theano.scan(OneStep, outputs_info=sample, n_steps=10)

gibbs10 = theano.function([sample], values[-1], updates=updates)
