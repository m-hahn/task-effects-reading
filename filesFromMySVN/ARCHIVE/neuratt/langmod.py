#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle

import codecs

import os

UNIT = 'word'#'word'#'char'#'word' # 'char'

#/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/lstm/lstm/testDir/


# TODO does not do chomp, therefore duplicate entries???

hasDeepmindStructure = 1
 # /disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/num2Chars
# "/disk/scratch2/s1582047/ptb/ptb2.0PureWordsAnonymized"



#  putInNamedEntities("/disk/scratch_ssd/deepmind/rc-data/dailymail/questions/training/","/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/withEntities/raw/")
def putInNamedEntities(directoryIn, directoryOut):
   files = os.listdir(directoryIn)
   numberOfFiles = len(files)
   fileCounter = 0.0
   for fileName in files:
       fileCounter = fileCounter + 1
       if fileCounter % 1000 == 0:
            print(100 * fileCounter/numberOfFiles)
       fIn = open(directoryIn+"/"+fileName, 'r')
       lineCounter = 0
       entities = {}
       text = ""
       for line in fIn:
          line = line.rstrip()
          if len(line) == 0:
              lineCounter = lineCounter + 1
          if lineCounter == 1:
              text = " "+line+" "
          elif lineCounter > 3:
            if len(line) > 0:
              lineList = line.split(":")
              replacer = " "
              if len(lineList) == 2:
                  replacer = " "+lineList[1].lower()+" "
                  text = text.replace(" "+lineList[0]+" ", replacer)
              elif  len(lineList) > 2:
                  uhu = 1
              #    replacer = " "+(" : ".join(lineList[1:]).lower())+" "
              #    print(replacer)
              else:
                  print("ERRROR "+line)

              #otherwise do nothing, then the term is likely to be quite illformed
       fIn.close()
       fOut = open(directoryOut+"/"+fileName, 'w')
       fOut.write("\n\n")
       fOut.write(text.rstrip().lstrip())
       fOut.write("\n\n")
       fOut.close()

# numerify(["/u/nlp/data/deepmind-qa/dailymail/train","/u/nlp/data/deepmind-qa/cnn/train"])

def numerify(directories):
     print("NOTE DOING "+UNIT)

     print("WARNING: GOING TO OVEWRITE DICTIONARY !!!")
     crash()

     dictionaryPath = "/afs/cs.stanford.edu/u/mhahn/num2CharsNoEntities-deepmind-joint" #"/afs/cs.stanford.edu/u/mhahn/num2CharsNoEntities-dailymail" #"/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/lstm/lstm/corpus7/num2Chars" #"/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training3/num2Chars" #gutenberg/num2Chars
     # CREATE COUNTS
     counts = []
     char2Nums = dict()
     ##########
     files = []
     for directory in directories:
        files = files + map(lambda x:directory+"/"+x,os.listdir(directory))
     print("Have listed files.")
     ##########
     counter = 0
     fileCounter = 0.0
     for fileName in files:
         fileCounter = fileCounter + 1.0

         #if fileCounter == 1500:
         #  break
         if fileCounter % 1000 == 0:
             print(" . "+fileName+" "+str(fileCounter / len(files)))
         if fileName == ".svn":
           continue
         f = open(fileName, 'r')      
         lineNumber = 0
         for line in f:
           lineNumber = lineNumber + 1
           if hasDeepmindStructure == 1:
             if lineNumber < 3:
                continue
             if lineNumber > 6:
                break
           #print(line)
           #print("++++++++++++\n")
           #line = unicode(line, "utf-8")

           line = line.rstrip()

           if(UNIT == 'word'):
              line =  line.split(" ")
           for char in line:


              if(not (char in char2Nums)):
                char2Nums[char] = counter
                counts.append([counter,1,char])
                #print(counter)
                #print(char)
                #print(counts[counter])
                #print("..")
                counter = counter + 1
              else:
                 counts[char2Nums[char]][1] = counts[char2Nums[char]][1] + 1



     # NUMBERING
     countsSorted = sorted(counts, key=lambda tup: tup[1], reverse=True)
     
     old2New = [0] * len(countsSorted)
     chars = [0] * len(countsSorted)
     for i in range(0, len(countsSorted)):
           #print(countsSorted[i][0])
           old2New[countsSorted[i][0]] = i
           chars[i] = countsSorted[i][2]

     # PRINT TRANSFORMED FILES

     def getCharNum(char):
        return old2New[char2Nums[char]]
     def charNumExists(char):
        return char in char2Nums
     def unknownWordIndex():
        return len(char2Nums)-1
    
     print("PRINTING DICTIONARY TO PATH "+dictionaryPath)


     dictFile = open(dictionaryPath, 'w')
     for i in range(0, len(chars)):
         dictFile.write(str(i)+" "+chars[i]+"\n")
     dictFile.close()

     transformFilesToNumbers(directories, getCharNum, charNumExists, unknownWordIndex)


def readDictionary(dictLocation):
     char2Nums = {}
     nums2Chars = {}
     dictFile = open(dictLocation, 'r')
     for line in dictFile:
        line = line.rstrip().split(" ")
        if len(line) > 1:
          try:
             nums2Chars[int(line[0])] = line[1]

             if line[1] in char2Nums:
                # Will not use the duplicate value, only the first one that turned up
                print("WARNING (duplicate entry) "+line[0]+"  "+line[1]+"  "+str(char2Nums[line[1]]))
             else:
                char2Nums[line[1]] = int(line[0])
                #print(str(nums2Chars[char2Nums[line[1]]])+"  "+line[1])
  
             #if char2Nums[line[1]] == 33821:
             #    crash()
          except ValueError:
             print("ERROR "+str(line))
             char2Nums[line[1]] = 0
     dictFile.close()
     return char2Nums, nums2Chars


#numerifyFromDict(["/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint-sample/training/original/"], "/afs/cs.stanford.edu/u/mhahn/scr/num2CharsNoEntities-cnn")
#numerifyFromDict(["/afs/cs.stanford.edu/u/mhahn/mhahn_files/ed1516/experiments/corpus/forEvaluation/questions/evaluation/", "/afs/cs.stanford.edu/u/mhahn/num2CharsNoEntities-deepmind-joint"])

# numerifyFromDict(["/u/nlp/data/deepmind-qa/cnn/dev"], "/afs/cs.stanford.edu/u/mhahn/scr/num2CharsNoEntities-cnn")


def numerifyFromDict(directories, dictionaryLocation):
     char2Nums, _ = readDictionary(dictionaryLocation)
     def getCharNum(char):
        return char2Nums[char]
     def charNumExists(char):
        return char in char2Nums
     def unknownWordIndex():
        return len(char2Nums)-1
     transformFilesToNumbers(directories, getCharNum, charNumExists, unknownWordIndex)


def createWordFreqListFromNumerifiedCorpus(directory, dictionaryPath,freqListPath):
     files = os.listdir(directory)
     fileCounter = 0.0
     freqList = {}
     tokenCounter = 0.0
     for fileName in files:
         fileCounter = fileCounter + 1.0
         if fileCounter % 1000 == 0:
             print(" ' "+fileName+" "+str(fileCounter / len(files)))
         f = open(directory+"/"+fileName, 'r')
         for line in f:
              line = line.rstrip()
              if len(line) == 0:
                  continue
              tokenCounter += 1
              if int(line) in freqList:
                  freqList[int(line)] = freqList[int(line)]+1
              else:
                  freqList[int(line)] = 1
         f.close()
     fileOut = open(freqListPath, 'w')
     _, nums2Chars = readDictionary(dictionaryPath)
     for key in freqList:
         fileOut.write(str(nums2Chars[key])+"\t"+str(freqList[key])+"\t"+str(freqList[key]/tokenCounter)+"\n")
     fileOut.close()



def createBigramFreqList(directory, dictionaryPath, freqListPath):
     files = os.listdir(directory)
     fileCounter = 0.0
     freqList = {}
     tokenCounter = 0.0
     for fileName in files:
         #if fileCounter > 50:
         #   break
         fileCounter = fileCounter + 1.0
         if fileCounter % 1000 == 0:
             print(" ' "+fileName+" "+str(fileCounter / len(files)))
             print(len(freqList))
         f = open(directory+"/"+fileName, 'r')
         lastToken = -1
         for line in f:
              line = line.rstrip()
              if len(line) == 0:
                  continue
              newToken = int(line)
              tokenCounter += 1
              if lastToken > -1:
                 if (lastToken,newToken) in freqList:
                   freqList[(lastToken,newToken)] = freqList[(lastToken,newToken)]+1
                 else:
                   freqList[(lastToken,newToken)] = 1
              lastToken = newToken
         f.close()
     fileOut = open(freqListPath, 'w')
     _, nums2Chars = readDictionary(dictionaryPath)
     for key in freqList:
       if freqList[key] > 10:
         if key[0] in nums2Chars and key[1] in nums2Chars:
             #print(str(nums2Chars[key[0]])+"\t"+str(nums2Chars[key[1]])+"\t"+str(freqList[key])+"\t"+str(freqList[key]/tokenCounter))
             fileOut.write(str(nums2Chars[key[0]])+"\t"+str(nums2Chars[key[1]])+"\t"+str(freqList[key])+"\t"+str(freqList[key]/tokenCounter)+"\n")
         else:
             print(key)
     fileOut.close()


def dailyMailToCNN(string):
  return string.replace("’","'")


def transformFilesToNumbers(directories, getCharNum, charNumExists, unknownWordIndex):
     files = {}
     for directory in directories:
        files[directory] = os.listdir(directory)
     if True:
       directoryOutTexts = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/cnn/dev/noentities/texts/" 
       directoryOutAnswers = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/cnn/dev/noentities/answers/"  
       directoryOutQuestions = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/cnn/dev/noentities/questions/" 
     elif False:
       directoryOutTexts = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/texts/noentities/" 
       directoryOutAnswers = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/answers/noentities/"  
       directoryOutQuestions = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/questions/noentities/" 
     elif False:
       directoryOutTexts = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint-sample/training/noentities-cnn/texts/" 
       directoryOutAnswers = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint-sample/training/noentities-cnn/answers/"  
       directoryOutQuestions = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint-sample/training/noentities-cnn/questions/" 
     else:
      assert False
      directoryOutTexts = "/afs/cs.stanford.edu/u/mhahn/mhahn_files/ed1516/experiments/corpus/forEvaluation/questions/numerified-anonymous-joint/texts/" 
      directoryOutAnswers = "/afs/cs.stanford.edu/u/mhahn/mhahn_files/ed1516/experiments/corpus/forEvaluation/questions/numerified-anonymous-joint/answers/"  
      directoryOutQuestions = "/afs/cs.stanford.edu/u/mhahn/mhahn_files/ed1516/experiments/corpus/forEvaluation/questions/numerified-anonymous-joint/questions/" 



#     directoryOutTexts =  "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/dailymail/training/texts/noentities/" #"/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/withEntities/numerified/" #"/disk/scratch2/s1582047/dundeetreebank/parts/PART1Num/" # "/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training3/texts/" # "/disk/scratch2/s1582047/dundee/PART1Num/" #"/disk/scratch2/s1582047/ptb/ptb2.0Num/" #"/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/lstm/lstm/corpus7/texts/" #"/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training3/texts/" #"/disk/scratch2/s1582047/gutenberg/out/"  #

#     directoryOutAnswers = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/dailymail/training/answers/noentities/"#"/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/lstm/lstm/corpus7/answers/" #"/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training3/answers/" #"/disk/scratch2/s1582047/gutenberg/out/"  #

#     directoryOutQuestions = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/dailymail/training/questions/noentities/"#"/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/lstm/lstm/corpus7/questions/" #"/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training3/questions/" #"/disk/scratch2/s1582047/gutenberg/out/"  #

     print("PRINT TRANSFORMED FILES")
     fileCounter = 0.0
     for directory in directories:
       if False:
          directoryPrefix = directory.replace("/","_")+"_" # apply a directory prefix
       else:
          directoryPrefix = ""
       for fileName in files[directory]:
         #print("###### "+fileName)
         fileCounter = fileCounter + 1.0

         if fileCounter % 1000 == 0:
             print(" ' "+directory+"  "+fileName+" "+str(fileCounter / len(files)))
         if fileName == ".svn":
            continue
         f = open(directory+"/"+fileName, 'r')
         fOutText = open(directoryOutTexts+"/"+directoryPrefix+fileName, 'w')
         if hasDeepmindStructure == 1:
             fOutAnswer = open(directoryOutAnswers+"/"+directoryPrefix+fileName, 'w')
             fOutQuestion = open(directoryOutQuestions+"/"+directoryPrefix+fileName, 'w')
         lineNumber = 0
         for line in f:
           line = dailyMailToCNN(line)
           lineNumber = lineNumber + 1
           if hasDeepmindStructure == 1:
             if lineNumber < 3: #3 = text, 5 = question
                continue
             if lineNumber > 7:
                break
           #print(lineNumber)
           #print(line)
           #print("++++++++++++\n")
           #line = unicode(line, "utf-8")
           line = line.rstrip()
           if(UNIT == 'word'):
              line =  line.split(" ")
           for char in line:

              # chomp
              char = char.rstrip() #should be redundant to the one a bit further up

              if len(char) == 0:
                 continue
              #print(char+"  "+str(old2New[char2Nums[char]]))



              num = -1
              if(False and (char == "@placeholder")):
                  num = -1
              elif(not charNumExists(char)):
                  print("WARNING (entry doesn't exist) #"+char+"# in file "+directoryPrefix+"_"+fileName+"\n")
                  num = unknownWordIndex()
              else:
                  num = getCharNum(char)


              if hasDeepmindStructure == 0:
                 fOutText.write(str(num)+"\n")
              else:
                 if lineNumber == 3:
                    fOutText.write(str(num)+"\n")
                 elif lineNumber == 5:
                    fOutQuestion.write(str(num)+"\n")
                 elif lineNumber == 7:
                    fOutAnswer.write(str(num)+"\n")
              #print((char+"  "+chars[old2New[char2Nums[char]]]))
         if hasDeepmindStructure == 1:
            fOutAnswer.close()
            fOutQuestion.close()

         fOutText.close()
   
      
     # PRINT DICTIONARY



     #print(counts)
     #print(char2Nums)
     #print(countsSorted)
     #print(old2New)


def printCorpusToListOfNumbers(corpus_name):
     (corpus,_) = read_corpus_no_counting(corpus_name)
     f = open(corpus_name+'.num', 'w')
     for num in corpus:
        f.write(str(num)+"\n")
     f.close()


def printCorpusInBatchesToListOfNumbers(corpus_name):
     (corpus,chars) = read_corpus_into_batches_no_counting(corpus_name, 100)
     f = open(corpus_name+'.num.b', 'w')
     for batch in corpus:
        for num in batch:
           f.write(str(num)+" ")
        f.write("\n")
     f.close()

     fOut = codecs.open(corpus_name+'.charnums.txt', "w", "utf-8")
     for i in range(0,len(chars)):
          #print(chars[i]+"\n")
          fOut.write(chars[i]+"\n")

     fOut.close()





def read_corpus_no_counting(corpus_name):
     (char2nums, chars, counts) = retrieve_word_freq_list(corpus_name)
     f = open(corpus_name, 'r')
     corpus = []
     print f
     for line in f:
         line = unicode(line, "utf-8")
         if(UNIT == 'word'):
            line =  line.split(" ")
         for char in line:
            corpus.append(char2nums[char])
            #print(char+"  "+chars[char2nums[char]]+"\n")
     f.close()
     
     return (corpus, chars)


def read_corpus_into_batches_no_counting(corpus_name, BATCH_LENGTH):
     (corpus, chars) = read_corpus_no_counting(corpus_name)
     sentences = []
     current_sentence = []
     i = 0
     for token in corpus:
          i+=1
          current_sentence.append(token)
          #print(chars[token])
          if i == BATCH_LENGTH: #chars[token] == ' ':
               i = 0
               #print(current_sentence)
               #print("---")
               sentences.append(current_sentence)
               current_sentence = []
     sentences.append(current_sentence)
     return (sentences, chars)



# RETURN: char2Nums, chars, counts (where the second component in each element can be ignored)
def store_word_freq_list(corpus_name):
     char2nums = dict()
     chars = []
     counts = []
     f = open(corpus_name, 'r')#
     #print(1)
     #f = open('24264-0.txt','r')
     counter = 0
     #corpus = []
     print f
     for line in f:
         line = unicode(line, "utf-8")
         #print line
         if(UNIT == 'word'):
            line = line.split(" ")
         #print(line)
         for char in line:
            #print(char)
            if(not (char in char2nums)):
                char2nums[char] = counter
                counts.append([0,counter])
                counter+=1
                chars.append(char)
            counts[char2nums[char]][0]+=1
     countsSorted = sorted(counts, key=lambda tup: tup[0], reverse=True)
     #print chars

     #maxCount = countsSorted[NUM_OF_CHARS][0]

     # old to new numbers
     newNumbers = [0]*len(chars)
     newChars = [0]*len(chars)
     newChar2Nums = dict()
     for i in range(0,len(countsSorted)):
          newNumbers[countsSorted[i][1]] = i
          newChars[i] = chars[countsSorted[i][1]]
          newChar2Nums[newChars[i]] = i


     f.close()
     #print maxCount

     f = open(corpus_name+'-char2Nums-'+UNIT, 'w')
     pickle.dump(newChar2Nums, f)
     f.close()

     f = open(corpus_name+'-chars-'+UNIT, 'w')
     pickle.dump(newChars, f)
     f.close()

     f = open(corpus_name+'-counts-'+UNIT, 'w')
     pickle.dump(countsSorted, f)
     f.close()

     #return (newChar2Nums, newChars, countsSorted)
        

def retrieve_word_freq_list(corpus_name):
     f = open(corpus_name+'-char2Nums-'+UNIT, 'r')
     char2Nums = pickle.load(f)
     f.close()

     f = open(corpus_name+'-chars-'+UNIT, 'r')
     chars = pickle.load(f)
     f.close()

     f = open(corpus_name+'-counts-'+UNIT, 'r')
     counts = pickle.load(f)
     f.close()

     return (char2Nums, chars, counts)


#class CorpusBatchReader(object):
#     def __init__(self, corpus_name):
#         (self.char2nums, self.chars, self.counts) = retrieve_word_freq_list(corpus_name)
#         self.f = open(corpus_name, 'r')
#         #self.buffer = unicode(self.f.readLine(), "utf-8")
#         self.lines = self.f.xreadlines()##

#     def next_batch(self,BATCH_LENGTH):
#         current_sentence = []
#         i = 0
#         while i < BATCH_LENGTH:
#            line = unicode(self.lines.next(), "utf-8")
#            for char in line:
#                current_sentence.append(self.char2nums[char])
#                i+=1
#                if(i == BATCH_LENGTH):
#                    return current_sentence
#            if(i < BATCH_LENGTH):
#                self.buffer = unicode(f.readLine(), "utf-8")
#                if(self.buffer == ''):
#                    return current_sentence

#         print(current_sentence)
#         print("---")
#         return current_sentence
 
#     def has_next(self):
#         return self.buffer != ''




def read_corpus(corpus_name):
     char2nums = dict()
     chars = []
     counts = []
     f = open(corpus_name, 'r')#
     #f = open('24264-0.txt','r')
     counter = 0
     corpus = []
     print f
     for line in f:
         line = unicode(line, "utf-8")
         #print line
         if(UNIT == 'word'):
            line = line.split(" ")
         for char in line:
            if(not (char in char2nums)):
                char2nums[char] = counter
                counts.append([0,counter])
                counter+=1
                chars.append(char)
            counts[char2nums[char]][0]+=1
            corpus.append(char2nums[char])
     countsSorted = sorted(counts, key=lambda tup: tup[0], reverse=True)
     #print chars

     maxCount = countsSorted[NUM_OF_CHARS][0]

     # old to new numbers
     newNumbers = [0]*len(chars)
     newChars = [0]*len(chars)
     for i in range(0,len(countsSorted)):
          newNumbers[countsSorted[i][1]] = i
          newChars[i] = chars[countsSorted[i][1]]

     corpusRenumbered = []
     for num in corpus:
          corpusRenumbered.append(newNumbers[num])
     

     f.close()
     print maxCount
     #print corpus
     #exitsdg()
     
     return (corpusRenumbered, newChars, maxCount)

def read_corpus_into_batches(corpus_name, BATCH_LENGTH):
     (corpus, chars, maxCount) = read_corpus(corpus_name)
     sentences = []
     current_sentence = []
     i = 0
     for token in corpus:
          i+=1
          current_sentence.append(token)
          #print(chars[token])
          if i == BATCH_LENGTH: #chars[token] == ' ':
               i = 0
               print(current_sentence)
               print("---")
               sentences.append(current_sentence)
               current_sentence = []
     sentences.append(current_sentence)
     return (sentences, corpus, chars, maxCount)








