__docformat__ = 'restructedtext en'


import os
import sys
import timeit

import numpy

import theano
import theano.tensor as T

from theano import config
exception_verbosity='high'
optimizer=None

from theano import *


import langmod


from theano.ifelse import ifelse

from theano.tensor.shared_randomstreams import RandomStreams

SOFT_ATTENTION ='n'
global_list = []
LEARNER='REINFORCE'
ATTENTION_COMP = 'neural'#'linear-form'#'neural'
INPUT='embedding'#'one-hot'#'embedding' # 'one-hot'

def initWeightMatrix(rng, name, columnsNum, rowsNum, rangeOfRand):
        W_values = numpy.asarray(rng.uniform(low=-rangeOfRand, high=rangeOfRand,size=(columnsNum, rowsNum)),dtype=theano.config.floatX)
        #    if activation == theano.tensor.nnet.sigmoid:
        W_values *= 4
        return theano.shared(value=W_values, name=name, borrow=True)



class RecurrentModule(object):

    def __init__(self, rng, externalInput, recurrentInput, n_ex_in, n_rec, n_ex_out, W=None, b=None,
                 activation=T.tanh):

        n_in = n_ex_in + n_rec
        n_out = n_rec + n_ex_out

        [self.W_re, self.W_er, self.W_ee, self.W_rr] = W
        [self.b_e, self.b_r] = b





class LSTMNetworkParameters(object):
    def __init__(self, rng, n_ex_in, n_ex_out, n_rec):

        self.n_ex_in = n_ex_in
        self.n_ex_out = n_ex_out
        self.n_rec = n_rec

        n_in = n_ex_in + n_rec
        n_out = n_ex_out + n_rec

        self.W_hi = initWeightMatrix(rng,'W_hi',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))

        self.W_ci = initWeightMatrix(rng,'W_ci',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec))) # bei initWeightMatrix: first rownumber, then colnumber



        self.W_hf = initWeightMatrix(rng,'W_hf',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec))) # bei initWeightMatrix: first rownumber, then colnumber

        self.W_cf = initWeightMatrix(rng,'W_cf',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))



        self.W_hc = initWeightMatrix(rng,'W_hc',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))

       


        self.W_ho = initWeightMatrix(rng,'W_ho',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_ex_in))) # bei initWeightMatrix: first rownumber, then colnumber

        self.W_co = initWeightMatrix(rng,'W_co',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))

        self.b_i = initWeightMatrix(rng,'b_i', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out))) #
        self.b_f = initWeightMatrix(rng,'b_f', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out))) #
	self.b_c = initWeightMatrix(rng,'b_c', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out))) #
	self.b_o = initWeightMatrix(rng,'b_o', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out)))
        
        self.params = [self.W_hi, self.W_ci, self.W_hf, self.W_cf, self.W_hc, self.W_ho, self.W_co, self.b_i, self.b_f, self.b_c, self.b_o]





class LSTMNetworkParametersReconstructor(LSTMNetworkParameters):
    def __init__(self, rng, n_ex_in, n_ex_out, n_rec):
         LSTMNetworkParameters.__init__(self, rng, n_ex_in, n_ex_out, n_rec)
         self.W_ce = initWeightMatrix(rng,'W_ce',n_ex_out, n_rec, numpy.sqrt(0.1 / (n_ex_out + n_rec)))
         self.params += [self.W_ce]


class RecurrentNetworkReconstructorLSTM(object):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, parameterSetting, inputSentence):
        initialMemoryCellVector = initWeightMatrix(rng,'W_er',n_rec, 1, 0.0)

        self.p = parameterSetting
        if self.p == None:
             self.p = LSTMNetworkParametersReconstructor(rng, n_ex_in, n_ex_out, n_rec)


        recurrentNodes = T.fmatrix()
        externalInput = T.fmatrix()


        self.L1 = (
            shared(numpy.cast['float32'](0.0))
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            shared(numpy.cast['float32'](0.0))
        )

        

	def recurrenceReconstruct(externalInput, currentHiddenVector, memoryCellVector):
		hiddenPrevious = currentHiddenVector
		cellPrevious = memoryCellVector

		if hasattr(self.p, 'W_xi'):
		    print('Node feeding back input 224')
		    inputCurrent = T.nnet.sigmoid(T.dot(self.p.W_xi, externalInput) + T.dot(self.p.W_hi, hiddenPrevious) + T.dot(self.p.W_ci, cellPrevious) + self.p.b_i)
		    forgetCurrent = T.nnet.sigmoid(T.dot(self.p.W_xf, externalInput) + T.dot(self.p.W_hf, hiddenPrevious) + T.dot(self.p.W_cf, cellPrevious) + self.p.b_f)
		    cellCurrent = forgetCurrent * cellPrevious + inputCurrent * T.tanh(T.dot(self.p.W_xc, externalInput) + T.dot(self.p.W_hc, hiddenPrevious) + self.p.b_c)
		    outputCurrent = T.nnet.sigmoid(T.dot(self.p.W_xo, externalInput) + T.dot(self.p.W_ho, hiddenPrevious) + T.dot(self.p.W_co, cellCurrent) + self.p.b_o)
		    hiddenCurrent = outputCurrent * T.tanh(cellCurrent)
		else:
		    inputCurrent = T.nnet.sigmoid(T.dot(self.p.W_hi, hiddenPrevious) + T.dot(self.p.W_ci, cellPrevious) + self.p.b_i)
		    forgetCurrent = T.nnet.sigmoid(T.dot(self.p.W_hf, hiddenPrevious) + T.dot(self.p.W_cf, cellPrevious) + self.p.b_f)
		    cellCurrent = forgetCurrent * cellPrevious + inputCurrent * T.tanh(T.dot(self.p.W_hc, hiddenPrevious) + self.p.b_c)
		    outputCurrent = T.nnet.sigmoid(T.dot(self.p.W_ho, hiddenPrevious) + T.dot(self.p.W_co, cellCurrent) + self.p.b_o)
		    hiddenCurrent = outputCurrent * T.tanh(cellCurrent)

		currentHiddenVector = hiddenCurrent
		memoryCellVector = cellCurrent


		# EXTERNAL OUTPUT
		externalOutput = T.nnet.softmax((T.dot(self.p.W_ce, cellCurrent)).T).T

		return [currentHiddenVector, memoryCellVector, externalOutput, - T.sum(T.log(T.dot(externalOutput.T, externalInput)))]

        [self.output_ext, _, _, negativeLogLikelihoods], _ = theano.scan(fn=recurrenceReconstruct, sequences = inputSentence, outputs_info=[initialRecurrentState, initialMemoryCellVector, None, None], non_sequences=[], n_steps=inputSentence.shape[0])


	self.overall_negative_log_likelihood = T.sum(negativeLogLikelihoods)

################

class LSTMNetworkParametersReader(LSTMNetworkParameters):
    def __init__(self, rng, n_ex_in, n_ex_out, n_rec, USE_ATTENTION, N_EMB):
         if INPUT == 'one-hot':
             n_emb = n_ex_in
         else:
             n_emb = N_EMB

         LSTMNetworkParameters.__init__(self, rng, n_emb, n_ex_out, n_rec)

         if INPUT == 'embedding':
             self.W_emb = initWeightMatrix(rng,'W_emb',n_emb, n_ex_in, numpy.sqrt(0.1 / (n_emb + n_ex_in)))
             self.params += [self.W_emb]

         self.W_xf = initWeightMatrix(rng,'W_xf',n_rec, n_emb, numpy.sqrt(0.1 / (n_rec + n_emb)))
         self.W_xi = initWeightMatrix(rng,'W_xi',n_rec, n_emb, numpy.sqrt(0.1 / (n_rec + n_emb))) # bei initWeightMatrix: first rownumber, then colnumber
         self.W_xc = initWeightMatrix(rng,'W_xc',n_rec, n_emb, numpy.sqrt(0.1 / (n_rec + n_emb))) # bei initWeightMatrix: first rownumber, then colnumber
         self.W_xo = initWeightMatrix(rng,'W_xo',n_rec, n_emb, numpy.sqrt(0.1 / (n_rec + n_rec)))
         self.params += [self.W_xf, self.W_xi, self.W_xc, self.W_xo]
	 if USE_ATTENTION == 'y':
             print('ERROR 255: Was not expecting USE_ATTENTION == \'y\' in the initializer')
             self.createAttentionParameters(rng, n_rec, n_emb, 0, n_emb)
         else:
             self.params += []
             self.W_att = None
         self.initialRecurrentState =  initWeightMatrix(rng, 'rec', n_rec, 1, 0.24) # TODO should be a model parameter
         self.initialMemoryCellVector = initWeightMatrix(rng,'W_er',n_rec, 1, 0.0)
         #self.params += [self.initialRecurrentState]
 


    def createAttentionParametersAndRemoveOthersFromParams(self, rng, n_rec, n_ex_in, N_EMB):
         self.createAttentionParameters(rng, n_rec, n_emb,1)

    def createAttentionParameters(self, rng, n_rec, n_ex_in,removeOtherParams, N_EMB):
         if INPUT == 'one-hot':
             n_emb = n_ex_in
         else:
             n_emb = N_EMB
         if hasattr(self, 'W_att') and self.W_att != None:
             print('Warning: Attention network exists already, keeping it!')
         else:
             print('Create attention network')
             if removeOtherParams == 1:
                 self.params=[]
             if ATTENTION_COMP == 'linear-form':
		     self.W_att = initWeightMatrix(rng,'W_att',n_rec, n_emb, numpy.sqrt(6. / (n_rec + n_emb)))
		     self.params += [self.W_att]
             elif ATTENTION_COMP == 'neural':
                     n_attention_hidden = 100
                     self.W_att = 0
                     self.W_att_rh = initWeightMatrix(rng,'W_att_rh',n_attention_hidden, n_rec, numpy.sqrt(1. / (n_attention_hidden + n_rec)))
                     self.W_att_ih = initWeightMatrix(rng, 'W_att_ih',n_attention_hidden, n_emb, numpy.sqrt(1. / (n_attention_hidden + n_emb)))
                     self.W_att_ho = initWeightMatrix(rng, 'W_att_ho',1, n_attention_hidden, numpy.sqrt(1. / (n_attention_hidden + 1)))
                     self.params += [self.W_att_rh, self.W_att_ih, self.W_att_ho]
	     else:
                     print('ERROR 289')

class RecurrentNetworkReader():
    def __init__(self, rng, trng, n_ex_in, n_ex_out, n_rec, USE_ATTENTION, defaultInput, general_attention, parameterSetting, n_emb, inputSentence):
        if hasattr(parameterSetting, 'initialRecurrentState'):
             currentHiddenVector = parameterSetting.initialRecurrentState
             memoryCellVector = parameterSetting.initialMemoryCellVector
        else:
             currentHiddenVector =  initWeightMatrix(rng, 'rec', n_rec, 1, 0.24)
             memoryCellVector = initWeightMatrix(rng,'W_er',n_rec, 1, 0.0)
             print('Warning: Randomly initializing hidden vectors')

        self.p = parameterSetting
        if self.p == None:
             self.p = LSTMNetworkParametersReader(rng, n_ex_in, n_ex_out, n_rec, USE_ATTENTION, n_emb)


        self.USE_ATTENTION = USE_ATTENTION

        self.L1 = (
            0
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            0
        )


        defaultExternalInput = defaultInput
        if INPUT == 'embedding':
            defaultExternalInput =  T.dot(self.p.W_emb, defaultInput)

	def recurrenceRead(rawExternalInput, currentHiddenVector, memoryCellVector):
                rawExternalInput = rawExternalInput.dimshuffle(('x', 0))
		if INPUT == 'embedding':
		     rawExternalInput = T.dot(self.p.W_emb, rawExternalInput)

		attention = shared(numpy.cast['float32'](-1.0))
		hasRead = shared(numpy.cast['float32'](-1.0))
		probabilityOfChoice = shared(numpy.cast['float32'](-1.0))

		if self.USE_ATTENTION == 'y':
		      if SOFT_ATTENTION=='y':
			    attention = self.computeAttention(rawExternalInput, currentHiddenVector)
			    externalInput = rawExternalInput  * attention
		      elif LEARNER=='REINFORCE':
			    rollDice = trng.uniform(size=(), low=0.0, high=1.0) 
			    attention = self.computeAttention(rawExternalInput, currentHiddenVector)
			    hasRead = ifelse(T.lt(rollDice, attention), 1.0, 0.0)
			    externalInput = ifelse(T.lt(rollDice, attention),  rawExternalInput, defaultExternalInput)
			    probabilityOfChoice = ifelse(T.lt(rollDice, attention), attention, 1-attention)
		else:
		 	    rollDice = trng.uniform(size=(), low=0.0, high=1.0)
			    attention = general_attention
			    externalInput = rawExternalInput#ifelse(T.lt(rollDice, attention),  rawExternalInput, defaultExternalInput)
			    hasRead = ifelse(T.lt(rollDice, attention), 1.0, 0.0)


		hiddenPrevious = currentHiddenVector
		cellPrevious = memoryCellVector

		inputCurrent = T.nnet.sigmoid(T.dot(self.p.W_xi, externalInput) + T.dot(self.p.W_hi, hiddenPrevious) + T.dot(self.p.W_ci, cellPrevious) + self.p.b_i)
		forgetCurrent = T.nnet.sigmoid(T.dot(self.p.W_xf, externalInput) + T.dot(self.p.W_hf, hiddenPrevious) + T.dot(self.p.W_cf, cellPrevious) + self.p.b_f)
		cellCurrent = forgetCurrent * cellPrevious + inputCurrent * T.tanh(T.dot(self.p.W_xc, externalInput) + T.dot(self.p.W_hc, hiddenPrevious) + self.p.b_c)
		outputCurrent = T.nnet.sigmoid(T.dot(self.p.W_xo, externalInput) + T.dot(self.p.W_ho, hiddenPrevious) + T.dot(self.p.W_co, cellCurrent) + self.p.b_o)
		hiddenCurrent = outputCurrent * T.tanh(cellCurrent)

		currentHiddenVector = hiddenCurrent
		memoryCellVector = cellCurrent

		return [currentHiddenVector, memoryCellVector, attention, hasRead, probabilityOfChoice]

        [recurrent, memoryCell, self.attentions, self.hasRead, probabilities], _ = theano.scan(fn=recurrenceRead, sequences = inputSentence, outputs_info=[self.p.initialRecurrentState, self.p.initialMemoryCellVector, None, None, None], non_sequences=[], n_steps=inputSentence.shape[0])
 	self.currentHiddenVector = recurrent[-1]
        self.attentionSoFar = T.sum(self.attentions)
        self.probabilityOfChoices = T.prod(probabilities)



    def computeAttention(self, rawExternalInput, currentHiddenVector):
        if ATTENTION_COMP == 'linear-form':
             return T.sum(T.nnet.sigmoid(T.dot(currentHiddenVector.T, T.dot(self.p.W_att, rawExternalInput))))
        elif ATTENTION_COMP == 'neural':
             return T.sum(T.nnet.sigmoid(T.dot(self.p.W_att_ho, T.tanh(T.dot(self.p.W_att_rh, currentHiddenVector) + T.dot(self.p.W_att_ih, rawExternalInput)))))


    def computeAntiAttention(self, rawExternalInput):
        return T.sum(T.nnet.sigmoid(T.dot(self.currentHiddenVector.T, T.dot(self.p.W_noatt, rawExternalInput))))



def resume_denoising_lstm(hashForExperimentNew, hashForExperimentOld, general_attention = None):
        print('Resuming '+hashForExperimentOld)
        model = loadConfiguration(hashForExperimentOld)
        print('   from iteration '+str(model['iteration']))
        setup = model['setup']
        setup['hashForExperiment'] = hashForExperimentNew
        pReader = model['reader.p']
        pReconstructor = model['reconstructor.p']
        print(setup)

        if('general_attention' in model and general_attention == None):
               general_attention = model['general_attention']
        elif(general_attention == None):
               general_attention = shared(numpy.cast['float32'](0.98))
        else:
               general_attention = general_attention 
        

        if(not ('N_EMB' in model)):
            model['N_EMB'] = 50

        train_rnn(setup, pReader, pReconstructor, general_attention)

def resume_for_attention(hashForExperimentNew, hashForExperimentOld, learning_rate = 0.01, attention_reg = 0.1, EPOCHS_NUM = 10, ONLY_TRAIN_ATT = 'y'):
        print('Starting to train attention for '+hashForExperimentOld)
        model = loadConfiguration(hashForExperimentOld)
        print('   from iteration '+str(model['iteration']))
        setup = model['setup']
        setup['hashForExperiment'] = hashForExperimentNew
        pReader = model['reader.p']
        pReconstructor = model['reconstructor.p']
        print(setup)



        if('general_attention' in model):
               general_attention = model['general_attention']
               if(str(type(general_attention)) == '<type \'float\'>'):
                     general_attention = shared(general_attention)
	else:
               print('Note: no shared attention found')
               general_attention = shared(numpy.cast['float32'](0.98))


        
        if(not ('N_EMB' in model)):
            model['N_EMB'] = 50

	print('Note: Resetting some values...')
        setup['attention_reg'] = attention_reg
        setup['learning_rate'] = learning_rate
        setup['EPOCHS_NUM'] = EPOCHS_NUM

	# ADD NEW VALUES
        setup['LEARNER'] = LEARNER


        setup['USE_ATTENTION'] = 'y'


        removeOtherParams = 1
        if ONLY_TRAIN_ATT == 'n':
            removeOtherParams = 0

        pReader.createAttentionParameters(numpy.random.RandomState(1234), setup['n_rec'], setup['NUM_OF_CHARS'], removeOtherParams, setup['N_EMB']) 


        train_attention(setup, pReader, pReconstructor, general_attention)


def initialOutput(p, methodName):
        print(__file__)
        print(methodName)
        print('Hash: '+p['hashForExperiment'])
        print('Corpus: '+p['corpus'])
        print('Learning Rate: '+str(p['learning_rate']))
        print('L1: '+str(p['L1_reg']))
        print('L2 '+str(p['L2_reg']))
        print('AttentionReg: '+str(p['attention_reg']))
        print('Recurrent Units: '+str(p['n_rec']))
        print('Batch size: '+str(p['TIME_LENGTH']))
        print('Alphabet size: '+str(p['NUM_OF_CHARS']))
        print('Number of epochs: '+str(p['EPOCHS_NUM']))
        print('Use Attention: '+p['USE_ATTENTION']) #'y', 'n'
        print('Momentum: '+str(p['momentum']))
        print('Reconstructor LSTM? '+str(p['RECONSTRUCTOR_LSTM']))
        print('Embedding Dimension '+str(p['N_EMB']))

def generateDefaultInput(num_of_chars):
       defaultInput = numpy.zeros((num_of_chars,1), dtype=theano.config.floatX)
       defaultInput[num_of_chars-1] = 1
       return defaultInput

def train_attention(p, pReader, pReconstructor, general_attention):
 

        initialOutput(p, 'train_attention')
        setup = p
        setup['__file__'] = __file__


	print '... building the model'
        

	rng = numpy.random.RandomState(1234)

        defaultInput = generateDefaultInput(p['NUM_OF_CHARS'])

        trng = T.shared_randomstreams.RandomStreams(1234)

	reader = RecurrentNetworkReader(
		rng=rng,trng = trng,
		n_ex_in = p['NUM_OF_CHARS'], 
		n_ex_out = p['NUM_OF_CHARS'], 
		n_rec = p['n_rec'], USE_ATTENTION = p['USE_ATTENTION'], defaultInput = shared(defaultInput), general_attention = general_attention, parameterSetting = pReader, n_emb = p['N_EMB'], inputSentence = inputVectorSequence
	)
        
        # the input list variables
        inputLists = []
        #noiseVariables = []
        SOFInputList = numpy.zeros((p['NUM_OF_CHARS'],1), dtype=theano.config.floatX)
	SOFInputList[p['NUM_OF_CHARS']-1] = 1
        for i in range(0,p['TIME_LENGTH']):
             newInputList = shared(SOFInputList, name='inp '+str(i)) 
             #newNoiseVariable = shared(numpy.cast['float32'](0.0))
             reader.addTimeStep(newInputList)
             inputLists.append(newInputList)
             #noiseVariables.append(newNoiseVariable)
	     
 
	if(p['RECONSTRUCTOR_LSTM'] == 'y'):
		reconstructor = RecurrentNetworkReconstructorLSTM(
			rng=rng,
			initialRecurrentState=reader.currentHiddenVector,
			n_ex_in = p['NUM_OF_CHARS'], 
			n_ex_out = p['NUM_OF_CHARS'], 
			n_rec = p['n_rec'],parameterSetting = pReconstructor
		)
	else:
		reconstructor = RecurrentNetworkReconstructor(
			rng=rng,
			initialRecurrentState=reader.currentHiddenVector,
			n_ex_in = p['NUM_OF_CHARS'], 
			n_ex_out = p['NUM_OF_CHARS'], 
			n_rec = p['n_rec'],parameterSetting=pReconstructor
		)
	reconstructor.addTimeStep(shared(defaultInput))
	for i in range(0,p['TIME_LENGTH']):
	     #reconstructor.addTimeStep(inputLists[i])
	     reconstructor.addTimeStep(shared(defaultInput))
	#'READONCE'

        if LEARNER == 'TD':
              trainLSTMAttentionTD(setup, reader, reconstructor, inputLists, general_attention)
        else:
              trainLSTMAttention(setup, reader, reconstructor, inputLists, general_attention)







def trainLSTMAttention(setup, reader, reconstructor, inputLists, general_attention):
	overall_negative_log_likelihood = shared(numpy.cast['float32'](0.0))
	for i in range(0, setup['TIME_LENGTH']):
	      overall_negative_log_likelihood += - T.sum(T.log(T.dot(reconstructor.output_ext[i].T, inputLists[i])))
        # TODO indices are wrong, so no training for first symbol??? HAE why should this be a problem? we simply disregard the last output, as it comes from the state where even the last word has been shown to the reconstructor
		
        TIME_LENGTH = setup['TIME_LENGTH']
        NUM_OF_CHARS = setup['NUM_OF_CHARS']

        regularizer =  setup['L1_reg'] * reader.L1 + setup['L1_reg'] * reconstructor.L1 + setup['L2_reg'] * reader.L2_sqr  + setup['L2_reg'] * reconstructor.L2_sqr

        reward = overall_negative_log_likelihood + setup['attention_reg'] * reader.attentionSoFar

	cost = (
	    overall_negative_log_likelihood
	    + regularizer
            + setup['attention_reg'] * reader.attentionSoFar
	)

        params = reader.p.params

        if(not 'reinforce_momentum' in setup):
             setup['reinforce_momentum'] = 0.9

        moving_average = shared(numpy.cast['float32'](0.0))

        updates = []
        ## Just gradient descent on cost
        for param in params:
                print(param)
                print(setup['learning_rate'])
		param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)


		updates.append((param, param - setup['learning_rate']*param_update))
                if SOFT_ATTENTION == 'y':
		     updates.append((param_update, setup['reinforce_momentum']*param_update + (1. - setup['reinforce_momentum'])*T.grad(theano.gradient.grad_clip(cost, -1, 1), param)))
        	else:
                     updates.append((param_update, setup['reinforce_momentum']*param_update + (1. - setup['reinforce_momentum'])*(reward-moving_average)*T.grad(theano.gradient.grad_clip(T.log(reader.probabilityOfChoicesSoFar), -1, 1), param))) # hullu



        updates.append((moving_average, shared(numpy.cast['float32'](0.9)) * moving_average + shared(numpy.cast['float32'](0.1)) * reward))

        #print(updates)

        do_the_training(setup, reader, reconstructor, inputLists, general_attention, updates, cost, regularizer, overall_negative_log_likelihood)


def do_the_training(setup, reader, reconstructor, general_attention, updates, cost, regularizer, overall_negative_log_likelihood, idxs):
        plotLogLikelihood = open(setup['hashForExperiment']+'-logl.dat', 'w',0)
        #try:
        do_the_training_inner(setup, reader, reconstructor, general_attention, updates, cost, regularizer, overall_negative_log_likelihood, plotLogLikelihood, idxs)
        #except:
        #     1 ==1

        plotLogLikelihood.close()


def do_the_training_inner(setup, reader, reconstructor, general_attention, updates, cost, regularizer, overall_negative_log_likelihood, plotLogLikelihood, idxs):

        TIME_LENGTH = setup['TIME_LENGTH']
        NUM_OF_CHARS = setup['NUM_OF_CHARS']

	train_model = theano.function(
	    inputs=[idxs],
	    outputs=Out(theano.sandbox.cuda.basic_ops.gpu_from_host(cost)),
	    updates=updates,
            #mode='ProfileMode'
	    #givens=givens
	)

        

        regularizerf = theano.function(inputs=[], outputs=regularizer)
        attention = theano.function(inputs=[idxs], outputs=reader.attentionSoFar)
        costf = theano.function(inputs=[idxs], outputs=cost)
        loglikelihoodf = theano.function(inputs=[idxs], outputs=overall_negative_log_likelihood)

        for_output = theano.function(inputs=[idxs], outputs=(reconstructor.output_ext + reader.attention_scores))

    ###############
    # TRAIN MODEL #
    ###############
	print '... training'
 

	(sentences, chars) = langmod.read_corpus_into_batches_no_counting(setup['corpus'], TIME_LENGTH)

        iteration = -1

        import time
        current = time.time()

 


        print(setup)

        for epoch in range(0,setup['EPOCHS_NUM']):
   	        for sent in sentences:
		    if len(sent)>=TIME_LENGTH:
                        if((iteration+5)%50000 == 0):
                              general_attention.set_value(numpy.cast['float32'](0.6 + 0.9*(general_attention.get_value()-0.6)))
                              print('NEW ATTENTION '+str(general_attention.get_value()))

                        if((iteration+5)%1000 == 0):
                              safeConfiguration(setup['hashForExperiment'], {'iteration':iteration, 'setup' : setup, 'reader.p' : reader.p, 'reconstructor.p' : reconstructor.p, 'general_attention' : general_attention})



                        iteration+=1

#TODO previous output becomes new input

                        if iteration%100 == 0:
                                if setup['USE_ATTENTION'] == 'n' and 'SOFT_ATTENTION' == 'y':
                                    print('Attention '+str(general_attention.get_value()))
                                else:
                                    print('Attention '+str(attention(sent)))

                                print((time.time()-current))
                                current = time.time()

				print('-- '+str(iteration)+' --')
				print('Input '+str([chars[num] for num in sent]))
				print('Cost '+str(costf(sent)))
                                print(str(regularizerf(sent))+" "+str(attention(sent)))

                                plotLogLikelihood.write(str(iteration)+'\t'+str(loglikelihoodf(sent))+'\n')

                                resultsForOutput = for_output(sent)
                             
			        [outputLayers, attentionScores] = numpy.split(resultsForOutput, [TIME_LENGTH+1])   #[range(NUM_OF_CHARS,]


				for i in range(0,TIME_LENGTH):
                                     predicted = numpy.argmax(outputLayers[i])
                                     print("  "+chars[min(NUM_OF_CHARS-1, sent[i])]+"  "+chars[min(NUM_OF_CHARS-1, predicted)]+" "+str(outputLayers[i][predicted])+" "+str(outputLayers[i][min(NUM_OF_CHARS-1, sent[i])])+" "+str(attentionScores[i]))


			# Predict, and calculate error gradients
			cost = train_model(sent)




def test_rnn(hashForExperiment='exp',
             corpus='corpus7.txt',
             learning_rate=0.001,
             L1_reg=0.00,
             L2_reg=0.00001,
             attention_reg=0.1,
             n_rec = 10,
             TIME_LENGTH = 10,
             NUM_OF_CHARS = 10, EPOCHS_NUM=8, USE_ATTENTION='y', momentum = 0.9, RECONSTRUCTOR_LSTM='y', N_EMB=50):
          general_attention = shared(numpy.cast['float32'](0.98))
          train_rnn(locals(), None, None, general_attention)


def train_rnn(p, pReader, pReconstructor, general_attention):
        setup = p
        setup['__file__'] = __file__

        initialOutput(p, 'train_rnn')

	rng = numpy.random.RandomState(1234)

        defaultInput = generateDefaultInput(p['NUM_OF_CHARS'])
        
        trng = T.shared_randomstreams.RandomStreams(1234)



        identityMatrix = theano.tensor.eye(n = p['NUM_OF_CHARS'], m=p['NUM_OF_CHARS'], k=0, dtype=theano.config.floatX)
        idxs = T.ivector()
        inputVectorSequence = identityMatrix[T.minimum(idxs, p['NUM_OF_CHARS']-1)].reshape((idxs.shape[0], p['NUM_OF_CHARS']))
        inputVectorSequenceF = theano.function(inputs=[idxs], outputs=inputVectorSequence)







	reader = RecurrentNetworkReader(
		rng=rng,trng = trng,
		n_ex_in = p['NUM_OF_CHARS'], 
		n_ex_out = p['NUM_OF_CHARS'], 
		n_rec = p['n_rec'], USE_ATTENTION = p['USE_ATTENTION'], defaultInput = shared(defaultInput), general_attention = general_attention, parameterSetting = pReader, n_emb = setup['N_EMB'],inputSentence=inputVectorSequence
	)
 
        reconstructor = RecurrentNetworkReconstructorLSTM(
			rng=rng,
			initialRecurrentState=reader.currentHiddenVector,
			n_ex_in = p['NUM_OF_CHARS'], 
			n_ex_out = p['NUM_OF_CHARS'], 
			n_rec = p['n_rec'],parameterSetting = pReconstructor, inputSentence = inputVectorSequence
	)


        trainLSTMDenoising(setup, reader, reconstructor, general_attention, idxs)





def trainLSTMDenoising(setup, reader, reconstructor, general_attention, idxs):
	
        TIME_LENGTH = setup['TIME_LENGTH']
        NUM_OF_CHARS = setup['NUM_OF_CHARS']

        regularizer =  setup['L1_reg'] * reader.L1 + setup['L1_reg'] * reconstructor.L1 + setup['L2_reg'] * reader.L2_sqr  + setup['L2_reg'] * reconstructor.L2_sqr


	cost = (
	    reconstructor.overall_negative_log_likelihood
	    + regularizer
            + setup['attention_reg'] * reader.attentionSoFar
	)

        params = reader.p.params + reconstructor.p.params
	#gparams = [T.grad(theano.gradient.grad_clip(cost, -1, 1), param) for param in params]



        updates = []
        ## Just gradient descent on cost
        for param in params:
		param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
		updates.append((param, param - setup['learning_rate']*param_update))
		updates.append((param_update, setup['momentum']*param_update + (1. - setup['momentum'])*T.grad(theano.gradient.grad_clip(cost, -1, 1), param)))

        do_the_training(setup, reader, reconstructor, general_attention, updates, cost, regularizer, reconstructor.overall_negative_log_likelihood, idxs)



import cPickle
def safeConfiguration(hashForExp, model):
        f = file(hashForExp+'.model', 'wb')
        cPickle.dump(model, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()
        print('Wrote model.')


def loadConfiguration(hashForExp):
        print('Loading file '+hashForExp+'.model')
        f = file(hashForExp+'.model', 'rb')
        model = cPickle.load(f)
        print(model)
        f.close()
        return model
