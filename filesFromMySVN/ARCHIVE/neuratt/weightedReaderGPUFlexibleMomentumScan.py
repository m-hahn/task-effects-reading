__docformat__ = 'restructedtext en'


import os
import sys
import timeit

import numpy

import theano
import theano.tensor as T

from theano import config
exception_verbosity='high'
optimizer=None

from theano import *


import langmod





def initWeightMatrix(rng, name, columnsNum, rowsNum, rangeOfRand):
        W_values = numpy.asarray(rng.uniform(low=-rangeOfRand, high=rangeOfRand,size=(columnsNum, rowsNum)),dtype=theano.config.floatX)
        W_values *= 4
        return theano.shared(value=W_values, name=name, borrow=True)



class RecurrentModule(object):

    def __init__(self, rng, externalInput, recurrentInput, n_ex_in, n_rec, n_ex_out, W=None, b=None,
                 activation=T.tanh):

        n_in = n_ex_in + n_rec
        n_out = n_rec + n_ex_out

        [self.W_re, self.W_er, self.W_ee, self.W_rr] = W
        [self.b_e, self.b_r] = b




# TODO range of random has to be adapted to the size of the network


class RecurrentNetwork(object):

    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, W=None, b=None):

        self.n_ex_in = n_ex_in
        self.n_ex_out = n_ex_out
        self.n_rec = n_rec
        self.time = 0
        self.rng = rng
        n_in = n_ex_in + n_rec
        n_out = n_ex_out + n_rec


        self.W_rr = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(1. / (n_rec + n_rec)))

        self.b_e = initWeightMatrix(rng,'b_e', n_ex_out, 1, 0) #
        self.b_r = initWeightMatrix(rng,'b_r', n_rec, 1, 0) #

        
        self.params = [self.W_rr]


        recurrentNodes = T.fmatrix()
        externalInput = T.fmatrix()

################

class RecurrentNetworkReconstructor(RecurrentNetwork):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, inputSentence, W=None, b=None):
        super(RecurrentNetworkReconstructor, self).__init__(rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec,W,b)
        self.W_re = initWeightMatrix(rng,'W_re',n_ex_in,n_rec, numpy.sqrt(1. / (n_ex_in + n_rec)))
        self.W_ee = initWeightMatrix(rng,'W_ee',n_ex_in,n_ex_out, numpy.sqrt(1. / (n_ex_in + n_ex_out)))
        self.params += [self.W_re, self.W_ee]
        self.L1 = (
            abs(self.W_re).sum() +
            abs(self.W_ee).sum() +
            abs(self.W_rr).sum()
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            (self.W_re ** 2).sum() +
            (self.W_ee ** 2).sum() +
            (self.W_rr ** 2).sum()
        )
        self.output_ext = []
        self.output_ext_funcs = []


        def recurrenceRec(externalInput, overall_negative_log_likelihood, currentRecurrentNodes):

                externalInput = externalInput.dimshuffle(0,'x')

		#lin_output_rec = self.b_r + T.dot(self.W_er, externalInput) + T.dot(self.W_rr, currentRecurrentNodes)
		#lin_output_ext = self.b_e + T.dot(self.W_re, currentRecurrentNodes) + T.dot(self.W_ee, externalInput)
		lin_output_rec = self.b_r + T.dot(self.W_rr, currentRecurrentNodes)
		lin_output_ext = self.b_e + T.dot(self.W_re, currentRecurrentNodes)
		externalOutput = T.nnet.softmax(lin_output_ext.T).T
		recurrentOutput = T.tanh(lin_output_rec)
                overall_negative_log_likelihoodNew = overall_negative_log_likelihood - T.sum(T.log(T.dot(externalOutput.T, externalInput)))
		currentRecurrentNodes = recurrentOutput
                return [overall_negative_log_likelihoodNew, currentRecurrentNodes]
        
        [negative_log_likelihoods, _], _ = theano.scan(fn=recurrenceRec, sequences = inputSentence, outputs_info=[shared(numpy.cast['float32'](0.0)), initialRecurrentState], non_sequences=[], n_steps=inputSentence.shape[0])
        self.overall_negative_log_likelihood = negative_log_likelihoods[-1]
	

################

class RecurrentNetworkReader(RecurrentNetwork):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, USE_ATTENTION, inputSentence, W=None, b=None):
        super(RecurrentNetworkReader, self).__init__(rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec,W,b)
        self.W_er = initWeightMatrix(rng,'W_er',n_rec, n_ex_out, numpy.sqrt(1. / (n_rec + n_ex_out))) # bei initWeightMatrix: first rownumber, then colnumber
        self.params+=[self.W_er]
        self.USE_ATTENTION = USE_ATTENTION

        #self.attentionSoFar = shared(numpy.cast['float32'](0.0)) # will be used in the computation of 'cost'
        if USE_ATTENTION == 'y':
            self.W_att = initWeightMatrix(rng,'W_att',n_rec, n_ex_in, numpy.sqrt(1. / 100.))
            self.params = [self.W_er, self.W_rr, self.W_att]
            # only for debugging/inspecting
            self.attention_scores = []
        else:
            self.params = [self.W_er, self.W_rr]

        self.L1 = (
            abs(self.W_er).sum() +
            abs(self.W_rr).sum()
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            (self.W_er ** 2).sum() +
            (self.W_rr ** 2).sum()
        )

 
# TODO Current
        def recurrenceRead(externalInput, currentRecurrentNodes):
                externalInput = externalInput.dimshuffle(0,'x')
 
		attention = T.sum(T.nnet.sigmoid(T.dot(currentRecurrentNodes.T, T.dot(self.W_att, externalInput))))
		externalInput = attention * externalInput
 
		lin_output_rec =  T.dot(self.W_er, externalInput) + T.dot(self.W_rr, currentRecurrentNodes) + self.b_r
		recurrentOutput = T.tanh(lin_output_rec)
		currentRecurrentNodesNew = recurrentOutput
		return [currentRecurrentNodesNew, attention]


        # TODO summing of attentions should be done only once!
        [recurrent, self.attentions], _ = theano.scan(fn=recurrenceRead, sequences = inputSentence, outputs_info=[initialRecurrentState,None], non_sequences=[], n_steps=inputSentence.shape[0])
	self.recurrentResult = recurrent[-1]
        self.recurrentBack = recurrent
        self.attentionSoFar = T.sum(self.attentions)



def test_rnn(hashForExperiment='exp',
             corpus='corpus7.txt',
             learning_rate=0.001,
             L1_reg=0.00,
             L2_reg=0.00001,
             attention_reg=0.1,
             n_rec = 9,
             TIME_LENGTH = 10,
             NUM_OF_CHARS = 8, EPOCHS_NUM=8, USE_ATTENTION='n', momentum = 0.9):

        print(__file__)
        print('Hash: '+hashForExperiment)
        print('Corpus: '+corpus)
        print('Learning Rate: '+str(learning_rate))
        print('L1: '+str(L1_reg))
        print('L2 '+str(L2_reg))
        print('AttentionReg: '+str(attention_reg))
        print('Recurrent Units: '+str(n_rec))
        print('Batch size: '+str(TIME_LENGTH))
        print('Alphabet size: '+str(NUM_OF_CHARS))
        print('Number of epochs: '+str(EPOCHS_NUM))
        print('Use Attention: '+USE_ATTENTION) #'y', 'n'
        print('Momentum: '+str(momentum))
	print '... building the model'


        
        plotLogLikelihood = open(hashForExperiment+'-logl.dat', 'w',0)

	rng = numpy.random.RandomState(1234)

        initialRecurrentStateReader =  initWeightMatrix(rng, 'rec', n_rec, 1, 0.24) 


        identityMatrix = theano.tensor.eye(n = NUM_OF_CHARS, m=NUM_OF_CHARS, k=0, dtype=theano.config.floatX)

        idxs = T.ivector()
        inputVectorSequence = identityMatrix[T.minimum(idxs, NUM_OF_CHARS-1)].reshape((idxs.shape[0], NUM_OF_CHARS))

        inputVectorSequenceF = theano.function(inputs=[idxs], outputs=inputVectorSequence)

	reader = RecurrentNetworkReader(
		rng=rng,
                initialRecurrentState=initialRecurrentStateReader,
		n_ex_in = NUM_OF_CHARS, 
		n_ex_out = NUM_OF_CHARS, 
		n_rec = n_rec, USE_ATTENTION = USE_ATTENTION, inputSentence = inputVectorSequence
	)


        reconstructor = RecurrentNetworkReconstructor(
                rng=rng,
                initialRecurrentState=reader.recurrentResult,
		n_ex_in = NUM_OF_CHARS, 
		n_ex_out = NUM_OF_CHARS, 
		n_rec = n_rec, inputSentence = inputVectorSequence
        )


        regularizer =  L1_reg * reader.L1 + L1_reg * reconstructor.L1 + L2_reg * reader.L2_sqr  + L2_reg * reconstructor.L2_sqr
       

	cost = (
	    T.sum(reconstructor.overall_negative_log_likelihood)
	    + regularizer
            + T.sum(attention_reg * reader.attentionSoFar)
	)

        

        params = reader.params + reconstructor.params

        updates = []
        ## Just gradient descent on cost
        for param in params:
		param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
		updates.append((param, param - learning_rate*param_update))
		updates.append((param_update, momentum*param_update + (1. - momentum)*T.grad(theano.gradient.grad_clip(cost, -1, 1), param)))


	train_model = theano.function(
	    inputs=[idxs],
	    outputs=Out(theano.sandbox.cuda.basic_ops.gpu_from_host(cost)),
	    updates=updates,
            #mode='ProfileMode'
	    #givens={
		#x: train_set_x[index * batch_size: (index + 1) * batch_size],
		#y: train_set_y[index * batch_size: (index + 1) * batch_size]
	    #}
	)

        

        regularizerf = theano.function(inputs=[], outputs=regularizer)
        attentionf = theano.function(inputs=[idxs], outputs=reader.attentionSoFar)
        costf = theano.function(inputs=[idxs], outputs=cost)
        loglikelihoodf = theano.function(inputs=[idxs], outputs=T.sum(reconstructor.overall_negative_log_likelihood))

    ###############
    # TRAIN MODEL #
    ###############
	print '... training'

	(sentences, chars) = langmod.read_corpus_into_batches_no_counting(corpus, TIME_LENGTH)

        iteration = -1

        for epoch in range(0,EPOCHS_NUM):
   	        for sent in sentences:
		    if len(sent)>=TIME_LENGTH:


                        iteration+=1

                        if iteration%100 == 0:

                                #print(theano.function(inputs=[idxs], outputs=reader.recurrentBack)(sent))

                                #print(theano.function(inputs=[idxs], outputs=reader.recurrentResult)(sent))

				print('-- '+str(iteration)+' --')
				print('Input '+str([chars[num] for num in sent]))
                                print(str(regularizerf())+" "+str(attentionf(sent)))



				print('Cost '+str(costf(sent)))

                                plotLogLikelihood.write(str(iteration)+'\t'+str(loglikelihoodf(sent))+'\n')

                                if USE_ATTENTION == 'y':
   				     print('Total Attention '+str(attentionf(sent)))


			cost = train_model(sent)

        plotLogLikelihood.close()
