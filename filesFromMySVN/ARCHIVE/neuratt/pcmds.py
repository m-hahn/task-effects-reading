#!/usr/bin/python2.7 

'''
this is a script for parallelly running several commands
'''

import argparse, os
from multiprocessing import Process

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('cmds', nargs='+', help = 'commands to be executed')
    parser.add_argument('--n_process', '-n', type=int, help = 'maximum number of processes to be executed paralelly')

    return parser.parse_args()

def run_cmds(cmds):
    for cmd in cmds:
        os.system(cmd)

def prun(cmds, max_para_num):
    max_para_num = min(len(cmds), max_para_num)
    print 'number of processes ', max_para_num

    cmd_groups = [[] for _ in xrange(max_para_num)]
    for i, cmd in enumerate(cmds):
        cmd_groups[i%max_para_num].append(cmd)

    processes = []
    for i in xrange(max_para_num):
        p = Process(target=run_cmds, args=(cmd_groups[i],))
        processes.append(p)
        p.start()

    for p in processes:
        p.join()
    print 'all processes done!'


if __name__ == '__main__':
    args = parse_args()
    if args.n_process is None:
        args.n_process = len(args.cmds)
    prun(args.cmds, args.n_process)


