lexiconPath = "/afs/cs.stanford.edu/u/mhahn/scr/num2CharsNoEntities-cnn"
filesPath = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/cnn/training/noentities/texts/"
fileListPath = "/afs/cs.stanford.edu/u/mhahn/scr/cnn-index.txt"
outPath = "/afs/cs.stanford.edu/u/mhahn/scr/num2CharsNoEntities-cnn-freqs"

counter = 0
with open(lexiconPath,"r") as lexicon:
  with open(fileListPath,"r") as files:
    lexicon = lexicon.read()
    lexicon = lexicon.split("\n")
    freqs = [0]*len(lexicon)

    for fileName in files:
       counter += 1
       if counter % 1000 == 0:
         print(counter)
       fileName = fileName.rstrip()
       with open(filesPath+fileName,"r") as inFile:
         inFile = inFile.read().rstrip().split("\n")
         for word in inFile:
#           print(word)
           word = int(word)
 #          print(word)
           #print(lexicon[word])
           freqs[word] = freqs[word]+1
           #print(freqs[word])
    with open(outPath,"w") as outFile:
        for word in range(0,len(lexicon)):
          print >>outFile, (lexicon[word]+" "+str(freqs[word]))
    print("Done")
