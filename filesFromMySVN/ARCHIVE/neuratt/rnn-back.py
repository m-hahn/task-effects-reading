__docformat__ = 'restructedtext en'


import os
import sys
import timeit

import numpy

import theano
import theano.tensor as T

from theano import config
exception_verbosity='high'
optimizer=None

from theano import *


import langmod





def initWeightMatrix(rng, name, columnsNum, rowsNum, rangeOfRand):
        W_values = numpy.asarray(rng.uniform(low=-rangeOfRand, high=rangeOfRand,size=(columnsNum, rowsNum)),dtype=theano.config.floatX)
        #    if activation == theano.tensor.nnet.sigmoid:
        W_values *= 4
        return theano.shared(value=W_values, name=name, borrow=True)



class RecurrentModule(object):

    def __init__(self, rng, externalInput, recurrentInput, n_ex_in, n_rec, n_ex_out, W=None, b=None,
                 activation=T.tanh):

        n_in = n_ex_in + n_rec
        n_out = n_rec + n_ex_out

        [self.W_re, self.W_er, self.W_ee, self.W_rr] = W
        [self.b_e, self.b_r] = b


        lin_output_rec = self.b_r + T.dot(self.W_er, externalInput) + T.dot(self.W_rr, recurrentInput)
        lin_output_ext = self.b_e + T.dot(self.W_re, recurrentInput) + T.dot(self.W_ee, externalInput)

        self.externalOutput = T.nnet.softmax(lin_output_ext.T).T


        self.recurrentOutput = activation(lin_output_rec)

        temp = theano.function(inputs=[],outputs=self.b_e )
        temp1 = theano.function(inputs=[],outputs=self.W_re )
        temp2 = theano.function(inputs=[],outputs=recurrentInput)
        print('external')
        print(temp())
        print(temp1())
        print(temp2())



# TODO NOTE be careful when changing the variables, you always have to change the things INSIDE the containers


class RecurrentNetwork(object):

    def __init__(self, rng, n_ex_in, n_ex_out, n_rec, W=None, b=None):

        self.currentOutput_rec = initWeightMatrix(rng, 'out-cur-rec', n_rec, 1, 0.24)
        self.currentOutput_ext = initWeightMatrix(rng, 'out-cur-ext', n_ex_out, 1, 0.24)
        self.currentRecurrentNodes = initWeightMatrix(rng, 'rec', n_rec, 1, 0.24)
        self.previousOutput_ext = initWeightMatrix(rng, 'out-prev', n_ex_out, 1, 0.24)

        self.currentInput_ext = None

        self.n_ex_in = n_ex_in
        self.n_ex_out = n_ex_out
        self.n_rec = n_rec
        self.time = 0
        self.rng = rng
        n_in = n_ex_in + n_rec
        n_out = n_ex_out + n_rec

        #if W is None:
        self.W_er = initWeightMatrix(rng,'W_er',n_rec, n_ex_out, numpy.sqrt(6. / 100.))
        self.W_re = initWeightMatrix(rng,'W_re',n_ex_in,n_rec, numpy.sqrt(6. / 100.))
        self.W_ee = initWeightMatrix(rng,'W_ee',n_ex_in,n_ex_out, numpy.sqrt(6. / 100.))
        self.W_rr = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(6. / 100.))

        #if b is None:
        self.b_e = initWeightMatrix(rng,'b_e', n_ex_out, 1, 0)
        self.b_r = initWeightMatrix(rng,'b_r', n_rec, 1, 0)

        self.W = [self.W_re, self.W_er, self.W_ee, self.W_rr]
        self.b = [self.b_e, self.b_r]


        # L1 norm ; one regularization option is to enforce L1 norm to
        # be small
        self.L1 = (
            abs(self.W_re).sum() +
            abs(self.W_er).sum() +
            abs(self.W_ee).sum() +
            abs(self.W_rr).sum()
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            (self.W_re ** 2).sum() +
            (self.W_er ** 2).sum() +
            (self.W_ee ** 2).sum() +
            (self.W_rr ** 2).sum()
        )
        self.params = [self.W_re, self.W_er, self.W_ee, self.W_rr] #, self.b_e, self.b_r]

        #negative_log_likelihoods = []
        self.negative_log_likelihood_at_current_timestep = 0

        


# (self, rng, externalInput, recurrentInput, n_ex_in, n_rec, n_ex_out, W=None, b=None,                 activation=T.tanh)

    def addTimeStep(self, externalInput, externalInputList):
        newReccurentModule = RecurrentModule(self.rng, externalInput, self.currentRecurrentNodes, self.n_ex_in, self.n_rec, self.n_ex_out, W=self.W, b=self.b,activation=T.tanh)
        self.currentRecurrentNodes = newReccurentModule.recurrentOutput
        self.previousOutput_rec = self.currentOutput_rec
        self.previousOutput_ext = self.currentOutput_ext
        self.currentOutput_ext = newReccurentModule.externalOutput
        self.currentInput_ext = externalInput
        print(type(self.previousOutput_rec))
        print(type(externalInput))
        print([x for x in range(len(externalInputList)) if externalInputList[x] == 1 ])
        self.negative_log_likelihood_at_current_timestep = - T.mean(T.log(T.dot(self.previousOutput_ext.T, externalInput)))    # .get_value()[([x for x in {0:len(externalInputList)} if externalInputList[x] == 1 ])[0]])
        #self.negative_log_likelihood_at_current_timestep = - T.mean(T.log(self.previousOutput[([x for x in range(len(externalInputList)) if externalInputList[x] == 1 ])[0]]))
        #print(self.negative_log_likelihood_at_current_timestep.get_value())
        temp = theano.function(
            inputs=[],
            outputs=self.previousOutput_ext.T#self.negative_log_likelihood_at_current_timestep
        )
        temp2 = theano.function(
            inputs=[],
            outputs=externalInput#self.negative_log_likelihood_at_current_timestep
        )
        temp3 = theano.function(
            inputs=[],
            outputs=self.negative_log_likelihood_at_current_timestep#self.negative_log_likelihood_at_current_timestep
        )
        print(temp())
        print(temp2())
        print(temp3())


def test_rnn(learning_rate=0.3, L1_reg=0.00, L2_reg=0.001, n_epochs=1000,
             dataset='mnist.pkl.gz', batch_size=20, n_hidden=500):

    print '... building the model'

    rng = numpy.random.RandomState(1234)

    classifier = RecurrentNetwork(
        rng=rng, 
        n_ex_in = langmod.NUM_OF_CARS, 
        n_ex_out = langmod.NUM_OF_CARS, 
        n_rec = 3
    )





    



    ###############
    # TRAIN MODEL #
    ###############
    print '... training'

    #TODO
    (corpus, chars, counts, maxCount) = langmod.read_corpus()

    for i in range(0, len(corpus)-1):
        newInputList = numpy.zeros((langmod.NUM_OF_CARS,1), dtype=theano.config.floatX)
        newInputList[min(langmod.NUM_OF_CARS-1, corpus[i])] = 1
        #print(min(999, corpus[i]))
        #print(newInputList[min(999, corpus[i])])
        #print(newInputList[999 ])
        #print([x for x in range(len(newInputList)) if newInputList[x] == [1] ])
        #newInput = theano.tensor.dmatrix()
        classifier.addTimeStep(shared(newInputList),newInputList)


        # start-snippet-4
        # the cost we minimize during training is the negative log likelihood of
        # the model plus the regularization terms (L1 and L2); cost is expressed
        # here symbolically
        cost = (
            classifier.negative_log_likelihood_at_current_timestep
            + L1_reg * classifier.L1
            + L2_reg * classifier.L2_sqr
        )
        # end-snippet-4
        print("..")
        print(classifier.negative_log_likelihood_at_current_timestep)
        print(type(cost))

        # start-snippet-5
        # compute the gradient of cost with respect to theta (sotred in params)
        # the resulting gradients will be stored in a list gparams
        gparams = [T.grad(cost, param) for param in classifier.params]

        # specify how to update the parameters of the model as a list of
        # (variable, update expression) pairs

        # given two lists of the same length, A = [a1, a2, a3, a4] and
        # B = [b1, b2, b3, b4], zip generates a list C of same size, where each
        # element is a pair formed from the two lists :
        #    C = [(a1, b1), (a2, b2), (a3, b3), (a4, b4)]
        updates = [
            (param, param - learning_rate * gparam)
            for param, gparam in zip(classifier.params, gparams)
        ]


        # compiling a Theano function `train_model` that returns the cost, but
        # in the same time updates the parameter of the model based on the rules
        # defined in `updates`
        train_model = theano.function(
            inputs=[],
            outputs=cost,
            updates=updates,
            givens={
                #x: train_set_x[index * batch_size: (index + 1) * batch_size],
                #y: train_set_y[index * batch_size: (index + 1) * batch_size]
            }
        )
        cost = train_model()
        print(cost)

