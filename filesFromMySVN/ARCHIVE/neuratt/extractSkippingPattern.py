import os

INPUT_DIR = "/disk/scratch2/s1582047/dundeetreebank/parts/PART1/annotation/att-pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb-0.0001/"
PATTERN_OUTPUT = "/disk/scratch2/s1582047/dundeetreebank/parts/PART1/patterns/pattern-att-pg-test-autoencoding-50-1000-0.7-100-R-4.1a0-emb-0.0001"
UNK_WORD = "10000"

fPattern = open(PATTERN_OUTPUT, 'w')

files = os.listdir(INPUT_DIR)
for fileName in files:
   fIn = open(INPUT_DIR+"/"+fileName, 'r')
   for line in fIn:
     line = line.rstrip().split("  ")
     if len(line) >= 3:
         if line[1] == UNK_WORD:
            fPattern.write("-1\n")
         else:
            fPattern.write(line[2]+"\n")
     else:
         fPattern.write("-1\n")
   fIn.close()

fPattern.close()
