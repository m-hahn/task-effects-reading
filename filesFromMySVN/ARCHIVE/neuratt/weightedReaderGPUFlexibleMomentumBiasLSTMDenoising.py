__docformat__ = 'restructedtext en'


import os
import sys
import timeit

import numpy

import theano
import theano.tensor as T

from theano import config
exception_verbosity='high'
optimizer=None

from theano import *


import langmod


from theano.ifelse import ifelse

from theano.tensor.shared_randomstreams import RandomStreams



from sys import getrecursionlimit, setrecursionlimit
setrecursionlimit(5000)




SOFT_ATTENTION ='n'
global_list = []
LEARNER='REINFORCE'
ATTENTION_COMP = 'neural'#'linear-form'#'neural'
INPUT='embedding'#'one-hot'#'embedding' # 'one-hot'

def initWeightMatrix(rng, name, columnsNum, rowsNum, rangeOfRand):
        W_values = numpy.asarray(rng.uniform(low=-rangeOfRand, high=rangeOfRand,size=(columnsNum, rowsNum)),dtype=theano.config.floatX)
        #    if activation == theano.tensor.nnet.sigmoid:
        W_values *= 4
        return theano.shared(value=W_values, name=name, borrow=True)



class RecurrentModule(object):

    def __init__(self, rng, externalInput, recurrentInput, n_ex_in, n_rec, n_ex_out, W=None, b=None,
                 activation=T.tanh):

        n_in = n_ex_in + n_rec
        n_out = n_rec + n_ex_out

        [self.W_re, self.W_er, self.W_ee, self.W_rr] = W
        [self.b_e, self.b_r] = b




# TODO range of random has to be adapted to the size of the network


class RecurrentNetwork(object):

    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec):

        self.currentOutput_ext = initWeightMatrix(rng, 'out-cur-ext', n_ex_out, 1, 0.24)
        self.currentRecurrentNodes = initialRecurrentState
        self.previousOutput_ext = initWeightMatrix(rng, 'out-prev', n_ex_out, 1, 0.24)


        self.n_ex_in = n_ex_in
        self.n_ex_out = n_ex_out
        self.n_rec = n_rec
        self.time = 0
        self.rng = rng
        n_in = n_ex_in + n_rec
        n_out = n_ex_out + n_rec

        self.W_er = initWeightMatrix(rng,'W_er',n_rec, n_ex_out, numpy.sqrt(6. / (n_rec + n_ex_out))) # bei initWeightMatrix: first rownumber, then colnumber
        self.W_rr = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(6. / (n_rec + n_rec)))

        self.b_e = initWeightMatrix(rng,'b_e', n_ex_out, 1, numpy.sqrt(6. / (n_rec + n_ex_out))) #
        self.b_r = initWeightMatrix(rng,'b_r', n_rec, 1, numpy.sqrt(6. / (n_rec + n_ex_out))) #

        
        self.params = [self.W_er, self.W_rr, self.b_e, self.b_r]


        #temp1 = theano.function(inputs=[],outputs=self.W_re )
        #temp2 = theano.function(inputs=[],outputs= self.currentRecurrentNodes)

        recurrentNodes = T.fmatrix()
        externalInput = T.fmatrix()


class LSTMNetworkParameters(object):
    def __init__(self, rng, n_ex_in, n_ex_out, n_rec):

        self.n_ex_in = n_ex_in
        self.n_ex_out = n_ex_out
        self.n_rec = n_rec

        n_in = n_ex_in + n_rec
        n_out = n_ex_out + n_rec

        self.W_hi = initWeightMatrix(rng,'W_hi',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))

        self.W_ci = initWeightMatrix(rng,'W_ci',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec))) # bei initWeightMatrix: first rownumber, then colnumber



        self.W_hf = initWeightMatrix(rng,'W_hf',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec))) # bei initWeightMatrix: first rownumber, then colnumber

        self.W_cf = initWeightMatrix(rng,'W_cf',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))



        self.W_hc = initWeightMatrix(rng,'W_hc',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))

       


        self.W_ho = initWeightMatrix(rng,'W_ho',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_ex_in))) # bei initWeightMatrix: first rownumber, then colnumber

        self.W_co = initWeightMatrix(rng,'W_co',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))

        self.b_i = initWeightMatrix(rng,'b_i', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out))) #
        self.b_f = initWeightMatrix(rng,'b_f', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out))) #
	self.b_c = initWeightMatrix(rng,'b_c', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out))) #
	self.b_o = initWeightMatrix(rng,'b_o', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out)))
        
        self.params = [self.W_hi, self.W_ci, self.W_hf, self.W_cf, self.W_hc, self.W_ho, self.W_co, self.b_i, self.b_f, self.b_c, self.b_o]



################

class RecurrentNetworkReconstructor(RecurrentNetwork):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec):
        super(RecurrentNetworkReconstructor, self).__init__(rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec)
        self.W_re = initWeightMatrix(rng,'W_re',n_ex_in,n_rec, numpy.sqrt(6. / (n_ex_in + n_rec)))
        self.W_ee = initWeightMatrix(rng,'W_ee',n_ex_in,n_ex_out, numpy.sqrt(6. / (n_ex_in + n_ex_out)))
        self.params += [self.W_re, self.W_ee]

        # TODO compute these automatically from params
        self.L1 = (
            abs(self.W_re).sum() +
            abs(self.W_er).sum() +
            abs(self.W_ee).sum() +
            abs(self.W_rr).sum() +
            abs(self.b_e).sum() +
            abs(self.b_r).sum()
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            (self.W_re ** 2).sum() +
            (self.W_er ** 2).sum() +
            (self.W_ee ** 2).sum() +
            (self.W_rr ** 2).sum() +
            (self.b_e ** 2).sum() +
            (self.b_r ** 2).sum()
        )
        self.output_ext = []
        self.output_ext_funcs = []


    def addTimeStep(self, externalInput):
        lin_output_rec = self.b_r + T.dot(self.W_er, externalInput) + T.dot(self.W_rr, self.currentRecurrentNodes)
        lin_output_ext = self.b_e + T.dot(self.W_re, self.currentRecurrentNodes) + T.dot(self.W_ee, externalInput)
        externalOutput = T.nnet.softmax(lin_output_ext.T).T
        recurrentOutput = T.tanh(lin_output_rec)

         #newReccurentModule = RecurrentModule(self.rng, externalInput, , self.n_ex_in, self.n_rec, self.n_ex_out, W=self.W, b=self.b,activation=T.tanh)

        self.currentRecurrentNodes = recurrentOutput
        self.previousOutput_ext = self.currentOutput_ext
        #self.currentOutput_ext = externalOutput
        self.output_ext.append(externalOutput)
        self.output_ext_funcs.append(theano.function(inputs=[],outputs=externalOutput))



class LSTMNetworkParametersReconstructor(LSTMNetworkParameters):
    def __init__(self, rng, n_ex_in, n_ex_out, n_rec):
         LSTMNetworkParameters.__init__(self, rng, n_ex_in, n_ex_out, n_rec)
         self.W_ce = initWeightMatrix(rng,'W_ce',n_ex_out, n_rec, numpy.sqrt(0.1 / (n_ex_out + n_rec)))
         self.params += [self.W_ce]


class RecurrentNetworkReconstructorLSTM(object):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, parameterSetting = None):
        self.currentHiddenVector = initialRecurrentState
        self.memoryCellVector = initWeightMatrix(rng,'W_er',n_rec, 1, 0.0)

        self.p = parameterSetting
        if self.p == None:
             self.p = LSTMNetworkParametersReconstructor(rng, n_ex_in, n_ex_out, n_rec)


        recurrentNodes = T.fmatrix()
        externalInput = T.fmatrix()


        self.L1 = (
            shared(numpy.cast['float32'](0.0))
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            shared(numpy.cast['float32'](0.0))
        )

        self.output_ext = []
        self.output_ext_funcs = []
        

# TODO just get rid of the wasted computations with the external input vector
    def addTimeStep(self, externalInput):
        hiddenPrevious = self.currentHiddenVector
        cellPrevious = self.memoryCellVector

        if hasattr(self.p, 'W_xi'):
            print('Node feeding back input 224')
            inputCurrent = T.nnet.sigmoid(T.dot(self.p.W_xi, externalInput) + T.dot(self.p.W_hi, hiddenPrevious) + T.dot(self.p.W_ci, cellPrevious) + self.p.b_i)
            forgetCurrent = T.nnet.sigmoid(T.dot(self.p.W_xf, externalInput) + T.dot(self.p.W_hf, hiddenPrevious) + T.dot(self.p.W_cf, cellPrevious) + self.p.b_f)
            cellCurrent = forgetCurrent * cellPrevious + inputCurrent * T.tanh(T.dot(self.p.W_xc, externalInput) + T.dot(self.p.W_hc, hiddenPrevious) + self.p.b_c)
            outputCurrent = T.nnet.sigmoid(T.dot(self.p.W_xo, externalInput) + T.dot(self.p.W_ho, hiddenPrevious) + T.dot(self.p.W_co, cellCurrent) + self.p.b_o)
            hiddenCurrent = outputCurrent * T.tanh(cellCurrent)
        else:
            inputCurrent = T.nnet.sigmoid(T.dot(self.p.W_hi, hiddenPrevious) + T.dot(self.p.W_ci, cellPrevious) + self.p.b_i)
            forgetCurrent = T.nnet.sigmoid(T.dot(self.p.W_hf, hiddenPrevious) + T.dot(self.p.W_cf, cellPrevious) + self.p.b_f)
            cellCurrent = forgetCurrent * cellPrevious + inputCurrent * T.tanh(T.dot(self.p.W_hc, hiddenPrevious) + self.p.b_c)
            outputCurrent = T.nnet.sigmoid(T.dot(self.p.W_ho, hiddenPrevious) + T.dot(self.p.W_co, cellCurrent) + self.p.b_o)
            hiddenCurrent = outputCurrent * T.tanh(cellCurrent)

        self.currentHiddenVector = hiddenCurrent
        self.memoryCellVector = cellCurrent


        # EXTERNAL OUTPUT
        externalOutput = T.nnet.softmax((T.dot(self.p.W_ce, cellCurrent)).T).T

        self.output_ext.append(externalOutput)
        self.output_ext_funcs.append(theano.function(inputs=[],outputs=externalOutput))


################

class LSTMNetworkParametersReader(LSTMNetworkParameters):
    def __init__(self, rng, n_ex_in, n_ex_out, n_rec, USE_ATTENTION, N_EMB):
         if INPUT == 'one-hot':
             n_emb = n_ex_in
         else:
             n_emb = N_EMB

         LSTMNetworkParameters.__init__(self, rng, n_emb, n_ex_out, n_rec)

         if INPUT == 'embedding':
             self.W_emb = initWeightMatrix(rng,'W_emb',n_emb, n_ex_in, numpy.sqrt(0.1 / (n_emb + n_ex_in)))
             self.params += [self.W_emb]

         self.W_xf = initWeightMatrix(rng,'W_xf',n_rec, n_emb, numpy.sqrt(0.1 / (n_rec + n_emb)))
         self.W_xi = initWeightMatrix(rng,'W_xi',n_rec, n_emb, numpy.sqrt(0.1 / (n_rec + n_emb))) # bei initWeightMatrix: first rownumber, then colnumber
         self.W_xc = initWeightMatrix(rng,'W_xc',n_rec, n_emb, numpy.sqrt(0.1 / (n_rec + n_emb))) # bei initWeightMatrix: first rownumber, then colnumber
         self.W_xo = initWeightMatrix(rng,'W_xo',n_rec, n_emb, numpy.sqrt(0.1 / (n_rec + n_rec)))
         self.params += [self.W_xf, self.W_xi, self.W_xc, self.W_xo]
	 if USE_ATTENTION == 'y':
             print('ERROR 255: Was not expecting USE_ATTENTION == \'y\' in the initializer')
             self.createAttentionParameters(rng, n_rec, n_emb, 0, n_emb)
         else:
             self.params += []
             self.W_att = None
         self.initialRecurrentState =  initWeightMatrix(rng, 'rec', n_rec, 1, 0.24) # TODO should be a model parameter
         self.initialMemoryCellVector = initWeightMatrix(rng,'W_er',n_rec, 1, 0.0)
         #self.params += [self.initialRecurrentState]
 


    def createAttentionParametersAndRemoveOthersFromParams(self, rng, n_rec, n_ex_in, N_EMB):
         self.createAttentionParameters(rng, n_rec, n_emb,1)

    def createAttentionParameters(self, rng, n_rec, n_ex_in,removeOtherParams, N_EMB):
         if INPUT == 'one-hot':
             n_emb = n_ex_in
         else:
             n_emb = N_EMB
         if hasattr(self, 'W_att') and self.W_att != None:
             print('Warning: Attention network exists already, keeping it!')
         else:
             print('Create attention network')
             if removeOtherParams == 1:
                 self.params=[]
             if ATTENTION_COMP == 'linear-form':
		     self.W_att = initWeightMatrix(rng,'W_att',n_rec, n_emb, numpy.sqrt(6. / (n_rec + n_emb)))
		     self.params += [self.W_att]
		     if LEARNER == 'TD':
			 self.W_noatt = initWeightMatrix(rng,'W_att',n_rec, n_emb, numpy.sqrt(6. / (n_rec + n_emb)))

			 self.W_attP = initWeightMatrix(rng,'W_att',n_rec, n_emb, numpy.sqrt(6. / (n_rec + n_emb)))
			 self.W_noattP = initWeightMatrix(rng,'W_att',n_rec, n_emb, numpy.sqrt(6. / (n_rec + n_emb)))

			 self.params += [self.W_noatt]
			 self.previousParams = [self.W_attP,self.W_noattP]
             elif ATTENTION_COMP == 'neural':
                     n_attention_hidden = 100
                     self.W_att = 0
                     self.W_att_rh = initWeightMatrix(rng,'W_att_rh',n_attention_hidden, n_rec, numpy.sqrt(1. / (n_attention_hidden + n_rec)))
                     self.W_att_ih = initWeightMatrix(rng, 'W_att_ih',n_attention_hidden, n_emb, numpy.sqrt(1. / (n_attention_hidden + n_emb)))
                     self.W_att_ho = initWeightMatrix(rng, 'W_att_ho',1, n_attention_hidden, numpy.sqrt(1. / (n_attention_hidden + 1)))
                     self.params += [self.W_att_rh, self.W_att_ih, self.W_att_ho]
	     else:
                     print('ERROR 289')

class RecurrentNetworkReader():
    def __init__(self, rng, trng, n_ex_in, n_ex_out, n_rec, USE_ATTENTION, defaultInput, general_attention, parameterSetting, n_emb):
        if hasattr(parameterSetting, 'initialRecurrentState'):
             self.currentHiddenVector = parameterSetting.initialRecurrentState
             self.memoryCellVector = parameterSetting.initialMemoryCellVector
        else:
             self.currentHiddenVector =  initWeightMatrix(rng, 'rec', n_rec, 1, 0.24)
             self.memoryCellVector = initWeightMatrix(rng,'W_er',n_rec, 1, 0.0)
             print('Warning: Randomly initializing hidden vectors')

        

        self.p = parameterSetting
        if self.p == None:
             self.p = LSTMNetworkParametersReader(rng, n_ex_in, n_ex_out, n_rec, USE_ATTENTION, n_emb)


        recurrentNodes = T.fmatrix()
        externalInput = T.fmatrix()

        self.trng = trng

        self.USE_ATTENTION = USE_ATTENTION


        self.attentionSoFar = shared(numpy.cast['float32'](0.0)) # will be used in the computation of 'cost'
        

        self.attention_scores = []
        self.L1 = (
            0
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            0
        )

        self.general_attention = general_attention

        self.defaultExternalInput = defaultInput
        if INPUT == 'embedding':
            self.defaultExternalInput =  T.dot(self.p.W_emb, defaultInput) #0*... for zerodefault

        self.probabilityOfChoicesSoFar = shared(numpy.cast['float32'](1.0))
        if LEARNER == 'TD':
            self.Qsas = []
            self.QsaPs = []

    def computeAttention(self, rawExternalInput):
        if ATTENTION_COMP == 'linear-form':
             return T.sum(T.nnet.sigmoid(T.dot(self.currentHiddenVector.T, T.dot(self.p.W_att, rawExternalInput))))
        elif ATTENTION_COMP == 'neural':
             return T.sum(T.nnet.sigmoid(T.dot(self.p.W_att_ho, T.tanh(T.dot(self.p.W_att_rh, self.currentHiddenVector) + T.dot(self.p.W_att_ih, rawExternalInput)))))
       #[self.W_att_rh, self.W_att_ih, self.W_att_ho

    def computeAntiAttention(self, rawExternalInput):
        return T.sum(T.nnet.sigmoid(T.dot(self.currentHiddenVector.T, T.dot(self.p.W_noatt, rawExternalInput))))

    def addTimeStep(self, rawExternalInput):
        if INPUT == 'embedding':
             rawExternalInput = T.dot(self.p.W_emb, rawExternalInput)


        if self.USE_ATTENTION == 'y':
              if SOFT_ATTENTION=='y':
                    attention = self.computeAttention(rawExternalInput) 
                    self.attention_scores.append(attention)
                    externalInput = rawExternalInput  * attention
                    self.attentionSoFar += attention
                    #print(self.attentionSoFar)
                    #print(T.grad(attention, self.p.W_att))
                    #global_list.append(theano.function(inputs=[], outputs=T.grad(attention, self.p.W_att)))
              elif LEARNER=='REINFORCE':
                    rollDice = self.trng.uniform(size=(), low=0.0, high=1.0) 
                    attention = self.computeAttention(rawExternalInput) 
                    self.attention_scores.append(T.eye(1,2,0) * ifelse(T.lt(rollDice, attention), 1, 0) + T.eye(1,2,1) * attention)
                    externalInput = ifelse(T.lt(rollDice, attention),  rawExternalInput, self.defaultExternalInput)
                    self.attentionSoFar += ifelse(T.lt(rollDice, attention), 1, 0)
                    self.probabilityOfChoicesSoFar *= ifelse(T.lt(rollDice, attention), attention, 1-attention)
              elif LEARNER=='TD': #TODO
                    # get random numbers
                    rollDice = self.trng.uniform(size=(), low=0.0, high=1.0)
                    rollDice2 = self.trng.uniform(size=(), low=0.0, high=1.0)

                    #compute Q values from current parameters
                    attention =  self.computeAttention(rawExternalInput)
                    antiAttention = self.computeAntiAttention(rawExternalInput)

                    # make choice for epsilon-greedy policy
                    lookAtWord = ifelse(T.lt(rollDice, 0.95), ifelse(T.lt(attention, antiAttention), 1, 0), ifelse(T.lt(rollDice2, 0.5), 1, 0))
                    externalInput = ifelse(T.lt(lookAtWord, 0.1), self.defaultExternalInput, rawExternalInput)
                    self.attentionSoFar += lookAtWord

                    # compute Q values from previous parameters
                    attentionOldP =  T.sum(T.nnet.sigmoid(T.dot(self.currentHiddenVector.T, T.dot(self.p.W_attP, rawExternalInput))))
                    antiAttentionOldP = T.sum(T.nnet.sigmoid(T.dot(self.currentHiddenVector.T, T.dot(self.p.W_noattP, rawExternalInput))))
                    
                    Qsa = ifelse(T.lt(lookAtWord,0.1), antiAttention, attention)
                    QsaP = T.largest(attentionOldP, antiAttentionOldP)

                    self.Qsas.append(Qsa)
                    self.QsaPs.append(QsaP)


              else:
                    print('ERROR')
                    rollDiceOut = self.trng.uniform(size=(), low=0.0, high=1.0)

                    rollDice = ifelse(T.lt(rollDiceOut, 0.8), 0.0, self.trng.uniform(size=(), low=0.0, high=1.0))

                    attention = self.computeAttention(rawExternalInput)
                    self.attention_scores.append(ifelse(T.lt(rollDice, attention), 1, 0))
                    externalInput = ifelse(T.lt(rollDice, attention),  rawExternalInput, self.defaultExternalInput)
                    self.attentionSoFar += ifelse(T.lt(rollDice, attention), 1, 0)
                    self.probabilityOfChoicesSoFar *= ifelse(T.lt(rollDiceOut,0.9), 1.0, ifelse(T.lt(rollDice, attention), attention, 1-attention))
        else:
              if SOFT_ATTENTION == 'y':
                   if 1==0:
                      rollDice = self.trng.uniform(size=(), low=0.0, high=1.0)
                      externalInput = rollDice * rawExternalInput
                      self.attention_scores.append(rollDice)
                   else:
                      self.attention_scores.append(shared(numpy.cast['float32'](1.0)))
                      externalInput = rawExternalInput
              else:
                   rollDice = self.trng.uniform(size=(), low=0.0, high=1.0)
                   attention = self.general_attention #+ 0.5 * (1 - self.general_attention) * self.trng.uniform(size=(), low=0.0, high=1.0)
                   externalInput = ifelse(T.lt(rollDice, attention),  rawExternalInput, self.defaultExternalInput)
                   self.attention_scores.append(ifelse(T.lt(rollDice, attention), 1, 0))
              

        hiddenPrevious = self.currentHiddenVector
        cellPrevious = self.memoryCellVector

        #externalInput = shared(numpy.cast['float32'](0.0)) * externalInput

 

        inputCurrent = T.nnet.sigmoid(T.dot(self.p.W_xi, externalInput) + T.dot(self.p.W_hi, hiddenPrevious) + T.dot(self.p.W_ci, cellPrevious) + self.p.b_i)
        forgetCurrent = T.nnet.sigmoid(T.dot(self.p.W_xf, externalInput) + T.dot(self.p.W_hf, hiddenPrevious) + T.dot(self.p.W_cf, cellPrevious) + self.p.b_f)
        cellCurrent = forgetCurrent * cellPrevious + inputCurrent * T.tanh(T.dot(self.p.W_xc, externalInput) + T.dot(self.p.W_hc, hiddenPrevious) + self.p.b_c)
        outputCurrent = T.nnet.sigmoid(T.dot(self.p.W_xo, externalInput) + T.dot(self.p.W_ho, hiddenPrevious) + T.dot(self.p.W_co, cellCurrent) + self.p.b_o)
        hiddenCurrent = outputCurrent * T.tanh(cellCurrent)

        self.currentHiddenVector = hiddenCurrent
        self.memoryCellVector = cellCurrent





def resume_denoising_lstm(hashForExperimentNew, hashForExperimentOld, general_attention = None, learning_rate = None):
        print('Resuming '+hashForExperimentOld)
        model = loadConfiguration(hashForExperimentOld)
        print('   from iteration '+str(model['iteration']))
        setup = model['setup']
        setup['hashForExperiment'] = hashForExperimentNew
        pReader = model['reader.p']
        pReconstructor = model['reconstructor.p']
        print(setup)

        if('general_attention' in model and general_attention == None):
               general_attention = model['general_attention']
        elif(general_attention == None):
               general_attention = shared(numpy.cast['float32'](0.98))
        else:
               general_attention = general_attention 
        
        if(not (learning_rate  == None)):
              setup['learning_rate'] = learning_rate

        if(not ('N_EMB' in setup)):
            setup['N_EMB'] = 50


        #setup['learning_rate'] = 0.0

        train_rnn(setup, pReader, pReconstructor, general_attention)



# TODO new parameters: attention_reg, learning_rate_attention

def resume_for_attention(hashForExperimentNew, hashForExperimentOld, learning_rate = 0.01, attention_reg = 0.1, EPOCHS_NUM = 10, ONLY_TRAIN_ATT = 'y'):
        print('Starting to train attention for '+hashForExperimentOld)
        model = loadConfiguration(hashForExperimentOld)
        print('   from iteration '+str(model['iteration']))
        setup = model['setup']
        setup['hashForExperiment'] = hashForExperimentNew
        pReader = model['reader.p']
        pReconstructor = model['reconstructor.p']
        print(setup)



        if('general_attention' in model):
               general_attention = model['general_attention']
               if(str(type(general_attention)) == '<type \'float\'>'):
                     general_attention = shared(general_attention)
	else:
               print('Note: no shared attention found')
               general_attention = shared(numpy.cast['float32'](0.98))


        
        if(not ('N_EMB' in setup)):
            setup['N_EMB'] = 50

	print('Note: Resetting some values...')
        setup['attention_reg'] = attention_reg
        setup['learning_rate'] = learning_rate
        setup['EPOCHS_NUM'] = EPOCHS_NUM

	# ADD NEW VALUES
        setup['LEARNER'] = LEARNER


        setup['USE_ATTENTION'] = 'y'


        removeOtherParams = 1
        if ONLY_TRAIN_ATT == 'n':
            removeOtherParams = 0

        pReader.createAttentionParameters(numpy.random.RandomState(1234), setup['n_rec'], setup['NUM_OF_CHARS'], removeOtherParams, setup['N_EMB']) 


        train_attention(setup, pReader, pReconstructor, general_attention)


def initialOutput(p, methodName):
        print(__file__)
        print(methodName)
        print('Hash: '+p['hashForExperiment'])
        print('Corpus: '+p['corpus'])
        print('Learning Rate: '+str(p['learning_rate']))
        print('L1: '+str(p['L1_reg']))
        print('L2 '+str(p['L2_reg']))
        print('AttentionReg: '+str(p['attention_reg']))
        print('Recurrent Units: '+str(p['n_rec']))
        print('Batch size: '+str(p['TIME_LENGTH']))
        print('Alphabet size: '+str(p['NUM_OF_CHARS']))
        print('Number of epochs: '+str(p['EPOCHS_NUM']))
        print('Use Attention: '+p['USE_ATTENTION']) #'y', 'n'
        print('Momentum: '+str(p['momentum']))
        print('Reconstructor LSTM? '+str(p['RECONSTRUCTOR_LSTM']))
        print('Embedding Dimension '+str(p['N_EMB']))

def generateDefaultInput(num_of_chars):
       defaultInput = numpy.zeros((num_of_chars,1), dtype=theano.config.floatX)
       defaultInput[num_of_chars-1] = 1
       return defaultInput

def train_attention(p, pReader, pReconstructor, general_attention):
 

        initialOutput(p, 'train_attention')
        setup = p
        setup['__file__'] = __file__


	print '... building the model'
        

	rng = numpy.random.RandomState(1234)

        defaultInput = generateDefaultInput(p['NUM_OF_CHARS'])

        trng = T.shared_randomstreams.RandomStreams(1234)

	reader = RecurrentNetworkReader(
		rng=rng,trng = trng,
		n_ex_in = p['NUM_OF_CHARS'], 
		n_ex_out = p['NUM_OF_CHARS'], 
		n_rec = p['n_rec'], USE_ATTENTION = p['USE_ATTENTION'], defaultInput = shared(defaultInput), general_attention = general_attention, parameterSetting = pReader, n_emb = p['N_EMB']
	)
        
        # the input list variables
        inputLists = []
        #noiseVariables = []
        SOFInputList = numpy.zeros((p['NUM_OF_CHARS'],1), dtype=theano.config.floatX)
	SOFInputList[p['NUM_OF_CHARS']-1] = 1
        for i in range(0,p['TIME_LENGTH']):
             newInputList = shared(SOFInputList, name='inp '+str(i)) 
             #newNoiseVariable = shared(numpy.cast['float32'](0.0))
             reader.addTimeStep(newInputList)
             inputLists.append(newInputList)
             #noiseVariables.append(newNoiseVariable)
	     
 
	if(p['RECONSTRUCTOR_LSTM'] == 'y'):
		reconstructor = RecurrentNetworkReconstructorLSTM(
			rng=rng,
			initialRecurrentState=reader.currentHiddenVector,
			n_ex_in = p['NUM_OF_CHARS'], 
			n_ex_out = p['NUM_OF_CHARS'], 
			n_rec = p['n_rec'],parameterSetting = pReconstructor
		)
	else:
		reconstructor = RecurrentNetworkReconstructor(
			rng=rng,
			initialRecurrentState=reader.currentHiddenVector,
			n_ex_in = p['NUM_OF_CHARS'], 
			n_ex_out = p['NUM_OF_CHARS'], 
			n_rec = p['n_rec'],parameterSetting=pReconstructor
		)
	reconstructor.addTimeStep(shared(defaultInput))
	for i in range(0,p['TIME_LENGTH']):
	     #reconstructor.addTimeStep(inputLists[i])
	     reconstructor.addTimeStep(shared(defaultInput))
	#'READONCE'

        if LEARNER == 'TD':
              trainLSTMAttentionTD(setup, reader, reconstructor, inputLists, general_attention)
        else:
              trainLSTMAttention(setup, reader, reconstructor, inputLists, general_attention)






def trainLSTMAttentionTD(setup, reader, reconstructor, inputLists, general_attention):
	overall_negative_log_likelihood = shared(numpy.cast['float32'](0.0))
	for i in range(0, setup['TIME_LENGTH']):
	      overall_negative_log_likelihood += - T.sum(T.log(T.dot(reconstructor.output_ext[i].T, inputLists[i])))
        # TODO indices are wrong, so no training for first symbol??? HAE why should this be a problem? we simply disregard the last output, as it comes from the state where even the last word has been shown to the reconstructor
		
        TIME_LENGTH = setup['TIME_LENGTH']
        NUM_OF_CHARS = setup['NUM_OF_CHARS']

        regularizer =  setup['L1_reg'] * reader.L1 + setup['L1_reg'] * reconstructor.L1 + setup['L2_reg'] * reader.L2_sqr  + setup['L2_reg'] * reconstructor.L2_sqr

        reward = overall_negative_log_likelihood + setup['attention_reg'] * reader.attentionSoFar

	cost = (
	    overall_negative_log_likelihood
	    + regularizer
            + setup['attention_reg'] * reader.attentionSoFar
	)

        params = reader.p.params

        print('TODO THE UPDATES')

        do_the_training(setup, reader, reconstructor, inputLists, general_attention, updates, cost, regularizer, overall_negative_log_likelihood)






def trainLSTMAttention(setup, reader, reconstructor, inputLists, general_attention):
	overall_negative_log_likelihood = shared(numpy.cast['float32'](0.0))
	for i in range(0, setup['TIME_LENGTH']):
	      overall_negative_log_likelihood += - T.sum(T.log(T.dot(reconstructor.output_ext[i].T, inputLists[i])))
        # TODO indices are wrong, so no training for first symbol??? HAE why should this be a problem? we simply disregard the last output, as it comes from the state where even the last word has been shown to the reconstructor
		
        TIME_LENGTH = setup['TIME_LENGTH']
        NUM_OF_CHARS = setup['NUM_OF_CHARS']

        regularizer =  setup['L1_reg'] * reader.L1 + setup['L1_reg'] * reconstructor.L1 + setup['L2_reg'] * reader.L2_sqr  + setup['L2_reg'] * reconstructor.L2_sqr

        reward = overall_negative_log_likelihood + setup['attention_reg'] * reader.attentionSoFar

	cost = (
	    overall_negative_log_likelihood
	    + regularizer
            + setup['attention_reg'] * reader.attentionSoFar
	)

        params = reader.p.params

        if(not 'reinforce_momentum' in setup):
             setup['reinforce_momentum'] = 0.9

        moving_average = shared(numpy.cast['float32'](0.0))

        updates = []
        ## Just gradient descent on cost
        for param in params:
                print(param)
                print(setup['learning_rate'])
		param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)


		updates.append((param, param - setup['learning_rate']*param_update))
                if SOFT_ATTENTION == 'y':
		     updates.append((param_update, setup['reinforce_momentum']*param_update + (1. - setup['reinforce_momentum'])*T.grad(theano.gradient.grad_clip(cost, -1, 1), param)))
        	else:
                     updates.append((param_update, setup['reinforce_momentum']*param_update + (1. - setup['reinforce_momentum'])*(reward-moving_average)*T.grad(theano.gradient.grad_clip(T.log(reader.probabilityOfChoicesSoFar), -1, 1), param))) 



        updates.append((moving_average, shared(numpy.cast['float32'](0.9)) * moving_average + shared(numpy.cast['float32'](0.1)) * reward))

        #print(updates)

        do_the_training(setup, reader, reconstructor, inputLists, general_attention, updates, cost, regularizer, overall_negative_log_likelihood)


def do_the_training(setup, reader, reconstructor, inputLists, general_attention, updates, cost, regularizer, overall_negative_log_likelihood):
        plotLogLikelihood = open(setup['hashForExperiment']+'-logl.dat', 'w',0)
        #try:
        do_the_training_inner(setup, reader, reconstructor, inputLists, general_attention, updates, cost, regularizer, overall_negative_log_likelihood, plotLogLikelihood)
        #except:
        #     1 ==1

        plotLogLikelihood.close()


def do_the_training_inner(setup, reader, reconstructor, inputLists, general_attention, updates, cost, regularizer, overall_negative_log_likelihood, plotLogLikelihood):

        TIME_LENGTH = setup['TIME_LENGTH']
        NUM_OF_CHARS = setup['NUM_OF_CHARS']

	train_model = theano.function(
	    inputs=[],
	    outputs=Out(theano.sandbox.cuda.basic_ops.gpu_from_host(cost)),
	    updates=updates,
            #mode='ProfileMode'
	    #givens=givens
	)

        

        #print(numpy.asarray(theano.function(inputs=[], outputs=reader.p.W_emb)()))

        regularizerf = theano.function(inputs=[], outputs=regularizer)
        attention = theano.function(inputs=[], outputs=reader.attentionSoFar)
        costf = theano.function(inputs=[], outputs=cost)
        loglikelihoodf = theano.function(inputs=[], outputs=overall_negative_log_likelihood)

        for_output = theano.function(inputs=[], outputs=(reconstructor.output_ext + reader.attention_scores))

    ###############
    # TRAIN MODEL #
    ###############
	print '... training'
 

	(sentences, chars) = langmod.read_corpus_into_batches_no_counting(setup['corpus'], TIME_LENGTH)

        iteration = -1

        import time
        current = time.time()

 


        print(setup)

        for epoch in range(0,setup['EPOCHS_NUM']):
                #print(iteration)
   	        for sent in sentences:

                    #print(type(general_attention))
                    #general_attention.set_value(numpy.cast['float32'](0.6 + 0.9*(general_attention.get_value()-0.6)))
                    #print('NEW ATTENTION '+str(general_attention.get_value()))

                    #print(sent)
		    if len(sent)>=TIME_LENGTH:
                        if((iteration+5)%50000 == 0):
                              general_attention.set_value(numpy.cast['float32'](0.6 + 0.95*(general_attention.get_value()-0.6)))
                              print('NEW ATTENTION '+str(general_attention.get_value()))

                        if((iteration+5)%1000 == 0):
                              safeConfiguration(setup['hashForExperiment'], {'iteration':iteration, 'setup' : setup, 'reader.p' : reader.p, 'reconstructor.p' : reconstructor.p, 'general_attention' : general_attention})



                        iteration+=1

			# Build Input Vector Sequence
			for i in range(0,TIME_LENGTH):
			     newInputList = numpy.zeros((NUM_OF_CHARS,1), dtype=theano.config.floatX)
			     newInputList[min(NUM_OF_CHARS-1, sent[i])] = 1
			     inputLists[i].set_value(newInputList)




#TODO previous output becomes new input

                        # NOTE all the things are recomputed for printing here...

                        if iteration%100 == 0:

                                #print(numpy.asarray(updatefs[0]()))
                                #print(numpy.asarray(updatefs[1]()))
                                #print(numpy.asarray(updatefs[2]()))
              

                                #for x in global_list:
                                #     print(x())

                                if setup['USE_ATTENTION'] == 'n' and 'SOFT_ATTENTION' == 'y':
                                    print('Attention '+str(reader.general_attention.get_value()))
                                else:
                                    print('Attention '+str(attention()))

                                print((time.time()-current))
                                current = time.time()

				print('-- '+str(iteration)+' --')
				print('Input '+str([chars[num] for num in sent]))
				print('Cost '+str(costf()))
                                print(str(regularizerf())+" "+str(attention()))

                                plotLogLikelihood.write(str(iteration)+'\t'+str(loglikelihoodf())+'\n')

                                #if setup['USE_ATTENTION'] == 'y':
   				     #print('Total Attention '+str(attention()))
				     # Attention scores
				     #print('Attention '+str([(chars[sent[i]], (reader.attention_scores[i])().item()) for i in range(0,TIME_LENGTH)]))


                                resultsForOutput = for_output()
                                

			        [outputLayers, attentionScores] = numpy.split(resultsForOutput, [TIME_LENGTH+1])   #[range(NUM_OF_CHARS,]

				#print('OUTPUT')
                                #print(outputLayers)
                                #print('ATTENTION')
                                #print(attentionScores)
                                #print('.')
				for i in range(0,TIME_LENGTH):

                                     #print(outputLayers[i])
                                     #print(attentionScores[i])
                                     predicted = numpy.argmax(outputLayers[i])
                                     #if USE_ATTENTION =='y':
    				     #           print("  "+chars[min(NUM_OF_CHARS-1, sent[i])]+"  "+chars[min(NUM_OF_CHARS-1, predicted)]+"  "+str(reader.attention_scores[i]()))
                                     #else:
                                     print("  "+chars[min(NUM_OF_CHARS-1, sent[i])]+"  "+chars[min(NUM_OF_CHARS-1, predicted)]+" "+str(outputLayers[i][predicted])+" "+str(outputLayers[i][min(NUM_OF_CHARS-1, sent[i])])+" "+str(attentionScores[i]))


			# Predict, and calculate error gradients
			cost = train_model()




def test_rnn(hashForExperiment='exp',
             corpus='corpus7.txt',
             learning_rate=0.001,
             L1_reg=0.00,
             L2_reg=0.00001,
             attention_reg=0.1,
             n_rec = 10,
             TIME_LENGTH = 10,
             NUM_OF_CHARS = 10, EPOCHS_NUM=8, USE_ATTENTION='y', momentum = 0.9, RECONSTRUCTOR_LSTM='y', N_EMB=50):
          general_attention = shared(numpy.cast['float32'](0.98))
          train_rnn(locals(), None, None, general_attention)


def train_rnn(p, pReader, pReconstructor, general_attention):
        setup = p
        setup['__file__'] = __file__

        initialOutput(p, 'train_rnn')

	rng = numpy.random.RandomState(1234)

        defaultInput = generateDefaultInput(p['NUM_OF_CHARS'])
        
        trng = T.shared_randomstreams.RandomStreams(1234)

	reader = RecurrentNetworkReader(
		rng=rng,trng = trng,
		n_ex_in = p['NUM_OF_CHARS'], 
		n_ex_out = p['NUM_OF_CHARS'], 
		n_rec = p['n_rec'], USE_ATTENTION = p['USE_ATTENTION'], defaultInput = shared(defaultInput), general_attention = general_attention, parameterSetting = pReader, n_emb = setup['N_EMB']
	)
 
        # the input list variables
        inputLists = []
        #noiseVariables = []
        SOFInputList = numpy.zeros((p['NUM_OF_CHARS'],1), dtype=theano.config.floatX)
	SOFInputList[p['NUM_OF_CHARS']-1] = 1
        for i in range(0,p['TIME_LENGTH']):
             newInputList = shared(SOFInputList, name='inp '+str(i)) 
             #newNoiseVariable = shared(numpy.cast['float32'](0.0))
             reader.addTimeStep(newInputList)
             inputLists.append(newInputList)
             #noiseVariables.append(newNoiseVariable)
	     

 
	if(p['RECONSTRUCTOR_LSTM'] == 'y'):
		reconstructor = RecurrentNetworkReconstructorLSTM(
			rng=rng,
			initialRecurrentState=reader.currentHiddenVector,
			n_ex_in = p['NUM_OF_CHARS'], 
			n_ex_out = p['NUM_OF_CHARS'], 
			n_rec = p['n_rec'],parameterSetting = pReconstructor
		)
	else:
		reconstructor = RecurrentNetworkReconstructor(
			rng=rng,
			initialRecurrentState=reader.currentHiddenVector,
			n_ex_in = p['NUM_OF_CHARS'], 
			n_ex_out = p['NUM_OF_CHARS'], 
			n_rec = p['n_rec'],parameterSetting=pReconstructor
		)
	reconstructor.addTimeStep(shared(defaultInput))
	for i in range(0,p['TIME_LENGTH']):
	     #reconstructor.addTimeStep(inputLists[i])
	     reconstructor.addTimeStep(shared(defaultInput))
	#'READONCE'


        trainLSTMDenoising(setup, reader, reconstructor, inputLists, general_attention)





def trainLSTMDenoising(setup, reader, reconstructor, inputLists, general_attention):
	overall_negative_log_likelihood = shared(numpy.cast['float32'](0.0))
	for i in range(0, setup['TIME_LENGTH']):
	      overall_negative_log_likelihood += - T.sum(T.log(T.dot(reconstructor.output_ext[i].T, inputLists[i])))
        # TODO indices are wrong, so no training for first symbol
		
        TIME_LENGTH = setup['TIME_LENGTH']
        NUM_OF_CHARS = setup['NUM_OF_CHARS']

        regularizer =  setup['L1_reg'] * reader.L1 + setup['L1_reg'] * reconstructor.L1 + setup['L2_reg'] * reader.L2_sqr  + setup['L2_reg'] * reconstructor.L2_sqr


	cost = (
	    overall_negative_log_likelihood
	    + regularizer
            + setup['attention_reg'] * reader.attentionSoFar
	)

        params = reader.p.params + reconstructor.p.params

	

#       if(1 == 1):
#               gparams = T.grad(theano.gradient.grad_clip(cost, -1, 1), params)
#		updates = []
#		## Just gradient descent on cost
#		for (param, gradient) in zip(params, gparams):
#			param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
#			updates.append((param, param - setup['learning_rate']*param_update))
#			updates.append((param_update, setup['momentum']*param_update + (1. - setup['momentum'])*gradient))
        #else:

	updates = []
	## Just gradient descent on cost
	for param in params:
		param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
		updates.append((param, param - setup['learning_rate']*param_update))
		updates.append((param_update, setup['momentum']*param_update + (1. - setup['momentum'])*T.grad(theano.gradient.grad_clip(cost, -1, 1), param)))

        do_the_training(setup, reader, reconstructor, inputLists, general_attention, updates, cost, regularizer, overall_negative_log_likelihood)



import cPickle
def safeConfiguration(hashForExp, model):
        f = file(hashForExp+'.model', 'wb')
        cPickle.dump(model, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()
        print('Wrote model.')


def loadConfiguration(hashForExp):
        print('Loading file '+hashForExp+'.model')
        f = file(hashForExp+'.model', 'rb')
        model = cPickle.load(f)
        print(model)
        f.close()
        return model
