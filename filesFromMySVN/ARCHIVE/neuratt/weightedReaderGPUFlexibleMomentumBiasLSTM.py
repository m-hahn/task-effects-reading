__docformat__ = 'restructedtext en'


import os
import sys
import timeit

import numpy

import theano
import theano.tensor as T

from theano import config
exception_verbosity='high'
optimizer=None

from theano import *


import langmod





def initWeightMatrix(rng, name, columnsNum, rowsNum, rangeOfRand):
        W_values = numpy.asarray(rng.uniform(low=-rangeOfRand, high=rangeOfRand,size=(columnsNum, rowsNum)),dtype=theano.config.floatX)
        #    if activation == theano.tensor.nnet.sigmoid:
        W_values *= 4
        return theano.shared(value=W_values, name=name, borrow=True)



class RecurrentModule(object):

    def __init__(self, rng, externalInput, recurrentInput, n_ex_in, n_rec, n_ex_out, W=None, b=None,
                 activation=T.tanh):

        n_in = n_ex_in + n_rec
        n_out = n_rec + n_ex_out

        [self.W_re, self.W_er, self.W_ee, self.W_rr] = W
        [self.b_e, self.b_r] = b




# TODO range of random has to be adapted to the size of the network


class RecurrentNetwork(object):

    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, W=None, b=None):

        self.currentOutput_ext = initWeightMatrix(rng, 'out-cur-ext', n_ex_out, 1, 0.24)
        self.currentRecurrentNodes = initialRecurrentState
        self.previousOutput_ext = initWeightMatrix(rng, 'out-prev', n_ex_out, 1, 0.24)


        self.n_ex_in = n_ex_in
        self.n_ex_out = n_ex_out
        self.n_rec = n_rec
        self.time = 0
        self.rng = rng
        n_in = n_ex_in + n_rec
        n_out = n_ex_out + n_rec

        self.W_er = initWeightMatrix(rng,'W_er',n_rec, n_ex_out, numpy.sqrt(6. / (n_rec + n_ex_out))) # bei initWeightMatrix: first rownumber, then colnumber
        self.W_rr = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(6. / (n_rec + n_rec)))

        self.b_e = initWeightMatrix(rng,'b_e', n_ex_out, 1, numpy.sqrt(6. / (n_rec + n_ex_out))) #
        self.b_r = initWeightMatrix(rng,'b_r', n_rec, 1, numpy.sqrt(6. / (n_rec + n_ex_out))) #

        
        self.params = [self.W_er, self.W_rr, self.b_e, self.b_r]


        #temp1 = theano.function(inputs=[],outputs=self.W_re )
        #temp2 = theano.function(inputs=[],outputs= self.currentRecurrentNodes)

        recurrentNodes = T.fmatrix()
        externalInput = T.fmatrix()


class LSTM(object):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, W=None, b=None):

        self.currentHiddenVector = initialRecurrentState
        self.memoryCellVector = initWeightMatrix(rng,'W_er',n_rec, 1, 0.0)

        self.n_ex_in = n_ex_in
        self.n_ex_out = n_ex_out
        self.n_rec = n_rec
        self.time = 0
        self.rng = rng
        n_in = n_ex_in + n_rec
        n_out = n_ex_out + n_rec



        self.W_xi = initWeightMatrix(rng,'W_er',n_rec, n_ex_in, numpy.sqrt(0.1 / (n_rec + n_ex_in))) # bei initWeightMatrix: first rownumber, then colnumber
        self.W_hi = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))

        self.W_ci = initWeightMatrix(rng,'W_er',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec))) # bei initWeightMatrix: first rownumber, then colnumber

        self.W_xf = initWeightMatrix(rng,'W_rr',n_rec, n_ex_in, numpy.sqrt(0.1 / (n_rec + n_ex_in)))

       

        self.W_hf = initWeightMatrix(rng,'W_er',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec))) # bei initWeightMatrix: first rownumber, then colnumber

        self.W_cf = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))

        self.W_xc = initWeightMatrix(rng,'W_er',n_rec, n_ex_in, numpy.sqrt(0.1 / (n_rec + n_ex_in))) # bei initWeightMatrix: first rownumber, then colnumber

        self.W_hc = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))

       
        self.W_xo = initWeightMatrix(rng,'W_rr',n_rec, n_ex_in, numpy.sqrt(0.1 / (n_rec + n_rec)))

        self.W_ho = initWeightMatrix(rng,'W_er',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_ex_in))) # bei initWeightMatrix: first rownumber, then colnumber

        self.W_co = initWeightMatrix(rng,'W_rr',n_rec, n_rec, numpy.sqrt(0.1 / (n_rec + n_rec)))

       



        self.b_i = initWeightMatrix(rng,'b_e', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out))) #
        self.b_f = initWeightMatrix(rng,'b_r', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out))) #
	self.b_c = initWeightMatrix(rng,'b_r', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out))) #
	self.b_o = initWeightMatrix(rng,'b_r', n_rec, 1, numpy.sqrt(0.1 / (n_rec + n_ex_out)))
        
        self.params = [self.W_xi, self.W_hi, self.W_ci, self.W_xf, self.W_hf, self.W_cf, self.W_xc, self.W_hc, self.W_xo, self.W_ho, self.W_co, self.b_i, self.b_f, self.b_c, self.b_o]


        #temp1 = theano.function(inputs=[],outputs=self.W_re )
        #temp2 = theano.function(inputs=[],outputs= self.currentRecurrentNodes)

        recurrentNodes = T.fmatrix()
        externalInput = T.fmatrix()



################

class RecurrentNetworkReconstructor(RecurrentNetwork):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec):
        super(RecurrentNetworkReconstructor, self).__init__(rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec)
        self.W_re = initWeightMatrix(rng,'W_re',n_ex_in,n_rec, numpy.sqrt(6. / (n_ex_in + n_rec)))
        self.W_ee = initWeightMatrix(rng,'W_ee',n_ex_in,n_ex_out, numpy.sqrt(6. / (n_ex_in + n_ex_out)))
        self.params += [self.W_re, self.W_ee]

        # TODO compute these automatically from params
        self.L1 = (
            abs(self.W_re).sum() +
            abs(self.W_er).sum() +
            abs(self.W_ee).sum() +
            abs(self.W_rr).sum() +
            abs(self.b_e).sum() +
            abs(self.b_r).sum()
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            (self.W_re ** 2).sum() +
            (self.W_er ** 2).sum() +
            (self.W_ee ** 2).sum() +
            (self.W_rr ** 2).sum() +
            (self.b_e ** 2).sum() +
            (self.b_r ** 2).sum()
        )
        self.output_ext = []
        self.output_ext_funcs = []


    def addTimeStep(self, externalInput):
        lin_output_rec = self.b_r + T.dot(self.W_er, externalInput) + T.dot(self.W_rr, self.currentRecurrentNodes)
        lin_output_ext = self.b_e + T.dot(self.W_re, self.currentRecurrentNodes) + T.dot(self.W_ee, externalInput)
        externalOutput = T.nnet.softmax(lin_output_ext.T).T
        recurrentOutput = T.tanh(lin_output_rec)

         #newReccurentModule = RecurrentModule(self.rng, externalInput, , self.n_ex_in, self.n_rec, self.n_ex_out, W=self.W, b=self.b,activation=T.tanh)

        self.currentRecurrentNodes = recurrentOutput
        self.previousOutput_ext = self.currentOutput_ext
        #self.currentOutput_ext = externalOutput
        self.output_ext.append(externalOutput)
        self.output_ext_funcs.append(theano.function(inputs=[],outputs=externalOutput))



class RecurrentNetworkReconstructorLSTM(LSTM):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec):
        super(RecurrentNetworkReconstructorLSTM, self).__init__(rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec)


        self.W_ce = initWeightMatrix(rng,'W_ce',n_ex_out, n_rec, numpy.sqrt(0.1 / (n_ex_out + n_rec)))


        self.params += [self.W_ce]


        self.L1 = (
            shared(numpy.cast['float32'](0.0))
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            shared(numpy.cast['float32'](0.0))
        )

        self.output_ext = []
        self.output_ext_funcs = []
        

    def addTimeStep(self, externalInput):
        hiddenPrevious = self.currentHiddenVector
        cellPrevious = self.memoryCellVector

        inputCurrent = T.nnet.sigmoid(T.dot(self.W_xi, externalInput) + T.dot(self.W_hi, hiddenPrevious) + T.dot(self.W_ci, cellPrevious) + self.b_i)
        forgetCurrent = T.nnet.sigmoid(T.dot(self.W_xf, externalInput) + T.dot(self.W_hf, hiddenPrevious) + T.dot(self.W_cf, cellPrevious) + self.b_f)
        cellCurrent = forgetCurrent * cellPrevious + inputCurrent * T.tanh(T.dot(self.W_xc, externalInput) + T.dot(self.W_hc, hiddenPrevious) + self.b_c)
        outputCurrent = T.nnet.sigmoid(T.dot(self.W_xo, externalInput) + T.dot(self.W_ho, hiddenPrevious) + T.dot(self.W_co, cellCurrent) + self.b_o)
        hiddenCurrent = outputCurrent * T.tanh(cellCurrent)

        self.currentHiddenVector = hiddenCurrent
        self.memoryCellVector = cellCurrent


        # EXTERNAL OUTPUT
        externalOutput = T.nnet.softmax((T.dot(self.W_ce, cellCurrent)).T).T

        self.output_ext.append(externalOutput)
        self.output_ext_funcs.append(theano.function(inputs=[],outputs=externalOutput))


################

class RecurrentNetworkReader(LSTM):
    def __init__(self, rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec, USE_ATTENTION):
        super(RecurrentNetworkReader, self).__init__(rng, initialRecurrentState, n_ex_in, n_ex_out, n_rec)

        self.USE_ATTENTION = USE_ATTENTION



        self.attentionSoFar = shared(numpy.cast['float32'](0.0)) # will be used in the computation of 'cost'
        if USE_ATTENTION == 'y':
            self.W_att = initWeightMatrix(rng,'W_att',n_rec, n_ex_in, numpy.sqrt(6. / (n_rec + n_ex_in)))
            self.params += [self.W_att]
            # only for debugging/inspecting
            self.attention_scores = []
        else:
            self.params += []

        self.L1 = (
            0
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            0
        )
        

    def addTimeStep(self, externalInput, i):
        if self.USE_ATTENTION == 'y':
              attention = T.sum(T.nnet.sigmoid(T.dot(self.currentRecurrentNodes.T, T.dot(self.W_att, externalInput))))
              self.attention_scores.append(theano.function(inputs=[],outputs=attention))
              externalInput = attention * externalInput
              self.attentionSoFar += attention

        hiddenPrevious = self.currentHiddenVector
        cellPrevious = self.memoryCellVector

        #externalInput = shared(numpy.cast['float32'](0.0)) * externalInput

        inputCurrent = T.nnet.sigmoid(T.dot(self.W_xi, externalInput) + T.dot(self.W_hi, hiddenPrevious) + T.dot(self.W_ci, cellPrevious) + self.b_i)
        forgetCurrent = T.nnet.sigmoid(T.dot(self.W_xf, externalInput) + T.dot(self.W_hf, hiddenPrevious) + T.dot(self.W_cf, cellPrevious) + self.b_f)
        cellCurrent = forgetCurrent * cellPrevious + inputCurrent * T.tanh(T.dot(self.W_xc, externalInput) + T.dot(self.W_hc, hiddenPrevious) + self.b_c)
        outputCurrent = T.nnet.sigmoid(T.dot(self.W_xo, externalInput) + T.dot(self.W_ho, hiddenPrevious) + T.dot(self.W_co, cellCurrent) + self.b_o)
        hiddenCurrent = outputCurrent * T.tanh(cellCurrent)

        self.currentHiddenVector = hiddenCurrent
        self.memoryCellVector = cellCurrent


class BOWReconstructor(object):
    def __init__(self, rng, recurrentState,  n_ex_out, n_rec):
        
        self.W_rec = initWeightMatrix(rng,'W_rec',n_ex_out, n_rec, numpy.sqrt(6. / (n_ex_out + n_rec)))
        self.params = [self.W_rec]

        self.L1 = (
            shared(numpy.cast['float32'](0.0))
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            shared(numpy.cast['float32'](0.0))
        )

        self.output = T.nnet.sigmoid(T.dot(self.W_rec, recurrentState))


#def pushToQueue(queueArray, element):
#        for i in range(-len(queueArray)+1, 0):
#               queueArray[-i].set_value(queueArray[-i-1].get_value())
#        queueArray[0].set_value(element)





def test_rnn(hashForExperiment='exp',
             corpus='corpus7.txt',
             learning_rate=0.001,
             L1_reg=0.00,
             L2_reg=0.00001,
             attention_reg=0.1,
             n_rec = 10,
             TIME_LENGTH = 10,
             NUM_OF_CHARS = 10, EPOCHS_NUM=8, USE_ATTENTION='y', momentum = 0.9, RECONSTRUCTOR_LSTM='y', FULL = 'y'):

        print(__file__)
        print('Hash: '+hashForExperiment)
        print('Corpus: '+corpus)
        print('Learning Rate: '+str(learning_rate))
        print('L1: '+str(L1_reg))
        print('L2 '+str(L2_reg))
        print('AttentionReg: '+str(attention_reg))
        print('Recurrent Units: '+str(n_rec))
        print('Batch size: '+str(TIME_LENGTH))
        print('Alphabet size: '+str(NUM_OF_CHARS))
        print('Number of epochs: '+str(EPOCHS_NUM))
        print('Use Attention: '+USE_ATTENTION) #'y', 'n'
        print('Momentum: '+str(momentum))
        print('Reconstructor LSTM? '+str(RECONSTRUCTOR_LSTM))
        print('Full (y)/BOW? '+str(FULL))
	print '... building the model'


        
        plotLogLikelihood = open(hashForExperiment+'-logl.dat', 'w',0)

	rng = numpy.random.RandomState(1234)

        initialRecurrentStateReader =  initWeightMatrix(rng, 'rec', n_rec, 1, 0.24) 


        defaultInput = numpy.zeros((NUM_OF_CHARS,1), dtype=theano.config.floatX)
	defaultInput[NUM_OF_CHARS-1] = 1


	reader = RecurrentNetworkReader(
		rng=rng,
                initialRecurrentState=initialRecurrentStateReader,
		n_ex_in = NUM_OF_CHARS, 
		n_ex_out = NUM_OF_CHARS, 
		n_rec = n_rec, USE_ATTENTION = USE_ATTENTION
	)
        


        # the input list variables
        inputLists = []
        SOFInputList = numpy.zeros((NUM_OF_CHARS,1), dtype=theano.config.floatX)
	SOFInputList[NUM_OF_CHARS-1] = 1
        for i in range(0,TIME_LENGTH):
             newInputList = shared(SOFInputList, name='inp '+str(i)) 
             reader.addTimeStep(newInputList, i)
             inputLists.append(newInputList)

        if(FULL == 'y'):
		if(RECONSTRUCTOR_LSTM == 'y'):
			reconstructor = RecurrentNetworkReconstructorLSTM(
				rng=rng,
				initialRecurrentState=reader.currentHiddenVector,
				n_ex_in = NUM_OF_CHARS, 
				n_ex_out = NUM_OF_CHARS, 
				n_rec = n_rec,
			)
		else:
			reconstructor = RecurrentNetworkReconstructor(
				rng=rng,
				initialRecurrentState=reader.currentHiddenVector,
				n_ex_in = NUM_OF_CHARS, 
				n_ex_out = NUM_OF_CHARS, 
				n_rec = n_rec,
			)
		reconstructor.addTimeStep(shared(defaultInput))
		for i in range(0,TIME_LENGTH):
		     #reconstructor.addTimeStep(inputLists[i])
		     reconstructor.addTimeStep(shared(defaultInput))
		#'READONCE'


                overall_negative_log_likelihood = shared(numpy.cast['float32'](0.0))
		for i in range(0, TIME_LENGTH):
		      overall_negative_log_likelihood += - T.sum(T.log(T.dot(reconstructor.output_ext[i].T, inputLists[i])))

		
        else:
                reconstructor = BOWReconstructor(rng=rng, recurrentState=reader.currentHiddenVector, n_ex_out = NUM_OF_CHARS, n_rec = n_rec)
                bow_vector = inputLists[0]
                for i in range(1, TIME_LENGTH):
                      bow_vector = bow_vector + inputLists[i]
		overall_negative_log_likelihood = T.sum((reconstructor.output - T.minimum(1.0, bow_vector))**2)

        regularizer =  L1_reg * reader.L1 + L1_reg * reconstructor.L1 + L2_reg * reader.L2_sqr  + L2_reg * reconstructor.L2_sqr

	cost = (
	    overall_negative_log_likelihood
	    + regularizer
            + attention_reg * reader.attentionSoFar
	)

        params = reader.params + reconstructor.params
	#gparams = [T.grad(theano.gradient.grad_clip(cost, -1, 1), param) for param in params]



        updates = []
        ## Just gradient descent on cost
        for param in params:
		param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
		updates.append((param, param - learning_rate*param_update))
		updates.append((param_update, momentum*param_update + (1. - momentum)*T.grad(theano.gradient.grad_clip(cost, -1, 1), param)))






	#updates = [
        #    (param, param - learning_rate *  gparam)
	#    for param, gparam in zip(reader.params + reconstructor.params, gparams)
	#] #+ [(initialRecurrentStateReader, reader.currentRecurrentNodes)]


	train_model = theano.function(
	    inputs=[],
	    outputs=cost, #Out(theano.sandbox.cuda.basic_ops.gpu_from_host
	    updates=updates,
            #mode='ProfileMode'
	    #givens={
		#x: train_set_x[index * batch_size: (index + 1) * batch_size],
		#y: train_set_y[index * batch_size: (index + 1) * batch_size]
	    #}
	)

        

        regularizerf = theano.function(inputs=[], outputs=regularizer)
        attention = theano.function(inputs=[], outputs=reader.attentionSoFar)
        costf = theano.function(inputs=[], outputs=cost)
        loglikelihoodf = theano.function(inputs=[], outputs=overall_negative_log_likelihood)

        if(FULL == 'n'):
              outputf = theano.function(inputs=[], outputs=reconstructor.output)

    ###############
    # TRAIN MODEL #
    ###############
	print '... training'

	(sentences, chars) = langmod.read_corpus_into_batches_no_counting(corpus, TIME_LENGTH)

        iteration = -1

        import time
        current = time.time()

        for epoch in range(0,EPOCHS_NUM):
   	        for sent in sentences:
		    if len(sent)>=TIME_LENGTH:

                        iteration+=1

			# Build Input Vector Sequence
			for i in range(0,TIME_LENGTH):
			     newInputList = numpy.zeros((NUM_OF_CHARS,1), dtype=theano.config.floatX)
			     newInputList[min(NUM_OF_CHARS-1, sent[i])] = 1
			     inputLists[i].set_value(newInputList)




#TODO previous output becomes new input

                        # NOTE all the things are recomputed for printing here...

                        if iteration%100 == 0:
                                print((time.time()-current))
                                current = time.time()

				print('-- '+str(iteration)+' --')
				print('Input '+str([chars[num] for num in sent]))
				print('Cost '+str(costf()))
                                print(str(regularizerf())+" "+str(attention()))

                                plotLogLikelihood.write(str(iteration)+'\t'+str(loglikelihoodf())+'\n')

                                if USE_ATTENTION == 'y':
   				     print('Total Attention '+str(attention()))
				     # Attention scores
				     print('Attention '+str([(chars[sent[i]], (reader.attention_scores[i])().item()) for i in range(0,TIME_LENGTH)]))

				# Predicted
				
                                
                                
					#print('Prediction '+str([chars[predicted] for i in range(0,TIME_LENGTH)]))


                                if FULL == 'y':
                                      for i in range(0,TIME_LENGTH):
					     output_layer = reconstructor.output_ext_funcs[i]()
					     predicted = numpy.argmax(output_layer)
					     if USE_ATTENTION =='y':
							print("  "+chars[min(NUM_OF_CHARS-1, sent[i])]+"  "+chars[min(NUM_OF_CHARS-1, predicted)]+"  "+str(reader.attention_scores[i]()))
					     else:
							print("  "+chars[min(NUM_OF_CHARS-1, sent[i])]+"  "+chars[min(NUM_OF_CHARS-1, predicted)]+" "+str(output_layer[predicted])+" "+str(output_layer[min(NUM_OF_CHARS-1, sent[i])]))
                                else:
                                      output = outputf()
                                      #print(output)
                                      for i in range(0,TIME_LENGTH):
                                             print("  "+chars[min(NUM_OF_CHARS-1, sent[i])]+"  "+str(output[min(NUM_OF_CHARS-1,sent[i])]))
                                             


			# Predict, and calculate error gradients
			cost = train_model()

        plotLogLikelihood.close()
