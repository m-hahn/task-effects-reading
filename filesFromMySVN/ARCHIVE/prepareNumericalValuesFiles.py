# THIS file should do: extract numerical annotation, should be in parallel to createRunningTextFromTokenizedTable

# does lower case


# TODO actually have to do tokenization correctly and consistently



##################
# as in addUnigramProb...
freqListFile = open("/disk/scratch2/s1582047/deepmind/rc-data/dailymail/questions/training3/freqList", 'r')

freqList = {}

for line in freqListFile:
     line = line.split("\t")
     if line[0] in freqList:
        #print("PROBLEM "+line[0]+" "+line[2].rstrip()+" "+str(freqList[line[0]]))
        freqList[line[0]] += float(line[2])
     else:
        freqList[line[0]] = float(line[2])
##################



import math
import os

directoryIn = "/disk/scratch2/s1582047/dundeetreebank/parts/PART2ReTokenized/"

directoryOut = "/disk/scratch2/s1582047/dundeetreebank/parts/PART2PredefinedNumerical/"

files = os.listdir("/disk/scratch2/s1582047/dundeetreebank/parts/PART2ReTokenized/")

# as in correlateAttAndSurprisalTreebank.py, should be synchronized
headings = ["LuaID", "Token", "CorpusID","Itemno","SentenceID","ID","Word","CPOS","FPOS","Head","DepRel","nFixations","Mean fixation duration per word","Total fixation duration per word","First fixation duration on every word","Fixation prob","nRefixations","nRegressions to","nRegressions from","Total regression from duration","Total regresssion to duration","nLong regressions from","nLong regressions to","n-1 fix prob","n+1 fix prob","n-2 fix prob","n+2 fix prob","Re-read prob","n-1 fix dur","n+1 fix dur","n+2 fix dur","n-2 fix dur","Participant","Frequency","First pass dur","Word length","WORD","TEXT","LINE","OLEN","WLEN","XPOS","WNUM","FDUR","OBLP","WDLP","FXNO","TXFR",         "C FILE", "C SUBJ", "C WNUM","C REGOUTN","C OLEN","C WLEN","C LDIST","C PREVFIX","C OBJLPOS","C WDLPOS","C TXFR","C FIXNO","C TOTDUR","C FFIXDUR","C FPASSD","C POS","C MPOS","C BIGRP","C BACKTRANS","C WNOS","C MITIC","C DEP","C FREQBIN","C LEXSURP","C ULXSPR","C WORD","C FRQLAST","C TRACEIC","C NBIGR","C BACKTR","C REGRESS","C ULXSPRS","C GOPAST","C LASTFIX"]


def getNumberOfColumn(colname):
    index =  headings.index(colname)
#    print(index)
    return index


for fileName in files:
         f = open(directoryIn+"/"+fileName, 'r')      

         fOut = open(directoryOut+"/"+fileName, 'w')      

         counter = 0
         for line in f:
             counter = counter+1
             line = line.rstrip().split("\t")

             print(len(line))


             if len(line) < 4:
                continue
               
             word = line[1].lower()
             if len(line) < 50:
                 fixno = "-1"
                 fpassdur = "-1"
             else:
                fixno = line[getNumberOfColumn("C FIXNO")]
                fpassdur = line[getNumberOfColumn("C TOTDUR")]
                print(getNumberOfColumn("C TOTDUR"))
                print(fpassdur+"  "+fixno)
                print(line[60:70])
                print(line[getNumberOfColumn("C WORD")])
             if word in freqList:
                wordfreq = str(-math.log( freqList[word] ))
             else:
                print("ERROR token not in freqList  "+word)
                print(line)
                wordfreq = "20"
             wlen = str(len(word))
             fOut.write(str(counter)+"  "+word+"  "+fixno+"  "+wordfreq+"  "+wlen+"  "+fpassdur+"\n")
         f.close()
         fOut.write("\n\n")
         fOut.close()



