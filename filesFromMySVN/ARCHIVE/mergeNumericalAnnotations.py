import math
import os

directoryInA = "/disk/scratch2/s1582047/dundeetreebank/parts/PART2PredefinedNumerical/"
suffixA = ""

# part 2
directoryInB = "/disk/scratch2/s1582047/dundeetreebank/parts/PART2/annotation/surp-pg-test-combined-50-1000-0.7-100encoder-1-full-att/"
# part 1
#directoryInB = "/disk/scratch2/s1582047/dundeetreebank/parts/PART1/annotation/surp-pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-full-att/"

suffixB = ".surp.anno"
# att-surp-uni-dundee-pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-full-att_pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full-full-att.csv

directoryOut = "/disk/scratch2/s1582047/dundeetreebank/parts/PART2PredefinedNumericalWithSurp/"

files = os.listdir("/disk/scratch2/s1582047/dundeetreebank/parts/PART2ReTokenized/")


def getAnnotation(f):
    anno = []
    for line in f:
        line = line.rstrip()
        if len(line) > 5:
            anno.append(line)
    return anno


def makeSureSameLength(l1, l2):
   if len(l1) < len(l2):
      l1 = l1 + ["  ".join(["-1"]*len(l2[0].split("  ")))]*(len(l2) - len(l1))
   return l1

for fileName in files:
         fA = open(directoryInA+"/"+fileName+suffixA, 'r')
         annoA = getAnnotation(fA)

         fB = open(directoryInB+"/"+fileName+suffixB, 'r')
         annoB = getAnnotation(fB)

         fOut = open(directoryOut+"/"+fileName, 'w')      

#         print(len(annoA))
 #        print(len(annoB))

         annoA = makeSureSameLength(annoA, annoB)
         annoB = makeSureSameLength(annoB, annoA)

         for entry in range(1, len(annoA)):
             print(annoA[entry]+"  "+annoB[entry]+"\n")
             fOut.write(annoA[entry]+"  "+annoB[entry]+"\n")
         fA.close()
         fB.close()
         fOut.write("\n\n")
         fOut.close()



