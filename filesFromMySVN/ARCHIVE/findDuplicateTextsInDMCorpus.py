urls = {}
counter = 0.0
membersOfDuplicates = 0.0
with open("/afs/cs.stanford.edu/u/mhahn/scr/cnn-index.txt","r") as inFile:
  fileList = inFile.readlines()
  for fileName in fileList:
    counter += 1
    if counter % 1000 == 0:
      print(counter/380298.0)
    fileName = fileName.rstrip()
    if len(fileName) > 0:
       with open("/u/nlp/data/deepmind-qa/cnn/train/"+fileName) as dataPoint:
           url = dataPoint.readline()
           dataPoint.readline()
           text = dataPoint.readline()
           if len(text.split(" ")) < 500:
            if not(url in urls):
              urls[url] = [fileName]
            else:
              membersOfDuplicates += 1
              urls[url].append(fileName)
              print(membersOfDuplicates)
    if membersOfDuplicates > 100:
       break
 #            print(urls[url])

with open("duplicates-cnn-list.txt","w") as fOut:
  with open("duplicates-cnn-with-urls","w") as fOut2:
    for url in urls:
      if len(urls[url]) > 1:
        for doc in urls[url]:
          print >> fOut, doc.rstrip()
          print >> fOut2, doc.rstrip()+"\t"+url.rstrip()
