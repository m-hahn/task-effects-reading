from random import randint

FILE_LIST_PATH = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-joint-index.txt"

with open(FILE_LIST_PATH,"r") as fileHandle:
   file_list = fileHandle.read().split("\n")

dailymail = filter(lambda x : "dailymail" in x, file_list)
cnn = filter(lambda x : "_cnn_" in x, file_list)

###########
others = filter(lambda x : not("dailymail" in x) and not("cnn" in x), file_list)
if len(others) > 0:
  assert(len(others) == 1 and others[0] == '')
else:
  assert(len(dailymail) + len(cnn) == len(file_list))
##########

fileList = []

for corpus in [dailymail,cnn]:
   for i in range(0,500):
      fileList.append(corpus[randint(0,len(corpus)-1)])


fileOut = open("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-sample-dev.txt","w")
print >> fileOut, "\n".join(fileList)+"\n"
fileOut.close()
