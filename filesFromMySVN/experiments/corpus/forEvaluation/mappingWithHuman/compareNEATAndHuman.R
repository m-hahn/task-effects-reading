library(tidyr)
library(dplyr)
human = read.csv("/home/user/CS_SCR/mappingWithHuman.tsv", sep="\t")
model = read.csv("/home/user/CS_SCR/NEURAL_ATTENTION_TASK/OUTPUTS_TEXTS/merged.tsv", sep="\t")
model = model %>% mutate(FixationProb = 1/(1+exp(-AttentionLogit)))
model_ = model %>% group_by(Text, Position, Word, IsCorrectAnswer, Condition, IsOOV, IsNamedEntity, LogWordFreq) %>% summarise(FixationProb = mean(FixationProb))
joint = merge(human, model_ %>% mutate(TextFileName=Text, AnonymizedPosition = Position), by=c("Condition", "TextFileName", "AnonymizedPosition"))

library(lme4)

# Predictors
# - LogWordFreq
# - WordLength
# - IsNamedEntity
# - PositionText
# - Surprisal


# PROBLEM: Participant labels across conditions




external = read.csv("/home/user/CS_SCR/mappingWithExternal.tsv", sep="\t") %>% select(TextFileName, JointPosition, AnonymizedToken, ExperimentTokenLength)
joint = merge(joint, external, by=c("TextFileName", "JointPosition", "AnonymizedToken"))



joint$ID = paste0(joint$TextFileName, joint$JointPosition)


summary(lmer(tt ~ FixationProb + ExperimentTokenLength + LogWordFreq + IsNamedEntity + IsCorrectAnswer+ HumanPosition + (1|Participant) +(1|ID), data=joint %>% filter(fp > 0)))


data = read.csv("../../aggregated_data_csv/data-full.csv")


data_ = data %>% group_by(TextNo, JointPosition) %>% summarise(Surprisal = mean(Surprisal, na.rm=TRUE))

joint_ = merge(joint, data_, by=c("TextNo", "JointPosition"), all.x=TRUE)


summary(lmer(tt ~ FixationProb + ExperimentTokenLength + LogWordFreq + IsNamedEntity + IsCorrectAnswer+ HumanPosition + Surprisal + (1|Participant) +(1|ID), data=joint_ %>% filter(fp > 0)))


#data$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = data, na.action=na.exclude))
#data$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = data, na.action=na.exclude))
#data$IsCorrectAnswer.Resid = resid(lm(IsCorrectAnswer ~ IsNamedEntity.Centered, data = data, na.action=na.exclude))



