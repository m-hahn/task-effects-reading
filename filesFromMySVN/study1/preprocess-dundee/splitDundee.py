
import os

fileIn = open("/disk/scratch2/s1582047/dundeetreebank/dundeegoldann_allreaders_with_measures.csv", 'r')


directoryOutTexts = "/disk/scratch2/s1582047/dundeetreebank/parts"
fileCounter = 0

f = 0

last = ""
for line in fileIn:
         lineList = line.split("\t")
         text = lineList[1]
         subject = lineList[30]
         element = text+"-"+subject
         if element != last:
             print(element)
             if f != 0:
                  f.close()
             last = element
             f = open(directoryOutTexts+"/"+"dundee-"+element, 'w')      
          
         f.write(line)

f.close()

# Note at the end of a file, the last out handle will not be closed. this may generate problems downstream when they are opened and it is assumed the last one is still wellformed and complete


