# THIS file should do: create the running text from which the numerified version is created

# does lower case


# TODO actually have to do tokenization correctly and consistently


import os

directoryIn = "/disk/scratch2/s1582047/dundeetreebank/parts/PART2ReTokenized/"

directoryOut = "/disk/scratch2/s1582047/dundeetreebank/parts/PART2Text/"

directoryOutTagged = "/disk/scratch2/s1582047/dundeetreebank/parts/PART2TextTagged/"

files = os.listdir("/disk/scratch2/s1582047/dundeetreebank/parts/PART2ReTokenized/")

for fileName in files:
         f = open(directoryIn+"/"+fileName, 'r')      

         fOut = open(directoryOut+"/"+fileName, 'w')      
         fOut.write("\n\n")

         fOutTagged = open(directoryOutTagged+"/"+fileName, 'w')      
         fOutTagged.write("\n\n")


         for line in f:
             line = line.rstrip().split("\t")
             if len(line) < 4:
                continue
             word = line[1].lower()
             postag = line[7]
             fOutTagged.write(word+"/"+postag+" ")
             fOut.write(word+" ")
         f.close()
         fOut.write("\n\n")
         fOutTagged.write("\n\n")
         fOut.close()
         fOutTagged.close()

