# THIS file should do: tokenize Dundee corpus and put in the reading time scores

# (so that the table can directly be used in correlateAttAndSurprisal)
# (can later be used to generate the running text from which the numerified version is created)


# TODO actually have to do tokenization correctly and consistently

# what should be done: put it into a tokenizer
#text = nltk.word_tokenize("And this bla.")
#nltk.pos_tag(text)

import os


from nltk.tokenize import TreebankWordTokenizer


directoryIn = "/disk/scratch2/s1582047/dundeetreebank/parts/PART2"

directoryOut = "/disk/scratch2/s1582047/dundeetreebank/parts/PART2ReTokenized/"

files = os.listdir("/disk/scratch2/s1582047/dundeetreebank/parts/PART2")

for fileName in files:
         print(fileName)
# first tokenize and tag. then go through the table again and find out
         if os.path.isdir(directoryIn+"/"+fileName):
             continue
         f = open(directoryIn+"/"+fileName, 'r')      
         wordList = []
         counter = 0
         lines = []
         for line in f:
             counter = counter + 1
             lineList = line.split("\t")
             word = lineList[34]
             wordList.append(word.replace("-"," - ").replace(".", " . ").replace("'"," ' "))
             wordList.append(str(counter))
             lines.append(line)
         text = " ".join(wordList)
         tokenized = TreebankWordTokenizer().tokenize(text)
         #print(tokenized)
             #totDur = line[12]
             #fpassDur = line[14]
             #fileName = line[0]
             #postag = line[16]
             #subj = line[1]
             #wasFixated = "1"
             #if fpassDur == "0":
             #    wasFixated = "0"
             #fOut.write(word.lower()+"\t"+totDur+"\t"+fileName+"\t"+subj+"\t"+postag+"\t"+fpassDur+"\t"+wasFixated+"\n")
#             fOut.write(word.lower()+"\t"+line+"\n")
         f.close()
  #       fOut.write("\n\n")

         defaultLine = "\t-1"*(len(lines[0].split("\t")))+"\n"
         fOut = open(directoryOut+"/"+fileName, 'w')      
         counter = 1
         newCounter = 0
         isTheFirstOne = True
         for word in tokenized:
             newCounter = newCounter + 1
             line = str(newCounter)+"\t"+word
             #print(counter)
             #print(word+"...")
             #print(word == str(counter))
             if word == str(counter):
                newCounter = newCounter - 1
                isTheFirstOne = True
                counter = counter + 1
                continue
             if isTheFirstOne:
                line = line+"\t"+lines[counter-1]
             else:
                line = line+defaultLine
             #print(line)
             isTheFirstOne = False
             #if counter == 20:
             #  crash()
             fOut.write(line)
         fOut.write("\n\n")


