






VERA_CORPUS = "/disk/scratch2/s1582047/dundee/corpus+gopast"

TREEBANK = "/disk/scratch2/s1582047/dundeetreebank/dundeegoldann_allreaders.csv"

ENRICHED_TREEBANK = "/disk/scratch2/s1582047/dundeetreebank/dundeegoldann_allreaders_with_measures.csv"





veraCorpus = open(VERA_CORPUS, 'r')

corpusEntries = {}
headerCorpus = ""
counter = 0
for line in veraCorpus:
   line = line.rstrip()
   if len(line) < 10:
      continue
   if counter == 0:
      headerCorpus = line
   else:
      lineList = line.split("\t")
      fileNum = lineList[0]
      subjName = lineList[1]
      wnum = lineList[2]
      if fileNum.startswith("0"):
           fileNum = fileNum[1:]
      key = (fileNum, subjName, wnum)
      #print(key)
      corpusEntries[key] = line
   counter = counter+1


headerCorpus = "\t"+headerCorpus
headerCorpus = headerCorpus.replace("\t", "\tC ")

veraCorpus.close()

treebank = open(TREEBANK, 'r')
enrichedTreebank = open(ENRICHED_TREEBANK, 'w')

noCorrespondingThing = 0
success = 0

used = {}

defaultString = "\t" * (len(headerCorpus.split("\t"))-2)

counter = 0
for line in treebank:
    line = line.rstrip()
    if len(line) < 10:
      continue
    if counter == 0:
      enrichedTreebank.write(line+headerCorpus+"\n")
    else:
      lineList = line.split("\t")
      fileNum = lineList[1].split(".")[0]
      subjName = lineList[30]
      #print(str((lineList[38],lineList[39],lineList[40],lineList[41],lineList[42],lineList[43],lineList[44],lineList[45])))
      wnum = lineList[40].split(".")[0]
      key = (fileNum, subjName, wnum)
      #print("."+str(key)+".")
      if key in corpusEntries:
          corpusLine = corpusEntries[key]
          used[key] = 1
          success = success + 1
      else:
          #print("&")
          corpusLine = defaultString
          noCorrespondingThing = noCorrespondingThing + 1
          #print(lineList[9])
      enrichedTreebank.write(line+"\t"+corpusLine+"\n")
      #print(len((line+corpusLine).split("\t")))
    counter = counter+1



print("Nothing")
print(noCorrespondingThing)
print(success)

print("Unused")
print(len(used) - len(corpusEntries))

treebank.close()
enrichedTreebank.close()

for key in corpusEntries:
   if (not (key in used)):
      print(key)
      line = corpusEntries[key]
      if line.split("\t")[6] != "NA":
          print(line)


