Preparing the Dundee Corpus

* `mergeDundeeCorporaNew.py`

Predicting reading times:
* `analyze/compare_NEAT_Human/reading-times/combineResults.py`
* `analyze/compare_NEAT_Human/reading-times/compareNEATHuman4b_Surprisals_FullSurp.R`
* `analyze/compare_NEAT_Human/reading-times/compareNEATHuman4b_Surprisals.R`
* `analyze/compare_NEAT_Human/reading-times/compareNEATHuman4b_Surprisals_RandomSurp.R`

Predicting fixations:
* `analyze/compare_NEAT_Human/accuracy/compareNEATHuman4b_accuracy.R`
* `analyze/compare_NEAT_Human/accuracy/compareWithoutCovariates3.R`
* `analyze/compare_NEAT_Human/accuracy/compareWithoutCovariates.R`
* `analyze/compare_NEAT_Human/fixations/compareNEATHuman4b_1_Merged2.R`
* `analyze/compare_NEAT_Human/fixations/compareNEATHuman4b_1_Merged.R`
* `analyze/compare_NEAT_Human/fixations/compareNEATHuman4b_1.R`
* `analyze/compare_NEAT_Human/fixations/compareNEATHuman4b_2.R`
* `analyze/compare_NEAT_Human/fixations/compareNEATHuman4b.R`
* `analyze/compare_NEAT_Human/fixations/compareNEATHuman.R`
* `analyze/compareNEATHuman4b_1.R`

Heatmaps:
* `analyze/compare_NEAT_Human/heatmaps/compareNEATHuman4b_heatmap_human.R`
* `analyze/compare_NEAT_Human/heatmaps/compareNEATHuman4b_heatmap.R`

Analyzing NEAT predictions:
* `analyze/analyze_NEAT/analyzeNEATPredictions4.R`

