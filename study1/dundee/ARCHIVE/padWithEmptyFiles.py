import os
import sys
BATCH_SIZE = int(sys.argv[1])
SEQ_LENGTH = int(sys.argv[2])

DIR = sys.argv[3]


files = os.listdir(DIR)

print("HAVE YOU MADE SURE THAT FILES STARTING WITH PADDING WILL COME AT THE END IN THE FILELIST?")

for i in range(1, BATCH_SIZE+10):
  f = open(DIR+"/PADDING"+str(i), 'w')
  for j in range(1, SEQ_LENGTH+5):
      f.write("1\n")
  f.close()
