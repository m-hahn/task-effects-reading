# THIS file should do: create the running text from which the numerified version is created

# does lower case


# TODO actually have to do tokenization correctly and consistently


import os

import config.tokenizedPart2 as directoryIn
import config.textPart2 as directoryOut
import config.taggedPart2 as directoryOutTagged

files = os.listdir(directoryIn)

for fileName in files:
         f = open(directoryIn+"/"+fileName, 'r')      

         fOut = open(directoryOut+"/"+fileName, 'w')      
         fOut.write("\n\n")

         fOutTagged = open(directoryOutTagged+"/"+fileName, 'w')      
         fOutTagged.write("\n\n")


         for line in f:
             line = line.rstrip().split("\t")
             if len(line) < 4:
                continue
             word = line[1].lower()
             postag = line[7]
             fOutTagged.write(word+"/"+postag+" ")
             fOut.write(word+" ")
         f.close()
         fOut.write("\n\n")
         fOutTagged.write("\n\n")
         fOut.close()
         fOutTagged.close()

