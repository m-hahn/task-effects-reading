human = read.csv("~/scr/Dundee/ReadingAndPOS.csv", sep="\t")
MODEL = "92479930"
data = read.csv(paste("/u/scr/mhahn/Dundee-annotated/run_attention2_dundee.py_", MODEL, sep=""), sep="\t", header=FALSE)
names(data) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "NEAT_Surprisal", "AttProbability", "Attended")
surprisal = read.csv("/u/scr/mhahn/Dundee-annotated/run_lm_dundee.py_42636007", sep="\t", header=FALSE)
names(surprisal) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "Surprisal")
data = merge(data, surprisal, by=c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab"))


#accuracy_train_attention2.py_19930371.txt
#accuracy_train_attention2.py_2173037.txt
#accuracy_train_attention2.py_39079559.txt
#accuracy_train_attention2.py_92479930.txt

library(tidyr)
library(dplyr)

wordfreq = read.csv("../../python/vocabularies/cnn_nonAnonymized_50000.txt", sep="\t", header=FALSE, quote=NULL)
names(wordfreq) = c("WordID", "Token", "WordFreq")

data = merge(data, wordfreq, by=c("Token"))

data = data %>% filter(InVocab == "InVocab") %>% group_by(Itemno, WNUM) %>% summarise(AttProbability = max(AttProbability), NEAT_Surprisal=sum(NEAT_Surprisal), WordFreq=min(WordFreq), Surprisal=sum(Surprisal))
data$LogWordFreq = log(data$WordFreq)

data = merge(human, data, by=c("Itemno", "WNUM"), all=TRUE)
data$WordLength = unlist(lapply(as.character(data$WORD), FUN=nchar))
library(lme4)
data = data %>% mutate(ItemID = paste0(Itemno, "_", WNUM))
data$FIXATED = (data$FPASSD > 0)

#summary(lmer(FPASSD ~ WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED)))

data = data %>% mutate(WordLength = (WordLength-mean(WordLength, na.rm=TRUE))/sd(WordLength, na.rm=TRUE))
data = data %>% mutate(LogWordFreq = (LogWordFreq-mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(WNUM.C = (WNUM-mean(WNUM, na.rm=TRUE))/sd(WNUM, na.rm=TRUE))
data = data %>% mutate(AttProbability = (AttProbability-mean(AttProbability, na.rm=TRUE))/sd(AttProbability, na.rm=TRUE))
data = data %>% mutate(Surprisal = (Surprisal-mean(Surprisal, na.rm=TRUE))/sd(Surprisal, na.rm=TRUE))


data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$AttProbability.Resid = residuals(lm(AttProbability ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal, data=data, na.action=na.exclude))


#model0 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
#model1 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
model2 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))

model3 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED)))
data$LOG_TOTDUR = log(data$TOTDUR)
model4 = (lmer(TOTDUR ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability + (1|SUBJ) + (1|ItemID), data=data %>% filter(TOTDUR>0))) 
model5 = (lmer(LOG_TOTDUR ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability + (1|SUBJ) + (1|ItemID), data=data %>% filter(TOTDUR>0))) 
data$LOG_TOTDUR = log(data$TOTDUR)
model6 = (lmer(LOG_TOTDUR ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(TOTDUR>0)))

#crash()

data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$AttProbability.Resid = residuals(lm(AttProbability ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal, data=data, na.action=na.exclude))

model = (glm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid, data=data, family="binomial")) 

#
#library(brms)
#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data, family="bernoulli", cores=4)) 
#

