human = read.csv("~/scr/Dundee/ReadingAndPOS.csv", sep="\t")
data = read.csv("/u/scr/mhahn/Dundee-annotated/run_attention3_dundee.py_71366487", sep="\t", header=FALSE)
names(data) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "NEAT_Surprisal", "AttProbability", "Attended")
surprisal = read.csv("/u/scr/mhahn/Dundee-annotated/run_lm_dundee.py_42636007", sep="\t", header=FALSE)
names(surprisal) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "Surprisal")
data = merge(data, surprisal, by=c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab"))

library(tidyr)
library(dplyr)

wordfreq = read.csv("../../python/vocabularies/cnn_nonAnonymized_50000.txt", sep="\t", header=FALSE, quote=NULL)
names(wordfreq) = c("WordID", "Token", "WordFreq")

data = merge(data, wordfreq, by=c("Token"))

data = data %>% filter(InVocab == "InVocab") %>% group_by(Itemno, WNUM) %>% summarise(AttProbability = max(AttProbability), NEAT_Surprisal=sum(NEAT_Surprisal), WordFreq=min(WordFreq), Surprisal=sum(Surprisal))
data$LogWordFreq = log(data$WordFreq)

data = merge(human, data, by=c("Itemno", "WNUM"), all=TRUE)


data$WordLength = unlist(lapply(as.character(data$WORD), FUN=nchar))



library(lme4)


data = data %>% mutate(ItemID = paste0(Itemno, "_", WNUM))

data$FIXATED = (data$FPASSD > 0)

#summary(lmer(FPASSD ~ WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED)))

data = data %>% mutate(WordLength = (WordLength-mean(WordLength, na.rm=TRUE))/sd(WordLength, na.rm=TRUE))
data = data %>% mutate(LogWordFreq = (LogWordFreq-mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(WNUM.C = (WNUM-mean(WNUM, na.rm=TRUE))/sd(WNUM, na.rm=TRUE))
data = data %>% mutate(AttProbability = (AttProbability-mean(AttProbability, na.rm=TRUE))/sd(AttProbability, na.rm=TRUE))
data = data %>% mutate(Surprisal = (Surprisal-mean(Surprisal, na.rm=TRUE))/sd(Surprisal, na.rm=TRUE))


data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$AttProbability.Resid = residuals(lm(AttProbability ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal, data=data, na.action=na.exclude))


model0 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
model1 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
model2 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))

crash()

#                       Estimate Std. Error t value
#(Intercept)            232.6186     8.2648  28.146
#WordLength              31.6413     0.4231  74.778
#LogWordFreq             -3.0582     0.4073  -7.509
#WNUM.C                  -2.3180     0.3130  -7.407
#AttProbability.Resid    -9.6607     0.4733 -20.413
#Surprisal.Resid         11.5372     0.4265  27.051
#WordLength:LogWordFreq  -1.0265     0.3140  -3.269
#LogWordFreq:WNUM.C      -0.8417     0.3970  -2.120
#WordLength:WNUM.C       -2.9569     0.4025  -7.346



anova(model1, model0)
#       Df     AIC     BIC   logLik deviance  Chisq Chi Df Pr(>Chisq)    
#model0 10 2762398 2762502 -1381189  2762378                             
#model1 11 2761669 2761783 -1380824  2761647 731.11      1  < 2.2e-16 ***
anova(model2, model1)
#       Df     AIC     BIC   logLik deviance  Chisq Chi Df Pr(>Chisq)    
#model1 11 2761669 2761783 -1380824  2761647                             
#model2 12 2761259 2761382 -1380617  2761235 412.73      1  < 2.2e-16 ***



#library(brms)
#model = (brm(FIXATED ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data, family="bernoulli", cores=4))

#summary(glmer(FIXATED ~ WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data, family="binomial"))
library(brms)
library(lme4)
#model = (brm(FIXATED ~ WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data, family="bernoulli", cores=4))

# Wlen*LWordFreq, Pos*LWordFreq, pos*WLen




#model = (brm(FIXATED ~ WordLength*LogWordFreq + WNUM*LogWordFreq + WNUM*WordLength + WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data, family="bernoulli", cores=4)) 


# model = (brm(FIXATED ~ WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data, family="bernoulli", cores=4))         



#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data, family="bernoulli", cores=4)) 


data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$AttProbability.Resid = residuals(lm(AttProbability ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal, data=data, na.action=na.exclude))

#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data, family="bernoulli", cores=4)) 
#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1|SUBJ), data=data, family="bernoulli", cores=4)) 
#model = (glm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid, data=data, family="binomial")) 
#                         Estimate Std. Error z value Pr(>|z|)    
#(Intercept)             4.203e-01  7.412e-03  56.705  < 2e-16 ***
#WordLength              8.539e-01  6.037e-03 141.445  < 2e-16 ***
#LogWordFreq            -2.450e-01  5.052e-03 -48.499  < 2e-16 ***
#WNUM                   -1.480e-05  4.592e-06  -3.223  0.00127 ** 
#AttProbability.Resid    5.673e-03  5.347e-03   1.061  0.28869    
#Surprisal.Resid         8.991e-03  5.149e-03   1.746  0.08081 .  
#WordLength:LogWordFreq  2.504e-01  4.362e-03  57.405  < 2e-16 ***



#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data, family="bernoulli", cores=4)) 
#model = (glmer(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data, family="binomial")) 
#model = (glm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid, data=data, family="binomial")) 
#                        Estimate Std. Error z value Pr(>|z|)    
#(Intercept)             0.401694   0.004409  91.108  < 2e-16 ***
#WordLength              0.854274   0.006029 141.699  < 2e-16 ***
#LogWordFreq            -0.244665   0.005045 -48.501  < 2e-16 ***
#WNUM.C                 -0.011032   0.003422  -3.224  0.00127 ** 
#Surprisal.Resid         0.008965   0.005150   1.741  0.08171 .  
#WordLength:LogWordFreq  0.250950   0.004331  57.944  < 2e-16 ***

model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + (1|SUBJ) , data=data, family="bernoulli", cores=4))     

#                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  0.40      0.19     0.03     0.79        586 1.01
#WordLength                 0.90      0.01     0.88     0.91       2602 1.00
#LogWordFreq               -0.26      0.01    -0.27    -0.25       2707 1.00
#WNUM.C                    -0.01      0.00    -0.02    -0.01       4000 1.00
#Surprisal.Resid            0.01      0.01    -0.00     0.02       3648 1.00
#WordLength:LogWordFreq     0.26      0.00     0.25     0.27       3532 1.00


#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1|SUBJ), data=data, family="bernoulli", cores=4))
# Doesn't converge. Need to change model
