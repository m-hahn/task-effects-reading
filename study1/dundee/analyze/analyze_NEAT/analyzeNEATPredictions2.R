MODEL = "11730083"
data = read.csv(paste("/u/scr/mhahn/Dundee-annotated/run_attention2_dundee.py_", MODEL, sep=""), sep="\t", header=FALSE)
names(data) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "NEAT_Surprisal", "AttProbability", "Attended")
surprisal = read.csv("/u/scr/mhahn/Dundee-annotated/run_lm_dundee.py_42636007", sep="\t", header=FALSE)
names(surprisal) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "Surprisal")
data = merge(data, surprisal, by=c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab"))

library(tidyr)
library(dplyr)

wordfreq = read.csv("../../python/vocabularies/cnn_nonAnonymized_50000.txt", sep="\t", header=FALSE, quote=NULL)
names(wordfreq) = c("WordID", "Token", "WordFreq")

data = merge(data, wordfreq, by=c("Token"))

data = data %>% filter(InVocab == "InVocab") %>% group_by(Itemno, WNUM, Token) %>% summarise(AttProbability = max(AttProbability), NEAT_Surprisal=sum(NEAT_Surprisal), WordFreq=min(WordFreq), Surprisal=sum(Surprisal), Attended = max(Attended))
data$LogWordFreq = log(data$WordFreq)



data$WordLength = unlist(lapply(as.character(data$Token), FUN=nchar))



library(lme4)


data = data %>% mutate(ItemID = paste0(Itemno, "_", WNUM))


data = data %>% mutate(WordLength = (WordLength-mean(WordLength, na.rm=TRUE))/sd(WordLength, na.rm=TRUE))
data = data %>% mutate(LogWordFreq = (LogWordFreq-mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(WNUM.C = (WNUM-mean(WNUM, na.rm=TRUE))/sd(WNUM, na.rm=TRUE))
data = data %>% mutate(Surprisal = (Surprisal-mean(Surprisal, na.rm=TRUE))/sd(Surprisal, na.rm=TRUE))


data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$WordLength.Resid = residuals(lm(WordLength ~ LogWordFreq, data=data, na.action=na.exclude))


summary(glmer(Attended ~ WordLength.Resid + LogWordFreq +  Surprisal.Resid + (1|ItemID), family="binomial", data=data))


summary(lmer(AttProbability ~ WordLength.Resid + LogWordFreq +  Surprisal.Resid + (1|ItemID), data=data))
#                  Estimate Std. Error t value
#(Intercept)       0.526224   0.001605  327.81
#WordLength.Resid  0.206695   0.007143   28.94
#LogWordFreq      -0.537143   0.002244 -239.41
#Surprisal.Resid   0.058494   0.003244   18.03


summary(lmer(AttProbability ~ LogWordFreq*WordLength.Resid + LogWordFreq +  Surprisal.Resid + (1|ItemID), data=data))




