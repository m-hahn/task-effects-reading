Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: FFIXDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) +      (1 | ItemID)
   Data: data %>% filter(FIXATED)

     AIC      BIC   logLik deviance df.resid 
 2542095  2542198 -1271037  2542075   223458 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-3.0209 -0.5663 -0.0756  0.4564 15.3829 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  278.6   16.69   
 SUBJ     (Intercept)  327.8   18.11   
 Residual             4857.4   69.70   
Number of obs: 223468, groups:  ItemID, 45621; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            200.8391     5.7293  35.055
WordLength               3.5376     0.2350  15.054
LogWordFreq             -4.6625     0.2274 -20.503
WNUM.C                  -0.6525     0.1778  -3.669
WordLength:LogWordFreq   0.3652     0.1750   2.087
LogWordFreq:WNUM.C      -0.5365     0.2225  -2.412
WordLength:WNUM.C       -0.7845     0.2246  -3.493

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C WL:LWF LWF:WN
WordLength   0.002                                   
LogWordFreq  0.002  0.549                            
WNUM.C       0.000  0.004  0.012                     
WrdLngt:LWF  0.016  0.292 -0.165 -0.003              
LgWF:WNUM.C  0.000  0.009  0.006  0.137  0.005       
WrdL:WNUM.C  0.000  0.015  0.007 -0.095  0.006  0.631
Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: FFIXDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid +      (1 | SUBJ) + (1 | ItemID)
   Data: data %>% filter(FIXATED)

     AIC      BIC   logLik deviance df.resid 
 2541684  2541798 -1270831  2541662   223457 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-3.0424 -0.5660 -0.0746  0.4567 15.3915 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  266.0   16.31   
 SUBJ     (Intercept)  327.9   18.11   
 Residual             4857.8   69.70   
Number of obs: 223468, groups:  ItemID, 45621; SUBJ, 10

Fixed effects:
                        Estimate Std. Error t value
(Intercept)            200.72871    5.72953  35.034
WordLength               2.81316    0.23629  11.906
LogWordFreq             -5.13671    0.22732 -22.597
WNUM.C                  -0.64142    0.17693  -3.625
Surprisal.Resid          1.54237    0.07572  20.369
WordLength:LogWordFreq   0.33900    0.17393   1.949
LogWordFreq:WNUM.C      -0.55218    0.22117  -2.497
WordLength:WNUM.C       -0.79683    0.22322  -3.570

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C Srpr.R WL:LWF LWF:WN
WordLength   0.002                                          
LogWordFreq  0.002  0.554                                   
WNUM.C       0.000  0.003  0.012                            
Surprsl.Rsd -0.001 -0.151 -0.103  0.003                     
WrdLngt:LWF  0.016  0.290 -0.165 -0.003 -0.007              
LgWF:WNUM.C  0.000  0.010  0.006  0.138 -0.004  0.006       
WrdL:WNUM.C  0.000  0.016  0.008 -0.096 -0.003  0.006  0.631
