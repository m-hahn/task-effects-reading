Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: FPASSD ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) +      (1 | ItemID)
   Data: data %>% filter(FIXATED)

     AIC      BIC   logLik deviance df.resid 
 2762398  2762502 -1381189  2762378   223458 

Scaled residuals: 
   Min     1Q Median     3Q    Max 
-5.126 -0.545 -0.149  0.331 47.571 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  1487.5   38.57  
 SUBJ     (Intercept)   613.3   24.76  
 Residual             12501.3  111.81  
Number of obs: 223468, groups:  ItemID, 45621; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            232.7084     7.8398  29.683
WordLength              32.8242     0.4275  76.785
LogWordFreq             -2.9935     0.4107  -7.289
WNUM.C                  -2.3032     0.3174  -7.256
WordLength:LogWordFreq  -0.6162     0.3175  -1.941
LogWordFreq:WNUM.C      -0.9641     0.4033  -2.390
WordLength:WNUM.C       -2.9405     0.4093  -7.184

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C WL:LWF LWF:WN
WordLength   0.004                                   
LogWordFreq  0.002  0.568                            
WNUM.C       0.000  0.004  0.013                     
WrdLngt:LWF  0.022  0.288 -0.140 -0.004              
LgWF:WNUM.C  0.001  0.008  0.005  0.124  0.004       
WrdL:WNUM.C  0.000  0.013  0.006 -0.072  0.005  0.639
Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: FPASSD ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid +      (1 | SUBJ) + (1 | ItemID)
   Data: data %>% filter(FIXATED)

     AIC      BIC   logLik deviance df.resid 
 2761761  2761875 -1380870  2761739   223457 

Scaled residuals: 
   Min     1Q Median     3Q    Max 
-5.013 -0.546 -0.148  0.331 47.537 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  1430.7   37.83  
 SUBJ     (Intercept)   613.6   24.77  
 Residual             12498.9  111.80  
Number of obs: 223468, groups:  ItemID, 45621; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            232.5568     7.8416  29.657
WordLength              31.0981     0.4296  72.386
LogWordFreq             -4.1076     0.4099 -10.021
WNUM.C                  -2.2847     0.3152  -7.249
Surprisal.Resid          3.6408     0.1434  25.389
WordLength:LogWordFreq  -0.6158     0.3150  -1.955
LogWordFreq:WNUM.C      -0.9865     0.4001  -2.465
WordLength:WNUM.C       -2.9660     0.4059  -7.307

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C Srpr.R WL:LWF LWF:WN
WordLength   0.004                                          
LogWordFreq  0.002  0.573                                   
WNUM.C       0.000  0.004  0.012                            
Surprsl.Rsd -0.001 -0.161 -0.108  0.002                     
WrdLngt:LWF  0.022  0.284 -0.141 -0.003 -0.001              
LgWF:WNUM.C  0.001  0.008  0.005  0.125 -0.003  0.005       
WrdL:WNUM.C  0.000  0.013  0.006 -0.073 -0.002  0.005  0.639
Data: data %>% filter(FIXATED)
Models:
model0: FPASSD ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C * 
model0:     WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) + 
model0:     (1 | ItemID)
model1: FPASSD ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C * 
model1:     WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + 
model1:     (1 | SUBJ) + (1 | ItemID)
       Df     AIC     BIC   logLik deviance  Chisq Chi Df Pr(>Chisq)    
model0 10 2762398 2762502 -1381189  2762378                             
model1 11 2761761 2761875 -1380870  2761739 639.22      1  < 2.2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
