library(tidyr)
library(dplyr)
human = read.csv("~/scr/Dundee/ReadingAndPOS.csv", sep="\t")
MODEL = "MERGED"
data = read.csv(paste("/u/scr/mhahn/Dundee-annotated/run_attention2_dundee.py_", MODEL, sep=""), sep="\t")

#data = data %>% filter(!(Token %in% c(".", ",", "!", "?")))
#data = data %>% filter(!(Token %in% c("n't", "'d", "'s", 'll', 're', 'clock')))

data2 = data[(2:nrow(data)),]
data3 = data[(1:nrow(data)-1),]
unique(data2[data2$WNUM == data3$WNUM,]$Token)

data2$DUPLICATED = FALSE
duplicated = data2[data2$WNUM == data3$WNUM,]
data2[data2$WNUM == data3$WNUM,]$DUPLICATED = TRUE

data$DUPLICATED = c(FALSE, data2$DUPLICATED)

data = data %>% filter(!DUPLICATED)

rows = data %>% group_by(PositionModel, Token, Itemno, WNUM, ID, InVocab) %>% summarise(rows=NROW(Attended))
print(paste("ASSERT", max(rows$rows) == min(rows$rows), "\n"))

data = data %>% group_by(PositionModel, Token, Itemno, WNUM, ID, InVocab) %>% summarise(AttProbability = mean(AttProbability), Attended = mean(Attended))


surprisal = read.csv("/u/scr/mhahn/Dundee-annotated/run_lm_dundee.py_42636007", sep="\t", header=FALSE)
names(surprisal) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "Surprisal")
data = merge(data, surprisal, by=c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab"))



wordfreq = read.csv("vocabularies/cnn_nonAnonymized_50000.txt", sep="\t", header=FALSE, quote=NULL)
names(wordfreq) = c("WordID", "Token", "WordFreq")

data = merge(data, wordfreq, by=c("Token"))

data = data %>% filter(InVocab == "InVocab") %>% group_by(Itemno, WNUM) %>% summarise(AttProbability = max(AttProbability), WordFreq=min(WordFreq), Surprisal=sum(Surprisal))
data$LogWordFreq = log(data$WordFreq)

data = merge(human, data, by=c("Itemno", "WNUM"), all=TRUE)
data$WordLength = unlist(lapply(as.character(data$WORD), FUN=nchar))
library(lme4)
data = data %>% mutate(ItemID = paste0(Itemno, "_", WNUM))
data$FIXATED = (data$FPASSD > 0)

#summary(lmer(FPASSD ~ WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED)))

data = data %>% mutate(WordLength.C = (WordLength-mean(WordLength, na.rm=TRUE))/sd(WordLength, na.rm=TRUE))
data = data %>% mutate(LogWordFreq.C = (LogWordFreq-mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(WNUM.C = (WNUM-mean(WNUM, na.rm=TRUE))/sd(WNUM, na.rm=TRUE))
data = data %>% mutate(AttProbability.C = (AttProbability-mean(AttProbability, na.rm=TRUE))/sd(AttProbability, na.rm=TRUE))
data = data %>% mutate(Surprisal.C = (Surprisal-mean(Surprisal, na.rm=TRUE))/sd(Surprisal, na.rm=TRUE))


data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$AttProbability.Resid = residuals(lm(AttProbability ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal, data=data, na.action=na.exclude))


cross_entropy = data %>% summarise(cross_entropy = -mean(FIXATED * log(AttProbability) + (1-FIXATED) * log(1-AttProbability), na.rm=TRUE))

full_data = data %>% group_by(WNUM, WORD) %>% summarise(FixationRate=mean(FIXATED, na.rm=TRUE), AttProbability=mean(AttProbability, na.rm=TRUE))
cor.test(full_data$FixationRate, full_data$AttProbability)

cutoff = quantile(data$AttProbability, 1-0.62, na.rm=TRUE)
mean(data$FIXATED == (data$AttProbability > cutoff), na.rm=TRUE)


precision = mean(data[(data$AttProbability > cutoff),]$FIXATED, na.rm=TRUE)
recall = mean(data[(data$FIXATED),]$AttProbability > cutoff, na.rm=TRUE)


f1_fix = 2*(precision*recall)/(precision+recall)



precision = mean(!data[(data$AttProbability <= cutoff),]$FIXATED, na.rm=TRUE)
recall = mean(data[(!data$FIXATED),]$AttProbability <= cutoff, na.rm=TRUE)


f1_skip = 2*(precision*recall)/(precision+recall)


cutoff_LogWordFreq = quantile(data$LogWordFreq, 0.62, na.rm=TRUE)

mean(data$FIXATED == (data$LogWordFreq < cutoff_LogWordFreq), na.rm=TRUE)


precision = mean(data[(data$LogWordFreq < cutoff_LogWordFreq),]$FIXATED, na.rm=TRUE)
recall = mean(data[(data$FIXATED),]$LogWordFreq < cutoff_LogWordFreq, na.rm=TRUE)


f1_fix = 2*(precision*recall)/(precision+recall)



precision = mean(!data[(data$LogWordFreq >= cutoff_LogWordFreq),]$FIXATED, na.rm=TRUE)
recall = mean(data[(!data$FIXATED),]$LogWordFreq >= cutoff_LogWordFreq, na.rm=TRUE)


f1_skip = 2*(precision*recall)/(precision+recall)



calculateAccuracy = function(data, predictor, sign) {
   cutoff_LogWordFreq = quantile(sign*data[[predictor]], 0.62, na.rm=TRUE)
   accuracy = mean(data$FIXATED == (sign*data[[predictor]] < cutoff_LogWordFreq), na.rm=TRUE)
   precision = mean(data[(sign*data[[predictor]] < cutoff_LogWordFreq),]$FIXATED, na.rm=TRUE)
   recall = mean(sign*data[(data$FIXATED),][[predictor]] < cutoff_LogWordFreq, na.rm=TRUE)
   f1_fix = 2*(precision*recall)/(precision+recall)
   precision = mean(!data[(sign*data[[predictor]] >= cutoff_LogWordFreq),]$FIXATED, na.rm=TRUE)
   recall = mean(data[(!data$FIXATED),][[predictor]] >= cutoff_LogWordFreq, na.rm=TRUE)
   f1_skip = 2*(precision*recall)/(precision+recall)
   print(paste(predictor, " ", accuracy, " ", f1_fix, " ", f1_skip, "\n"))
}


sink("accuracies_2.txt")
calculateAccuracy(data, "AttProbability", -1)
calculateAccuracy(data, "LogWordFreq", 1)
calculateAccuracy(data, "WordLength",-1)
calculateAccuracy(data, "Surprisal", -1)


data$RandomUniform = runif(nrow(data))
calculateAccuracy(data, "RandomUniform",1)


# Now per human subject
data_ = data.frame()
for(subject in unique(data$SUBJ)) {
  averages = data %>% filter(SUBJ != subject) %>% group_by(WNUM, WORD) %>% summarise(HumanFixationRate=mean(FIXATED, na.rm=TRUE))
  averages = merge(averages, data %>% filter(SUBJ == subject), by=c("WNUM", "WORD"))
  data_ = rbind(averages, data_)
}
calculateAccuracy(data_, "HumanFixationRate", -1)
sink()

pos = read.csv("en-ptb.map", sep="\t")
data = merge(data, pos, by=c("CPOS"), all.x=TRUE)

library(ggplot2)
plot = ggplot(data %>% group_by(UPOS) %>% summarise(FixationRate = mean(FIXATED, na.rm=TRUE)), aes(x=UPOS, y=FixationRate)) +  geom_bar(stat="identity")
ggsave(plot, file="bypos-human2.pdf")
plot = ggplot(data %>% group_by(UPOS) %>% summarise(FixationRate = mean(AttProbability > quantile(AttProbability, 1-0.62, na.rm=TRUE), na.rm=TRUE)), aes(x=UPOS, y=FixationRate)) +  geom_bar(stat="identity")
ggsave(plot, file="bypos-model2.pdf")
plot = ggplot(data %>% group_by(UPOS) %>% summarise(FixationRate = mean(LogWordFreq < quantile(LogWordFreq, 0.62, na.rm=TRUE), na.rm=TRUE)), aes(x=UPOS, y=FixationRate)) + geom_bar(stat="identity") 
ggsave(plot, file="bypos-wordfreq2.pdf")

a = data %>% group_by(UPOS) %>% summarise(FixationRate = mean(FIXATED, na.rm=TRUE)) %>% mutate(Group="Human")
b = data %>% group_by(UPOS) %>% summarise(FixationRate = mean(AttProbability > quantile(AttProbability, 1-0.62, na.rm=TRUE), na.rm=TRUE)) %>% mutate(Group="Model")
c = data %>% group_by(UPOS) %>% summarise(FixationRate = mean(LogWordFreq < quantile(LogWordFreq, 0.62, na.rm=TRUE), na.rm=TRUE)) %>% mutate(Group="LogWordFreq")
plot = ggplot(rbind(a, rbind(b,c)), aes(x=UPOS, y=FixationRate, group=Group, color=Group, fill=Group)) + geom_bar(stat="identity", position="dodge") 
ggsave(plot, file="bypos2.pdf")







