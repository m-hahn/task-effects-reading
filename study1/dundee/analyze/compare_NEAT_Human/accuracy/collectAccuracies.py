with open("accuracies_5.txt", "r") as inFile:
   data = [x.split(" ") for x in inFile.read().strip().split("\n")]
data = {x[1].replace('"', "") : [y for y in x  if len(y) > 0] for x in data}
print(data)

def display(name, column):
   print(f"{name} & {round(100*float(data[column][2]), 1)} & {round(100*float(data[column][3]), 1)} & {round(100*float(data[column][4]), 1)} \\\\")
import sys
with open("accuracies.txt", "w") as outFile: 
   sys.stdout = outFile
   display( "NEAT", "AttProbability")
   print("\\hline")
   print("\\multicolumn{4}{c}{Supervised Models} \\\\ \\hline")
   print("\\cite{nilsson_learning_2009} & 69.5 & 75.2 & 62.6 \\\\")
   print("\\cite{matthies_blinkers_2013} &  69.9 & 72.3 & 66.1 \\\\ \\hline")
   print("\\multicolumn{4}{c}{Human Performance and Baselines} \\\\ \\hline")
   display( "Random Attention", "RandomUniform")
   display( "Full Surprisal", "Surprisal")
   display( "Word Frequency", "LogWordFreq")
   display( "Word Length", "WordLength")
   display( "Human Agreement", "HumanFixationRate")

