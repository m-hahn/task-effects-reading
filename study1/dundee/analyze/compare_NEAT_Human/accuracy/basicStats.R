library(tidyr)
library(dplyr)
human = read.csv("~/scr/Dundee/ReadingAndPOS.csv", sep="\t")
MODEL = "MERGED"
data = read.csv(paste("/u/scr/mhahn/Dundee-annotated/run_attention4_dundee.py_", MODEL, sep=""), sep="\t")

mean(data$Attended, na.rm=TRUE)

