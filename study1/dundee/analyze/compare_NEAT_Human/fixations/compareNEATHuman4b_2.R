human = read.csv("~/scr/Dundee/ReadingAndPOS.csv", sep="\t")
MODEL = "25107161"
data = read.csv(paste("/u/scr/mhahn/Dundee-annotated/run_attention4_dundee.py_", MODEL, sep=""), sep="\t", header=FALSE)
names(data) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "NEAT_Surprisal", "AttProbability", "Attended")
surprisal = read.csv("/u/scr/mhahn/Dundee-annotated/run_lm_dundee.py_42636007", sep="\t", header=FALSE)
names(surprisal) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "Surprisal")
data = merge(data, surprisal, by=c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab"))


#accuracy_train_attention4.py_11730083.txt
#accuracy_train_attention4.py_17697468.txt
#accuracy_train_attention4.py_25107161.txt
#accuracy_train_attention4.py_29085010.txt
#accuracy_train_attention4.py_30409286.txt
#accuracy_train_attention4.py_57242158.txt
#accuracy_train_attention4.py_63121873.txt
#accuracy_train_attention4.py_67785982.txt

library(tidyr)
library(dplyr)

wordfreq = read.csv("../../python/vocabularies/cnn_nonAnonymized_50000.txt", sep="\t", header=FALSE, quote=NULL)
names(wordfreq) = c("WordID", "Token", "WordFreq")

data = merge(data, wordfreq, by=c("Token"))

data = data %>% filter(InVocab == "InVocab") %>% group_by(Itemno, WNUM) %>% summarise(AttProbability = max(AttProbability), NEAT_Surprisal=sum(NEAT_Surprisal), WordFreq=min(WordFreq), Surprisal=sum(Surprisal), Attended=max(Attended))
data$LogWordFreq = log(data$WordFreq)

data = merge(human, data, by=c("Itemno", "WNUM"), all=TRUE)
data$WordLength = unlist(lapply(as.character(data$WORD), FUN=nchar))
library(lme4)
data = data %>% mutate(ItemID = paste0(Itemno, "_", WNUM))
data$FIXATED = (data$FPASSD > 0)

#summary(lmer(FPASSD ~ WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED)))

data = data %>% mutate(WordLength = (WordLength-mean(WordLength, na.rm=TRUE))/sd(WordLength, na.rm=TRUE))
data = data %>% mutate(LogWordFreq = (LogWordFreq-mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(WNUM.C = (WNUM-mean(WNUM, na.rm=TRUE))/sd(WNUM, na.rm=TRUE))
data = data %>% mutate(AttProbability = (AttProbability-mean(AttProbability, na.rm=TRUE))/sd(AttProbability, na.rm=TRUE))
data = data %>% mutate(Surprisal = (Surprisal-mean(Surprisal, na.rm=TRUE))/sd(Surprisal, na.rm=TRUE))


data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$AttProbability.Resid = residuals(lm(AttProbability ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal, data=data, na.action=na.exclude))


##model0 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
##model1 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
#model2 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
#
#model3 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED)))
#data$LOG_TOTDUR = log(data$TOTDUR)
#model4 = (lmer(TOTDUR ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability + (1|SUBJ) + (1|ItemID), data=data %>% filter(TOTDUR>0))) 
#model5 = (lmer(LOG_TOTDUR ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability + (1|SUBJ) + (1|ItemID), data=data %>% filter(TOTDUR>0))) 
#data$LOG_TOTDUR = log(data$TOTDUR)
#model6 = (lmer(LOG_TOTDUR ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(TOTDUR>0)))
#
##crash()

data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$AttProbability.Resid = residuals(lm(AttProbability ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal, data=data, na.action=na.exclude))

#model = (glm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid, data=data, family="binomial")) 


library(brms)
#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data, family="bernoulli", cores=4)) 


#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 5000), family="bernoulli", cores=4)) # 9580.73 seconds (Total)

#                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  0.36      0.23    -0.10     0.82        988 1.00
#WordLength                 0.93      0.03     0.88     0.98       4000 1.00
#LogWordFreq               -0.29      0.02    -0.33    -0.24       4000 1.00
#WNUM.C                    -0.04      0.02    -0.07    -0.01       4000 1.00
#AttProbability.Resid       0.08      0.05    -0.02     0.17       2883 1.00
#Surprisal.Resid            0.01      0.02    -0.03     0.05       4000 1.00
#WordLength:LogWordFreq     0.23      0.02     0.19     0.27       4000 1.00




#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 10000), family="bernoulli", cores=4)) 

#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 10000), family="bernoulli", cores=4, prior=prior(normal(0, 10), class="b")))

#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 10000), family="bernoulli", cores=4, prior=prior(normal(0, 1), class="b")))



#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 50000), family="bernoulli", cores=4)) 


#
#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 10000), family="bernoulli", cores=4))
#                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  0.37      0.24    -0.11     0.84        444 1.01
#WordLength                 0.94      0.02     0.90     0.97       3004 1.00
#LogWordFreq               -0.30      0.02    -0.33    -0.27       2974 1.00
#WNUM.C                    -0.01      0.01    -0.03     0.01       3422 1.00
#AttProbability.Resid       0.08      0.03     0.02     0.14       2196 1.00
#Surprisal.Resid            0.01      0.02    -0.02     0.04       3306 1.00
#WordLength:LogWordFreq     0.25      0.01     0.22     0.28       3474 1.00

#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 10000), family="bernoulli", cores=4, prior=prior(normal(0, 10), class="b")))  # 21219.5 seconds (Total)

#                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  0.38      0.22    -0.08     0.82        632 1.00
#WordLength                 0.94      0.02     0.90     0.97       2595 1.00
#LogWordFreq               -0.30      0.02    -0.33    -0.27       2349 1.00
#WNUM.C                    -0.01      0.01    -0.03     0.01       3001 1.00
#AttProbability.Resid       0.08      0.03     0.02     0.15       2017 1.00
#Surprisal.Resid            0.01      0.02    -0.02     0.04       3160 1.00
#WordLength:LogWordFreq     0.25      0.01     0.22     0.28       3051 1.00

#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 10000), family="bernoulli", cores=4, prior=prior(normal(0, 1), class="b"))) # 20927.7 seconds (Total)


#                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  0.39      0.23    -0.09     0.85        567 1.01
#WordLength                 0.94      0.02     0.90     0.98       2922 1.00
#LogWordFreq               -0.30      0.02    -0.33    -0.27       2507 1.00
#WNUM.C                    -0.01      0.01    -0.03     0.01       4000 1.00
#AttProbability.Resid       0.08      0.03     0.02     0.15       2214 1.00
#Surprisal.Resid            0.01      0.02    -0.02     0.04       3507 1.00
#WordLength:LogWordFreq     0.25      0.01     0.22     0.28       4000 1.00


#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 10000), family="bernoulli", cores=4))   

#                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  0.40      0.23    -0.07     0.86        376 1.01
#WordLength                 0.94      0.02     0.90     0.98       2954 1.00
#LogWordFreq               -0.30      0.02    -0.33    -0.27       2936 1.00
#WNUM                      -0.00      0.00    -0.00     0.00       3548 1.00
#AttProbability.Resid       0.08      0.03     0.02     0.14       1730 1.00
#Surprisal.Resid            0.01      0.02    -0.03     0.04       2157 1.00
#WordLength:LogWordFreq     0.25      0.01     0.22     0.27       3309 1.00

#model = (brm(FIXATED ~ WNUM*WordLength + WNUM*LogWordFreq + WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 20000), family="bernoulli", cores=4, iter=4000))   
# Doesn't cvonverge with the default iteration count


model = (brm(FIXATED ~ WNUM*WordLength + WNUM*LogWordFreq + WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 20000), family="bernoulli", cores=4, iter=6000))   



# Does not converge!!!!
model = (brm(FIXATED ~ WNUM*WordLength + WNUM*LogWordFreq + WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+AttProbability.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 20000), family="bernoulli", cores=4, iter=4000))


