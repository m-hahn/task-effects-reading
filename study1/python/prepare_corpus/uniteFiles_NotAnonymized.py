import os

assert False, "don't redo this"

from collections import defaultdict
for corpus in ["cnn", "dailymail"]:
   vocabulary = defaultdict(int)
   for partition in ["test", "validation", "training"]:
     print(corpus, partition)
     with open(f'/john5/scr1/mhahn/CHO/{corpus}/questions/{partition}_nonAnonymized.txt', "w") as outFile:
       PATH = f'/john5/scr1/mhahn/CHO/{corpus}/questions/{partition}/'
       files = os.listdir(PATH)
       for name in files:
         with open(PATH+name) as inFile:
           data = inFile.read().strip().split("\n\n")
#           data = inFile.read().strip().replace("\n\n", "##########").replace("\n", "@@@@")
           assert (len(data)) == 5
           assert "\n" not in data[0]
           assert "\n" not in data[1]
           assert "\n" not in data[2]
           assert "\n" not in data[3]
           #print(data)
           #quit()
   #        print(data)
           text = data[1]
           neMapping = {x[:x.find(":")] : x[x.find(":")+1:] for x in data[4].strip().split("\n")}
  #         print(text)
 #          print(neMapping)
           for x in neMapping:
              text = text.replace(f"{x} ",f"{neMapping[x]} ".lower())
#           print(text)
           print(text, file=outFile)
           for word in data[1].split(" "):
               if len(word) == 0:
                   continue
               vocabulary[word] += 1
   vocabulary_list = list(vocabulary.items())
   vocabulary_list = sorted(vocabulary_list, key=lambda x:x[1], reverse=True)
   with open(f"vocabularies/{corpus}_nonAnonymized.txt", "w") as outFile:
       for i in range(min(500000, len(vocabulary_list))):
           print(f"{str(i)}\t{vocabulary_list[i][0]}\t{str(vocabulary_list[i][1])}", file=outFile)
