# train_attention_6.py: THIS one works nice and fast

import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=random.choice([64])) #random.choice([16, 32]))
parser.add_argument('--learning_rate', type=float, default=random.choice([0.1, 0.2, 0.5, 1.0]))
parser.add_argument('--glove', type=bool, default=True)
parser.add_argument('--dropout', type=float, default=0.0)
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))
parser.add_argument('--LAMBDA', type=float, default=2.25) #random.choice([1.5, 1.75, 2, 2.25, 2.5]))
parser.add_argument('--REWARD_FACTOR', type=float, default=0.1)
#parser.add_argument('--permuteEntities', type=bool, default=True)

args = parser.parse_args()



vocabulary = [x.split("\t") for x in open(f"vocabularies/{args.corpus}_nonAnonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 
def permuteEntitiesInQ(result):
    if not result["answer"].startswith("@entity"):
      print(result["answer"])
      result["answer"] = result["answer"][result["answer"].index("@entity"):]
    entities = unique([x for x in result["text"]+result["question"]+[result["answer"]] if x.startswith("@entity")])
    entities_mapping = dict(zip(entities, [f"@entity{x}" for x in range(len(entities))]))
    assert len(entities_mapping) == len(entities)
    result["text"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["text"]]
    result["question"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["question"]]
    result["answer"] = entities_mapping[result["answer"]]


TEXT_LENGTH_BOUND = 500

def processQuestion(question, permuteEntities=True):
    question = question.split("##########")
    result = {"text" : question[1].split(" "), "question" : question[2].split(" "), "answer" : question[3]}
    if permuteEntities:
       permuteEntitiesInQ(result)

    answerDistribution[0]+=1
    answerDistribution[1][int(result["answer"].replace("@entity", ""))]+=1

    return result

import gzip

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
   assert partition in ["test", "training", "validation"]
   with gzip.open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/cnn_{partition}_nonAnonymized.txt.gz", "rb") as inFile1:
    with gzip.open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/dailymail_{partition}_nonAnonymized.txt.gz", "rb") as inFile2:
     while True:
        buff = []
        print("Filling buffer...")
        for _ in range(10*args.batchSize):
         try:
           buff.append(next(inFile1).decode().strip().split(" "))
           buff.append(next(inFile2).decode().strip().split(" "))
         except StopIteration:
            break
        if len(buff) == 0:
           break
        random.shuffle(buff)
        concatenated = []
        for x in buff:
           for y in x:
            concatenated.append(y)
        partitions = []
        for i in range(int(len(concatenated)/50)+1):
          r = concatenated[i*50:(i+1)*50]
          if len(r) > 0:
            partitions.append(r)
        random.shuffle(partitions)
        for i in range(int(len(partitions)/args.batchSize)+1):
          r = partitions[i*args.batchSize:(i+1)*args.batchSize]
          if len(r) > 0:
            yield r
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()

########################
reader = torch.nn.LSTM(200, 1024, 1).cuda()
reconstructor = torch.nn.LSTM(200, 1024, 1).cuda()
output = torch.nn.Linear(1024, 50000 + 4).cuda()

input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss(reduction="none", ignore_index=PAD)
crossEntropy = torch.nn.CrossEntropyLoss(reduction="none", ignore_index=PAD)

components_lm = [word_embeddings, reader, reconstructor, output]


loaded = torch.load(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/train_autoencoder.py_5619394.ckpt")
#{"devLosses" : devLosses, "args" : args, "components" : [x.state_dict() for x in components_lm], "learning_rate" : learning_rate}, )
for i in range(len(loaded["components"])):
   components_lm[i].load_state_dict(loaded["components"][i])



bilinear = torch.nn.Bilinear(1024, 200, 1).cuda()
bilinear.weight.data.zero_()
bilinear.bias.data.zero_()

components_attention = [bilinear]
runningAverageParameter = torch.FloatTensor([0]).cuda()

def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param
 for c in components_attention:
   for param in c.parameters():
      yield param
 yield runningAverageParameter

optimizer = torch.optim.SGD(parameters(), lr = args.learning_rate)




def forwardAttention(batch):
    texts = [[numerify(y) for y in x["text"][:TEXT_LENGTH_BOUND]] for x in batch] # [:500]
    questions = [set([numerify(y) for y in x["question"]]) for x in batch]
    text_length = max([len(x) for x in texts])
    position = torch.FloatTensor([list(range(text_length)) for _ in batch]).cuda().t()
    condition = torch.FloatTensor([[0 if i < len(batch)/2 else 1 for _ in range(text_length)] for i in range(len(batch))]).cuda().t()
    condition_ = condition - 0.5
    position = (position)/500 - 0.5
    positionTimesCondition = condition_ * position
    occursInQuestion = torch.FloatTensor([[1 if j < len(texts[i]) and texts[i][j] in questions[i] else 0 for j in range(text_length)] for i in range(len(batch))]).cuda().t() - 0.1
    occursInQuestion = occursInQuestion * condition
    occursInQuestionAverage = torch.FloatTensor([0 for i in range(len(batch))]).cuda()
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)

    texts = torch.LongTensor(texts).cuda().transpose(0,1)
    texts_num = texts
    texts = word_embeddings(texts).detach()
    global featuresAverage
    featuresAverage_ = featuresAverage.clone()
    attentionLogprobabilities = []
    attentionDecisionsList = []
    zeroProbabilities = torch.zeros(len(batch)).cuda()
    runningAverage = torch.nn.functional.sigmoid(runningAverageParameter)
    oneMinus = (1-runningAverage)
    runningAverage_ = torch.cat([ oneMinus*torch.pow(runningAverage, 10-i) for i in range(10)] + [runningAverage], dim=0).unsqueeze(1)
    attentionScores = []
    ones = torch.zeros(text_length, len(batch)).cuda()+1
#    print(featuresAverage_)



 
  
    embedding = texts
    #print(embedding)

#    attentionLogit = linear(features) + bilinear(features, embedding)
    if random.random() < 0.01:
        print(bilinear.weight.size(), embedding.size())
        perWordWeights = torch.matmul(bilinear.weight.squeeze(2), embedding.transpose(1,2))[:10].cpu().detach().numpy()
        print("Linear1", linear.weight.data)
        print("Position\tCondition\tPositionXCondition\tInQuestion")
        for i in range(min(10, len(batch[0]["text"]))):
           print(i, "\t", batch[0]["text"][i], "\t", "\t".join([str(float(x)) for x in perWordWeights[i,:,0]]))

    #print(linear.weight.abs().max())
    #print(bilinear.weight.abs().max())
    #print("attentionLogit", attentionLogit)
    attentionLogProbability = torch.nn.functional.logsigmoid(torch.where(attentionDecisions == 1, attentionLogit, -attentionLogit))
    #print(attentionProbability)
    #print(attentionDecisions) 
#    quit()
    return attentionDecisions, attentionLogProbability, attentionProbability




def forward(batch, calculateAccuracy=False):
    texts = [[PAD] + [numerify(y) for y in x] + [PAD] for x in batch] # [:500]
    text_length = max([len(x) for x in texts])
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    texts =  torch.LongTensor(texts).cuda()

    mask = torch.FloatTensor([1 for _ in range(len(batch))]).cuda()
    masked = torch.LongTensor([SKIPPED]).cuda().expand(len(batch))
#    print(masked.size())
    hidden = None
    outputs = []

    attentionProbability_ = []
    attentionDecisions_ = []
    attentionLogit_ = []

    for i in range(texts.size()[1]-1):
#       print(mask.size(), texts[:,i].size(), masked.size())
       embedded_ = word_embeddings(torch.where(mask==1.0, texts[:,i], masked)).unsqueeze(0)
 #      print(embedded_.size())
       _, hidden = reader(embedded_, hidden)
       outputs.append(hidden[0])
       #print("HIDDEN", hidden.size())
       #print(output_here.size())
       embedded_ = word_embeddings(texts[:,i+1])
  #     print(embedded_.size(), hidden[0].size())
       attention_logits = bilinear(hidden[0].squeeze(0), embedded_).squeeze(1)
       #print(attention_logits.size())
       attentionProbability = torch.nn.functional.sigmoid(attention_logits)
       attentionDecisions = torch.bernoulli(attentionProbability)
       mask = attentionDecisions
       attentionProbability_.append(attentionProbability)
       attentionDecisions_.append(attentionDecisions)
       attentionLogit_.append(attention_logits)

    attentionProbability = torch.stack(attentionProbability_, dim=0)
    attentionDecisions = torch.stack(attentionDecisions_, dim=0)
    attentionLogit = torch.stack(attentionLogit_, dim=0)

   # print(attentionProbability.size(), attentionDecisions.size(), attentionLogit.size())
    outputs_reader = torch.cat(outputs, dim=0)
    #print(outputs_reader.size())
    embedded = word_embeddings(texts).transpose(0,1)
    outputs_decoder, _ = reconstructor(embedded[:-1], hidden)
#    print(outputs_decoder.size())
 #   print(embedded.size())
    targets = texts.transpose(0,1)
    targets = torch.cat([targets[1:], targets[1:]], dim=0)
    outputs_cat = output(torch.cat([outputs_reader, outputs_decoder], dim=0))
  #  print(outputs_cat.size(), targets.size())
    loss = crossEntropy(outputs_cat.view(-1, 50004), targets.view(-1)).view(outputs_cat.size()[0], outputs_cat.size()[1])
   # print(loss.size(), loss.mean())

    attentionLogProbability = torch.nn.functional.logsigmoid(torch.where(attentionDecisions == 1, attentionLogit, -attentionLogit))


    if random.random() < 0.1:
       print(len(texts), loss.size(), targets.size(), attentionProbability.size(), attentionDecisions.size())
       loss_reader = loss[:10, 0].cpu()
       loss_reconstructor = loss[51:61, 0].cpu()

       attentionProbability_ = attentionProbability[:10, 0].cpu()
       attentionDecisions_ = attentionDecisions[:10, 0].cpu()
       print("\t".join(["Pos", "Word", "Pred", "Rec", "AttProb", "Att?"]))
       for j in range(10):
         print("\t".join([str(y) for  y in [j, batch[0][j]] +[round(float(x),4) for x in [loss_reader[j], loss_reconstructor[j], attentionProbability_[j], attentionDecisions_[j]]]]))
#       quit()

    #print(attentionLogProbability.size())
    loss = loss.mean(dim=0)
    attentionDecisions = attentionDecisions.mean(dim=0)
    return loss, attentionLogProbability, attentionDecisions

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

fixationRunningAverageByCondition = [0.5,0.5]
rewardAverage = 10.0
lossAverageByCondition = [10.0, 10.0]

def backward(loss, action_logprob, fixatedFraction, printHere=True):
   global rewardAverage
   if random.random() < 0.99:
     batchSize = fixatedFraction.size()[0]
     fix1 = float(fixatedFraction.mean())
     fixationRunningAverageByCondition[0] = 0.99 * fixationRunningAverageByCondition[0] + (1-0.99) * fix1

     loss1 = float(loss.mean())
     lossAverageByCondition[0] = 0.99 * lossAverageByCondition[0] + (1-0.99) * loss1
     if printHere:
       print("FIXATION RATE", fixationRunningAverageByCondition, "REWARD", rewardAverage, "LossByCondition", lossAverageByCondition)
   optimizer.zero_grad()
   reward = (loss.detach() + args.LAMBDA * fixatedFraction)
   loss_ = args.REWARD_FACTOR * (action_logprob * (reward - rewardAverage)).mean() + loss.mean()
   loss_.backward()
   rewardAverage = 0.99 * rewardAverage + (1-0.99) * float(reward.mean())
   if args.clip_type is not "None":
#      print("NROM MAX", max(p.grad.detach().abs().max().cpu() for p in parameters()))
      torch.nn.utils.clip_grad_norm_(parameters(), args.clip_bound, norm_type=args.clip_type)
   optimizer.step()

import time


learning_rate=args.learning_rate

devLosses = []
lossRunningAverage = 6.4
devAccuracies = []
devRewards = []
noImprovement = 0
for epoch in range(10000):
  
  if epoch > 0:
    validLoss = []
    examplesNumber = 0
    validAccuracy = []
    validReward = []
    validAccuracyPerCondition = [0.0, 0.0]
    validFixationsPerCondition = [0.0, 0.0]
    for batch in  loadQACorpus(args.corpus, "validation", args.batchSize, permuteEntities=True):
     with torch.no_grad():
       loss, action_logprob, fixatedFraction = forward(batch, calculateAccuracy = True)
       reward = float((loss.detach() + args.LAMBDA * fixatedFraction).mean())
       loss = float(loss.mean())
       print("VALID", loss, examplesNumber, reward)
#     print(accuracy)
     #print(fixatedFraction)
     fixationsCond1 = float(fixatedFraction.sum())
     validFixationsPerCondition[0] += fixationsCond1


     validLoss.append(float(loss)*len(batch))
     validReward.append(reward*len(batch))
     examplesNumber += len(batch)
     validAccuracy.append(float(accuracy)*len(batch))
    devLosses.append(sum(validLoss)/examplesNumber)
    devAccuracies.append(sum(validAccuracy)/examplesNumber)
    devRewards.append(sum(validReward)/examplesNumber)
    validFixationsPerCondition[0] /= examplesNumber
    print(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/study1Scores_2020/accuracy_{__file__}_{args.myID}.txt")
    with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/study1Scores_2020/accuracy_{__file__}_{args.myID}.txt", "w") as outFile:
       print(args, file=outFile)
       print(devAccuracies, file=outFile)
       print(devLosses, file=outFile)
       print(devRewards, file=outFile)
       print(fixationRunningAverageByCondition[0], fixationRunningAverageByCondition[1], file=outFile)
       print(rewardAverage, "\t", validAccuracyPerCondition[0], validAccuracyPerCondition[1], "\t", validFixationsPerCondition[0], validFixationsPerCondition[1], file=outFile)
    if len(devRewards) >1 and devRewards[-1] > devRewards[-2]:
       learning_rate *= 0.8
       optimizer = torch.optim.SGD(parameters(), lr = learning_rate)
       noImprovement += 1
  #  elif len(devAccuracies) > 1 and devAccuracies[-1] < max(devAccuracies):
   #    torch.save({"devAccuracies" : devAccuracies, "devLosses" : devLosses, "args" : args, "components" : [x.state_dict() for x in components_lm], "learning_rate" : learning_rate}, f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/{__file__}_{args.myID}.ckpt")
  
    #   noImprovement = 0
    if noImprovement > 5:
      print("End training, no improvement for 5 epochs")
      break
  words = 0
  timeStart = time.time()
  counter = 0
  for batch in loadQACorpus(args.corpus, "training", args.batchSize, permuteEntities=True):
    counter += 1
    printHere = (counter % 5) == 0
    loss, action_logprob, fixatedFraction = forward(batch)
    backward(loss, action_logprob, fixatedFraction, printHere=printHere)
    loss = float(loss.mean())
    lossRunningAverage = 0.99 *lossRunningAverage + (1-0.99) * float(loss)
    words += len(batch)
    if printHere:
       print(devAccuracies)
       print(devLosses)
       print(devRewards)
       print(float(loss), lossRunningAverage, args, __file__)
       print(words/(time.time()-timeStart), "questions per second")
      

