import glob
import subprocess
files = glob.glob("/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2020/*Save_9b.py_*.txt")
header = None
print(files)

                                                                                                                           
with open("output/fixations_nonAnonymized_texts.tsv", "w") as outFile:           
   print("\t".join(["FixationRate", "Condition"]), file=outFile)

for f in files:
 # try:
   with open(f, "r") as inFile:
       args = next(inFile).strip()
       accuracies = next(inFile).strip()
       next(inFile)
       rewards = [float(x) for x in next(inFile).strip()[1:-1].split(", ") ]
       next(inFile)
       tradeoff = next(inFile).strip()
   print(args)
   if  "ENTROPY_WEIGHT=0.01" in args and "LAMBDA=1.0" in args:
#          print(tradeoff, len(rewards))
          print(f)
          modelNumber = f[f.rfind("/")+1:].replace("accuracy_", "").replace(".txt", ".ckpt")
#          quit()
          modelNumber = f[f.rfind("_")+1:][:-4]
#          print(modelNumber)
 #         quit()
          subprocess.call(["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", "run_lm_noised_texts.py", "--NEAT_model="+modelNumber])
for id_ in ["FULL", "RANDOM"]:
   subprocess.call(["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", "run_lm_noised_texts_fullOrRandom.py", "--NEAT_model="+id_])

