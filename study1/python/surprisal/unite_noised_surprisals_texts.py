lm_id = "65591293"

import glob
files = glob.glob(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/NEAT_SURPRISAL_OUTPUT/run_lm_noised_texts.py_*_{lm_id}")
print(files)
data = []
for f in files:
   if "MERGED" in f:
      continue
   with open(f, "r") as inFile:
     inFile = [x.split("\t") for x in inFile.read().strip().split("\n")]
     if len(inFile) > 2:
        data.append(inFile)

header = data[0][0]
header = dict(list(zip(header, range(len(header)))))
print([len(x) for x in data])

def mean(x):
  return sum(x)/len(x)

with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/NEAT_SURPRISAL_OUTPUT/run_lm_noised_texts.py_MERGED_{lm_id}", "w") as outFile:
 print("\t".join(data[0][0]), file=outFile)

 for i in range(1, len(data[0])):
#    print(data[0][i])
    tokens = [x[i][header["Token"]] for x in data]
    assert len(set(tokens)) == 1, tokens


    surprisals = mean([float(x[i][header["Surprisal"]]) for x in data])
    line_ = data[0][i][:]
    line_[header["Surprisal"]] = str(surprisals)
    print("\t".join(line_), file=outFile)

