import glob
files = glob.glob("/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/train_attention4.py_*.ckpt")
import subprocess
for f in files:
   id_ = f[f.rfind("_")+1:-5]
   subprocess.call(["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", "run_lm_noised_dundee_4.py", "--NEAT_model="+id_])
for id_ in ["REAL", "RANDOM"]:
   subprocess.call(["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", "run_lm_noised_dundee_4_fullOrRandom.py", "--NEAT_model="+id_])

