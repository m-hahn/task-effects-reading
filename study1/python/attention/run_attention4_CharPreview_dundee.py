import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=random.choice([64])) #random.choice([16, 32]))
parser.add_argument('--learning_rate', type=float, default=random.choice([0.1, 0.2, 0.5, 1.0]))
parser.add_argument('--glove', type=bool, default=True)
parser.add_argument('--dropout', type=float, default=0.0)
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))
parser.add_argument('--LAMBDA', type=float, default=2.25) #random.choice([1.5, 1.75, 2, 2.25, 2.5]))
parser.add_argument('--REWARD_FACTOR', type=float, default=0.1)
parser.add_argument('--ENTROPY_WEIGHT', type=float, default=0.005) #random.choice([0.0001, 0.001, 0.01, 0.1]))
parser.add_argument('--previewLength', type=int, default=random.choice([4]))
parser.add_argument('--lengthUntil', type=int, default=random.choice([0]))
parser.add_argument('--SEQUENCE_LENGTH', type=int, default=50)
parser.add_argument('--LOAD_CKPT', type=str, default="92479930") #random.choice([1.5, 1.75, 2, 2.25, 2.5]))

args = parser.parse_args()

SEQUENCE_LENGTH = args.SEQUENCE_LENGTH

vocabulary = [x.split("\t") for x in open(f"vocabularies/{args.corpus}_nonAnonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

itos_characters = set()
for x in itos:
   for y in x:
      itos_characters.add(y)
      itos_characters.add(y.upper())
itos_characters = ["OOV", "PAD", "EOS", "SOS"] + (sorted(list(itos_characters))[:100])
stoi_characters = dict(list(zip(itos_characters, range(len(itos_characters)))))
print(stoi_characters)


def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 
def permuteEntitiesInQ(result):
    if not result["answer"].startswith("@entity"):
      print(result["answer"])
      result["answer"] = result["answer"][result["answer"].index("@entity"):]
    entities = unique([x for x in result["text"]+result["question"]+[result["answer"]] if x.startswith("@entity")])
    entities_mapping = dict(zip(entities, [f"@entity{x}" for x in range(len(entities))]))
    assert len(entities_mapping) == len(entities)
    result["text"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["text"]]
    result["question"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["question"]]
    result["answer"] = entities_mapping[result["answer"]]


TEXT_LENGTH_BOUND = 500

def processQuestion(question, permuteEntities=True):
    question = question.split("##########")
    result = {"text" : question[1].split(" "), "question" : question[2].split(" "), "answer" : question[3]}
    if permuteEntities:
       permuteEntitiesInQ(result)

    answerDistribution[0]+=1
    answerDistribution[1][int(result["answer"].replace("@entity", ""))]+=1

    return result

import gzip

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
        with open("/u/scr/mhahn/Dundee/DundeeTreebankTokenized.csv", "r") as inFile:
           data = [x.split("\t") for x in inFile.read().strip().split("\n")]
        global HEADER
        HEADER = dict(list(zip(data[0], range(len(data[0])))))
        concatenated = data[1:]
        partitions = []
        for i in range(int(len(concatenated)/SEQUENCE_LENGTH)+1):
          r = concatenated[i*SEQUENCE_LENGTH:(i+1)*SEQUENCE_LENGTH]
          if len(r) > 0:
            partitions.append(r)
        for i in range(int(len(partitions)/args.batchSize)+1):
          r = partitions[i*args.batchSize:(i+1)*args.batchSize]
          if len(r) > 0:
            yield r
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def lengthBin(word):
  if len(word) < args.lengthUntil:
     return len(word)
  return args.lengthUntil


def makePrefix(x):
   preview = x[:args.previewLength]+"_"+str(lengthBin(x))
   return preview


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

def numerify_preview_characters(token):
#   token = token.replace("—", "-").replace('”', '"').replace('“', '"').replace("»", '"').replace("…", "...").replace("‘", "'").replace("’", "'").replace("!", "!").replace("–", "-") # this relates to DailyMail vs CNN
   token = token[:args.previewLength]
   #for x in token:
  #   if x not in stoi_characters:
 #       print("UNKNOWN", x)
   return [stoi_characters.get(x, stoi_characters["OOV"]) for x in token] + [stoi_characters["PAD"] for _ in range(args.previewLength-len(token))]




def numerify_preview(token):
   token = token.lower()
#   token = token.replace("—", "-").replace('”', '"').replace('“', '"').replace("»", '"').replace("…", "...").replace("‘", "'").replace("’", "'").replace("!", "!").replace("–", "-") # this relates to DailyMail vs CNN
   preview = makePrefix(token)
   if preview not in stoi_prefixes:
      return OOV
   else:
      return stoi_prefixes[preview]


import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()
char_embeddings = torch.nn.Embedding(num_embeddings = 100+4, embedding_dim = 128).cuda()

########################
reader = torch.nn.LSTM(200, 1024, 1).cuda()
reconstructor = torch.nn.LSTM(200, 1024, 1).cuda()
output = torch.nn.Linear(1024, 50000 + 4).cuda()

input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss(reduction="none", ignore_index=PAD)
crossEntropy = torch.nn.CrossEntropyLoss(reduction="none", ignore_index=PAD)

components_lm = [word_embeddings, reader, reconstructor, output]

ONLY_REC = True
WITH_CONTEXT = False
WITH_LM = False

if not ONLY_REC:
  loaded = torch.load(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/train_autoencoder.py_5619394.ckpt")
else:
  loaded = torch.load(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/train_autoencoder_onlyRec.py_71629641.ckpt")
#{"devLosses" : devLosses, "args" : args, "components" : [x.state_dict() for x in components_lm], "learning_rate" : learning_rate}, )
for i in range(len(loaded["components"])):
   components_lm[i].load_state_dict(loaded["components"][i])


if WITH_CONTEXT:
  # Use a bilinear module to combine word embedding with context information
  bilinear = torch.nn.Bilinear(1024, 328, 1).cuda()
else:
  # Use a linear module for derive attention logit from word embedding
  bilinear = torch.nn.Linear(328, 1).cuda()
bilinear.weight.data.zero_()
bilinear.bias.data.zero_()




def findFirstSatisfying(startIncl, endIncl, condF):
   if condF(startIncl):
     return startIncl
   i = endIncl
   while True:
      if startIncl == i:
         return i
      if startIncl+1 == i:
         return i
      med = int((startIncl + i)/2)
      if condF(med):
          i = med
      else:
         startIncl = med
   

associated = {}

vocab_ = sorted(itos[:50000], key=lambda x:(makePrefix(x), len(x)))
print(vocab_[:100])
lastPrefix = ""
startPos = 0
notDone = set(range(50005))
#word_embeddings_preview.weight.data = word_embeddings.weight.data.clone().mean(dim=0).unsqueeze(0).expand(word_embeddings.weight.data.size()[0], -1)

#word_embeddings_preview.weight.data.zero_()


itos_prefixes = ["PAD", "SKIPPED", "OOV", "PLACEHOLDER"]
stoi_prefixes = {x : itos_prefixes.index(x) for x in itos_prefixes}
prefix_embeddings_matrix = [word_embeddings.weight.data[i].clone() for i in range(len(itos_prefixes))]

startPos = 0
lastPrefix = makePrefix(vocab_[0])
for i in range(1,len(vocab_)):
   prefix = makePrefix(vocab_[i])
   if prefix in stoi_prefixes:
     continue
   elif prefix != lastPrefix:
       first = startPos #findFirstSatisfying(startPos, j, lambda k:(len(vocab_[j]) / len(vocab_[k])) < 2)
       last = i #findFirstSatisfying(j, i-1, lambda k:(len(vocab_[k]) / len(vocab_[j])) > 2)
       assert makePrefix(vocab_[first]) == lastPrefix
       assert makePrefix(vocab_[last-1]) == lastPrefix
       assert makePrefix(vocab_[last]) != lastPrefix
#       if last > 5000:
 #        print(vocab_[first:last])
  #       quit()
       relevant = torch.LongTensor([numerify(x) for x in vocab_[first:last]]).cuda()
       embedded = word_embeddings(relevant)
      # print(embedded.size())

       # non-capitalized version
       stoi_prefixes[prefix] = len(itos_prefixes)
       itos_prefixes.append(prefix)
       prefix_embeddings_matrix.append(embedded.mean(dim=0).data.clone())



       lastPrefix = prefix
       startPos = i


print(len(itos_prefixes))
assert stoi_prefixes[itos_prefixes[100]] == 100




word_embeddings_preview = torch.nn.Embedding(num_embeddings = len(itos_prefixes), embedding_dim = 200).cuda()
print(word_embeddings_preview.weight.data.size())

word_embeddings_preview.weight.data = torch.stack(prefix_embeddings_matrix, dim=0)

print(word_embeddings_preview.weight.data.size())



components_attention = [bilinear, word_embeddings_preview, char_embeddings]

def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param
 for c in components_attention:
   for param in c.parameters():
      yield param

state = torch.load(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2021/train_attention4_CharPreview.py_{args.LOAD_CKPT}.ckpt")


print("args", state["args"])
print(state["devRewards"])
if len(state["devRewards"]) < 2:
   quit()
for i in range(len(components_lm)):
   components_lm[i].load_state_dict(state["components_lm"][i])
for i in range(len(components_attention)):
   components_attention[i].load_state_dict(state["components_attention"][i])






def forward(batch, calculateAccuracy=False):
    texts = [[PAD] + [numerify(y[HEADER["Token"]]) for y in x] + [PAD] for x in batch] # [:500]
    padding = [[stoi_characters["PAD"] for _ in range(args.previewLength)]]
    texts_preview_characters_num = [padding + [numerify_preview_characters(y[HEADER["Token"]]) for y in x] + padding for x in batch] # [:500]
    texts_preview_numerified = [[PAD] + [numerify_preview(y[HEADER["Token"]]) for y in x] + [PAD] for x in batch] # [:500]
    text_length = max([len(x) for x in texts])
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    texts =  torch.LongTensor(texts).cuda()


    for text in texts_preview_numerified:
       while len(text) < text_length:
          text.append(PAD)
    texts_preview_numerified = torch.LongTensor(texts_preview_numerified).cuda()



    mask = torch.FloatTensor([1 for _ in range(len(batch))]).cuda()
    masked = torch.LongTensor([SKIPPED]).cuda().expand(len(batch))
#    print(masked.size())
    hidden = None
    outputs = []

    attentionProbability_ = []
    attentionDecisions_ = []
    attentionLogit_ = []


#    texts = torch.LongTensor(texts).cuda().transpose(0,1)
 #   texts_num = texts

    maxLength = max(len(x) for x in texts_preview_characters_num)
    for x in texts_preview_characters_num:
       while len(x) < maxLength:
          x.append([stoi_characters["PAD"] for _ in range(args.previewLength)])
    texts_preview_characters_num = torch.LongTensor(texts_preview_characters_num).cuda()
    texts_preview_characters = char_embeddings(texts_preview_characters_num)
    texts_rnn = texts_preview_characters
    texts_preview_rnn = texts_rnn.mean(dim=2)


    texts_preview = word_embeddings_preview(texts_preview_numerified).detach()
#    print(texts_preview.size(), texts_preview_rnn.size())
    texts_preview = torch.cat([texts_preview, texts_preview_rnn], dim=2)




    if not WITH_CONTEXT:
      # Calculate the context-independent attention logits
      attention_logits_total = bilinear(texts_preview)
    # Iterate over the input
    for i in range(texts.size()[1]-1):
#       print(mask.size(), texts[:,i].size(), masked.size())
       embedded_ = word_embeddings(torch.where(mask==1.0, texts[:,i], masked)).unsqueeze(0)
 #      print(embedded_.size())
       _, hidden = reader(embedded_, hidden)
       outputs.append(hidden[0])
       #print("HIDDEN", hidden.size())
       #print(output_here.size())
       if WITH_CONTEXT:
         # Calculate the context-dependent attention logits
         embedded_ = texts_preview[:,i+1]
         attention_logits = bilinear(hidden[0].squeeze(0), embedded_).squeeze(1)
       else:
         attention_logits = attention_logits_total[:,i+1].squeeze(1)
       #print(attention_logits.size())
       attentionProbability = torch.nn.functional.sigmoid(attention_logits)
       if WITH_CONTEXT:
         attentionDecisions = torch.bernoulli(torch.clamp(attentionProbability, min=0.01, max=0.99))
       else:
         attentionDecisions = torch.bernoulli(attentionProbability)
       mask = attentionDecisions
       attentionProbability_.append(attentionProbability)
       attentionDecisions_.append(attentionDecisions)
       attentionLogit_.append(attention_logits)

    attentionProbability = torch.stack(attentionProbability_, dim=0)
    attentionDecisions = torch.stack(attentionDecisions_, dim=0)
    attentionLogit = torch.stack(attentionLogit_, dim=0)
    if WITH_LM:
      # Collect outputs from the reader module to compute surprisal
      outputs_reader = torch.cat(outputs, dim=0)
    embedded = word_embeddings(texts).transpose(0,1)
    outputs_decoder, _ = reconstructor(embedded[:-1], hidden)
    if WITH_LM:
      # Collect target values for both surprisal and decoding loss
      targets = texts.transpose(0,1)
      targets = torch.cat([targets[1:], targets[1:]], dim=0)
      outputs_cat = output(torch.cat([outputs_reader, outputs_decoder], dim=0))
    else:
      # Collect target values for decoding loss
      targets = texts.transpose(0,1).contiguous()
      targets = targets[1:]
      outputs_cat = output(outputs_decoder)
    loss = crossEntropy(outputs_cat.view(-1, 50004), targets.view(-1)).view(outputs_cat.size()[0], outputs_cat.size()[1])

    text_from_batch = []


    if True:
       sequenceLengthHere= text_length-2
       loss_reader = loss.cpu()
       attentionProbability_ = attentionProbability.cpu()
       attentionDecisions_ = attentionDecisions.cpu()
       for batch_ in range(loss.size()[1]):
        for pos in range(loss.size()[0]):
         try:
#           print(batch[batch_][pos])
           lineForWord = batch[batch_][pos]
           text_from_batch.append([str(y) for  y in [pos, lineForWord[HEADER["Token"]], lineForWord[HEADER["Itemno"]], lineForWord[HEADER["WNUM"]], lineForWord[HEADER["ID"]], "InVocab" if stoi.get(lineForWord[HEADER["Token"]], 100000) < 50000 else "OOV"] +[round(float(x),4) for x in [loss_reader[pos,batch_], attentionProbability_[pos,batch_], attentionDecisions_[pos, batch_]]]])
         except IndexError:
           pass
    loss = loss.mean(dim=0)
    print("return", len(text_from_batch))
    return loss, text_from_batch

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

fixationRunningAverageByCondition = [0.5,0.5]
lossAverageByCondition = [10.0, 10.0]

import time


learning_rate=args.learning_rate

devLosses = []
lossRunningAverage = 6.4
noImprovement = 0

concatenated = []
with open(f"/u/scr/mhahn/Dundee-annotated/{__file__}_{args.LOAD_CKPT}", "w") as outFile:
    validLoss = []
    examplesNumber = 0
    TEXT_ = []
    batches = list(loadQACorpus(args.corpus, None, args.batchSize, permuteEntities=True))
    print("batches", len(batches))
    for batch in batches:
     with torch.no_grad():
       loss, TEXT = forward(batch, calculateAccuracy = True)
       loss = float(loss.mean())
       print("VALID", loss, examplesNumber)
       for x in TEXT:
         TEXT_.append(x)

     validLoss.append(float(loss)*len(batch))
     examplesNumber += len(batch)
     count = 0
    for x in TEXT_:
       print("\t".join(x), file=outFile)

