# train_attention_6.py: THIS one works nice and fast

import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=64) #random.choice([16, 32]))
parser.add_argument('--learning_rate', type=float, default=random.choice([1.0]))
parser.add_argument('--glove', type=bool, default=True)
parser.add_argument('--dropout', type=float, default=random.choice([0.0, 0.05, 0.1, 0.15, 0.2]))
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))
parser.add_argument('--SEQUENCE_LENGTH', type=int, default=50)
parser.add_argument('--LOAD_CKPT', type=str, default="92479930") #random.choice([1.5, 1.75, 2, 2.25, 2.5]))

args = parser.parse_args()

SEQUENCE_LENGTH = args.SEQUENCE_LENGTH

vocabulary = [x.split("\t") for x in open(f"vocabularies/{args.corpus}_nonAnonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 
def permuteEntitiesInQ(result):
    if not result["answer"].startswith("@entity"):
      print(result["answer"])
      result["answer"] = result["answer"][result["answer"].index("@entity"):]
    entities = unique([x for x in result["text"]+result["question"]+[result["answer"]] if x.startswith("@entity")])
    entities_mapping = dict(zip(entities, [f"@entity{x}" for x in range(len(entities))]))
    assert len(entities_mapping) == len(entities)
    result["text"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["text"]]
    result["question"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["question"]]
    result["answer"] = entities_mapping[result["answer"]]


TEXT_LENGTH_BOUND = 500

def processQuestion(question, permuteEntities=True):
    question = question.split("##########")
    result = {"text" : question[1].split(" "), "question" : question[2].split(" "), "answer" : question[3]}
    if permuteEntities:
       permuteEntitiesInQ(result)

    answerDistribution[0]+=1
    answerDistribution[1][int(result["answer"].replace("@entity", ""))]+=1

    return result

import gzip
import os

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
   assert partition in ["validation"]
   with gzip.open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/cnn_{partition}_nonAnonymized.txt.gz", "rb") as inFile1:
    with gzip.open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/dailymail_{partition}_nonAnonymized.txt.gz", "rb") as inFile2:
     while True:
        buff = []
        print("Filling buffer...")
        for _ in range(10*args.batchSize):
         try:
           buff.append(next(inFile1).decode().strip().split(" "))
           buff.append(next(inFile2).decode().strip().split(" "))
         except StopIteration:
            break
        if len(buff) == 0:
           break
        random.shuffle(buff)
        concatenated = []
        for x in buff:
           for y in x:
            concatenated.append(y)
        partitions = []
        for i in range(int(len(concatenated)/50)+1):
          r = concatenated[i*50:(i+1)*50]
          if len(r) > 0:
            partitions.append(r)
        for i in range(int(len(partitions)/args.batchSize)+1):
          r = partitions[i*args.batchSize:(i+1)*args.batchSize]
          if len(r) > 0:
            yield r
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()

########################
reader = torch.nn.LSTM(200, 1024, 1).cuda()
reconstructor = torch.nn.LSTM(200, 1024, 1).cuda()
output = torch.nn.Linear(1024, 50000 + 4).cuda()

input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss(reduction="none", ignore_index=PAD)
crossEntropy = torch.nn.CrossEntropyLoss(reduction="none", ignore_index=PAD)

components_lm = [word_embeddings, reader, reconstructor, output]





bilinear = torch.nn.Bilinear(1024, 200, 1).cuda()
bilinear.weight.data.zero_()
bilinear.bias.data.zero_()

components_attention = [bilinear]
runningAverageParameter = torch.FloatTensor([0]).cuda()

def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param
 for c in components_attention:
   for param in c.parameters():
      yield param
 yield runningAverageParameter

state = torch.load(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/train_attention2.py_{args.LOAD_CKPT}.ckpt")


print("args", state["args"])
args.LAMBDA=state["args"].LAMBDA
print(state["devRewards"])
if len(state["devRewards"]) < 2:
   quit()
for i in range(len(components_lm)):
   components_lm[i].load_state_dict(state["components_lm"][i])
for i in range(len(components_attention)):
   components_attention[i].load_state_dict(state["components_attention"][i])


#def SAVE():
#       torch.save({"devRewards" : devRewards, "args" : args, "components_lm" : [x.state_dict() for x in components_lm], "components_attention" : [x.state_dict() for x in components_attention], "learning_rate" : learning_rate}, my_save_path)




def forward(batch, calculateAccuracy=False):
    texts = [[PAD] + [numerify(y) for y in x] + [PAD] for x in batch] # [:500]
    text_length = max([len(x) for x in texts])
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    texts =  torch.LongTensor(texts).cuda()

    mask = torch.FloatTensor([1 for _ in range(len(batch))]).cuda()
    masked = torch.LongTensor([SKIPPED]).cuda().expand(len(batch))
#    print(masked.size())
    hidden = None
    outputs = []

    attentionProbability_ = []
    attentionDecisions_ = []
    attentionLogit_ = []

    try:
       pass
#       print(texts.min(), texts.max(), 239)
    except RuntimeError:
       print(texts.size())
       print(texts)
       print(texts.min(), texts.max())
    for i in range(texts.size()[1]-1):
#       print(mask.size(), texts[:,i].size(), masked.size())
       try:
          pass
#          _ = (texts.min(), texts.max(), 247, i)
       except RuntimeError:
          print("POSITION", i)
          print(texts.size())
          print(texts)
          print(texts.min(), texts.max())
       embedded_ = word_embeddings(torch.where(mask==1.0, texts[:,i], masked)).unsqueeze(0)
 #      print(embedded_.size())
       try:
          pass
 #         _ = (texts.min(), texts.max(), 249, i)
       except RuntimeError:
          print("POSITION", i)
          print(texts.size())
          print(texts)
          print(texts.min(), texts.max())
  
       try:
          _, hidden = reader(embedded_, hidden)
       except RuntimeError:
          print(i)
          print(texts)
          print(texts.size())
          print(texts.min(), texts.max(), "ERROR 245 a")
          print(masked.size())
          print(masked.min(), masked.max(), "ERROR 245 b")
          print(mask.size())
          print(mask.min(), mask.max(), "ERROR 245 c")
       outputs.append(hidden[0])
       #print("HIDDEN", hidden.size())
       #print(output_here.size())
       embedded_ = word_embeddings(texts[:,i+1])
       try:
          attention_logits = bilinear(hidden[0].squeeze(0), embedded_).squeeze(1)
       except RuntimeError:
          print(texts.size())
          print(texts)
          print(texts.min(), texts.max(), "ERROR 245 aA")
          print(embedded_.size())
          print(embedded_.min(), embedded_.max(), "ERROR 245 bA")
          print(hidden[0].size())
          print(hidden[0].min(), hidden[0].max(), "ERROR 245 cA")
       #print(attention_logits.size())
       try:
          pass
#          MAXIMUM = ("ATTENTION", str(attention_logits.abs().max()))
       except RuntimeError:
            print(MAXIMUM)
            print(i)
            print("SOme Runtime Error? Unclear what it is 293")
            print(attention_logits.max())
       attentionProbability = torch.nn.functional.sigmoid(attention_logits)
       try:
          attentionDecisions = torch.bernoulli(torch.clamp(attentionProbability, min=0.01, max=0.99))
#          MAXIMUM2 = ("ATTENTION", str(attention_logits.max()))
#          _ = (attentionProbability.max())
 #         _ = (attentionDecisions.max())
       except RuntimeError:
            print(MAXIMUM)

            for c in components_lm:
              for param in c.parameters():
                 print(c, param.max())
            for c in components_attention:
              for param in c.parameters():
                 print(c, param.max())
            print(runningAverageParameter.max())




            print(MAXIMUM2)
            print(i)
            print("SOme Runtime Error? Unclear what it is")
            print(attention_logits.max())
            print(attentionProbability.min(), attentionProbability.max())
            print(attentionDecisions.min(), attentionDecisions.max())
            print(texts.size(), texts.min(), texts.max(), masked.min(), masked.max())
            return None, None, None
       mask = attentionDecisions
       attentionProbability_.append(attentionProbability)
       attentionDecisions_.append(attentionDecisions)
       attentionLogit_.append(attention_logits)

    attentionProbability = torch.stack(attentionProbability_, dim=0)
    attentionDecisions = torch.stack(attentionDecisions_, dim=0)
    attentionLogit = torch.stack(attentionLogit_, dim=0)

   # print(attentionProbability.size(), attentionDecisions.size(), attentionLogit.size())
    outputs_reader = torch.cat(outputs, dim=0)
    #print(outputs_reader.size())
    embedded = word_embeddings(texts).transpose(0,1)
    outputs_decoder, _ = reconstructor(embedded[:-1], hidden)
#    print(outputs_decoder.size())
 #   print(embedded.size())
    targets = texts.transpose(0,1)
    targets = torch.cat([targets[1:], targets[1:]], dim=0)
    outputs_cat = output(torch.cat([outputs_reader, outputs_decoder], dim=0))
  #  print(outputs_cat.size(), targets.size())
    loss = crossEntropy(outputs_cat.view(-1, 50004), targets.view(-1)).view(outputs_cat.size()[0], outputs_cat.size()[1])
   # print(loss.size(), loss.mean())

    attentionLogProbability = torch.nn.functional.logsigmoid(torch.where(attentionDecisions == 1, attentionLogit, -attentionLogit))


    if random.random() < 0.1:
       print(len(texts), loss.size(), targets.size(), attentionProbability.size(), attentionDecisions.size())
       loss_reader = loss[:10, 0].cpu()
       loss_reconstructor = loss[51:61, 0].cpu()

       attentionProbability_ = attentionProbability[:10, 0].cpu()
       attentionDecisions_ = attentionDecisions[:10, 0].cpu()
       print("\t".join(["Pos", "Word", "Pred", "Rec", "AttProb", "Att?"]))
       for j in range(10):
         try:
            print("\t".join([str(y) for  y in [j, batch[0][j]] +[round(float(x),4) for x in [loss_reader[j], loss_reconstructor[j], attentionProbability_[j], attentionDecisions_[j]]]]))
         except IndexError:
            pass
    loss = loss.mean(dim=0)
    attentionDecisions = attentionDecisions.mean(dim=0)
    return loss, attentionLogProbability, attentionDecisions

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

fixationRunningAverageByCondition = [0.5,0.5]
rewardAverage = 10.0
lossAverageByCondition = [10.0, 10.0]

import time


learning_rate=args.learning_rate

devLosses = []
lossRunningAverage = 6.4
devAccuracies = []
devRewards = []
noImprovement = 0
for epoch in range(1):
  
  if epoch >= 0:
    validLoss = []
    examplesNumber = 0
    validAccuracy = []
    validReward = []
    validAccuracyPerCondition = [0.0, 0.0]
    validFixationsPerCondition = [0.0, 0.0]
    for batch in  loadQACorpus(args.corpus, "validation", args.batchSize, permuteEntities=True):
     with torch.no_grad():
       loss, action_logprob, fixatedFraction = forward(batch, calculateAccuracy = True)
       if loss is None:
          continue
       reward = float((loss.detach() + args.LAMBDA * fixatedFraction).mean())
       loss = float(loss.mean())
       print("VALID", loss, examplesNumber, reward)
#     print(accuracy)
     #print(fixatedFraction)
     fixationsCond1 = float(fixatedFraction.sum())
     validFixationsPerCondition[0] += fixationsCond1


     validLoss.append(float(loss)*len(batch))
     validReward.append(reward*len(batch))
     examplesNumber += len(batch)
    devLosses.append(sum(validLoss)/examplesNumber)
    devRewards.append(sum(validReward)/examplesNumber)
    validFixationsPerCondition[0] /= examplesNumber
    print(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/study1Scores_2020/dev_accuracy_{__file__}_{args.myID}.txt")
    with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/study1Scores_2020/dev_accuracy_{__file__}_{args.myID}.txt", "w") as outFile:
       print(state["args"], file=outFile)
       print(devAccuracies, file=outFile)
       print(devLosses, file=outFile)
       print(devRewards, file=outFile)
       print(fixationRunningAverageByCondition[0], fixationRunningAverageByCondition[1], file=outFile)
       print(rewardAverage, "\t", validAccuracyPerCondition[0], validAccuracyPerCondition[1], "\t", validFixationsPerCondition[0], validFixationsPerCondition[1], file=outFile)

