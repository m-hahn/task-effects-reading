
import glob
files = glob.glob(f"/u/scr/mhahn/Dundee-annotated/run_attention4_dundee.py*")
print(files)
data = []
for f in files:
   if "MERGED" in f:
      continue
   model_id = f[f.rfind("_")+1:]
   with open(f, "r") as inFile:
     inFile = [x for x in inFile.read().strip().split("\n")]
     if len(inFile) > 2:
        data.append((model_id, inFile))

header = data[0][0]
header = dict(list(zip(header, range(len(header)))))

def mean(x):
  return sum(x)/len(x)

HEADER = ["PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "NEAT_Surprisal", "AttProbability", "Attended", "ModelID"]
outpath = f"/u/scr/mhahn/Dundee-annotated/run_attention4_dundee.py_MERGED"
print(outpath)
with open(outpath, "w") as outFile:
 print("\t".join(HEADER), file=outFile)

 for model_id, table in data:
    for line in table:
       print((line+"\t"+model_id), file=outFile)
