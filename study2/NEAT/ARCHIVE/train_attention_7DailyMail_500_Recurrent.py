# train_attention_6.py: THIS one works nice and fast

import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=16) #random.choice([16, 32]))
parser.add_argument('--learning_rate', type=float, default=random.choice([0.02]))
parser.add_argument('--glove', type=bool, default=True)
parser.add_argument('--dropout', type=float, default=0.0)
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))
parser.add_argument('--LAMBDA', type=float, default=2.25) #random.choice([1.5, 1.75, 2, 2.25, 2.5]))
#parser.add_argument('--permuteEntities', type=bool, default=True)

args = parser.parse_args()



vocabulary = [x.split("\t") for x in open(f"vocabularies/{args.corpus}_anonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 
def permuteEntitiesInQ(result):
    if not result["answer"].startswith("@entity"):
      print(result["answer"])
      result["answer"] = result["answer"][result["answer"].index("@entity"):]
    entities = unique([x for x in result["text"]+result["question"]+[result["answer"]] if x.startswith("@entity")])
    entities_mapping = dict(zip(entities, [f"@entity{x}" for x in range(len(entities))]))
    assert len(entities_mapping) == len(entities)
    result["text"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["text"]]
    result["question"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["question"]]
    result["answer"] = entities_mapping[result["answer"]]


TEXT_LENGTH_BOUND = 500

def processQuestion(question, permuteEntities=True):
    question = question.split("##########")
    result = {"text" : question[1].split(" "), "question" : question[2].split(" "), "answer" : question[3]}
    if permuteEntities:
       permuteEntitiesInQ(result)

    answerDistribution[0]+=1
    answerDistribution[1][int(result["answer"].replace("@entity", ""))]+=1

    return result

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
   assert partition in ["test", "training", "validation"]
   with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/cnn_{partition}.txt", "r") as inFile1:
    with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/dailymail_{partition}.txt", "r") as inFile2:
     while True:
        buff = []
        print("Filling buffer...")
        for _ in range(10*args.batchSize):
         try:
           buff.append(processQuestion(next(inFile1).strip(), permuteEntities=permuteEntities))
           buff.append(processQuestion(next(inFile2).strip(), permuteEntities=permuteEntities))
         except StopIteration:
            break
        if len(buff) == 0:
           break
        buff = sorted(buff, key=lambda x:len(x["text"]))
        partitions = [buff[i*args.batchSize:(i+1)*args.batchSize] for i in range(21)]
        random.shuffle(partitions)
        for partition in partitions:
           if len(partition) > 0:
               yield partition
        
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()




text_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()
question_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()


U = torch.nn.Parameter(torch.Tensor(256,256).cuda())
U.data.fill_(0)


output = torch.nn.Linear(256, 600 + 4).cuda()

input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss(reduction="none")
crossEntropy = torch.nn.CrossEntropyLoss(reduction="none")

components_lm = [word_embeddings, text_reader, question_reader, output]



state = torch.load("/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/train_qa_dropout_mask_relabling_attrain_decay_Chosen_Hybrid.py_8585151.ckpt")
print("devAccuraces", state["devAccuracies"])
print("devAccuraces", state["args"])
for i in range(len(components_lm)):
   components_lm[i].load_state_dict(state["components"][i])
U.data = state["U"].data



#parameters_lm_cached = [x for x in parameters_lm()]

linear = torch.nn.Linear(6, 1, bias=False).cuda()
linear.weight.data.zero_()

bilinear = torch.nn.Bilinear(6, 200, 1).cuda()
bilinear.weight.data.zero_()
bilinear.bias.data.zero_()

components_attention = [linear, bilinear]
runningAverageParameter = torch.FloatTensor([0]).cuda()

def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param
 yield U
 for c in components_attention:
   for param in c.parameters():
      yield param
 yield runningAverageParameter

optimizer = torch.optim.SGD(parameters(), lr = args.learning_rate)


def getEntities(x):
    return set([int(y.replace("@entity", "")) for y in x["text"] + x["question"] if y.startswith("@entity")])

featuresAverage = torch.zeros(4).cuda()


def forwardAttention(batch):
    texts = [[numerify(y) for y in x["text"][:TEXT_LENGTH_BOUND]] for x in batch] # [:500]
    questions = [set([numerify(y) for y in x["question"]]) for x in batch]
    text_length = max([len(x) for x in texts])
    position = torch.FloatTensor([list(range(text_length)) for _ in batch]).cuda().t()
    condition = torch.FloatTensor([[0 if i < len(batch)/2 else 1 for _ in range(text_length)] for i in range(len(batch))]).cuda().t()
    condition_ = condition - 0.5
    position = (position)/500 - 0.5
    positionTimesCondition = condition_ * position
    occursInQuestion = torch.FloatTensor([[1 if j < len(texts[i]) and texts[i][j] in questions[i] else 0 for j in range(text_length)] for i in range(len(batch))]).cuda().t() - 0.1
    occursInQuestion = occursInQuestion * condition
    occursInQuestionAverage = torch.FloatTensor([0 for i in range(len(batch))]).cuda()
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)

    texts = torch.LongTensor(texts).cuda().transpose(0,1)
    texts_num = texts
    texts = word_embeddings(texts).detach()
    global featuresAverage
    featuresAverage_ = featuresAverage.clone()
    attentionLogprobabilities = []
    attentionDecisionsList = []
    zeroProbabilities = torch.zeros(len(batch)).cuda()
    runningAverage = torch.nn.functional.sigmoid(runningAverageParameter)
    oneMinus = (1-runningAverage)
    runningAverage_ = torch.cat([ oneMinus*torch.pow(runningAverage, 10-i) for i in range(10)] + [runningAverage], dim=0).unsqueeze(1)
    attentionScores = []
    ones = torch.zeros(text_length, len(batch)).cuda()+1
#    print(featuresAverage_)



 
  
    embedding = texts
    #print(embedding)

    attentionProbability_ = []
    attentionDecisions_ = []
    attentionLogit_ = []
    perWordWeights = torch.matmul(bilinear.weight.squeeze(2), embedding.transpose(1,2)).transpose(1,2)
    #print(perWordWeights.size())
    for j in range(perWordWeights.size()[0]):
       #print(ones[j].size(), position[j].size(), condition_[j].size(), positionTimesCondition[j].size(), occursInQuestion[j].size(), occursInQuestionAverage.size())
       features = torch.stack([ones[j], position[j], condition_[j], positionTimesCondition[j], occursInQuestion[j].detach(), occursInQuestionAverage], dim=1)
       #print(features.size(), perWordWeights.size())
       attentionLogit = (perWordWeights[j] * features).sum(dim=1) + linear(features).squeeze(1)
       #print(perWordWeights[j].size(), features.size(), (perWordWeights[j] * features).sum(dim=1).size(), linear(features).size(), attentionLogit.size())
       #quit()
       #print(attentionLogit)
       attentionProbability = torch.nn.functional.sigmoid(attentionLogit)
       attentionDecisions = torch.bernoulli(attentionProbability)
      
       #print(occursInQuestion[j].size(), attentionDecisions.size())
       occursInQuestionAverage = runningAverage * occursInQuestionAverage + oneMinus * attentionDecisions * occursInQuestion[j].detach()
       #print(attentionDecisions)
       attentionProbability_.append(attentionProbability)
       attentionDecisions_.append(attentionDecisions)
       attentionLogit_.append(attentionLogit)

    attentionProbability = torch.stack(attentionProbability_, dim=0)
    attentionDecisions = torch.stack(attentionDecisions_, dim=0)
    attentionLogit = torch.stack(attentionLogit_, dim=0)
    #quit()
      

#    attentionLogit = linear(features) + bilinear(features, embedding)
    if random.random() < 0.01:
        print(bilinear.weight.size(), embedding.size())
        perWordWeights = torch.matmul(bilinear.weight.squeeze(2), embedding.transpose(1,2))[:10].cpu().detach().numpy()
        print("Linear1", linear.weight.data)
        print("Position\tCondition\tPositionXCondition\tInQuestion")
        for i in range(min(10, len(batch[0]["text"]))):
           print(i, "\t", batch[0]["text"][i], "\t", "\t".join([str(float(x)) for x in perWordWeights[i,:,0]]))

    #print(linear.weight.abs().max())
    #print(bilinear.weight.abs().max())
    #print("attentionLogit", attentionLogit)
    attentionLogProbability = torch.nn.functional.logsigmoid(torch.where(attentionDecisions == 1, attentionLogit, -attentionLogit))
    #print(attentionProbability)
    #print(attentionDecisions) 
#    quit()
    return attentionDecisions, attentionLogProbability, attentionProbability




def forward(batch, calculateAccuracy=False):
    texts = [[numerify(y) for y in x["text"][:TEXT_LENGTH_BOUND]] for x in batch] # [:500]
#    print([len(texts[0]), len(texts[1])])
    questions = [[numerify(y) for y in x["question"]] for x in batch]
    answers = [int(x["answer"].replace("@entity", "")) for x in batch]
    text_length = max([len(x) for x in texts])
    question_length = max([len(x) for x in questions])+2
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    for question in questions:
       while len(question) < question_length:
          question.append(PAD)

    entitiesPerText = [getEntities(x) for x in batch]
    MASK = torch.FloatTensor(len(batch), 600+4)
    MASK.fill_(float("-inf"))
    for i in range(len(batch)):
       for j in entitiesPerText[i]:
          MASK[i,j] = 0
    MASK = MASK.cuda()
#    texts = [[random.randint(1,10) for _ in range(args.batchSize)] for j in range(args.batchSize)]
#    questions = [[j, PLACEHOLDER] for j in range(args.batchSize)] # need to add j
#    answers = [texts[j][j] for j in range(args.batchSize)]
#    data = list(zip(texts, questions, answers))
##    print(min(answers), max(answers))
#    random.shuffle(data)
#    texts, questions, answers = zip(*data)
##    print(answers)
#    questions_raw = questions
    texts =  torch.LongTensor(texts).cuda().transpose(0,1)

    mask, action_logprob, attentionScores = forwardAttention(batch)
    #print(mask)
    #print(action_logprob)
    #print(attentionScores)
#    mask = mask.squeeze(2)
    mask = torch.where(texts == PAD, 1+0*mask, mask)
    fixatedFraction = torch.where(texts == PAD, 0*mask, mask).sum(dim=0) / (texts != PAD).sum(dim=0)
#    print(mask.size(), texts.size())
    texts = torch.where(mask == 1, texts, SKIPPED + 0*texts)
    #print(texts)
    questions = torch.LongTensor(questions).cuda().transpose(0,1)
    answers = torch.LongTensor(answers).cuda()
#    print(texts.max(), texts.min())
    texts = word_embeddings(texts)
    if True:
       questions = word_embeddings(questions)
       output_text, _ = text_reader(texts)
       if not calculateAccuracy and args.dropout > 0:
          texts = input_dropout(texts)
          questions = input_dropout(questions)
       _, question_representations = question_reader(questions)
       #print(output_text.size())
       question_representations = question_representations[0].transpose(0,1).contiguous().view(-1, 256)
       #print(question_representations.size())
   
       part1 = torch.matmul(output_text, U)
       #print("PART1", part1.size(), "Q", question_representations.size())
       question_representations = question_representations.unsqueeze(2) # torch.transpose(question_representations, 0, 1).unsqueeze(1)
       part1 = part1.unsqueeze(2)
       #print("PART1", part1.size(), "Q", question_representations.size())
       attention = torch.matmul(part1, question_representations) # 
       attention = attention.squeeze(2).squeeze(2)
       attention = torch.nn.functional.softmax(attention, dim=0)
#       if random.random() < 0.01:
#          attention0 = attention[:,0].cpu()
#          mask0 = mask[:,0].cpu()
#          attentionScores0 = attentionScores[:,0,0].cpu()
#          for i in range(len(batch[0]["text"][:TEXT_LENGTH_BOUND])):
#             print(i, "\t", batch[0]["text"][i], "\t", attentionScores0[i], "\t", float(mask0[i]), "\t", float(attention0[i]), "\t", batch[0]["answer"], "\t", " ".join(batch[0]["question"]))
#          print(attention)
   #    print(attention[:, 0], questions_raw[0]) # for DEBUGGING
       #print(attention.size(), output_text.size())
   #    print(entityOccurrence.size(), attention.size(), answers.size())
       text_representation =  output((attention.unsqueeze(2) * output_text).sum(dim=0)) + MASK
       loss = crossEntropy(text_representation, answers)
    #assert float(loss.mean()) < 100, loss
    if calculateAccuracy:
       prediction = text_representation.argmax(dim=1)
       return loss, action_logprob, fixatedFraction, float((prediction == answers).float().mean())
    else:
       return loss, action_logprob, fixatedFraction

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

fixationRunningAverageByCondition = [0.5,0.5]
rewardAverage = 3.0
lossAverageByCondition = [2.0, 2.0]

def backward(loss, action_logprob, fixatedFraction, printHere=True):
   global rewardAverage
   if random.random() < 0.99:
     batchSize = fixatedFraction.size()[0]
     fix1, fix2 = float(fixatedFraction[:int(batchSize/2)].mean()), float(fixatedFraction[int(batchSize/2):].mean())
     fixationRunningAverageByCondition[0] = 0.99 * fixationRunningAverageByCondition[0] + (1-0.99) * fix1
     fixationRunningAverageByCondition[1] = 0.99 * fixationRunningAverageByCondition[1] + (1-0.99) * fix2

     loss1, loss2 = float(loss[:int(batchSize/2)].mean()), float(loss[int(batchSize/2):].mean())
     lossAverageByCondition[0] = 0.99 * lossAverageByCondition[0] + (1-0.99) * loss1
     lossAverageByCondition[1] = 0.99 * lossAverageByCondition[1] + (1-0.99) * loss2
     if printHere:
       print("FIXATION RATE", fixationRunningAverageByCondition, "REWARD", rewardAverage, "LossByCondition", lossAverageByCondition)
   optimizer.zero_grad()
   reward = (loss.detach() + args.LAMBDA * fixatedFraction)
   loss_ = 10 * (action_logprob * (reward - rewardAverage)).mean() + loss.mean()
   loss_.backward()
   rewardAverage = 0.99 * rewardAverage + (1-0.99) * float(reward.mean())
   if args.clip_type is not "None":
#      print("NROM MAX", max(p.grad.detach().abs().max().cpu() for p in parameters()))
      torch.nn.utils.clip_grad_norm_(parameters(), args.clip_bound, norm_type=args.clip_type)
   optimizer.step()

import time

expectedEntropy = 0

learning_rate=args.learning_rate

devLosses = []
lossRunningAverage = 6.4
devAccuracies = []
devRewards = []
noImprovement = 0
for epoch in range(10000):
  
  if epoch > 0:
    validLoss = []
    examplesNumber = 0
    validAccuracy = []
    validReward = []
    for batch in  loadQACorpus(args.corpus, "validation", args.batchSize, permuteEntities=True):
     with torch.no_grad():
       loss, action_logprob, fixatedFraction, accuracy = forward(batch, calculateAccuracy = True)
       reward = float((loss.detach() + args.LAMBDA * fixatedFraction).mean())
       loss = float(loss.mean())
       print("VALID", loss, accuracy, examplesNumber, reward)
     validLoss.append(float(loss)*len(batch))
     validReward.append(reward*len(batch))
     examplesNumber += len(batch)
     validAccuracy.append(float(accuracy)*len(batch))
    devLosses.append(sum(validLoss)/examplesNumber)
    devAccuracies.append(sum(validAccuracy)/examplesNumber)
    devRewards.append(sum(validReward)/examplesNumber)
    with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2020/accuracy_{__file__}_{args.myID}.txt", "w") as outFile:
       print(args, file=outFile)
       print(devAccuracies, file=outFile)
       print(devLosses, file=outFile)
       print(devRewards, file=outFile)
       print(fixationRunningAverageByCondition[0], fixationRunningAverageByCondition[1], file=outFile)
       print(rewardAverage, file=outFile)
    if len(devRewards) >1 and devRewards[-1] > devRewards[-2]:
       learning_rate *= 0.8
       optimizer = torch.optim.SGD(parameters(), lr = learning_rate)
       noImprovement += 1
  #  elif len(devAccuracies) > 1 and devAccuracies[-1] < max(devAccuracies):
   #    torch.save({"devAccuracies" : devAccuracies, "devLosses" : devLosses, "args" : args, "components" : [x.state_dict() for x in components_lm], "learning_rate" : learning_rate}, f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/{__file__}_{args.myID}.ckpt")
  
    #   noImprovement = 0
    if noImprovement > 5:
      print("End training, no improvement for 5 epochs")
      break
  words = 0
  timeStart = time.time()
  counter = 0
  for batch in loadQACorpus(args.corpus, "training", args.batchSize, permuteEntities=True):
    counter += 1
    printHere = (counter % 5) == 0
    loss, action_logprob, fixatedFraction = forward(batch)
    backward(loss, action_logprob, fixatedFraction, printHere=printHere)
    loss = float(loss.mean())
    lossRunningAverage = 0.99 *lossRunningAverage + (1-0.99) * float(loss)
    words += len(batch)
    if printHere:
       print(devAccuracies)
       print(devLosses)
       print(devRewards)
       print(float(loss), lossRunningAverage, expectedEntropy, args, __file__)
       print(words/(time.time()-timeStart), "questions per second")
    if expectedEntropy == 0 or (answerDistribution[0] < 20000 and random.random() < 0.05):
       dist = torch.FloatTensor(answerDistribution[1])
       dist = dist / dist.sum()
       expectedEntropy = float(-(dist*(dist+1e-10).log()).sum())
      

