
FILE = "merged.tsv"
data = read.csv(paste("/u/scr/mhahn/NEURAL_ATTENTION_TASK/OUTPUTS/",FILE, sep=""), sep="\t")  
data = data[!data$IsOOV,]
summary(data)
data$Condition.C = (data$Condition == "Preview")
data$Condition.C = data$Condition.C - 0.5
data$LogWordFreq = (data$LogWordFreq - mean(data[!data$IsNamedEntity,]$LogWordFreq, na.rm=TRUE))
data[data$IsNamedEntity,]$LogWordFreq = 0
data$LogWordFreq = data$LogWordFreq/sd(data$LogWordFreq, na.rm=TRUE)
data$Position = (data$Position - mean(data$Position))/sd(data$Position)
data$IsCorrectAnswer.C = residuals(lm(IsCorrectAnswer~IsNamedEntity, data=data))
data$ID = paste0(data$Text, "_", data$Position)


library(dplyr)
library(tidyr)
library(brms)


data2 = rbind(data)
data2$IsCorrectAnswer.C = round(data2$IsCorrectAnswer.C, 2)
data2$Attended_1 = c(FALSE, data2$Attended[1:(nrow(data2)-1)])
data2$Attended_2 = c(FALSE, FALSE, data2$Attended[1:(nrow(data2)-2)])
data2$Attended_3 = c(FALSE, FALSE, FALSE, data2$Attended[1:(nrow(data2)-3)])
data2$Attended_4 = c(FALSE, FALSE, FALSE, FALSE, data2$Attended[1:(nrow(data2)-4)])
data2$Attended_M1 = c(data2$Attended[2:nrow(data2)], FALSE)
data2$Attended_M2 = c(data2$Attended[3:nrow(data2)], FALSE, FALSE)
data2$Attended_M3 = c(data2$Attended[4:nrow(data2)], FALSE, FALSE, FALSE)
data2$Attended_M4 = c(data2$Attended[5:nrow(data2)], FALSE, FALSE, FALSE, FALSE)

data2 = data2 %>% mutate(AttendedNeighborhood = (Attended_1 + Attended_2 + Attended_3 + Attended_4 + Attended_M1 + Attended_M2 + Attended_M3 + Attended_M4 + Attended)/9)


data2 = data2 %>% mutate(AttendedNeighborhood = (Attended_1 + Attended_2 + Attended_M1 + Attended_M2 + Attended)/9)


data2 %>% filter(IsNamedEntity) %>% group_by(IsCorrectAnswer, Condition) %>% summarise(AttendedNeighborhood = mean(AttendedNeighborhood))

library(ggplot2)
plot = ggplot(data2 %>% filter(IsNamedEntity) %>% group_by(IsCorrectAnswer, Condition) %>% summarise(AttendedNeighborhood = mean(AttendedNeighborhood)), aes(x=IsCorrectAnswer, y=AttendedNeighborhood, color=Condition)) + geom_line()
ggsave(plot, file="neighborhood1.pdf")



#########################################
library(lme4)
summary(lmer(AttendedNeighborhood ~ IsCorrectAnswer.C * Condition.C + (1|ID) + (1|ModelID), data=data2 %>% filter(IsNamedEntity)))

library(brms)
summary(brm(AttendedNeighborhood ~ IsCorrectAnswer.C * Condition.C + (1+Condition.C|ID) + (1 + IsCorrectAnswer.C + Condition.C + IsCorrectAnswer.C * Condition.C|ModelID), data=data2 %>% filter(IsNamedEntity)))
#########################################

