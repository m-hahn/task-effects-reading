# Trying to replicate old simulations.
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_2.py_88916881.ckpt.txt" # LogWordFreq, Condition effects as expected, But Cond*LogWF inverse. LAMBDA=1.0
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_3.py_90279247.ckpt.txt" # LogWF effect as expected, Cond*LogF almost absent. LAMBDA=1.0
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_4.py_6933855.ckpt.txt" # LogWF and Cod*LogWF as expected. LAMBDA=0.5
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_4.py_86021010.ckpt.txt"  # LogWF as expected, Cond*LogF inverse. LAMBDA=0.5
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_5.py_48830253.ckpt.txt" # LogWF as expected, no Cond*LogWF. LAMBDA=1.0
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save.py_15166302.ckpt.txt" # LogWF inverse, Cnd*LogWF inverse. LAMBDA=0.5
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save.py_5364461.ckpt.txt" # LogWF inverse, no Cnd*LogWF. LAMBDA=0.5
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save.py_68084917.ckpt.txt" # LogWF, Cnd*LogWF as expected. LAMBDA=1.5
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save.py_76906648.ckpt.txt" # LogWF inverse. LAMBDA=0.5
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save.py_78256753.ckpt.txt"# LogWF, but Cnd*LogWF inverse. LAMBDA=1.0
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save.py_79271746.ckpt.txt" # LogWordF, Cnd*LogWF as expected. LAMBDA=2.0
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save.py_91173090.ckpt.txt" # LogWordFreq, Cnd*LogWF as expected. LAMBDA=1.5
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_6.py_73903837.ckpt.txt" # LogWF reverse, Cond*LogWF reverse. LAMBDA=0.5, FIX 0.45
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_6.py_79060983.ckpt.txt" # LogWF reverse, no Cond*LogWF (had not trained for long enough to interpret)
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_6.py_81622850.ckpt.txt" # LogWF reverse, Cond*LogWF as expected. LAMBDA=0.5, FIX 0.42
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_7.py_26170674.ckpt.txt" # LogWF and Cond*LogWF as expected. ENTROPY 0.0001, LAMBDA=2.0, FIX 0.17
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_7.py_2663878.ckpt.txt" # LogWF reverse, no Cond*LogWF. ENTROPY 0.0001, LAMBDA=0.5, FIX 0.5
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_7.py_43501854.ckpt.txt" # LogWF as expected, no Cond*LogWF. ENTROPY 0.1, LAMBDA=0.5, FIX 0.53
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_7.py_6059446.ckpt.txt" # LogWF, Cond*IsNE, Cond*LogWF as expected. ENTROPY 0.1, LAMBDA=2.5, FIX 0.4
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_7.py_61217359.ckpt.txt" # no LogWF effect, no Cond*LogWF, but Cond*IsNE. ENTROPY=0.1, LAMBDA=2.0, FIX 0.45
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_7.py_68182375.ckpt.txt" # LogWF effect as expected, LogWF*Cond reverse. ENTROPY=0.01, LAMBDA=1.5, FIX 0.25


#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9.py_16501924.ckpt.txt" # LogWF, Cond*LogWF as expected but weak
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9.py_17117518.ckpt.txt" # LogWF, Cond*LogWF as expected but weak
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9.py_3806447.ckpt.txt" # LogWF as expected, Condition*LogWF weak
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9.py_7090982.ckpt.txt" # LogWF as expected, COndition*LogWF as expected and strong
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9.py_72681138.ckpt.txt" # LogWF as expected, no Cond*LogWF
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9.py_84575894.ckpt.txt" # LogWF as expected, Cond*LogWF as expected but small
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9.py_85317681.ckpt.txt" # LogWF, LogWF*Cond both inverse



#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_46097535.ckpt.txt" # LogWF inerse, Cond*LogWF absent. ENT 0.001, LAMBDA 0.5, Fix 0.5
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_50912404.ckpt.txt" # LogWF as expected, but Cond*LogWF absent. ENT 0.01, LAMBDA 2.0, Fix 0.2
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_56290015.ckpt.txt" # LogWF, Cond*LogWF as expected. ENT 0.01, LABDA 1.0, Fix 0.4
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_61330156.ckpt.txt" # LogWF, Cond*LogWF expected direction. ENT 0.01, LAMBDA 2.5, Fix 0.15
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_69505530.ckpt.txt" # LogWF, Cond*LogWF expected direction. ENT 0.001, LAMBDA 1.5, Fix 0.2
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_71511816.ckpt.txt" # LogWF unexpected direction. ENT 0.1, LAMBDA 1.5, Fix 0.5
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_92170074.ckpt.txt" # small effects. ENT 0.1, LAMBDA 1.0, Fix 0.5

#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_28977994.ckpt.txt"
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_50969041.ckpt.txt" #  LogWF, Cond*LogWF as expected, also strong Cond*IsNE
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_56783757.ckpt.txt" # LogWF, Cond*LogWF as expected
#FILE = "TMP_train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py_60268495.ckpt.txt" # LogWF, Cond*LogWF as expected




data = read.csv(paste("/u/scr/mhahn/NEURAL_ATTENTION_TASK/OUTPUTS/",FILE, sep=""), sep="\t")  
data = data[!data$IsOOV,]
summary(data)
data$Condition.C = (data$Condition == "Preview")
data$Condition.c = data$Condition.C - 0.5
data$Condition.C = data$Condition.C - 0.5
data$LogWordFreq = (data$LogWordFreq - mean(data$LogWordFreq, na.rm=TRUE))/sd(data$LogWordFreq, na.rm=TRUE)
data[data$IsNamedEntity,]$LogWordFreq = 0
data$Position = (data$Position - mean(data$Position))/sd(data$Position)
data$IsCorrectAnswer.C = residuals(lm(IsCorrectAnswer~IsNamedEntity, data=data))
summary(glm(Attended ~ Condition.C*IsCorrectAnswer.C + Condition.C*IsNamedEntity +Condition.C*LogWordFreq, family="binomial", data=data))
summary(lm(AttentionLogit ~ Condition.C*IsCorrectAnswer.C + Condition.C*IsNamedEntity +Condition.C*LogWordFreq, data=data))

#summary(glm(Attended ~ Position*Condition.C + Condition.C*IsCorrectAnswer.C + Condition.C*IsNamedEntity +Condition.C*LogWordFreq + LogWordFreq*Position + Position*IsNamedEntity, family="binomial", data=data))
#summary(glm(AttentionLogit ~ Position*Condition.C + Condition.C*IsCorrectAnswer.C + Condition.C*IsNamedEntity +Condition.C*LogWordFreq + LogWordFreq*Position + Position*IsNamedEntity, data=data))  
#savehistory("Rhistory.R")
