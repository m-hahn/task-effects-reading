
FILE = "merged.tsv"
data = read.csv(paste("/u/scr/mhahn/NEURAL_ATTENTION_TASK/OUTPUTS/",FILE, sep=""), sep="\t")  
data = data[!data$IsOOV,]
summary(data)
data$Condition.C = (data$Condition == "Preview")
data$Condition.C = data$Condition.C - 0.5
data$LogWordFreq = (data$LogWordFreq - mean(data[!data$IsNamedEntity,]$LogWordFreq, na.rm=TRUE))
data[data$IsNamedEntity,]$LogWordFreq = 0
data$LogWordFreq = data$LogWordFreq/sd(data$LogWordFreq, na.rm=TRUE)
data$Position = (data$Position - mean(data$Position))/sd(data$Position)
data$IsCorrectAnswer.C = residuals(lm(IsCorrectAnswer~IsNamedEntity, data=data))
data$ID = paste0(data$Text, "_", data$Position)


library(dplyr)
library(tidyr)
library(brms)


data2 = rbind(data)
data2$IsCorrectAnswer.C = round(data2$IsCorrectAnswer.C, 2)
data2$IsCorrectAnswer_1 = c(FALSE, data2$IsCorrectAnswer.C[1:(nrow(data2)-1)])
data2$IsCorrectAnswer_2 = c(FALSE, FALSE, data2$IsCorrectAnswer.C[1:(nrow(data2)-2)])
data2$IsCorrectAnswer_3 = c(FALSE, FALSE, FALSE, data2$IsCorrectAnswer.C[1:(nrow(data2)-3)])
data2$IsCorrectAnswer_4 = c(FALSE, FALSE, FALSE, FALSE, data2$IsCorrectAnswer.C[1:(nrow(data2)-4)])
data2$IsCorrectAnswer_M1 = c(data2$IsCorrectAnswer.C[2:nrow(data2)], FALSE)
data2$IsCorrectAnswer_M2 = c(data2$IsCorrectAnswer.C[3:nrow(data2)], FALSE, FALSE)
data2$IsCorrectAnswer_M3 = c(data2$IsCorrectAnswer.C[4:nrow(data2)], FALSE, FALSE, FALSE)
data2$IsCorrectAnswer_M4 = c(data2$IsCorrectAnswer.C[5:nrow(data2)], FALSE, FALSE, FALSE, FALSE)

data2 = data2 %>% mutate(IsCorrectAnswerNeighborhood = IsCorrectAnswer_1 + IsCorrectAnswer_2 + IsCorrectAnswer_3 + IsCorrectAnswer_4 + IsCorrectAnswer_M1 + IsCorrectAnswer_M2 + IsCorrectAnswer_M3 + IsCorrectAnswer_M4 + IsCorrectAnswer)
data2 = data2 %>% mutate(s_IsCorrectAnswerNeighborhood = sign(IsCorrectAnswerNeighborhood))
data2 = data2 %>% mutate(s_IsCorrectAnswerNeighborhood = s_IsCorrectAnswerNeighborhood - mean(s_IsCorrectAnswerNeighborhood))

#########################################
summary(glm(Attended ~ IsCorrectAnswerNeighborhood * Condition.C, family="binomial", data=data2))
#########################################

summary(lm(AttentionLogit ~ Condition.C*IsCorrectAnswerNeighborhood + Condition.C*IsNamedEntity +Condition.C*LogWordFreq, data=data2))
summary(glm(Attended ~ Condition.C*IsCorrectAnswerNeighborhood + Condition.C*IsNamedEntity +Condition.C*LogWordFreq, family="binomial", data=data2))


library(ggplot2)
plot = ggplot(data2 %>% group_by(s_IsCorrectAnswerNeighborhood, Condition) %>% filter(s_IsCorrectAnswerNeighborhood != 0) %>% summarise(Attended = mean(Attended)), aes(x=s_IsCorrectAnswerNeighborhood, y=Attended, color=Condition)) + geom_line()
ggsave(plot, file="neighborhood1.pdf")
plot = ggplot(data2 %>% group_by(IsCorrectAnswerNeighborhood, Condition) %>% summarise(Attended = mean(Attended)), aes(x=IsCorrectAnswerNeighborhood, y=Attended, color=Condition)) + geom_line()
ggsave(plot, file="neighborhood2.pdf")

library(brms)
model = (brm(Attended ~ Condition.C*IsCorrectAnswerNeighborhood + Condition.C*IsNamedEntity +Condition.C*LogWordFreq + (1|ID) + (1|ModelID), family="bernoulli", data=data2 %>% filter(as.numeric(Text) < 40)))
#Population-Level Effects: 
#                                            Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                                      -0.83      0.01    -0.85    -0.80       1707 1.00
#Condition.CTRUE                                -0.26      0.01    -0.28    -0.23       4000 1.00
#IsCorrectAnswerNeighborhood                    -0.09      0.04    -0.17    -0.01       1150 1.00
#IsNamedEntityTRUE                               7.47      0.22     7.04     7.92       2010 1.00
#LogWordFreq                                    -0.15      0.01    -0.17    -0.12       1155 1.00
#Condition.CTRUE:IsCorrectAnswerNeighborhood     0.03      0.04    -0.05     0.11       3858 1.00
#Condition.CTRUE:IsNamedEntityTRUE              -0.33      0.27    -0.88     0.20       2300 1.00
#Condition.CTRUE:LogWordFreq                     0.09      0.01     0.07     0.11       3899 1.00


library(brms)
summary(brm(Attended ~ Condition.C + Condition.C+IsNamedEntity +Condition.C*LogWordFreq + (1|ModelID) + (1|ID), family="bernoulli", data=data %>% filter(as.numeric(Text) < 10)))
#                            Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                      -0.83      0.03    -0.88    -0.77       2543 1.00
#Condition.CTRUE                -0.32      0.02    -0.36    -0.27       4000 1.00
#IsNamedEntityTRUE               7.30      0.29     6.76     7.91       4000 1.00
#LogWordFreq                    -0.15      0.02    -0.19    -0.11       1816 1.00
#Condition.CTRUE:LogWordFreq     0.07      0.02     0.03     0.11       4000 1.00


summary(brm(Attended ~ Condition.C + Condition.C+IsNamedEntity +Condition.C*LogWordFreq + (1+Condition.C+IsNamedEntity+LogWordFreq + Condition.C+IsNamedEntity +Condition.C*LogWordFreq|ModelID) + (1|ID), family="bernoulli", data=data %>% filter(as.numeric(Text) < 10))) # TODO condition slopes on ID are missing
# Redone with 19 models
#                        Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  -0.97      0.03    -1.02    -0.92        780 1.00
#Condition.C                -0.32      0.02    -0.37    -0.28       1866 1.00
#IsNamedEntityTRUE           7.43      0.24     7.02     7.96       2460 1.00
#LogWordFreq                -0.07      0.02    -0.12    -0.03        892 1.00
#Condition.C:LogWordFreq     0.04      0.02     0.01     0.07       2895 1.00


summary(brm(Attended ~ Condition.C + Condition.C+IsNamedEntity +Condition.C*LogWordFreq + (1+Condition.C+IsNamedEntity+LogWordFreq + Condition.C+IsNamedEntity +Condition.C*LogWordFreq|ModelID) + (1|ID), family="bernoulli", data=data %>% filter(as.numeric(Text) < 20)))
#                            Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                      -0.85      0.03    -0.90    -0.79       1464 1.00
#Condition.CTRUE                -0.31      0.05    -0.40    -0.22        968 1.00
#IsNamedEntityTRUE               7.56      0.38     6.89     8.44       1047 1.00
#LogWordFreq                    -0.16      0.07    -0.32    -0.03        253 1.01
#Condition.CTRUE:LogWordFreq     0.07      0.05    -0.03     0.17       1482 1.00

summary(brm(Attended ~ Condition.C + Condition.C+IsNamedEntity +Condition.C*LogWordFreq + (1+Condition.C+IsNamedEntity+LogWordFreq + Condition.C+IsNamedEntity +Condition.C*LogWordFreq|ModelID) + (1|ID), family="bernoulli", data=data %>% filter(as.numeric(Text) < 40)))
#                            Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                      -0.83      0.02    -0.88    -0.78       1715 1.00
#Condition.CTRUE                -0.25      0.05    -0.36    -0.14        652 1.00
#IsNamedEntityTRUE               7.29      0.21     6.90     7.70       1066 1.00
#LogWordFreq                    -0.15      0.05    -0.25    -0.04       1239 1.00
#Condition.CTRUE:LogWordFreq     0.09      0.05    -0.01     0.20        895 1.00


summary(brm(Attended ~ Condition.C + Condition.C+IsNamedEntity +Condition.C*LogWordFreq + (1+Condition.C+IsNamedEntity+LogWordFreq + Condition.C+IsNamedEntity +Condition.C*LogWordFreq|ModelID) + (1|ID), family="bernoulli", data=data %>% filter(as.numeric(Text) < 60))) 
# (using 18 models)
#                        Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  -0.95      0.02    -0.99    -0.92        831 1.00
#Condition.C                -0.29      0.02    -0.33    -0.26       1736 1.00
#IsNamedEntityTRUE           7.34      0.10     7.15     7.55       4000 1.00
#LogWordFreq                -0.07      0.02    -0.11    -0.04       1734 1.00
#Condition.C:LogWordFreq     0.05      0.02     0.01     0.09       2666 1.00


summary(glm(Attended ~ Condition.C*IsCorrectAnswer.C + Condition.C*IsNamedEntity +Condition.C*LogWordFreq, family="binomial", data=data))
summary(lm(AttentionLogit ~ Condition.C*IsCorrectAnswer.C + Condition.C*IsNamedEntity +Condition.C*LogWordFreq, data=data))


summary(lmer(AttentionLogit ~ Condition.C + Condition.C+IsNamedEntity +Condition.C*LogWordFreq + (1|ModelID) + (1|ID), data=data %>% filter(as.numeric(Text) < 40)))
summary(glmer(Attended ~ Condition.C + Condition.C+IsNamedEntity +Condition.C*LogWordFreq + (1|ModelID) + (1|ID), family="binomial", data=data %>% filter(as.numeric(Text) < 10)))


#summary(glm(Attended ~ Position*Condition.C + Condition.C*IsCorrectAnswer.C + Condition.C*IsNamedEntity +Condition.C*LogWordFreq + LogWordFreq*Position + Position*IsNamedEntity, family="binomial", data=data))
#summary(glm(AttentionLogit ~ Position*Condition.C + Condition.C*IsCorrectAnswer.C + Condition.C*IsNamedEntity +Condition.C*LogWordFreq + LogWordFreq*Position + Position*IsNamedEntity, data=data))  
#savehistory("Rhistory.R")
