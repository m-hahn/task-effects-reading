import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=2)
parser.add_argument('--learning_rate', type=float, default=0.1)

args = parser.parse_args()



vocabulary = [x.split("\t") for x in open(f"vocabularies/{args.corpus}_anonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])


def permuteEntitiesInQ(result):
    entities = list(set([x for x in result["text"]+result["question"]+[result["answer"]] if x.startswith("@entity")]))
    entities_mapping = dict(zip(entities, [f"@entity{x}" for x in random.sample(range(600), len(entities))]))
    assert len(entities_mapping) == len(entities)
    result["text"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["text"]]
    result["question"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["question"]]
    result["answer"] = entities_mapping[result["answer"]]



def processQuestion(question, permuteEntities=True):
    question = question.split("##########")
    result = {"text" : question[1].split(" "), "question" : question[2].split(" "), "answer" : question[3]}
    if permuteEntities:
       permuteEntitiesInQ(result)
    return result

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
   assert partition in ["test", "training", "validation"]
   with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/{corpus}_{partition}.txt", "r") as inFile:
     while True:
        buff = []
        print("Filling buffer...")
        for _ in range(20*args.batchSize):
         try:
           buff.append(processQuestion(next(inFile).strip(), permuteEntities=permuteEntities))
         except StopIteration:
            break
        if len(buff) == 0:
           break
        buff = sorted(buff, key=lambda x:len(x["text"]))
        partitions = [buff[i*args.batchSize:(i+1)*args.batchSize] for i in range(21)]
        random.shuffle(partitions)
        for partition in partitions:
           if len(partition) > 0:
               yield partition
        
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 10).cuda()
text_reader = torch.nn.LSTM(10, 16, 1, bidirectional=True).cuda()
question_reader = torch.nn.LSTM(10, 16, 1, bidirectional=True).cuda()


U = torch.nn.Parameter(torch.Tensor(32,32).cuda())
U.data.fill_(0)


output = torch.nn.Linear(32, 10 + 4).cuda()

crossEntropy = torch.nn.CrossEntropyLoss()

components_lm = [word_embeddings, text_reader, question_reader, output]

def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param
 yield U

#parameters_lm_cached = [x for x in parameters_lm()]


optimizer = torch.optim.SGD(parameters(), lr = args.learning_rate)





def forward(batch):
    texts = [[numerify(y) for y in x["text"]] for x in batch] # [:500]
#    print([len(texts[0]), len(texts[1])])
    questions = [[numerify(y) for y in x["question"]] for x in batch]
    answers = [int(x["answer"].replace("@entity", "")) for x in batch]
    text_length = max([len(x) for x in texts])
    question_length = max([len(x) for x in texts])
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    for question in questions:
       while len(question) < question_length:
          question.append(PAD)
    texts = [[random.randint(1,10) for _ in range(args.batchSize)] for j in range(args.batchSize)]
    questions = [[random.randint(args.batchSize+1, 1000) for _ in range(20)] + [j, PLACEHOLDER] for j in range(args.batchSize)] # need to add j
    for i in range(len(questions)):
        random.shuffle(questions[i])
    answers = [texts[j][j] for j in range(args.batchSize)]
    data = list(zip(texts, questions, answers))
#    print(min(answers), max(answers))
    random.shuffle(data)
    texts, questions, answers = zip(*data)
#    print(answers)
    questions_raw = questions
    texts =  torch.LongTensor(texts).cuda().transpose(0,1)
    questions = torch.LongTensor(questions).cuda().transpose(0,1)
    answers = torch.LongTensor(answers).cuda()
#    print(texts.max(), texts.min())
    texts = word_embeddings(texts)
    questions = word_embeddings(questions)
    output_text, _ = text_reader(texts)
    _, question_representations = question_reader(questions)
    #print(output_text.size())
    question_representations = question_representations[0].transpose(0,1).contiguous().view(-1, 32)
    #print(question_representations.size())

    part1 = torch.matmul(output_text, U)
    #print("PART1", part1.size(), "Q", question_representations.size())
    question_representations = question_representations.unsqueeze(2) # torch.transpose(question_representations, 0, 1).unsqueeze(1)
    part1 = part1.unsqueeze(2)
    #print("PART1", part1.size(), "Q", question_representations.size())
    attention = torch.matmul(part1, question_representations) # 
    attention = attention.squeeze(2).squeeze(2)
    attention = torch.nn.functional.softmax(attention, dim=0)
    print(attention[:, 0]) # for DEBUGGING
    #print(attention.size(), output_text.size())
    text_representation =  output((attention.unsqueeze(2) * output_text).sum(dim=0))
    loss = crossEntropy(text_representation, answers)
    return loss

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

def backward(loss):
   optimizer.zero_grad()
   loss.backward()
   if args.clip_type is not "None":
#      print("NROM MAX", max(p.grad.detach().abs().max().cpu() for p in parameters()))
      torch.nn.utils.clip_grad_norm_(parameters(), args.clip_bound, norm_type=args.clip_type)
   optimizer.step()

devLosses = []
for epoch in range(10000):


  validLoss = []
  examplesNumber = 0
  for batch in  loadQACorpus(args.corpus, "validation", args.batchSize, permuteEntities=False):
   with torch.no_grad():
     loss = forward(batch)
     print("VALID", loss, examplesNumber)
   validLoss.append(float(loss)*len(batch))
   examplesNumber += len(batch)
  devLosses.append(sum(validLoss)/examplesNumber)

  for batch in loadQACorpus(args.corpus, "training", args.batchSize, permuteEntities=True):
    loss = forward(batch)
    backward(loss)
    print(float(loss), devLosses, args, __file__)


