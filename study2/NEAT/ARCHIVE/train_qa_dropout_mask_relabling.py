import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=random.choice([16, 32]))
parser.add_argument('--learning_rate', type=float, default=random.choice([0.01, 0.05, 0.1, 0.2, 1.0]))
parser.add_argument('--glove', type=bool, default=True)
parser.add_argument('--permuteEntities', type=bool, default=True)

args = parser.parse_args()



vocabulary = [x.split("\t") for x in open(f"vocabularies/{args.corpus}_anonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 
def permuteEntitiesInQ(result):
    entities = unique([x for x in result["text"]+result["question"]+[result["answer"]] if x.startswith("@entity")])
    entities_mapping = dict(zip(entities, [f"@entity{x}" for x in range(len(entities))]))
    assert len(entities_mapping) == len(entities)
    result["text"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["text"]]
    result["question"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["question"]]
    result["answer"] = entities_mapping[result["answer"]]



def processQuestion(question, permuteEntities=True):
    question = question.split("##########")
    result = {"text" : question[1].split(" "), "question" : question[2].split(" "), "answer" : question[3]}
    if permuteEntities:
       permuteEntitiesInQ(result)

    answerDistribution[0]+=1
    answerDistribution[1][int(result["answer"].replace("@entity", ""))]+=1

    return result

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
   assert partition in ["test", "training", "validation"]
   with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/{corpus}_{partition}.txt", "r") as inFile:
     while True:
        buff = []
        print("Filling buffer...")
        for _ in range(20*args.batchSize):
         try:
           buff.append(processQuestion(next(inFile).strip(), permuteEntities=permuteEntities))
         except StopIteration:
            break
        if len(buff) == 0:
           break
        buff = sorted(buff, key=lambda x:len(x["text"]))
        partitions = [buff[i*args.batchSize:(i+1)*args.batchSize] for i in range(21)]
        random.shuffle(partitions)
        for partition in partitions:
           if len(partition) > 0:
               yield partition
        
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()


if args.glove:
 with open("/u/scr/mhahn/glove/glove.6B.200d.txt", "r") as inFile:
  print("Loading embeddings")
  counter = 0
  for line in inFile:
    counter += 1
    line = next(inFile).strip().split(" ")
    word = line[0]
    if word in stoi and stoi[word] < 50000:
       #print(stoi[word])
       embedding = torch.FloatTensor([float(x) for x in line[1:]])
       word_embeddings.weight.data[stoi[word]+4] = embedding
       # print(counter, word)
    if counter > 100000:
      break
  print("Done loading embeddings")
 

text_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()
question_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()


U = torch.nn.Parameter(torch.Tensor(256,256).cuda())
U.data.fill_(0)


output = torch.nn.Linear(256, 600 + 4).cuda()

input_dropout = torch.nn.Dropout(0.2)

crossEntropy = torch.nn.CrossEntropyLoss()

components_lm = [word_embeddings, text_reader, question_reader, output]

def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param
 yield U

#parameters_lm_cached = [x for x in parameters_lm()]


optimizer = torch.optim.SGD(parameters(), lr = args.learning_rate)




def getEntities(x):
    return set([int(y.replace("@entity", "")) for y in x["text"] + x["question"] if y.startswith("@entity")])

def forward(batch, calculateAccuracy=False):
    texts = [[numerify(y) for y in x["text"]] for x in batch] # [:500]
#    print([len(texts[0]), len(texts[1])])
    questions = [[numerify(y) for y in x["question"]] for x in batch]
    answers = [int(x["answer"].replace("@entity", "")) for x in batch]
    text_length = max([len(x) for x in texts])
    question_length = max([len(x) for x in texts])
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    for question in questions:
       while len(question) < question_length:
          question.append(PAD)


    entitiesPerText = [getEntities(x) for x in batch]
    MASK = torch.FloatTensor(len(batch), 600+4)
    MASK.fill_(float("-inf"))
    for i in range(len(batch)):
       for j in entitiesPerText[i]:
          MASK[i,j] = 0
    MASK = MASK.cuda()
#    texts = [[random.randint(1,10) for _ in range(args.batchSize)] for j in range(args.batchSize)]
#    questions = [[j, PLACEHOLDER] for j in range(args.batchSize)] # need to add j
#    answers = [texts[j][j] for j in range(args.batchSize)]
#    data = list(zip(texts, questions, answers))
##    print(min(answers), max(answers))
#    random.shuffle(data)
#    texts, questions, answers = zip(*data)
##    print(answers)
#    questions_raw = questions
    texts =  torch.LongTensor(texts).cuda().transpose(0,1)
    questions = torch.LongTensor(questions).cuda().transpose(0,1)
    answers = torch.LongTensor(answers).cuda()
#    print(texts.max(), texts.min())
    texts = word_embeddings(texts)
    questions = word_embeddings(questions)
    output_text, _ = text_reader(texts)
    if not calculateAccuracy:
       texts = input_dropout(texts)
       questions = input_dropout(questions)
    _, question_representations = question_reader(questions)
    #print(output_text.size())
    question_representations = question_representations[0].transpose(0,1).contiguous().view(-1, 256)
    #print(question_representations.size())

    part1 = torch.matmul(output_text, U)
    #print("PART1", part1.size(), "Q", question_representations.size())
    question_representations = question_representations.unsqueeze(2) # torch.transpose(question_representations, 0, 1).unsqueeze(1)
    part1 = part1.unsqueeze(2)
    #print("PART1", part1.size(), "Q", question_representations.size())
    attention = torch.matmul(part1, question_representations) # 
    attention = attention.squeeze(2).squeeze(2)
    attention = torch.nn.functional.softmax(attention, dim=0)
    if random.random() < 0.01:
       attention0 = attention[:,0].cpu()
       for i in range(len(batch[0]["text"])):
          print(i, "\t", batch[0]["text"][i], "\t", float(attention0[i]), "\t", batch[0]["answer"], "\t", " ".join(batch[0]["question"]))
       print(attention)
#    print(attention[:, 0], questions_raw[0]) # for DEBUGGING
    #print(attention.size(), output_text.size())
    text_representation =  output((attention.unsqueeze(2) * output_text).sum(dim=0)) + MASK
    loss = crossEntropy(text_representation, answers)
    if calculateAccuracy:
       prediction = text_representation.argmax(dim=1)
       return loss, float((prediction == answers).float().mean())
    else:
       return loss

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

def backward(loss):
   optimizer.zero_grad()
   loss.backward()
   if args.clip_type is not "None":
#      print("NROM MAX", max(p.grad.detach().abs().max().cpu() for p in parameters()))
      torch.nn.utils.clip_grad_norm_(parameters(), args.clip_bound, norm_type=args.clip_type)
   optimizer.step()

expectedEntropy = 0

devLosses = []
lossRunningAverage = 6.4
devAccuracies = []
for epoch in range(10000):


  validLoss = []
  examplesNumber = 0
  validAccuracy = []
  for batch in  loadQACorpus(args.corpus, "validation", args.batchSize, permuteEntities=True):
   with torch.no_grad():
     loss, accuracy = forward(batch, calculateAccuracy = True)
     print("VALID", loss, examplesNumber)
   validLoss.append(float(loss)*len(batch))
   examplesNumber += len(batch)
   validAccuracy.append(float(accuracy)*len(batch))
  devLosses.append(sum(validLoss)/examplesNumber)
  devAccuracies.append(sum(validAccuracy)/examplesNumber)

  for batch in loadQACorpus(args.corpus, "training", args.batchSize, permuteEntities=args.permuteEntities):
    loss = forward(batch)
    backward(loss)
    lossRunningAverage = 0.99 *lossRunningAverage + (1-0.99) * float(loss)
    print(devAccuracies)
    print(devLosses)
    print(float(loss), lossRunningAverage, expectedEntropy, args, __file__)
    if expectedEntropy == 0 or (answerDistribution[0] < 20000 and random.random() < 0.05):
       dist = torch.FloatTensor(answerDistribution[1])
       dist = dist / dist.sum()
       expectedEntropy = float(-(dist*(dist+1e-10).log()).sum())
      

