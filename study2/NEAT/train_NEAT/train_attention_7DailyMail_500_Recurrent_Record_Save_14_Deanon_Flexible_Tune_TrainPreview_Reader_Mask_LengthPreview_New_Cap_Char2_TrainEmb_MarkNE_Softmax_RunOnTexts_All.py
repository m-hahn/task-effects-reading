import glob
import subprocess
files = glob.glob(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2021/*{__file__.replace('_RunOnTexts_All', '')}_*.txt")
files += glob.glob(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2021/*{__file__.replace('_RunOnTexts_All.py', '')}_Baseline.py*.txt")
files += glob.glob(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2021/*{__file__.replace('_RunOnTexts_All.py', '')}_Baseline2.py*.txt")
files += glob.glob(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2021/*{__file__.replace('_RunOnTexts_All.py', '')}_Continue.py*.txt")
header = None
print(files)
for f in files:
 # try:
   with open(f, "r") as inFile:
       args = next(inFile).strip()
       accuracies = next(inFile).strip()
       next(inFile)
       rewards = [float(x) for x in next(inFile).strip()[1:-1].split(", ") ]
       next(inFile)
       tradeoff = next(inFile).strip()
   print(args)
   if  True:
#          print(tradeoff, len(rewards))
          print(f)
          modelNumber = f[f.rfind("/")+1:].replace("accuracy_", "").replace(".txt", ".ckpt")
#          quit()
 #         modelNumber = f[f.rfind("_")+1:][:-4]
          print(modelNumber)
          subprocess.call(["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", __file__.replace("_All.py", ".py"), "--LOAD_CKPT="+modelNumber])

