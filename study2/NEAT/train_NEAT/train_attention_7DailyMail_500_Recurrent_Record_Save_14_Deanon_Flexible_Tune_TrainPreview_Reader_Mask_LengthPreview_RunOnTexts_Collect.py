import glob
import subprocess
files = glob.glob("/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2021/*LengthPreview.py_*.txt")
header = None
print(files)
with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/OUTPUTS_TEXTS/{__file__}.tsv", "w") as outFile:
 output = ["ModelNumber", "Position"]
 output.append("Text")
 output.append("Word")
 output.append("Attended")
 output.append("IsCorrectAnswer")
 output.append("Condition")
 output.append("IsOOV")
 output.append("IsNamedEntity")
 output.append("AttentionLogit")
 output.append("LogWordFreq")
 print("\t".join([str(x) for x in output]), file=outFile)
 for f in files:
   with open(f, "r") as inFile:
       args = next(inFile).strip()
       accuracies = next(inFile).strip()
       next(inFile)
       rewards = [float(x) for x in next(inFile).strip()[1:-1].split(", ") ]
       next(inFile)
       tradeoff = next(inFile).strip()
   print(args)
   if True:
     print(f)
     modelNumber = f[f.rfind("/")+1:].replace("accuracy_", "").replace(".txt", ".ckpt")
     print(modelNumber)
     modelNumber_ = modelNumber.replace(".ckpt", "").split("_")[-1]
     try:
      with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/OUTPUTS_TEXTS/TMP_{modelNumber}.txt", "r") as inFile:
        next(inFile)
        for line in inFile:
          line = line.strip().split("\t")
          try:
            line[8] = str(round(float(line[8]), 2))
            line[9] = str(round(float(line[9]), 2))
          except ValueError:
              pass
          print(modelNumber_+"\t"+("\t".join(line)), file=outFile)
     except FileNotFoundError:
        continue
