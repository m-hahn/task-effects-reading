import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=128) #random.choice([16, 32]))
parser.add_argument('--learning_rate', type=float, default=random.choice([0.02]))
parser.add_argument('--glove', type=bool, default=True)
parser.add_argument('--dropout', type=float, default=0.0)
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))
parser.add_argument('--LAMBDA', type=float, default=random.choice([1.0])) #random.choice([1.5, 1.75, 2, 2.25, 2.5]))
parser.add_argument('--ENTROPY_WEIGHT', type=float, default=0.01) #random.choice([0.0001, 0.001, 0.01, 0.1]))
parser.add_argument('--previewLength', type=int, default=random.choice([4]))
parser.add_argument('--feedPreview', type=int, default=random.choice([0]))
parser.add_argument('--lengthUntil', type=int, default=random.choice([0]))

args = parser.parse_args()



vocabulary = [x.split("\t") for x in open(f"vocabularies/{args.corpus}_anonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 
def permuteEntitiesInQ(result):
    if not result["answer"].startswith("@entity"):
      print(result["answer"])
      result["answer"] = result["answer"][result["answer"].index("@entity"):]
    entities = unique([x for x in result["text"]+result["question"]+[result["answer"]] if x.startswith("@entity")])
    entities_mapping = dict(zip(entities, [f"@entity{x}" for x in range(len(entities))]))
    assert len(entities_mapping) == len(entities)
    result["text"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["text"]]
    result["question"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["question"]]
    result["answer"] = entities_mapping[result["answer"]]


TEXT_LENGTH_BOUND = 500

def processQuestion(question, permuteEntities=True):
    question = question.translate(question.maketrans("—”“»…‘’!–", "-\"\"\".''!-" ))
    question = question.split("##########")
    result = {"text" : question[1].split(" "), "question" : question[2].split(" "), "answer" : question[3]}
    if permuteEntities:
       permuteEntitiesInQ(result)

    answerDistribution[0]+=1
    answerDistribution[1][int(result["answer"].replace("@entity", ""))]+=1

    return result

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
   assert partition in ["test", "training", "validation"]
   with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/cnn_{partition}.txt", "r") as inFile1:
    with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/dailymail_{partition}.txt", "r") as inFile2:
     while True:
        buff = []
        print("Filling buffer...")
        for _ in range(10*args.batchSize):
         try:
           for x in [inFile1, inFile2]:
             q = processQuestion(next(x).strip())
             buff.append(q) #processQuestion(next(inFile1).strip()))
#           buff.append(processQuestion(next(inFile2).strip()))
         except StopIteration:
            break
        if len(buff) == 0:
           break
        buff = sorted(buff, key=lambda x:len(x["text"]))
        partitions = [buff[i*args.batchSize:(i+1)*args.batchSize] for i in range(21)]
        random.shuffle(partitions)
        for partition in partitions:
           if len(partition) > 0:
               random.shuffle(partition) # ensure there is no confond between condition and length
               yield partition
        
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def lengthBin(word):
  if len(word) < args.lengthUntil:
     return len(word)
  return args.lengthUntil


def makePrefix(x):
   preview = x[:args.previewLength]+"_"+str(lengthBin(x))
   return preview


def numerify(token, ignoreCapitalization=True):
   if len(token) > 0:
      isCapitalized = (token[0] != token[0].lower())
   else:
      print("WARNING empty token")
      isCapitalized = False
   
   token = token.lower() #.replace("—", "-").replace('”', '"').replace('“', '"').replace("»", '"').replace("…", "...").replace("‘", "'").replace("’", "'").replace("!", "!").replace("–", "-") # this relates to DailyMail vs CNN
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      if isCapitalized and (not ignoreCapitalization):
         return 50000+stoi[token]+4
      else:
         return stoi[token]+4

def numerify_preview_characters(token):
#   token = token.replace("—", "-").replace('”', '"').replace('“', '"').replace("»", '"').replace("…", "...").replace("‘", "'").replace("’", "'").replace("!", "!").replace("–", "-") # this relates to DailyMail vs CNN
   token = token[:args.previewLength]
   #for x in token:
  #   if x not in stoi_characters:
 #       print("UNKNOWN", x)
   return [stoi_characters.get(x, stoi_characters["OOV"]) for x in token] + [stoi_characters["PAD"] for _ in range(args.previewLength-len(token))]


def numerify_preview(token, ignoreCapitalization=True):
   if ignoreCapitalization:
     token = token.lower()
#   token = token.replace("—", "-").replace('”', '"').replace('“', '"').replace("»", '"').replace("…", "...").replace("‘", "'").replace("’", "'").replace("!", "!").replace("–", "-") # this relates to DailyMail vs CNN
   preview = makePrefix(token)
   if preview not in stoi_prefixes:
      return OOV
   else:
      return stoi_prefixes[preview]


import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()




text_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()
question_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()


U = torch.nn.Parameter(torch.Tensor(256,256).cuda())
U.data.fill_(0)


output = torch.nn.Linear(256, 600 + 4).cuda()

input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss(reduction="none")
crossEntropy = torch.nn.CrossEntropyLoss(reduction="none")

components_lm = [word_embeddings, text_reader, question_reader, output]



state = torch.load("/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/train_qa_dropout_mask_relabling_attrain_decay_Chosen_Hybrid.py_8585151.ckpt")
print("devAccuraces", state["devAccuracies"])
print("devAccuraces", state["args"])
for i in range(len(components_lm)):
   components_lm[i].load_state_dict(state["components"][i])
U.data = state["U"].data



#parameters_lm_cached = [x for x in parameters_lm()]

linear = torch.nn.Linear(4, 1, bias=False).cuda()
linear.weight.data.zero_()

bilinear = torch.nn.Bilinear(4, 200, 1).cuda()
bilinear.weight.data.zero_()
bilinear.bias.data.zero_()




def findFirstSatisfying(startIncl, endIncl, condF):
   if condF(startIncl):
     return startIncl
   i = endIncl
   while True:
      if startIncl == i:
         return i
      if startIncl+1 == i:
         return i
      med = int((startIncl + i)/2)
      if condF(med):
          i = med
      else:
         startIncl = med
   

associated = {}

vocab_ = sorted(itos[:50000], key=lambda x:(makePrefix(x), len(x)))
print(vocab_[:100])
lastPrefix = ""
startPos = 0
notDone = set(range(50005))
#word_embeddings_preview.weight.data = word_embeddings.weight.data.clone().mean(dim=0).unsqueeze(0).expand(word_embeddings.weight.data.size()[0], -1)

#word_embeddings_preview.weight.data.zero_()


itos_prefixes = ["PAD", "SKIPPED", "OOV", "PLACEHOLDER"]
stoi_prefixes = {x : itos_prefixes.index(x) for x in itos_prefixes}
prefix_embeddings_matrix = [word_embeddings.weight.data[i].clone() for i in range(len(itos_prefixes))]

startPos = 0
lastPrefix = makePrefix(vocab_[0])
for i in range(1,len(vocab_)):
   prefix = makePrefix(vocab_[i])
   if prefix in stoi_prefixes:
     continue
   elif prefix != lastPrefix:
       first = startPos #findFirstSatisfying(startPos, j, lambda k:(len(vocab_[j]) / len(vocab_[k])) < 2)
       last = i #findFirstSatisfying(j, i-1, lambda k:(len(vocab_[k]) / len(vocab_[j])) > 2)
       assert makePrefix(vocab_[first]) == lastPrefix
       assert makePrefix(vocab_[last-1]) == lastPrefix
       assert makePrefix(vocab_[last]) != lastPrefix
#       if last > 5000:
 #        print(vocab_[first:last])
  #       quit()
       relevant = torch.LongTensor([numerify(x) for x in vocab_[first:last]]).cuda()
       embedded = word_embeddings(relevant)
      # print(embedded.size())

       # non-capitalized version
       stoi_prefixes[prefix] = len(itos_prefixes)
       itos_prefixes.append(prefix)
       prefix_embeddings_matrix.append(embedded.mean(dim=0).data.clone())




       lastPrefix = prefix
       startPos = i


print(len(itos_prefixes))
assert stoi_prefixes[itos_prefixes[100]] == 100




word_embeddings_preview = torch.nn.Embedding(num_embeddings = len(itos_prefixes), embedding_dim = 200).cuda()
print(word_embeddings_preview.weight.data.size())

word_embeddings_preview.weight.data = torch.stack(prefix_embeddings_matrix, dim=0)

print(word_embeddings_preview.weight.data.size())



components_attention = [linear, bilinear, word_embeddings_preview]
runningAverageParameter = torch.FloatTensor([0]).cuda()
runningAverageParameter.requires_grad = True

def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param
 yield U
 for c in components_attention:
   for param in c.parameters():
      yield param
 yield runningAverageParameter


components_other = [U, runningAverageParameter]

optimizer = torch.optim.SGD(parameters(), lr = args.learning_rate)


def formatText(x):
    return (x+(""*20))[:15]

def getEntities(x):
    return set([int(y.replace("@entity", "")) for y in x["text"] + x["question"] if y.startswith("@entity")])

featuresAverage = torch.zeros(4).cuda()


def forwardAttention(batch, printEverything=False, printExample=False):
#    texts = [[numerify("@entity1", ignoreCapitalization=False) if y.startswith("@entity") else numerify(y) for y in x["text"][:TEXT_LENGTH_BOUND]] for x in batch] # [:500]
    texts_numerified = [[numerify(y, ignoreCapitalization=False) for y in x["text"][:TEXT_LENGTH_BOUND]] for x in batch] # [:500]



    texts_preview_numerified = [[numerify_preview(y, ignoreCapitalization=False) for y in x["text"][:TEXT_LENGTH_BOUND]] for x in batch] # [:500]
    questions_preview = [set([numerify_preview(y, ignoreCapitalization=False) for y in x["question"]]) for x in batch]
    questions = [set([numerify(y, ignoreCapitalization=False) for y in x["question"]]) for x in batch]
    text_length = max([len(x) for x in texts_numerified])
    #position = torch.FloatTensor([list(range(text_length)) for _ in batch]).cuda().t()
    condition = torch.FloatTensor([[0 if i < len(batch)/2 else 1 for _ in range(text_length)] for i in range(len(batch))]).cuda().t()
    condition_ = condition - 0.5
    #position = (position)/500 - 0.5
    #positionTimesCondition = condition_ * position
    occursInQuestion = torch.FloatTensor([[1 if j < len(texts_numerified[i]) and texts_preview_numerified[i][j] in questions_preview[i] else 0 for j in range(text_length)] for i in range(len(batch))]).cuda().t() - 0.1
    occursInQuestion = occursInQuestion * condition
    occursInQuestionFull = torch.FloatTensor([[1 if j < len(texts_numerified[i]) and texts_numerified[i][j] in questions[i] else 0 for j in range(text_length)] for i in range(len(batch))]).cuda().t() - 0.1
    occursInQuestionFull = occursInQuestionFull * condition
    occursInQuestionAverage = torch.FloatTensor([0 for i in range(len(batch))]).cuda()
    for text in texts_numerified:
       while len(text) < text_length:
          text.append(PAD)
    for text in texts_preview_numerified:
       while len(text) < text_length:
          text.append(PAD)


    texts_preview_numerified = torch.LongTensor(texts_preview_numerified).cuda().transpose(0,1)
#    texts = torch.LongTensor(texts).cuda().transpose(0,1)
 #   texts_num = texts



    texts_preview = word_embeddings_preview(texts_preview_numerified).detach()



    #texts = word_embeddings(texts).detach()
    global featuresAverage
    featuresAverage_ = featuresAverage.clone()
    attentionLogprobabilities = []
    attentionDecisionsList = []
    zeroProbabilities = torch.zeros(len(batch)).cuda()
    runningAverage = torch.nn.functional.sigmoid(runningAverageParameter)
    oneMinus = (1-runningAverage)
    runningAverage_ = torch.cat([ oneMinus*torch.pow(runningAverage, 10-i) for i in range(10)] + [runningAverage], dim=0).unsqueeze(1)
    attentionScores = []
    ones = torch.zeros(text_length, len(batch)).cuda()+1
#    print(featuresAverage_)



 
  
 #   embedding = texts
    embedding_preview = texts_preview
    #print(embedding)

    attentionProbability_ = []
    attentionDecisions_ = []
    attentionLogit_ = []
    perWordWeights = torch.matmul(bilinear.weight.squeeze(2), embedding_preview.transpose(1,2)).transpose(1,2)
    #print(perWordWeights.size())
    for j in range(perWordWeights.size()[0]):
       #print(ones[j].size(), position[j].size(), condition_[j].size(), positionTimesCondition[j].size(), occursInQuestion[j].size(), occursInQuestionAverage.size())
       features = torch.stack([ones[j], condition_[j], occursInQuestion[j].detach(), occursInQuestionAverage], dim=1)
       #print(features.size(), perWordWeights.size())
       attentionLogit = (perWordWeights[j] * features).sum(dim=1) + linear(features).squeeze(1)
       #print(perWordWeights[j].size(), features.size(), (perWordWeights[j] * features).sum(dim=1).size(), linear(features).size(), attentionLogit.size())
       #quit()
       #print(attentionLogit)
       attentionProbability = torch.nn.functional.sigmoid(attentionLogit)
       attentionDecisions = torch.bernoulli(attentionProbability)
      
       #print(occursInQuestion[j].size(), attentionDecisions.size())
       occursInQuestionAverage = runningAverage * occursInQuestionAverage + oneMinus * attentionDecisions * occursInQuestionFull[j].detach()
       #print(attentionDecisions)
       attentionProbability_.append(attentionProbability)
       attentionDecisions_.append(attentionDecisions)
       attentionLogit_.append(attentionLogit)

    attentionProbability = torch.stack(attentionProbability_, dim=0)
    attentionDecisions = torch.stack(attentionDecisions_, dim=0)
    attentionLogit = torch.stack(attentionLogit_, dim=0)
    #quit()
      
    if printEverything:
     with open(f"/u/scr/mhahn/NEAT_SCRATCH/{__file__}_{args.myID}.tsv", "w") as outFile:
       print(args, file=outFile)
       print(devAccuracies, file=outFile)
       print(devLosses, file=outFile)
       print(devRewards, file=outFile)
       print(fixationRunningAverageByCondition[0], fixationRunningAverageByCondition[1], file=outFile)
       print(rewardAverage, "\t", "Accuracy", validAccuracyPerCondition[0], validAccuracyPerCondition[1], "\t", "Fixations", validFixationsPerCondition[0], validFixationsPerCondition[1], file=outFile)
       for q in range(len(batch)):
         for i in range(min(TEXT_LENGTH_BOUND, len(batch[q]["text"]))):
           print("NoPrev " if q < len(batch)//2 else "Preview", q, i, "\t", formatText(batch[q]["text"][i]), "\t", float(attentionDecisions[i,q]), "\t", float(attentionProbability[i,q]), "\t", "\t".join([str(float(x)) for x in perWordWeights[i,q,:]]), "\t", float(occursInQuestion[i,q]), "\t", "NA", "\t", "NA", "\t", "_".join(batch[q]["question"]), file=outFile)
         
#    attentionLogit = linear(features) + bilinear(features, embedding)
    if printExample:
        print(bilinear.weight.size(), embedding_preview.size())
        perWordWeights = torch.matmul(bilinear.weight.squeeze(2), embedding_preview.transpose(1,2))[:10].cpu().detach().numpy()
        print("Linear1", linear.weight.data)
#       features = torch.stack([ones[j], condition_[j], occursInQuestion[j].detach(), occursInQuestionAverage], dim=1)

        print("Position\tFixated?\tFixProb\tOverall\tCondition\tInQuestion\tRunningAvg")
        for i in range(min(10, len(batch[0]["text"]))):
           print("NoPrev ", i, "\t", formatText(makePrefix(batch[0]["text"][i])), "\t", formatText(batch[0]["text"][i]), "\t", float(attentionDecisions[i,0]), "\t", float(attentionProbability[i,0]), "\t", "\t".join([str(float(x)) for x in perWordWeights[i,:,0]]))
        print("Position\tFixated?\tFixProb\tOverall\tCondition\tInQuestion\tRunningAvg")
        for i in range(min(10, len(batch[-1]["text"]))):
           print("Preview", i, "\t", formatText(makePrefix(batch[-1]["text"][i])), "\t", formatText(batch[-1]["text"][i]), "\t", float(attentionDecisions[i,-1]), "\t", float(attentionProbability[i,-1]), "\t", "\t".join([str(float(x)) for x in perWordWeights[i,:,-1]]))

    #print(linear.weight.abs().max())
    #print(bilinear.weight.abs().max())
    #print("attentionLogit", attentionLogit)
    attentionLogProbability = torch.nn.functional.logsigmoid(torch.where(attentionDecisions == 1, attentionLogit, -attentionLogit))
    #print(attentionProbability)
    #print(attentionDecisions) 
#    quit()
    return attentionDecisions, attentionLogProbability, attentionProbability




def forward(batch, calculateAccuracy=False, printEverything=False, printExample=False):
    texts = [[numerify(y) for y in x["text"][:TEXT_LENGTH_BOUND]] for x in batch] # [:500]
#    print([len(texts[0]), len(texts[1])])
    questions = [[numerify(y) for y in x["question"]] for x in batch]
    answers = [int(x["answer"].replace("@entity", "")) for x in batch]
    text_length = max([len(x) for x in texts])
    question_length = max([len(x) for x in questions])+2
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    for question in questions:
       while len(question) < question_length:
          question.append(PAD)

    entitiesPerText = [getEntities(x) for x in batch]
    MASK = torch.FloatTensor(len(batch), 600+4)
    MASK.fill_(float("-inf"))
    for i in range(len(batch)):
       for j in entitiesPerText[i]:
          MASK[i,j] = 0
    MASK = MASK.cuda()
#    texts = [[random.randint(1,10) for _ in range(args.batchSize)] for j in range(args.batchSize)]
#    questions = [[j, PLACEHOLDER] for j in range(args.batchSize)] # need to add j
#    answers = [texts[j][j] for j in range(args.batchSize)]
#    data = list(zip(texts, questions, answers))
##    print(min(answers), max(answers))
#    random.shuffle(data)
#    texts, questions, answers = zip(*data)
##    print(answers)
#    questions_raw = questions
    texts =  torch.LongTensor(texts).cuda().transpose(0,1)

    mask, action_logprob, attentionScores = forwardAttention(batch, printEverything=printEverything, printExample=printExample)
    #print(mask)
    #print(action_logprob)
    #print(attentionScores)
#    mask = mask.squeeze(2)
    mask = torch.where(texts == PAD, 1+0*mask, mask)
    fixatedFractionExpectation = torch.where(texts == PAD, 0*attentionScores, attentionScores).sum(dim=0) / (texts != PAD).sum(dim=0)
    fixatedFraction = torch.where(texts == PAD, 0*mask, mask).sum(dim=0) / (texts != PAD).sum(dim=0)
#    print(mask.size(), texts.size())
    texts = torch.where(mask == 1, texts, SKIPPED + 0*texts)
    #print(texts)
    questions = torch.LongTensor(questions).cuda().transpose(0,1)
    answers = torch.LongTensor(answers).cuda()
#    print(texts.max(), texts.min())
    texts = word_embeddings(texts)
    if True:
       questions = word_embeddings(questions)
       output_text, _ = text_reader(texts)
       if not calculateAccuracy and args.dropout > 0:
          texts = input_dropout(texts)
          questions = input_dropout(questions)
       _, question_representations = question_reader(questions)
       #print(output_text.size())
       question_representations = question_representations[0].transpose(0,1).contiguous().view(-1, 256)
       #print(question_representations.size())
   
       part1 = torch.matmul(output_text, U)
       #print("PART1", part1.size(), "Q", question_representations.size())
       question_representations = question_representations.unsqueeze(2) # torch.transpose(question_representations, 0, 1).unsqueeze(1)
       part1 = part1.unsqueeze(2)
       #print("PART1", part1.size(), "Q", question_representations.size())
       attention = torch.matmul(part1, question_representations) # 
       attention = attention.squeeze(2).squeeze(2)
       attention = torch.nn.functional.softmax(attention, dim=0)
#       if random.random() < 0.01:
#          attention0 = attention[:,0].cpu()
#          mask0 = mask[:,0].cpu()
#          attentionScores0 = attentionScores[:,0,0].cpu()
#          for i in range(len(batch[0]["text"][:TEXT_LENGTH_BOUND])):
#             print(i, "\t", batch[0]["text"][i], "\t", attentionScores0[i], "\t", float(mask0[i]), "\t", float(attention0[i]), "\t", batch[0]["answer"], "\t", " ".join(batch[0]["question"]))
#          print(attention)
   #    print(attention[:, 0], questions_raw[0]) # for DEBUGGING
       #print(attention.size(), output_text.size())
   #    print(entityOccurrence.size(), attention.size(), answers.size())
       text_representation =  output((attention.unsqueeze(2) * output_text).sum(dim=0)) + MASK
       loss = crossEntropy(text_representation, answers)
    #assert float(loss.mean()) < 100, loss
    if calculateAccuracy:
       prediction = text_representation.argmax(dim=1)
       return loss, action_logprob, fixatedFraction, fixatedFractionExpectation, ((prediction == answers).float())
    else:
       return loss, action_logprob, fixatedFraction, fixatedFractionExpectation

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

fixationRunningAverageByCondition = [0.5,0.5]
rewardAverage = 3.0
lossAverageByCondition = [2.0, 2.0]

def backward(loss, action_logprob, fixatedFraction, fixatedFractionExpectation, printHere=True):
   global rewardAverage
   if random.random() < 0.99:
     batchSize = fixatedFraction.size()[0]
     fix1, fix2 = float(fixatedFraction[:int(batchSize/2)].mean()), float(fixatedFraction[int(batchSize/2):].mean())
     fixationRunningAverageByCondition[0] = 0.99 * fixationRunningAverageByCondition[0] + (1-0.99) * fix1
     fixationRunningAverageByCondition[1] = 0.99 * fixationRunningAverageByCondition[1] + (1-0.99) * fix2

     loss1, loss2 = float(loss[:int(batchSize/2)].mean()), float(loss[int(batchSize/2):].mean())
     lossAverageByCondition[0] = 0.99 * lossAverageByCondition[0] + (1-0.99) * loss1
     lossAverageByCondition[1] = 0.99 * lossAverageByCondition[1] + (1-0.99) * loss2
     if printHere:
       print("FIXATION RATE", [fix1, fix2], "REWARD", rewardAverage, "LossByCondition", lossAverageByCondition, "RunningAvgPar", float(runningAverageParameter))
   optimizer.zero_grad()
   action_prob = torch.exp(action_logprob)
   oneMinusActionProb = 1-action_prob
   negEntropy = (action_prob * action_logprob + oneMinusActionProb * oneMinusActionProb.log()).mean()
   reward = (loss.detach() + args.LAMBDA * fixatedFraction)
   loss_ = 10 * (action_logprob.mean(dim=0) * (reward - rewardAverage)).mean() + loss.mean() + args.ENTROPY_WEIGHT * negEntropy
   assert negEntropy.max() < 1e10, negEntropy
   loss_.backward()
   rewardAverage = 0.99 * rewardAverage + (1-0.99) * float(reward.mean())
   if args.clip_type is not "None":
#      print("NROM MAX", max(p.grad.detach().abs().max().cpu() for p in parameters()))
      torch.nn.utils.clip_grad_norm_(parameters(), args.clip_bound, norm_type=args.clip_type)
   optimizer.step()

#my_save_path = f"/jagupard18/scr1/mhahn/{__file__}_{args.myID}.ckpt"
my_save_path = f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2021/{__file__}_{args.myID}.ckpt"

import time

expectedEntropy = 0

learning_rate=args.learning_rate

devLosses = []
lossRunningAverage = 6.4
devAccuracies = []
devRewards = []
noImprovement = 0

FROM_ATTENTION_WEIGHT = 1.0

for epoch in range(10000):
  
  if epoch >= 1:
    validLoss = []
    examplesNumber = 0
    validAccuracy = []
    validReward = []
    validAccuracyPerCondition = [0.0, 0.0]
    validFixationsPerCondition = [0.0, 0.0]
    batchNum = 0
    for batch in  loadQACorpus(args.corpus, "validation", args.batchSize):
     batchNum += 1
     with torch.no_grad():
       loss, action_logprob, fixatedFraction, _, accuracy = forward(batch, calculateAccuracy = True, printEverything=(batchNum == 1))
       reward = float((loss.detach() + args.LAMBDA * fixatedFraction).mean())
       loss = float(loss.mean())
       print("VALID", loss, examplesNumber, reward)
#     print(accuracy)
     #print(fixatedFraction)
     accuracyCond1 = float(accuracy[:int(len(batch)/2)].sum())
     accuracyCond2 = float(accuracy[int(len(batch)/2):].sum())
     accuracy = float(accuracy.mean())
     validAccuracyPerCondition[0] += accuracyCond1
     validAccuracyPerCondition[1] += accuracyCond2
     fixationsCond1 = float(fixatedFraction[:int(len(batch)/2)].sum())
     fixationsCond2 = float(fixatedFraction[int(len(batch)/2):].sum())
     validFixationsPerCondition[0] += fixationsCond1
     validFixationsPerCondition[1] += fixationsCond2


     validLoss.append(float(loss)*len(batch))
     validReward.append(reward*len(batch))
     examplesNumber += len(batch)
     validAccuracy.append(float(accuracy)*len(batch))
    devLosses.append(sum(validLoss)/examplesNumber)
    devAccuracies.append(sum(validAccuracy)/examplesNumber)
    devRewards.append(sum(validReward)/examplesNumber)
    validAccuracyPerCondition[0] /= (examplesNumber/2)
    validAccuracyPerCondition[1] /= (examplesNumber/2)
    validFixationsPerCondition[0] /= (examplesNumber/2)
    validFixationsPerCondition[1] /= (examplesNumber/2)
    print(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2021/accuracy_{__file__}_{args.myID}.txt")
    with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2021/accuracy_{__file__}_{args.myID}.txt", "w") as outFile:
       print(args, file=outFile)
       print(devAccuracies, file=outFile)
       print(devLosses, file=outFile)
       print(devRewards, file=outFile)
       print(fixationRunningAverageByCondition[0], fixationRunningAverageByCondition[1], "savePath="+my_save_path, file=outFile)
       print(rewardAverage, "\t", "Accuracy", validAccuracyPerCondition[0], validAccuracyPerCondition[1], "\t", "Fixations", validFixationsPerCondition[0], validFixationsPerCondition[1], file=outFile)
    if len(devRewards) >1 and devRewards[-1] > devRewards[-2]:
       learning_rate *= 0.8
       optimizer = torch.optim.SGD(parameters(), lr = learning_rate)
       noImprovement += 1
    elif len(devRewards) > 1 and devRewards[-1] == min(devRewards):
       torch.save({"devRewards" : devRewards, "args" : args, "components_other" : [x.data for x in components_other], "components_lm" : [x.state_dict() for x in components_lm], "components_attention" : [x.state_dict() for x in components_attention], "learning_rate" : learning_rate, "itos_prefixes" : itos_prefixes}, my_save_path)
  
    #   noImprovement = 0
    if noImprovement > 5:
      print("End training, no improvement for 5 epochs")
      break
  words = 0
  timeStart = time.time()
  counter = 0
  for batch in loadQACorpus(args.corpus, "training", args.batchSize, permuteEntities=True):
    counter += 1
    printHere = (counter % 5) == 0
    loss, action_logprob, fixatedFraction, fixatedFractionExpectation = forward(batch, printExample = (random.random() < 0.05))
    backward(loss, action_logprob, fixatedFraction, fixatedFractionExpectation, printHere=printHere)
    loss = float(loss.mean())
    lossRunningAverage = 0.99 *lossRunningAverage + (1-0.99) * float(loss)
    words += len(batch)
    if printHere:
       print(devAccuracies)
       print(devLosses)
       print(devRewards)
       print(float(loss), lossRunningAverage, expectedEntropy, args, __file__)
       print(words/(time.time()-timeStart), "questions per second")
    if expectedEntropy == 0 or (answerDistribution[0] < 20000 and random.random() < 0.05):
       dist = torch.FloatTensor(answerDistribution[1])
       dist = dist / dist.sum()
       expectedEntropy = float(-(dist*(dist+1e-10).log()).sum())

print(devAccuracies)
print(devLosses)

