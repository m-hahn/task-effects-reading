import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=random.choice([32]))
parser.add_argument('--learning_rate', type=float, default=random.choice([0.05]))
parser.add_argument('--glove', type=bool, default=True)
parser.add_argument('--dropout', type=float, default=random.choice([0.3]))
parser.add_argument('--lr_decay', type=float, default=random.choice([0.5]))
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))
#parser.add_argument('--permuteEntities', type=bool, default=True)

args = parser.parse_args()



vocabulary = [x.split("\t") for x in open(f"vocabularies/{args.corpus}_nonAnonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 
def permuteEntitiesInQ(result):
    entities = unique([x for x in result["text"]+result["question"]+[result["answer"]] if x.startswith("@entity")])
    entities_mapping = dict(zip(entities, [f"@entity{x}" for x in range(len(entities))]))
    assert len(entities_mapping) == len(entities)
    result["text"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["text"]]
    result["question"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["question"]]
    result["answer"] = entities_mapping[result["answer"]]

def deanonymize(entityMapping, text):
   text_ = []
   for x in text:
     if x in entityMapping:
        text_ += entityMapping[x].split(" ")
     else:
        text_.append(x)
   return text_

def makeEntitiesMask(entityMapping, text):
   text_ = []
   for x in text:
     if x in entityMapping:
        text_ += [0 for _ in entityMapping[x].split(" ")]
     else:
        text_.append(-1e100)
   return text_



def processQuestion(question):
    question = question.split("##########")
    entityMapping = dict([(lambda q: (q[0], q[1].strip().lower()))(x.split(":")) for x in question[4].split("@@@@@")])


    # Now prepare the result
    result = {"text" : deanonymize(entityMapping, question[1].split(" ")), "question" : deanonymize(entityMapping, question[2].split(" ")), "answer" : deanonymize(entityMapping, question[3].split(" ")), "answer_index" : [x[0] for x in entityMapping.items()].index(question[3]), "all_entities" : [y.split(" ") for _, y in entityMapping.items()], "text_mask" : makeEntitiesMask(entityMapping, question[1].split(" "))}

#    print(result)
 #   print(entityMapping)
  #  quit()
    return result

def loadQACorpus(corpus, partition, batchSize):
   assert partition in ["test", "training", "validation"]
   with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/{corpus}_{partition}.txt", "r") as inFile:
     while True:
        buff = []
        print("Filling buffer...")
        for _ in range(20*args.batchSize):
         try:
           buff.append(processQuestion(next(inFile).strip()))
         except StopIteration:
            break
        if len(buff) == 0:
           break
        buff = sorted(buff, key=lambda x:len(x["text"]))
        partitions = [buff[i*args.batchSize:(i+1)*args.batchSize] for i in range(21)]
        random.shuffle(partitions)
        for partition in partitions:
           if len(partition) > 0:
               yield partition
        
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()




if args.glove:
 with open("/u/scr/mhahn/glove/glove.6B.200d.txt", "r") as inFile:
  print("Loading embeddings")
  counter = 0
  for line in inFile:
    counter += 1
    line = next(inFile).strip().split(" ")
    word = line[0]
    if word in stoi and stoi[word] < 50000:
       #print(stoi[word])
       embedding = torch.FloatTensor([float(x) for x in line[1:]])
       word_embeddings.weight.data[stoi[word]+4] = embedding
       # print(counter, word)
    if counter > 100000:
      break
  print("Done loading embeddings")
 

text_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()
question_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()
answer_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()


U = torch.nn.Parameter(torch.Tensor(256,256).cuda())
U.data.fill_(0)


output = torch.nn.Linear(256, 600 + 4).cuda()

input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss()
crossEntropy = torch.nn.CrossEntropyLoss()

components_lm = [word_embeddings, text_reader, question_reader, output, answer_reader]


entities_mask = torch.zeros(2,200).cuda()
entities_mask.requires_grad = True

def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param
 yield U
 yield entities_mask
#parameters_lm_cached = [x for x in parameters_lm()]


optimizer = torch.optim.SGD(parameters(), lr = args.learning_rate)





def forward(batch, calculateAccuracy=False):
   
    texts = [[numerify(y) if calculateAccuracy or random.random() > 0.1 else SKIPPED for y in x["text"]] for x in batch] # [:500]
#    print([len(texts[0]), len(texts[1])])
    questions = [[numerify(y) for y in x["question"]] for x in batch]
    answers = [int(x["answer_index"]) for x in batch]
    texts_mask = [x["text_mask"] for x in batch]
    # Process the answer options
    answers_flattened = []
    all_entities = [[[numerify(y) for y in z] for z in x["all_entities"]] for x in batch]
    number_of_entities = sum([len(x) for x in all_entities])
    all_entities_flattened = []
    mask_flattened = torch.zeros(len(batch), number_of_entities) - 1e100
    for i, x in enumerate(all_entities):
       for j, y in enumerate(x):
          mask_flattened[i, len(all_entities_flattened)] = 0
          if j == answers[i]:
             answers_flattened.append(len(all_entities_flattened))
          all_entities_flattened.append(y)
    mask_flattened = mask_flattened.cuda()
    answers_flattened = torch.LongTensor(answers_flattened).cuda()
    entities_length = max([max([len(q) for q in z]) for z in all_entities])
    for text in all_entities:
       for entity in text:
          while len(entity) < entities_length:
             entity.append(PAD)
    #print(all_entities)
    #print(entities_length)
    entities_number = max([len(q) for q in all_entities])
    #print(entities_number, min([len(q) for q in all_entities]))
    #print(len(all_entities_flattened))
    text_length = max([len(x) for x in texts])
    question_length = max([len(x) for x in questions])+2
    for text in texts_mask:
       while len(text) < text_length:
          text.append(1e-100)
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    for question in questions:
       while len(question) < question_length:
          question.append(PAD)

    texts_mask = torch.FloatTensor(texts_mask).cuda()

    #entitiesPerText = [getEntities(x) for x in batch]
    #MASK = torch.FloatTensor(len(batch), 600+4)
    #MASK.fill_(float("-inf"))
    #for i in range(len(batch)):
    #   for j in entitiesPerText[i]:
    #      MASK[i,j] = 0
    #MASK = MASK.cuda()
#    texts = [[random.randint(1,10) for _ in range(args.batchSize)] for j in range(args.batchSize)]
#    questions = [[j, PLACEHOLDER] for j in range(args.batchSize)] # need to add j
#    answers = [texts[j][j] for j in range(args.batchSize)]
#    data = list(zip(texts, questions, answers))
##    print(min(answers), max(answers))
#    random.shuffle(data)
#    texts, questions, answers = zip(*data)
##    print(answers)
#    questions_raw = questions
    texts =  torch.LongTensor(texts).cuda().transpose(0,1)
    questions = torch.LongTensor(questions).cuda().transpose(0,1)
    all_entities_flattened = torch.LongTensor(all_entities_flattened).cuda().transpose(0,1)
#    print(texts.max(), texts.min())
    texts = word_embeddings(texts)
 #   print(texts.size())
#    print((texts_mask == 0).mean())

    # Let the network know which words are capitalized (named entities)
    entities_markup = (torch.where(texts_mask.t().unsqueeze(2) == 0, entities_mask[0].view(1, 1, 200).expand(texts.size()[0], texts.size()[1], -1), entities_mask[1].view(1, 1, 200).expand(texts.size()[0], texts.size()[1], -1)))
    texts = texts + entities_markup
    questions = word_embeddings(questions)
    all_entities_flattened = word_embeddings(all_entities_flattened)
    output_text, _ = text_reader(texts)
    if not calculateAccuracy and args.dropout > 0:
       texts = input_dropout(texts)
       questions = input_dropout(questions)
    _, question_representations = question_reader(questions)
    _, answer_representations = answer_reader(all_entities_flattened)
    #print(output_text.size())
    question_representations = question_representations[0].transpose(0,1).contiguous().view(-1, 256)
    answer_representations = answer_representations[0].transpose(0,1).contiguous().view(-1, 256)
    #print(question_representations.size())

    part1 = torch.matmul(output_text, U)
    #print("PART1", part1.size(), "Q", question_representations.size())
    question_representations = question_representations.unsqueeze(2) # torch.transpose(question_representations, 0, 1).unsqueeze(1)
    part1 = part1.unsqueeze(2)
    #print("PART1", part1.size(), "Q", question_representations.size())
    attention = torch.matmul(part1, question_representations) # 
#    print(texts_mask.t())
    attention = attention.squeeze(2).squeeze(2) + texts_mask.t()
    attention = torch.nn.functional.softmax(attention, dim=0)
 #   print(attention)
  #  quit()
    if random.random() < 0.01:
       attention0 = attention[:,0].cpu()
       for i in range(len(batch[0]["text"])):
          print(i, "\t", batch[0]["text"][i], "\t", float(attention0[i]), "\t", batch[0]["answer"], "\t", " ".join(batch[0]["question"]))
  #     print(attention)
#    print(attention[:, 0], questions_raw[0]) # for DEBUGGING
    #print(attention.size(), output_text.size())
    text_representation =  (attention.unsqueeze(2) * output_text).sum(dim=0)
  #  print(text_representation.size())
 #   print(answer_representations.size())
#    print(args.batchSize)
    matched_scores = torch.matmul(text_representation, answer_representations.t())
#    print(matched_scores.size(), mask_flattened.size(), answers_flattened.size()) # batch x #entities
 #   print(mask_flattened)
 #   print(mask_flattened)
#    quit()
    loss = crossEntropy(matched_scores + mask_flattened, answers_flattened)
    if calculateAccuracy:
       prediction = (matched_scores+mask_flattened).argmax(dim=1)
       return loss, float((prediction == answers_flattened).float().mean())
    else:
       return loss

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

def backward(loss):
   optimizer.zero_grad()
   loss.backward()
   if args.clip_type is not "None":
#      print("NROM MAX", max(p.grad.detach().abs().max().cpu() for p in parameters()))
      torch.nn.utils.clip_grad_norm_(parameters(), args.clip_bound, norm_type=args.clip_type)
   optimizer.step()

import time

expectedEntropy = 0

learning_rate=args.learning_rate

devLosses = []
lossRunningAverage = 6.4
devAccuracies = []
noImprovement = 0

FROM_ATTENTION_WEIGHT = 1.0

for epoch in range(10000):


  validLoss = []
  examplesNumber = 0
  validAccuracy = []
  for batch in  loadQACorpus(args.corpus, "validation", args.batchSize):
   with torch.no_grad():
     loss, accuracy = forward(batch, calculateAccuracy = True)
     print("VALID", loss, examplesNumber)
   validLoss.append(float(loss)*len(batch))
   examplesNumber += len(batch)
   validAccuracy.append(float(accuracy)*len(batch))
  devLosses.append(sum(validLoss)/examplesNumber)
  devAccuracies.append(sum(validAccuracy)/examplesNumber)
  if len(devAccuracies) >1 and devAccuracies[-1] < devAccuracies[-2]:
     learning_rate *= args.lr_decay
     optimizer = torch.optim.SGD(parameters(), lr = learning_rate)
     noImprovement += 1
  elif len(devAccuracies) > 1 and devAccuracies[-1] >= max(devAccuracies):
#     assert False
     torch.save({"devAccuracies" : devAccuracies, "devLosses" : devLosses, "args" : args, "components" : [x.state_dict() for x in components_lm], "entities_mask" : entities_mask, "U" : U.data, "learning_rate" : learning_rate}, f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2021/{__file__}_{args.myID}.ckpt")

     noImprovement = 0
  print(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2021/accuracy_{__file__}_{args.myID}.txt")
  with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2021/accuracy_{__file__}_{args.myID}.txt", "w") as outFile:
     print(args, file=outFile)
     print(devAccuracies, file=outFile)
     print(devLosses, file=outFile)

  if noImprovement > 10:
    print("End training, no improvement for 10 epochs")
    break
  words = 0
  timeStart = time.time()
  for batch in loadQACorpus(args.corpus, "training", args.batchSize):
    loss = forward(batch)
    backward(loss)
    lossRunningAverage = 0.99 *lossRunningAverage + (1-0.99) * float(loss)
    words += len(batch)
    print(devAccuracies)
    print(devLosses)
    print(float(loss), lossRunningAverage, expectedEntropy, args, __file__)
    print(words/(time.time()-timeStart), "questions per second")
    if expectedEntropy == 0 or (answerDistribution[0] < 20000 and random.random() < 0.05):
       dist = torch.FloatTensor(answerDistribution[1])
       dist = dist / dist.sum()
       expectedEntropy = float(-(dist*(dist+1e-10).log()).sum())
      
print(devAccuracies)
print(devLosses)

