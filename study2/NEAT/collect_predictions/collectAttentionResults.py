import glob
files = glob.glob("/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2020/accuracy_train_attention*py*txt")
results = []
for f in files:
 with open(f, "r") as inFile:
   inFile = inFile.read().strip().split("\n")
   if len(inFile) != 6:
     print("SKIPPING", f, inFile)
     continue
   args, accuracies, losses, rewards, fixations, something = inFile
   if "LAMBDA=2.25" not in args:
      print("SKIPPING", args)
      continue
   rewards = [float(x) for x in rewards[1:-1].split(", ")]
   results.append((min(rewards), f[f.rfind("/"):], args))
results = sorted(results, reverse=True)

for r in results:
  print(r)

