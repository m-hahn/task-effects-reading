import glob
files = glob.glob("/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2020/*Save_9b.py_*.txt")
with open("results/tradeoff.tsv", "w") as outFile:
 print("\t".join(["Model", "Accuracy", "Fixations", "Condition"]), file=outFile)
 for f in files:
   with open(f, "r") as inFile:
       args = next(inFile).strip()
       accuracies = next(inFile).strip()
       next(inFile)
       rewards = [float(x) for x in next(inFile).strip()[1:-1].split(", ") ]
       next(inFile)
       tradeoff = next(inFile).strip()
       if "ENTROPY_WEIGHT=0.01" in args and "LAMBDA=1.0" in args:
          _, accuracies, fixations = (tradeoff.split("\t"))
          accuracies = accuracies.strip().split(" ")
          fixations = fixations.strip().split(" ")

          print("\t".join([str(x) for x in [f[f.rfind("_")+1:-4], accuracies[1], fixations[1], "NoPreview"]]), file=outFile)
          print("\t".join([str(x) for x in [f[f.rfind("_")+1:-4], accuracies[2], fixations[2], "Preview"]]), file=outFile)

