import glob
files = glob.glob("/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2020/*Save_9b.py_*.txt")
for f in files:
   with open(f, "r") as inFile:
       args = next(inFile).strip()
       accuracies = next(inFile).strip()
       next(inFile)
       rewards = [float(x) for x in next(inFile).strip()[1:-1].split(", ") ]
       next(inFile)
       tradeoff = next(inFile).strip()
       if "ENTROPY_WEIGHT=0.01" in args and "LAMBDA=1.0" in args:
#          print(tradeoff, len(rewards))
          print(f)
