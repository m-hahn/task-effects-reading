import glob
files = glob.glob("/u/scr/mhahn/NEURAL_ATTENTION_TASK/accuracies_2020/*Save_9b.py_*.txt")
header = None
with open("/u/scr/mhahn/NEURAL_ATTENTION_TASK/OUTPUTS_TEXTS/merged.tsv", "w") as outFile:
 for f in files:
  try:
   with open(f, "r") as inFile:
       args = next(inFile).strip()
       accuracies = next(inFile).strip()
       next(inFile)
       rewards = [float(x) for x in next(inFile).strip()[1:-1].split(", ") ]
       next(inFile)
       tradeoff = next(inFile).strip()
       if "ENTROPY_WEIGHT=0.01" in args and "LAMBDA=1.0" in args:
#          print(tradeoff, len(rewards))
          print(f)
          outputFile = "/u/scr/mhahn/NEURAL_ATTENTION_TASK/OUTPUTS_TEXTS/TMP_"+f[f.rfind("/")+1:].replace("accuracy_", "").replace(".txt", ".ckpt.txt")
          modelNumber = f[f.rfind("_")+1:][:-4]
          print(modelNumber)
          with open(outputFile, "r") as inFile:
            header_ = next(inFile)
            if header is None:
              header = header_
              print(f"ModelID\t{header.strip()}", file=outFile)
            for line in inFile:
               print(f"{modelNumber}\t{line.strip()}", file=outFile)
  except FileNotFoundError:
      pass
