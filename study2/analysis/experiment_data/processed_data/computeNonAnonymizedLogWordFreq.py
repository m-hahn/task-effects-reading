
with open("../../../../study1/python/prepare_corpus/vocabularies/cnn_nonAnonymized.txt", "r") as inFile:
   frequenciesCNN = dict([x.split("\t")[1:] for x in inFile.read().strip().split("\n")])
with open("../../../../study1/python/prepare_corpus/vocabularies/dailymail_nonAnonymized.txt", "r") as inFile:
   frequenciesDailymail = dict([x.split("\t")[1:] for x in inFile.read().strip().split("\n")])

header = ["TextFileName", "HumanPosition", "AnonymizedPosition", "OriginalToken", "NonAnonymizedWordFreq"]
with open("mappingNonAnonymizedWordFreq.tsv", "w") as outFile:
  print("\t".join(header), file=outFile)
  with open("mappingSurprisals.tsv", "r") as inFile:
     headerIn = next(inFile).strip().split("\t")
     headerIn = dict(list(zip(headerIn, range(len(headerIn)))))
     for line in inFile:
        line = line.strip("\n").split("\t")
        token = line[headerIn["OriginalToken"]].lower()
        print("\t".join([line[headerIn[x]] for x in header[:-1]] + [str(int(frequenciesDailymail.get(token, 0)) + int(frequenciesCNN.get(token, 0)))]), file=outFile)
     
