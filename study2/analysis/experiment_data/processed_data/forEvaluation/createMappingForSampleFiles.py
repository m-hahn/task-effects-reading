def Main():
  fileCounter = 0
  fileListPath = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-sample-dev.txt"
  fileList = open(fileListPath, "r") #"/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/experiments/corpus/"+CORPUS+"/fileListTest.txt", 'r')
  for line in fileList:
    fileName = line.rstrip()
    if len(fileName) == 0:
      continue
    fileCounter += 1
    filePath = fileName.replace("_","/")
    print(filePath)
    article = open(filePath, 'r') # "/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/experiments/corpus/"+CORPUS+"/docs/"+fileName, 'r')
    counter = 0
    entitiesDictionary = {}
    for line2 in article:
      if counter == 0:
         url = line2.rstrip()
      elif counter == 2:
         text = line2.split(" ")
         length = len(text)
      elif counter == 4:
         question = line2.rstrip()+" "
      elif counter == 6:
         answer = line2.rstrip().lstrip()+" "
      elif "@entity" in line2 :
         line2 = line2.rstrip().split(":")
         if len(line2) > 1:
           answer = answer.replace(line2[0]+" ", line2[1]+" ")
           question = question.replace(line2[0]+" ", line2[1]+" ")
           entitiesDictionary[line2[0]] = line2[1]
      counter += 1
    #print(text)
    with open("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/mapping/"+fileName.replace(".question","")+".csv",'w') as mappingFile:
      humanCounter = 0
      for i in range(0,len(text)):
        text[i] = text[i].rstrip()
        if len(text[i]) == 0:
          continue
        if "@entity" in text[i]:
           humanTokens = entitiesDictionary[text[i]].rstrip().split(" ")
        else:
           humanTokens = [text[i]]
        print >> mappingFile, "\t".join([str(humanCounter),str(i),text[i],humanTokens[0],humanTokens[0]])
        humanCounter += 1
        for j in range(1,len(humanTokens)):
           print >> mappingFile, "\t".join([str(humanCounter),"","",humanTokens[j],humanTokens[j]])
           humanCounter += 1
Main()
