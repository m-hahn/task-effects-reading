header = None
with open("mappingWithHuman_FurtherMeasures.tsv", "w") as outFile:
  for condition in ["preview", "nopreview"]:
    for participant in range(1,13):
        for text in range(1, 21):
         try:
          with open(f"{condition}/Participant-{participant}/E-{text}.csv", "r") as inFile:
            header_ = next(inFile).strip()
            if header is None:
                header = header_
                print("TextFileName\t"+header.strip(), file=outFile)
            else:
                assert header == header_
            for line in inFile:
                line = line.strip("\n")
                assert len(line.split("\t")) ==  len(header.split("\t"))
#                if len(line.split("\t")) == 8:
#                    print(line.split("\t"))
                line = line.split("\t")
                assert line[6] in ["preview", "nopreview"], line
                line[6] = line[6].replace("no", "No").replace("pre", "Pre")
                line[5] = line[6][0]+"_"+line[5]
                line = "\t".join(line)
                print(f"TE-{text}\t"+line, file=outFile)
         except FileNotFoundError:
            pass

