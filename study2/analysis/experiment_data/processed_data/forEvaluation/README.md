* `createMappingForSampleFiles.py`
* `fileList.txt`
* `mapping` Mapping between three tokenizations: Anonymized, ByToken, ByWhitespace
* `mappingWithExternal` The same, augmented with relevant predictor variables
* `mappingWithHuman`	The same, with experimental reading measures
* `mergeSurprisals.py` 
* `prepareItemsForEvaluation.py`
* `questions`
* `surprisals`
