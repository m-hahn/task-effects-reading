with open("../mappingSurprisals.tsv", "w") as outFile:
  header = ["TextFileName", "HumanPosition", "AnonymizedPosition", "OriginalToken", "Surprisal_OOV_Status", "Surprisal"]
  print("\t".join(header), file=outFile)
  for text in range(1, 21):
    with open(f"surprisals/E-{text}.csv", "r") as inFile:
      for line in inFile:
         line = line.strip("\n")
         print(f"TE-{text}\t"+line, file=outFile)
