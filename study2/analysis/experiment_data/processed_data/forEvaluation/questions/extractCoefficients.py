import os

# takes csv's output by the Torch code and builds CSV files to be read by the R script.

# modeled on stripQuestionsAndMerge.py

ROOT_FOLDERS = ["/afs/cs.stanford.edu/u/mhahn/mhahn_files/", "/mhahn/repo2/mhahn_files/"]
ROOT_FOLDER = ROOT_FOLDERS[map(lambda x: x[1], filter(lambda x : x[0], zip(map(os.path.isdir, ROOT_FOLDERS), range(0,len(ROOT_FOLDERS)))))[0]]

if True: #False: #True:
  baseFolder = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/"
  annotationFolder = baseFolder+"/annotation/"
  annotationOnlyTextsFolder = baseFolder+"/annotation-textsonly/"
else:
  baseFolder = ROOT_FOLDER + "/ed1516/experiments/corpus/forEvaluation/"
  annotationFolder = baseFolder+"/questions/annotation/"
  annotationOnlyTextsFolder = baseFolder+"questions/annotation-textsonly/"


models = os.listdir(annotationFolder)
for model in models:
   if model.startswith(".svn"):
     continue
   if os.path.isfile(annotationFolder+model):
      continue
   assert(os.path.isdir(annotationFolder+model))
   if (not (os.path.isdir(annotationOnlyTextsFolder+model))):
     os.mkdir(annotationOnlyTextsFolder+model)
   assert(os.path.isdir(annotationOnlyTextsFolder+model))
   textFiles = os.listdir(annotationFolder+model)

   if model.startswith("att"):
     typeOfModel = "attention"
     if False:
       print("WARNING: NOT DOING ATTENTION NOW. 3339.")
       continue
   elif model.startswith("surp"):
     typeOfModel = "surprisal"
     print("Ignoring surprisal output. This script only looks at attention coefficients")
     continue
   else:
     assert False

   for textFile in textFiles:
     if textFile.startswith(".svn"):
       continue
     fInNEAT = open(annotationFolder+model+"/"+textFile,"r")
     if not ("question.coef.anno" in textFile):
       continue
     fInTexts = open(baseFolder+"/mapping/"+(textFile.replace("question.coef.anno","csv")),"r")
     fOutNEAT = open(annotationOnlyTextsFolder+model+"/"+(textFile.replace("question.coef.anno","coef.csv")),"w")

     coefficientNames = ["attRelativeToQ", "fromInput", "gateFromInput", "dotproductAffine", "questHistoryFutureGate", "gatedQuestForFuture", "questHistoryOutGate", "gatedFromHistory", "positionGate", "positionGated",  "conditionGate", "conditionTimesPositionGate"]



     header = ["HumanPosition","AnonymizedPosition","AnonymizedToken","OriginalToken","ExperimentToken","LuaID","JointPosition"]+coefficientNames

     print >> fOutNEAT, "\t".join(header)
     neatOutput = map(lambda x : x.rstrip().split("  "), fInNEAT.readlines())
     print(annotationOnlyTextsFolder+model+"/"+(textFile.replace("question.coef.anno","coef.csv")) )

     assert("split" in model)

     NUMBER_OF_COLUMN_ENTRIES = {"attention" : 5, "surprisal" : 3}[typeOfModel] #for the mapping file
     separationToken = "tiebreak"


     neatOutput = filter(lambda x : len(x) >= NUMBER_OF_COLUMN_ENTRIES, neatOutput)
     jointPosition = 0
     for mappingLine in fInTexts:
        mappingLine = mappingLine.rstrip().split("\t")

        # assume:
        # 0 position in human texts
        # 1 position in aomyized
        # 2 token in anonymized
        # 3 token in original
        # 4 token in human texts (with some problems at the beginning -- so just ignore the first few tokens for the evaluation)

        if len(mappingLine) < 5:
          mappingLine = mappingLine+([""]*(5-len(mappingLine)))          
        assert(len(mappingLine) == 5)


        positionHumanString = mappingLine[0]
        positionAnonymizedString = mappingLine[1]
        tokenAnonymized = mappingLine[2]
        tokenOriginal = mappingLine[3]
        tokenHuman = mappingLine[4]


        if len(mappingLine[1]) == 0:
          modelOutput = [""]*(3+len(coefficientNames))
        else: 
         positionInAnonymized = int(mappingLine[1])
         if positionInAnonymized < len(neatOutput):

           if True or typeOfModel == "attention":
             luaPosition = neatOutput[positionInAnonymized][0]
             luaID = neatOutput[positionInAnonymized][1]
             luaToken = neatOutput[positionInAnonymized][2]

             modelCoefficients = neatOutput[positionInAnonymized][3:]
             if len(modelCoefficients) < len(coefficientNames):
                 modelCoefficients = modelCoefficients + ([""]*(len(coefficientNames) - len(modelCoefficients)))
             if luaToken != tokenAnonymized:
                print(luaToken+"  "+tokenAnonymized)
         else:
           luaPosition = ""
           luaID = ""
           luaToken = ""
           modelCoefficients = ([""]*(len(coefficientNames)))
        jointPosition += 1
        outLine = [positionHumanString,positionAnonymizedString,tokenAnonymized,tokenOriginal,tokenHuman,luaID,str(jointPosition)] + modelCoefficients

        assert(len(outLine) == len(header)), outLine
        print >> fOutNEAT, "\t".join(outLine).replace('"',"-").replace("'","-")
 
    
     fInNEAT.close()
     fOutNEAT.close()
     fInTexts.close()
