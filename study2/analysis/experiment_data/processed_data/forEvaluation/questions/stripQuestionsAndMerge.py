import os

# takes csv's output by the Torch code and builds CSV files to be read by the R script.




ROOT_FOLDERS = ["/afs/cs.stanford.edu/u/mhahn/mhahn_files/", "/mhahn/repo2/mhahn_files/"]
ROOT_FOLDER = ROOT_FOLDERS[map(lambda x: x[1], filter(lambda x : x[0], zip(map(os.path.isdir, ROOT_FOLDERS), range(0,len(ROOT_FOLDERS)))))[0]]

if False:
  baseFolder = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/"
  annotationFolder = baseFolder+"/annotation/"
  annotationOnlyTextsFolder = baseFolder+"/annotation-textsonly/"
else:
  baseFolder = ROOT_FOLDER + "/ed1516/experiments/corpus/forEvaluation/"
  annotationFolder = baseFolder+"/questions/annotation/"
  annotationOnlyTextsFolder = baseFolder+"questions/annotation-textsonly/"


models = os.listdir(annotationFolder)
for model in models:
   if model.startswith(".svn"):
     continue
   if os.path.isfile(annotationFolder+model):
      continue
   assert(os.path.isdir(annotationFolder+model))
   if (not (os.path.isdir(annotationOnlyTextsFolder+model))):
     os.mkdir(annotationOnlyTextsFolder+model)
   assert(os.path.isdir(annotationOnlyTextsFolder+model))
   textFiles = os.listdir(annotationFolder+model)

   if model.startswith("att"):
     typeOfModel = "attention"
     if False:
       print("WARNING: NOT DOING ATTENTION NOW. 3339.")
       continue
   elif model.startswith("surp"):
     typeOfModel = "surprisal"
   else:
     assert False

   for textFile in textFiles:
     if textFile.startswith(".svn"):
       continue
     fInNEAT = open(annotationFolder+model+"/"+textFile,"r")
     if "coef.anno" in textFile:
        continue
     assert("question.att.anno" in textFile or "question.surp.anno" in textFile), textFile
     fInTexts = open(baseFolder+"/mapping/"+(textFile.replace("question.att.anno","csv").replace("question.surp.anno","csv")),"r")
     fOutNEAT = open(annotationOnlyTextsFolder+model+"/"+(textFile.replace("question.att.anno","csv").replace("question.surp.anno","csv")),"w")
     if typeOfModel == "attention":
        header = ["HumanPosition","AnonymizedPosition","AnonymizedToken","OriginalToken","ExperimentToken","LuaID","AttentionDecision","AttentionScore","JointPosition"]
     elif typeOfModel == "surprisal":
        header = ["HumanPosition","AnonymizedPosition","AnonymizedToken","OriginalToken","ExperimentToken","LuaID","Surprisal","JointPosition"]
     print >> fOutNEAT, "\t".join(header)
     neatOutput = map(lambda x : x.rstrip().split("  "), fInNEAT.readlines())
#[positionHumanString,positionAnonymizedString,tokenAnonymized,tokenOriginal,tokenHuman,luaID,luaAttentionDecision,luaAttentionScore]
     print(annotationOnlyTextsFolder+model+"/"+(textFile.replace("question.att.anno","csv").replace("question.surp.anno","csv")) )

     if "-qt-" in model:
       mode = "QT"
     elif "-tq-" in model:
       mode = "TQ"
     elif "-split-" in model:
       mode = "SPLIT"
     elif "langmod" in model:
       mode = "SPLIT"
     else:
       assert(False)


     NUMBER_OF_COLUMN_ENTRIES = {"attention" : 5, "surprisal" : 3}[typeOfModel] #for the mapping file
     separationToken = "tiebreak"


     neatOutput = filter(lambda x : len(x) >= NUMBER_OF_COLUMN_ENTRIES, neatOutput)
     if mode == "QT" or mode == "TQ":
        separatorPosition = next(i for i in range(0,len(neatOutput)) if neatOutput[i][2] == separationToken)
#        print(separatorPosition)
        if mode == "QT":
           neatOutput = neatOutput[separatorPosition+1:] 
        elif mode == "TQ":
           neatOutput = neatOutput[:separatorPosition] 
        else:
           assert(False)
#     print(neatOutput)
     jointPosition = 0
     for mappingLine in fInTexts:
        mappingLine = mappingLine.rstrip().split("\t")

        # assume:
        # 0 position in human texts
        # 1 position in aomyized
        # 2 token in anonymized
        # 3 token in original
        # 4 token in human texts (with some problems at the beginning -- so just ignore the first few tokens for the evaluation)

        if len(mappingLine) < 5:
          mappingLine = mappingLine+([""]*(5-len(mappingLine)))          
        assert(len(mappingLine) == 5)


        positionHumanString = mappingLine[0]
        positionAnonymizedString = mappingLine[1]
        tokenAnonymized = mappingLine[2]
        tokenOriginal = mappingLine[3]
        tokenHuman = mappingLine[4]


#        if typeOfModel == "surprisal":
 #            print("10521")
  #           print(mappingLine)
   #          print(neatOutput)
        if len(mappingLine[1]) == 0:
          luaPosition = ""
          luaID = ""
          luaToken = ""
          luaAttentionDecision = ""
          luaAttentionScore = ""
          luaSurprisal = ""
        else: 
         positionInAnonymized = int(mappingLine[1])
         if positionInAnonymized < len(neatOutput):
 #          if typeOfModel == "surprisal":
#             print(str(mappingLine)+"  "+str(neatOutput[positionInAnonymized]))


           if typeOfModel == "attention":
             luaPosition = neatOutput[positionInAnonymized][0]
             luaID = neatOutput[positionInAnonymized][1]
             luaToken = neatOutput[positionInAnonymized][2]
             luaAttentionDecision = neatOutput[positionInAnonymized][3]
             luaAttentionScore = neatOutput[positionInAnonymized][4]
             if luaToken != tokenAnonymized:
                print(luaToken+"  "+tokenAnonymized)
           elif typeOfModel == "surprisal":
             luaPosition = neatOutput[positionInAnonymized][0]
             luaID = neatOutput[positionInAnonymized][1]
#             luaToken = neatOutput[positionInAnonymized][2]
             luaSurprisal = neatOutput[positionInAnonymized][2]
         else:
           luaPosition = ""
           luaID = ""
           luaToken = ""
           luaAttentionDecision = ""
           luaAttentionScore = ""
           luaSurprisal = ""
        jointPosition += 1
        if typeOfModel == "attention":
          outLine = [positionHumanString,positionAnonymizedString,tokenAnonymized,tokenOriginal,tokenHuman,luaID,luaAttentionDecision,luaAttentionScore,str(jointPosition)]
        elif typeOfModel == "surprisal":
          outLine = [positionHumanString,positionAnonymizedString,tokenAnonymized,tokenOriginal,tokenHuman,luaID,luaSurprisal,str(jointPosition)]


        print >> fOutNEAT, "\t".join(outLine).replace('"',"-").replace("'","-")
 
     # first move to the beginning of the text in the NEAT output
#     while True:
#       nextLineOfNEATOutput = next(fInNEAT,"EOF").rstrip()
#       if nextLineOfNEATOutput == "EOF":
#         break
#       nextLineOfNEATOutput = nextLineOfNEATOutput.split("  ")
#       print(nextLineOfNEATOutput)

     #print(fInNEAT.next())
#     for line in fInNEAT: 
     
     fInNEAT.close()
     fOutNEAT.close()
     fInTexts.close()
