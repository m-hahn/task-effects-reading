# Copyright 2014 Google Inc. All Rights Reserved.

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
#
#
# adapted mhahn 2016, see below for explanation of what it does
#
#
#
#

"""Script for downloading and generating question/answer pairs.
"""

import argparse
from collections import namedtuple
import hashlib
from itertools import chain
from itertools import izip
from itertools import repeat
import math
from multiprocessing.pool import Pool
from multiprocessing.pool import ThreadPool
import os
import re
import sys
import time
import cchardet as chardet
from lxml import html
import requests
import socket
import numpy

# from https://rosettacode.org/wiki/Levenshtein_distance#Python
def minimumEditDistance(s1,s2):
    if len(s1) > len(s2):
        s1,s2 = s2,s1
    distances = range(len(s1) + 1)
    for index2,char2 in enumerate(s2):
        newDistances = [index2+1]
        for index1,char1 in enumerate(s1):
            if char1 == char2:
                newDistances.append(distances[index1])
            else:
                newDistances.append(1 + min((distances[index1],
                                             distances[index1+1],
                                             newDistances[-1])))
        distances = newDistances
    return distances[-1]



class Story(namedtuple('StoryBase', 'url content highlights')):

  def ToString(self):
    return self.content + ''.join([
        '\n\n@highlight\n\n' + highlight
        for highlight in
        self.highlights])


AnonymizedStory = namedtuple(
    'AnonymizedStory', 'url content highlights anonymization_info')
RawStory = namedtuple('RawStory', 'url html')
TokenizedStory = namedtuple('TokenizedStory', 'url tokens')


class QuestionContext(
    namedtuple(
        'QuestionContextBase',
        'url context question answer anonymization_info')):

  def ToString(self):
    return '%s\n\n%s\n\n%s\n\n%s\n\n%s' % (
        self.url, self.context, self.question, self.answer,
        '\n'.join(
            [
                key + ':' + value
                for key, value in self.anonymization_info.iteritems()]))


def ReadUrls(filename):
  """Reads a list of URLs.

  Args:
    filename: The filename containing the URLs.

  Returns:
    A list of URLs.
  """

  with open(filename) as f:
    return [line.strip('\n') for line in f]


def ReadMultipleUrls(filename):
  """Reads a list of URL lists.

  Each line in the filename should contain a list of URLs separated by comma.

  Args:
    filename: The filename containing the URLs.

  Returns:
    A list of list of URLs.
  """

  with open(filename) as f:
    return [line.strip('\n').split(',') for line in f]


def WriteUrls(filename, urls):
  """Writes a list of URLs to a file.

  Args:
    filename: The filename to the file where the URLs should be written.
    urls: The list of URLs to write.
  """

  with open(filename, 'w') as f:
    f.writelines(url + '\n' for url in urls)


def Hashhex(s):
  """Returns a heximal formated SHA1 hash of the input string.

  Args:
    s: The string to hash.

  Returns:
    A heximal formatted hash of the input string.
  """

  h = hashlib.sha1()
  h.update(s)
  return h.hexdigest()


def ReadDownloadedUrl(url, corpus):
  """Reads a downloaded URL from disk.

  Args:
    url: The URL to read.
    corpus: The corpus the URL belongs to.

  Returns:
    The content of the URL.
  """

  try:
    with open('%s/downloads/%s.html' % (corpus, Hashhex(url))) as f:
      return f.read()
  except IOError:
    return None


wayback_pattern = re.compile(r'web/([^/]*)/')


def WaybackUrl(urls, max_attempts=6):
  """Retrieves the URL for the latest historic copy using Wayback Machine.

  Args:
    urls: The URL for a specific page (canonical URL + forwarding URL's).
    max_attempts: The maximum attempts at requesting the URL.

  Returns:
    The URL or None if no copy is stored for the URL.

  Raises:
    RuntimeError: Failed to retrieve the URL.
  """

  if not urls:
    return None

  url = urls[0]

  index_collection_url = 'http://archive.org/wayback/available'

  payload = {'url': url}

  attempts = 0

  while attempts < max_attempts:
    try:
      entry_req = requests.get(index_collection_url, params=payload,
                               allow_redirects=False)

      if entry_req.status_code != requests.codes.ok:
        return WaybackUrl(urls[1:], max_attempts)

      entry = entry_req.json()

      if 'closest' not in entry['archived_snapshots']:
        return WaybackUrl(urls[1:], max_attempts)

      wayback_url = entry['archived_snapshots']['closest']['url']
      wayback_url = wayback_pattern.sub(r'web/\g<1>id_/', wayback_url, 1)
      return wayback_url

    except requests.exceptions.ConnectionError:
      pass

    # Exponential back-off.
    time.sleep(math.pow(2, attempts))
    attempts += 1

  raise RuntimeError(
      'Failed to download URL for %s after %d attempts. Please run the script '
      'again.' %
      (url, max_attempts))


def DownloadUrl(url, corpus, max_attempts=5, timeout=5):
  """Downloads a URL.

  Args:
    url: The URL.
    corpus: The corpus of the URL.
    max_attempts: Max attempts for downloading the URL.
    timeout: Connection timeout in seconds for each attempt.

  Returns:
    The HTML at the URL or None if the request failed.
  """

  try:
    with open('%s/downloads/%s.html' % (corpus, Hashhex(url))) as f:
      return f.read()
  except IOError:
    pass

  attempts = 0

  while attempts < max_attempts:
    try:
      req = requests.get(url, allow_redirects=False, timeout=timeout)

      if req.status_code == requests.codes.ok:
        content = req.text.encode(req.encoding)
        with open('%s/downloads/%s.html' % (corpus, Hashhex(url)), 'w') as f:
          f.write(content)
        return content
      elif (req.status_code in [301, 302, 404, 503]
            and attempts == max_attempts - 1):
        return None
    except requests.exceptions.ConnectionError:
      pass
    except requests.exceptions.ContentDecodingError:
      return None
    except requests.exceptions.ChunkedEncodingError:
      return None
    except requests.exceptions.Timeout:
      pass
    except socket.timeout:
      pass

    # Exponential back-off.
    time.sleep(math.pow(2, attempts))
    attempts += 1

  return None


def ParseHtml(story, corpus):
  #crash()
  """Parses the HTML of a news story.

  Args:
    story: The raw Story to be parsed.
    corpus: Either 'cnn' or 'dailymail'.

  Returns:
    A Story containing URL, paragraphs and highlights.
  """

  parser = html.HTMLParser(encoding=chardet.detect(story.html)['encoding'])
  tree = html.document_fromstring(story.html, parser=parser)

  # Elements to delete.
  delete_selectors = {
      'cnn': [
          '//blockquote[contains(@class, "twitter-tweet")]',
          '//blockquote[contains(@class, "instagram-media")]'
      ],
      'dailymail': [
          '//blockquote[contains(@class, "twitter-tweet")]',
          '//blockquote[contains(@class, "instagram-media")]'
      ]
  }

  # Paragraph exclusions: ads, links, bylines, comments
  cnn_exclude = (
      'not(ancestor::*[contains(@class, "metadata")])'
      ' and not(ancestor::*[contains(@class, "pullquote")])'
      ' and not(ancestor::*[contains(@class, "SandboxRoot")])'
      ' and not(ancestor::*[contains(@class, "twitter-tweet")])'
      ' and not(ancestor::div[contains(@class, "cnnStoryElementBox")])'
      ' and not(contains(@class, "cnnTopics"))'
      ' and not(descendant::*[starts-with(text(), "Read:")])'
      ' and not(descendant::*[starts-with(text(), "READ:")])'
      ' and not(descendant::*[starts-with(text(), "Join us at")])'
      ' and not(descendant::*[starts-with(text(), "Join us on")])'
      ' and not(descendant::*[starts-with(text(), "Read CNNOpinion")])'
      ' and not(descendant::*[contains(text(), "@CNNOpinion")])'
      ' and not(descendant-or-self::*[starts-with(text(), "Follow us")])'
      ' and not(descendant::*[starts-with(text(), "MORE:")])'
      ' and not(descendant::*[starts-with(text(), "SPOILER ALERT:")])')

  dm_exclude = (
      'not(ancestor::*[contains(@id,"reader-comments")])'
      ' and not(contains(@class, "byline-plain"))'
      ' and not(contains(@class, "byline-section"))'
      ' and not(contains(@class, "count-number"))'
      ' and not(contains(@class, "count-text"))'
      ' and not(contains(@class, "video-item-title"))'
      ' and not(ancestor::*[contains(@class, "column-content")])'
      ' and not(ancestor::iframe)')

  paragraph_selectors = {
      'cnn': [
          '//div[contains(@class, "cnnContentContainer")]//p[%s]' % cnn_exclude,
          '//div[contains(@class, "l-container")]//p[%s]' % cnn_exclude,
          '//div[contains(@class, "cnn_strycntntlft")]//p[%s]' % cnn_exclude
      ],
      'dailymail': [
          '//div[contains(@class, "article-text")]//p[%s]' % dm_exclude
      ]
  }

  # Highlight exclusions.
  he = (
      'not(contains(@class, "cnnHiliteHeader"))'
      ' and not(descendant::*[starts-with(text(), "Next Article in")])')
  highlight_selectors = {
      'cnn': [
          '//*[contains(@class, "el__storyhighlights__list")]//li[%s]' % he,
          '//*[contains(@class, "cnnStryHghLght")]//li[%s]' % he,
          '//*[@id="cnnHeaderRightCol"]//li[%s]' % he
      ],
      'dailymail': [
          '//h1/following-sibling::ul//li'
      ]
  }

  def ExtractText(selector):
    """Extracts a list of paragraphs given a XPath selector.

    Args:
      selector: A XPath selector to find the paragraphs.

    Returns:
      A list of raw text paragraphs with leading and trailing whitespace.
    """

    xpaths = map(tree.xpath, selector)
    elements = list(chain.from_iterable(xpaths))
    paragraphs = [e.text_content().encode('utf-8') for e in elements]

    # Remove editorial notes, etc.
    if corpus == 'cnn' and len(paragraphs) >= 2 and '(CNN)' in paragraphs[1]:
      paragraphs.pop(0)

    paragraphs = map(str.strip, paragraphs)
    paragraphs = [s for s in paragraphs if s and not str.isspace(s)]

    return paragraphs

  for selector in delete_selectors[corpus]:
    for bad in tree.xpath(selector):
      bad.getparent().remove(bad)

  paragraphs = ExtractText(paragraph_selectors[corpus])
  highlights = ExtractText(highlight_selectors[corpus])

  content = '\n\n'.join(paragraphs)

  return Story(story.url, content, highlights)


def WriteStory(story, corpus):
  """Writes a news story to disk.

  Args:
    story: The news story to write.
    corpus: The corpus the news story belongs to.
  """

  story_string = story.ToString()
  url_hash = Hashhex(story.url)

  with open('%s/stories/%s.story' % (corpus, url_hash), 'w') as f:
    f.write(story_string)


def LoadTokenMapping(filename):
  """Loads a token mapping from the given filename.

  Args:
    filename: The filename containing the token mapping.

  Returns:
    A list of (start, end) where start and
    end (inclusive) are offsets into the content for a token. The list is
    sorted.
  """

  mapping = []

  with open(filename) as f:
    line = f.readline().strip()

    for token_mapping in line.split(';'):
      if not token_mapping:
        continue

      start, length = token_mapping.split(',')

      mapping.append((int(start), int(start) + int(length)))

    mapping.sort(key=lambda x: x[1])  # Sort by start.

  return mapping


def Tokenize(story, corpus):
  """Tokenizes a news story.

  Args:
    story: The Story.
    corpus: The corpus of the news story.

  Returns:
    A TokenizedStory containing the URL and the tokens or None if no token
    mapping was found for the URL.
  """

  s = story.ToString()
  url_hash = Hashhex(story.url)
  print(url_hash)
  mapping_filename = '%s/tokens/%s.txt' % (corpus, url_hash)
  print(mapping_filename)
  if not os.path.exists(mapping_filename):
    return None
  
  mapping = LoadTokenMapping(mapping_filename)

  tokens = []

  for (start, end) in mapping:
    tokens.append(s[start:end + 1])

  return TokenizedStory(story.url, tokens)


def LoadEntityMapping(filename):
  """Loads an entity mapping from the given filename.

  Args:
    filename: The filename containing the entity mapping.

  Returns:
    A list of (entity_index, start, end)
    where start and end (inclusive) are token offsets for an entity. The list
    is sorted.
  """

  mapping = []

  with open(filename) as f:
    line = f.readline().strip()

    for entity_mapping in line.split(';'):
      if not entity_mapping:
        continue

      entity_index, start, end = entity_mapping.split(',')

      mapping.append((int(entity_index), int(start), int(end)))

    mapping.sort(key=lambda x: x[2])  # Sort by start.

  return mapping


def Anonymize(tokenized_story, corpus):
  """Anonymizes a tokenized news story.

  Args:
    tokenized_story: A TokenizedStory.
    corpus: The corpus of the tokenized news story.

  Returns:
    A Story containing the URL, anonymized content and anonymized highlights or
    None if no entity mapping exists for the news story.
  """
  url_hash = Hashhex(tokenized_story.url)

  mapping_filename = '%s/entities/%s.txt' % (corpus, url_hash)
  if not os.path.exists(mapping_filename):
    return None

  mapping = LoadEntityMapping(mapping_filename)

  mapping_index = 0
  mapping_len = len(mapping)

  new_tokens = []
  anonymization_info = {}

  i = 0
  while i < len(tokenized_story.tokens):
    if mapping_index < mapping_len and mapping[mapping_index][1] == i:
      entity_index, start, end = mapping[mapping_index]
      anonymized_entity_name = '@entity%d' % entity_index
      new_tokens.append(anonymized_entity_name)
      anonymization_info[anonymized_entity_name] = ' '.join(
          tokenized_story.tokens[start: end + 1]).replace(' - ', '-')

      mapping_index += 1
      i = end + 1
    else:
      new_tokens.append(tokenized_story.tokens[i])

      i += 1

  parts = ' '.join(new_tokens).split(' @ highlight ')

  content = parts[0]
  highlights = parts[1:]

  return AnonymizedStory(
      tokenized_story.url, content, highlights, anonymization_info)

entity_pattern = re.compile(r'@entity\d+')


def GenerateQuestionContexts(anonymized_story, context_token_limit):
  """Generates a list of question/answer pairs given an anonymized news story.

  One question/answer pair is generated for each anonymized entity appearing in
  the question.

  Args:
    anonymized_story: The anonymized news story.
    context_token_limit: If the context of a news story is above the limit, the
        empty list will be returned.

  Returns:
    A list of QuestionContext containing questions and answers.
  """

  result = []

  if anonymized_story.content.count(' ') + 1 > context_token_limit:
    return result

  entities_in_context = set(entity_pattern.findall(anonymized_story.content))

  for highlight in anonymized_story.highlights:
    for match in entity_pattern.finditer(highlight):
      start, end = match.span()

      answer = highlight[start:end]

      if answer not in entities_in_context:
        # Ignore entities that doesn't appear in the content as these will be
        # impossible (or very hard to answer).
        continue

      question = ('%s@placeholder%s' %
                  (highlight[0:start], highlight[end:])).lower()
      context = anonymized_story.content.lower()
      url = anonymized_story.url
      anonymization_info = anonymized_story.anonymization_info
      result.append(
          QuestionContext(url, context, question, answer, anonymization_info))

  return result


def WriteQuestionContext(question_context, corpus, dataset, fileName):
  """Writes a question/answer pair to disk.

  Args:
    question_context: The QuestionContext to write containing the question and
        answer.
    corpus: The corpus the question/answer belongs to.
    dataset: One of 'training', 'validation' and 'test'.
  """

  s = question_context.ToString()
##  h = Hashhex(s)

  with open('%s/questions/%s/%s.question' % (corpus, dataset, fileName), 'w') as f:
    f.write(s)


class ProgressBar(object):
  """Simple progress bar.

  Output example:
    100.00% [2152/2152]
  """

  def __init__(self, total=100, stream=sys.stderr):
    self.total = total
    self.stream = stream
    self.last_len = 0
    self.curr = 0

  def Increment(self):
    self.curr += 1
    self.PrintProgress(self.curr)

    if self.curr == self.total:
      print ''

  def PrintProgress(self, value):
    self.stream.write('\b' * self.last_len)
    pct = 100 * self.curr / float(self.total)
    out = '{:.2f}% [{}/{}]'.format(pct, value, self.total)
    self.last_len = len(out)
    self.stream.write(out)
    self.stream.flush()


datasets = ['training', 'validation', 'test']


def UrlMode(corpus, request_parallelism):
  """Finds Wayback Machine URLs and writes them to disk.

  Args:
    corpus: A corpus.
    request_parallelism: The number of concurrent requests.
  """

  for dataset in datasets:
    print 'Finding Wayback Machine URLs for the %s set:' % dataset
    old_urls_filename = '%s/%s_urls.txt' % (corpus, dataset)
    new_urls_filename = '%s/wayback_%s_urls.txt' % (corpus, dataset)

    urls = ReadMultipleUrls(old_urls_filename)

    p = ThreadPool(request_parallelism)
    results = p.imap_unordered(WaybackUrl, urls)

    progress_bar = ProgressBar(len(urls))
    new_urls = []
    for result in results:
      if result:
        new_urls.append(result)

      progress_bar.Increment()

    WriteUrls(new_urls_filename, new_urls)


def DownloadMapper(t):
  """Downloads an URL and checks that metadata is available for the URL.

  Args:
    t: a tuple (url, corpus).

  Returns:
    A pair of URL and content.

  Raises:
    RuntimeError: No metadata available.
  """

  url, corpus = t

  url_hash = Hashhex(url)

  mapping_filename = '%s/entities/%s.txt' % (corpus, url_hash)
  if not os.path.exists(mapping_filename):
    raise RuntimeError('No metadata available for %s.' % url)

  return url, DownloadUrl(url, corpus)


def DownloadMode(corpus, request_parallelism):
  """Downloads the URLs for the specified corpus.

  Args:
    corpus: A corpus.
    request_parallelism: The number of concurrent download requests.
  """

  missing_urls = []
  for dataset in datasets:
    print 'Downloading URLs for the %s set:' % dataset

    urls_filename = '%s/wayback_%s_urls.txt' % (corpus, dataset)
    urls = ReadUrls(urls_filename)

    missing_urls_filename = '%s/missing_urls.txt' % corpus
    if os.path.exists(missing_urls_filename):
      print 'Only downloading missing URLs'
      urls = list(set(urls).intersection(ReadUrls(missing_urls_filename)))

    p = ThreadPool(request_parallelism)
    results = p.imap_unordered(DownloadMapper, izip(urls, repeat(corpus)))

    progress_bar = ProgressBar(len(urls))

    collected_urls = []
    try:
      for url, story_html in results:
        if story_html:
          collected_urls.append(url)

        progress_bar.Increment()
    except KeyboardInterrupt:
      print 'Interrupted by user'

    missing_urls.extend(set(urls) - set(collected_urls))

  WriteUrls('%s/missing_urls.txt' % corpus, missing_urls)

  if missing_urls:
    print ('%d URLs couldn\'t be downloaded, see %s/missing_urls.txt.'
           % (len(missing_urls), corpus))
    print 'Try and run the command again to download the missing URLs.'


def StoreMapper(t):
  """Reads an URL from disk and returns the parsed news story.

  Args:
    t: a tuple (url, corpus).

  Returns:
    A Story containing the parsed news story.
  """

  url, corpus = t

  story_html = ReadDownloadedUrl(url, corpus)

  if not story_html:
    return None

  raw_story = RawStory(url, story_html)

  return ParseHtml(raw_story, corpus)


def StoreMode(corpus):
  for dataset in datasets:
    print 'Storing news stories for the %s set:' % dataset
    urls_filename = '%s/wayback_%s_urls.txt' % (corpus, dataset)
    urls = ReadUrls(urls_filename)

    p = Pool()
    stories = p.imap_unordered(StoreMapper, izip(urls, repeat(corpus)))

    progress_bar = ProgressBar(len(urls))
    for story in stories:
      if story:
        WriteStory(story, corpus)

      progress_bar.Increment()


def GenerateMapper(t):
  """Reads an URL from disk and returns a list of question/answer pairs.

  Args:
    t: a tuple (url, corpus).

  Returns:
    A list of QuestionContext containing a question and an answer.
  """

  url, corpus, context_token_limit = t
  story_html = ReadDownloadedUrl(url, corpus)

  if not story_html:
    return None

  raw_story = RawStory(url, story_html)

  story = ParseHtml(raw_story, corpus)
  tokenized = Tokenize(story, corpus)

  if not tokenized:
    return None

  anonymized = Anonymize(tokenized, corpus)

  if not anonymized:
    return None

  return GenerateQuestionContexts(anonymized, context_token_limit)


def GenerateMode(corpus, context_token_limit):
  for dataset in datasets:
    print 'Generating questions for the %s set:' % dataset

    urls_filename = '%s/wayback_%s_urls.txt' % (corpus, dataset)
    urls = ReadUrls(urls_filename)

    p = Pool()
    question_context_lists = p.imap_unordered(
        GenerateMapper, izip(urls, repeat(corpus), repeat(context_token_limit)))

    progress_bar = ProgressBar(len(urls))
    for question_context_list in question_context_lists:
      if question_context_list:
        for question_context in question_context_list:
          WriteQuestionContext(question_context, corpus, dataset)

      progress_bar.Increment()


def RemoveMode(corpus):
  missing_urls = set(ReadUrls('%s/missing_urls.txt' % corpus))

  for dataset in datasets:
    urls_filename = '%s/wayback_%s_urls.txt' % (corpus, dataset)
    urls = ReadUrls(urls_filename)

    new_urls = []

    for url in urls:
      if url not in missing_urls:
        new_urls.append(url)

    WriteUrls(urls_filename, new_urls)


def main():




  crash()
  parser = argparse.ArgumentParser(
      description='Generates question/answer pairs')
  parser.add_argument('--corpus', choices=['cnn', 'dailymail'], default='cnn')
  parser.add_argument(
      '--mode', choices=['store', 'generate', 'download', 'urls', 'remove'],
      default='generate')
  parser.add_argument('--request_parallelism', type=int, default=200)
  parser.add_argument('--context_token_limit', type=int, default=2000)
  args = parser.parse_args()

  stories_dir = '%s/stories' % args.corpus
  if not os.path.exists(stories_dir):
    os.mkdir(stories_dir)

  downloads_dir = '%s/downloads' % args.corpus
  if not os.path.exists(downloads_dir):
    os.mkdir(downloads_dir)

  questions_dir = '%s/questions' % args.corpus
  if not os.path.exists(questions_dir):
    os.mkdir(questions_dir)

  for dataset in datasets:
    dataset_dir = '%s/questions/%s' % (args.corpus, dataset)
    if not os.path.exists(dataset_dir):
      os.mkdir(dataset_dir)

  if args.mode == 'store':
    StoreMode(args.corpus)
  elif args.mode == 'generate':
    GenerateMode(args.corpus, args.context_token_limit)
  elif args.mode == 'download':
    DownloadMode(args.corpus, args.request_parallelism)
  elif args.mode == 'urls':
    UrlMode(args.corpus, args.request_parallelism)
  elif args.mode == 'remove':
    RemoveMode(args.corpus)



def GetData(url, corpus):
  DownloadUrl(url, corpus)
  story_html = ReadDownloadedUrl(url, corpus)

  raw_story = RawStory(url, story_html)

  story = ParseHtml(raw_story, corpus)
  return story

def GetArticle(url, corpus):
  story = GetData(url)
  text = story.content
  highlights = story.highlights
  return story

import shutil

def Main():
#  crash()
  CORPUS = "dailymail" #"cnn" # "dailymail"
  fileCounter = 0
  fileList = open(CORPUS+"/fileListTest.txt", "r") #"/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/experiments/corpus/"+CORPUS+"/fileListTest.txt", 'r')
  for line in fileList:
    fileName = line.rstrip()
    fileCounter += 1
    if fileCounter > 500:
        break 
    if 0:
      shutil.copyfile("/disk/scratch_ssd/deepmind/rc-data/"+CORPUS+"/questions/test/"+fileName, "/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/experiments/corpus/"+CORPUS+"/docs/"+fileName)
      continue
    # process file
    article = open("/disk/scratch_ssd/deepmind/rc-data/"+CORPUS+"/questions/test/"+fileName, 'r') # "/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/experiments/corpus/"+CORPUS+"/docs/"+fileName, 'r')
    counter = 0
    for line2 in article:
      if counter == 0:
         url = line2.rstrip()
      elif counter == 2:
         text = line2.split(" ")
         length = len(text)
      elif counter == 4:
         question = line2.rstrip()+" "
      elif counter == 6:
         answer = line2.rstrip().lstrip()+" "
      elif "@entity" in line2 :
         line2 = line2.rstrip().split(":")
         if len(line2) > 1:
           answer = answer.replace(line2[0]+" ", line2[1]+" ")
           question = question.replace(line2[0]+" ", line2[1]+" ")
      counter += 1
    #if length<1000:
#    print(url)
    story = GetData(url, CORPUS)

    # find out where the answer occurs
    answerParts = answer.split(" ")
    answerLoc = -1
    answer = answer.rstrip()
    text = story.content.replace("\n","").replace("\"","").replace("'", "")
#    for answerPart in answerParts:
#       newLoc = text.find(answer)
#       if newLoc > 0:
#         if answerLoc > 0:
#           answerLoc = min(newLoc, answerLoc) + 0.0
#         else:
#           answerLoc = newLoc
    print("|"+answer+"|")
    print(text.find(answer))
    if len(text) == 0:
         continue
    answerLoc = (text.find(answer)+0.0) / len(text)
#    print(answerLoc)
    if answerLoc > 0.3:
      print("  "+answer+"  "+str(answerLoc))
    print(len(text.split(" ")))
    if (1 and len(text.split(" ")) > 500) or (0 and answerLoc < 0.3):
       continue
    print("\n\n\n")
    print(text.find(answer))
    print(len(text))
    print(text)
    print(text[text.find(answer):text.find(answer)+5])
    print(text.find(answer))
#    print("GOAT DATA")
    file = open(CORPUS+"-docs/"+CORPUS+"-text-"+str(length)+".txt", "w")  #"/afs/inf.ed.ac.uk/user/s15/s1582047/repo/mhahn_files/ed1516/experiments/corpus/"+CORPUS+"-docs/"+CORPUS+"-text-"+str(length)+".txt", "w")
    file.write(str(length)+"\n")
    file.write(url)
    file.write("\n\n--\n\n")
    file.write(story.content.replace("\n\n", "\n")+"\n")
    file.write("\n--\n\n")
    file.write(question+"\n")
    file.write(str(answerLoc)+"\n")
    file.write("+"+answer+"\n")
    file.write("-\n-\n-\n\n")
    for sentence in story.highlights:
      file.write(sentence+"\n\n")
      file.write("+\n-\n-\n-\n\n")
    file.close()
    #print(".\n"+str(length)+"\n   "+url+"\n  "+"/disk/scratch_ssd/deepmind/rc-data/"+CORPUS+"/questions/test/"+fileName+"\n  "+question+"\n  "+answer)


#
# This ad-hoc function replicates the tokenization of the original data for the alignment to the eyetracking data
#
def createATokenizedVersionOfText(text):
   text = text+" "
   text = text.replace(". "," . ")
   text = text.replace(", "," , ")
   text = text.replace(";"," ; ")
   text = text.replace("("," ( ")
   text = text.replace(")"," ) ")
#   text = text.replace("-"," - ")
   text = text.replace("?"," ? ")
   text = text.replace("!"," ! ")

#   text = text.replace(". "," . ")
   text = text.replace('"',' " ')
   text = text.replace(", "," , ")
#   text = text.replace(". "," . ")

   text = text.replace("-", " - ")

#   text = text.replace("smoke-free","smoke - free")
   text = text.replace("over - the","over-the")
   text = text.replace("anti - riot","anti-riot")
   text = text.replace("semi - final","semi-final")

#   text = text.replace("Times-Picayune","Times - Picayune")
#   text = text.replace("must-do","must - do")
#   text = text.replace("murder-suicide","murder - suicide")
   text = text.replace("a.m .","a.m. ")
   text = text.replace("a.m  .","a.m. ")
   text = text.replace("a.m   .","a.m. ")
   text = text.replace("Co .","Co. ")
   text = text.replace("Co  .","Co. ")
   text = text.replace("U.S .","U.S. ")
   text = text.replace("U.S  .","U.S. ")
   text = text.replace("Feb .","Feb. ")
   text = text.replace("B.B .","B.B. ")
   text = text.replace("Mr .","Mr. ")
   text = text.replace("Jr .","Jr. ")

   text = text.replace(".. .",". ")

 
   text = text.replace("who was team doctor","who team doctor")


   text = text.replace("gonna"," gon na ")

   text = text.replace("www.luketheboxer.com  ,","www.luketheboxer.com ")

   text = text.replace("14-day","  14 - day ")

   print(text)

   text = text.replace("'"," ' ")
   text = text.replace(" ' s "," 's ")
   text = text.replace("n ' t "," n't ")
   text = text.replace(" ' m "," 'm ")
   text = text.replace(" ' ve "," 've ")
   text = text.replace(" ' ll "," 'll ")
   text = text.replace(" ' re "," 're ")

   text = text.replace("% "," % ")

   text = text.replace("\xc2\xa3"," \xc2\xa3 ")
   text = text.replace("$"," $ ")

   while(text[0] == " "):
    text = text[1:]
   while(text[-1] == " "):
    text = text[:-1]




   text = text.replace("  "," ")
   text = text.replace("  "," ")
   text = text.replace("  "," ")
   text = text.replace("  "," ")
   text = text.replace("  "," ")

   text = text.replace("- -","--")

#   print(text)
#   print(text.split(" "))
   return text.split(" ")


###################################################################
#
# mhahn 2016
# Goal is to
#  (1) build anonymized files specifically for the items occurring in the human set, which are put into cnn/ and dailymail/ respectively in questions/evaluation
#  (2) align these token-by-token to the files used for the human experiments. These alignments are put into mapping/
#
# There are some minor problems. (1) sometimes the human indices start 0, 2, ..., and the zero element has @POSID... as its human token.
# (2) in the nagobads text, there is a jump in the human indices
# (3) in the nagobads text, the token "was" at the beginning is not inserted into the table because it is missing in the original webdata (the code cannot deal with things that are missing in the webdata, it just handles things that are missing in the experimental data because they were removed, such as image captions)
#
###################################################################


itemCounter = 0

trainingNumber = 2


listFile = open("../selection.txt", "r")
numberOfDependentPages = {}
correctAnswers = {}
originalFiles = {}
EP = "P"
for fileName in listFile:
   if fileName[0] == "#":
      continue
   itemCounter += 1
   if itemCounter == trainingNumber+1:
      EP = "E"
   fileName = fileName.rstrip()
   print
   print
   print
   print
   print("---------"+fileName)
   print
   print

   originalFiles[itemCounter] = fileName
   itemFile = open("../CORPUS/"+fileName, "r")
   mode = 0
   text = ""
   question = ""
   correctAnswer = ""
   answers = []
   url = ""
   for line in itemFile:
      if len(answers) == 4:
         break
      line = line.rstrip().replace("  "," ")
      if len(line) == 0:
         continue
      elif line == "--":
         mode += 1
      elif mode == 0 and "http" in line:
         url = line
         url = url.replace(" ","")
      elif mode == 1:
         text = text+" "+line
      elif mode == 2:
         if line[0] == "+":
            correctAnswers[itemCounter] = line[1:].lstrip()
            answers.append(line[1:].lstrip())
         elif line[0] == "-":
            answers.append(line[1:].lstrip())
         elif line[0:2] != "0.":
            question = line.lstrip()

   print(text)
   print(question)
   print(answers)
   print("URL "+url)

   dependentCounter = 0
   itemNumber = itemCounter
   if EP == "E":
     itemNumber -= trainingNumber




   print("trial "+EP+"1I"+str(itemNumber))
   if "cnn" in url:
     corpus = "cnn"
   else:
     corpus = "dailymail"

   # download the story from the web again

   result = DownloadMapper((url,corpus))
   story = GetData(url, corpus)

   print(story)

   tokenized = Tokenize(story, corpus)
   
   print(tokenized)

#   outFile = open("mapping/"+EP+"-"+str(itemNumber)+".csv", "w")

   textSplit = text.split(" ") 
   newText = []
   for i in range(0, len(textSplit)):
     newText.append("@POSID"+str(i))
     newText.append(textSplit[i])
   tokenizedText = createATokenizedVersionOfText(" ".join(newText))

   lag = 0
       
   def identical(x,y):
    return minimumEditDistance(x,y) < 4

   countAfterPosId = 0
   lastPositionInHuman = -1    

   alignedData = []

   for i in range(0,len(tokenizedText)):
     lagPrior = lag
     if "@POSID" in tokenizedText[i]:
       alignedData.append(tokenizedText[i])
       countAfterPosId = 0
       lastPositionInHuman = tokenizedText[i][6:]
       lag -= 1
       continue
     countAfterPosId += 1

     # Here, I have hard-coded one specific problem in the data where "was" is missing in the original data and was added for the eyetracking experiment
     if tokenizedText[i] == "was" and tokenizedText[i+2] == "team":
        lag -= 1
        continue


     while(lag + i < len(tokenized.tokens)):
       if identical(tokenized.tokens[lag+i], tokenizedText[i]):
          break
       else:
          alignedData.append(tokenized.tokens[lag+i])
#          print >> outFile,("\t"+str(lag+i)+"\t"+tokenized.tokens[lag+i]+"\t")
          lag += 1
     alignedData.append(tokenized.tokens[lag+i])
#     if countAfterPosId == 1:
#       print >> outFile,( lastPositionInHuman+"\t"+str(lag+i)+"\t"+tokenized.tokens[lag+i]+"\t"+textSplit[int(lastPositionInHuman)])
#     else:
#       print >> outFile,("\t"+str(lag+i)+"\t"+tokenized.tokens[lag+i]+"\t")





   # At this point, we have aligned the eyetracking version with the unanonymized but pre-tokenized version.
   # Now, we want to align these with the anonymized version.

   print(alignedData)


#   outFile.close()

   outFile = open("mapping/"+EP+"-"+str(itemNumber)+".csv", "w")

   anonymized = Anonymize(tokenized, corpus)

   print(anonymized)
   

   humanPositionIsNew = 0
   anonymizedList = anonymized.content.split(" ")
   print(anonymizedList)
   lag = 0
   for i in range(0,len(anonymizedList)):
     if i+lag == len(alignedData): #the eyetracking data may be shorter than anonymizedLis due to things missing at the end, so alignedData is also shorter
       break
     
     if "@POSID" in alignedData[i+lag]:
       lastPositionInHuman = alignedData[i+lag][6:]
       humanPositionIsNew = 1

       lag += 1
     if "@entity" in anonymizedList[i]:
       if humanPositionIsNew:
         print >> outFile,(lastPositionInHuman+"\t"+str(i)+"\t"+anonymizedList[i]+"\t"+alignedData[i+lag]+"\t"+textSplit[int(lastPositionInHuman)])
       else:
         print >> outFile,("\t"+str(i)+"\t"+anonymizedList[i]+"\t"+alignedData[i+lag])
       humanPositionIsNew = 0

       continue
     while (i+lag < len(alignedData)):
       if alignedData[i+lag] == anonymizedList[i]:
          break
       else:
         if "@POSID" in alignedData[i+lag]:
            lastPositionInHuman = alignedData[i+lag][6:]
            humanPositionIsNew = 1

         else:  
           if humanPositionIsNew:
             print >> outFile,(lastPositionInHuman+"\t"+"\t"+"\t"+alignedData[i+lag]+"\t"+textSplit[int(lastPositionInHuman)])
           else:
             print >> outFile,("\t"+"\t"+"\t"+alignedData[i+lag])

           humanPositionIsNew = 0

         lag += 1     

     if humanPositionIsNew:
             print >> outFile,(lastPositionInHuman+"\t"+str(i)+"\t"+anonymizedList[i]+"\t"+alignedData[i+lag]+"\t"+textSplit[int(lastPositionInHuman)])
     else:
             print >> outFile,("\t"+str(i)+"\t"+anonymizedList[i]+"\t"+alignedData[i+lag])


     humanPositionIsNew = 0

   outFile.close()


   print(question)
   print("...")
   question_context = GenerateQuestionContexts(anonymized, 2000)
   distances = map(lambda x : minimumEditDistance(question,x.question), question_context)
   bestContext = question_context[numpy.argmin(distances)]
   WriteQuestionContext(bestContext, corpus, "evaluation", EP+"-"+str(itemNumber))
   print(bestContext.question)   











