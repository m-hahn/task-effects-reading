#"","X","Participant","TextNo","JointPosition","HumanPosition.x","AnonymizedPosition.x","AnonymizedToken.x","OriginalToken.x","ExperimentToken.x","ExperimentTokenLength","IsNamedEntity","OccursInQuestion","IsCorrectAnswer","POS.PTB","POS.Universal","PositionInHumanSentence","OccurrencesOfCorrectAnswerSoFar","IsFirstInLine","WordFreq","HumanPosition.x.1","AnonymizedPosition.x.1","AnonymizedToken.x.1","OriginalToken.x.1","ExperimentToken.x.1","Condition","tt","fp","rp","ff","HumanPosition.y","AnonymizedPosition.y","AnonymizedToken.y","OriginalToken.y","ExperimentToken.y","LuaID.x","AttentionDecision","AttentionScore","HumanPosition.y.1","AnonymizedPosition.y.1","AnonymizedToken.y.1","OriginalToken.y.1","ExperimentToken.y.1","LuaID.y","Surprisal","tokenID","FIXATED","IsPunctuation","FunctionWord","IsPunctuation.Centered","FunctionWord.Centered","PositionInHumanSentence.Centered","IsNamedEntity.Centered","OccursInQuestion.Centered","IsCorrectAnswer.Centered","OccurrencesOfCorrectAnswerSoFar.Centered","ExperimentTokenLength.Centered","fp.Centered","AttentionScore.Centered","AttentionDecision.Centered","HumanPosition","HumanPosition.Centered","Correct","Condition.dummy","Condition.dummy.Centered","Surprisal.Centered","LogWordFreq","LogWordFreq.Centered"
#"1",1,"NP_1",10,1,0,0,"@entity0","Raheem","Raheem",6,TRUE,TRUE,FALSE,"NNP","NOUN",1,0,FALSE,883763,0,0,"@entity0","Raheem","Raheem","nopreview",227,0,0,0,0,0,"@entity0","Raheem","Raheem",119,1,0.93503051996231,0,0,"@entity0","Raheem","Raheem",41,3.2334651947021,100001,0,FALSE,FALSE,-0.343612530953755,-0.727948083986857,-1.34085697494207,0.883754633371422,0.847690978512305,-0.0153423817619353,-1.86458013990691,1.31786349286984,-97.8812179789979,0.223636507184801,0.282082170935836,0,-0.55672309106523,1,0.5,0.545344332655166,-0.379385745856921,13.6919442061272,0.34964637679042
#"2",2,"NP_1",10,2,2,NA,"","Sterling","Sterling",8,TRUE,TRUE,FALSE,"NNP","NOUN",2,0,FALSE,NA,2,NA,"","Sterling","Sterling","nopreview",289,145,516,145,2,NA,"","Sterling","Sterling",NA,NA,NA,2,NA,"","Sterling","Sterling",NA,NA,100002,1,FALSE,FALSE,-0.343612530953755,-0.727948083986857,-1.2165246694166,0.883754633371422,0.847690978512305,-0.0153423817619353,-1.86458013990691,3.31786349286984,47.1187820210021,NA,NA,2,-0.550056424398563,1,0.5,0.545344332655166,NA,NA,NA


header = None
headerAdditional = ["ExperimentTokenLength", "IsNamedEntity", "IsCorrectAnswer", "OccurrencesOfCorrectAnswerSoFar", "WordFreq"]

with open("aggregated.tsv", "w") as outFile:
 for condition in ["preview", "nopreview"]:
   for text in range(1, 21):
      with open(f"../forEvaluation/mappingWithExternal/E-{text}.csv", "r") as inFile:
          headerExt = next(inFile).strip("\n").split("\t")
          data = [x.strip("\n").split("\t") for x in inFile]
#          print(data[:5])
 #         quit()
      for participant in range(1,13):
         try:
          with open(f"../forEvaluation/mappingWithHuman/{condition}/Participant-{participant}/E-{text}.csv", "r") as inFile:
            header_ = next(inFile).strip()
            if header is None:
                header = header_
                print("TextFileName\t"+header.strip()+"\t"+"\t".join(headerAdditional), file=outFile)
            else:
                assert header == header_
            lineCount = 0
            for line in inFile:
#                print(line)
                line = line.strip("\n")
                assert len(line.split("\t")) ==  len(header.split("\t"))
#                if len(line.split("\t")) == 8:
#                    print(line.split("\t"))
                line = line.split("\t")
                assert line[6] in ["preview", "nopreview"], line
                line[6] = line[6].replace("no", "No").replace("pre", "Pre")
                line[5] = line[6][0]+"_"+line[5]

 #               print(data[lineCount])
                for col in headerAdditional:
                    line.append(data[lineCount][headerExt.index(col)])
                assert line[4] == data[lineCount][headerExt.index("ExperimentToken")]
                
                lineCount += 1


#                line = [x if x != "" else "NA" for x in line]
                line = "\t".join(line)
                print(f"TE-{text}\t"+line, file=outFile)
#                quit()
         except FileNotFoundError:
            pass

