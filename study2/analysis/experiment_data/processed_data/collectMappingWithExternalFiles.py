header = None
with open("mappingWithExternal.tsv", "w") as outFile:
   for text in range(1, 21):
         try:
          with open(f"forEvaluation/mappingWithExternal/E-{text}.csv", "r") as inFile:
            header_ = next(inFile).strip()
            if header is None:
                header = header_
                print("TextFileName\t"+header.strip(), file=outFile)
            else:
                assert header == header_
            for line in inFile:
                line = line.strip("\n")
                assert len(line.split("\t")) ==  len(header.split("\t"))
#                if len(line.split("\t")) == 8:
#                    print(line.split("\t"))
                line = line.split("\t")
                line = "\t".join(line)
                print(f"TE-{text}\t"+line, file=outFile)
         except FileNotFoundError:
            print("File not found")
            pass

