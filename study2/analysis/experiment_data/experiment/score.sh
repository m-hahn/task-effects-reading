#!/bin/bash

LIST="$(ls *NPR.asc.txt)"
for i in $LIST; do
     printf $i
     printf ": "
     get_question_answers.perl $i
done

printf "\nAverage:     "
get_question_answers.perl $LIST
printf "\n\n"

LIST="$(ls *PRE.asc.txt)"
for i in $LIST; do
     printf $i
     printf ": "
     get_question_answers.perl $i
done

printf "\nAverage:     "
get_question_answers.perl $LIST

