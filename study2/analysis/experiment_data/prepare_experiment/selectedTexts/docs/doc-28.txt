469
   http://web.archive.org/web/20150724081821id_/http://www.dailymail.co.uk/news/article-3027897/Ex-Marine-apologizes-prison-setting-mosque-fire.html

  /disk/scratch_ssd/deepmind/rc-data/dailymail/questions/test/2a73a2c1847f0c1ea2311b3d0995534221a68e3a.question
  Randy Linn was sentenced two years ago for starting a fire that caused $ 1million in damages to a suburban @placeholder , Ohio mosque 
  Toledo 
.

Randy Linn was sentenced two years ago for starting a fire that caused $1million in damages to a suburban Toledo, Ohio mosque
He is now in a California prison and says he 'can't believe' what he did
Prosecutors said he drove two hours from his home and broke into the mosque, where he poured gasoline on a prayer rug and lit it on fire
He said that being forgiven would give him 'a little bit of peace of mind' 
--
A former Marine who was sentenced two years ago for starting a fire that caused over $1million in damages to an Ohio mosque has sent a letter apologizing and asking forgiveness from its members.
Randy Linn, now in a California federal prison where he's two years into his 20-year sentence, said that he can't believe that he tried to destroy a place of worship.
Federal prosecutors said Linn drove about two hours from his home in St. Joe, Indiana, to suburban Toledo on September 30, 2012, and broke into the mosque where he poured gasoline on a prayer rug and lit it on fire. 
'Well again, I’m very sorry for what I did. Hopefully someday you all can forgive me. And yes I don’t like it here, you can tell everyone, that I’m sure will put a smile on everyone face,' he said in his letter, according to The Blade.
Linn had several firearms in his car and carried a gun into the mosque, which was empty at the time.
Linn later said while pleading guilty to hate crime charges that he'd become enraged after seeing images of wounded soldiers in the news and decided to burn the mosque. 
He also blamed what happened on a day of heavy drinking.
Being forgiven, he wrote last December, would 'probably give me a little bit of peace of mind.'
Cherrefe Kadri, president of the Islamic Center of Greater Toledo, told The Blade said she waited nearly six weeks to respond to Linn's letter.
Most of the members have moved on, she said. 'I think his letter was truly sincere, and I told him that,' she said.
Her response, written on stationery with an image of the mosque at the top, said that she hopes he can make peace with God.
'Neither our community nor I harbor any hatred, ill-will or malice toward you,' she wrote.
A sprinkler system extinguished the blaze, leaving smoke and water damage in the prayer room of the facility. No one was injured in the fire

