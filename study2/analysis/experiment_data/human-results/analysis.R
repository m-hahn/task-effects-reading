library(lme4)
library(ggplot2)

########## nopreview condition

# read in data
nopreview <- read.table("nopreview.fp", sep = "\t", col.names = c("subj","item","cond","region"), as.is = c("region"));
nopreview$region <- lapply(strsplit(nopreview$region, " "), as.numeric);

# recode to be the same as preview condition
nopreview$item <- nopreview$item + 1;
nopreview$cond <- as.factor("nopreview");

# compute skipping rate
nopreview$skips <- as.numeric(lapply(nopreview$region, function(x) length(x[x==0])/length(x)));

# compute mean reading time per item (removing 0s)
nopreview$mean <- as.numeric(lapply(nopreview$region, function(x) sum(x[x>0])/length(x[x>0])))


########## preview condition

# read in data
preview <- read.table("preview.fp", sep = "\t", col.names = c("subj","item","cond","region"), as.is = c("region"));
preview$region <- lapply(strsplit(preview$region, " "), as.numeric);

# recode to be the same as preview condition
preview$subj <- preview$subj + 12;
preview$cond <- as.factor("preview");

# compute skipping rate
preview$skips <- as.numeric(lapply(preview$region, function(x) length(x[x==0])/length(x)));

# compute mean reading time per item (removing 0s)
preview$mean <- as.numeric(lapply(preview$region, function(x) sum(x[x>0])/length(x[x>0])))


# merge the two dataframes
all <- rbind(nopreview, preview);


########## mixed models

# first some summary stats
aggregate(all$mean, list(Cond = all$cond), na.rm=TRUE, mean);
aggregate(all$skips, list(Cond = all$cond), na.rm=TRUE, mean);

# center predictors
all$cond <- as.numeric(all$cond)
all$cond <- all$cond - mean(all$cond)

# mixed models
summary(lmer(mean ~ (1 | subj) + (1 | item) + cond, data=all));
summary(lmer(skips ~ (1 | subj) + (1 | item) + cond, data=all));
