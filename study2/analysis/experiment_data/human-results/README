###############################################
   Make Region, Count, and Measures Files
###############################################

# insert region boundaries
../measures/insert_regions.pl deepmind_reading_exp_nopreview_prepared.script > nopreview.script
../measures/insert_regions.pl deepmind_reading_exp_preview_prepared.script > preview.script

# create the count file
../measures/make_region_file.pl nopreview.script > nopreview.cnt
../measures/make_region_file.pl preview.script > preview.cnt

# create list files by hand
# nopreview.lst
# preview.lst

##### compute measures for no preview condition
cp nopreview.cnt exp.cnt
cp nopreview.lst exp.lst

# compute measures 
# note that "f1" and "1" are ignored: the output contains both items
# and subject ID, and all regions
../measures/measures.pl rp f1 1 > nopreview.rp
../measures/measures.pl fp f1 1 > nopreview.fp
../measures/measures.pl ff f1 1 > nopreview.ff
../measures/measures.pl ro f1 1 > nopreview.ro
../measures/measures.pl rb f1 1 > nopreview.rb
../measures/measures.pl sp f1 1 > nopreview.sp
../measures/measures.pl tt f1 1 > nopreview.tt

##### compute measures for preview condition
cp preview.cnt exp.cnt
cp preview.lst exp.lst

# compute measures 
# note that "f1" and "1" are ignored: the output contains both items
# and subject ID, and all regions
../measures/measures.pl rp f1 1 > preview.rp
../measures/measures.pl fp f1 1 > preview.fp
../measures/measures.pl ff f1 1 > preview.ff
../measures/measures.pl ro f1 1 > preview.ro
../measures/measures.pl rb f1 1 > preview.rb
../measures/measures.pl sp f1 1 > preview.sp
../measures/measures.pl tt f1 1 > preview.tt

# available measures are
# rp = regression path
# fp = first pass
# fpnr = first pass no regressions
# ff = first fix
# ro = regressions out
# rb = right_bounded
# sp = second pass
# sp_reg = second pass regressions only
# lc = load contrib
# tt = total time


###############################################
   Output Format (in *.fp files etc)
###############################################

Subject Item Condition Region1 Region2 Region3 ...

Subject: subject number (counting from 1, not the original subject ID
assigned by the experimenter)

Condition: experimental condition (always 1 in this experiment)

Item: item number (as in the original file)

RegionN: total time in ms (or whatever measure is requested) for
region N (see below for tokenization of the text).

Questions (both preview questions and questions at the end) are
removed, as well as practice items.

Note that we need to do separate analyses for the preview and
nopreview conditions, as the item IDs differ, because of the extra
question in the preview condition. (It would have been clever to match
the item ID, and use the condition ID to indicate preview/no preview,
but too late.)


###############################################
   Tokenization
###############################################

We use standard eye-tracking tokenization, where a whitespace (either
" " or newline) corresponds to a token boundary. This means that
contractions and clitics (n't, 's, 're etc.) are NOT separate
tokens. Also, punctuation is not a separate token (too small to
measure on).

The files nopreview.script and preview.script contain the tokenized
version of the texts; a token boundary is indicated by "/". Note that
a space " " is counted as part of a token. By convention, it belongs
to the token following it.

The files nopreview.cnt and preview.cnt contain the character counts
that are used by measures.pl. The first number on each line is the
item ID, then the condition ID, then the number of regions + 1. Then
there is a 0 (start of the first region). This is followed by the
position of the last character for each region. Note that the count is
incremented by 160 for each newline. So 3 is the third character on line
1, 175 is the 15th character on line 2, etc.
