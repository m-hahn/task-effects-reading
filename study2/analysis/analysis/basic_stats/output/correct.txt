# A tibble: 2 x 2
  isPreview Correct
  <lgl>       <dbl>
1 FALSE       0.74 
2 TRUE        0.912
Generalized linear mixed model fit by maximum likelihood (Laplace Approximation) ['glmerMod']
 Family: binomial  ( logit )
Formula: Correct ~ isPreview + (1 + isPreview | TextNo) + (1 | Participant)
   Data: humanScores

     AIC      BIC   logLik deviance df.resid 
   358.3    382.8   -173.1    346.3      434 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.9375  0.1126  0.2466  0.4359  1.2421 

Random effects:
 Groups      Name          Variance Std.Dev. Corr
 Participant (Intercept)   0.6899   0.8306       
 TextNo      (Intercept)   0.5554   0.7452       
             isPreviewTRUE 1.2062   1.0983   0.18
Number of obs: 440, groups:  Participant, 22; TextNo, 20

Fixed effects:
              Estimate Std. Error z value Pr(>|z|)    
(Intercept)     1.2838     0.3718   3.453 0.000554 ***
isPreviewTRUE   2.0531     0.6868   2.989 0.002797 ** 
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Correlation of Fixed Effects:
            (Intr)
isPrevwTRUE -0.353
