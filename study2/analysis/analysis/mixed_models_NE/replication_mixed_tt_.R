# For first fixation duration and first pass time, no trials in which the region is skipped on first-
# pass reading (i.e., when first fixation duration is zero) were included in the analysis. For total time,
# only trials with a non-zero total time were included in the analysis.


source("../prettyPrint.R")

library(tidyr)
library(dplyr)

mapping_surprisals = read.csv("../../experiment_data/processed_data//mappingSurprisals.tsv", sep="\t", quote="@")
mapping_surprisals$MappingLineNum = (1:nrow(mapping_surprisals))-1
joint = mapping_surprisals
human = read.csv("../../experiment_data/processed_data//mappingWithHuman.tsv", sep="\t")

humanWordFreq = read.csv("../../experiment_data/processed_data/mappingNonAnonymizedWordFreq.tsv", sep="\t", quote="^")

human = merge(human, humanWordFreq, by=c("TextFileName", "HumanPosition", "AnonymizedPosition"), all.x=TRUE)
print(mean(as.character(human$OriginalToken.x) == as.character(human$OriginalToken.y), na.rm=TRUE))
human$OriginalToken = human$OriginalToken.x
human$OriginalToken.x = NULL
human$OriginalToken.y = NULL


human$LogWordFreq = log(human$NonAnonymizedWordFreq+1)


# EXCLUDED PARTICIPANTS
human = human[!(human$Participant %in% c("N_3", "N_9")),]



joint = merge(human, joint, by=c("TextFileName", "HumanPosition", "AnonymizedPosition")) #, "OriginalToken"))

mean(as.character(joint$OriginalToken.x) == as.character(joint$OriginalToken.y), na.rm=TRUE)
joint$OriginalToken = joint$OriginalToken.y
joint$OriginalToken.x = NULL
joint$OriginalToken.y = NULL


library(lme4)

external = read.csv("../../experiment_data/processed_data/mappingWithExternal.tsv", sep="\t") %>% select(TextFileName, JointPosition, AnonymizedToken, ExperimentTokenLength, WordFreq, IsNamedEntity, IsCorrectAnswer) %>% mutate(WordFreq=NULL) # this is the wrong WordFreq --> it is the anonymized one!
joint = merge(joint, external, by=c("TextFileName", "JointPosition", "AnonymizedToken"))



joint$tokenID = as.factor(paste0(joint$TextFileName, joint$JointPosition))

#joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal = NA
joint$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = joint, na.action=na.exclude))
joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal.Resid = 0
joint$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = joint, na.action=na.exclude))

library(brms)
joint = joint %>% filter(!is.na(fp))
joint$FIXATED = (joint$fp > 0)



data = joint[joint$IsNamedEntity,]
dataN = data[data$tt > 0,]

dataN = dataN %>% mutate(IsNamedEntity.Centered = (IsNamedEntity - mean(IsNamedEntity)))
dataN = dataN %>% mutate(LogWordFreq.Centered = (LogWordFreq - mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
dataN = dataN %>% mutate(HumanPosition.Centered = (HumanPosition - mean(HumanPosition))/sd(HumanPosition))
dataN = dataN %>% mutate(Condition.dummy.Centered = ifelse(Condition == "Preview", -0.5, 0.5))

dataN$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = dataN, na.action=na.exclude))
dataN[dataN$Surprisal_OOV_Status == "OOV",]$Surprisal.Resid = 0

dataN$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = dataN, na.action=na.exclude))
dataN$IsCorrectAnswer.Resid = resid(lm(IsCorrectAnswer ~ IsNamedEntity, data = dataN, na.action=na.exclude))

fullFormula <- readLines("../select_interactions_NE/selection/selected_tt.txt")[1]
#fullFormula = 'HumanPosition.Centered + Condition.dummy.Centered + LogWordFreq.Centered + IsNamedEntity.Centered + IsCorrectAnswer.Centered + Surprisal.Resid + ExperimentTokenLength.Resid + (1|tokenID) + (1|Participant) + Condition.dummy.Centered*IsCorrectAnswer.Centered + Condition.dummy.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*LogWordFreq.Centered + Surprisal.Resid*ExperimentTokenLength.Resid + IsNamedEntity.Centered*Surprisal.Resid + HumanPosition.Centered*IsCorrectAnswer.Centered + HumanPosition.Centered*LogWordFreq.Centered + Condition.dummy.Centered*IsNamedEntity.Centered + IsCorrectAnswer.Centered*ExperimentTokenLength.Resid + HumanPosition.Centered*IsNamedEntity.Centered + LogWordFreq.Centered*IsNamedEntity.Centered + LogWordFreq.Centered*ExperimentTokenLength.Resid + HumanPosition.Centered*ExperimentTokenLength.Resid + ExperimentTokenLength.Resid*ExperimentTokenLength.Resid'

library('texreg')
write.table(data, file="CSVs/from_new.tsv", sep="\t")


# without clipping
modelTT = lme4::lmer(formula(paste("tt", " ~ ",fullFormula, sep="")) , data=dataN)#, REML=FALSE)
sink("models/tt-lmer-vanilla.txt")
print(renamePredictorsInStringOutput(texreg(modelTT ,single.row = TRUE)))
sink()


# with clipping
limit = mean(dataN$ff, na.rm=TRUE) + 4*sd(dataN$tt, na.rm=TRUE)
dataN_tt = dataN[dataN$tt < limit,]
modelTT = lme4::lmer(formula(paste("tt", " ~ ",fullFormula, sep="")) , data=dataN_tt)#, REML=FALSE)
sink("models/tt-lmer-clipping.txt")
print(renamePredictorsInStringOutput(texreg(modelTT ,single.row = TRUE)))
sink()

# log-transformed
dataN$logTT = log(dataN$tt)
modelLogTT = lme4::lmer(formula(paste("logTT", " ~ ",fullFormula, sep="")) , data=dataN, REML=FALSE)
sink("models/tt-lmer-log.txt")
print(renamePredictorsInStringOutput(texreg(modelLogTT ,single.row = TRUE)))
sink()



