
models = {x : open(f"models/{x}-lmer-log.txt", "r").read().strip().split("\n") for x in ["ff", "fp", "tt"]}
#print(models)
models = {x : models[x][5:-16] for x in models}
#print(models["ff"])
predictors = set()
for x in models:
    assert models[x][0] == '\\hline'
    assert models[x][-1] == '\\hline'
    models[x]=models[x][1:-1]
    models[x] = [[z.strip() for z in y.replace("\\", "").split("&")] for y in models[x]]
    models[x] = {y[0] : y[1:] for y in models[x]}
    for y in models[x]:
        predictors.add(y)
    print(models[x])
predictors_main = sorted([x for x in predictors if ":" not in x])
predictors_other = sorted([x for x in predictors if ":" in x and "Condition" not in x])
predictors_cond = sorted([x for x in predictors if ":" in x and "Condition" in x])


def flatten(y):
    r = []
    for x in y:
        for z in x:
            r.append(z)
    return r

def sameLengthForEachEntry(l):
    for i in range(len(l)):
        if len(l[i]) < 8:
            l[i] += " "*(8-len(l[i]))
    return l


def padEffect(x):
    return x+" "*(30-len(x))


print(predictors)
with open(f"output/{__file__}.tex", "w") as outFile:
  for group in [predictors_main, predictors_other, predictors_cond]:
    for effect in group:
        print("&".join(flatten([[padEffect(effect.replace(".Resid", ""))]] + [sameLengthForEachEntry(models[x].get(effect, ["", "", ""])) for x in ["ff", "fp", "tt"]])) + " \\\\", file=outFile)
    print("\\hline", file=outFile)


