
\begin{table}$
\begin{center}$
\begin{tabular}${l c }$
\hline
 & Model 1 \\
\hline
(Intercept)                                    & 5.25 & (0.02) &  $^{***}$  \\
PositionText                         & 0.00 & (0.01) &          \\
Condition                       & 0.07 & (0.04) &          \\
LogWordFreq                           & -0.03 & (0.01) &  $^{***}$ \\
IsCorrectAnswer.Resid                          & -0.01 & (0.02) &         \\
Surprisal.Resid                                & 0.00 & (0.00) &          \\
WordLength.Resid                    & -0.00 & (0.00) &         \\
PositionText:LogWordFreq    & 0.01 & (0.01) &          \\
Condition:IsCorrectAnswer.Resid & -0.05 & (0.03) &  $^{*}$   \\
\hline
AIC                                            & 5574.54                 \\
BIC                                            & 5656.76                 \\
Log Likelihood                                 & -2775.27                \\
Num. obs.                                      & 6987                    \\
Num. groups: tokenID                           & 623                     \\
Num. groups: Participant                       & 22                      \\
Var: tokenID (Intercept)                       & 0.01                    \\
Var: Participant (Intercept)                   & 0.01                    \\
Var: Residual                                  & 0.12                    \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001,  &  $^{**}$p<0.01,  &  $^*p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
