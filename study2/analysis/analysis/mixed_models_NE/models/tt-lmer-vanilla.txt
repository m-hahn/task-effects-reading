
\begin{table}$
\begin{center}$
\begin{tabular}${l c }$
\hline
 & Model 1 \\
\hline
(Intercept)                                          & 344.19 & (13.86) &  $^{***}$  \\
PositionText                               & -35.39 & (4.96) &  $^{***}$   \\
Condition                             & 83.22 & (26.51) &  $^{**}$    \\
LogWordFreq                                 & -57.72 & (4.88) &  $^{***}$   \\
IsCorrectAnswer.Resid                                & 90.50 & (17.54) &  $^{***}$   \\
Surprisal.Resid                                      & 0.54 & (1.28) &             \\
WordLength.Resid                          & 20.32 & (2.16) &  $^{***}$    \\
Condition:IsCorrectAnswer.Resid       & -191.09 & (16.57) &  $^{***}$ \\
Condition:LogWordFreq        & -25.09 & (5.31) &  $^{***}$   \\
PositionText:IsCorrectAnswer.Resid         & -125.87 & (24.03) &  $^{***}$ \\
LogWordFreq:IsCorrectAnswer.Resid           & -81.88 & (18.07) &  $^{***}$  \\
IsCorrectAnswer.Resid:Surprisal.Resid                & -14.09 & (4.25) &  $^{***}$   \\
Condition:WordLength.Resid & 8.78 & (2.48) &  $^{***}$     \\
IsCorrectAnswer.Resid:WordLength.Resid    & 19.76 & (7.04) &  $^{**}$     \\
PositionText:LogWordFreq          & 9.99 & (4.42) &  $^{*}$       \\
LogWordFreq:WordLength.Resid     & -4.92 & (2.22) &  $^{*}$      \\
PositionText:Condition      & -11.59 & (5.36) &  $^{*}$     \\
\hline
AIC                                                  & 132244.42                  \\
BIC                                                  & 132387.50                  \\
Log Likelihood                                       & -66102.21                  \\
Num. obs.                                            & 9451                       \\
Num. groups: tokenID                                 & 627                        \\
Num. groups: Participant                             & 22                         \\
Var: tokenID (Intercept)                             & 8772.40                    \\
Var: Participant (Intercept)                         & 3671.07                    \\
Var: Residual                                        & 65037.70                   \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001,  &  $^{**}$p<0.01,  &  $^*p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
