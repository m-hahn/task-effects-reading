
\begin{table}$
\begin{center}$
\begin{tabular}${l c }$
\hline
 & Model 1 \\
\hline
(Intercept)                                          & 238.89 & (7.72) &  $^{***}$ \\
PositionText                               & -3.69 & (2.01) &          \\
Condition                             & 46.78 & (15.22) &  $^{**}$  \\
LogWordFreq                                 & -7.10 & (0.70) &  $^{***}$  \\
IsCorrectAnswer.Resid                                & -6.48 & (6.88) &          \\
Surprisal.Resid                                      & 0.62 & (0.53) &           \\
WordLength.Resid                          & 5.87 & (0.89) &  $^{***}$   \\
Condition:WordLength.Resid & 0.54 & (1.33) &           \\
Condition:LogWordFreq        & -2.63 & (0.97) &  $^{**}$   \\
LogWordFreq:WordLength.Resid     & -0.24 & (0.31) &          \\
LogWordFreq:IsCorrectAnswer.Resid           & -4.96 & (2.49) &  $^{*}$    \\
PositionText:IsCorrectAnswer.Resid         & -16.49 & (9.79) &         \\
IsCorrectAnswer.Resid:Surprisal.Resid                & -0.14 & (1.76) &          \\
\hline
AIC                                                  & 85546.74                 \\
BIC                                                  & 85656.17                 \\
Log Likelihood                                       & -42757.37                \\
Num. obs.                                            & 6901                     \\
Num. groups: tokenID                                 & 623                      \\
Num. groups: Participant                             & 22                       \\
Var: tokenID (Intercept)                             & 904.28                   \\
Var: Participant (Intercept)                         & 1214.95                  \\
Var: Residual                                        & 13387.33                 \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001,  &  $^{**}$p<0.01,  &  $^*p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
