Fixed effects:
                                                      Estimate Std. Error
(Intercept)                                           343.8224    13.8862
HumanPosition.Centered                                -35.1501     4.9880
Condition.dummy.Centered                               83.1012    26.5617
LogWordFreq.Centered                                  -57.7525     4.9070
IsCorrectAnswer.Resid                                  90.7374    17.5883
Surprisal.Resid                                         0.4443     1.2952
ExperimentTokenLength.Resid                            20.2221     2.1670
Condition.dummy.Centered:IsCorrectAnswer.Resid       -190.6949    16.5952
Condition.dummy.Centered:LogWordFreq.Centered         -24.9872     5.3248
HumanPosition.Centered:IsCorrectAnswer.Resid         -126.1967    24.0967
LogWordFreq.Centered:IsCorrectAnswer.Resid            -81.8753    18.1220
IsCorrectAnswer.Resid:Surprisal.Resid                 -13.9668     4.2623
Condition.dummy.Centered:ExperimentTokenLength.Resid    8.7258     2.4899
IsCorrectAnswer.Resid:ExperimentTokenLength.Resid      19.8883     7.0546
HumanPosition.Centered:LogWordFreq.Centered             9.9522     4.4442
LogWordFreq.Centered:ExperimentTokenLength.Resid       -4.9100     2.2236
HumanPosition.Centered:Condition.dummy.Centered       -11.3127     5.3888
                                                     t value
(Intercept)                                           24.760
HumanPosition.Centered                                -7.047
Condition.dummy.Centered                               3.129
LogWordFreq.Centered                                 -11.769
IsCorrectAnswer.Resid                                  5.159
Surprisal.Resid                                        0.343
ExperimentTokenLength.Resid                            9.332
Condition.dummy.Centered:IsCorrectAnswer.Resid       -11.491
Condition.dummy.Centered:LogWordFreq.Centered         -4.693
HumanPosition.Centered:IsCorrectAnswer.Resid          -5.237
LogWordFreq.Centered:IsCorrectAnswer.Resid            -4.518
IsCorrectAnswer.Resid:Surprisal.Resid                 -3.277
Condition.dummy.Centered:ExperimentTokenLength.Resid   3.505
IsCorrectAnswer.Resid:ExperimentTokenLength.Resid      2.819
HumanPosition.Centered:LogWordFreq.Centered            2.239
LogWordFreq.Centered:ExperimentTokenLength.Resid      -2.208
HumanPosition.Centered:Condition.dummy.Centered       -2.099

Correlation matrix not shown by default, as p = 17 > 12.
Use print(x, correlation=TRUE)  or
    vcov(x)        if you need it

> summary(lme4::lmer(formula(paste("tt", " ~ ",fullFormula, sep="")) , data=dataN %>% filter(HumanPosition > 1)))

