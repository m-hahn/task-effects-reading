Fixed effects:
                                                     Estimate Std. Error
(Intercept)                                           328.919     12.310
HumanPosition.Centered                                -24.191      3.989
Condition.dummy.Centered                               77.620     23.794
LogWordFreq.Centered                                  -44.184      3.935
IsCorrectAnswer.Resid                                  62.392     13.965
Surprisal.Resid                                         1.566      1.039
ExperimentTokenLength.Resid                            15.374      1.753
Condition.dummy.Centered:IsCorrectAnswer.Resid       -126.082     13.976
Condition.dummy.Centered:LogWordFreq.Centered         -28.939      4.493
HumanPosition.Centered:IsCorrectAnswer.Resid          -72.281     19.281
LogWordFreq.Centered:IsCorrectAnswer.Resid            -16.127     14.499
IsCorrectAnswer.Resid:Surprisal.Resid                  -6.117      3.392
Condition.dummy.Centered:ExperimentTokenLength.Resid    6.241      2.129
IsCorrectAnswer.Resid:ExperimentTokenLength.Resid       8.027      5.765
HumanPosition.Centered:LogWordFreq.Centered             7.216      3.543
LogWordFreq.Centered:ExperimentTokenLength.Resid       -4.238      1.828
HumanPosition.Centered:Condition.dummy.Centered       -10.071      4.528
                                                     t value
(Intercept)                                           26.719
HumanPosition.Centered                                -6.065
Condition.dummy.Centered                               3.262
LogWordFreq.Centered                                 -11.228
IsCorrectAnswer.Resid                                  4.468
Surprisal.Resid                                        1.507
ExperimentTokenLength.Resid                            8.770
Condition.dummy.Centered:IsCorrectAnswer.Resid        -9.022
Condition.dummy.Centered:LogWordFreq.Centered         -6.442
HumanPosition.Centered:IsCorrectAnswer.Resid          -3.749
LogWordFreq.Centered:IsCorrectAnswer.Resid            -1.112
IsCorrectAnswer.Resid:Surprisal.Resid                 -1.803
Condition.dummy.Centered:ExperimentTokenLength.Resid   2.932
IsCorrectAnswer.Resid:ExperimentTokenLength.Resid      1.392
HumanPosition.Centered:LogWordFreq.Centered            2.037
LogWordFreq.Centered:ExperimentTokenLength.Resid      -2.319
HumanPosition.Centered:Condition.dummy.Centered       -2.224

Correlation matrix not shown by default, as p = 17 > 12.
Use print(x, correlation=TRUE)  or
    vcov(x)        if you need it

> summary(lme4::lmer(formula(paste("tt", " ~ ",fullFormula, sep="")) , data=dataN %>% filter(HumanPosition > 5, tt < 359.2367+4*297.6929)))

