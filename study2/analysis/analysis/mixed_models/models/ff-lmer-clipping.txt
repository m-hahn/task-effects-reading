
\begin{table}$
\begin{center}$
\begin{tabular}${l c}$
\hline
 & Model 1 \\
\hline
(Intercept)                                          & 198.79 & (4.35) &  $^{***}$ \\
PositionText                               & 0.55 & (0.41) &           \\
Condition                             & 16.69 & (8.68) &          \\
LogWordFreq                                 & -5.45 & (0.47) &  $^{***}$  \\
IsNamedEntity                               & -3.76 & (1.27) &  $^{**}$   \\
IsCorrectAnswer.Resid                                & -2.65 & (3.40) &          \\
Surprisal.Resid                                      & 1.26 & (0.17) &  $^{***}$   \\
WordLength.Resid                          & 0.32 & (0.22) &           \\
LogWordFreq:WordLength.Resid     & 0.59 & (0.21) &  $^{**}$    \\
Condition:WordLength.Resid & -0.71 & (0.31) &  $^{*}$    \\
LogWordFreq:Surprisal.Resid                 & 0.35 & (0.19) &           \\
Surprisal.Resid:WordLength.Resid          & 0.13 & (0.07) &           \\
Condition:IsCorrectAnswer.Resid       & -13.72 & (5.13) &  $^{**}$  \\
\hline
AIC                                                  & 521480.77              \\
BIC                                                  & 521620.55              \\
Log Likelihood                                       & -260724.38             \\
Num. obs.                                            & 46011                  \\
Num. groups: tokenID                                 & 4946                   \\
Num. groups: Participant                             & 22                     \\
Var: tokenID (Intercept)                             & 263.89                 \\
Var: Participant (Intercept)                         & 408.71                 \\
Var: Residual                                        & 4673.31                \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001;  &  $^{**}$p<0.01;  &  $^{*}$p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
