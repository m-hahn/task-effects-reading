# For first fixation duration and first pass time, no trials in which the region is skipped on first-
# pass reading (i.e., when first fixation duration is zero) were included in the analysis. For total time,
# only trials with a non-zero total time were included in the analysis.


source("../prettyPrint.R")

library(tidyr)
library(dplyr)


data = read.csv("../../experiment_data/processed_data/aggregated_data_csv/data-full.csv")

conditionLevelsDiff = max(data$Condition.dummy.Centered) - min(data$Condition.dummy.Centered)
if(conditionLevelsDiff > 1) {
   data$Condition.dummy.Centered = data$Condition.dummy.Centered / conditionLevelsDiff
}

data.Complete = data[complete.cases(data[,c("HumanPosition","Condition.dummy","IsCorrectAnswer","IsNamedEntity","Surprisal","ExperimentTokenLength","LogWordFreq","tokenID","Participant", "fp", "ff", "tt")]),]

dataN = data.Complete[data.Complete$fp > 0,]

dataN$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = dataN, na.action=na.exclude))
dataN$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = dataN, na.action=na.exclude))


dataN$IsCorrectAnswer.Resid = resid(lm(IsCorrectAnswer ~ IsNamedEntity, data = dataN, na.action=na.exclude))


fullFormula <- readLines("../selection/selected_fp.txt")[1]
#fullFormula = 'HumanPosition.Centered + Condition.dummy.Centered + LogWordFreq.Centered + IsNamedEntity.Centered + IsCorrectAnswer.Centered + Surprisal.Resid + ExperimentTokenLength.Resid + (1|tokenID) + (1|Participant) + Condition.dummy.Centered*IsCorrectAnswer.Centered + Condition.dummy.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*LogWordFreq.Centered + Surprisal.Resid*ExperimentTokenLength.Resid + IsNamedEntity.Centered*Surprisal.Resid + HumanPosition.Centered*IsCorrectAnswer.Centered + HumanPosition.Centered*LogWordFreq.Centered + Condition.dummy.Centered*IsNamedEntity.Centered + IsCorrectAnswer.Centered*ExperimentTokenLength.Resid + HumanPosition.Centered*IsNamedEntity.Centered + LogWordFreq.Centered*IsNamedEntity.Centered + LogWordFreq.Centered*ExperimentTokenLength.Resid + HumanPosition.Centered*ExperimentTokenLength.Resid + ExperimentTokenLength.Resid*ExperimentTokenLength.Resid'

library('texreg')


# without clipping
modelTT = lme4::lmer(formula(paste("fp", " ~ ",fullFormula, sep="")) , data=dataN)#, REML=FALSE)
sink("../models/fp-lmer-vanilla.txt")
print(renamePredictorsInStringOutput(texreg(modelTT ,single.row = TRUE)))
sink()


# with clipping
limit = mean(dataN$ff, na.rm=TRUE) + 4*sd(dataN$fp, na.rm=TRUE)
dataN_fp = dataN[dataN$fp < limit,]
modelTT = lme4::lmer(formula(paste("fp", " ~ ",fullFormula, sep="")) , data=dataN_fp)#, REML=FALSE)
sink("../models/fp-lmer-clipping.txt")
print(renamePredictorsInStringOutput(texreg(modelTT ,single.row = TRUE)))
sink()

# log-transformed
dataN$logTT = log(dataN$fp)
modelLogTT = lme4::lmer(formula(paste("logTT", " ~ ",fullFormula, sep="")) , data=dataN, REML=FALSE)
sink("../models/fp-lmer-log.txt")
print(renamePredictorsInStringOutput(texreg(modelLogTT ,single.row = TRUE)))
sink()



