
for x in ["FIXATED", "ff", "fp", "tt"]:
  try:
    formula = open(f"selection/selected_{x}_RESID.txt", "r").read().strip()
  except FileNotFoundError:
    print("File not found")
    continue
  print(formula)
  formula = [x for x in formula.split(" + ") if not x.startswith("(")]
  print(formula)
  withToken = ['HumanPosition.Centered', 'LogWordFreq.Centered', 'IsNamedEntity.Resid', 'IsCorrectAnswer.Resid', 'Surprisal.Resid', 'ExperimentTokenLength.Resid']
  withParticipant = ['Condition.dummy.Centered']
  slopesTokenID = [x for x in formula if all(y not in x for y in withToken)]
  slopesParticipant = [x for x in formula if all(y not in x for y in withParticipant)]
  print(slopesTokenID)
  print(slopesParticipant)
  
  with open(f"selection/selected_{x}_slopes_RESID.txt", "w") as outFile:
     print(" + ".join(formula) + " + " + ("(1 + " + " + ".join(slopesTokenID)+ "|tokenID)") + " + " + ("(1 + " + " + ".join(slopesParticipant)+ "|Participant)"), file=outFile)
  
  
