source("../prettyPrint.R")

library(tidyr)
library(dplyr)




library(tidyr)
library(dplyr)

mapping_surprisals = read.csv("../../experiment_data/processed_data//mappingSurprisals.tsv", sep="\t", quote="@")
mapping_surprisals$MappingLineNum = (1:nrow(mapping_surprisals))-1
joint = mapping_surprisals
human = read.csv("../../experiment_data/processed_data//mappingWithHuman.tsv", sep="\t")

humanWordFreq = read.csv("../../experiment_data/processed_data/mappingNonAnonymizedWordFreq.tsv", sep="\t", quote="^")

human = merge(human, humanWordFreq, by=c("TextFileName", "HumanPosition", "AnonymizedPosition"), all.x=TRUE)
print(mean(as.character(human$OriginalToken.x) == as.character(human$OriginalToken.y), na.rm=TRUE))
human$OriginalToken = human$OriginalToken.x
human$OriginalToken.x = NULL
human$OriginalToken.y = NULL


human$LogWordFreq = log(human$NonAnonymizedWordFreq+1)


# EXCLUDED PARTICIPANTS
human = human[!(human$Participant %in% c("N_3", "N_9")),]



joint = merge(human, joint, by=c("TextFileName", "HumanPosition", "AnonymizedPosition")) #, "OriginalToken"))

mean(as.character(joint$OriginalToken.x) == as.character(joint$OriginalToken.y), na.rm=TRUE)
joint$OriginalToken = joint$OriginalToken.y
joint$OriginalToken.x = NULL
joint$OriginalToken.y = NULL


library(lme4)

external = read.csv("../../experiment_data/processed_data/mappingWithExternal.tsv", sep="\t") %>% select(TextFileName, JointPosition, AnonymizedToken, ExperimentTokenLength, WordFreq, IsNamedEntity, IsCorrectAnswer) %>% mutate(WordFreq=NULL) # this is the wrong WordFreq --> it is the anonymized one!
joint = merge(joint, external, by=c("TextFileName", "JointPosition", "AnonymizedToken"))



joint$tokenID = as.factor(paste0(joint$TextFileName, joint$JointPosition))

#joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal = NA
joint$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = joint, na.action=na.exclude))
joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal.Resid = 0
joint$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = joint, na.action=na.exclude))

joint = joint %>% filter(!is.na(fp))
joint$FIXATED = (joint$fp > 0)



data = joint[joint$IsNamedEntity,]

dataIncomplete = data[!complete.cases(data[,c("HumanPosition","Condition","IsCorrectAnswer","IsNamedEntity","Surprisal","ExperimentTokenLength","LogWordFreq","tokenID","Participant", "fp")]),]

data = data[complete.cases(data[,c("HumanPosition","Condition","IsCorrectAnswer","IsNamedEntity","Surprisal","ExperimentTokenLength","LogWordFreq","tokenID","Participant", "fp")]),]


# Centering
data = data %>% mutate(IsNamedEntity.Centered = (IsNamedEntity - mean(IsNamedEntity)))
data = data %>% mutate(LogWordFreq.Centered = (LogWordFreq - mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(HumanPosition.Centered = (HumanPosition - mean(HumanPosition))/sd(HumanPosition))
data = data %>% mutate(Condition.dummy.Centered = ifelse(Condition == "Preview", -0.5, 0.5))

# Residualizing surprisal & zeroing out OOV
data$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = data, na.action=na.exclude))
data[data$Surprisal_OOV_Status == "OOV",]$Surprisal.Resid = 0

# Residualizing word length
data$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = data, na.action=na.exclude))

# Residualizing answer status
data$IsCorrectAnswer.Resid = resid(lm(IsCorrectAnswer ~ IsNamedEntity, data = data, na.action=na.exclude))




  # base model
  predictors = c("HumanPosition.Centered", "Condition.dummy.Centered", "LogWordFreq.Centered", "IsCorrectAnswer.Resid", "Surprisal.Resid", "ExperimentTokenLength.Resid")
  
  # for 
  current_components = predictors
  current_components = c(current_components, "(1|tokenID)", "(1|Participant)")
  
  TARGET_VARIABLE = "FIXATED"
  
  
  #
    modelFP = lme4::glmer(formula(paste(TARGET_VARIABLE, " ~ ",paste(current_components, collapse=" + "), sep="")) , data=data, family = binomial("logit") , REML=FALSE)

  bestModelSoFarFP = modelFP
  
  foundNew = TRUE
  while(foundNew) {
    foundNew = FALSE
    bestNewLLFP = 1
    bestNewInteraction = NULL
    
    for(i in (1:length(predictors))) {
       for(j in (i:length(predictors))) {
         new_interaction = paste(predictors[i], predictors[j], sep="*")
         new_formula = paste(paste(current_components, collapse=" + "), new_interaction, sep=" + ")
         if(TARGET_VARIABLE != "FIXATED") {
             modelFPNew = lme4::lmer(formula(paste(TARGET_VARIABLE," ~ ",new_formula, sep="")) , data=data, REML=FALSE)
         } else {
            modelFPNew = lme4::glmer(formula(paste(TARGET_VARIABLE," ~ ",new_formula, sep="")) , data=data, family = binomial("logit"), REML=FALSE)
         }
  
         llFPNew = anova(modelFPNew, bestModelSoFarFP)$"Pr(>Chisq)"[2]
         if(llFPNew < bestNewLLFP) {
              bestNewInteraction = new_interaction
              bestNewLLFP = llFPNew
              bestModelInThisIterFP = modelFPNew
              cat(new_interaction, llFPNew, sep="\n")
              cat(new_formula, sep="\n")
          } else {
              cat(new_interaction, llFPNew, sep="\n")
          }
       } 
    }
         if(bestNewLLFP < 0.05) {
              foundNew = TRUE
              current_components = c(current_components, bestNewInteraction)
  
              bestModelSoFarFP = bestModelInThisIterFP
              cat("SELECTED")
              cat(TARGET_VARIABLE, "\t")
              cat(bestNewInteraction, bestNewLLFP, sep="\n")
              
              #cat(new_formula, sep="\n")
          } else {
              foundNew = FALSE
              #cat(new_interaction, llFP, llFPNew, sep="\n")
          }
  }
  
sink(paste("selection/selected_", TARGET_VARIABLE, ".txt", sep=""))
cat(paste(paste(current_components, collapse=" + "), new_interaction, sep=" + "))
sink()

