
Descriptive statistics:
* `basic_stats/replication_descriptive.R`

Reading times and answer accuracy:
* `basic_stats/replication_answer_scores.R`


Visualization:
* `visualization/replication_viz.R`

Model selection:
* `select_interactions/replication_selectInteractions_ff.R`
* `select_interactions/replication_selectInteractions_FIXATED.R`
* `select_interactions/replication_selectInteractions_fp.R`
* `select_interactions/replication_selectInteractions.R`
* `select_interactions/replication_selectInteractions_tt.R`

Obtain formulas for selected models:
* `select_interactions/addSlopes.py`

Mixed effects models:
* `mixed_models/replication_mixed_brms_ff_clip.R`
* `mixed_models/replication_mixed_brms_ff_gamma.R`
* `mixed_models/replication_mixed_brms_ff_log.R`
* `mixed_models/replication_mixed_brms_ff_vanilla.R`
* `mixed_models/replication_mixed_brms_FIXATED.R`
* `mixed_models/replication_mixed_brms_fp_clip.R`
* `mixed_models/replication_mixed_brms_fp_gamma.R`
* `mixed_models/replication_mixed_brms_fp_log.R`
* `mixed_models/replication_mixed_brms_fp_vanilla.R`
* `mixed_models/replication_mixed_brms_tt_clipped.R`
* `mixed_models/replication_mixed_brms_tt_gamma.R`
* `mixed_models/replication_mixed_brms_tt_log.R`
* `mixed_models/replication_mixed_brms_tt_vanilla.R`
* `mixed_models/replication_mixed_ff.R`
* `mixed_models/replication_mixed_fp.R`
* `mixed_models/replication_mixed.R`
* `mixed_models/replication_mixed_tt.R`

Collect results:

* `print_models/putTogetherModels_clip.py`
* `print_models/putTogetherModels_log.py`
* `print_models/putTogetherModels.py`

