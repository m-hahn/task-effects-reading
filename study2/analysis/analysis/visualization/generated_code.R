library(ggplot2)

library(tidyr)
library(dplyr)
mapping_surprisals = read.csv("../../experiment_data/processed_data//mappingSurprisals.tsv", sep="\t", quote="@")
mapping_surprisals$MappingLineNum = (1:nrow(mapping_surprisals))-1
joint = mapping_surprisals
human = read.csv("../../experiment_data/processed_data//mappingWithHuman.tsv", sep="\t")
humanWordFreq = read.csv("../../experiment_data/processed_data/mappingNonAnonymizedWordFreq.tsv", sep="\t", quote="^")
human = merge(human, humanWordFreq, by=c("TextFileName", "HumanPosition", "AnonymizedPosition"), all.x=TRUE)
print(mean(as.character(human$OriginalToken.x) == as.character(human$OriginalToken.y), na.rm=TRUE))
human$OriginalToken = human$OriginalToken.x
human$OriginalToken.x = NULL
human$OriginalToken.y = NULL
human$LogWordFreq = log(human$NonAnonymizedWordFreq+1)
human = human[!(human$Participant %in% c("N_3", "N_9")),]
joint = merge(human, joint, by=c("TextFileName", "HumanPosition", "AnonymizedPosition")) #, "OriginalToken"))
mean(as.character(joint$OriginalToken.x) == as.character(joint$OriginalToken.y), na.rm=TRUE)
joint$OriginalToken = joint$OriginalToken.y
joint$OriginalToken.x = NULL
joint$OriginalToken.y = NULL
library(lme4)
external = read.csv("../../experiment_data/processed_data/mappingWithExternal.tsv", sep="\t") %>% select(TextFileName, JointPosition, AnonymizedToken, ExperimentTokenLength, WordFreq, IsNamedEntity, IsCorrectAnswer) %>% mutate(WordFreq=NULL) # this is the wrong WordFreq --> it is the anonymized one!
joint = merge(joint, external, by=c("TextFileName", "JointPosition", "AnonymizedToken"))
joint$tokenID = as.factor(paste0(joint$TextFileName, joint$JointPosition))
joint$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = joint, na.action=na.exclude))
joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal.Resid = 0
joint$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = joint, na.action=na.exclude))
joint = joint %>% filter(!is.na(fp))
joint$FIXATED = (joint$fp > 0) + 0.0
data = joint
dataN = data[data$fp > 0,]
dataN = dataN %>% mutate(IsNamedEntity.Centered = (IsNamedEntity - mean(IsNamedEntity)))
dataN = dataN %>% mutate(LogWordFreq.Centered = (LogWordFreq - mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
dataN = dataN %>% mutate(HumanPosition.Centered = (HumanPosition - mean(HumanPosition))/sd(HumanPosition))
dataN = dataN %>% mutate(Condition.dummy.Centered = ifelse(Condition == "Preview", -0.5, 0.5))
dataN$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = dataN, na.action=na.exclude))
dataN$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = dataN, na.action=na.exclude))
dataN$IsCorrectAnswer.Resid = resid(lm(IsCorrectAnswer ~ IsNamedEntity, data = dataN, na.action=na.exclude))
plot = ggplot(data=dataN %>% filter(IsNamedEntity) %>% filter(Surprisal_OOV_Status != 'OOV'), aes(x=Surprisal, y=tt, group=IsCorrectAnswer, color=IsCorrectAnswer)) + geom_smooth() + xlab("Surprisal") + ylab("TotalTime")
ggsave("figures/IsCorrectAnswer-Surprisal-tt.pdf", width=4, height=4)
plot = ggplot(data=dataN %>% filter(IsNamedEntity), aes(x=ExperimentTokenLength, y=tt, group=IsCorrectAnswer, color=IsCorrectAnswer)) + geom_smooth() + xlab("WordLength") + ylab("TotalTime")
ggsave("figures/IsCorrectAnswer-WordLength-tt.pdf", width=4, height=4)
plot = ggplot(data=dataN %>% filter(Surprisal_OOV_Status != 'OOV'), aes(x=Surprisal, y=tt, group=IsNamedEntity, color=IsNamedEntity)) + geom_smooth() + xlab("Surprisal") + ylab("TotalTime")
ggsave("figures/IsNamedEntity-Surprisal-tt.pdf", width=4, height=4)
plot = ggplot(data=dataN, aes(x=ExperimentTokenLength, y=tt, group=IsNamedEntity, color=IsNamedEntity)) + geom_smooth() + xlab("WordLength") + ylab("TotalTime")
ggsave("figures/IsNamedEntity-WordLength-tt.pdf", width=4, height=4)
plot = ggplot(data=dataN %>% filter(IsNamedEntity), aes(x=LogWordFreq, y=tt, group=IsCorrectAnswer, color=IsCorrectAnswer)) + geom_smooth() + xlab("LogWordFreq") + ylab("TotalTime")
ggsave("figures/LogWordFreq-IsCorrectAnswer-tt.pdf", width=4, height=4)
plot = ggplot(data=dataN, aes(x=LogWordFreq, y=tt, group=IsNamedEntity, color=IsNamedEntity)) + geom_smooth() + xlab("LogWordFreq") + ylab("TotalTime")
ggsave("figures/LogWordFreq-IsNamedEntity-tt.pdf", width=4, height=4)
plot = ggplot(data=dataN %>% filter(IsNamedEntity), aes(x=HumanPosition, y=tt, group=IsCorrectAnswer, color=IsCorrectAnswer)) + geom_smooth() + xlab("PositionText") + ylab("TotalTime")
ggsave("figures/PositionText-IsCorrectAnswer-tt.pdf", width=4, height=4)
plot = ggplot(data=dataN, aes(x=HumanPosition, y=tt, group=IsNamedEntity, color=IsNamedEntity)) + geom_smooth() + xlab("PositionText") + ylab("TotalTime")
ggsave("figures/PositionText-IsNamedEntity-tt.pdf", width=4, height=4)
plot = ggplot(data=dataN %>% filter(IsNamedEntity) %>% group_by(IsCorrectAnswer, Condition) %>% summarise(ff = mean(ff)), aes(x=IsCorrectAnswer, y=ff, group=Condition, color=Condition)) + geom_line() + xlab("IsCorrectAnswer") + ylab("FirstFixation")
ggsave("figures/Condition-IsCorrectAnswer-ff.pdf", width=4, height=4)
plot = ggplot(data=dataN %>% filter(IsNamedEntity) %>% group_by(IsCorrectAnswer, Condition) %>% summarise(tt = mean(tt)), aes(x=IsCorrectAnswer, y=tt, group=Condition, color=Condition)) + geom_line() + xlab("IsCorrectAnswer") + ylab("TotalTime")
ggsave("figures/Condition-IsCorrectAnswer-tt.pdf", width=4, height=4)
plot = ggplot(data=data %>% filter(IsNamedEntity) %>% group_by(IsCorrectAnswer, Condition) %>% summarise(FIXATED = mean(FIXATED)), aes(x=IsCorrectAnswer, y=FIXATED, group=Condition, color=Condition)) + geom_line() + xlab("IsCorrectAnswer") + ylab("Fixation Rate")
ggsave("figures/Condition-IsCorrectAnswer-FIXATED.pdf", width=4, height=4)
plot = ggplot(data=data %>% group_by(IsNamedEntity, Condition) %>% summarise(FIXATED = mean(FIXATED)), aes(x=IsNamedEntity, y=FIXATED, group=Condition, color=Condition)) + geom_line() + xlab("IsNamedEntity") + ylab("Fixation Rate")
ggsave("figures/Condition-IsNamedEntity-FIXATED.pdf", width=4, height=4)
plot = ggplot(data=dataN, aes(x=LogWordFreq, y=fp, group=Condition, color=Condition)) + geom_smooth() + xlab("LogWordFreq") + ylab("FirstPass")
ggsave("figures/Condition-LogWordFreq-fp.pdf", width=4, height=4)
plot = ggplot(data=dataN, aes(x=LogWordFreq, y=tt, group=Condition, color=Condition)) + geom_smooth() + xlab("LogWordFreq") + ylab("TotalTime")
ggsave("figures/Condition-LogWordFreq-tt.pdf", width=4, height=4)
plot = ggplot(data=data, aes(x=LogWordFreq, y=FIXATED, group=Condition, color=Condition)) + geom_smooth() + xlab("LogWordFreq") + ylab("Fixation Rate")
ggsave("figures/Condition-LogWordFreq-FIXATED.pdf", width=4, height=4)
plot = ggplot(data=dataN, aes(x=ExperimentTokenLength, y=ff, group=Condition, color=Condition)) + geom_smooth() + xlab("WordLength") + ylab("FirstFixation")
ggsave("figures/Condition-WordLength-ff.pdf", width=4, height=4)
plot = ggplot(data=dataN, aes(x=ExperimentTokenLength, y=fp, group=Condition, color=Condition)) + geom_smooth() + xlab("WordLength") + ylab("FirstPass")
ggsave("figures/Condition-WordLength-fp.pdf", width=4, height=4)
plot = ggplot(data=dataN, aes(x=ExperimentTokenLength, y=tt, group=Condition, color=Condition)) + geom_smooth() + xlab("WordLength") + ylab("TotalTime")
ggsave("figures/Condition-WordLength-tt.pdf", width=4, height=4)
