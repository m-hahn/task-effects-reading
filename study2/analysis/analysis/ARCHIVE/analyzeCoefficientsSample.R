

prepareCoefficientsSample = function() {
  data = getMultipleModelsCoefficientsSample()
# data = data[!is.na(data$LuaID),]
#data = data[data$LuaID < 50000,]

write.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-coefficients-1400.csv", x=data)

return(data) 
}

# July 23, 2017
analyzeCoefficientsSample = function() {

dataCoef = read.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-coefficients.csv")
data = dataCoef
data = data[!data$IsPunctuation,]
# remove words that were OOV
data = data[!is.na(data$LuaID),]
data = data[data$LuaID < 49999,]

logWFMean = mean(data$LogWordFreq[!data$IsNamedEntity], na.rm=TRUE)
data$LogWordFreq[data$IsNamedEntity] <- logWFMean

corAnsMean = mean(data$IsCorrectAnswer[data$IsNamedEntity], na.rm=TRUE)
data$IsCorrectAnswer = data$IsCorrectAnswer+0
data$IsCorrectAnswer[!data$IsNamedEntity] <- corAnsMean



data = centerColumn(data,"LogWordFreq")
data = centerColumn(data,"OccursInQuestion")
data = centerColumn(data,"IsCorrectAnswer")
data = centerColumn(data,"IsNamedEntity")
data = centerColumn(data,"HumanPosition.x")
data = centerColumn(data,"ExperimentTokenLength")



data$position = data$positionGated/data$positionGate

data$linearScore.Preview = data$fromInput + data$positionGated + 0.5 * data$conditionGate + data$gateFromInput * data$dotproductAffine + data$gatedFromHistory + 0.5 * data$position * data$conditionTimesPositionGate
data$linearScore.NoPreview = data$fromInput + data$positionGated - 0.5 * data$conditionGate - 0.5 * data$position * data$conditionTimesPositionGate
data$P.Minus.NP = data$linearScore.Preview - data$linearScore.NoPreview

dataP = cbind(data)
dataNP = cbind(data)
dataP$linearScore = dataP$linearScore.Preview
dataNP$linearScore = dataP$linearScore.NoPreview
dataP$Condition.dummy.Centered = -0.5
dataNP$Condition.dummy.Centered = 0.5
dataDouble = rbind(dataP,dataNP)

dataDouble$ExperimentTokenLength.Centered.Resid = residuals(lm(ExperimentTokenLength.Centered ~ LogWordFreq, data=dataDouble, na.action=na.exclude))
#dataN$Surprisal.Centered.Resid = residuals(lm(Surprisal.Centered ~ LogWordFreq + ExperimentTokenLength.Centered, data=dataN, na.action=na.exclude))


library(lme4)
model = lme4::lmer(linearScore ~ HumanPosition.x.Centered*Condition.dummy.Centered + LogWordFreq.Centered*Condition.dummy.Centered  + IsNamedEntity.Centered*Condition.dummy.Centered + IsCorrectAnswer.Centered*Condition.dummy.Centered + OccursInQuestion.Centered*Condition.dummy.Centered + HumanPosition.x.Centered*LogWordFreq.Centered + HumanPosition.x.Centered*IsNamedEntity.Centered + (1|tokenID) + (1|model), data=dataDouble)

model = lme4::lmer(linearScore ~ HumanPosition.x.Centered*Condition.dummy.Centered + LogWordFreq.Centered*Condition.dummy.Centered  + IsNamedEntity.Centered*Condition.dummy.Centered + IsCorrectAnswer.Centered*Condition.dummy.Centered + OccursInQuestion.Centered*Condition.dummy.Centered + HumanPosition.x.Centered*LogWordFreq.Centered + HumanPosition.x.Centered*IsNamedEntity.Centered + ExperimentTokenLength.Centered.Resid*Condition.dummy.Centered + (1|tokenID) + (1|model), data=dataDouble)


}


# July 2017
studyCoefficients = function() {
  dataCoef = getMultipleModelsCoefficients()
 dataCoef$LogWordFreq = log(dataCoef$WordFreq)
 dataCoef = centerColumn(dataCoef,"LogWordFreq")




}

