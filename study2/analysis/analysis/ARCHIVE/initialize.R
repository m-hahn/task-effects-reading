
# Training data: texts c(2,4,6,8,10,12,14,16,18,20)
# participants c(2,4,6,8,10,12)

initialize = function(modelP, modelNP, partition = "dev") {
  library(lme4)
  dataP = initializeForCondition("preview", modelP, partition)
  dataNP = initializeForCondition("nopreview", modelNP, partition)

  if(RENAME_PARTICIPANT_NAMES) {
    dataP$Participant = (paste("P", dataP$Participant, sep="_"))
    dataNP$Participant = (paste("NP", dataNP$Participant, sep="_"))
  }

  data = rbind(dataP, dataNP)

  data$Participant = as.factor(data$Participant)

  data = center(data)
  data = readHumanScores(data)
  dummy = model.matrix( ~ Condition -1, data=data)
  data$Condition.dummy = dummy[,2] - 0.5
  data = centerColumn(data, "Condition.dummy") 
  
  data = centerColumn(data,"Surprisal")
  
  data$LogWordFreq = log(data$WordFreq)
  
  data = centerColumn(data, "LogWordFreq")

  return(data)
}


