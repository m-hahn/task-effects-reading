







##############################
##############################


evaluateAllModelsAgainstHumanDataCond = function() {
  sink("~/scr/results-against-human-cond.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + Condition.dummy.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model2 = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + Condition.dummy.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=TRUE)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}



evaluateAllModelsAgainstHumanDataInter = function() {
  sink("~/scr/results-against-human-inter.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered*Condition.dummy.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model2 = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + Condition.dummy.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}


evaluateAllModelsAgainstHumanDataLogTT = function() {
  sink("~/scr/results-against-human-log-tt.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[dataN$tt < 1000,]

dataN$tt.log = log(dataN$tt)

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt.log ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

model2 = lme4::lmer(tt.log ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}


evaluateAllModelsAgainstHumanDataTT1400 = function() {
  sink("~/scr/results-against-human-tt-1400.txt")
  models1400 = c(1400, 1401, 1402, 1403, 1410, 1411, 1412, 1420, 1421, 1422, 1430, 1431, 1432, 1450, 1451, 1452, 1460, 1461, 1462, 1463, 1480, 1481, 1482, 1490, 1491, 1492)

  for(j in (1:length(models1400))) {
    cat(j,sep="\n")
    i = models1400[j]
    data = initialize(i,i)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model2 = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }
  cat("Have finished the iteration")

  sink()

}




evaluateAllModelsAgainstHumanDataTT = function() {
  sink("~/scr/results-against-human-tt.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

model2 = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}

evaluateAllModelsAgainstHumanDataTTSlope = function() {
  sink("~/scr/results-against-human-tt-slope.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + (AttentionScore.Centered|Participant) + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model2 = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}



evaluateAllModelsAgainstHumanData = function() {
  sink("~/scr/results-against-human.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(fp ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

model2 = lme4::lmer(fp ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}



