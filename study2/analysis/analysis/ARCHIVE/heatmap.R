
# July 2017
buildHeatmapForAverages = function() {
  data = initialize(1,1)
  data2 = getMultipleModelsBothConditions()
  data2$ModelAttention <-  data2$AttentionScore
  dataM = aggregate(data2["ModelAttention"],by=c(data2["tokenID"], data2["Condition"]), mean, na.rm=TRUE)
  dataNew = merge(data,dataM, by=c("tokenID", "Condition"))

  dataNew = dataNew[with(dataNew, order(tokenID)),]

# (JULY 2017) TODO figure out why there are two different LuaID's. Does one come from CNN and one from DailyMail?
data2$LuaID = data2$LuaID.x

  dataM = aggregate(data2["AttentionScore"], by=c(data2["WordFreq"],data2["tokenID"], data2["Condition"], data2["ExperimentToken.x"], data2["TextNo"], data2["JointPosition"], data2["LuaID"]), mean, na.rm=TRUE)
  dataM = dataM[with(dataM, order(tokenID)),]


dataP = dataM[dataM$Condition == "preview",]
dataNP = dataM[dataM$Condition == "nopreview",]

printHeatmap_Fresh(dataP, 0.0, 1.0,"preview", 200, TRUE, "AttentionScore", 2) 
#printHeatmap = function(data, low, high, only_condition, end, exclude, attention, textno) {



}

# July 2017
printHeatmap_Fresh = function(data, low, high, only_condition, end, exclude, attention, textno) {
        data = data[data$TextNo == textno,]
        attention = data[,attention]
	itemno = data$tokenID
		words = data$ExperimentToken.x
		wordfreq = data$LuaID
                condition = data$Condition
		initializeColors()
                end = min(end,length(itemno))
		cat("\\definecolor{none}{rgb}{0.99,0.99,0.99}","\n",sep="")
		for(i in (1:end)) {
#			if(itemno[i] > 0 && itemno[i] != 3) {
#				break
#			}
			if(words[i] == -1) {
				next
			}
                        if(condition[i] != only_condition) {
                                next
                        }
                        if(words[i] == "") {
                          next
                        }
			if((exclude && (wordfreq[i] > 9999)) ||  is.na(attention[i]  )){
#    cat(" ",as.character(words[i]),sep="")
				cat(" \\colorbox{none}{",as.character(words[i]),"}",sep="")
			} else {
				colorID = min(18, max(3, floor((attention[i] - low) * 20 / (high - low))))
					cat(" \\colorbox{color", colorID,     "}{",as.character(words[i]),"}",sep="")
			}
			if(i %% 10 == 0) {
				cat("\n")
			}

		}

}


