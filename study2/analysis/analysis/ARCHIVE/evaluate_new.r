DO_LOGISTIC = FALSE #TRUE
DO_LINEAR = TRUE


RENAME_PARTICIPANT_NAMES = TRUE

models = list("full"="Full", "s" = "No Surprisal", "rs" = "No Context", "ir" = "Only Surprisal")


cat("(1) supposedly participant 17 (NPR) stopped after half the trials? but which participant does this map on? which participants in the fp/tt files do the numbers in the subject notes map on. is this participant 9 from the NPR file (who has many zeros)", sep="\n")
cat("(2) for participant NP_9, file 17 is missing. why did this happen?", sep="\n")
cat("(3) In any case, the mapping from the original 24 numbers to the two groups of 12 numbers appears to be correct.", sep="\n")
cat("(4) Please exclude the nonnative participants in R", sep="\n")
cat('(5) data = read.csv("~/scr/NEURAL_ATTENTION_TASK/ed1516/data-dev.csv")', sep="\n")
cat('(5) dataN = data[data$fp>0,]', sep="\n")
cat('data$LogWordFreq = log(data$WordFreq)')
cat('data$NLogWordFreq = -log(data$WordFreq)')
cat("There is a problem in ~/scr/NEURAL_ATTENTION_TASK/ed1516/data-full.csv: For P_2 and NP_2, one text (length about 168) appears twice. Must have to do with the way the texts are read in, first text is read twice or something like this.")
cat("Who is the nonnative speaker again? NP_03 or NP_05? It looks as if 3 was excluded but 5 is nonnative according to notes?")


createUniqueItemIDs = function(data, columnName) {
   newName = (paste(columnName,"Renumbered",sep="."))
   data[,newName] = as.integer(factor(data[,columnName]))
   return (data)
}


# Diagnostics
#cor(data$price, data$exprice, use="complete")



##########################
##########################
# aggregate some eyetracking measure
aggregateTTPerQuestion = function(data) {
 agg = aggregate(data[,24],by=list(data$Participant,data$TextNo,data$Correct.x,data$Condition), mean, na.rm=TRUE)
}

##########################
##########################




# /mhahn/repo2/mhahn_files/ed1516/experiments/corpus/forEvaluation/questions/annotation-textsonly/att-pg-test-neat-qa-500-512-0.7-100-R-0.5again-tq-hard/E-2.csv
# /mhahn/repo2/mhahn_files/ed1516/experiments/corpus/forEvaluation/mappingWithHuman/nopreview/Participant-1/E-2.csv
# 














# July 2017
buildMixEfFitForModelFixations = function() {
#  data = initialize(1,1)
  data = getMultipleModelsBothConditions()
#  data2$ModelAttention <-  data2$AttentionScore
#  dataM = aggregate(data2["ModelAttention"],by=c(data2["tokenID"], data2["Condition"]), mean, na.rm=TRUE)
#  dataNew = merge(data,dataM, by=c("tokenID", "Condition"))
 data$LogWordFreq = log(data$WordFreq)
 data = centerColumn(data,"LogWordFreq")
# model = glm(AttentionDecision ~ Condition.dummy.Centered*LogWordFreq.Centered, family = binomial("logit"),data=data)
  model = glmer(AttentionDecision ~ Condition.dummy.Centered*LogWordFreq.Centered + (Condition.dummy.Centered*LogWordFreq.Centered|model) + (1|model), family = binomial("logit"),data=data)

}


















readExternalEntropy = function() {
   path = "/juicier/scr120/scr/mhahn/qp/run_on_eyetrack-with-info.txt"
#   path = "/juicier/scr120/scr/mhahn/qp/entropy-surprisal-eyetrack.csv"
    dataEnt = (read.csv(path,header=TRUE,sep="\t"))
  dataEnt$JointPosition = dataEnt$JointPosition+2
  return(dataEnt)
}
# data2 = merge(data, dataEnt, by=c("TextNo", "JointPosition"))
#  data2 = data2[!(as.character(data2$Token.TF) == "UnitedHealth"),]
# entropy is a strong predictor in addition to surprisal!
#
# summary(lmer(fp ~ Surprisal.TF + Info.0 + LogWordFreq + Condition + (1|tokenID) + (1|Participant), data=data2))
# `Info' does not seem to work well. But real one-word entropy is a good predictor
sandbox = function() {

 data2 = merge(data, dataEnt, by=c("TextNo", "JointPosition"))
  data2 = data2[!(as.character(data2$Token.TF) == "UnitedHealth"),]

 data2$Info.5 = (data2$Info.5 - mean(data2$Info.5)) / mean(data2$Info.5)
 data2$Info.6 = (data2$Info.6 - mean(data2$Info.6)) / mean(data2$Info.6)
 data2$Info.7 = (data2$Info.7 - mean(data2$Info.7)) / mean(data2$Info.7)
 data2$Info.8 = (data2$Info.8 - mean(data2$Info.8)) / mean(data2$Info.8)

 data2$Info.0.R = resid(lm(data2$Info.0 ~ data2$LogWordFreq + data2$Surprisal.TF, na.action=na.exclude))
 data2$Info.1.R = resid(lm(data2$Info.1 ~ data2$LogWordFreq +  data2$Surprisal.TF + data2$Info.0, na.action=na.exclude))
 data2$Info.2.R = resid(lm(data2$Info.2 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0+ data2$Info.1, na.action=na.exclude))
 data2$Info.3.R = resid(lm(data2$Info.3 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2, na.action=na.exclude))
 data2$Info.4.R = resid(lm(data2$Info.4 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2 + data2$Info.3, na.action=na.exclude))
 data2$Info.5.R = resid(lm(data2$Info.5 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2 + data2$Info.3 + data2$Info.4, na.action=na.exclude))
 data2$Info.6.R = resid(lm(data2$Info.6 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2 + data2$Info.3 + data2$Info.4 + data2$Info.5, na.action=na.exclude))
 data2$Info.7.R = resid(lm(data2$Info.7 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2 + data2$Info.3 + data2$Info.4 + data2$Info.5 + data2$Info.6, na.action=na.exclude))
 data2$Info.8.R = resid(lm(data2$Info.8 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2 + data2$Info.3 + data2$Info.4 + data2$Info.5 + data2$Info.6 + data2$Info.7, na.action=na.exclude))

model = lmer(fp ~ Surprisal.TF + Info.0.R + Info.1.R + Info.2.R + Info.3.R + Info.4.R + Info.5.R + Info.6.R + Info.7.R + Info.8.R + LogWordFreq + Condition + (1|tokenID) + (1|Participant), data=data2)

summary(model)
}

readCoefficients = function(modelFile, text, dataset) {
  if(dataset == "eyetrack") {
    return(read.csv(paste("forEvaluation/questions/annotation-textsonly/",modelFile,"/E-",text,".coef.csv", sep=""),header=TRUE,sep="\t"))
  } else if(dataset == "sample") {
    return(read.csv(paste("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/annotation-textsonly/",modelFile,"/",text,".coef.csv", sep=""),header=TRUE,sep="\t"))
  }
  crash()
}

readModelData = function(modelFile, text, dataset) {
  if(dataset == "eyetrack") {
    return(read.csv(paste("forEvaluation/questions/annotation-textsonly/",modelFile,"/E-",text,".csv", sep=""),header=TRUE,sep="\t"))
  } else if(dataset == "sample") {
    cat(paste("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/annotation-textsonly/",modelFile,"/",text,".csv", sep=""),sep="\n")
    return(read.csv(paste("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/annotation-textsonly/",modelFile,"/",text,".csv", sep=""),header=TRUE,sep="\t"))
  } else {
    crash()
  }
}

readExternalData = function(text, dataset) {
  if(dataset == "eyetrack") {
    return(read.csv(paste("forEvaluation/mappingWithExternal/E-",text,".csv",sep = ""),header=TRUE,sep="\t"))
  } else if(dataset == "sample") {
    cat(paste("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/mappingWithExternal/",text,".csv", sep=""), sep="\n")
    return(read.csv(paste("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/mappingWithExternal/",text,".csv", sep=""),header=TRUE,sep="\t"))
  } else {
    crash()
  }
}


initializeModelOnlyForCondition = function(condition, model, dataset) {
  if(dataset == "sample") {
     modelFile = getModelFilesName(condition,model)
  dataModel = readModelData(modelFile,"merged","sample")
  dataExternal = readExternalData("merged","sample")

  data = merge(dataExternal,dataModel, by=c("AnonymizedPosition","TextID"))
  cat("Warning: HumanPosition, ExperimentToken are reliable only in one of the two tables because of an upstream bug.",sep="\n\n")



  data$IsPunctuation = (data$POS.Universal == ".")
  data$FunctionWord = (data$POS.Universal =="ADP"  | data$POS.Universal =="ADV" | data$POS.Universal =="CONJ" | data$POS.Universal =="DET" | data$POS.Universal =="PRON" | data$POS.Universal =="PRT")
  data = centerColumn(data, "IsPunctuation")
  data = centerColumn(data, "FunctionWord")

  data = centerColumn(data, "PositionInHumanSentence")
  #data = center(data)


data$Condition <- condition

 
  return(data)
   


  } else {
    crash()
  }
}


#  cat("(1) 17 was not recorded for participant 9 in NoPreview -> exclude", sep="\n")
  cat("(2) participants do not distinguish between conditions, has to be fixed", sep="\n")
#  crash()

centerColumn = function(data,columnName) {
  newName = (paste(columnName,"Centered",sep="."))
  data[,newName] <- data[,columnName] - mean(data[,columnName], na.rm = TRUE)
  data[,newName] <- data[,newName] / sd(data[,columnName], na.rm = TRUE)
  return(data)
}

centerColumnDiscrete = function(data,columnName) {
  newName = (paste(columnName,"Centered",sep="."))
  data[,newName] <- data[,columnName] - mean(data[,columnName], na.rm = TRUE)
  span = max(data[,newName]) - min(data[,newName])
  data[,newName] <- data[,newName] / span 
  return(data)
}



##########################################
##########################################
extractNamedEntities = function(data) {
  dataNE = data[data$IsNamedEntity == TRUE,]
  return(dataNE)
}






##########################################
##########################################
getNozeros = function(data)
{

# remove where FDUR is NAN
	data.Nozeros <- data[data$fp > 0,]

#		data.Nozeros <- data.Nozeros[complete.cases(data.Nozeros[,1]), ]
#		data.Nozeros <- data.Nozeros[data.Nozeros$C.FPASSD < 1000,]
#		data.Nozeros <- data.Nozeros[data.Nozeros$C.FPASSD > 80,]


#		data.Nozeros$C.WLEN <- data.Nozeros$C.WLEN - mean(data.Nozeros$C.WLEN, na.rm = TRUE)
#		data.Nozeros$PREVFIX <- data.Nozeros$PREVFIX - mean(data.Nozeros$PREVFIX, na.rm = TRUE)
#		data.Nozeros$OBLP <- data.Nozeros$WDLP - mean(data.Nozeros$WDLP, na.rm = TRUE)
#		data.Nozeros$ID <- data.Nozeros$ID - mean(data.Nozeros$ID, na.rm = TRUE)
#		data.Nozeros$WordFreq <- data.Nozeros$WordFreq - mean(data.Nozeros$WordFreq, na.rm = TRUE)
#		data.Nozeros$FRQLAST <- data.Nozeros$FRQLAST - mean(data.Nozeros$FRQLAST, na.rm = TRUE)
##data.Nozeros$NBIGR <- data.Nozeros$NBIGR - mean(data.Nozeros$NBIGR, na.rm = TRUE)
##data.Nozeros$BACKTR <- data.Nozeros$BACKTR - mean(data.Nozeros$BACKTR, na.rm = TRUE)
#		data.Nozeros$LASTFIX <- data.Nozeros$LASTFIX - mean(data.Nozeros$LASTFIX, na.rm = TRUE)
#		data.Nozeros$Surprisal <- data.Nozeros$Surprisal - mean(data.Nozeros$Surprisal, na.rm = TRUE)
#		data.Nozeros$Attention <- data.Nozeros$Attention - mean(data.Nozeros$Attention, na.rm = TRUE)


		return(data.Nozeros)
}
##########################################
##########################################




































































#################
##########################
##########################


mse = function(a,b) {
   return(sum((a-b)^2) / length(a))
}

##########################
##########################



lastOnesVector = function(vec) {
	return(append(-1, vec[-c(length(vec))]))

}



##########################
##########################


performDiscretizedEvaluation =function(data) {
cat("\nA\n")
  predA = getThresholdPredictor(data, data$NotCentered.Attention)
  doDiscreteEvaluation(predA, data$C.FIXATED)
cat("\nS\n")
  predS = getThresholdPredictor(data, data$NotCentered.Surprisal)
  doDiscreteEvaluation(predS, data$C.FIXATED)


}



splitIntoReaders = function(data) {

	data$C.FIXATED = pmin(data$C.FIXNO, 1)

		da = data[data$Participant == "sa",]
		db = data[data$Participant == "sb",]
		dc = data[data$Participant == "sc",]
		dd = data[data$Participant == "sd",]
		de = data[data$Participant == "se",]
		df = data[data$Participant == "sf",]
		dg = data[data$Participant == "sg",]
		dh = data[data$Participant == "sh",]
		di = data[data$Participant == "si",]
		dj = data[data$Participant == "sj",]

		da = da[order(da$tokenID),]
		db = db[order(db$tokenID),]
		dc = dc[order(dc$tokenID),]
		dd = dd[order(dd$tokenID),]
		de = de[order(de$tokenID),]
		df = df[order(df$tokenID),]
		dg = dg[order(dg$tokenID),]
		dh = dh[order(dh$tokenID),]
		di = di[order(di$tokenID),]
		dj = dj[order(dj$tokenID),]

		datasets = list(da, db, dc, dd, de, df, dg, dh, di, dj)

		for(j in (1:length(datasets))) {
			dataset = datasets[[j]]
				for(i in 1:ncol(dataset)){
					dataset[is.na(dataset[,i]), i] <- mean(dataset[,i], na.rm = TRUE)
				}
			datasets[[j]] <- dataset
		}



	return(datasets)
}



##########################
##########################



# NOTE data will be altered
# Assumes -1s are already taken out
discreteEvaluationForLogisticModel = function(readers, trainer, tester) {

	da = readers[[trainer]]
		db = readers[[tester]]
		return(discreteEvaluationForLogisticModelOnOtherData(db,db))
}

discreteEvaluationForLogisticModelOnOtherData = function(da, db) {

	lm = glm(formula = C.FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST +   WordFreq + Attention, family = binomial("logit"), data=da)


		for(i in 1:ncol(db)){
			db[is.na(db[,i]), i] <- mean(db[,i], na.rm = TRUE)
		}


	resu = predict(lm, type="response", newdata =data.frame(Attention=db$Attention,  C.WLEN=db$C.WLEN, C.FREQBIN=db$C.FREQBIN , C.FRQLAST=db$C.FRQLAST ,    WordFreq=db$WordFreq), na.action=na.pass)


		doDiscreteEvaluation(db$C.FIXATED, getThresholdPredictor(db, resu))


		resu = (sign(resu - 0.5) + 1)/2
		doDiscreteEvaluation(db$C.FIXATED, resu)
		return(lm)
}



##########################
##########################




markLastFix = function(data) {
	attended = data$Attended
		data$PrevAttended <- append(-1, attended[-c(length(attended))])


		condProb = sum(data$Attended * data$PrevAttended) / sum(data$PrevAttended)
		marginalProb = mean(data$Attended)
		cat(condProb,"  ",marginalProb,"\n")

}



##########################
##########################



readStatisticalNumbersFromFiles = function(descriptor, partNum) {
	numberOfDataPoints = 6
		datapoints = list()

		files = system(paste(" ls /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "/annotation/perp-pg-test-",descriptor,sep=""), intern=TRUE)
		print(files)
		for(i in (1:length(files))) {
			data = read.csv(files[[i]], header=FALSE, sep="\t")
				for(j in (1:numberOfDataPoints)) {
					if(i == 1) {
						datapoints = append(datapoints, data[j,][1])
					} else {
						datapoints[[j]] = append(datapoints[[j]], data[j,][1])
					}
				}
		}
	print(datapoints)
		for(j in (1:numberOfDataPoints)) {
			cat("--",j,"\n")
				evalList(datapoints[[j]], 0)
		}
}



##########################
##########################



getGaussianVector = function(length) {
	gauvector = c(rnorm(length-1, mean=0, sd = 0.01), length)
		gauvector = pmax(pmin(gauvector, 1), -1)
		return(gauvector)
}


##########################
##########################



evaluateLanguageModelRuns = function() {


}



##########################
##########################



discreteEvaluationForRandom = function(fixationrate) {


	truePositives = fixationrate * fixationrate
		trueNegatives = (1-fixationrate) * (1-fixationrate)


		correctPredictions = truePositives+trueNegatives


		totalRealPositives = fixationrate
		totalRealNegatives = 1-fixationrate
		totalItems = 1

		accuracy = correctPredictions / totalItems
		fixPrecision = truePositives / fixationrate
		fixRecall = truePositives / fixationrate
		skipPrecision = trueNegatives / (1-fixationrate)
		skipRecall = trueNegatives / (1-fixationrate)

		f1Fix = 2 * (fixPrecision * fixRecall) / (fixPrecision + fixRecall)
		f1Skip = 2 * (skipPrecision * skipRecall) / (skipPrecision + skipRecall)


		cat("Accuracy   ", accuracy,"\n")
		cat("F1 Fix     ", f1Fix, "\n")
		cat("F1 Skip    ", f1Skip, "\n")

}


##########################
##########################




findThreshold = function(data, predictor) {
	return(sort(predictor)[floor((1-mean(data$C.FIXATED)) * length(data$C.FIXATED))])
}


##########################
##########################




predictWithThreshold = function(predictor, threshold) {
	return(sign(sign(predictor - threshold) +1))
}


##########################
##########################



# NOTE assumes C.FIXATED has not been centered
getThresholdPredictor = function(data, predictor) {
	threshold = findThreshold(data, predictor)
		binarizedPredictor = predictWithThreshold(predictor, threshold)
		return(binarizedPredictor)
}



##########################
##########################



compareHumans = function(data) {
	data$C.FIXATED = pmin(data$C.FIXNO, 1)

		da = data[data$Participant == "sa",]
		db = data[data$Participant == "sb",]
		dc = data[data$Participant == "sc",]
		dd = data[data$Participant == "sd",]
		de = data[data$Participant == "se",]
		df = data[data$Participant == "sf",]
		dg = data[data$Participant == "sg",]
		dh = data[data$Participant == "sh",]
		di = data[data$Participant == "si",]
		dj = data[data$Participant == "sj",]

		da = da[order(da$tokenID),]
		db = db[order(db$tokenID),]
		dc = dc[order(dc$tokenID),]
		dd = dd[order(dd$tokenID),]
		de = de[order(de$tokenID),]
		df = df[order(df$tokenID),]
		dg = dg[order(dg$tokenID),]
		dh = dh[order(dh$tokenID),]
		di = di[order(di$tokenID),]
		dj = dj[order(dj$tokenID),]

		ppa = getThresholdPredictor(da, (db$FIXATED + dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*da$FIXATED
		ppb = getThresholdPredictor(db, (da$FIXATED + dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*db$FIXATED
		ppc = getThresholdPredictor(dc, (da$FIXATED + db$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dc$FIXATED
		ppd = getThresholdPredictor(dd, (da$FIXATED + db$FIXATED+dc$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dd$FIXATED
		ppe = getThresholdPredictor(de, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*de$FIXATED
		ppf = getThresholdPredictor(df, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*df$FIXATED
		ppg = getThresholdPredictor(dg, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dg$FIXATED
		pph = getThresholdPredictor(dh, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dh$FIXATED
		ppi = getThresholdPredictor(di, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+dj$FIXATED)) + 0*di$FIXATED
		ppj = getThresholdPredictor(dj, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED)) + 0*dj$FIXATED


		ta = da$FIXATED
		tb = db$FIXATED
		tc = dc$FIXATED
		td = dd$FIXATED
		te = de$FIXATED
		tf = df$FIXATED
		tg = dg$FIXATED
		th = dh$FIXATED
		ti = di$FIXATED
		tj = dj$FIXATED

		ta = ta[!is.na(ppa)]
		tb = tb[!is.na(ppb)]
		tc = tc[!is.na(ppc)]
		td = td[!is.na(ppd)]
		te = te[!is.na(ppe)]
		tf = tf[!is.na(ppf)]
		tg = tg[!is.na(ppg)]
		th = th[!is.na(pph)]
		ti = ti[!is.na(ppi)]
		tj = tj[!is.na(ppj)]



		ppa = ppa[!is.na(ppa)]
		ppb = ppb[!is.na(ppb)]
		ppc = ppc[!is.na(ppc)]
		ppd = ppd[!is.na(ppd)]
		ppe = ppe[!is.na(ppe)]
		ppf = ppf[!is.na(ppf)]
		ppg = ppg[!is.na(ppg)]
		pph = pph[!is.na(pph)]
		ppi = ppi[!is.na(ppi)]
		ppj = ppj[!is.na(ppj)]

		doDiscreteEvaluation(c(ta,tb,tc,td,te,tf,tg,th,ti,tj), c(ppa, ppb, ppc, ppd, ppe, ppf, ppg, pph, ppi, ppj))


		cat(mean(da$FIXATED == db$FIXATED, na.rm=TRUE),"\n")
		cat(mean(db$FIXATED == dc$FIXATED, na.rm=TRUE),"\n")
		cat(mean(dc$FIXATED == dd$FIXATED, na.rm=TRUE),"\n")
		cat(mean(dd$FIXATED == de$FIXATED, na.rm=TRUE),"\n")
		cat(mean(de$FIXATED == df$FIXATED, na.rm=TRUE),"\n")
		cat(mean(df$FIXATED == dg$FIXATED, na.rm=TRUE),"\n")
		cat(mean(dg$FIXATED == dh$FIXATED, na.rm=TRUE),"\n")


		runEvaluationOfHumanOnOther(da$FIXATED, db$FIXATED)
		runEvaluationOfHumanOnOther(db$FIXATED, dc$FIXATED)
		runEvaluationOfHumanOnOther(dc$FIXATED, dd$FIXATED)
		runEvaluationOfHumanOnOther(dd$FIXATED, de$FIXATED)
		runEvaluationOfHumanOnOther(de$FIXATED, df$FIXATED)
		runEvaluationOfHumanOnOther(df$FIXATED, dg$FIXATED)
		runEvaluationOfHumanOnOther(dg$FIXATED, dh$FIXATED)
		runEvaluationOfHumanOnOther(dh$FIXATED, di$FIXATED)
		runEvaluationOfHumanOnOther(di$FIXATED, dj$FIXATED)
		runEvaluationOfHumanOnOther(dj$FIXATED, da$FIXATED)



}



##########################
##########################




runEvaluationOfHumanOnOther = function(target, predictor) {
	target2 = target * sign(predictor+1)
		predictor2 = predictor * sign(target+1)

		target2 = target2[!is.na(target2)]
		predictor2 = predictor2[!is.na(predictor2)]


		doDiscreteEvaluation(target2, predictor2)
}


##########################
##########################

getHumanSplit = function(data) {


	data$C.ITEM <- 10000*data$C.FILE+data$C.WNUM



		data$C.FIXATED = pmin(data$C.FIXNO, 1)


		da = data[data$Participant == "sa",]
		db = data[data$Participant == "sb",]
		dc = data[data$Participant == "sc",]
		dd = data[data$Participant == "sd",]
		de = data[data$Participant == "se",]
		df = data[data$Participant == "sf",]
		dg = data[data$Participant == "sg",]
		dh = data[data$Participant == "sh",]
		di = data[data$Participant == "si",]
		dj = data[data$Participant == "sj",]

                da = da[order(da$tokenID),]
                db = db[order(db$tokenID),]
                dc = dc[order(dc$tokenID),]
                dd = dd[order(dd$tokenID),]
                de = de[order(de$tokenID),]
                df = df[order(df$tokenID),]
                dg = dg[order(dg$tokenID),]
                dh = dh[order(dh$tokenID),]
                di = di[order(di$tokenID),]
                dj = dj[order(dj$tokenID),]


FIXATED_COLUMN = 92 #90

		da[is.na(da[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(da[,FIXATED_COLUMN], na.rm = TRUE)
		db[is.na(db[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(db[,FIXATED_COLUMN], na.rm = TRUE)
		dc[is.na(dc[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dc[,FIXATED_COLUMN], na.rm = TRUE)
		dd[is.na(dd[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dd[,FIXATED_COLUMN], na.rm = TRUE)
		de[is.na(de[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(de[,FIXATED_COLUMN], na.rm = TRUE)
		df[is.na(df[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(df[,FIXATED_COLUMN], na.rm = TRUE)
		dg[is.na(dg[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dg[,FIXATED_COLUMN], na.rm = TRUE)
		dh[is.na(dh[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dh[,FIXATED_COLUMN], na.rm = TRUE)
		di[is.na(di[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(di[,FIXATED_COLUMN], na.rm = TRUE)
		dj[is.na(dj[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dj[,FIXATED_COLUMN], na.rm = TRUE)


		da$Attention = (da$C.FIXATED + db$C.FIXATED + dc$C.FIXATED + dd$C.FIXATED + de$C.FIXATED + df$C.FIXATED + dg$C.FIXATED + dh$C.FIXATED + di$C.FIXATED + dj$C.FIXATED)/10

return(da)

}


getSkippingRatesHumanForOneTest = function(data) {


	data$C.ITEM <- 10000*data$C.FILE+data$C.WNUM



		data$C.FIXATED = pmin(data$C.FIXNO, 1)


		da = data[data$Participant == "sa" & data$Itemno==3,]
		db = data[data$Participant == "sb" & data$Itemno==3,]
		dc = data[data$Participant == "sc" & data$Itemno==3,]
		dd = data[data$Participant == "sd" & data$Itemno==3,]
		de = data[data$Participant == "se" & data$Itemno==3,]
		df = data[data$Participant == "sf" & data$Itemno==3,]
		dg = data[data$Participant == "sg" & data$Itemno==3,]
		dh = data[data$Participant == "sh" & data$Itemno==3,]
		di = data[data$Participant == "si" & data$Itemno==3,]
		dj = data[data$Participant == "sj" & data$Itemno==3,]


FIXATED_COLUMN = 90

		da[is.na(da[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(da[,FIXATED_COLUMN], na.rm = TRUE)
		db[is.na(db[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(db[,FIXATED_COLUMN], na.rm = TRUE)
		dc[is.na(dc[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dc[,FIXATED_COLUMN], na.rm = TRUE)
		dd[is.na(dd[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dd[,FIXATED_COLUMN], na.rm = TRUE)
		de[is.na(de[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(de[,FIXATED_COLUMN], na.rm = TRUE)
		df[is.na(df[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(df[,FIXATED_COLUMN], na.rm = TRUE)
		dg[is.na(dg[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dg[,FIXATED_COLUMN], na.rm = TRUE)
		dh[is.na(dh[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dh[,FIXATED_COLUMN], na.rm = TRUE)
		di[is.na(di[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(di[,FIXATED_COLUMN], na.rm = TRUE)
		dj[is.na(dj[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dj[,FIXATED_COLUMN], na.rm = TRUE)


		da$Attention = (da$C.FIXATED + db$C.FIXATED + dc$C.FIXATED + dd$C.FIXATED + de$C.FIXATED + df$C.FIXATED + dg$C.FIXATED + dh$C.FIXATED + di$C.FIXATED + dj$C.FIXATED)/10

return(da)







}



##########################
##########################

#aggregateOverReaders = function(data)  {
#   dataAggregate = aggregate(data["AttentionScore"],by=data["Token"], mean, na.rm=TRUE)
#}


##########################
##########################



##########################
##########################


##########################
##########################



##########################
##########################



percentageOfSignificant = function(xlist, threshold)
{
	return(sum(xlist > threshold) / length(xlist))
}



##########################
##########################



evalList = function(xlist, threshold)
{
	cat(percentageOfSignificant(xlist, threshold), mean(xlist),sd(xlist),min(xlist), which.min(xlist), " -- ",max(xlist),which.max(xlist),"\n", sep=" ")
}



##########################
##########################



logisticFitWithBaselines = function(data)
{


	lm = glm(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST +  C.PREVFIX +  C.OBJLPOS + WordFreq +  NotCentered.Attention, family = binomial("logit"), data=data)
		resu = predict(lm, type="response", newdata =data.frame(NotCentered.Attention=data$NotCentered.Attention,  C.WLEN=data$C.WLEN, C.FREQBIN=data$C.FREQBIN , C.FRQLAST=data$C.FRQLAST ,  C.PREVFIX=data$C.PREVFIX ,  C.OBJLPOS=data$C.OBJLPOS , WordFreq=data$WordFreq))
		return(resu)

}



##########################
##########################


maxLikelihoodInterpolatorWithBaselines = function(data)
{
	resu = logisticFitWithBaselines(data)
		resu = (sign(resu - 0.5) + 1)/2
		return(resu)
}



##########################
##########################





logisticFit = function(data)
{
	lm = glm(formula = FIXATED ~  NotCentered.Attention, family = binomial("logit"), data=data)
		resu = predict(lm, type="response", newdata =data.frame(NotCentered.Attention=data$NotCentered.Attention))
		return(resu)

}


##########################
##########################



maxLikelihoodInterpolator = function(data)
{
	resu = logisticFit(data)
		resu = (sign(resu - 0.5) + 1)/2
		return(resu)
}



##########################
##########################





##########################
##########################



doEvaluationForAllGroups = function()
{
	if(TRUE) {
		doEvaluationForGroup("ir","5.0", "-combined-real")
			doEvaluationForGroup("rs","5.0", "-combined-real")
			doEvaluationForGroup("s","5.0", "-combined-real")
			doEvaluationForGroup("full","5.0", "-combined-real")

			doEvaluationForGroup("ir","4.5", "-combined-real")
			doEvaluationForGroup("rs","4.5", "-combined-real")
			doEvaluationForGroup("full","4.5", "-combined-real")
			doEvaluationForGroup("s","4.5", "-combined-real")
	} else {

		doEvaluationForGroup("ir","5.0", "encoder-1")
			doEvaluationForGroup("rs","5.0", "encoder-1")
			doEvaluationForGroup("s","5.0", "encoder-1")
			doEvaluationForGroup("full","5.0", "encoder-1")

	}


	if(FALSE) {
		doEvalForPairs()
	}
}


doEvalForPairs = function() {
	compareTwoGroups("ir", "5.0", "rs", "5.0", "-combined-real")
		compareTwoGroups("ir", "5.0", "s", "5.0", "-combined-real")
		compareTwoGroups("ir", "5.0", "full", "5.0", "-combined-real")

#compareTwoGroups("rs", "5.0", "ir", "5.0", "-combined-real")
		compareTwoGroups("rs", "5.0", "s", "5.0", "-combined-real")
		compareTwoGroups("rs", "5.0", "full", "5.0", "-combined-real")

#compareTwoGroups("s", "5.0", "ir", "5.0", "-combined-real")
#compareTwoGroups("s", "5.0", "rs", "5.0", "-combined-real")
		compareTwoGroups("s", "5.0", "full", "5.0", "-combined-real")

#compareTwoGroups("full", "5.0", "ir", "5.0", "-combined-real")
#compareTwoGroups("full", "5.0", "rs", "5.0", "-combined-real")
#compareTwoGroups("full", "5.0", "s", "5.0", "-combined-real")



}



##########################
##########################



getProcessedData = function(CSV_FILE) {
  return(processDataForEvaluation(getData(CSV_FILE), 1))
}


#assumes that processDataForEvaluation has been run on them
compareTwoModelsLogistic = function(data1, data2)
{


	data = merge(data1, data2, by=c("C.ITEM", "Participant"))

		data$resid.Attention.NoSurp.x.Against = residuals(lm(Attention.x ~ C.WLEN.x + C.FREQBIN.x +   WordFreq.x + Attention.y, data=data, na.action=na.exclude))
		data$resid.Attention.NoSurp.y.Against = residuals(lm(Attention.y ~ C.WLEN.y + C.FREQBIN.y +   WordFreq.y + Attention.x, data=data, na.action=na.exclude))

		data$resid.Attention.x.Against = residuals(lm(Attention.x ~ C.WLEN.x + C.FREQBIN.x +   WordFreq.x + Surprisal.x + Attention.y, data=data, na.action=na.exclude))
		data$resid.Attention.y.Against = residuals(lm(Attention.y ~ C.WLEN.y + C.FREQBIN.y +   WordFreq.y + Surprisal.y + Attention.x, data=data, na.action=na.exclude))


#log.Full.x = try(glmer(formula = FIXATED.x ~ C.WLEN.x+ C.FREQBIN.x + C.FRQLAST.x + C.PREVFIX.x +  C.OBJLPOS.x + WordFreq.x  + Surprisal.x + Attention.y + resid.Attention.x.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))
		log.Att.x  = try(glmer(formula = FIXATED.x ~ C.WLEN.x+ C.FREQBIN.x + C.FRQLAST.x + C.PREVFIX.x +  C.OBJLPOS.x + WordFreq.x  +               Attention.y + resid.Attention.NoSurp.x.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))

#log.Full.y = try(glmer(formula = FIXATED.y ~ C.WLEN.y+ C.FREQBIN.y + C.FRQLAST.y + C.PREVFIX.y +  C.OBJLPOS.y + WordFreq.y  + Surprisal.y + Attention.x + resid.Attention.y.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))
		log.Att.y  = try(glmer(formula = FIXATED.y ~ C.WLEN.y+ C.FREQBIN.y + C.FRQLAST.y + C.PREVFIX.y +  C.OBJLPOS.y + WordFreq.y  +               Attention.x + resid.Attention.NoSurp.y.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))



		return(list("att-x" = log.Att.x, "att-y" = log.Att.y))


}
##########################
##########################


compareTwoModelsLinear = function(data1, data2)
{
	data.Nozeros = merge(getNozeros(data1), getNozeros(data2), by=c("C.ITEM", "Participant"))

		data.Nozeros$residSurprisal.x.Against = residuals(lm(Surprisal.x ~ C.WLEN.x + WordFreq.x +  FRQLAST.x +    WordFreq.x + Surprisal.y, data=data.Nozeros, na.action=na.exclude))
		data.Nozeros$residSurprisal.y.Against = residuals(lm(Surprisal.y ~ C.WLEN.y + WordFreq.y +  FRQLAST.y +    WordFreq.y + Surprisal.x, data=data.Nozeros, na.action=na.exclude))

		linear.Surp.x = lmer(C.FPASSD.x ~ C.WLEN.x+ C.FREQBIN.x + C.FRQLAST.x + C.TRACEIC.x + C.BACKTR.x + C.NBIGR.x + C.PREVFIX.x + C.LDIST.x + C.OBJLPOS.x + WordFreq.x + Surprisal.y + residSurprisal.x.Against + (residSurprisal.x.Against|C.ITEM) + (1|Participant) + (1|C.ITEM), data=data.Nozeros)
		linear.Surp.y = lmer(C.FPASSD.y ~ C.WLEN.y+ C.FREQBIN.y + C.FRQLAST.y + C.TRACEIC.y + C.BACKTR.y + C.NBIGR.y + C.PREVFIX.y + C.LDIST.y + C.OBJLPOS.y + WordFreq.y + Surprisal.x + residSurprisal.y.Against + (residSurprisal.y.Against|C.ITEM) + (1|Participant) + (1|C.ITEM), data=data.Nozeros)

		return(list("x" = linear.Surp.x, "y" = linear.Surp.y))
}

##########################
##########################



compareTwoGroups = function(abl1, weight1, abl2, weight2, encoderName, partNum)
{
	x_over_y = {}
	y_over_x = {}

	library(lme4)
		for(num1 in (1:10)) {
			name1 = getNameFromAblAndWeight(abl1, weight1,num1, encoderName)
				CSV_FILE1 = paste("att-surp-uni-dundee-",name1,"_",name1,".csv",sep="")
				cat(paste("X: /disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE1,sep=""),"\n")
				if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE1,sep=""))) {
					data1 = processDataForEvaluation(getData(CSV_FILE1), "1")
						if(is.null(data1)) {
							next
						}

					if(runif(1,0,1) > 0.5){
						next
					}

					for(num2 in (1:10)) {
						name2 = getNameFromAblAndWeight(abl2, weight2,num2, encoderName)
							CSV_FILE2 = paste("att-surp-uni-dundee-",name2,"_",name2,".csv",sep="")
							cat("\n","----",paste("X: /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE1,sep=""),"\n")
							cat(paste("Y: /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE2,sep=""),"\n")
							if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE2,sep=""))) {
								data2 = processDataForEvaluation(getData(CSV_FILE2), "2")
									if(is.null(data2)) {
										next
									}
								if(runif(1,0,1) > 0.5) {
									next
								}

								compareLog = compareTwoModelsLogistic(data1, data2)
									compareLin = compareTwoModelsLinear(data1, data2)


# cat(capture.output(summary(compareLog$"full-x")), sep="\n")
									cat(capture.output(summary(compareLog$"att-x")), sep="\n")
# cat(capture.output(summary(compareLog$"full-y")), sep="\n")
									cat(capture.output(summary(compareLog$"att-y")), sep="\n")

									cat(capture.output(summary(compareLin$"x")), sep="\n")
									cat(capture.output(summary(compareLin$"y")), sep="\n")

									x_over_y = append(x_over_y, coef(summary(compareLog$"att-x"))["resid.Attention.NoSurp.x.Against","z value"])
									y_over_x = append(y_over_x, coef(summary(compareLog$"att-y"))["resid.Attention.NoSurp.y.Against","z value"])



							}
					}
				}
		}
	cat("X OVER Y for ","X",abl1,weight1,"Y",abl2,weight2,"\n",sep=" ")
		cat(x_over_y,"\n")
		cat(evalList(x_over_y, 2),"\n")

		cat("Y OVER X for ","Y",abl2,weight2,"X",abl1,weight1,"\n",sep=" ")
		cat(y_over_x,"\n")
		cat(evalList(y_over_x, 2),"\n")


}

##########################
##########################

getNameFromAblAndWeight = function(abl, weight, num, encoderName, soft)
{
	if(as.numeric(weight) == floor(as.numeric(weight))) {
		weightR = floor(as.numeric(weight))
	} else {
		weightR = weight
	}
	if (! soft){
		return(paste("pg-test-combined-50-1000-0.7-100-R-",weightR,"a0-", weight, "-entities-entropy5.0-1.0",encoderName,"-",abl,"-re", num, sep=""))
	} else {
		crash()
	}
}

##########################
##########################



doEvaluationForGroup = function(abl, weight, encoderName, partNum)
{
	library(lme4)
		cat("\n\n",abl,weight,"---------","\n",sep="\n")
		l.tval.fix.full =      {}
	l.tval.fix.surp =      {}
	l.tval.fix.att =       {}

	l.cor.att.surp =      {}
	l.cor.att.wordfreq =  {}
	l.cor.att.wordlength = {}

	l.tval.fp.full =      {}
	l.tval.fp.surp =     {}
	l.tval.fp.att =        {}

	l.meansurp =          {}

	l.loglikelihood =    {}

	l.ratio =           {}

	disc.att.acc = {}
	disc.att.f1f = {}
	disc.att.f1s = {}

	disc.surp.acc = {} 
	disc.surp.f1f = {} 
	disc.surp.f1s = {} 


	for(num in (1:10)) {
		name = getNameFromAblAndWeight(abl, weight, num, encoderName)
			CSV_FILE = paste("att-surp-uni-dundee-",name,"_",name,".csv",sep="")
			cat(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE,sep=""),"\n")
			if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE,sep=""))) {
				results = evaluate.Treebank(CSV_FILE)
					if(is.null(results)) {
						next
					}
				if(DO_LOGISTIC) {
					tval.fix.full =      coef(summary(results$"log.Full"))["C.residAttention","z value"]
						tval.fix.surp =      coef(summary(results$"log.Surp"))["C.residSurprisal","z value"]
						tval.fix.att =       coef(summary(results$"log.Att"))["C.residAttentionNoSurp","z value"]

						l.tval.fix.full  =   append(l.tval.fix.full    , tval.fix.full   )
						l.tval.fix.surp  =   append(l.tval.fix.surp    ,  tval.fix.surp  )
						l.tval.fix.att  =   append(l.tval.fix.att    , tval.fix.att   )


				}

				cor.att.surp =       results$"Att-Surp"
					cor.att.wordfreq =   results$"Att-log(WordFreq)"
					cor.att.wordlength = results$"Att-WordLength"

					if(DO_LINEAR) {
						tval.fp.full =       coef(summary(results$"FP.Full"))["residAttentionWithSurp","t value"]
							tval.fp.surp =       coef(summary(results$"FP.Surprisal"))["residSurprisal","t value"]
							tval.fp.att =        coef(summary(results$"FP.Att"))["residAttentionNoSurp","t value"]


							l.tval.fp.full  =   append(l.tval.fp.full    ,  tval.fp.full  )
							l.tval.fp.surp  =   append(l.tval.fp.surp    ,  tval.fp.surp  )
							l.tval.fp.att  =   append(l.tval.fp.att    ,  tval.fp.att  )


					}

				meansurp =           results$"meanSurp"

					loglikelihood =      results$"approxPerp"$"model"

					ratio =              results$"approxPerp"$"ratio"


					l.cor.att.surp  =   append(l.cor.att.surp    ,  cor.att.surp  )
					l.cor.att.wordfreq  =   append(l.cor.att.wordfreq    ,  cor.att.wordfreq  )
					l.cor.att.wordlength  =   append(l.cor.att.wordlength    ,  cor.att.wordlength  )
					l.meansurp  =   append(l.meansurp    ,  meansurp  )
					l.loglikelihood  =   append(l.loglikelihood    ,  loglikelihood  )
					l.ratio  =   append(l.ratio    ,  ratio  )


					disc.att = results$"Disc.Att"
					disc.surp = results$"Disc.Surp"
					disc.att.acc = append(disc.att.acc, disc.att$"Acc")
					disc.att.f1f = append(disc.att.f1f, disc.att$"F1 Fix")
					disc.att.f1s = append(disc.att.f1s, disc.att$"F1 Skip")

					disc.surp.acc = append(disc.surp.acc, disc.surp$"Acc")
					disc.surp.f1f = append(disc.surp.f1f, disc.surp$"F1 Fix")
					disc.surp.f1s = append(disc.surp.f1s, disc.surp$"F1 Skip")




#   return(list("Acc" = accuracy, "F1 Fix" = f1Fix, "F1 Skip" = f1Skip))



			}
	}
	cat("EVAL FOR ",abl,"  ",weight," ",encoderName,"\n")

		ModelName = getModelName(abl)

		buildRow(paste(ModelName," (Prob)"), list(100*disc.att.acc, 100*disc.att.f1f, 100*disc.att.f1s))


		buildRow(paste(ModelName," (Surp)"), list(100*disc.surp.acc, 100*disc.surp.f1f, 100*disc.surp.f1s))



		printEvalFeature(disc.att.acc, "disc.att.acc", 0.52)
		printEvalFeature(disc.att.f1f, "disc.att.f1f", 0.6)
		printEvalFeature(disc.att.f1s, "disc.att.f1s", 0.4)

		printEvalFeature(disc.surp.acc, "disc.surp.acc", 0.52)
		printEvalFeature(disc.surp.f1f, "disc.surp.f1f", 0.6)
		printEvalFeature(disc.surp.f1s, "disc.surp.f1s", 0.4)


		cat("         l.tval.fix.full ","\n")
		cat(         l.tval.fix.full ,"\n")
		cat(   evalList(l.tval.fix.full, 1.96 ))
		cat("\n")
		cat( "    l.tval.fix.surp ","\n")
		cat(     l.tval.fix.surp ,"\n")
		cat(  evalList(l.tval.fix.surp , 1.96))
		cat("\n")
		cat(  "       l.tval.fix.att ","\n")
		cat(         l.tval.fix.att ,"\n")
		cat(evalList(l.tval.fix.att ,1.96))
		cat("\n")
		cat(   "    l.cor.att.surp","\n")
		cat(       l.cor.att.surp,"\n")
		cat(  evalList(       l.cor.att.surp,0))
		cat("\n")
		cat(    " l.cor.att.wordfreq ","\n")
		cat(     l.cor.att.wordfreq ,"\n")
		cat(  evalList(   l.cor.att.wordfreq ,0))
		cat("\n")
		cat(     "    l.cor.att.wordlength ","\n")
		cat(         l.cor.att.wordlength ,"\n")
		cat(   evalList(      l.cor.att.wordlength ,0))
		cat("\n")
		cat(      "   l.tval.fp.full" ,"\n")
		cat(         l.tval.fp.full ,"\n")      
		cat(evalList(         l.tval.fp.full,2.0 ))
		cat("\n")
		cat("     l.tval.fp.surp  ","\n")
		cat(     l.tval.fp.surp  ,"\n")    
		cat(evalList(     l.tval.fp.surp ,0 ))
		cat("\n")
		cat( "        l.tval.fp.att    ","\n")
		cat(         l.tval.fp.att   ,"\n" )   
		cat(evalList(         l.tval.fp.att   ,0 ))
		cat("\n")
		cat(  "       l.meansurp      "  ,"\n")
		cat(         l.meansurp       ,"\n" )  
		cat(evalList(         l.meansurp      ,0  ))
		cat("\n")
		cat(   "      l.loglikelihood" ,"\n")
		cat(         l.loglikelihood ,"\n")
		cat( evalList(        l.loglikelihood,0 ))
		cat("\n")
		cat(    "     l.ratio       "  ,"\n" )
		cat(         l.ratio         ,"\n" )  
		cat(  evalList(       l.ratio          ,3))
		cat("\n")
}


##########################
##########################



printEvalFeature = function(feature, name, threshold) {


	cat("\t\t",name,"\t\t\n")
		cat(       feature ,"\n")
		cat(   evalList(feature, threshold ))
		cat("\n")
}


##########################
##########################



getModelName = function(ABL) {
	return(models[[ABL]])
}


##########################
##########################




buildRow = function(RowName, data) {
#buildRow(paste(ModelName," (Prob)"), c(disc.att.acc, disc.att.f1f, disc.att.f1s))
	cat(RowName,"  ")
		for(i in (1:length(data))) {
			datapoint = data[[i]]
				cat("&   ",round(mean(datapoint),2)," (",round(sd(datapoint),2),")  ",sep="")
		}
	cat("\\\\\n")
}



##########################
##########################



bootstrapping = function(CSV_FILE) {
	library(boot)
		data = processDataForEvaluation(getData(CSV_FILE), CSV_FILE)
		if(! is.data.frame(data)) {
			return(NULL)
		}
	results <- boot(data = data, statistic = getStatisticsForData, R = 10)
		return(results)

}



##########################
##########################



getStatisticsForData = function(data, indices){

	d <- data[indices,]
		return(computeStatisticsOnData(d,TRUE))
}


#models <- list("data" = data,               "log.Full" = log.Full,               "log.Surp" = log.Surp,               "log.Att" = log.Att,"Att-Surp" =cor(data$Surprisal, data$Attention), "Att-log(WordFreq)" = cor(data$Attention, data$WordFreq),"Att-WordLength" = cor(data$Attention,data$C.WLEN), "FP.Surprisal" = linear.Surp, "FP.Att" = linear.Att= "FP.Full" = linear.Full, "meanSurp" = mean(data.Nozeros$NotCentered.Surprisal), "approxPerp" = approxPerp)
#return list("model" = perpModel, "random" = perpRandom, "ratio" = (perpRandom - perpModel))


##########################
##########################



##########################
##########################




evaluateAll.Treebank = function(partNum)
{
	library(lme4)
#library(ggplot2)
		files = list.files(path="/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/")
		for(q in (1:length(files))) {
#sink()
#         cat(files[q], sep="\n")
			try(evaluate.Treebank(files[q]))
		}

}



##########################
##########################



#result = merge(data.Treebank, data.Corpus, by=0, all=TRUE)

buildRowNames.T = function(dataT)
{
	rowNames = row.names(dataT)
		for(q in (1:length(dataT$Surprisal))) {
			print(q)
				rowNames[q] <- paste(dataT$C.SUBJ[q], dataT$C.ITEM[q])
		}
	row.names(dataT) <- rowNames
		return(dataT)
}


##########################
##########################



buildRowNames.C = function(dataC)
{
	rowNames = row.names(dataC)
		for(q in (1:length(dataC$Surprisal))) {
			print(q)
				rowNames[q] <- paste(dataC$SUBJ[q], dataC$ITEM[q])
		}
	row.names(dataC) <- rowNames
		return(dataC)
}



##########################
##########################



randomlySampleData.Treebank = function(data, prob) {
	column = data$Surprisal
		for(q in (1:length(column))) {
			dice = runif(1,0.0,1.0)
				column[q] = dice - prob
		}

	data2 = data[column < 0,]


		m1 = try(glm(formula = FIXATED ~ C.WLEN+ C.PREVFIX + C.OBJLPOS + WordFreq+ C.FRQLAST +  Surprisal + Attention, family = binomial("logit"), data=data2))

		m2 = try(glm(formula = FIXATED ~  C.WLEN+ C.PREVFIX + C.OBJLPOS  + WordFreq + C.FRQLAST +   Surprisal, family = binomial("logit"), data=data2))

		m3 = try(glm(formula = FIXATED ~  C.WLEN+ C.PREVFIX + C.OBJLPOS +  WordFreq + C.FRQLAST,  family = binomial("logit"), data=data2))

		m4 = try(glm(formula = FIXATED ~  C.WLEN+ C.PREVFIX + C.OBJLPOS +  WordFreq + C.FRQLAST + Attention,  family = binomial("logit"), data=data2))



		cat("--- nFixations, RESID, LOGISTIC ---",sep="\n")
		try(cat(capture.output(m1), "\n", sep="\n"))

		cat("--- nFixations, RESID, LOGISTIC BASELINE ---",sep="\n")
		try(cat(capture.output(m2), "\n", sep="\n"))

		cat("--- nFixations, RESID, LOGISTIC BASELINE NO SURP ---",sep="\n")
		try(cat(capture.output(m3), "\n", sep="\n"))

		cat("--- nFixations, RESID, NO SURP ---",sep="\n")
		try(cat(capture.output(m4), "\n", sep="\n"))


		cat("--- LIKELIHOOD RATIO ---",sep="\n")
		try(cat(capture.output(lrtest(m1,m2)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m1,m3)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m2,m3)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m1,m4)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m2,m4)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m3,m4)), "\n", sep="\n"))

}



##########################
##########################




test.Treebank = function()
{
	library(lme4)
#library(ggplot2)
		evaluate.Treebank('att-surp-uni-dundee-pg-test-autoencoding-20-700-0.7-100-R-4.6a0-emb_pg-test-langmod-20-200-0.7-100-R-4.6e1.csv')
}

#data <- read.csv("/disk/scratch2/s1582047/dundee/PART1Statistics/statisticsFinal//att-surp-uni-dundee-pg-test-autoencoding-20-700-0.7-100-R-4.6a0-emb_pg-test-langmod-20-200-0.7-100-R-4.6e1.csv", header = TRUE, sep = "\t")


##########################
##########################




evaluate.Treebank = function(CSV_FILE)
{
	data = processDataForEvaluation(getData(CSV_FILE), CSV_FILE)
		return(computeStatisticsOnData(data, FALSE))
}



##########################
##########################


#########
##########################


