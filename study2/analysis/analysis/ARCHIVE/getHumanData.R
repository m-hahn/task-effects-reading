###############################################
###############################################
getHumanData = function(data) {
   data = data[!is.na(data$HumanPosition),]
   # http://stackoverflow.com/questions/26997586/r-shifting-a-vector
   comesBeforeBreak <-c(rep(TRUE,times=1), data$IsFirstInLine[1:(length(data$IsFirstInLine)-1)]) 
   comesBeforeBreak2 <-c(rep(TRUE,times=2), data$IsFirstInLine[1:(length(data$IsFirstInLine)-2)]) 
   comesAfterBreak <-c(data$IsFirstInLine[2:(length(data$IsFirstInLine))],   rep(TRUE,times=1)) 
   data$IsCloseToLinebreak = data$IsFirstInLine | comesBeforeBreak #| comesBeforeBreak2 | comesAfterBreak
   data = data[!data$IsCloseToLinebreak,]
   return(data)
}

