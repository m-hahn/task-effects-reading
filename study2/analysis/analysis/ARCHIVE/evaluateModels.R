

evaluationOfModels = function(data) {
sink("~/scr/coefficients-model-output.txt")
data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")

 model = lmer(fromInput ~ LogWordFreq.Centered + IsNamedEntity.Centered + FunctionWord.Centered + (1|model) + (1|LuaID.x), data=data)
cat( capture.output(summary(model)),sep="\n")

  model = lmer(positionGate ~ FunctionWord.Centered + ExperimentTokenLength.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
 cat(capture.output(summary(model)),sep="\n")

 model = lmer(conditionGate ~ IsNamedEntity.Centered + FunctionWord.Centered  + (1|model) + (1|LuaID.x), data=data)# [maybe better just throw punctuation away]
  cat(capture.output(summary(model)),sep="\n")

  model = lmer(conditionTimesPositionGate ~ (1|model) + (1|LuaID.x), data=data)
cat(  capture.output(summary(model)),sep="\n")
sink()
}

printLineForPred = function(name, model) {
  cat(name, sep=" ")
  for(i in (1:4)) {
   sd = summary(model)$vcov@factors$correlation@sd[i]
   beta = fixef(model)[[i]]
   cat(paste(" & ",round(beta,3),"  (",round(sd,3),", ",round(beta/sd,2),")  ", sep=""), sep = " ")
  }
  cat(" \\\\ \n")
}


# July 2017
evaluationOfNEATCoefficients = function(data) {


data$position = data$positionGated/data$positionGate

data$linearScore.Preview = data$fromInput + data$positionGated + 0.5 * data$conditionGate + data$gateFromInput * data$dotproductAffine + data$gatedFromHistory + 0.5 * data$position * data$conditionTimesPositionGate
data$linearScore.NoPreview = data$fromInput + data$positionGated - 0.5 * data$conditionGate - 0.5 * data$position * data$conditionTimesPositionGate
data$P.Minus.NP = data$linearScore.Preview - data$linearScore.NoPreview

dataP = cbind(data)
dataNP = cbind(data)
dataP$linearScore = dataP$linearScore.Preview
dataNP$linearScore = dataP$linearScore.NoPreview
dataP$Condition.dummy = -1
dataNP$Condition.dummy = 1
dataDouble = rbind(dataP,dataNP)

#################



data$position = data$positionGated/data$positionGate


data$linearScore.Preview = data$fromInput + data$positionGated + 0.5 * data$conditionGate + data$gateFromInput * data$dotproductAffine + data$gatedFromHistory + 0.5 * data$position * data$conditionTimesPositionGate
data$linearScore.NoPreview = data$fromInput + data$positionGated - 0.5 * data$conditionGate - 0.5 * data$position * data$conditionTimesPositionGate
data$P.Minus.NP = data$linearScore.Preview - data$linearScore.NoPreview
#model = lmer(P.Minus.NP ~ LogWordFreq.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)

data$Att.P = 1/(1+exp(-data$linearScore.Preview))
data$Att.NP = 1/(1+exp(-data$linearScore.NoPreview))
#data$P.Minus.NP = data$Att.P - data$Att.NP
#model = lmer(P.Minus.NP ~ LogWordFreq.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)


dataAgg = aggregate(c(data["fromInput"],data["positionGate"],data["conditionGate"],data["gateFromInput"], data["questHistoryOutGate"], data["conditionTimesPositionGate"]), by=c(data["LogWordFreq.Centered"], data["IsNamedEntity.Centered"], data["FunctionWord.Centered"], data["LuaID.x"], data["model"]), mean)

model = lmer(fromInput ~ LogWordFreq.Centered + FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "Intercept"
printLineForPred(name, model)

model = lmer(positionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "Position"
printLineForPred(name, model)

#model = lmer(conditionGate ~ LogWordFreq.Centered+ (LogWordFreq.Centered|model) + IsNamedEntity.Centered + (1|model) , data=dataAgg)

# does not really make sense. multiple observations do not mean anything when there is no variation, and for single observations per group a random effect is meaningless. So the analyses here make no sense.
# If at all, need slopes for model and no random effect for LuaID.x

model = lmer(conditionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "Condition.Preview"
printLineForPred(name, model)

model = lmer(gateFromInput ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "UnigramOverlap"
printLineForPred(name, model)

model = lmer(questHistoryOutGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "History"
printLineForPred(name, model)

model = lmer(conditionTimesPositionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "CondPrev*Position"
printLineForPred(name, model)










model = lmer(fromInput ~ LogWordFreq.Centered + FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "Intercept"
printLineForPred(name, model)

model = lmer(positionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "Position"
printLineForPred(name, model)

#model = lmer(conditionGate ~ LogWordFreq.Centered+ (LogWordFreq.Centered|model) + IsNamedEntity.Centered + (1|model) , data=data)

# does not really make sense. multiple observations do not mean anything when there is no variation, and for single observations per group a random effect is meaningless. So the analyses here make no sense.
# If at all, need slopes for model and no random effect for LuaID.x

model = lmer(conditionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "Condition.Preview"
printLineForPred(name, model)

model = lmer(gateFromInput ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "UnigramOverlap"
printLineForPred(name, model)

model = lmer(questHistoryOutGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "History"
printLineForPred(name, model)

model = lmer(conditionTimesPositionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "CondPrev*Position"
printLineForPred(name, model)

data$questionInducedValue = 0.5 * data$conditionTimesPositionGate * data$position + 0.5 * data$conditionGate + data$gatedFromHistory + data$gateFromInput * data$dotproductAffine
#data$questionInducedValue =  data$gateFromInput * data$dotproductAffine

model = lmer(questionInducedValue ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|tokenID), data=data)
#                       Estimate Std. Error t value
#(Intercept)             0.14328    0.03385   4.233
#LogWordFreq.Centered    0.09453    0.02014   4.695
#FunctionWord.Centered   0.10871    0.02055   5.291
#IsNamedEntity.Centered  0.07169    0.02196   3.264


# Note that LuaID.x as a random effect is a bad idea, since things vary by tokenID in this quantity
#model = lmer(questionInducedValue ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "ActualValue"
printLineForPred(name, model)

model = lmer(questionInducedValue ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) , data=data)



#dotproductAffine: UnigramOverlap
#attRelativeToQ : the actual vector similarity (not used)
# what's the difference?
#  fromInput = attention.scalarCoefficient(embeddingIntermediateDimension+2,fromInput,"fromInput") -- attention score from the input
#  gateFromInput = attention.scalarCoefficient(embeddingIntermediateDimension,xemb,"gateFromInput") -- gating score from the input for UnigramOverlap


#  questHistOutGate = nn.StoreInContainerLayer("questHistoryOutGate", true)(questHistOutGate) # the history gate
#  -- used for this decision
#  gatedFromHistory = nn.CMulTable()({questHistOutGate, questionHistory})
#  gatedFromHistory = nn.StoreInContainerLayer("gatedFromHistory", true)(gatedFromHistory) # the gated history

#positionGated = nn.StoreInContainerLayer("positionGated", true)(positionGated)



#Coefficient & Predictor & Estimate & SE & t \\ \hline
#Intercept & Intercept &-0.36 & 0.05 & -7.4 \\



# model = lmer(conditionGate ~ LuaID.x.Centered + (LuaID.x.Centered|model) + (1|model) + (1|LuaID.x), data=data) #: no effect
#  model = lmer(conditionGate ~ IsNamedEntity.Centered + (IsNamedEntity.Centered|model) + (1|model) + (1|LuaID.x), data=data) #: no effect
 #model = lmer(conditionGate ~ IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data) #: effect stronger in NoPreview
# model = lmer(conditionGate ~ WordFreq + (1|model) + (1|LuaID.x), data=data) #: unexpected effect
 #model = lmer(conditionGate ~ LuaID.x.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data) #: NE negative, LUAID positive
#  model = lmer( gateFromInput ~ (1|model) + (1|LuaID.x), data=data) #: positive
# model = lmer(conditionGate ~ FunctionWord.Centered + (1|model) + (1|LuaID.x), data=data) #didn't converge
#  model = lmer(positionGate ~ FunctionWord.Centered + (FunctionWord.Centered|model) + (1|model) + (1|LuaID.x.Centered), data=data)
# model = lmer(positionGate ~ FunctionWord.Centered + (1|model) + (1|LuaID.x), data=data) is positive
# bigger correlation with surprisal in NoPreview should be provable
#   model = lme4::lmer(AttentionScore.Centered ~ Surprisal.Centered * Condition.dummy.Centered + (1|tokenID), data=data)
}



