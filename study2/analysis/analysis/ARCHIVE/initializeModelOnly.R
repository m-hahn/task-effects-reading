initializeModelOnly = function(modelP, modelNP) {
  library(lme4)
  dataP = initializeModelOnlyForCondition("preview",modelP,"sample")
  dataNP = initializeModelOnlyForCondition("nopreview",modelNP,"sample")
  data = rbind(dataP, dataNP)

  data$IsNamedEntity.Centered = data$IsNamedEntity - mean(data$IsNamedEntity,na.rm=TRUE)
  data$OccursInQuestion.Centered = data$OccursInQuestion - mean(data$OccursInQuestion,na.rm=TRUE)
  data$IsCorrectAnswer.Centered = data$IsCorrectAnswer - mean(data$IsCorrectAnswer,na.rm=TRUE)
  		data$OccurrencesOfCorrectAnswerSoFar.Centered <- data$OccurrencesOfCorrectAnswerSoFar - mean(data$OccurrencesOfCorrectAnswerSoFar, na.rm = TRUE)
  
  data$HumanPosition.x = NULL
  data$HumanPosition.y = NULL


  data$ExperimentTokenLength.Centered <- data$ExperimentTokenLength - mean(data$ExperimentTokenLength, na.rm = TRUE)
  data$AttentionScore.Centered   <- data$AttentionScore  - mean(data$AttentionScore  , na.rm = TRUE)
  data$AttentionDecision.Centered   <- data$AttentionDecision  - mean(data$AttentionDecision  , na.rm = TRUE)
  data = centerColumn(data,"AnonymizedPosition")
  data$LuaID.Centered   <- (data$LuaID  - mean(data$LuaID  , na.rm = TRUE)) / sd(data$LuaID, na.rm=TRUE)
#		data$   <- data$  - mean(data$  , na.rm = TRUE)
#		data$   <- data$  - mean(data$  , na.rm = TRUE)

  dummy = model.matrix( ~ Condition -1, data=data)
  data$Condition.dummy = dummy[,2] - 0.5
  data = centerColumn(data, "Condition.dummy") 

  data$TextID.renumbered = as.integer(data$TextID)
  data$tokenID = 10000 * data$TextID.renumbered + data$AnonymizedPosition

  return(data)

}


