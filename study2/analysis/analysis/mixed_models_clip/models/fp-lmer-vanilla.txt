
\begin{table}$
\begin{center}$
\begin{tabular}${l c }$
\hline
 & Model 1 \\
\hline
(Intercept)                                          & 222.84 & (6.13) &  $^{***}$ \\
PositionText                               & -0.35 & (0.60) &          \\
Condition                             & 40.01 & (12.23) &  $^{**}$  \\
LogWordFreq                                 & -17.57 & (0.66) &  $^{***}$ \\
IsNamedEntity                               & -7.58 & (2.27) &  $^{***}$  \\
IsCorrectAnswer.Resid                                & -15.14 & (7.75) &         \\
Surprisal.Resid                                      & 1.79 & (0.24) &  $^{***}$   \\
WordLength.Resid                          & 6.47 & (0.31) &  $^{***}$   \\
Condition:LogWordFreq        & -6.70 & (0.94) &  $^{***}$  \\
LogWordFreq:IsNamedEntity          & -9.70 & (1.83) &  $^{***}$  \\
Condition:WordLength.Resid & 2.09 & (0.45) &  $^{***}$   \\
IsNamedEntity:Surprisal.Resid               & -1.01 & (0.55) &          \\
Surprisal.Resid:WordLength.Resid          & 0.25 & (0.10) &  $^{*}$     \\
PositionText:Surprisal.Resid               & -0.44 & (0.22) &          \\
LogWordFreq:IsCorrectAnswer.Resid           & -12.15 & (5.93) &  $^{*}$   \\
\hline
AIC                                                  & 554059.15                \\
BIC                                                  & 554216.36                \\
Log Likelihood                                       & -277011.57               \\
Num. obs.                                            & 45881                    \\
Num. groups: tokenID                                 & 4944                     \\
Num. groups: Participant                             & 22                       \\
Var: tokenID (Intercept)                             & 635.20                   \\
Var: Participant (Intercept)                         & 809.70                   \\
Var: Residual                                        & 9768.51                  \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001,  &  $^{**}$p<0.01,  &  $^*p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
