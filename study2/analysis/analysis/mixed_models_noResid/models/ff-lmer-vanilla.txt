
\begin{table}$
\begin{center}$
\begin{tabular}${l c}$
\hline
 & Model 1 \\
\hline
(Intercept)                                         & 202.64 & (4.72) &  $^{***}$ \\
PositionText                              & 0.69 & (0.45) &           \\
Condition                            & 20.01 & (9.40) &  $^{*}$    \\
LogWordFreq                                & -4.54 & (0.71) &  $^{***}$  \\
IsNamedEntity                              & -5.69 & (1.72) &  $^{***}$  \\
IsCorrectAnswer                            & -3.03 & (3.83) &          \\
Surprisal                                  & 2.95 & (0.45) &  $^{***}$   \\
WordLength                      & 0.76 & (0.60) &           \\
LogWordFreq:WordLength & 2.05 & (0.50) &  $^{***}$   \\
LogWordFreq:IsNamedEntity         & -6.53 & (1.50) &  $^{***}$  \\
IsNamedEntity:Surprisal           & -2.23 & (0.99) &  $^{*}$    \\
Condition:IsCorrectAnswer   & -12.98 & (5.84) &  $^{*}$   \\
\hline
AIC                                                 & 536937.44              \\
BIC                                                 & 537068.58              \\
Log Likelihood                                      & -268453.72             \\
Num. obs.                                           & 46291                  \\
Num. groups: tokenID                                & 4945                   \\
Num. groups: Participant                            & 22                     \\
Var: tokenID (Intercept)                            & 311.74                 \\
Var: Participant (Intercept)                        & 478.73                 \\
Var: Residual                                       & 6117.70                \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001;  &  $^{**}$p<0.01;  &  $^{*}$p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
