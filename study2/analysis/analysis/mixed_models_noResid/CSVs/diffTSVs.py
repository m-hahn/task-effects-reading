with open("from_old.tsv", "r") as inFile:
   data = [x.replace('"', "").split("\t") for x in inFile.read().strip().split("\n")]
   header = ["RowNum"] + data[0]
   assert len(data[1]) == len(header)
   print(header)
   data_old = [{header[i] : line[i] for  i in range(len(header))} for line in data[1:]]
with open("from_new.tsv", "r") as inFile:
   data = [x.replace('"', "").split("\t") for x in inFile.read().strip().split("\n")]
   header = ["RowNum"] + data[0]
   assert len(data[1]) == len(header)
   print(header)
   data_new = [{header[i] : line[i] for  i in range(len(header))} for line in data[1:]]

for condition in ["P", "NP"]:
    for participant in range(1, 13):
     for text in range(25):
        #for line in data_new:
        #    print(line["Participant"])
        new_ = sorted([x for x in data_new if x["Participant"] == f"{condition[0]}_{participant}" and x["TextNo"] == str(text)], key=lambda x:int(x["JointPosition"]))
        old_ = sorted([x for x in data_old if x["Participant"] == f"{condition}_{participant}" and x["TextNo"] == str(text)], key=lambda x:int(x["HumanPosition.x"]))
 #       new_ = ([x for x in data_new if x["Participant"] == f"{condition[0]}_{participant}"])
#        old_ = ([x for x in data_old if x["Participant"] == f"{condition}_{participant}"])
        if len(new_) != len(old_):
            print(condition, participant, text, len(new_), len(old_))
#        print("\n\n@@@@@@@@@@@@")
 #       print(len(new_), len(old_))
        if len(new_) < len(old_):
            for new__, old__ in zip(new_, old_):
                print(new__["JointPosition"], new__["AnonymizedToken"], new__["fp"], "   ", old__["AnonymizedToken.x"], old__["fp"])

