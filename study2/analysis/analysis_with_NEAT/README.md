Heat maps for human and NEAT fixations:

* `heatmaps/analizeNEATPredictions_heatmap_human_NoPreview.R`
* `heatmaps/analizeNEATPredictions_heatmap_human_Preview.R`
* `heatmaps/analizeNEATPredictions_heatmap_NoPreview.R`
* `heatmaps/analizeNEATPredictions_heatmap_Preview.R`

Analyze NEAT fixations:
* `analyzing_neat/analizeNEATPredictions.R`
* `analyzing_neat/analizeNEATPredictions_viz.R`

Predicting reading measures from NEAT:
* `neat_predicts_human/compareNEATAndHuman_fix2_test.R`
* `neat_predicts_human/compareNEATAndHuman_fix3_test.R`
* `neat_predicts_human/compareNEATAndHuman_fix.R`
* `neat_predicts_human/compareNEATAndHuman_Inter_surp.R`
* `neat_predicts_human/prettyPrint.R`
* `neat_predicts_human/prepareCompareTable.py`
