data = read.csv("/u/scr/mhahn/NEURAL_ATTENTION_TASK/OUTPUTS_TEXTS/train_attention_7DailyMail_500_Recurrent_Record_Save_14_Deanon_Flexible_Tune_TrainPreview_Reader_Mask_LengthPreview_New_Cap_Char2_TrainEmb_MarkNE_Softmax_RunOnTexts_Collect.py.tsv", sep="\t", quote="")
params = read.csv("/juice/scr/mhahn/CODE/task-effects-reading/study2/NEAT/train_NEAT/logs/SUMMARY_train_attention_7DailyMail_500_Recurrent_Record_Save_14_Deanon_Flexible_Tune_TrainPreview_Reader_Mask_LengthPreview_New_Cap_Char2_TrainEmb_MarkNE_Softmax_CollectTradeoff.py.tsv", sep="\t")
# TODO somehow they don't match!


summary(lm(Tradeoff ~ Condition + log(learning_rate) + log(learning_rate_attention) + NumEpochs, data=params))
summary(lm(Fixations ~ Condition + log(learning_rate) + log(learning_rate_attention) + NumEpochs, data=params))


library(dplyr)
library(tidyr)
library(lme4)


data = merge(data, params %>% rename(ModelNumber=myID), by=c("ModelNumber", "Condition"), all=TRUE)


data = data %>% filter(NumEpochs >= 8)

#u = data %>% group_by(Condition, ModelNumber) %>% summarise(c = cor(LogWordFreq, AttentionLogit, use='complete'))



data$LogWordFreq.C = data$LogWordFreq - mean(data$LogWordFreq, na.rm=TRUE)
data$Condition.C = ifelse(data$Condition == "Preview", 0.5, -0.5)
data$IsNamedEntity.C = data$IsNamedEntity - mean(data$IsNamedEntity, na.rm=TRUE)
data$IsCorrectAnswer.C = resid(lm(IsCorrectAnswer ~ IsNamedEntity, data=data, na.action=na.exclude))

library(stringr)
data$WordLength = str_length(data$Word)



data = data[data$WordLength < 100,]

# for some, this is unavailable (why?)
data = data %>% filter(!is.na(LAMBDA))
#data$WordLength.C = resid(lm(WordLength ~ LogWordFreq, data=data, na.action=na.exclude))
data$WordLength.C = resid(lm(WordLength ~ 1, data=data, na.action=na.exclude))

                                                                                                                            
data$Position.C = (data$Position - mean(data$Position)) / sd(data$Position)    



data$Item = paste(data$Text, data$Position)

#model = (lmer(AttentionLogit ~ IsCorrectAnswer*Condition + IsNamedEntity*IsCorrectAnswer + LogWordFreq*Condition + IsNamedEntity*Condition + (1|ModelNumber) + (1|Word), data=data))

#model = (lmer(Attended ~ IsCorrectAnswer.C*Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1|ModelNumber) + (1|Item), data=data))

#model = (lmer(Attended ~ IsCorrectAnswer.C*Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1|ModelNumber) + (1|Item), data=data %>% filter(LAMBDA==1)))
#print(summary(model))

# TODO recode forward selection
# TODO include Position

#                                Estimate Std. Error t value
#(Intercept)                    0.4451842  0.0195431  22.780
#IsCorrectAnswer.C             -0.0500912  0.0278267  -1.800
#Condition.C                   -0.0450388  0.0041027 -10.978
#WordLength.C                   0.0202441  0.0021871   9.256
#LogWordFreq.C                 -0.0396661  0.0010877 -36.469
#IsNamedEntity.C                0.3828013  0.0153734  24.900
#IsCorrectAnswer.C:Condition.C -0.0126356  0.0370317  -0.341
#Condition.C:WordLength.C      -0.0132755  0.0023303  -5.697
#Condition.C:LogWordFreq.C      0.0160219  0.0013893  11.532
#LogWordFreq.C:IsNamedEntity.C  0.0305543  0.0034778   8.785
#WordLength.C:IsNamedEntity.C  -0.0062084  0.0050395  -1.232
#WordLength.C:LogWordFreq.C     0.0011811  0.0006031   1.959
#Condition.C:IsNamedEntity.C    0.0691370  0.0141775   4.877


#model = (glmer(Attended ~ Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1|ModelNumber) + (1|Item), data=data %>% filter(LAMBDA==1), family="binomial"))



#model = (glmer(Attended ~ Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1|ModelNumber) + (1|Item), data=data %>% filter(LAMBDA==1), family="binomial"))
#print(summary(model))
#
#                               Estimate Std. Error z value Pr(>|z|)    
#(Intercept)                   -0.172407   0.045377  -3.799 0.000145 ***
#Position.C                     0.029909   0.011685   2.560 0.010478 *  
#IsCorrectAnswer.C             -0.237455   0.109843  -2.162 0.030635 *  
#Condition.C                   -0.202634   0.007012 -28.898  < 2e-16 ***
#WordLength.C                   0.150105   0.008291  18.105  < 2e-16 ***
#LogWordFreq.C                 -0.216999   0.004171 -52.024  < 2e-16 ***
#IsNamedEntity.C                1.629880   0.058934  27.656  < 2e-16 ***
#Condition.C:WordLength.C      -0.023655   0.003988  -5.932    3e-09 ***
#Condition.C:LogWordFreq.C      0.062537   0.002489  25.121  < 2e-16 ***
#LogWordFreq.C:IsNamedEntity.C  0.156901   0.013601  11.536  < 2e-16 ***
#WordLength.C:IsNamedEntity.C  -0.053668   0.019827  -2.707 0.006792 ** 
#WordLength.C:LogWordFreq.C     0.021167   0.002296   9.217  < 2e-16 ***
#Condition.C:IsNamedEntity.C    0.276870   0.026161  10.583  < 2e-16 ***

crash()


#model = (glmer(Attended ~ Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1 + Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C|ModelNumber) + (1+Condition.C|Item), data=data %>% filter(LAMBDA==1), family="binomial"))


library(brms)
#model = (brm(Attended ~ Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1 + Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C|ModelNumber) + (1+Condition.C|Item), data=data %>% filter(LAMBDA==1), family="bernoulli", cores=4))
#model = (brm(Attended ~ Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1 + Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C|ModelNumber) + (1|Item), data=data %>% filter(LAMBDA==1), family="bernoulli", cores=4))
model = (brm(Attended ~ Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1|ModelNumber) + (1|Item), data=data %>% filter(LAMBDA==1), family="bernoulli", cores=4))


model = (brm(Attended ~ Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1 + Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C|ModelNumber) + (1+Condition.C|Item), data=data %>% filter(LAMBDA==1), family="bernoulli", chains=1, iterations=10000, cores=4))


# New analysis!
model = (brm(Attended ~ IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1 + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C|ModelNumber) + (1+Condition.C|Item), data=data %>% filter(LAMBDA==1), family="bernoulli", chains=1, iter=10000, cores=4))

model = (glmer(Attended ~ IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1|ModelNumber) + (1|Item), data=data %>% filter(LAMBDA==1), family="binomial"))




#                              Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
#Intercept                        -0.19      0.04    -0.28    -0.11 1.05       64      151
#Position.C                        0.03      0.01     0.01     0.05 1.01      132      309 *
#IsCorrectAnswer.C                -0.23      0.12    -0.45     0.00 1.01      161      220
#Condition.C                      -0.26      0.04    -0.34    -0.17 1.00      171      462 *
#WordLength.C                      0.15      0.01     0.13     0.18 1.00      131      247 *
#LogWordFreq.C                    -0.23      0.01    -0.25    -0.22 1.00      182      293 *
#IsNamedEntity.C                   1.73      0.09     1.56     1.92 1.01      168      377 *
#Condition.C:WordLength.C         -0.01      0.01    -0.03     0.02 1.00      295      477 -
#Condition.C:LogWordFreq.C         0.03      0.01     0.01     0.05 1.01      229      489 *
#LogWordFreq.C:IsNamedEntity.C     0.17      0.02     0.14     0.20 1.00      121      339 *
#WordLength.C:IsNamedEntity.C     -0.03      0.02    -0.07     0.01 1.01      127      394 
#WordLength.C:LogWordFreq.C        0.02      0.00     0.01     0.03 1.00      257      464 *
#Condition.C:IsNamedEntity.C       0.35      0.06     0.23     0.46 1.00      300      608 *
#
#Samples were drawn using sampling(NUTS). For each parameter, Bulk_ESS
#and Tail_ESS are effective sample size measures, and Rhat is the potential
#scale reduction factor on split chains (at convergence, Rhat = 1).
#> model = (brm(Attended ~ Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1 + Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C|ModelNumber) + (1+Condition.C|Item), data=data %>% filter(LAMBDA==1), family="bernoulli", chains=1))





# The third model, no random slopes
#                              Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
#Intercept                        -0.17      0.05    -0.27    -0.07 1.01      358      663 
#Position.C                        0.03      0.01     0.01     0.05 1.01      448     1034 
#IsCorrectAnswer.C                -0.24      0.11    -0.46    -0.03 1.00      568     1092 
#Condition.C                      -0.20      0.01    -0.22    -0.19 1.00     8869     2692
#WordLength.C                      0.15      0.01     0.13     0.17 1.01      436      850
#LogWordFreq.C                    -0.22      0.00    -0.22    -0.21 1.00      605     1252
#IsNamedEntity.C                   1.63      0.06     1.52     1.75 1.01      495     1071
#Condition.C:WordLength.C         -0.02      0.00    -0.03    -0.02 1.00    10384     2837
#Condition.C:LogWordFreq.C         0.06      0.00     0.06     0.07 1.00     5697     3048
#LogWordFreq.C:IsNamedEntity.C     0.16      0.01     0.13     0.18 1.01      563     1083
#WordLength.C:IsNamedEntity.C     -0.05      0.02    -0.09    -0.01 1.00      507      887 
#WordLength.C:LogWordFreq.C        0.02      0.00     0.02     0.03 1.00      570     1324 
#Condition.C:IsNamedEntity.C       0.28      0.03     0.22     0.33 1.00     7503     3041 





#model = (brm(Attended ~ Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1 + Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C|ModelNumber) + (1+Condition.C|Item), data=data %>% filter(LAMBDA==1), family="bernoulli", chains=1, iter=5000))
#                              Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
#Intercept                        -0.20      0.05    -0.29    -0.11 1.00      139      349
#Position.C                        0.03      0.01     0.01     0.05 1.01      224      549
#IsCorrectAnswer.C                -0.24      0.11    -0.46    -0.02 1.00      474     1028
#Condition.C                      -0.26      0.05    -0.34    -0.16 1.00      486      765
#WordLength.C                      0.16      0.01     0.13     0.18 1.00      421      919
#LogWordFreq.C                    -0.23      0.01    -0.25    -0.22 1.00      447      704
#IsNamedEntity.C                   1.72      0.10     1.53     1.92 1.01      186      531
#Condition.C:WordLength.C         -0.01      0.01    -0.04     0.01 1.00      761     1364
#Condition.C:LogWordFreq.C         0.03      0.01     0.01     0.05 1.00      603     1105
#LogWordFreq.C:IsNamedEntity.C     0.17      0.02     0.13     0.20 1.00      322      526
#WordLength.C:IsNamedEntity.C     -0.03      0.02    -0.07     0.01 1.00      485      864
#WordLength.C:LogWordFreq.C        0.02      0.00     0.01     0.03 1.00      461      808
#Condition.C:IsNamedEntity.C       0.34      0.06     0.24     0.46 1.00      635     1110





#model = (brm(Attended ~ Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1 + Position.C + IsCorrectAnswer.C + Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C|ModelNumber) + (1|Item), data=data %>% filter(LAMBDA==1), family="bernoulli", cores=4))
#                              Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
#Intercept                        -0.18      0.05    -0.26    -0.08 1.02      211      292
#Position.C                        0.03      0.01     0.01     0.06 1.01      480      940
#IsCorrectAnswer.C                -0.22      0.12    -0.44     0.01 1.00      677     1507
#Condition.C                      -0.20      0.04    -0.28    -0.12 1.00     1091     1683
#WordLength.C                      0.15      0.01     0.13     0.18 1.01      620     1067
#LogWordFreq.C                    -0.22      0.01    -0.23    -0.21 1.01      731     1256
#IsNamedEntity.C                   1.64      0.10     1.45     1.83 1.01      494     1128
#Condition.C:WordLength.C         -0.03      0.01    -0.05    -0.01 1.00     1526     2518
#Condition.C:LogWordFreq.C         0.07      0.01     0.05     0.08 1.00     1116     2163
#LogWordFreq.C:IsNamedEntity.C     0.16      0.02     0.13     0.19 1.01      688     1206
#WordLength.C:IsNamedEntity.C     -0.05      0.02    -0.09    -0.01 1.00      684     1613
#WordLength.C:LogWordFreq.C        0.02      0.00     0.02     0.03 1.00      792     1623
#Condition.C:IsNamedEntity.C       0.26      0.03     0.20     0.31 1.00     5602     3276



