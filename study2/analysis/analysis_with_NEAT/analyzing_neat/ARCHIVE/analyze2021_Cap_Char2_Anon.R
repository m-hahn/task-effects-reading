data = read.csv("/u/scr/mhahn/NEURAL_ATTENTION_TASK/OUTPUTS_TEXTS/train_attention_7DailyMail_500_Recurrent_Record_Save_14_Flexible_Tune_TrainPreview_Reader_Mask_LengthPreview_New_Cap_Char2_RunOnTexts_Collect.py.tsv", sep="\t")
params = read.csv("/juice/scr/mhahn/CODE/task-effects-reading/study2/NEAT/train_NEAT/logs/SUMMARY_train_attention_7DailyMail_500_Recurrent_Record_Save_14_Flexible_Tune_TrainPreview_Reader_Mask_LengthPreview_New_Cap_Char2_CollectTradeoff.py.tsv", sep="\t")


summary(lm(Tradeoff ~ Condition + log(learning_rate) + log(learning_rate_attention) + NumEpochs, data=params))
summary(lm(Fixations ~ Condition + log(learning_rate) + log(learning_rate_attention) + NumEpochs, data=params))


library(dplyr)
library(tidyr)
library(lme4)


data = merge(data, params %>% rename(ModelNumber=myID), by=c("ModelNumber", "Condition"), all=TRUE)


data = data %>% filter(NumEpochs >= 10)

#u = data %>% group_by(Condition, ModelNumber) %>% summarise(c = cor(LogWordFreq, AttentionLogit, use='complete'))



data$LogWordFreq.C = data$LogWordFreq - mean(data$LogWordFreq, na.rm=TRUE)
data$Condition.C = ifelse(data$Condition == "Preview", 0.5, -0.5)
data$IsNamedEntity.C = data$IsNamedEntity - mean(data$IsNamedEntity, na.rm=TRUE)
data$IsCorrectAnswer.C = resid(lm(IsCorrectAnswer ~ IsNamedEntity, data=data, na.action=na.exclude))

library(stringr)
data$WordLength = str_length(data$Word)


# TODO occasionally there are very long entries in "Word". Figure out where they come from

data = data[data$WordLength < 20,]

# for some, this is unavailable (why?)
data = data %>% filter(!is.na(LAMBDA))
data$WordLength.C = resid(lm(WordLength ~ LogWordFreq, data=data, na.action=na.exclude))

data[data$IsNamedEntity,]$LogWordFreq.C = 0
data[data$IsNamedEntity,]$WordLength.C = 0




data$Item = paste(data$Text, data$Position)

#model = (lmer(AttentionLogit ~ IsCorrectAnswer*Condition + IsNamedEntity*IsCorrectAnswer + LogWordFreq*Condition + IsNamedEntity*Condition + (1|ModelNumber) + (1|Word), data=data))

#model = (lmer(Attended ~ IsCorrectAnswer.C*Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1|ModelNumber) + (1|Item), data=data))

model = (lmer(Attended ~ IsCorrectAnswer.C*Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1|ModelNumber) + (1|Item), data=data))
print(summary(model))

#model = (glmer(Attended ~ IsCorrectAnswer.C*Condition.C + WordLength.C*Condition.C + LogWordFreq.C*Condition.C + LogWordFreq.C*IsNamedEntity.C + IsNamedEntity.C*WordLength.C + WordLength.C*LogWordFreq.C + IsNamedEntity.C*Condition.C + (1|ModelNumber) + (1|Item), data=data, family="binomial"))
#summary(model)

library(ggplot2)

FILE = "analyze2021_Cap_Char2_Anon"

plot = ggplot(params, aes(x=Fixations, y=Accuracy, color=Condition)) + geom_point()
ggsave(plot, file=paste("figures/", FILE, "_Accuracy_Condition.pdf", sep=""), height=4, width=4)

plot = ggplot(data %>% filter(IsNamedEntity) %>% group_by(LAMBDA, ModelNumber, IsCorrectAnswer, Condition) %>% filter(!is.na(IsCorrectAnswer)) %>% summarise(Fixations=mean(Attended)), aes(x=IsCorrectAnswer, y=Fixations, linetype=Condition, color=ModelNumber, group=paste(Condition,ModelNumber))) + geom_line() + facet_wrap(~LAMBDA)
ggsave(plot, file=paste("figures/", FILE, "_IsCorrectAnswer_Condition.pdf", sep=""), height=4, width=4)

plot = ggplot(data %>% group_by(LAMBDA, ModelNumber, IsNamedEntity, Condition) %>% filter(!is.na(IsNamedEntity)) %>% summarise(Fixations=mean(Attended)), aes(x=IsNamedEntity, y=Fixations, linetype=Condition, color=ModelNumber, group=paste(Condition,ModelNumber))) + geom_line() + facet_wrap(~LAMBDA)
ggsave(plot, file=paste("figures/", FILE, "_IsNamedEntity_Condition.pdf", sep=""), height=4, width=4)


plot = ggplot(data, aes(x=LogWordFreq, y=Attended, linetype=Condition, color=ModelNumber, group=paste(Condition,ModelNumber))) + geom_smooth(method="gam") + facet_wrap(~LAMBDA)
ggsave(plot, file=paste("figures/", FILE, "_LogWordFreq_Condition.pdf", sep=""), height=4, width=4)


plot = ggplot(data, aes(x=WordLength, y=Attended, linetype=Condition, color=ModelNumber, group=paste(Condition,ModelNumber))) + geom_smooth(method="gam") + facet_wrap(~LAMBDA)
ggsave(plot, file=paste("figures/", FILE, "_WordLength_Condition.pdf", sep=""), height=4, width=4)




# analyze2021_Cap_Char2_Anon.R promising:
#                           Estimate Std. Error z value Pr(>|z|)    
#(Intercept)               -0.191847   0.053118  -3.612 0.000304 ***
#IsCorrectAnswer           -0.053439   0.261542  -0.204 0.838100    
#Condition                 -0.137424   0.015845  -8.673  < 2e-16 ***
#IsNamedEntity              5.614542   0.085693  65.519  < 2e-16 ***
#LogWordFreq               -0.048579   0.003384 -14.354  < 2e-16 ***
#IsCorrectAnswer:Condition -0.876423   0.536345  -1.634 0.102245    
#Condition:LogWordFreq      0.050282   0.003348  15.018  < 2e-16 ***
#Condition:IsNamedEntity    0.037694   0.159397   0.236 0.813063    

