library(tidyr)
library(dplyr)
model = read.csv("~/CS_SCR/NEURAL_ATTENTION_TASK/OUTPUTS_TEXTS/merged.tsv", sep="\t")
model = model %>% mutate(FixationProb = 1/(1+exp(-AttentionLogit)))

library(lme4)

data=model
data = data[!data$IsOOV,]                          
summary(data)                     
data$Condition.C = (data$Condition == "Preview")                    
data$Condition.C = data$Condition.C - 0.5          
data$IsNamedEntity.C = (data$IsNamedEntity - mean(data$IsNamedEntity, na.rm=TRUE))
data$LogWordFreq.C = (data$LogWordFreq - mean(data$LogWordFreq, na.rm=TRUE))/sd(data$LogWordFreq, na.rm=TRUE)            
data[data$IsNamedEntity,]$LogWordFreq = 0          
data$Position.C = (data$Position - mean(data$Position))/sd(data$Position)              
data$IsCorrectAnswer.C = residuals(lm(IsCorrectAnswer~IsNamedEntity, data=data))                      
data$ID = paste0(data$Text, "_", data$Position)


library(ggplot2)


plot = ggplot(data %>% filter(!IsOOV, !IsNamedEntity), aes(x=LogWordFreq, y=Attended, color=Condition)) + geom_smooth(method=glm, method.args= list(family="binomial")) + xlab("Log Word Frequency") + ylab("Fixation Rate")
ggsave("figures/plot1113.pdf")

