def stars(x):
   x = min(x, 1-x)
   if x < 0.001:
      return "***"
   elif x < 0.01:
      return "**"
   else:
      return "*"


data = {}
for measure in ["ff", "fp", "tt"]:
    with open(f"models/{measure}-lmer-bare_inter.txt", "r") as inFile:
        data_ = inFile.read().strip().split("\n")
        data[measure] = [[y.strip() for y in x.strip().replace("\\hline", "").strip().replace("\\", "").split("&")] for x in data_[6:-16]]
print(data)

def processPred(x):
    return ":".join(sorted(x.strip().replace(".Resid", "").replace(".C", "").replace("ExperimentTokenLength", "WordLength").replace("HumanPosition", "PositionText").replace("Intercept", "(Intercept)").replace(".", ":").split(":")))

with open("models/FIXATED-Neat_surp.txt", "r") as inFile:
   dataFix = inFile.read().strip().split("\n")
   print(dataFix)
   coefficients = [[y for y in x.split(" ") if len(y) > 0] for x in dataFix[dataFix.index('Population-Level Effects: ')+2:dataFix.index("POSTERIOR")-3]]
   coefficients = {processPred(x[0].strip()) : x for x in coefficients if len(x) > 0}
   posterior = [x.split("\t") for x in dataFix[dataFix.index("POSTERIOR")+1:]]
   print(posterior)
   posterior = {processPred(x[0]) : float(x[3]) for x in posterior}
   print(coefficients)
   print(posterior)
#quit()


def formatSig(x, sig):
    if not sig:
        return x
    else:
        return "\\textbf{"+x+"}"



predictors = sorted(list(set(([":".join(sorted(x.replace(".Resid", "").replace(".C", "").split(":"))) for x in list(sorted(set([x[0] for x in data["tt"] + data["ff"] + data["fp"]])))]))), key=lambda x:(1 if ":" in x else 0, x))
print(predictors)
print(list(coefficients))
print(list(posterior))

data["ff"] = {":".join(sorted(x[0].replace(".Resid", "").replace(".C", "").split(":"))) : x for x in data["ff"]}
data["fp"] = {":".join(sorted(x[0].replace(".Resid", "").replace(".C", "").split(":"))) : x for x in data["fp"]}
data["tt"] = {":".join(sorted(x[0].replace(".Resid", "").replace(".C", "").split(":"))) : x for x in data["tt"]}

with open("output/table.tex", "w") as outFile:
  for predName in predictors:
  #    if predName == "(Intercept)":
  #       predName = "Intercept"
  #    elif predName == "WordLength":
  #       predName = "ExperimentTokenLength"
  #    elif predName == "PositionText":
  #      predName = "HumanPosition"
      if predName.strip() == "":
          continue
      out = predName
      for measure in ["ff", "fp", "tt"]:
          if predName in data[measure] and len(data[measure][predName]) > 1:
             resu = data[measure][predName][1:]
             t = float(resu[0])/float(resu[1][1:-1])
             out += " & " + " & ".join([formatSig(resu[0], abs(t) > 2), resu[1]])
          else:
             out += " & " + " & ".join(data[measure].get(predName, [None, "", ""])[1:])
      outFix = coefficients.get(predName, None)
      if outFix is None:
         out += " & " + " & ".join(["", ""])
      else:
         out += " & " + " & ".join([formatSig(outFix[1], abs(0.5-posterior[predName]) > 0.45), "("+outFix[2]+")"])
      out = out + "\\\\"
      print(out, file=outFile)
  print("\\hline", file=outFile)
  
  for measure in ["ff", "fp", "tt"]:
      with open(f"models/{measure}-lmer-neat_inter.txt", "r") as inFile:
          data_ = inFile.read().strip().split("\n")
          data[measure] = [[y.strip() for y in x.strip().replace("\\hline", "").strip().replace("\\", "").split("&")] for x in data_[6:-16]]
  
  data["ff"] = {":".join(sorted(x[0].replace(".Resid", "").replace(".C", "").split(":"))) : x for x in data["ff"]}
  data["fp"] = {":".join(sorted(x[0].replace(".Resid", "").replace(".C", "").split(":"))) : x for x in data["fp"]}
  data["tt"] = {":".join(sorted(x[0].replace(".Resid", "").replace(".C", "").split(":"))) : x for x in data["tt"]}
  
  #quit()
  if True:
      predName = "FixationProb"
      out = "NEAT Attention"
      for measure in ["ff", "fp", "tt"]:
          resu = data[measure][predName][1:]
          t = float(resu[0])/float(resu[1][1:-1])
          out += " & " + " & ".join([formatSig(resu[0], abs(t) > 2), resu[1]])
      predName = "FixationProb"
      outFix = coefficients[predName]
      out += " & " + " & ".join([formatSig(outFix[1], posterior[predName] < 0.05), "("+outFix[2]+")"])
      out = out + "\\\\"
      print(out, file=outFile)
  
  data = {}
  for measure in ["ff", "fp", "tt"]:
      with open(f"models/comparison-{measure}_inter.txt", "r") as inFile:
          data[measure] = [[float(y) if y != "NA" else None for y in x.split(" ")] for x in inFile.read().strip().split("\n")]
  
  def deltaAIC(x):
      return round(x[0][0]-x[0][1])
  def deltaBIC(x):
      return round(x[1][0]-x[1][1])
  def deviance(x):
      return round(x[2][0]-x[2][1])
  def chis(x):
      return round(x[3][1],3)
  def pchis(x):
      return round(x[4][1],4)
  def flatten(x):
      r = []
      for y in x:
          for z in y:
              r.append(z)
      return r 
  print("\\quad AIC Difference & ", " & ".join(flatten([(str(deltaAIC(data[measure])), "") for measure in ["ff", "fp", "tt"]])) + "\\\\", file=outFile)
  print("\\quad BIC Difference & ", " & ".join(flatten([(str(deltaBIC(data[measure])), "") for measure in ["ff", "fp", "tt"]])) + "\\\\", file=outFile)
  print("\\quad Deviance & ", " & ".join(flatten([(str(deviance(data[measure])), "") for measure in ["ff", "fp", "tt"]])) + "\\\\", file=outFile)
  print("\\quad $\\chi^2$ & ", " & ".join(flatten([(str(chis(data[measure])), "") for measure in ["ff", "fp", "tt"]])) + "\\\\", file=outFile)
  print("\\quad $P(>\\chi^2)$ & ", " & ".join(flatten([(str(pchis(data[measure])), "") for measure in ["ff", "fp", "tt"]])) + "\\\\", file=outFile)

#
#(Intercept)    &198.59 & (5.18) & $^{***}$ &217.84 & (8.41) & $^{***}$ &266.75 & (10.51)& $^{***}$ &-0.486 & (0.18)  & $^{**}$ \\ % (Intercept)  
#LogWordFreq    & -6.74 & (0.73) & $^{***}$ &-21.19 & (1.19) & $^{***}$ &-37.86 & (1.95) & $^{***}$ &-0.606 & (0.042) & $^{***}$\\
#WordLength     &  0.35 & (0.34) &          &  8.67 & (0.55) & $^{***}$ & 14.00 & (0.90) & $^{***}$ & 0.54  & (0.032) & $^{***}$\\ % WordLength   
#IsNamedEntity  &  9.96 & (2.61) & $^{***}$ & 20.33 & (4.29) & $^{***}$ & 64.86 & (7.02) & $^{***}$ & 0.065 & (0.018) & $^{**}$ \\ % IsNamedEntity
#PositionText   & -2.02 & (2.17) &          & -5.49 & (3.54) &          &-29.65 & (5.76) & $^{***}$ & 0.007 & (0.055) & $^{}$   \\ % PositionText 
#Surprisal      &  5.07 & (0.91) & $^{***}$ &  7.38 & (1.49) & $^{***}$ & 16.07 & (2.43) & $^{***}$ & 0.029 & (0.018) & $^{}$   \\ % Surprisal    
#
