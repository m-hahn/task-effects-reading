library(tidyr)
library(dplyr)
SCR = "~/scr/" #Sys.getenv("SCR")
#SCR = Sys.getenv("SCR")
surprisals = read.csv(paste(SCR,"/mappingSurprisals.tsv", sep=""), sep="\t", quote="@")
human = read.csv(paste(SCR,"/mappingWithHuman.tsv", sep=""), sep="\t")
model = read.csv(paste(SCR,"/NEURAL_ATTENTION_TASK/OUTPUTS_TEXTS/merged.tsv", sep=""), sep="\t")
model = model %>% mutate(FixationProb = 1/(1+exp(-AttentionLogit)))
model_ = model %>% group_by(Text, Position, Word, IsCorrectAnswer, Condition, IsOOV, IsNamedEntity, LogWordFreq) %>% summarise(FixationProb = mean(FixationProb))
joint = merge(human, model_ %>% mutate(TextFileName=Text, AnonymizedPosition = Position), by=c("Condition", "TextFileName", "AnonymizedPosition"))

joint = merge(joint, surprisals, by=c("TextFileName", "HumanPosition", "AnonymizedPosition", "OriginalToken"))



library(lme4)

# Predictors
# - LogWordFreq
# - WordLength
# - IsNamedEntity
# - PositionText
# - Surprisal


# PROBLEM: Participant labels across conditions




external = read.csv(paste(SCR,"/mappingWithExternal.tsv", sep=""), sep="\t") %>% select(TextFileName, JointPosition, AnonymizedToken, ExperimentTokenLength)
joint = merge(joint, external, by=c("TextFileName", "JointPosition", "AnonymizedToken"))



joint$ID = paste0(joint$TextFileName, joint$JointPosition)

joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal = NA
joint$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = joint, na.action=na.exclude))
joint$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = joint, na.action=na.exclude))

library(brms)
joint = joint %>% filter(!is.na(fp))
joint$Fixated = (joint$fp > 0)

joint = joint %>% mutate(LogWordFreq.C = (LogWordFreq - mean(LogWordFreq, na.rm=TRUE)) / sd(LogWordFreq, na.rm=TRUE))
joint = joint %>% mutate(HumanPosition.C = (HumanPosition - mean(HumanPosition, na.rm=TRUE)) / sd(HumanPosition, na.rm=TRUE))
joint = joint %>% mutate(Condition.C = ifelse(Condition == "Preview", -0.5, 0.5))
joint = joint %>% mutate(IsNamedEntity.C = ifelse(IsNamedEntity == FALSE, -0.5, 0.5))
joint = joint %>% mutate(FixationProb.C = (FixationProb - mean(FixationProb, na.rm=TRUE))/sd(FixationProb, na.rm=TRUE))

joint$FixationProb.Resid = resid(lm(FixationProb ~  ExperimentTokenLength.Resid + LogWordFreq +  HumanPosition + Surprisal.Resid, data=joint, na.action=na.exclude))
joint$IsCorrectAnswer.Resid = resid(lm(IsCorrectAnswer ~ IsNamedEntity, data=joint, na.action=na.exclude))

# Without CONDITION
modelFix = brm(Fixated ~ FixationProb.C + ExperimentTokenLength.Resid*LogWordFreq.C + ExperimentTokenLength.Resid + LogWordFreq.C +  HumanPosition.C + Surprisal.Resid + (1 + FixationProb.C + ExperimentTokenLength.Resid*LogWordFreq.C + ExperimentTokenLength.Resid + LogWordFreq.C +  HumanPosition.C + Surprisal.Resid|Participant) +(1|ID), data=joint, family="bernoulli", cores=4)

sink("models/FIXATED-neat-summary.txt")
summary(modelFix)
sink()

samples = posterior_samples(modelFix)


sink("models/FIXATED-Neat.txt")
print(summary(modelFix))
fitted = data.frame(fixef(modelFix, summary=FALSE))
cat("POSTERIOR\n")
for(p in names(fitted)) {
   cat(p, "\t", mean(fitted[[p]]), "\t", sd(fitted[[p]]), "\t", mean(fitted[[p]] < 0.0), "\n")
}
sink()




