
\begin{table}$
\begin{center}$
\begin{tabular}${l c }$
\hline
 & Model 1 \\
\hline
(Intercept)                                 & 201.36 & (4.49) &  $^{***}$ \\
PositionText.C                             & 0.76 & (0.46) &           \\
LogWordFreq.C                               & -6.11 & (0.49) &  $^{***}$  \\
Surprisal.Resid                             & 1.27 & (0.19) &  $^{***}$   \\
WordLength.Resid                 & 0.41 & (0.25) &           \\
Condition.C                                 & 20.07 & (8.97) &  $^{*}$    \\
FixationProb.Resid                          & -0.07 & (1.73) &          \\
LogWordFreq.C:WordLength.Resid   & 1.02 & (0.23) &  $^{***}$   \\
LogWordFreq.C:Surprisal.Resid               & 0.55 & (0.21) &  $^{**}$    \\
Surprisal.Resid:WordLength.Resid & 0.18 & (0.08) &  $^{*}$     \\
Condition.C:FixationProb.Resid              & -0.66 & (2.89) &          \\
\hline
AIC                                         & 537474.63                \\
BIC                                         & 537597.04                \\
Log Likelihood                              & -268723.31               \\
Num. obs.                                   & 46334                    \\
Num. groups: tokenID                        & 4950                     \\
Num. groups: Participant                    & 22                       \\
Var: tokenID (Intercept)                    & 309.45                   \\
Var: Participant (Intercept)                & 435.30                   \\
Var: Residual                               & 6119.34                  \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001,  &  $^{**}$p<0.01,  &  $^*p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
