 Family: bernoulli 
  Links: mu = logit 
Formula: Fixated ~ FixationProb.C + ExperimentTokenLength.Resid * LogWordFreq.C + ExperimentTokenLength.Resid + LogWordFreq.C + HumanPosition.C + Surprisal.Resid + (1 + FixationProb.C + ExperimentTokenLength.Resid * LogWordFreq.C + ExperimentTokenLength.Resid + LogWordFreq.C + HumanPosition.C + Surprisal.Resid | Participant) + (1 | ID) 
   Data: joint (Number of observations: 90900) 
Samples: 4 chains, each with iter = 2000; warmup = 1000; thin = 1;
         total post-warmup samples = 4000

Group-Level Effects: 
~ID (Number of levels: 3759) 
              Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
sd(Intercept)     0.65      0.01     0.63     0.68       2083 1.00

~Participant (Number of levels: 24) 
                                                                           Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
sd(Intercept)                                                                  0.88      0.12     0.68     1.15        807 1.00
sd(FixationProb.C)                                                             0.04      0.01     0.01     0.07       1740 1.00
sd(ExperimentTokenLength.Resid)                                                0.06      0.01     0.05     0.09       1560 1.01
sd(LogWordFreq.C)                                                              0.19      0.03     0.14     0.26       1562 1.01
sd(HumanPosition.C)                                                            0.21      0.04     0.15     0.29       1738 1.00
sd(Surprisal.Resid)                                                            0.01      0.01     0.00     0.02        928 1.00
sd(ExperimentTokenLength.Resid:LogWordFreq.C)                                  0.03      0.01     0.01     0.04        831 1.00
cor(Intercept,FixationProb.C)                                                 -0.20      0.25    -0.64     0.32       4000 1.00
cor(Intercept,ExperimentTokenLength.Resid)                                     0.73      0.11     0.45     0.89       2283 1.00
cor(FixationProb.C,ExperimentTokenLength.Resid)                                0.03      0.26    -0.50     0.52       1571 1.00
cor(Intercept,LogWordFreq.C)                                                  -0.61      0.13    -0.82    -0.32       1913 1.00
cor(FixationProb.C,LogWordFreq.C)                                             -0.15      0.25    -0.62     0.34       1478 1.00
cor(ExperimentTokenLength.Resid,LogWordFreq.C)                                -0.58      0.15    -0.82    -0.22       1533 1.00
cor(Intercept,HumanPosition.C)                                                 0.36      0.16     0.01     0.64       2574 1.00
cor(FixationProb.C,HumanPosition.C)                                            0.02      0.27    -0.50     0.55        964 1.00
cor(ExperimentTokenLength.Resid,HumanPosition.C)                              -0.09      0.19    -0.46     0.27       2252 1.00
cor(LogWordFreq.C,HumanPosition.C)                                            -0.11      0.19    -0.46     0.26       2869 1.00
cor(Intercept,Surprisal.Resid)                                                 0.06      0.28    -0.50     0.59       4000 1.00
cor(FixationProb.C,Surprisal.Resid)                                           -0.19      0.32    -0.74     0.46       2060 1.00
cor(ExperimentTokenLength.Resid,Surprisal.Resid)                              -0.06      0.28    -0.59     0.51       4000 1.00
cor(LogWordFreq.C,Surprisal.Resid)                                             0.10      0.28    -0.46     0.61       4000 1.00
cor(HumanPosition.C,Surprisal.Resid)                                           0.02      0.29    -0.54     0.57       4000 1.00
cor(Intercept,ExperimentTokenLength.Resid:LogWordFreq.C)                       0.16      0.23    -0.31     0.59       4000 1.00
cor(FixationProb.C,ExperimentTokenLength.Resid:LogWordFreq.C)                 -0.37      0.27    -0.82     0.23       1508 1.00
cor(ExperimentTokenLength.Resid,ExperimentTokenLength.Resid:LogWordFreq.C)     0.18      0.25    -0.34     0.62       4000 1.00
cor(LogWordFreq.C,ExperimentTokenLength.Resid:LogWordFreq.C)                   0.19      0.24    -0.29     0.63       4000 1.00
cor(HumanPosition.C,ExperimentTokenLength.Resid:LogWordFreq.C)                 0.03      0.24    -0.44     0.50       4000 1.00
cor(Surprisal.Resid,ExperimentTokenLength.Resid:LogWordFreq.C)                -0.14      0.32    -0.70     0.51       1820 1.00

Population-Level Effects: 
                                          Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
Intercept                                    -0.50      0.18    -0.86    -0.15        344 1.01
FixationProb.C                                0.04      0.02     0.00     0.07       1992 1.00
ExperimentTokenLength.Resid                   0.28      0.02     0.25     0.32        654 1.00
LogWordFreq.C                                -0.63      0.04    -0.71    -0.54        717 1.00
HumanPosition.C                              -0.02      0.05    -0.11     0.07       1758 1.00
Surprisal.Resid                               0.01      0.01     0.00     0.02       2807 1.00
ExperimentTokenLength.Resid:LogWordFreq.C     0.06      0.01     0.04     0.08       2495 1.00

Samples were drawn using sampling(NUTS). For each parameter, Eff.Sample 
is a crude measure of effective sample size, and Rhat is the potential 
scale reduction factor on split chains (at convergence, Rhat = 1).
POSTERIOR
Intercept 	 -0.5023057 	 0.1842725 	 0.99675 
FixationProb.C 	 0.03646833 	 0.01599868 	 0.01325 
ExperimentTokenLength.Resid 	 0.2848378 	 0.01619201 	 0 
LogWordFreq.C 	 -0.6254844 	 0.04212057 	 1 
HumanPosition.C 	 -0.01692736 	 0.04606511 	 0.6465 
Surprisal.Resid 	 0.01300305 	 0.006057707 	 0.01875 
ExperimentTokenLength.Resid.LogWordFreq.C 	 0.06066948 	 0.009798823 	 0 
