 Family: bernoulli 
  Links: mu = logit 
Formula: FIXATED ~ IsNamedEntity.C * FixationProb.C + Condition.C * FixationProb.C + HumanPosition.C + LogWordFreq.C + Surprisal.Resid + ExperimentTokenLength.Resid + LogWordFreq.C * ExperimentTokenLength.Resid + ExperimentTokenLength.Resid * ExperimentTokenLength.Resid + (1 + Condition.C + Condition.C * FixationProb.C | tokenID) + (1 + HumanPosition.C + LogWordFreq.C + Surprisal.Resid + ExperimentTokenLength.Resid + LogWordFreq.C * ExperimentTokenLength.Resid + ExperimentTokenLength.Resid * ExperimentTokenLength.Resid + FixationProb.C + IsNamedEntity.C | Participant) 
   Data: data (Number of observations: 97741) 
Samples: 4 chains, each with iter = 2000; warmup = 1000; thin = 1;
         total post-warmup samples = 4000

Group-Level Effects: 
~Participant (Number of levels: 22) 
                                                                           Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
sd(Intercept)                                                                  0.73      0.10     0.58     0.95         38 1.20
sd(HumanPosition.C)                                                            0.29      0.04     0.22     0.38         53 1.10
sd(LogWordFreq.C)                                                              0.18      0.03     0.13     0.24         60 1.04
sd(Surprisal.Resid)                                                            0.02      0.01     0.01     0.03        100 1.01
sd(ExperimentTokenLength.Resid)                                                0.06      0.01     0.04     0.09         67 1.04
sd(FixationProb.C)                                                             0.07      0.04     0.00     0.16         61 1.06
sd(IsNamedEntity.C)                                                            0.12      0.05     0.01     0.22        117 1.03
sd(LogWordFreq.C:ExperimentTokenLength.Resid)                                  0.03      0.01     0.02     0.05        130 1.02
cor(Intercept,HumanPosition.C)                                                 0.37      0.14     0.09     0.61         46 1.08
cor(Intercept,LogWordFreq.C)                                                  -0.41      0.17    -0.73    -0.09         13 1.18
cor(HumanPosition.C,LogWordFreq.C)                                            -0.07      0.19    -0.42     0.32         25 1.11
cor(Intercept,Surprisal.Resid)                                                 0.15      0.22    -0.29     0.55         28 1.07
cor(HumanPosition.C,Surprisal.Resid)                                          -0.12      0.26    -0.61     0.39         59 1.04
cor(LogWordFreq.C,Surprisal.Resid)                                             0.21      0.21    -0.23     0.58        156 1.02
cor(Intercept,ExperimentTokenLength.Resid)                                     0.73      0.11     0.49     0.90         88 1.06
cor(HumanPosition.C,ExperimentTokenLength.Resid)                              -0.05      0.18    -0.39     0.30         68 1.07
cor(LogWordFreq.C,ExperimentTokenLength.Resid)                                -0.61      0.14    -0.86    -0.31         82 1.02
cor(Surprisal.Resid,ExperimentTokenLength.Resid)                              -0.00      0.23    -0.43     0.43         59 1.05
cor(Intercept,FixationProb.C)                                                 -0.15      0.29    -0.64     0.46         66 1.06
cor(HumanPosition.C,FixationProb.C)                                            0.10      0.30    -0.53     0.65        137 1.02
cor(LogWordFreq.C,FixationProb.C)                                             -0.04      0.32    -0.64     0.61        150 1.02
cor(Surprisal.Resid,FixationProb.C)                                           -0.18      0.32    -0.72     0.47        182 1.01
cor(ExperimentTokenLength.Resid,FixationProb.C)                               -0.06      0.29    -0.60     0.53        143 1.03
cor(Intercept,IsNamedEntity.C)                                                -0.28      0.25    -0.72     0.23        134 1.02
cor(HumanPosition.C,IsNamedEntity.C)                                          -0.08      0.26    -0.57     0.44        194 1.01
cor(LogWordFreq.C,IsNamedEntity.C)                                             0.51      0.24    -0.05     0.87         89 1.04
cor(Surprisal.Resid,IsNamedEntity.C)                                           0.03      0.29    -0.53     0.59         98 1.04
cor(ExperimentTokenLength.Resid,IsNamedEntity.C)                              -0.28      0.25    -0.74     0.20        102 1.04
cor(FixationProb.C,IsNamedEntity.C)                                           -0.10      0.33    -0.70     0.55        123 1.01
cor(Intercept,LogWordFreq.C:ExperimentTokenLength.Resid)                       0.22      0.19    -0.17     0.58         93 1.04
cor(HumanPosition.C,LogWordFreq.C:ExperimentTokenLength.Resid)                -0.01      0.22    -0.43     0.41         88 1.02
cor(LogWordFreq.C,LogWordFreq.C:ExperimentTokenLength.Resid)                   0.21      0.21    -0.22     0.58        137 1.02
cor(Surprisal.Resid,LogWordFreq.C:ExperimentTokenLength.Resid)                 0.05      0.25    -0.42     0.53        117 1.01
cor(ExperimentTokenLength.Resid,LogWordFreq.C:ExperimentTokenLength.Resid)     0.13      0.22    -0.32     0.55         88 1.03
cor(FixationProb.C,LogWordFreq.C:ExperimentTokenLength.Resid)                 -0.20      0.34    -0.76     0.58         17 1.12
cor(IsNamedEntity.C,LogWordFreq.C:ExperimentTokenLength.Resid)                 0.03      0.28    -0.51     0.59        145 1.01

~tokenID (Number of levels: 4382) 
                                               Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
sd(Intercept)                                      0.63      0.01     0.61     0.66         28 1.14
sd(Condition.C)                                    0.28      0.05     0.18     0.37          4 1.47
sd(FixationProb.C)                                 0.89      0.09     0.73     1.06         13 1.41
sd(Condition.C:FixationProb.C)                     0.18      0.14     0.01     0.58          7 1.40
cor(Intercept,Condition.C)                         0.27      0.12     0.06     0.51          5 1.34
cor(Intercept,FixationProb.C)                     -0.01      0.07    -0.15     0.12         20 1.21
cor(Condition.C,FixationProb.C)                    0.33      0.28    -0.44     0.70          3 2.47
cor(Intercept,Condition.C:FixationProb.C)          0.00      0.42    -0.87     0.74         25 1.13
cor(Condition.C,Condition.C:FixationProb.C)       -0.25      0.45    -0.90     0.71         19 1.08
cor(FixationProb.C,Condition.C:FixationProb.C)    -0.03      0.43    -0.81     0.77         56 1.04

Population-Level Effects: 
                                          Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
Intercept                                    -3.88      4.11   -12.32     3.26         24 1.13
IsNamedEntity.C                             -28.15     32.92   -95.44    29.10         26 1.12
FixationProb.C                                5.67      6.54    -5.70    19.08         26 1.12
Condition.C                                   0.57      0.16     0.25     0.83         33 1.13
HumanPosition.C                              -0.03      0.06    -0.14     0.10         22 1.17
LogWordFreq.C                                -0.66      0.03    -0.73    -0.60         16 1.27
Surprisal.Resid                               0.01      0.01    -0.00     0.02         83 1.02
ExperimentTokenLength.Resid                   0.29      0.01     0.26     0.31         12 1.32
IsNamedEntity.C:FixationProb.C               45.26     53.16   -47.26   153.98         26 1.12
FixationProb.C:Condition.C                   -0.14      0.08    -0.30     0.02        116 1.04
LogWordFreq.C:ExperimentTokenLength.Resid     0.05      0.01     0.04     0.08         34 1.09

Samples were drawn using sampling(NUTS). For each parameter, Eff.Sample 
is a crude measure of effective sample size, and Rhat is the potential 
scale reduction factor on split chains (at convergence, Rhat = 1).
POSTERIOR
Intercept 	 -3.883999 	 4.107331 	 0.806 
IsNamedEntity.C 	 -28.1539 	 32.91594 	 0.781 
FixationProb.C 	 5.674886 	 6.542975 	 0.21475 
Condition.C 	 0.5712131 	 0.1559999 	 0 
HumanPosition.C 	 -0.0288653 	 0.06076157 	 0.689 
LogWordFreq.C 	 -0.6626618 	 0.03468546 	 1 
Surprisal.Resid 	 0.01062878 	 0.006627065 	 0.06025 
ExperimentTokenLength.Resid 	 0.2869847 	 0.0147572 	 0 
IsNamedEntity.C.FixationProb.C 	 45.26029 	 53.16079 	 0.22 
FixationProb.C.Condition.C 	 -0.1420208 	 0.08168567 	 0.96 
LogWordFreq.C.ExperimentTokenLength.Resid 	 0.0548536 	 0.01016349 	 0 
