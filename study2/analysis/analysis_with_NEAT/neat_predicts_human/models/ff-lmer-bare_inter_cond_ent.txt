
\begin{table}$
\begin{center}$
\begin{tabular}${l c }$
\hline
 & Model 1 \\
\hline
(Intercept)                                 & 201.35 & (4.49) &  $^{***}$ \\
PositionText.C                             & 0.76 & (0.46) &           \\
LogWordFreq.C                               & -6.44 & (0.52) &  $^{***}$  \\
Surprisal.Resid                             & 1.27 & (0.19) &  $^{***}$   \\
WordLength.Resid                 & 0.38 & (0.25) &           \\
IsNamedEntity.C                             & -2.65 & (1.42) &          \\
Condition.C                                 & 20.05 & (8.97) &  $^{*}$    \\
LogWordFreq.C:WordLength.Resid   & 1.07 & (0.23) &  $^{***}$   \\
LogWordFreq.C:Surprisal.Resid               & 0.53 & (0.21) &  $^{*}$     \\
Surprisal.Resid:WordLength.Resid & 0.18 & (0.08) &  $^{*}$     \\
\hline
AIC                                         & 537469.19                \\
BIC                                         & 537582.85                \\
Log Likelihood                              & -268721.59               \\
Num. obs.                                   & 46334                    \\
Num. groups: tokenID                        & 4950                     \\
Num. groups: Participant                    & 22                       \\
Var: tokenID (Intercept)                    & 308.69                   \\
Var: Participant (Intercept)                & 435.28                   \\
Var: Residual                               & 6119.38                  \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001,  &  $^{**}$p<0.01,  &  $^*p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
