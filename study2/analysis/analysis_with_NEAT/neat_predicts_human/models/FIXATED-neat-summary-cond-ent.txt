 Family: bernoulli 
  Links: mu = logit 
Formula: Fixated ~ IsNamedEntity.C * FixationProb.Resid + Condition.C * FixationProb.Resid + ExperimentTokenLength.Resid * LogWordFreq.C + ExperimentTokenLength.Resid + LogWordFreq.C + HumanPosition.C + Surprisal.Resid + (1 + IsNamedEntity.C * FixationProb.Resid + Condition.C * FixationProb.Resid + FixationProb.Resid + ExperimentTokenLength.Resid * LogWordFreq.C + ExperimentTokenLength.Resid + LogWordFreq.C + HumanPosition.C + Surprisal.Resid | Participant) + (1 | ID) 
   Data: joint (Number of observations: 90900) 
Samples: 4 chains, each with iter = 2000; warmup = 1000; thin = 1;
         total post-warmup samples = 4000

Group-Level Effects: 
~ID (Number of levels: 3759) 
              Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
sd(Intercept)     0.65      0.01     0.63     0.68       1867 1.00

~Participant (Number of levels: 24) 
                                                                                  Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
sd(Intercept)                                                                         0.74      0.12     0.53     1.00        500 1.01
sd(IsNamedEntity.C)                                                                   0.11      0.07     0.00     0.26       1036 1.00
sd(FixationProb.Resid)                                                                0.10      0.06     0.01     0.22        844 1.00
sd(Condition.C)                                                                       0.29      0.24     0.01     0.88        213 1.02
sd(ExperimentTokenLength.Resid)                                                       0.06      0.01     0.04     0.08       1894 1.00
sd(LogWordFreq.C)                                                                     0.19      0.03     0.14     0.25       1429 1.00
sd(HumanPosition.C)                                                                   0.21      0.04     0.15     0.29       1659 1.00
sd(Surprisal.Resid)                                                                   0.01      0.01     0.00     0.03       1115 1.00
sd(IsNamedEntity.C:FixationProb.Resid)                                                0.12      0.09     0.00     0.32       2094 1.00
sd(FixationProb.Resid:Condition.C)                                                    0.13      0.09     0.01     0.34       1506 1.00
sd(ExperimentTokenLength.Resid:LogWordFreq.C)                                         0.03      0.01     0.01     0.04       1584 1.00
cor(Intercept,IsNamedEntity.C)                                                        0.03      0.27    -0.50     0.54       4000 1.00
cor(Intercept,FixationProb.Resid)                                                    -0.09      0.25    -0.56     0.42       4000 1.00
cor(IsNamedEntity.C,FixationProb.Resid)                                               0.03      0.28    -0.52     0.56       2192 1.00
cor(Intercept,Condition.C)                                                           -0.10      0.28    -0.60     0.46       3105 1.00
cor(IsNamedEntity.C,Condition.C)                                                      0.00      0.28    -0.54     0.55       4000 1.00
cor(FixationProb.Resid,Condition.C)                                                  -0.00      0.29    -0.57     0.55       2807 1.00
cor(Intercept,ExperimentTokenLength.Resid)                                            0.66      0.13     0.37     0.86       1949 1.00
cor(IsNamedEntity.C,ExperimentTokenLength.Resid)                                      0.04      0.27    -0.47     0.53       1261 1.00
cor(FixationProb.Resid,ExperimentTokenLength.Resid)                                   0.05      0.26    -0.48     0.54        849 1.00
cor(Condition.C,ExperimentTokenLength.Resid)                                         -0.04      0.28    -0.55     0.51       1419 1.00
cor(Intercept,LogWordFreq.C)                                                         -0.50      0.15    -0.74    -0.16        778 1.00
cor(IsNamedEntity.C,LogWordFreq.C)                                                   -0.17      0.27    -0.65     0.38        716 1.00
cor(FixationProb.Resid,LogWordFreq.C)                                                -0.12      0.26    -0.60     0.41       1033 1.00
cor(Condition.C,LogWordFreq.C)                                                        0.03      0.28    -0.52     0.55       1012 1.00
cor(ExperimentTokenLength.Resid,LogWordFreq.C)                                       -0.50      0.15    -0.75    -0.17       1948 1.00
cor(Intercept,HumanPosition.C)                                                        0.27      0.17    -0.09     0.57       1354 1.00
cor(IsNamedEntity.C,HumanPosition.C)                                                  0.06      0.28    -0.49     0.60        478 1.00
cor(FixationProb.Resid,HumanPosition.C)                                               0.04      0.26    -0.45     0.53        691 1.00
cor(Condition.C,HumanPosition.C)                                                     -0.05      0.28    -0.58     0.49        705 1.01
cor(ExperimentTokenLength.Resid,HumanPosition.C)                                     -0.10      0.18    -0.44     0.24       2246 1.00
cor(LogWordFreq.C,HumanPosition.C)                                                   -0.10      0.17    -0.43     0.25       2186 1.00
cor(Intercept,Surprisal.Resid)                                                        0.12      0.23    -0.36     0.55       4000 1.00
cor(IsNamedEntity.C,Surprisal.Resid)                                                 -0.11      0.29    -0.64     0.46       1302 1.00
cor(FixationProb.Resid,Surprisal.Resid)                                              -0.12      0.27    -0.62     0.44       1965 1.00
cor(Condition.C,Surprisal.Resid)                                                     -0.04      0.28    -0.56     0.50       1328 1.00
cor(ExperimentTokenLength.Resid,Surprisal.Resid)                                     -0.06      0.23    -0.49     0.40       4000 1.00
cor(LogWordFreq.C,Surprisal.Resid)                                                    0.09      0.23    -0.37     0.52       4000 1.00
cor(HumanPosition.C,Surprisal.Resid)                                                  0.01      0.24    -0.46     0.46       4000 1.00
cor(Intercept,IsNamedEntity.C:FixationProb.Resid)                                     0.10      0.28    -0.47     0.61       4000 1.00
cor(IsNamedEntity.C,IsNamedEntity.C:FixationProb.Resid)                              -0.06      0.29    -0.61     0.49       4000 1.00
cor(FixationProb.Resid,IsNamedEntity.C:FixationProb.Resid)                            0.05      0.29    -0.52     0.61       4000 1.00
cor(Condition.C,IsNamedEntity.C:FixationProb.Resid)                                  -0.02      0.29    -0.55     0.52       4000 1.00
cor(ExperimentTokenLength.Resid,IsNamedEntity.C:FixationProb.Resid)                   0.04      0.28    -0.51     0.56       4000 1.00
cor(LogWordFreq.C,IsNamedEntity.C:FixationProb.Resid)                                -0.06      0.28    -0.59     0.49       4000 1.00
cor(HumanPosition.C,IsNamedEntity.C:FixationProb.Resid)                               0.06      0.29    -0.51     0.59       4000 1.00
cor(Surprisal.Resid,IsNamedEntity.C:FixationProb.Resid)                               0.01      0.28    -0.54     0.55       4000 1.00
cor(Intercept,FixationProb.Resid:Condition.C)                                        -0.02      0.27    -0.53     0.50       4000 1.00
cor(IsNamedEntity.C,FixationProb.Resid:Condition.C)                                   0.00      0.29    -0.55     0.56       4000 1.00
cor(FixationProb.Resid,FixationProb.Resid:Condition.C)                               -0.01      0.28    -0.54     0.51       4000 1.00
cor(Condition.C,FixationProb.Resid:Condition.C)                                      -0.03      0.28    -0.56     0.52       4000 1.00
cor(ExperimentTokenLength.Resid,FixationProb.Resid:Condition.C)                       0.04      0.27    -0.50     0.55       4000 1.00
cor(LogWordFreq.C,FixationProb.Resid:Condition.C)                                     0.05      0.27    -0.47     0.56       4000 1.00
cor(HumanPosition.C,FixationProb.Resid:Condition.C)                                  -0.08      0.28    -0.59     0.46       4000 1.00
cor(Surprisal.Resid,FixationProb.Resid:Condition.C)                                  -0.07      0.29    -0.62     0.49       3087 1.00
cor(IsNamedEntity.C:FixationProb.Resid,FixationProb.Resid:Condition.C)               -0.00      0.30    -0.56     0.58       2475 1.00
cor(Intercept,ExperimentTokenLength.Resid:LogWordFreq.C)                              0.08      0.21    -0.34     0.48       2788 1.00
cor(IsNamedEntity.C,ExperimentTokenLength.Resid:LogWordFreq.C)                       -0.09      0.28    -0.60     0.45       1420 1.00
cor(FixationProb.Resid,ExperimentTokenLength.Resid:LogWordFreq.C)                    -0.16      0.28    -0.66     0.43       1359 1.00
cor(Condition.C,ExperimentTokenLength.Resid:LogWordFreq.C)                            0.04      0.29    -0.51     0.58       1343 1.01
cor(ExperimentTokenLength.Resid,ExperimentTokenLength.Resid:LogWordFreq.C)            0.17      0.21    -0.28     0.55       4000 1.00
cor(LogWordFreq.C,ExperimentTokenLength.Resid:LogWordFreq.C)                          0.18      0.21    -0.24     0.57       4000 1.00
cor(HumanPosition.C,ExperimentTokenLength.Resid:LogWordFreq.C)                        0.03      0.22    -0.41     0.45       4000 1.00
cor(Surprisal.Resid,ExperimentTokenLength.Resid:LogWordFreq.C)                       -0.14      0.25    -0.62     0.38       2407 1.00
cor(IsNamedEntity.C:FixationProb.Resid,ExperimentTokenLength.Resid:LogWordFreq.C)     0.06      0.29    -0.50     0.60       2338 1.00
cor(FixationProb.Resid:Condition.C,ExperimentTokenLength.Resid:LogWordFreq.C)         0.02      0.28    -0.52     0.55       2416 1.00

Population-Level Effects: 
                                          Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
Intercept                                     0.24      0.41    -0.54     1.08        757 1.00
IsNamedEntity.C                               1.49      0.76     0.00     3.00        912 1.00
FixationProb.Resid                           -1.01      0.64    -2.30     0.22        922 1.00
Condition.C                                   0.53      0.24     0.07     1.01        674 1.00
ExperimentTokenLength.Resid                   0.28      0.02     0.25     0.31        805 1.00
LogWordFreq.C                                -0.63      0.04    -0.71    -0.55        883 1.01
HumanPosition.C                              -0.01      0.04    -0.10     0.08       1655 1.00
Surprisal.Resid                               0.01      0.01    -0.00     0.02       1410 1.00
IsNamedEntity.C:FixationProb.Resid           -2.18      1.28    -4.72     0.34        913 1.00
FixationProb.Resid:Condition.C               -0.06      0.09    -0.24     0.12       2632 1.00
ExperimentTokenLength.Resid:LogWordFreq.C     0.06      0.01     0.04     0.08       1690 1.00

Samples were drawn using sampling(NUTS). For each parameter, Eff.Sample 
is a crude measure of effective sample size, and Rhat is the potential 
scale reduction factor on split chains (at convergence, Rhat = 1).
