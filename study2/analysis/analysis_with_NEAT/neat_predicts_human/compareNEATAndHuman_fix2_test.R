library(tidyr)
library(dplyr)

mapping_surprisals = read.csv("../../experiment_data/processed_data//mappingSurprisals.tsv", sep="\t", quote="@")
mapping_surprisals$MappingLineNum = (1:nrow(mapping_surprisals))-1
joint = mapping_surprisals
human = read.csv("../../experiment_data/processed_data//mappingWithHuman.tsv", sep="\t")

humanWordFreq = read.csv("../../experiment_data/processed_data/mappingNonAnonymizedWordFreq.tsv", sep="\t", quote="^")

human = merge(human, humanWordFreq, by=c("TextFileName", "HumanPosition", "AnonymizedPosition"), all.x=TRUE)
print(mean(as.character(human$OriginalToken.x) == as.character(human$OriginalToken.y), na.rm=TRUE))
human$OriginalToken = human$OriginalToken.x
human$OriginalToken.x = NULL
human$OriginalToken.y = NULL


human$LogWordFreq = log(human$NonAnonymizedWordFreq+1)


# EXCLUDED PARTICIPANTS
human = human[!(human$Participant %in% c("N_3", "N_9")),]



joint = merge(human, joint, by=c("TextFileName", "HumanPosition", "AnonymizedPosition")) #, "OriginalToken"))

mean(as.character(joint$OriginalToken.x) == as.character(joint$OriginalToken.y), na.rm=TRUE)
joint$OriginalToken = joint$OriginalToken.y
joint$OriginalToken.x = NULL
joint$OriginalToken.y = NULL


library(lme4)

external = read.csv("../../experiment_data/processed_data/mappingWithExternal.tsv", sep="\t") %>% select(TextFileName, JointPosition, AnonymizedToken, ExperimentTokenLength, WordFreq, IsNamedEntity, IsCorrectAnswer) %>% mutate(WordFreq=NULL)
joint = merge(joint, external, by=c("TextFileName", "JointPosition", "AnonymizedToken"))



joint$tokenID = as.factor(paste0(joint$TextFileName, joint$JointPosition))

#joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal = NA
joint$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = joint, na.action=na.exclude))
joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal.Resid = 0
joint$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = joint, na.action=na.exclude))

library(brms)
joint = joint %>% filter(!is.na(fp))
joint$FIXATED = (joint$fp > 0)

########################################

model_ = read.csv("averaged-NEAT-predictions.tsv", sep="\t")

joint = merge(joint, model_ %>% mutate(TextFileName=Text, AnonymizedPosition = Position), by=c("Condition", "TextFileName", "AnonymizedPosition"), all.x=TRUE)



joint = joint %>% filter(!is.na(fp))
joint = joint[complete.cases(joint[,c("HumanPosition","Condition","IsCorrectAnswer","IsNamedEntity","Surprisal","ExperimentTokenLength","LogWordFreq","tokenID","Participant", "fp")]),]


#######################################


library(lme4)

# Predictors
# - LogWordFreq
# - WordLength
# - IsNamedEntity
# - PositionText
# - Surprisal

joint$FIXATED = (joint$fp > 0)+0.0

data = joint

# Centering
data = data %>% mutate(IsNamedEntity.C = (IsNamedEntity - mean(IsNamedEntity)))
data = data %>% mutate(LogWordFreq.C = (LogWordFreq - mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(HumanPosition.C = (HumanPosition - mean(HumanPosition))/sd(HumanPosition))
data = data %>% mutate(Condition.C = ifelse(Condition == "Preview", -0.5, 0.5))

# Residualizing surprisal & zeroing out OOV
data$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = data, na.action=na.exclude))
data[data$Surprisal_OOV_Status == "OOV",]$Surprisal.Resid = 0

# Residualizing word length
data$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = data, na.action=na.exclude))

# Residualizing answer status
data$IsCorrectAnswer.Resid = resid(lm(IsCorrectAnswer ~ IsNamedEntity, data = data, na.action=na.exclude))

data$FixationProb.C = (data$FixationProb - mean(data$FixationProb, na.rm=TRUE))

fullFormula_FIXATED <- readLines("modelSelection/selection/selected_FIXATED_slopes.txt")[1]

library(stringr)

fullFormula_FIXATED = str_replace(fullFormula_FIXATED, "\\|tokenID\\)", " + Condition.C + Condition.C*FixationProb.C|tokenID\\)")



# With CONDITION
modelFix = brm(formula(paste("FIXATED ~ Condition.C*FixationProb.C + " , fullFormula_FIXATED, sep="")), data=data, family="bernoulli", cores=4)

sink("models/FIXATED-neat-summary-cond_surp.txt")
print(summary(modelFix))
sink()

samples = posterior_samples(modelFix)


sink("models/FIXATED-Neat-cond_surp.txt")
print(summary(modelFix))
fitted = data.frame(fixef(modelFix, summary=FALSE))
cat("POSTERIOR\n")
for(p in names(fitted)) {
   cat(p, "\t", mean(fitted[[p]]), "\t", sd(fitted[[p]]), "\t", mean(fitted[[p]] < 0.0), "\n")
}
sink()




