
for x in ["FIXATED", "ff", "fp", "tt"]:
  formula = open(f"selection/selected_{x}.txt", "r").read().strip()
  print(formula)
  formula = [x for x in formula.split(" + ") if not x.startswith("(")]
  print(formula)
  withToken = ['HumanPosition.C', 'LogWordFreq.C', 'IsNamedEntity.C', 'IsCorrectAnswer.Resid', 'Surprisal.Resid', 'ExperimentTokenLength.Resid']
  withParticipant = ['Condition.dummy.C']
  slopesTokenID = [x for x in formula if all(y not in x for y in withToken)]
  slopesParticipant = [x for x in formula if all(y not in x for y in withParticipant)]
  print(slopesTokenID)
  print(slopesParticipant)
  
  with open(f"selection/selected_{x}_slopes.txt", "w") as outFile:
     print(" + ".join(formula) + " + " + ("(1 " + ("+" if len(slopesTokenID) > 0 else "") + " + ".join(slopesTokenID)+ "|tokenID)") + " + " + ("(1 " + ("+" if len(slopesParticipant) > 0 else "") + " + ".join(slopesParticipant)+ "+ FixationProb.C|Participant)"), file=outFile)
  
  
