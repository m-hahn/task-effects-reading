library(tidyr)
library(dplyr)

mapping_surprisals = read.csv("../../experiment_data/processed_data//mappingSurprisals.tsv", sep="\t", quote="@")
mapping_surprisals$MappingLineNum = (1:nrow(mapping_surprisals))-1
joint = mapping_surprisals
human = read.csv("../../experiment_data/processed_data//mappingWithHuman.tsv", sep="\t")

humanWordFreq = read.csv("../../experiment_data/processed_data/mappingNonAnonymizedWordFreq.tsv", sep="\t", quote="^")

human = merge(human, humanWordFreq, by=c("TextFileName", "HumanPosition", "AnonymizedPosition"), all.x=TRUE)
print(mean(as.character(human$OriginalToken.x) == as.character(human$OriginalToken.y), na.rm=TRUE))
human$OriginalToken = human$OriginalToken.x
human$OriginalToken.x = NULL
human$OriginalToken.y = NULL


human$LogWordFreq = log(human$NonAnonymizedWordFreq+1)


# EXCLUDED PARTICIPANTS
human = human[!(human$Participant %in% c("N_3", "N_9")),]



joint = merge(human, joint, by=c("TextFileName", "HumanPosition", "AnonymizedPosition")) #, "OriginalToken"))

mean(as.character(joint$OriginalToken.x) == as.character(joint$OriginalToken.y), na.rm=TRUE)
joint$OriginalToken = joint$OriginalToken.y
joint$OriginalToken.x = NULL
joint$OriginalToken.y = NULL


library(lme4)

external = read.csv("../../experiment_data/processed_data/mappingWithExternal.tsv", sep="\t") %>% select(TextFileName, JointPosition, AnonymizedToken, ExperimentTokenLength, WordFreq, IsNamedEntity, IsCorrectAnswer) %>% mutate(WordFreq=NULL) # this is the wrong WordFreq --> it is the anonymized one!
joint = merge(joint, external, by=c("TextFileName", "JointPosition", "AnonymizedToken"))



joint$tokenID = as.factor(paste0(joint$TextFileName, joint$JointPosition))

#joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal = NA
joint$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = joint, na.action=na.exclude))
joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal.Resid = 0
joint$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = joint, na.action=na.exclude))

library(brms)
joint = joint %>% filter(!is.na(fp))
joint$FIXATED = (joint$fp > 0)

########################################

model_ = read.csv("averaged-NEAT-predictions.tsv", sep="\t")

joint = merge(joint, model_ %>% mutate(TextFileName=Text, AnonymizedPosition = Position), by=c("Condition", "TextFileName", "AnonymizedPosition"), all.x=TRUE)



joint = joint %>% filter(!is.na(fp))
joint = joint[complete.cases(joint[,c("HumanPosition","Condition","IsCorrectAnswer","IsNamedEntity","Surprisal","ExperimentTokenLength","LogWordFreq","tokenID","Participant", "fp")]),]


#######################################


library(lme4)

# Predictors
# - LogWordFreq
# - WordLength
# - IsNamedEntity
# - PositionText
# - Surprisal


data = joint
dataN = data[data$tt > 0,]

# Centering
dataN = dataN %>% mutate(IsNamedEntity.C = (IsNamedEntity - mean(IsNamedEntity)))
dataN = dataN %>% mutate(LogWordFreq.C = (LogWordFreq - mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
dataN = dataN %>% mutate(HumanPosition.C = (HumanPosition - mean(HumanPosition))/sd(HumanPosition))
dataN = dataN %>% mutate(Condition.C = ifelse(Condition == "Preview", -0.5, 0.5))

# Residualizing surprisal & zeroing out OOV
dataN$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = dataN, na.action=na.exclude))
dataN[dataN$Surprisal_OOV_Status == "OOV",]$Surprisal.Resid = 0

# Residualizing word length
dataN$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = dataN, na.action=na.exclude))

# Residualizing answer status
dataN$IsCorrectAnswer.Resid = resid(lm(IsCorrectAnswer ~ IsNamedEntity, data = dataN, na.action=na.exclude))


dataN$FixationProb.Resid = resid(lm(FixationProb ~ LogWordFreq.C*ExperimentTokenLength.Resid + HumanPosition.C*LogWordFreq.C + HumanPosition.C*ExperimentTokenLength.Resid + Surprisal.Resid*ExperimentTokenLength.Resid + ExperimentTokenLength.Resid + LogWordFreq.C +  HumanPosition.C + Surprisal.Resid, data = dataN, na.action=na.exclude))
dataN[is.na(dataN$FixationProb.Resid),]$FixationProb.Resid = 0


fullFormula_ff <- readLines("modelSelection/selection/selected_ff.txt")[1]
fullFormula_fp <- readLines("modelSelection/selection/selected_fp.txt")[1]
fullFormula_tt <- readLines("modelSelection/selection/selected_tt.txt")[1]


library(lme4)
modelTT = (lmer(formula(paste("tt ~ ", fullFormula_tt, " + FixationProb.Resid", sep="")), data=dataN, REML=FALSE))
modelTTBare = (lmer(formula(paste("tt ~ ", fullFormula_tt, sep="")), data=dataN, REML=FALSE))
library(texreg)
source("prettyPrint.R")     
sink("models/tt-lmer-bare_inter.txt")        
print(renamePredictorsInStringOutput(texreg(modelTTBare ,single.row = TRUE)))       
sink()       
sink("models/tt-lmer-neat_inter.txt")        
print(renamePredictorsInStringOutput(texreg(modelTT ,single.row = TRUE)))   
sink()       
sink("models/comparison-tt_inter.txt")
cat(paste(anova(modelTT, modelTTBare)$AIC, sep="\t"))
cat("\n")
cat(paste(anova(modelTT, modelTTBare)$BIC, sep="\t"))
cat("\n")
cat(paste(anova(modelTT, modelTTBare)$deviance, sep="\t"))
cat("\n")
cat(paste(anova(modelTT, modelTTBare)$Chisq, sep="\t"))
cat("\n")
cat(paste(anova(modelTT, modelTTBare)$P, sep="\t"))
sink()

modelFF = (lmer(formula(paste("ff ~ ", fullFormula_ff, " + FixationProb.Resid", sep="")), data=dataN %>% filter(fp > 0), REML=FALSE))
modelFFBare = (lmer(formula(paste("ff ~ ", fullFormula_ff, sep="")), data=dataN %>% filter(fp > 0), REML=FALSE))
library(texreg)
source("prettyPrint.R")     
sink("models/ff-lmer-bare_inter.txt")        
print(renamePredictorsInStringOutput(texreg(modelFFBare ,single.row = TRUE)))       
sink()       
sink("models/ff-lmer-neat_inter.txt")        
print(renamePredictorsInStringOutput(texreg(modelFF ,single.row = TRUE)))   
sink()       
sink("models/comparison-ff_inter.txt")
cat(paste(anova(modelFF, modelFFBare)$AIC, sep="\t"))
cat("\n")
cat(paste(anova(modelFF, modelFFBare)$BIC, sep="\t"))
cat("\n")
cat(paste(anova(modelFF, modelFFBare)$deviance, sep="\t"))
cat("\n")
cat(paste(anova(modelFF, modelFFBare)$Chisq, sep="\t"))
cat("\n")
cat(paste(anova(modelFF, modelFFBare)$P, sep="\t"))
sink()

modelFP = (lmer(formula(paste("fp ~ ", fullFormula_fp, " + FixationProb.Resid", sep="")), data=dataN %>% filter(fp > 0), REML=FALSE))
modelFPBare = (lmer(formula(paste("fp ~ ", fullFormula_fp, sep="")), data=dataN %>% filter(fp > 0), REML=FALSE))
library(texreg)
source("prettyPrint.R")     
sink("models/fp-lmer-bare_inter.txt")        
print(renamePredictorsInStringOutput(texreg(modelFPBare ,single.row = TRUE)))       
sink()       
sink("models/fp-lmer-neat_inter.txt")        
print(renamePredictorsInStringOutput(texreg(modelFP ,single.row = TRUE)))   
sink()       
sink("models/comparison-fp_inter.txt")
cat(paste(anova(modelFP, modelFPBare)$AIC, sep="\t"))
cat("\n")
cat(paste(anova(modelFP, modelFPBare)$BIC, sep="\t"))
cat("\n")
cat(paste(anova(modelFP, modelFPBare)$deviance, sep="\t"))
cat("\n")
cat(paste(anova(modelFP, modelFPBare)$Chisq, sep="\t"))
cat("\n")
cat(paste(anova(modelFP, modelFPBare)$P, sep="\t"))
sink()



#data$IsCorrectAnswer.Resid = resid(lm(IsCorrectAnswer ~ IsNamedEntity.C, data = data, na.action=na.exclude))



