library(tidyr)
library(dplyr)
SCR = "~/scr/" #Sys.getenv("SCR")
surprisals = read.csv(paste(SCR,"/NEURAL_ATTENTION_TASK/NEAT_SURPRISAL_OUTPUT/run_lm_noised_texts_fullOrRandom.py_FULL_65591293", sep=""), sep="\t", quote="@") %>% rename(MappingLineNum=JointPosition)
mapping_surprisals = read.csv(paste(SCR,"/mappingSurprisals.tsv", sep=""), sep="\t", quote="@") %>% mutate(Surprisal=NULL, Surprisal_OOV_Status=NULL)
mapping_surprisals$MappingLineNum = (1:nrow(mapping_surprisals))-1
joint = merge(mapping_surprisals, surprisals, by=c("MappingLineNum", "TextFileName", "AnonymizedPosition", "HumanPosition"), all=TRUE)
human = read.csv(paste(SCR,"/mappingWithHuman.tsv", sep=""), sep="\t")
joint = merge(human, joint, by=c("TextFileName", "HumanPosition", "AnonymizedPosition", "Condition", "OriginalToken"))

library(lme4)

external = read.csv(paste(SCR,"/mappingWithExternal.tsv", sep=""), sep="\t") %>% select(TextFileName, JointPosition, AnonymizedToken, ExperimentTokenLength, WordFreq) %>% mutate(LogWordFreq=log(WordFreq))
joint = merge(joint, external, by=c("TextFileName", "JointPosition", "AnonymizedToken"))



joint$ID = paste0(joint$TextFileName, joint$JointPosition)

joint[joint$Surprisal_OOV_Status == "OOV",]$Surprisal = NA
joint$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = joint, na.action=na.exclude))
joint$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = joint, na.action=na.exclude))

library(brms)
joint = joint %>% filter(!is.na(fp))
joint$Fixated = (joint$fp > 0)

#modelFix = brm(Fixated ~ FixationProb + ExperimentTokenLength.Resid*LogWordFreq + HumanPosition*ExperimentTokenLength.Resid + HumanPosition*LogWordFreq + ExperimentTokenLength.Resid*Surprisal.Resid + ExperimentTokenLength.Resid + LogWordFreq +  HumanPosition + Surprisal.Resid + (1 + FixationProb + ExperimentTokenLength.Resid*LogWordFreq + HumanPosition*ExperimentTokenLength.Resid + HumanPosition*LogWordFreq + ExperimentTokenLength.Resid*Surprisal.Resid + ExperimentTokenLength.Resid + LogWordFreq +  HumanPosition + Surprisal.Resid|Participant) +(1|ID), data=joint, family="bernoulli", cores=4)
# 2000 iterations: 258477 seconds. But doesn't converge.


joint = joint %>% mutate(LogWordFreq.C = (LogWordFreq - mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
joint = joint %>% mutate(HumanPosition.C = (HumanPosition - mean(HumanPosition, na.rm=TRUE))/sd(HumanPosition, na.rm=TRUE))


jointN = joint %>% filter(Fixated, !is.na(Surprisal.Resid))

model0 = lmer(fp ~ ExperimentTokenLength.Resid*LogWordFreq.C + HumanPosition.C*ExperimentTokenLength.Resid + HumanPosition.C*LogWordFreq.C + ExperimentTokenLength.Resid + LogWordFreq.C +  HumanPosition.C  + (1|Participant) + (1|ID), data=jointN, REML=FALSE)
model1 = lmer(fp ~ ExperimentTokenLength.Resid*LogWordFreq.C + HumanPosition.C*ExperimentTokenLength.Resid + HumanPosition.C*LogWordFreq.C + ExperimentTokenLength.Resid + LogWordFreq.C +  HumanPosition.C + Surprisal.Resid + (1|Participant) + (1|ID), data=jointN, REML=FALSE)


anova(model0, model1)

