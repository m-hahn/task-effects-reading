
predictors = ["occursInQuestion","isCorrectAnswer","logWordFreq","position","isNamedEntity","wordLength","functionWord","surprisal"]

interactions = [["functionWord","position"],["isNamedEntity","position"],["wordLength","position"]] + map(lambda x:[x],predictors)
interactions += map(lambda x:x+["condition"], interactions)
interactions += [["condition"]]


print("fix[n] ~ bernoulli_logit( alpha + a[participant[n]] + b[item[n]] +  " + " + ".join(map(lambda i : "[n]*".join(interactions[i])+"[n]"+"*(beta{0})".format(i+1), range(0,len(interactions))))+");")

print("\n")
print("\n ".join(map(lambda i:" real<lower=-20,upper=20> beta{0};".format(i+1),range(0,len(interactions)))))

###############
###############
print("\n\n\n")
with open("tmp.txt","r") as stream:
  stanOutput = map(lambda x:x.split(" "),stream.read().rstrip().replace("  "," ").replace("  "," ").split("\n"))
print(stanOutput)
predictors = ["(Intercept)"] + map(lambda x:"*".join(map(lambda y:y.title(), x)), interactions)
assert(len(predictors) == len(stanOutput))

import numpy as np

def significance(low,high):
  low,high = map(float,(low,high))
  if (np.sign(low) == np.sign(high)):
     return ('%+d' % np.sign(low))[0]
  else:
     return ""

stanResults = map(lambda x:[x[0]]+[x[1][i] for i in [1,2,4,5]]+[significance(x[1][4],x[1][5])], zip(predictors,stanOutput))
header = ["Predictor","Mean","SD","0.025","0.975","Sig."]
assert(len(header) == len(stanResults[0]))
print("\\begin{tabular}{"+"l"*len(header)+"}")
print(" & ".join(header)+" \\\\ \\hline")
for result in stanResults:
  print((" & ".join(result)).replace("-","--")+" \\\\")
#          mean se_mean    sd   2.5%    50%  97.5% n_eff  Rhat
print("\\end{tabular}")
