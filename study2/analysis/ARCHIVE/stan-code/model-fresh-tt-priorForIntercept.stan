data {
 int<lower=1> N;
 real tt[N];
 int M;
 int L;
int<lower=1,upper=M> tokenID[N];
int<lower=1,upper=L> Participant[N];
vector<lower=-100,upper=100>[N] HumanPosition;
vector<lower=-100,upper=100>[N] OccursInQuestion;
vector<lower=-100,upper=100>[N] Condition;
vector<lower=-100,upper=100>[N] IsCorrectAnswer;
vector<lower=-100,upper=100>[N] IsNamedEntity;
vector<lower=-100,upper=100>[N] Surprisal;
vector<lower=-100,upper=100>[N] ExperimentTokenLength;
vector<lower=-100,upper=100>[N] LogWordFreq;
}
parameters {
real<lower=-1,upper=1> alpha;
vector[M] tokenIDIntercept;
vector[M]  tokenIDSlopes[18];
vector[L] ParticipantIntercept;
vector[L]  ParticipantSlopes[18];
 real<lower=-20,upper=20> beta1;
 real<lower=-20,upper=20> beta2;
 real<lower=-20,upper=20> beta3;
 real<lower=-20,upper=20> beta4;
 real<lower=-20,upper=20> beta5;
 real<lower=-20,upper=20> beta6;
 real<lower=-20,upper=20> beta7;
 real<lower=-20,upper=20> beta8;
 real<lower=-20,upper=20> beta9;
 real<lower=-20,upper=20> beta10;
 real<lower=-20,upper=20> beta11;
 real<lower=-20,upper=20> beta12;
 real<lower=-20,upper=20> beta13;
 real<lower=-20,upper=20> beta14;
 real<lower=-20,upper=20> beta15;
 real<lower=-20,upper=20> beta16;
 real<lower=-20,upper=20> beta17;
 real<lower=-20,upper=20> beta18;
 real<lower=0,upper=2> sigma1;
 real<lower=0,upper=2> sigma2;
 real<lower=0> sigma_e;
 vector<lower=0,upper=2>[18] sigmaSlope_tokenID;
 vector<lower=0,upper=2>[18] sigmaSlope_Participant;
}
model {
 vector[N] center;

 alpha ~ normal(0,10); #for the intercept, assuming that TT was log-transformed

  tokenIDIntercept  ~ normal(0,sigma1);
  ParticipantIntercept  ~ normal(0,sigma2);
  for (n in 1:18) {
     tokenIDSlopes[n] ~ normal(0,sigmaSlope_tokenID[n]);
     ParticipantSlopes[n] ~ normal(0,sigmaSlope_Participant[n]);
  }
 for (n in 1:N){
   int tokenIDForN;
   int ParticipantForN;
   tokenIDForN = tokenID[n];
   ParticipantForN = Participant[n];
   center[n] =  alpha +tokenIDIntercept[tokenIDForN] + ParticipantIntercept[ParticipantForN] + HumanPosition[n] * (beta1 + tokenIDSlopes[1][tokenIDForN] + ParticipantSlopes[1][ParticipantForN]) + OccursInQuestion[n] * (beta2 + tokenIDSlopes[2][tokenIDForN] + ParticipantSlopes[2][ParticipantForN]) + Condition[n] * (beta3 + tokenIDSlopes[3][tokenIDForN] + ParticipantSlopes[3][ParticipantForN]) + IsCorrectAnswer[n] * (beta4 + tokenIDSlopes[4][tokenIDForN] + ParticipantSlopes[4][ParticipantForN]) + IsNamedEntity[n] * (beta5 + tokenIDSlopes[5][tokenIDForN] + ParticipantSlopes[5][ParticipantForN]) + Surprisal[n] * (beta6 + tokenIDSlopes[6][tokenIDForN] + ParticipantSlopes[6][ParticipantForN]) + ExperimentTokenLength[n] * (beta7 + tokenIDSlopes[7][tokenIDForN] + ParticipantSlopes[7][ParticipantForN]) + LogWordFreq[n] * (beta8 + tokenIDSlopes[8][tokenIDForN] + ParticipantSlopes[8][ParticipantForN]) + Condition[n] * HumanPosition[n] * (beta9 + tokenIDSlopes[9][tokenIDForN] + ParticipantSlopes[9][ParticipantForN]) + Condition[n] * OccursInQuestion[n] * (beta10 + tokenIDSlopes[10][tokenIDForN] + ParticipantSlopes[10][ParticipantForN]) + Condition[n] * IsCorrectAnswer[n] * (beta11 + tokenIDSlopes[11][tokenIDForN] + ParticipantSlopes[11][ParticipantForN]) + Condition[n] * IsNamedEntity[n] * (beta12 + tokenIDSlopes[12][tokenIDForN] + ParticipantSlopes[12][ParticipantForN]) + Condition[n] * Surprisal[n] * (beta13 + tokenIDSlopes[13][tokenIDForN] + ParticipantSlopes[13][ParticipantForN]) + Condition[n] * ExperimentTokenLength[n] * (beta14 + tokenIDSlopes[14][tokenIDForN] + ParticipantSlopes[14][ParticipantForN]) + Condition[n] * LogWordFreq[n] * (beta15 + tokenIDSlopes[15][tokenIDForN] + ParticipantSlopes[15][ParticipantForN]) + HumanPosition[n] * ExperimentTokenLength[n] * (beta16 + tokenIDSlopes[16][tokenIDForN] + ParticipantSlopes[16][ParticipantForN]) + HumanPosition[n] * IsNamedEntity[n] * (beta17 + tokenIDSlopes[17][tokenIDForN] + ParticipantSlopes[17][ParticipantForN]) + HumanPosition[n] * LogWordFreq[n] * (beta18 + tokenIDSlopes[18][tokenIDForN] + ParticipantSlopes[18][ParticipantForN]);
 }
 tt ~ normal(center , sigma_e);

}


