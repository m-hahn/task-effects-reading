data {
 int<lower=1> N;
 int fix[N];
 int M;
 int L;
int<lower=1,upper=M> tokenID[N];
int<lower=1,upper=L> modelName[N];
vector<lower=-100,upper=100>[N] HumanPosition;
vector<lower=-100,upper=100>[N] OccursInQuestion;
vector<lower=-100,upper=100>[N] Condition;
vector<lower=-100,upper=100>[N] IsCorrectAnswer;
vector<lower=-100,upper=100>[N] IsNamedEntity;
vector<lower=-100,upper=100>[N] LogWordFreq;
}
parameters {
real alpha;
vector[M] tokenIDIntercept;
vector[M]  tokenIDSlopes[13];
vector[L] modelNameIntercept;
vector[L]  modelNameSlopes[13];
 real beta1;
 real beta2;
 real beta3;
 real beta4;
 real beta5;
 real beta6;
 real beta7;
 real beta8;
 real beta9;
 real beta10;
 real beta11;
 real beta12;
 real beta13;
 real<lower=0> sigma1;
 real<lower=0> sigma2;
 vector<lower=0>[13] sigmaSlope_tokenID;
 vector<lower=0>[13] sigmaSlope_modelName;
}
model {
  alpha ~ normal(0,1);

  tokenIDIntercept  ~ normal(0,sigma1);
  modelNameIntercept  ~ normal(0,sigma2);
  for (n in 1:13) {
     tokenIDSlopes[n] ~ normal(0,sigmaSlope_tokenID[n]);
     modelNameSlopes[n] ~ normal(0,sigmaSlope_modelName[n]);
  }
 for (n in 1:N){
   int tokenIDForN;
   int modelNameForN;
   tokenIDForN = tokenID[n];
   modelNameForN = modelName[n];
fix[n] ~ bernoulli_logit( alpha +tokenIDIntercept[tokenIDForN] + modelNameIntercept[modelNameForN] + HumanPosition[n] * (beta1 + tokenIDSlopes[1][tokenIDForN] + modelNameSlopes[1][modelNameForN]) + OccursInQuestion[n] * (beta2 + tokenIDSlopes[2][tokenIDForN] + modelNameSlopes[2][modelNameForN]) + Condition[n] * (beta3 + tokenIDSlopes[3][tokenIDForN] + modelNameSlopes[3][modelNameForN]) + IsCorrectAnswer[n] * (beta4 + tokenIDSlopes[4][tokenIDForN] + modelNameSlopes[4][modelNameForN]) + IsNamedEntity[n] * (beta5 + tokenIDSlopes[5][tokenIDForN] + modelNameSlopes[5][modelNameForN]) + LogWordFreq[n] * (beta6 + tokenIDSlopes[6][tokenIDForN] + modelNameSlopes[6][modelNameForN]) + Condition[n] * HumanPosition[n] * (beta7 + tokenIDSlopes[7][tokenIDForN] + modelNameSlopes[7][modelNameForN]) + Condition[n] * OccursInQuestion[n] * (beta8 + tokenIDSlopes[8][tokenIDForN] + modelNameSlopes[8][modelNameForN]) + Condition[n] * IsCorrectAnswer[n] * (beta9 + tokenIDSlopes[9][tokenIDForN] + modelNameSlopes[9][modelNameForN]) + Condition[n] * IsNamedEntity[n] * (beta10 + tokenIDSlopes[10][tokenIDForN] + modelNameSlopes[10][modelNameForN]) + Condition[n] * LogWordFreq[n] * (beta11 + tokenIDSlopes[11][tokenIDForN] + modelNameSlopes[11][modelNameForN]) + HumanPosition[n] * IsNamedEntity[n] * (beta12 + tokenIDSlopes[12][tokenIDForN] + modelNameSlopes[12][modelNameForN]) + HumanPosition[n] * LogWordFreq[n] * (beta13 + tokenIDSlopes[13][tokenIDForN] + modelNameSlopes[13][modelNameForN]));
 }
}

