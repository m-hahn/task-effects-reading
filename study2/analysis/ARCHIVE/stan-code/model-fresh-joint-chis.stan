data {
 int<lower=1> N;
 real ff[N];
 real fp[N];
 real tt[N];
// int fix[N];
 int M;
 int L;
 int<lower=1,upper=M> tokenID[N];
 int<lower=1,upper=L> Participant[N];
 vector<lower=-100,upper=100>[N] HumanPosition;
 vector<lower=-100,upper=100>[N] Condition;
 vector<lower=-100,upper=100>[N] IsCorrectAnswer;
 vector<lower=-100,upper=100>[N] IsNamedEntity;
 vector<lower=-100,upper=100>[N] Surprisal;
 vector<lower=-100,upper=100>[N] ExperimentTokenLength;
 vector<lower=-100,upper=100>[N] LogWordFreq;
}
parameters {
 matrix[6,M]  z_tokenID;
 matrix[15,L]  z_Participant_ff;
 matrix[15,L]  z_Participant_fp;
 matrix[15,L]  z_Participant_tt;

 real alpha_ff;
 real beta_ff1;
 real beta_ff2;
 real beta_ff3;
 real beta_ff4;
 real beta_ff5;
 real beta_ff6;
 real beta_ff7;
 real beta_ff8;
 real beta_ff9;
 real beta_ff10;
 real beta_ff11;
 real beta_ff12;
 real beta_ff13;
 real beta_ff14;
 real beta_ff15;
 real beta_ff16;
 real beta_ff17;
 real beta_ff18;
 real beta_ff19;
 real beta_ff20;
 real<lower=0> sigma_e_ff;

real alpha_tt;
 real beta_tt1;
 real beta_tt2;
 real beta_tt3;
 real beta_tt4;
 real beta_tt5;
 real beta_tt6;
 real beta_tt7;
 real beta_tt8;
 real beta_tt9;
 real beta_tt10;
 real beta_tt11;
 real beta_tt12;
 real beta_tt13;
 real beta_tt14;
 real beta_tt15;
 real beta_tt16;
 real beta_tt17;
 real beta_tt18;
 real beta_tt19;
 real beta_tt20;
 real<lower=0> sigma_e_tt;


real alpha_fp;
 real beta_fp1;
 real beta_fp2;
 real beta_fp3;
 real beta_fp4;
 real beta_fp5;
 real beta_fp6;
 real beta_fp7;
 real beta_fp8;
 real beta_fp9;
 real beta_fp10;
 real beta_fp11;
 real beta_fp12;
 real beta_fp13;
 real beta_fp14;
 real beta_fp15;
 real beta_fp16;
 real beta_fp17;
 real beta_fp18;
 real beta_fp19;
 real beta_fp20;
 real<lower=0> sigma_e_fp;


//real alpha_fix;
// real beta_fix1;
// real beta_fix2;
// real beta_fix3;
// real beta_fix4;
// real beta_fix5;
// real beta_fix6;
// real beta_fix7;
// real beta_fix8;
// real beta_fix9;
// real beta_fix10;
// real beta_fix11;
// real beta_fix12;
// real beta_fix13;
// real beta_fix14;
// real beta_fix15;
// real beta_fix16;
// real beta_fix17;
// real beta_fix18;
// real beta_fix19;
// real beta_fix20;
 vector<lower=0>[6] sigmaSlope_tokenID;
 vector<lower=0>[15] sigmaSlope_Participant_ff;
 vector<lower=0>[15] sigmaSlope_Participant_fp;
 vector<lower=0>[15] sigmaSlope_Participant_tt;

 cholesky_factor_corr[6] L_tokenID;
 cholesky_factor_corr[15] L_Participant_ff;
 cholesky_factor_corr[15] L_Participant_fp;
 cholesky_factor_corr[15] L_Participant_tt;

}
transformed parameters{
 matrix[6,M] for_tokenID;
 matrix[15,L] for_Participant_ff;
 matrix[15,L] for_Participant_fp;
 matrix[15,L] for_Participant_tt;

 for_tokenID = diag_pre_multiply(sigmaSlope_tokenID,L_tokenID) * z_tokenID;
 for_Participant_ff = diag_pre_multiply(sigmaSlope_Participant_ff,L_Participant_ff) * z_Participant_ff;
 for_Participant_fp = diag_pre_multiply(sigmaSlope_Participant_fp,L_Participant_fp) * z_Participant_fp;
 for_Participant_tt = diag_pre_multiply(sigmaSlope_Participant_tt,L_Participant_tt) * z_Participant_tt;

}
model {
  L_tokenID ~ lkj_corr_cholesky(2.0);
  L_Participant_ff ~ lkj_corr_cholesky(2.0);
  L_Participant_fp ~ lkj_corr_cholesky(2.0);
  L_Participant_tt ~ lkj_corr_cholesky(2.0);

  to_vector(z_tokenID) ~ normal(0,1);
  to_vector(z_Participant_ff) ~ normal(0,1);
  to_vector(z_Participant_fp) ~ normal(0,1);
  to_vector(z_Participant_tt) ~ normal(0,1);

 for (n in 1:N){
   int tokenIDForN;
   int ParticipantForN;
   tokenIDForN = tokenID[n];
   ParticipantForN = Participant[n];
ff[n] ~ normal( alpha_ff +for_tokenID[1,tokenIDForN] + for_Participant_ff[1,ParticipantForN] + HumanPosition[n] * (beta_ff1 + for_Participant_ff[2,ParticipantForN]) + Condition[n] * (beta_ff2 + for_tokenID[2,tokenIDForN]) + IsCorrectAnswer[n] * (beta_ff3 + for_Participant_ff[3,ParticipantForN]) + IsNamedEntity[n] * (beta_ff4 + for_Participant_ff[4,ParticipantForN]) + Surprisal[n] * (beta_ff5 + for_Participant_ff[5,ParticipantForN]) + ExperimentTokenLength[n] * (beta_ff6 + for_Participant_ff[6,ParticipantForN]) + LogWordFreq[n] * (beta_ff7 + for_Participant_ff[7,ParticipantForN]) + Condition[n] * LogWordFreq[n] * (beta_ff8) + HumanPosition[n] * ExperimentTokenLength[n] * (beta_ff9 + for_Participant_ff[8,ParticipantForN]) + Condition[n] * ExperimentTokenLength[n] * (beta_ff10) + Surprisal[n] * ExperimentTokenLength[n] * (beta_ff11 + for_Participant_ff[9,ParticipantForN]) + Condition[n] * IsCorrectAnswer[n] * (beta_ff12) + Condition[n] * IsNamedEntity[n] * (beta_ff13) + IsNamedEntity[n] * Surprisal[n] * (beta_ff14 + for_Participant_ff[10,ParticipantForN]) + HumanPosition[n] * LogWordFreq[n] * (beta_ff15 + for_Participant_ff[11,ParticipantForN]) + HumanPosition[n] * IsNamedEntity[n] * (beta_ff16 + for_Participant_ff[12,ParticipantForN]) + IsCorrectAnswer[n] * ExperimentTokenLength[n] * (beta_ff17 + for_Participant_ff[13,ParticipantForN]) + IsCorrectAnswer[n] * Surprisal[n] * (beta_ff18 + for_Participant_ff[14,ParticipantForN]) + HumanPosition[n] * Condition[n] * (beta_ff19) + LogWordFreq[n] * ExperimentTokenLength[n] * (beta_ff20 + for_Participant_ff[15,ParticipantForN]) , sigma_e_ff);

fp[n] ~ normal( alpha_fp +for_tokenID[3,tokenIDForN] + for_Participant_fp[1,ParticipantForN] + HumanPosition[n] * (beta_fp1 + for_Participant_fp[2,ParticipantForN]) + Condition[n] * (beta_fp2 + for_tokenID[4,tokenIDForN]) + IsCorrectAnswer[n] * (beta_fp3 + for_Participant_fp[3,ParticipantForN]) + IsNamedEntity[n] * (beta_fp4 + for_Participant_fp[4,ParticipantForN]) + Surprisal[n] * (beta_fp5 + for_Participant_fp[5,ParticipantForN]) + ExperimentTokenLength[n] * (beta_fp6 + for_Participant_fp[6,ParticipantForN]) + LogWordFreq[n] * (beta_fp7 + for_Participant_fp[7,ParticipantForN]) + Condition[n] * LogWordFreq[n] * (beta_fp8) + HumanPosition[n] * ExperimentTokenLength[n] * (beta_fp9 + for_Participant_fp[8,ParticipantForN]) + Condition[n] * ExperimentTokenLength[n] * (beta_fp10) + Surprisal[n] * ExperimentTokenLength[n] * (beta_fp11 + for_Participant_fp[9,ParticipantForN]) + Condition[n] * IsCorrectAnswer[n] * (beta_fp12) + Condition[n] * IsNamedEntity[n] * (beta_fp13) + IsNamedEntity[n] * Surprisal[n] * (beta_fp14 + for_Participant_fp[10,ParticipantForN]) + HumanPosition[n] * LogWordFreq[n] * (beta_fp15 + for_Participant_fp[11,ParticipantForN]) + HumanPosition[n] * IsNamedEntity[n] * (beta_fp16 + for_Participant_fp[12,ParticipantForN]) + IsCorrectAnswer[n] * ExperimentTokenLength[n] * (beta_fp17 + for_Participant_fp[13,ParticipantForN]) + IsCorrectAnswer[n] * Surprisal[n] * (beta_fp18 + for_Participant_fp[14,ParticipantForN]) + HumanPosition[n] * Condition[n] * (beta_fp19) + LogWordFreq[n] * ExperimentTokenLength[n] * (beta_fp20 + for_Participant_fp[15,ParticipantForN]) , sigma_e_fp);

tt[n] ~ normal( alpha_tt +for_tokenID[5,tokenIDForN] + for_Participant_tt[1,ParticipantForN] + HumanPosition[n] * (beta_tt1 + for_Participant_tt[2,ParticipantForN]) + Condition[n] * (beta_tt2 + for_tokenID[6,tokenIDForN]) + IsCorrectAnswer[n] * (beta_tt3 + for_Participant_tt[3,ParticipantForN]) + IsNamedEntity[n] * (beta_tt4 + for_Participant_tt[4,ParticipantForN]) + Surprisal[n] * (beta_tt5 + for_Participant_tt[5,ParticipantForN]) + ExperimentTokenLength[n] * (beta_tt6 + for_Participant_tt[6,ParticipantForN]) + LogWordFreq[n] * (beta_tt7 + for_Participant_tt[7,ParticipantForN]) + Condition[n] * LogWordFreq[n] * (beta_tt8) + HumanPosition[n] * ExperimentTokenLength[n] * (beta_tt9 + for_Participant_tt[8,ParticipantForN]) + Condition[n] * ExperimentTokenLength[n] * (beta_tt10) + Surprisal[n] * ExperimentTokenLength[n] * (beta_tt11 + for_Participant_tt[9,ParticipantForN]) + Condition[n] * IsCorrectAnswer[n] * (beta_tt12) + Condition[n] * IsNamedEntity[n] * (beta_tt13) + IsNamedEntity[n] * Surprisal[n] * (beta_tt14 + for_Participant_tt[10,ParticipantForN]) + HumanPosition[n] * LogWordFreq[n] * (beta_tt15 + for_Participant_tt[11,ParticipantForN]) + HumanPosition[n] * IsNamedEntity[n] * (beta_tt16 + for_Participant_tt[12,ParticipantForN]) + IsCorrectAnswer[n] * ExperimentTokenLength[n] * (beta_tt17 + for_Participant_tt[13,ParticipantForN]) + IsCorrectAnswer[n] * Surprisal[n] * (beta_tt18 + for_Participant_tt[14,ParticipantForN]) + HumanPosition[n] * Condition[n] * (beta_tt19) + LogWordFreq[n] * ExperimentTokenLength[n] * (beta_tt20 + for_Participant_tt[15,ParticipantForN]) , sigma_e_tt);

//fix[n] ~ bernoulli_logit( alpha_fix +for_tokenID[7,tokenIDForN] + for_Participant[46,ParticipantForN] + HumanPosition[n] * (beta_fix1 + for_Participant[47,ParticipantForN]) + Condition[n] * (beta_fix2 + for_tokenID[8,tokenIDForN]) + IsCorrectAnswer[n] * (beta_fix3 + for_Participant[48,ParticipantForN]) + IsNamedEntity[n] * (beta_fix4 + for_Participant[49,ParticipantForN]) + Surprisal[n] * (beta_fix5 + for_Participant[50,ParticipantForN]) + ExperimentTokenLength[n] * (beta_fix6 + for_Participant[51,ParticipantForN]) + LogWordFreq[n] * (beta_fix7 + for_Participant[52,ParticipantForN]) + Condition[n] * LogWordFreq[n] * (beta_fix8) + HumanPosition[n] * ExperimentTokenLength[n] * (beta_fix9 + for_Participant[53,ParticipantForN]) + Condition[n] * ExperimentTokenLength[n] * (beta_fix10) + Surprisal[n] * ExperimentTokenLength[n] * (beta_fix11 + for_Participant[54,ParticipantForN]) + Condition[n] * IsCorrectAnswer[n] * (beta_fix12) + Condition[n] * IsNamedEntity[n] * (beta_fix13) + IsNamedEntity[n] * Surprisal[n] * (beta_fix14 + for_Participant[55,ParticipantForN]) + HumanPosition[n] * LogWordFreq[n] * (beta_fix15 + for_Participant[56,ParticipantForN]) + HumanPosition[n] * IsNamedEntity[n] * (beta_fix16 + for_Participant[57,ParticipantForN]) + IsCorrectAnswer[n] * ExperimentTokenLength[n] * (beta_fix17 + for_Participant[58,ParticipantForN]) + IsCorrectAnswer[n] * Surprisal[n] * (beta_fix18 + for_Participant[59,ParticipantForN]) + HumanPosition[n] * Condition[n] * (beta_fix19) + LogWordFreq[n] * ExperimentTokenLength[n] * (beta_fix20 + for_Participant[60,ParticipantForN]));

 }
}
//generated quantities {
//  matrix[45,45] Omega_Participant;
//  matrix[45,45] Sigma_Participant;
//  matrix[6,6] Omega_tokenID;
//  matrix[6,6] Sigma_tokenID;
//
//  Omega_Participant <- multiply_lower_tri_self_transpose(L_Participant);
//  Sigma_Participant <- quad_form_diag(Omega_Participant, sigmaSlope_Participant);
//
//  Omega_tokenID <- multiply_lower_tri_self_transpose(L_tokenID);
//  Sigma_tokenID <- quad_form_diag(Omega_tokenID, sigmaSlope_tokenID);
//}

