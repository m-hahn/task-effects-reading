data {
 int<lower=1> N; //number of data points
 int fix[N]; //fixations
 int M; // participants
 int L; // items


 int<lower=1,upper=M> participant[N];
 vector<lower=-1,upper=1>[N] occursInQuestion;
 vector<lower=-1,upper=1>[N] condition;

 //vector[N] residOccurrencesOfCorrectAnswerSoFar;
 vector<lower=-1,upper=1>[N] isNamedEntity;
 vector<lower=-1,upper=1>[N] logWordFreq;
 vector<lower=-1,upper=1>[N] position;
 int<lower=1, upper=L> item[N];
 vector<lower=-3, upper=10>[N] wordLength;
 vector<lower=-2, upper=2>[N] functionWord;
 vector<lower=-2, upper=8>[N] surprisal;
 vector<lower=-2,upper=2>[N] isCorrectAnswer;
}
parameters {
 real<lower=-2,upper=2> alpha; // intercept
 vector[M] a; // per-participant intercepts
 vector[L] b; // per-item intercepts
// vector[M]  perParticipantSlopes[23]; //
// vector[L]  perItemSlopes[23]; // 

 real<lower=-20,upper=20> beta1;
 real<lower=-20,upper=20> beta2;
 real<lower=-20,upper=20> beta3;
 real<lower=-20,upper=20> beta4;
 real<lower=-20,upper=20> beta5;
 real<lower=-20,upper=20> beta6;
 real<lower=-20,upper=20> beta7;
 real<lower=-20,upper=20> beta8;
 real<lower=-20,upper=20> beta9;
 real<lower=-20,upper=20> beta10;
 real<lower=-20,upper=20> beta11;
 real<lower=-20,upper=20> beta12;
 real<lower=-20,upper=20> beta13;
  real<lower=-20,upper=20> beta14;
  real<lower=-20,upper=20> beta15;
  real<lower=-20,upper=20> beta16;
  real<lower=-20,upper=20> beta17;
  real<lower=-20,upper=20> beta18;
  real<lower=-20,upper=20> beta19;
  real<lower=-20,upper=20> beta20;
  real<lower=-20,upper=20> beta21;
  real<lower=-20,upper=20> beta22;
  real<lower=-20,upper=20> beta23;



 real<lower=0,upper=1> sigma1;
 real<lower=0, upper=1> sigma2;
// vector<lower=0,upper=1>[23] sigmaSlope;
 //vector<lower=0,upper=1>[23] sigmaSlopeForItems;

}
model {


  a ~ normal(0,sigma1);
  b ~ normal(0,sigma2);
  //for (n in 1:23) {
  //   perParticipantSlopes[n] ~ normal(0,sigmaSlope[n]);
  //   perItemSlopes[n] ~ normal(0,sigmaSlopeForItems[n]);
  //}



 for (n in 1:N){ // likelihood
   int itemForN;
   int participantForN;
   participantForN = participant[n];
   itemForN = item[n];


fix[n] ~ bernoulli_logit( alpha + a[participant[n]] + b[item[n]] +  functionWord[n]*position[n]*(beta1) + isNamedEntity[n]*position[n]*(beta2) + wordLength[n]*position[n]*(beta3) + occursInQuestion[n]*(beta4) + isCorrectAnswer[n]*(beta5) + logWordFreq[n]*(beta6) + position[n]*(beta7) + isNamedEntity[n]*(beta8) + wordLength[n]*(beta9) + functionWord[n]*(beta10) + surprisal[n]*(beta11) + functionWord[n]*position[n]*condition[n]*(beta12) + isNamedEntity[n]*position[n]*condition[n]*(beta13) + wordLength[n]*position[n]*condition[n]*(beta14) + occursInQuestion[n]*condition[n]*(beta15) + isCorrectAnswer[n]*condition[n]*(beta16) + logWordFreq[n]*condition[n]*(beta17) + position[n]*condition[n]*(beta18) + isNamedEntity[n]*condition[n]*(beta19) + wordLength[n]*condition[n]*(beta20) + functionWord[n]*condition[n]*(beta21) + surprisal[n]*condition[n]*(beta22) + condition[n]*(beta23));



 }
}

