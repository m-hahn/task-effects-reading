data {
 int<lower=1> N;
 real ff[N];
 int M;
 int L;
int<lower=1,upper=M> tokenID[N];
int<lower=1,upper=L> Participant[N];
vector<lower=-100,upper=100>[N] HumanPosition;
vector<lower=-100,upper=100>[N] Condition;
vector<lower=-100,upper=100>[N] IsCorrectAnswer;
vector<lower=-100,upper=100>[N] IsNamedEntity;
vector<lower=-100,upper=100>[N] Surprisal;
vector<lower=-100,upper=100>[N] ExperimentTokenLength;
vector<lower=-100,upper=100>[N] LogWordFreq;
}
parameters {
real alpha;
matrix[2,M]  z_tokenID;
matrix[22,L]  z_Participant;
 real beta1;
 real beta2;
 real beta3;
 real beta4;
 real beta5;
 real beta6;
 real beta7;
 real beta8;
 real beta9;
 real beta10;
 real beta11;
 real beta12;
 real beta13;
 real beta14;
 real beta15;
 real beta16;
 real beta17;
 real beta18;
 real beta19;
 real beta20;
 real beta21;
 real beta22;
 real beta23;
 real beta24;
 real beta25;
 real beta26;
 real beta27;
 real beta28;
 real<lower=0> sigma_e;
 vector<lower=0>[2] sigmaSlope_tokenID;
 vector<lower=0>[22] sigmaSlope_Participant;
 cholesky_factor_corr[2] L_tokenID;
 cholesky_factor_corr[22] L_Participant;
}
transformed parameters{
 matrix[2,M] for_tokenID;
 matrix[22,L] for_Participant;
 for_tokenID = diag_pre_multiply(sigmaSlope_tokenID,L_tokenID) * z_tokenID;
 for_Participant = diag_pre_multiply(sigmaSlope_Participant,L_Participant) * z_Participant;
}
model {
  L_tokenID ~ lkj_corr_cholesky(2.0);
  L_Participant ~ lkj_corr_cholesky(2.0);
  to_vector(z_tokenID) ~ normal(0,1);
  to_vector(z_Participant) ~ normal(0,1);
 for (n in 1:N){
   int tokenIDForN;
   int ParticipantForN;
   tokenIDForN = tokenID[n];
   ParticipantForN = Participant[n];
ff[n] ~ normal( alpha +for_tokenID[1,tokenIDForN] + for_Participant[1,ParticipantForN] + HumanPosition[n] * (beta1 + for_Participant[2,ParticipantForN]) + Condition[n] * (beta2 + for_tokenID[2,tokenIDForN]) + IsCorrectAnswer[n] * (beta3 + for_Participant[3,ParticipantForN]) + IsNamedEntity[n] * (beta4 + for_Participant[4,ParticipantForN]) + Surprisal[n] * (beta5 + for_Participant[5,ParticipantForN]) + ExperimentTokenLength[n] * (beta6 + for_Participant[6,ParticipantForN]) + LogWordFreq[n] * (beta7 + for_Participant[7,ParticipantForN]) + HumanPosition[n] * Condition[n] * (beta8) + HumanPosition[n] * IsCorrectAnswer[n] * (beta9 + for_Participant[8,ParticipantForN]) + HumanPosition[n] * IsNamedEntity[n] * (beta10 + for_Participant[9,ParticipantForN]) + HumanPosition[n] * Surprisal[n] * (beta11 + for_Participant[10,ParticipantForN]) + HumanPosition[n] * ExperimentTokenLength[n] * (beta12 + for_Participant[11,ParticipantForN]) + HumanPosition[n] * LogWordFreq[n] * (beta13 + for_Participant[12,ParticipantForN]) + Condition[n] * IsCorrectAnswer[n] * (beta14) + Condition[n] * IsNamedEntity[n] * (beta15) + Condition[n] * Surprisal[n] * (beta16) + Condition[n] * ExperimentTokenLength[n] * (beta17) + Condition[n] * LogWordFreq[n] * (beta18) + IsCorrectAnswer[n] * IsNamedEntity[n] * (beta19 + for_Participant[13,ParticipantForN]) + IsCorrectAnswer[n] * Surprisal[n] * (beta20 + for_Participant[14,ParticipantForN]) + IsCorrectAnswer[n] * ExperimentTokenLength[n] * (beta21 + for_Participant[15,ParticipantForN]) + IsCorrectAnswer[n] * LogWordFreq[n] * (beta22 + for_Participant[16,ParticipantForN]) + IsNamedEntity[n] * Surprisal[n] * (beta23 + for_Participant[17,ParticipantForN]) + IsNamedEntity[n] * ExperimentTokenLength[n] * (beta24 + for_Participant[18,ParticipantForN]) + IsNamedEntity[n] * LogWordFreq[n] * (beta25 + for_Participant[19,ParticipantForN]) + Surprisal[n] * ExperimentTokenLength[n] * (beta26 + for_Participant[20,ParticipantForN]) + Surprisal[n] * LogWordFreq[n] * (beta27 + for_Participant[21,ParticipantForN]) + ExperimentTokenLength[n] * LogWordFreq[n] * (beta28 + for_Participant[22,ParticipantForN]) , sigma_e);
 }
}

