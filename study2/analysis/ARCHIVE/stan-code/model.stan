data {
 int<lower=1> N; //number of data points
 int fix[N]; //fixations
 int M; // participants
 int L; // items


 int<lower=1,upper=M> participant[N];
 vector<lower=-1,upper=1>[N] occursInQuestion;
 vector<lower=-1,upper=1>[N] condition;
 int<lower=1, upper=L> item[N];
}

//transformed data {           // interaction
//  vector[N] inter;
//  inter <- mom_hs .* mom_iq;
//}

parameters {
 real<lower=-2,upper=2> alpha; // intercept
 real<lower=-2,upper=2> a[M]; // per-participant intercepts
 real<lower=-2,upper=2> b[L]; // per-item intercepts
 real<lower=-3,upper=3> beta1;
 real<lower=-20,upper=20> beta2;
 real<lower=-20,upper=20> beta3;
 real<lower=0,upper=1> sigma1;
 real<lower=0,upper=1> sigma2;
}
model {
  vector[N] bPerItem;
  vector[N] aPerItem;


  alpha ~ normal(0,1); 
  a ~ normal(0,sigma1);
  b ~ normal(0,sigma2);
  beta1 ~ normal(0,1);
  beta2 ~ normal(0,1);
  beta3 ~ normal(0,1);

  for (n in 1:N) {
    bPerItem[n] = b[item[n]];
    aPerItem[n] = a[participant[n]];
  }

  
  fix ~ bernoulli(inv_logit( alpha + bPerItem + aPerItem + (occursInQuestion .* condition) * beta1 + occursInQuestion*beta2 + condition*beta3));

}

