data {
 int<lower=1> N;
 int fix[N];
 int L;
int<lower=1,upper=L> modelName[N];
vector<lower=-100,upper=100>[N] HumanPosition;
vector<lower=-100,upper=100>[N] OccursInQuestion;
vector<lower=-100,upper=100>[N] Condition;
vector<lower=-100,upper=100>[N] IsCorrectAnswer;
vector<lower=-100,upper=100>[N] IsNamedEntity;
vector<lower=-100,upper=100>[N] LogWordFreq;
}
parameters {
real alpha;
matrix[14,L]  z_modelName;
 real beta1;
 real beta2;
 real beta3;
 real beta4;
 real beta5;
 real beta6;
 real beta7;
 real beta8;
 real beta9;
 real beta10;
 real beta11;
 real beta12;
 real beta13;
 vector<lower=0>[14] sigmaSlope_modelName;
 cholesky_factor_corr[14] L_modelName;
}
transformed parameters{
 matrix[14,L] for_modelName;
 for_modelName = diag_pre_multiply(sigmaSlope_modelName,L_modelName) * z_modelName;
}
model {
  L_modelName ~ lkj_corr_cholesky(2.0);
  to_vector(z_modelName) ~ normal(0,1);
 for (n in 1:N){
   int modelNameForN;
   modelNameForN = modelName[n];
fix[n] ~ bernoulli_logit( alpha +for_modelName[1,modelNameForN] + HumanPosition[n] * (beta1 + for_modelName[2,modelNameForN]) + OccursInQuestion[n] * (beta2 + for_modelName[3,modelNameForN]) + Condition[n] * (beta3 + for_modelName[4,modelNameForN]) + IsCorrectAnswer[n] * (beta4 + for_modelName[5,modelNameForN]) + IsNamedEntity[n] * (beta5 + for_modelName[6,modelNameForN]) + LogWordFreq[n] * (beta6 + for_modelName[7,modelNameForN]) + Condition[n] * HumanPosition[n] * (beta7 + for_modelName[8,modelNameForN]) + Condition[n] * OccursInQuestion[n] * (beta8 + for_modelName[9,modelNameForN]) + Condition[n] * IsCorrectAnswer[n] * (beta9 + for_modelName[10,modelNameForN]) + Condition[n] * IsNamedEntity[n] * (beta10 + for_modelName[11,modelNameForN]) + Condition[n] * LogWordFreq[n] * (beta11 + for_modelName[12,modelNameForN]) + HumanPosition[n] * IsNamedEntity[n] * (beta12 + for_modelName[13,modelNameForN]) + HumanPosition[n] * LogWordFreq[n] * (beta13 + for_modelName[14,modelNameForN]));
 }
}

