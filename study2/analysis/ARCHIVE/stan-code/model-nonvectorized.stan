data {
 int<lower=1> N; //number of data points
 int fix[N]; //fixations
 //int M; // participants
 int L; // items


 //int<lower=1,upper=M> participant[N];
 real<lower=-1,upper=1> occursInQuestion[N];
 real<lower=-1,upper=1> condition[N];
 int<lower=1, upper=L> item[N];
}
parameters {
 real<lower=-2,upper=2> alpha; // intercept
 //real<lower=-2,upper=2> a[M]; // per-participant intercepts
 real<lower=-2,upper=2> b[L]; // per-item intercepts
 real<lower=-3,upper=3> beta1;
 real<lower=-20,upper=20> beta2;
 real<lower=-20,upper=20> beta3;
 real<lower=0,upper=1> sigma1;
 real<lower=0,upper=1> sigma2;
}
model {
  alpha ~ normal(0,1); 
//  a ~ normal(0,sigma1);
  b ~ normal(0,sigma2);
  beta1 ~ normal(0,1);
  beta2 ~ normal(0,1);
  beta3 ~ normal(0,1);

 for (n in 1:N){ // likelihood

  fix[n] ~ bernoulli(inv_logit( alpha + b[item[n]] +  occursInQuestion[n]*condition[n]*beta1 + occursInQuestion[n]*beta2 + condition[n]*beta3));

// a[participant[n]] +

 }
}

