data {
 int<lower=1> N;
 real tt[N];
 int M;
 int L;
int<lower=1,upper=M> tokenID[N];
int<lower=1,upper=L> Participant[N];
vector<lower=-100,upper=100>[N] HumanPosition;
vector<lower=-100,upper=100>[N] Condition;
vector<lower=-100,upper=100>[N] IsCorrectAnswer;
vector<lower=-100,upper=100>[N] IsNamedEntity;
vector<lower=-100,upper=100>[N] Surprisal;
vector<lower=-100,upper=100>[N] ExperimentTokenLength;
vector<lower=-100,upper=100>[N] LogWordFreq;
}
parameters {
real alpha;
matrix[2,M]  z_tokenID;
matrix[15,L]  z_Participant;
 real beta1;
 real beta2;
 real beta3;
 real beta4;
 real beta5;
 real beta6;
 real beta7;
 real beta8;
 real beta9;
 real beta10;
 real beta11;
 real beta12;
 real beta13;
 real beta14;
 real beta15;
 real beta16;
 real beta17;
 real beta18;
 real beta19;
 real beta20;
 real<lower=0> sigma_e;
 vector<lower=0>[2] sigmaSlope_tokenID;
 vector<lower=0>[15] sigmaSlope_Participant;
 cholesky_factor_corr[2] L_tokenID;
 cholesky_factor_corr[15] L_Participant;
}
transformed parameters{
 matrix[2,M] for_tokenID;
 matrix[15,L] for_Participant;
 for_tokenID = diag_pre_multiply(sigmaSlope_tokenID,L_tokenID) * z_tokenID;
 for_Participant = diag_pre_multiply(sigmaSlope_Participant,L_Participant) * z_Participant;
}
model {
  L_tokenID ~ lkj_corr_cholesky(2.0);
  L_Participant ~ lkj_corr_cholesky(2.0);
  to_vector(z_tokenID) ~ normal(0,1);
  to_vector(z_Participant) ~ normal(0,1);
 for (n in 1:N) {
   int tokenIDForN;
   int ParticipantForN;
   real gamma_mean;
   real alpha_par;
   tokenIDForN = tokenID[n];
   ParticipantForN = Participant[n];
   gamma_mean = alpha +for_tokenID[1,tokenIDForN] + for_Participant[1,ParticipantForN] + HumanPosition[n] * (beta1 + for_Participant[2,ParticipantForN]) + Condition[n] * (beta2 + for_tokenID[2,tokenIDForN]) + IsCorrectAnswer[n] * (beta3 + for_Participant[3,ParticipantForN]) + IsNamedEntity[n] * (beta4 + for_Participant[4,ParticipantForN]) + Surprisal[n] * (beta5 + for_Participant[5,ParticipantForN]) + ExperimentTokenLength[n] * (beta6 + for_Participant[6,ParticipantForN]) + LogWordFreq[n] * (beta7 + for_Participant[7,ParticipantForN]) + Condition[n] * LogWordFreq[n] * (beta8) + HumanPosition[n] * ExperimentTokenLength[n] * (beta9 + for_Participant[8,ParticipantForN]) + Condition[n] * ExperimentTokenLength[n] * (beta10) + Surprisal[n] * ExperimentTokenLength[n] * (beta11 + for_Participant[9,ParticipantForN]) + Condition[n] * IsCorrectAnswer[n] * (beta12) + Condition[n] * IsNamedEntity[n] * (beta13) + IsNamedEntity[n] * Surprisal[n] * (beta14 + for_Participant[10,ParticipantForN]) + HumanPosition[n] * LogWordFreq[n] * (beta15 + for_Participant[11,ParticipantForN]) + HumanPosition[n] * IsNamedEntity[n] * (beta16 + for_Participant[12,ParticipantForN]) + IsCorrectAnswer[n] * ExperimentTokenLength[n] * (beta17 + for_Participant[13,ParticipantForN]) + IsCorrectAnswer[n] * Surprisal[n] * (beta18 + for_Participant[14,ParticipantForN]) + HumanPosition[n] * Condition[n] * (beta19) + LogWordFreq[n] * ExperimentTokenLength[n] * (beta20 + for_Participant[15,ParticipantForN]);
   //tt[n] ~ normal( gamma_mean , sigma_e);
   alpha_par = fmax(10.0,gamma_mean) * sigma_e;
   tt[n] ~ gamma(alpha_par, sigma_e);
 }
}

