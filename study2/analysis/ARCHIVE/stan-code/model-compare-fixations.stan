data {
 int<lower=1> N;
 int fix[N];
 int M;
 int L;
int<lower=1,upper=M> tokenID[N];
int<lower=1,upper=L> Participant[N];
vector<lower=-100,upper=100>[N] ModelAttention;
vector<lower=-100,upper=100>[N] HumanPosition;
vector<lower=-100,upper=100>[N] IsNamedEntity;
vector<lower=-100,upper=100>[N] Surprisal;
vector<lower=-100,upper=100>[N] ExperimentTokenLength;
vector<lower=-100,upper=100>[N] LogWordFreq;
}
parameters {
real alpha;
matrix[2,M]  z_tokenID;
matrix[6,L]  z_Participant;
 real beta1;
 real beta2;
 real beta3;
 real beta4;
 real beta5;
 real beta6;
 vector<lower=0>[2] sigmaSlope_tokenID;
 vector<lower=0>[6] sigmaSlope_Participant;
 cholesky_factor_corr[2] L_tokenID;
 cholesky_factor_corr[6] L_Participant;
}
transformed parameters{
 matrix[2,M] for_tokenID;
 matrix[6,L] for_Participant;
 for_tokenID = diag_pre_multiply(sigmaSlope_tokenID,L_tokenID) * z_tokenID;
 for_Participant = diag_pre_multiply(sigmaSlope_Participant,L_Participant) * z_Participant;
}
model {
  L_tokenID ~ lkj_corr_cholesky(2.0);
  L_Participant ~ lkj_corr_cholesky(2.0);
  to_vector(z_tokenID) ~ normal(0,1);
  to_vector(z_Participant) ~ normal(0,1);
 for (n in 1:N){
   int tokenIDForN;
   int ParticipantForN;
   tokenIDForN = tokenID[n];
   ParticipantForN = Participant[n];
fix[n] ~ bernoulli_logit( alpha +for_tokenID[1,tokenIDForN] + for_Participant[1,ParticipantForN] + ModelAttention[n] * (beta1 + for_tokenID[2,tokenIDForN]) + HumanPosition[n] * (beta2 + for_Participant[2,ParticipantForN]) + IsNamedEntity[n] * (beta3 + for_Participant[3,ParticipantForN]) + Surprisal[n] * (beta4 + for_Participant[4,ParticipantForN]) + ExperimentTokenLength[n] * (beta5 + for_Participant[5,ParticipantForN]) + LogWordFreq[n] * (beta6 + for_Participant[6,ParticipantForN]));
 }
}

