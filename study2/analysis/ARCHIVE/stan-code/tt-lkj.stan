data {
 int<lower=1> N;
 real tt[N];
 int M;
 int L;
int<lower=1,upper=M> tokenID[N];
int<lower=1,upper=L> Participant[N];
vector<lower=-100,upper=100>[N] HumanPosition;
vector<lower=-100,upper=100>[N] OccursInQuestion;
vector<lower=-100,upper=100>[N] Condition;
vector<lower=-100,upper=100>[N] IsCorrectAnswer;
vector<lower=-100,upper=100>[N] IsNamedEntity;
vector<lower=-100,upper=100>[N] Surprisal;
vector<lower=-100,upper=100>[N] ExperimentTokenLength;
vector<lower=-100,upper=100>[N] LogWordFreq;
}
parameters {
real alpha;
//vector[M] tokenIDIntercept;
//vector[M]  tokenIDSlopes[1];
//vector[L] ParticipantIntercept;
//vector[L]  ParticipantSlopes[10];
// real beta1;
// real beta2;
// real beta3;
// real beta4;
 //real beta5;
 //real beta6;
 //real beta7;
 //real beta8;
 //real beta9;
 //real beta10;
 //real beta11;
 //real beta12;
 //real beta13;
 //real beta14;
 //real beta15;
 //real beta16;
 //real beta17;
 //real beta18;


real<lower=0> sigma_e;

 vector<lower=0>[2] sigmaSlope_tokenID;
 vector<lower=0>[11] sigmaSlope_Participant;

//cholesky_factor_corr[2] L_tokenID;
//cholesky_factor_corr[11] L_Participant;


matrix[2,M] z_tokenID;
matrix[11,L] z_Participant;

// real<lower=0> sigma1;
// real<lower=0> sigma2;

}

transformed parameters{
}

model {
 real mu[N];
 matrix[2,M] for_tokenID;
 matrix[11,L] for_Participant;

//sigma_e ~ normal(0,8);
//sigmaSlope_tokenID ~ normal(0,8);
//sigmaSlope_Participant ~ normal(0,8);
//  L_tokenID ~ lkj_corr_cholesky(2.0);
//  L_Participant ~ lkj_corr_cholesky(2.0);
//print("L_tokenID", L_tokenID);
//print("L_Participant", L_Participant);
//  to_vector(z_tokenID) ~ normal(0,1);
//  to_vector(z_Participant) ~ normal(0,1);
// for_tokenID = diag_pre_multiply(sigmaSlope_tokenID,L_tokenID)*z_tokenID;
// for_Participant = diag_pre_multiply(sigmaSlope_Participant,L_Participant) * z_Participant;

  to_vector(z_tokenID) ~ normal(0,sigmaSlope_tokenID);
  to_vector(z_Participant) ~ normal(0,sigmaSlope_Participant);


print("z_tokenID", z_tokenID);
print("z_Participant", z_Participant);

 for_tokenID = z_tokenID;
 for_Participant = z_Participant;



print("for_tokenID", for_tokenID);
print("for_Participant", for_Participant);

 for (n in 1:N){
   int tokenIDForN;
   int ParticipantForN;
   tokenIDForN = tokenID[n];
   ParticipantForN = Participant[n];
   mu[n] =  alpha + for_tokenID[1,tokenIDForN] + for_Participant[1,ParticipantForN]; // + HumanPosition[n] * (beta1 + for_Participant[2,ParticipantForN]) + OccursInQuestion[n] * (beta2 + for_Participant[3,ParticipantForN]) + Condition[n] * (beta3 + for_tokenID[2,tokenIDForN]) + IsCorrectAnswer[n] * (beta4 + for_Participant[4,ParticipantForN]) + IsNamedEntity[n] * (beta5 + for_Participant[5,ParticipantForN]) + Surprisal[n] * (beta6 + for_Participant[6,ParticipantForN]) + ExperimentTokenLength[n] * (beta7 + for_Participant[7,ParticipantForN]) + LogWordFreq[n] * (beta8 + for_Participant[8,ParticipantForN]) + Condition[n] * HumanPosition[n] * (beta9) + Condition[n] * OccursInQuestion[n] * (beta10) + Condition[n] * IsCorrectAnswer[n] * (beta11) + Condition[n] * IsNamedEntity[n] * (beta12) + Condition[n] * Surprisal[n] * (beta13) + Condition[n] * ExperimentTokenLength[n] * (beta14) + Condition[n] * LogWordFreq[n] * (beta15) + HumanPosition[n] * ExperimentTokenLength[n] * (beta16 + for_Participant[9,ParticipantForN]) + HumanPosition[n] * IsNamedEntity[n] * (beta17 + for_Participant[10,ParticipantForN]) + HumanPosition[n] * LogWordFreq[n] * (beta18 + for_Participant[11,ParticipantForN]);
 }
tt ~ normal(mu,sigma_e);
}


