DO_LOGISTIC = FALSE #TRUE
DO_LINEAR = TRUE


RENAME_PARTICIPANT_NAMES = TRUE

models = list("full"="Full", "s" = "No Surprisal", "rs" = "No Context", "ir" = "Only Surprisal")


cat("(1) supposedly participant 17 (NPR) stopped after half the trials? but which participant does this map on? which participants in the fp/tt files do the numbers in the subject notes map on. is this participant 9 from the NPR file (who has many zeros)", sep="\n")
cat("(2) for participant NP_9, file 17 is missing. why did this happen?", sep="\n")
cat("(3) In any case, the mapping from the original 24 numbers to the two groups of 12 numbers appears to be correct.", sep="\n")
cat("(4) Please exclude the nonnative participants in R", sep="\n")
cat('(5) data = read.csv("~/scr/NEURAL_ATTENTION_TASK/ed1516/data-dev.csv")', sep="\n")
cat('(5) dataN = data[data$fp>0,]', sep="\n")
cat('data$LogWordFreq = log(data$WordFreq)')
cat('data$NLogWordFreq = -log(data$WordFreq)')
cat("There is a problem in ~/scr/NEURAL_ATTENTION_TASK/ed1516/data-full.csv: For P_2 and NP_2, one text (length about 168) appears twice. Must have to do with the way the texts are read in, first text is read twice or something like this.")
cat("Who is the nonnative speaker again? NP_03 or NP_05? It looks as if 3 was excluded but 5 is nonnative according to notes?")

groupedBootstrap = function(data2) {

  library("data.table")
  data3 = dcast(setDT(data2), tokenID ~ Participant, value.var=c("tt"))

  means = c()
 
  for(step in (1:100)) {
     newColumns =  c(1,sample((2:ncol(data3)), ncol(data3), replace=TRUE))
     newRows = sample((1:nrow(data3)), nrow(data3), replace=TRUE)
   
     data4 = data3[newRows,]
     data4 = data4[,newColumns,with=FALSE]
     data4 = melt(as.data.frame(data4), id=c("tokenID")) # TODO better not carry through tokenID and just bring it back now?
     newMean = mean(data4$value, na.rm=TRUE)
     means = c(means, newMean)
  }
  return(means) 
}


groupedBootstrapMatrix = function(dataMatrix) {
   # assumes dataMatrix is a matrix


  means = c()
 
  for(step in (1:100)) {
     newColumns =  sample((1:ncol(dataMatrix)), ncol(dataMatrix), replace=TRUE)
     newRows = sample((1:nrow(dataMatrix)), nrow(dataMatrix), replace=TRUE)
   
     data4 = dataMatrix[newRows,]
     data4 = data4[,newColumns]
     newMean = mean(data4, na.rm=TRUE)
     means = c(means, newMean)
  }
  return(means) 
}

basicBootstrap = function(dataVector) {

  means = c()
 
  for(step in (1:100)) {
     newRows = sample((1:length(dataVector)), length(dataVector), replace=TRUE)
   
     data4 = dataVector[newRows]
     newMean = mean(data4, na.rm=TRUE)
     means = c(means, newMean)
  }
  return(means) 

}

groupedBootstrapDev = function() {

  # TODO so simulations to compare this with plain bootstrap

#  data2 = aggregate(data["tt"], by=c(data["Condition"], data["Participant"], data["tokenID"]), mean, na.rm=TRUE)
#  data2 = data2[data2$Condition == "nopreview",]
#   groupedBootstrap(data2)

# simulations

rejectedNullBasic = 0
rejectedNullGroup = 0


subjects = 23
items = 1000
offset = 0.0 # question: is the mean different from zero?
varianceAcrossItems = 0.2
varianceAcrossSubjects = 0.2
epsilon = 0.1

iterations = 100
for(iter in (1:iterations)) {
  perItemMeans = rnorm(items, mean=0, sd=varianceAcrossItems)
  perSubjectMeans = rnorm(subjects, mean=0, sd=varianceAcrossSubjects)
  means = outer(perItemMeans, perSubjectMeans, "+")
  observations = rnorm(means, mean=means, epsilon)

  cat(iter,mean(observations),"\n")


  dataMatrix = matrix(observations, nrow=items)
  
  
  basic = basicBootstrap(observations)
  basicDown = quantile(basic,0.025)
  basicUp = quantile(basic, 0.975)
  if(basicDown > 0.0 || basicUp < 0.0) {
    rejectedNullBasic = rejectedNullBasic + 1.0
  }
  
  grouped = groupedBootstrapMatrix(dataMatrix)
  groupedDown = quantile(grouped,0.025)
  groupedUp = quantile(grouped, 0.975)
  
  if(groupedDown > 0.0 || groupedUp < 0.0) {
    rejectedNullGroup = rejectedNullGroup + 1.0
  }
  
}
cat("Basic",100*rejectedNullBasic/iterations,"Group",100*rejectedNullGroup/iterations,sep="\n")

}


createUniqueItemIDs = function(data, columnName) {
   newName = (paste(columnName,"Renumbered",sep="."))
   data[,newName] = as.integer(factor(data[,columnName]))
   return (data)
}



predictCorrectAnswer = function() {
    data = initialize(1, 1, "full")
    dataU = data[data$IsCorrectAnswer,]
    
    dataV = aggregate(dataU["tt"], by=c(dataU["Participant"], dataU["Condition"], dataU["Correct"], dataU["TextNo"]), sum, na.rm=TRUE)
    
    dataV = centerColumn(dataV, "tt")
    
    dummy = model.matrix( ~ Condition -1, data=dataV)
    dataV$Condition.dummy = dummy[,2] - 0.5
    dataV = centerColumn(dataV, "Condition.dummy")
    
    summary(glmer(Correct ~ Condition.dummy.Centered + tt.Centered + (tt.Centered|Participant) + (tt.Centered|TextNo) + (1|TextNo) + (1|Participant), data=dataV, family="binomial"))
}


# Diagnostics
#cor(data$price, data$exprice, use="complete")

newAnalyses = function() {

#  data = initialize(16,4, "dev")
  data = read.csv("~/scr/NEURAL_ATTENTION_TASK/ed1516/data-full.csv")

conditionLevelsDiff = max(data$Condition.dummy.Centered) - min(data$Condition.dummy.Centered)
if(conditionLevelsDiff > 1) {
   data$Condition.dummy.Centered = data$Condition.dummy.Centered / conditionLevelsDiff
}

  data.Complete = data[complete.cases(data[,c("HumanPosition","Condition.dummy","IsCorrectAnswer","IsNamedEntity","Surprisal","ExperimentTokenLength","LogWordFreq","tokenID","Participant", "fp", "ff", "tt")]),]
  
  dataN = data.Complete[data.Complete$fp > 0,]
  
  dataN$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = dataN, na.action=na.exclude))
  dataN$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = dataN, na.action=na.exclude))
  
  fullFormula = 'HumanPosition.Centered + Condition.dummy.Centered + LogWordFreq.Centered + IsNamedEntity.Centered + IsCorrectAnswer.Centered + Surprisal.Resid + ExperimentTokenLength.Resid + (1|tokenID) + (1|Participant) + Condition.dummy.Centered*LogWordFreq.Centered + HumanPosition.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*ExperimentTokenLength.Resid + Surprisal.Resid*ExperimentTokenLength.Resid + Condition.dummy.Centered*IsCorrectAnswer.Centered + Condition.dummy.Centered*IsNamedEntity.Centered + IsNamedEntity.Centered*Surprisal.Resid + HumanPosition.Centered*LogWordFreq.Centered + HumanPosition.Centered*IsNamedEntity.Centered + IsCorrectAnswer.Centered*ExperimentTokenLength.Resid + IsCorrectAnswer.Centered*Surprisal.Resid +Condition.dummy.Centered*LogWordFreq.Centered + Surprisal.Resid*ExperimentTokenLength.Resid + Condition.dummy.Centered*ExperimentTokenLength.Resid + HumanPosition.Centered*ExperimentTokenLength.Resid + IsNamedEntity.Centered*Surprisal.Resid + HumanPosition.Centered*IsNamedEntity.Centered + HumanPosition.Centered*LogWordFreq.Centered + HumanPosition.Centered*Condition.dummy.Centered + Condition.dummy.Centered*LogWordFreq.Centered + HumanPosition.Centered*Condition.dummy.Centered + LogWordFreq.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*IsCorrectAnswer.Centered + HumanPosition.Centered*ExperimentTokenLength.Resid'
 
  limit = mean(dataN$ff, na.rm=TRUE) + 4*sd(dataN$ff, na.rm=TRUE)
  dataN_ff = dataN[dataN$ff < limit,]

  limit = mean(dataN$ff, na.rm=TRUE) + 4*sd(dataN$fp, na.rm=TRUE)
  dataN_fp = dataN[dataN$fp < limit,]

  limit = mean(dataN$ff, na.rm=TRUE) + 4*sd(dataN$tt, na.rm=TRUE)
  dataN_tt = dataN[dataN$tt < limit,]
 
  
  modelFP = lme4::lmer(formula(paste("fp", " ~ ",fullFormula, sep="")) , data=dataN_fp)#, REML=FALSE)
  modelFF = lme4::lmer(formula(paste("ff", " ~ ",fullFormula, sep="")) , data=dataN_ff)#, REML=FALSE)
  modelTT = lme4::lmer(formula(paste("tt", " ~ ",fullFormula, sep="")) , data=dataN_tt)#, REML=FALSE)
  
    library('texreg')
  renamePredictorsInStringOutput(texreg(c(modelFF, modelFP, modelTT) ,single.row = TRUE))
  
  dataN$logFF = log(dataN$ff)
  dataN$logFP = log(dataN$fp)
  dataN$logTT = log(dataN$tt)
  
  
  modelLogFP = lme4::lmer(formula(paste("logFP", " ~ ",fullFormula, sep="")) , data=dataN, REML=FALSE)
  modelLogFF = lme4::lmer(formula(paste("logFF", " ~ ",fullFormula, sep="")) , data=dataN, REML=FALSE)
  modelLogTT = lme4::lmer(formula(paste("logTT", " ~ ",fullFormula, sep="")) , data=dataN, REML=FALSE)
  
  renamePredictorsInStringOutput(texreg(c(modelLogFF, modelLogFP, modelLogTT) ,single.row = TRUE))

}


selectInteractions = function() {

  data = initialize(16,4, "full")
  
  data.Complete = data[complete.cases(data[,c("HumanPosition","Condition.dummy","IsCorrectAnswer","IsNamedEntity","Surprisal","ExperimentTokenLength","LogWordFreq","tokenID","Participant", "fp")]),]
  
  dataN = data.Complete[data.Complete$fp > 0,]
  
  dataN$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = dataN, na.action=na.exclude))
  dataN$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = dataN, na.action=na.exclude))
  
  
  data.Complete$Surprisal.Resid = resid(lm(Surprisal ~ LogWordFreq, data = data.Complete, na.action=na.exclude))
  data.Complete$ExperimentTokenLength.Resid = resid(lm(ExperimentTokenLength ~ LogWordFreq, data = data.Complete, na.action=na.exclude))
  
  
  
  # base model
  predictors = c("HumanPosition.Centered", "Condition.dummy.Centered", "LogWordFreq.Centered", "IsNamedEntity.Centered", "IsCorrectAnswer.Centered", "Surprisal.Resid", "ExperimentTokenLength.Resid")
  
  # for 
  current_components = predictors
  current_components = c(current_components, "(1|tokenID)", "(1|Participant)")
  
  TARGET_VARIABLE = "ff"
#  TARGET_VARIABLE = "FIXATED"
  
  
  #
  if(TARGET_VARIABLE != "FIXATED") {
    modelFP = lme4::lmer(formula(paste(TARGET_VARIABLE, " ~ ",paste(current_components, collapse=" + "), sep="")) , data=dataN, REML=FALSE)
  } else {
    modelFP = lme4::glmer(formula(paste(TARGET_VARIABLE, " ~ ",paste(current_components, collapse=" + "), sep="")) , data=data.Complete, family = binomial("logit") , REML=FALSE)
  }
  
  bestModelSoFarFP = modelFP
  
  foundNew = TRUE
  while(foundNew) {
    foundNew = FALSE
    bestNewLLFP = 1
    bestNewInteraction = NULL
    
    for(i in (1:length(predictors))) {
       for(j in (i:length(predictors))) {
         new_interaction = paste(predictors[i], predictors[j], sep="*")
         new_formula = paste(paste(current_components, collapse=" + "), new_interaction, sep=" + ")
         if(TARGET_VARIABLE != "FIXATED") {
             modelFPNew = lme4::lmer(formula(paste(TARGET_VARIABLE," ~ ",new_formula, sep="")) , data=dataN, REML=FALSE)
         } else {
            modelFPNew = lme4::glmer(formula(paste(TARGET_VARIABLE," ~ ",new_formula, sep="")) , data=data.Complete, family = binomial("logit"), REML=FALSE)
         }
  
         llFPNew = anova(modelFPNew, bestModelSoFarFP)$"Pr(>Chisq)"[2]
         if(llFPNew < bestNewLLFP) {
              bestNewInteraction = new_interaction
              bestNewLLFP = llFPNew
              bestModelInThisIterFP = modelFPNew
              cat(new_interaction, llFPNew, sep="\n")
              cat(new_formula, sep="\n")
          } else {
              cat(new_interaction, llFPNew, sep="\n")
          }
       } 
    }
         if(bestNewLLFP < 0.05) {
              foundNew = TRUE
              current_components = c(current_components, bestNewInteraction)
  
              bestModelSoFarFP = bestModelInThisIterFP
              cat("SELECTED")
              cat(TARGET_VARIABLE, "\t")
              cat(bestNewInteraction, bestNewLLFP, sep="\n")
              
              #cat(new_formula, sep="\n")
          } else {
              foundNew = FALSE
              #cat(new_interaction, llFP, llFPNew, sep="\n")
          }
  }
  
  #TT ChiSq
  #HumanPosition.Centered + Condition.dummy.Centered + LogWordFreq.Centered + IsNamedEntity.Centered + IsCorrectAnswer.Centered + Surprisal.Resid + ExperimentTokenLength.Resid + (1|tokenID) + (1|Participant) + Condition.dummy.Centered*LogWordFreq.Centered + HumanPosition.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*ExperimentTokenLength.Resid + Surprisal.Resid*ExperimentTokenLength.Resid + Condition.dummy.Centered*IsCorrectAnswer.Centered + Condition.dummy.Centered*IsNamedEntity.Centered + IsNamedEntity.Centered*Surprisal.Resid + HumanPosition.Centered*LogWordFreq.Centered + HumanPosition.Centered*IsNamedEntity.Centered + IsCorrectAnswer.Centered*ExperimentTokenLength.Resid + IsCorrectAnswer.Centered*Surprisal.Resid 
  
  #FP ChiSq
  #HumanPosition.Centered + Condition.dummy.Centered + LogWordFreq.Centered + IsNamedEntity.Centered + IsCorrectAnswer.Centered + Surprisal.Resid + ExperimentTokenLength.Resid + (1|tokenID) + (1|Participant) + Condition.dummy.Centered*LogWordFreq.Centered + Surprisal.Resid*ExperimentTokenLength.Resid + Condition.dummy.Centered*ExperimentTokenLength.Resid + HumanPosition.Centered*ExperimentTokenLength.Resid + IsNamedEntity.Centered*Surprisal.Resid + HumanPosition.Centered*IsNamedEntity.Centered + HumanPosition.Centered*LogWordFreq.Centered + HumanPosition.Centered*Condition.dummy.Centered
  
  # Fix ChiSq
  #HumanPosition.Centered + Condition.dummy.Centered + LogWordFreq.Centered + IsNamedEntity.Centered + IsCorrectAnswer.Centered + Surprisal.Resid + ExperimentTokenLength.Resid + (1|tokenID) + (1|Participant) + Condition.dummy.Centered*LogWordFreq.Centered + HumanPosition.Centered*Condition.dummy.Centered + LogWordFreq.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*IsCorrectAnswer.Centered + HumanPosition.Centered*ExperimentTokenLength.Resid
  ##############################
  
  
  
  # FP BIC: HumanPosition.Centered + Condition.dummy.Centered + LogWordFreq.Centered + IsNamedEntity.Centered + IsCorrectAnswer.Centered + Surprisal.Resid + ExperimentTokenLength.Resid + (1|tokenID) + (1|Participant) + HumanPosition.Centered*IsNamedEntity.Centered + HumanPosition.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*LogWordFreq.Centered + Condition.dummy.Centered*ExperimentTokenLength.Resid + Surprisal.Resid*ExperimentTokenLength.Resid
  
  # TT AIC: HumanPosition.Centered + Condition.dummy.Centered + LogWordFreq.Centered + IsNamedEntity.Centered + IsCorrectAnswer.Centered + Surprisal.Resid + ExperimentTokenLength.Resid + (1|tokenID) + (1|Participant) + HumanPosition.Centered*LogWordFreq.Centered + HumanPosition.Centered*IsNamedEntity.Centered + HumanPosition.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*LogWordFreq.Centered + Condition.dummy.Centered*IsNamedEntity.Centered + Condition.dummy.Centered*IsCorrectAnswer.Centered + Condition.dummy.Centered*ExperimentTokenLength.Resid + LogWordFreq.Centered*IsNamedEntity.Centered + IsNamedEntity.Centered*Surprisal.Resid + IsCorrectAnswer.Centered*Surprisal.Resid + IsCorrectAnswer.Centered*ExperimentTokenLength.Resid + Surprisal.Resid*ExperimentTokenLength.Resid + LogWordFreq.Centered*ExperimentTokenLength.Resid
  
  # TT BIC: HumanPosition.Centered + Condition.dummy.Centered + LogWordFreq.Centered + IsNamedEntity.Centered + IsCorrectAnswer.Centered + Surprisal.Resid + ExperimentTokenLength.Resid + (1|tokenID) + (1|Participant) + HumanPosition.Centered*IsNamedEntity.Centered + HumanPosition.Centered*ExperimentTokenLength.Resid + Condition.dummy.Centered*LogWordFreq.Centered + Condition.dummy.Centered*IsNamedEntity.Centered + Condition.dummy.Centered*IsCorrectAnswer.Centered + Condition.dummy.Centered*ExperimentTokenLength.Resid + Surprisal.Resid*ExperimentTokenLength.Resid + HumanPosition.Centered*LogWordFreq.Centered
  
  #latexModel(lme4::lmer(fp ~ PositionInHumanSentence.Centered + Condition.dummy.Centered + HumanPosition.Centered*Condition.dummy.Centered +  ExperimentTokenLength.Centered*Condition.dummy.Centered  + IsNamedEntity.Centered*Condition.dummy.Centered + IsCorrectAnswer.Centered*Condition.dummy.Centered + OccursInQuestion.Centered*Condition.dummy.Centered + Surprisal.Centered*Condition.dummy.Centered + FunctionWord.Centered*Condition.dummy.Centered*HumanPosition.Centered + HumanPosition.Centered*ExperimentTokenLength.Centered + HumanPosition.Centered*IsNamedEntity.Centered + (1|tokenID) + (1|Participant), data=dataN))



}



renamePredictorsInStringOutput = function(output) {
 output = gsub(".Centered","",output,fixed=TRUE)
 output = gsub(".dummy","",output,fixed=TRUE)
 output = gsub("PositionInHumanSentence","PositionSentence",output,fixed=TRUE)
 output = gsub("HumanPosition","PositionText",output,fixed=TRUE)
 output = gsub("ExperimentTokenLength","WordLength",output,fixed=TRUE)
 output = gsub("FunctionWord","IsFunctionWord",output,fixed=TRUE)

 output = gsub("\\;","&",output,fixed=TRUE)
 output = gsub(")$",")^{}$",output,fixed=TRUE)

 output = gsub("$","",output,fixed=TRUE)
 output = gsub("^"," &  $^",output,fixed=TRUE)
 output = gsub("}","}$",output,fixed=TRUE)

 output = gsub("$^{}$", "", output,fixed=TRUE)

#LogWordFreq:WordLength.Resid     & $0.90  \;   (0.36)^{*}$     & $0.92 \; (0.56)$         & $0.89 \; (0.94)$          \\
#(Intercept)                      & 204.24 &    (5.58) &   $^{***}$  & 227.88    &    (7.30) &   $^{***}$  & 288.70    &    (14.19) &  $^{***}$ \\


 return(output)
}

latexModel = function(model) {
  library('texreg')
  return(renamePredictorsInStringOutput(texreg(model ,single.row = TRUE)))
}


plotIntoJPG = function(figNum, vector1, vector2, xlab, ylab, main,xlim=NULL,ylim=NULL) {
 pdf(paste('figures/plot',figNum,'.pdf',sep=""))
 plot(vector1,vector2,xlab=list(xlab,cex=2.0),ylab=list(ylab,cex=2.0),main=main,xlim=xlim,ylim=ylim)
 dev.off()
}


slideFunct <- function(data, windowBefore, windowAfter, step){
  total <- length(data)
  spots <- seq(from=1, to=(total), by=step)
  result <- vector(length = length(spots), mode="numeric") * NA
  for(i in (1+windowBefore):(length(spots)-windowAfter)){
    result[i] <- mean(data[(spots[i]-windowBefore):(spots[i]+windowAfter)])
  }
  return(result)
}



##########################
##########################

#agg = aggregate(corAns[,24],by=list(corAns$Correct.y,corAns$Participant, corAns$TextNo, corAns$Condition), sum, na.rm=TRUE)
# check whether there is an upstream bug. people answer correctly without even looking at the question...
readHumanScores = function(data) {
   humanScores = read.csv(paste("../../eyetracking-data/scores.csv", sep=""),header=TRUE,sep="\t")
   humanScores$isPreview = ((humanScores$Participant %% 2) == 0)
   humanScoresPreview = humanScores[humanScores$isPreview,]
   humanScoresNoPreview = humanScores[!humanScores$isPreview,]
   # exclude nonnative participants
   if(TRUE) {
   humanScoresNoPreview = humanScoresNoPreview[!(humanScoresNoPreview$Participant %in% c(5, 17)),]
   }

  humanScoresPreview$Participant = humanScoresPreview$Participant / 2
  humanScoresNoPreview$Participant = (humanScoresNoPreview$Participant + 1) / 2
  if(RENAME_PARTICIPANT_NAMES) {
    humanScoresPreview$Participant = paste("P", humanScoresPreview$Participant, sep="_")
    humanScoresNoPreview$Participant = paste("NP", humanScoresNoPreview$Participant, sep="_")
  }


  humanScores = rbind(humanScoresPreview, humanScoresNoPreview)
  humanScores$Participant = as.factor(humanScores$Participant)
  humanScores$isPreview = NULL
   return(merge(data, humanScores, by=c("Participant","TextNo")))

}

# aggregate some eyetracking measure
aggregateTTPerQuestion = function(data) {
 agg = aggregate(data[,24],by=list(data$Participant,data$TextNo,data$Correct.x,data$Condition), mean, na.rm=TRUE)
}

##########################
##########################




# /mhahn/repo2/mhahn_files/ed1516/experiments/corpus/forEvaluation/questions/annotation-textsonly/att-pg-test-neat-qa-500-512-0.7-100-R-0.5again-tq-hard/E-2.csv
# /mhahn/repo2/mhahn_files/ed1516/experiments/corpus/forEvaluation/mappingWithHuman/nopreview/Participant-1/E-2.csv
# 

# Training data: texts c(2,4,6,8,10,12,14,16,18,20)
# participants c(2,4,6,8,10,12)

initialize = function(modelP, modelNP, partition = "dev") {
  library(lme4)
  dataP = initializeForCondition("preview", modelP, partition)
  dataNP = initializeForCondition("nopreview", modelNP, partition)

  if(RENAME_PARTICIPANT_NAMES) {
    dataP$Participant = (paste("P", dataP$Participant, sep="_"))
    dataNP$Participant = (paste("NP", dataNP$Participant, sep="_"))
  }

  data = rbind(dataP, dataNP)

  data$Participant = as.factor(data$Participant)

  data = center(data)
  data = readHumanScores(data)
  dummy = model.matrix( ~ Condition -1, data=data)
  data$Condition.dummy = dummy[,2] - 0.5
  data = centerColumn(data, "Condition.dummy") 
  
  data = centerColumn(data,"Surprisal")
  
  data$LogWordFreq = log(data$WordFreq)
  
  data = centerColumn(data, "LogWordFreq")

  return(data)
}

initializeModelOnly = function(modelP, modelNP) {
  library(lme4)
  dataP = initializeModelOnlyForCondition("preview",modelP,"sample")
  dataNP = initializeModelOnlyForCondition("nopreview",modelNP,"sample")
  data = rbind(dataP, dataNP)

  data$IsNamedEntity.Centered = data$IsNamedEntity - mean(data$IsNamedEntity,na.rm=TRUE)
  data$OccursInQuestion.Centered = data$OccursInQuestion - mean(data$OccursInQuestion,na.rm=TRUE)
  data$IsCorrectAnswer.Centered = data$IsCorrectAnswer - mean(data$IsCorrectAnswer,na.rm=TRUE)
  		data$OccurrencesOfCorrectAnswerSoFar.Centered <- data$OccurrencesOfCorrectAnswerSoFar - mean(data$OccurrencesOfCorrectAnswerSoFar, na.rm = TRUE)
  
  data$HumanPosition.x = NULL
  data$HumanPosition.y = NULL


  data$ExperimentTokenLength.Centered <- data$ExperimentTokenLength - mean(data$ExperimentTokenLength, na.rm = TRUE)
  data$AttentionScore.Centered   <- data$AttentionScore  - mean(data$AttentionScore  , na.rm = TRUE)
  data$AttentionDecision.Centered   <- data$AttentionDecision  - mean(data$AttentionDecision  , na.rm = TRUE)
  data = centerColumn(data,"AnonymizedPosition")
  data$LuaID.Centered   <- (data$LuaID  - mean(data$LuaID  , na.rm = TRUE)) / sd(data$LuaID, na.rm=TRUE)
#		data$   <- data$  - mean(data$  , na.rm = TRUE)
#		data$   <- data$  - mean(data$  , na.rm = TRUE)

  dummy = model.matrix( ~ Condition -1, data=data)
  data$Condition.dummy = dummy[,2] - 0.5
  data = centerColumn(data, "Condition.dummy") 

  data$TextID.renumbered = as.integer(data$TextID)
  data$tokenID = 10000 * data$TextID.renumbered + data$AnonymizedPosition

  return(data)

}

















evaluationOfModels = function(data) {
sink("~/scr/coefficients-model-output.txt")
data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")

 model = lmer(fromInput ~ LogWordFreq.Centered + IsNamedEntity.Centered + FunctionWord.Centered + (1|model) + (1|LuaID.x), data=data)
cat( capture.output(summary(model)),sep="\n")

  model = lmer(positionGate ~ FunctionWord.Centered + ExperimentTokenLength.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
 cat(capture.output(summary(model)),sep="\n")

 model = lmer(conditionGate ~ IsNamedEntity.Centered + FunctionWord.Centered  + (1|model) + (1|LuaID.x), data=data)# [maybe better just throw punctuation away]
  cat(capture.output(summary(model)),sep="\n")

  model = lmer(conditionTimesPositionGate ~ (1|model) + (1|LuaID.x), data=data)
cat(  capture.output(summary(model)),sep="\n")
sink()
}

printLineForPred = function(name, model) {
  cat(name, sep=" ")
  for(i in (1:4)) {
   sd = summary(model)$vcov@factors$correlation@sd[i]
   beta = fixef(model)[[i]]
   cat(paste(" & ",round(beta,3),"  (",round(sd,3),", ",round(beta/sd,2),")  ", sep=""), sep = " ")
  }
  cat(" \\\\ \n")
}


# July 2017
evaluationOfNEATCoefficients = function(data) {


data$position = data$positionGated/data$positionGate

data$linearScore.Preview = data$fromInput + data$positionGated + 0.5 * data$conditionGate + data$gateFromInput * data$dotproductAffine + data$gatedFromHistory + 0.5 * data$position * data$conditionTimesPositionGate
data$linearScore.NoPreview = data$fromInput + data$positionGated - 0.5 * data$conditionGate - 0.5 * data$position * data$conditionTimesPositionGate
data$P.Minus.NP = data$linearScore.Preview - data$linearScore.NoPreview

dataP = cbind(data)
dataNP = cbind(data)
dataP$linearScore = dataP$linearScore.Preview
dataNP$linearScore = dataP$linearScore.NoPreview
dataP$Condition.dummy = -1
dataNP$Condition.dummy = 1
dataDouble = rbind(dataP,dataNP)

#################



data$position = data$positionGated/data$positionGate


data$linearScore.Preview = data$fromInput + data$positionGated + 0.5 * data$conditionGate + data$gateFromInput * data$dotproductAffine + data$gatedFromHistory + 0.5 * data$position * data$conditionTimesPositionGate
data$linearScore.NoPreview = data$fromInput + data$positionGated - 0.5 * data$conditionGate - 0.5 * data$position * data$conditionTimesPositionGate
data$P.Minus.NP = data$linearScore.Preview - data$linearScore.NoPreview
#model = lmer(P.Minus.NP ~ LogWordFreq.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)

data$Att.P = 1/(1+exp(-data$linearScore.Preview))
data$Att.NP = 1/(1+exp(-data$linearScore.NoPreview))
#data$P.Minus.NP = data$Att.P - data$Att.NP
#model = lmer(P.Minus.NP ~ LogWordFreq.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)


dataAgg = aggregate(c(data["fromInput"],data["positionGate"],data["conditionGate"],data["gateFromInput"], data["questHistoryOutGate"], data["conditionTimesPositionGate"]), by=c(data["LogWordFreq.Centered"], data["IsNamedEntity.Centered"], data["FunctionWord.Centered"], data["LuaID.x"], data["model"]), mean)

model = lmer(fromInput ~ LogWordFreq.Centered + FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "Intercept"
printLineForPred(name, model)

model = lmer(positionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "Position"
printLineForPred(name, model)

#model = lmer(conditionGate ~ LogWordFreq.Centered+ (LogWordFreq.Centered|model) + IsNamedEntity.Centered + (1|model) , data=dataAgg)

# does not really make sense. multiple observations do not mean anything when there is no variation, and for single observations per group a random effect is meaningless. So the analyses here make no sense.
# If at all, need slopes for model and no random effect for LuaID.x

model = lmer(conditionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "Condition.Preview"
printLineForPred(name, model)

model = lmer(gateFromInput ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "UnigramOverlap"
printLineForPred(name, model)

model = lmer(questHistoryOutGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "History"
printLineForPred(name, model)

model = lmer(conditionTimesPositionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=dataAgg)
name = "CondPrev*Position"
printLineForPred(name, model)










model = lmer(fromInput ~ LogWordFreq.Centered + FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "Intercept"
printLineForPred(name, model)

model = lmer(positionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "Position"
printLineForPred(name, model)

#model = lmer(conditionGate ~ LogWordFreq.Centered+ (LogWordFreq.Centered|model) + IsNamedEntity.Centered + (1|model) , data=data)

# does not really make sense. multiple observations do not mean anything when there is no variation, and for single observations per group a random effect is meaningless. So the analyses here make no sense.
# If at all, need slopes for model and no random effect for LuaID.x

model = lmer(conditionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "Condition.Preview"
printLineForPred(name, model)

model = lmer(gateFromInput ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "UnigramOverlap"
printLineForPred(name, model)

model = lmer(questHistoryOutGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "History"
printLineForPred(name, model)

model = lmer(conditionTimesPositionGate ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "CondPrev*Position"
printLineForPred(name, model)

data$questionInducedValue = 0.5 * data$conditionTimesPositionGate * data$position + 0.5 * data$conditionGate + data$gatedFromHistory + data$gateFromInput * data$dotproductAffine
#data$questionInducedValue =  data$gateFromInput * data$dotproductAffine

model = lmer(questionInducedValue ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|tokenID), data=data)
#                       Estimate Std. Error t value
#(Intercept)             0.14328    0.03385   4.233
#LogWordFreq.Centered    0.09453    0.02014   4.695
#FunctionWord.Centered   0.10871    0.02055   5.291
#IsNamedEntity.Centered  0.07169    0.02196   3.264


# Note that LuaID.x as a random effect is a bad idea, since things vary by tokenID in this quantity
#model = lmer(questionInducedValue ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data)
name = "ActualValue"
printLineForPred(name, model)

model = lmer(questionInducedValue ~ LogWordFreq.Centered+ FunctionWord.Centered + IsNamedEntity.Centered + (1|model) , data=data)



#dotproductAffine: UnigramOverlap
#attRelativeToQ : the actual vector similarity (not used)
# what's the difference?
#  fromInput = attention.scalarCoefficient(embeddingIntermediateDimension+2,fromInput,"fromInput") -- attention score from the input
#  gateFromInput = attention.scalarCoefficient(embeddingIntermediateDimension,xemb,"gateFromInput") -- gating score from the input for UnigramOverlap


#  questHistOutGate = nn.StoreInContainerLayer("questHistoryOutGate", true)(questHistOutGate) # the history gate
#  -- used for this decision
#  gatedFromHistory = nn.CMulTable()({questHistOutGate, questionHistory})
#  gatedFromHistory = nn.StoreInContainerLayer("gatedFromHistory", true)(gatedFromHistory) # the gated history

#positionGated = nn.StoreInContainerLayer("positionGated", true)(positionGated)



#Coefficient & Predictor & Estimate & SE & t \\ \hline
#Intercept & Intercept &-0.36 & 0.05 & -7.4 \\



# model = lmer(conditionGate ~ LuaID.x.Centered + (LuaID.x.Centered|model) + (1|model) + (1|LuaID.x), data=data) #: no effect
#  model = lmer(conditionGate ~ IsNamedEntity.Centered + (IsNamedEntity.Centered|model) + (1|model) + (1|LuaID.x), data=data) #: no effect
 #model = lmer(conditionGate ~ IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data) #: effect stronger in NoPreview
# model = lmer(conditionGate ~ WordFreq + (1|model) + (1|LuaID.x), data=data) #: unexpected effect
 #model = lmer(conditionGate ~ LuaID.x.Centered + IsNamedEntity.Centered + (1|model) + (1|LuaID.x), data=data) #: NE negative, LUAID positive
#  model = lmer( gateFromInput ~ (1|model) + (1|LuaID.x), data=data) #: positive
# model = lmer(conditionGate ~ FunctionWord.Centered + (1|model) + (1|LuaID.x), data=data) #didn't converge
#  model = lmer(positionGate ~ FunctionWord.Centered + (FunctionWord.Centered|model) + (1|model) + (1|LuaID.x.Centered), data=data)
# model = lmer(positionGate ~ FunctionWord.Centered + (1|model) + (1|LuaID.x), data=data) is positive
# bigger correlation with surprisal in NoPreview should be provable
#   model = lme4::lmer(AttentionScore.Centered ~ Surprisal.Centered * Condition.dummy.Centered + (1|tokenID), data=data)
}



prepareCoefficientsSample = function() {
  data = getMultipleModelsCoefficientsSample()
# data = data[!is.na(data$LuaID),]
#data = data[data$LuaID < 50000,]

write.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-coefficients-1400.csv", x=data)

return(data) 
}

# July 23, 2017
analyzeCoefficientsSample = function() {

dataCoef = read.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-coefficients.csv")
data = dataCoef
data = data[!data$IsPunctuation,]
# remove words that were OOV
data = data[!is.na(data$LuaID),]
data = data[data$LuaID < 49999,]

logWFMean = mean(data$LogWordFreq[!data$IsNamedEntity], na.rm=TRUE)
data$LogWordFreq[data$IsNamedEntity] <- logWFMean

corAnsMean = mean(data$IsCorrectAnswer[data$IsNamedEntity], na.rm=TRUE)
data$IsCorrectAnswer = data$IsCorrectAnswer+0
data$IsCorrectAnswer[!data$IsNamedEntity] <- corAnsMean



data = centerColumn(data,"LogWordFreq")
data = centerColumn(data,"OccursInQuestion")
data = centerColumn(data,"IsCorrectAnswer")
data = centerColumn(data,"IsNamedEntity")
data = centerColumn(data,"HumanPosition.x")
data = centerColumn(data,"ExperimentTokenLength")



data$position = data$positionGated/data$positionGate

data$linearScore.Preview = data$fromInput + data$positionGated + 0.5 * data$conditionGate + data$gateFromInput * data$dotproductAffine + data$gatedFromHistory + 0.5 * data$position * data$conditionTimesPositionGate
data$linearScore.NoPreview = data$fromInput + data$positionGated - 0.5 * data$conditionGate - 0.5 * data$position * data$conditionTimesPositionGate
data$P.Minus.NP = data$linearScore.Preview - data$linearScore.NoPreview

dataP = cbind(data)
dataNP = cbind(data)
dataP$linearScore = dataP$linearScore.Preview
dataNP$linearScore = dataP$linearScore.NoPreview
dataP$Condition.dummy.Centered = -0.5
dataNP$Condition.dummy.Centered = 0.5
dataDouble = rbind(dataP,dataNP)

dataDouble$ExperimentTokenLength.Centered.Resid = residuals(lm(ExperimentTokenLength.Centered ~ LogWordFreq, data=dataDouble, na.action=na.exclude))
#dataN$Surprisal.Centered.Resid = residuals(lm(Surprisal.Centered ~ LogWordFreq + ExperimentTokenLength.Centered, data=dataN, na.action=na.exclude))


library(lme4)
model = lme4::lmer(linearScore ~ HumanPosition.x.Centered*Condition.dummy.Centered + LogWordFreq.Centered*Condition.dummy.Centered  + IsNamedEntity.Centered*Condition.dummy.Centered + IsCorrectAnswer.Centered*Condition.dummy.Centered + OccursInQuestion.Centered*Condition.dummy.Centered + HumanPosition.x.Centered*LogWordFreq.Centered + HumanPosition.x.Centered*IsNamedEntity.Centered + (1|tokenID) + (1|model), data=dataDouble)

model = lme4::lmer(linearScore ~ HumanPosition.x.Centered*Condition.dummy.Centered + LogWordFreq.Centered*Condition.dummy.Centered  + IsNamedEntity.Centered*Condition.dummy.Centered + IsCorrectAnswer.Centered*Condition.dummy.Centered + OccursInQuestion.Centered*Condition.dummy.Centered + HumanPosition.x.Centered*LogWordFreq.Centered + HumanPosition.x.Centered*IsNamedEntity.Centered + ExperimentTokenLength.Centered.Resid*Condition.dummy.Centered + (1|tokenID) + (1|model), data=dataDouble)


}


# July 15
getMultipleModelsCoefficientsSample = function() {
  texts = sampleTextsNames()
#  participants = c(1,2,3,4,5,6,7,8,9,10,11,12)

if(FALSE) {
  models = c(300, 301, 302, 306, 307, 308, 310, 311, 312, 313, 316, 317, 318, 319, 810, 850, 851, 852, 860, 861, 870, 880, 884, 990)
  condition = "preview" #does this mean separate sets were saved in the two conditions???

} else {
  models = c(1400, 1401, 1402, 1403, 1410, 1411, 1412, 1420, 1421, 1422, 1430, 1431, 1432, 1450, 1451, 1452, 1460, 1461, 1462, 1463, 1480, 1481, 1482, 1490, 1491, 1492)
  condition = "nopreview" #does this mean separate sets were saved in the two conditions???

}



  modelNum = models[1]
  text = texts[1]

  modelFile = paste("att-",condition,"-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",modelNum,"-l1-nll", sep="")

  dataCoef = readCoefficients(modelFile,text,"sample")
  dataExternal = readExternalData(text,"sample") 
#  dataSurprisal = readSurprisalData(text,"sample")
  data = merge(dataExternal,dataCoef, by=c("JointPosition"))
#  data = merge(data,dataSurprisal, by=c("JointPosition"))
  data$model = modelNum
  data$TextNo = text

  for (text in texts) {
   for (modelNum in 1:length(models)) {

     if(!(text == texts[1] && modelNum == models[1])) {

      modelFile = paste("att-",condition,"-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",models[modelNum],"-l1-nll", sep="")
#     
if(!file.exists(paste("forEvaluation/questions/annotation-textsonly/",modelFile,sep=""))) {
      modelFile = paste("att-","preview","-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",models[modelNum],"-l1-nll", sep="")

}
 
      if(file.exists(paste("forEvaluation/questions/annotation-textsonly/",modelFile,sep=""))) {
        cat(modelFile,sep="\n")

          dataExternal = readExternalData(text,"sample")
  #        dataSurprisal = readSurprisalData(text,"sample")

          dataCoef = readCoefficients(modelFile,text,"sample")

          dataNew = merge(dataExternal,dataCoef, by=c("JointPosition"))
 #         dataNew = merge(dataNew,dataSurprisal, by=c("JointPosition"))
          dataNew$model = modelNum
          dataNew$TextNo = text
          data <- rbind(data,dataNew)
          write.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-coefficients-1400.csv", x=data)
      } else {
        cat("Doesn't exist",sep="\n")
        cat(modelFile,sep="\n")
      }
    }
   }
  }

  data = createUniqueItemIDsByColumnName(data, "TextNo")
  data$tokenID = 10000 * data$TextNo.Renumbered + data$JointPosition

  data = centerColumn(data, "HumanPosition.x") 
  data$Condition = condition



  data$IsPunctuation = (data$POS.Universal == ".")
  data$FunctionWord = (data$POS.Universal =="ADP"  | data$POS.Universal =="ADV" | data$POS.Universal =="CONJ" | data$POS.Universal =="DET" | data$POS.Universal =="PRON" | data$POS.Universal =="PRT")
  data = centerColumn(data, "IsPunctuation")
  data = centerColumn(data, "FunctionWord")
  data = centerColumn(data, "ExperimentTokenLength")
  data = centerColumn(data, "IsNamedEntity")
  data = centerColumn(data, "IsCorrectAnswer")
  data = centerColumn(data, "OccursInQuestion")
#  data = centerColumn(data, "Surprisal")
  data$LogWordFreq = log(data$WordFreq)
  data = centerColumn(data, "LogWordFreq")


  data = centerColumn(data, "PositionInHumanSentence")
  return(data)
}








getMultipleModelsCoefficients = function() {
  texts = c(2,4,6,8,10,12,14,16,18,20)
#  participants = c(1,2,3,4,5,6,7,8,9,10,11,12)
  condition = "mixed"

  models = c(300, 301, 302, 303, 307, 308, 310, 311, 312, 313, 316, 317, 318, 319, 810, 850, 851, 852, 860, 861, 870, 880, 884, 990)


  modelNum = 300
  text = 2

  modelFile = paste("att-",condition,"-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",modelNum,"-l1-nll", sep="")

  dataCoef = readCoefficients(modelFile,text,"eyetrack")
  dataExternal = readExternalData(text,"eyetrack") 
  dataSurprisal = readSurprisalData(text,"eyetrack")
  data = merge(dataExternal,dataCoef, by=c("JointPosition"))
  data = merge(data,dataSurprisal, by=c("JointPosition"))
  data$model = modelNum
  data$TextNo = text

  for (text in texts) {
   for (modelNum in 1:length(models)) {

     if(!(text == 2 && modelNum == 1)) {

      modelFile = paste("att-",condition,"-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",models[modelNum],"-l1-nll", sep="")
#     
if(!file.exists(paste("forEvaluation/questions/annotation-textsonly/",modelFile,sep=""))) {
      modelFile = paste("att-","preview","-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",models[modelNum],"-l1-nll", sep="")

}
 
      if(file.exists(paste("forEvaluation/questions/annotation-textsonly/",modelFile,sep=""))) {
        cat(modelFile,sep="\n")

          dataExternal = readExternalData(text,"eyetrack")
          dataSurprisal = readSurprisalData(text,"eyetrack")

          dataCoef = readCoefficients(modelFile,text,"eyetrack")

          dataNew = merge(dataExternal,dataCoef, by=c("JointPosition"))
          dataNew = merge(dataNew,dataSurprisal, by=c("JointPosition"))
          dataNew$model = modelNum
          dataNew$TextNo = text
          data <- rbind(data,dataNew)
      } else {
        cat("Doesn't exist",sep="\n")
        cat(modelFile,sep="\n")
      }
    }
   }
  }



  data$tokenID = 10000 * data$TextNo + data$JointPosition

  data = centerColumn(data, "HumanPosition") 
  data$Condition = condition



  data$IsPunctuation = (data$POS.Universal == ".")
  data$FunctionWord = (data$POS.Universal =="ADP"  | data$POS.Universal =="ADV" | data$POS.Universal =="CONJ" | data$POS.Universal =="DET" | data$POS.Universal =="PRON" | data$POS.Universal =="PRT")
  data = centerColumn(data, "IsPunctuation")
  data = centerColumn(data, "FunctionWord")
  data = centerColumn(data, "ExperimentTokenLength")
  data = centerColumn(data, "IsNamedEntity")
  data = centerColumn(data, "IsCorrectAnswer")
  data = centerColumn(data, "OccursInQuestion")
  data = centerColumn(data, "Surprisal")
  data$LogWordFreq = log(data$WordFreq)
  data = centerColumn(data, "LogWordFreq")


  data = centerColumn(data, "PositionInHumanSentence")
  return(data)
}


# July 2017
studyCoefficients = function() {
  dataCoef = getMultipleModelsCoefficients()
 dataCoef$LogWordFreq = log(dataCoef$WordFreq)
 dataCoef = centerColumn(dataCoef,"LogWordFreq")




}

# July 2017
buildMixEfFitForModelFixations = function() {
#  data = initialize(1,1)
  data = getMultipleModelsBothConditions()
#  data2$ModelAttention <-  data2$AttentionScore
#  dataM = aggregate(data2["ModelAttention"],by=c(data2["tokenID"], data2["Condition"]), mean, na.rm=TRUE)
#  dataNew = merge(data,dataM, by=c("tokenID", "Condition"))
 data$LogWordFreq = log(data$WordFreq)
 data = centerColumn(data,"LogWordFreq")
# model = glm(AttentionDecision ~ Condition.dummy.Centered*LogWordFreq.Centered, family = binomial("logit"),data=data)
  model = glmer(AttentionDecision ~ Condition.dummy.Centered*LogWordFreq.Centered + (Condition.dummy.Centered*LogWordFreq.Centered|model) + (1|model), family = binomial("logit"),data=data)

}

# July 2017
buildHeatmapForAverages = function() {
  data = initialize(1,1)
  data2 = getMultipleModelsBothConditions()
  data2$ModelAttention <-  data2$AttentionScore
  dataM = aggregate(data2["ModelAttention"],by=c(data2["tokenID"], data2["Condition"]), mean, na.rm=TRUE)
  dataNew = merge(data,dataM, by=c("tokenID", "Condition"))

  dataNew = dataNew[with(dataNew, order(tokenID)),]

# (JULY 2017) TODO figure out why there are two different LuaID's. Does one come from CNN and one from DailyMail?
data2$LuaID = data2$LuaID.x

  dataM = aggregate(data2["AttentionScore"], by=c(data2["WordFreq"],data2["tokenID"], data2["Condition"], data2["ExperimentToken.x"], data2["TextNo"], data2["JointPosition"], data2["LuaID"]), mean, na.rm=TRUE)
  dataM = dataM[with(dataM, order(tokenID)),]


dataP = dataM[dataM$Condition == "preview",]
dataNP = dataM[dataM$Condition == "nopreview",]

printHeatmap_Fresh(dataP, 0.0, 1.0,"preview", 200, TRUE, "AttentionScore", 2) 
#printHeatmap = function(data, low, high, only_condition, end, exclude, attention, textno) {



}

# July 2017
printHeatmap_Fresh = function(data, low, high, only_condition, end, exclude, attention, textno) {
        data = data[data$TextNo == textno,]
        attention = data[,attention]
	itemno = data$tokenID
		words = data$ExperimentToken.x
		wordfreq = data$LuaID
                condition = data$Condition
		initializeColors()
                end = min(end,length(itemno))
		cat("\\definecolor{none}{rgb}{0.99,0.99,0.99}","\n",sep="")
		for(i in (1:end)) {
#			if(itemno[i] > 0 && itemno[i] != 3) {
#				break
#			}
			if(words[i] == -1) {
				next
			}
                        if(condition[i] != only_condition) {
                                next
                        }
                        if(words[i] == "") {
                          next
                        }
			if((exclude && (wordfreq[i] > 9999)) ||  is.na(attention[i]  )){
#    cat(" ",as.character(words[i]),sep="")
				cat(" \\colorbox{none}{",as.character(words[i]),"}",sep="")
			} else {
				colorID = min(18, max(3, floor((attention[i] - low) * 20 / (high - low))))
					cat(" \\colorbox{color", colorID,     "}{",as.character(words[i]),"}",sep="")
			}
			if(i %% 10 == 0) {
				cat("\n")
			}

		}

}


#hahahihi
evaluateAverages = function() {
#  data2 = getMultipleModelsBothConditions(TRUE)
  data2 = read.csv("~/scr/NEURAL_ATTENTION_TASK/ed1516/data-models.csv")

#  data = initialize(1,1) #, "total")
  data = read.csv("~/scr/NEURAL_ATTENTION_TASK/ed1516/data-full.csv")

cat("There is a problem in ~/scr/NEURAL_ATTENTION_TASK/ed1516/data-full.csv: For P_2 and NP_2, one text (length about 168) appears twice. Must have to do with the way the texts are read in.")

#  cat("PROBLEM")
 # crash()
#aggregate(data["tokenID"], by=c(data["tokenID"], data["Participant"], data["Condition"]), NROW)
# shows that many participants have multiple datapopints in a single condition!
  data2$ModelAttention <-  data2$AttentionScore
  dataM = aggregate(data2["ModelAttention"],by=c(data2["tokenID"], data2["Condition"]), mean, na.rm=TRUE)
  dataNew = merge(data,dataM, by=c("tokenID", "Condition"))

data = dataNew

data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("ModelAttention")]),]

# NOTE can also do log-transformed reading times
dataN$ExperimentTokenLength.Centered.Resid = residuals(lm(ExperimentTokenLength.Centered ~ LogWordFreq, data=dataN, na.action=na.exclude))
dataN$Surprisal.Centered.Resid = residuals(lm(Surprisal.Centered ~ LogWordFreq + ExperimentTokenLength.Centered, data=dataN, na.action=na.exclude))

dataN$ModelAttention.Resid = residuals(lm(ModelAttention ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid + LogWordFreq.Centered, data=dataN, na.action=na.exclude))

data = data[complete.cases(data[,c("ModelAttention")]),]


data$ExperimentTokenLength.Centered.Resid = residuals(lm(ExperimentTokenLength.Centered ~ LogWordFreq, data=data, na.action=na.exclude))
data$Surprisal.Centered.Resid = residuals(lm(Surprisal.Centered ~ LogWordFreq + ExperimentTokenLength.Centered, data=data, na.action=na.exclude))

data$ModelAttention.Resid = residuals(lm(ModelAttention ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid + LogWordFreq.Centered, data=data, na.action=na.exclude))



model.Null.fix = lme4::glmer(FIXATED ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=data, REML=FALSE, family="binomial")

#anova(model.Plain.tt, model.Null.tt)

model.Slope.fix = lme4::glmer(FIXATED ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid  + ModelAttention.Resid +  (ModelAttention.Resid|tokenID) + (ModelAttention.Resid|Participant) + LogWordFreq.Centered +  (1|tokenID) + (1|Participant), data=data, REML=FALSE, family="binomial")

anova(model.Plain.fix, model.Null.fix)




dataNTT = dataN[data$tt > 0,]

# removed PositionInHumanSentence.Centered + 
#model.Plain.tt = lme4::lmer(tt ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid  + ModelAttention.Resid +  LogWordFreq.Centered +  (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model.Null.tt = lme4::lmer(tt ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataNTT, REML=FALSE)

#anova(model.Plain.tt, model.Null.tt)

model.Slope.tt = lme4::lmer(tt ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid  + ModelAttention.Resid +  (ModelAttention.Resid|tokenID) + (ModelAttention.Resid|Participant) + LogWordFreq.Centered +  (1|tokenID) + (1|Participant), data=dataNTT, REML=FALSE)

anova(model.Slope.tt, model.Null.tt)


#model.Slope.tt = lme4::lmer(tt ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid  + ModelAttention.Resid +  LogWordFreq.Centered + (ModelAttention.Resid|tokenID) + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)


###############

#model.Plain.fp = lme4::lmer(fp ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid  + ModelAttention.Resid +  LogWordFreq.Centered + (ModelAttention.Resid|tokenID) + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model.Null.fp = lme4::lmer(fp ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model.Slope.fp = lme4::lmer(fp ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid  + ModelAttention.Resid +  (ModelAttention.Resid|tokenID) + (ModelAttention.Resid|Participant) + LogWordFreq.Centered +  (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)


anova(model.Slope.fp, model.Null.fp)

###############

#model.Plain.ff = lme4::lmer(ff ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid  + ModelAttention.Resid +  LogWordFreq.Centered + (ModelAttention.Resid|tokenID) + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model.Null.ff = lme4::lmer(ff ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model.Slope.ff = lme4::lmer(ff ~ HumanPosition.Centered +  ExperimentTokenLength.Centered.Resid  + IsNamedEntity.Centered +  Surprisal.Centered.Resid  + ModelAttention.Resid +  (ModelAttention.Resid|tokenID) + (ModelAttention.Resid|Participant) + LogWordFreq.Centered +  (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)


anova(model.Slope.ff, model.Null.ff)


  renamePredictorsInStringOutput(texreg(c(model.Slope.ff, model.Slope.fp, model.Slope.tt) ,single.row = TRUE))



}






##############################
##############################

getMultipleModelsBothConditions = function(use1400 = TRUE) {
  dataP = getMultipleModels("preview", use1400)
  dataNP = getMultipleModels("nopreview", use1400)
          data <- rbind(dataP,dataNP)


dummy = model.matrix( ~ Condition -1, data=data)
data$Condition.dummy = dummy[,2] - 0.5
data = centerColumn(data, "Condition.dummy") 

return(data)
}


createUniqueItemIDsByColumnName = function(data, columnName) {
   newName = (paste(columnName,"Renumbered",sep="."))
   data[,newName] = as.integer(factor(data[,columnName]))
   return (data)
}

getMultipleModelsBothConditionsSample1400 = function() {
  crash()
  dataP = getMultipleModelsSample("preview")
  dataNP = getMultipleModelsSample("nopreview")
          data <- rbind(dataP,dataNP)


dummy = model.matrix( ~ Condition -1, data=data)
data$Condition.dummy = dummy[,2] - 0.5
data = centerColumn(data, "Condition.dummy") 

data = data[!is.na(data$LuaID),]
data = data[data$LuaID < 50000,]

write.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-processed.csv-1400", x=data)

return(data)
}

# July 2017
getMultipleModelsBothConditionsSample = function() {
  crash()
  dataP = getMultipleModelsSample("preview")
  dataNP = getMultipleModelsSample("nopreview")
          data <- rbind(dataP,dataNP)


dummy = model.matrix( ~ Condition -1, data=data)
data$Condition.dummy = dummy[,2] - 0.5
data = centerColumn(data, "Condition.dummy") 

data = data[!is.na(data$LuaID),]
data = data[data$LuaID < 50000,]

write.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-processed.csv", x=data)

return(data)
}

# July 2017
readMultipleModelsSampleFromFile = function() {
 data = read.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-processed.csv") #-1400")
 data$LogWordFreq = log(data$WordFreq)
 data = centerColumn(data, "LogWordFreq")

data$HumanPosition = data$HumanPosition.x


# restrict to some texts?
#data = data[data$TextNo.Renumbered %% 10 == 0,]

# remove words beyond the 500-words limit
data = data[!is.na(data$LuaID),]

# remove punctuation
data = data[!data$IsPunctuation,]

# remove OOV
data = data[data$LuaID < 49999,]


# remove named entities from LogWordFreq
logWFMean = mean(data$LogWordFreq[!data$IsNamedEntity], na.rm=TRUE)
data$LogWordFreq[data$IsNamedEntity] <- logWFMean

# remove non-named entities from IsCorrectAnswer
corAnsMean = mean(data$IsCorrectAnswer[data$IsNamedEntity], na.rm=TRUE)
data$IsCorrectAnswer = data$IsCorrectAnswer+0
data$IsCorrectAnswer[!data$IsNamedEntity] <- corAnsMean




 return(data)
}

# July 15
getMultipleModelsBothConditionsSampleFromFile = function() {
  dataP = read.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-processed-preview.csv")
  dataNP = read.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-processed-nopreview.csv")
          data <- rbind(dataP,dataNP)


dummy = model.matrix( ~ Condition -1, data=data)
data$Condition.dummy = dummy[,2] - 0.5
data = centerColumn(data, "Condition.dummy") 

data = data[!is.na(data$LuaID),]
data = data[data$LuaID < 50000,]

write.csv("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-processed.csv", x=data)

return(data)
}


# July 15
prepareMultipleModelsSampleData = function(condition) {
  dataP = getMultipleModelsSample(condition)
  write.csv(paste("/afs/cs.stanford.edu/u/mhahn/scr/cuny-sample-processed-",condition,".csv", sep=""), x=dataP)
  return(dataP)
}


getMultipleModelsSample = function(condition) {
  texts = sampleTextsNames()

#  participants = c(1,2,3,4,5,6,7,8,9,10,11,12)


if(FALSE) {
  models = c(300, 301, 302, 306, 307, 308, 310, 311, 312, 313, 316, 317, 318, 319, 810, 850, 851, 852, 860, 861, 870, 880, 884, 990)
} else {
  models = c(1400, 1401, 1402, 1403, 1410, 1411, 1412, 1420, 1421, 1422, 1430, 1431, 1432, 1450, 1451, 1452, 1460, 1461, 1462, 1463, 1480, 1481, 1482, 1490, 1491, 1492)
}


  
  modelNum = 1
  text = texts[1]

  modelFile = paste("att-",condition,"-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",models[modelNum],"-l1-nll", sep="")
  dataModel = readModelData(modelFile,text,"sample")
  dataExternal = readExternalData(text,"sample") 
#  dataSurprisal = readSurprisalData(text,"sample")
  data = merge(dataExternal,dataModel, by=c("JointPosition"))
#  data = merge(data,dataSurprisal, by=c("JointPosition"))
  data$model = models[modelNum]
  data$TextNo = text

  for (text in texts) {
   for (modelNum in 1:length(models)) {
     if(!(text == texts[1] && modelNum == models[1])) {

      modelFile = paste("att-",condition,"-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",models[modelNum],"-l1-nll", sep="")
#      if(file.exists(paste("forEvaluation/questions/annotation-textsonly/",modelFile,sep=""))) {
        cat(modelFile,sep="\n")

          dataExternal = readExternalData(text,"sample")
#          dataSurprisal = readSurprisalData(text,"sample")
# July 15
          dataModel = readModelData(modelFile,text,"sample")

          dataNew = merge(dataExternal,dataModel, by=c("JointPosition"))
#          dataNew = merge(dataNew,dataSurprisal, by=c("JointPosition"))
          dataNew$model = models[modelNum]
          dataNew$TextNo = text
          data <- rbind(data,dataNew)
 #     }
    }
   }
  }

  data = createUniqueItemIDsByColumnName(data, "TextNo")
  data$tokenID = 10000 * data$TextNo.Renumbered + data$JointPosition

  data$HumanPosition = data$HumanPosition.x
  data = centerColumn(data, "HumanPosition") 
  data$Condition = condition



  data$IsPunctuation = (data$POS.Universal == ".")
  data$FunctionWord = (data$POS.Universal =="ADP"  | data$POS.Universal =="ADV" | data$POS.Universal =="CONJ" | data$POS.Universal =="DET" | data$POS.Universal =="PRON" | data$POS.Universal =="PRT")
  data = centerColumn(data, "IsPunctuation")
  data = centerColumn(data, "FunctionWord")
  data = centerColumn(data, "ExperimentTokenLength")
  data = centerColumn(data, "IsNamedEntity")
  data = centerColumn(data, "IsCorrectAnswer")
  data = centerColumn(data, "OccursInQuestion")
#  data = centerColumn(data, "Surprisal")


  data = centerColumn(data, "PositionInHumanSentence")
  return(data)
}






getMultipleModels = function(condition, use1400 = FALSE, partition = "dev") {
  if(partition == "dev") {
     texts = c(2,4,6,8,10,12,14,16,18,20)
  } else {
     crash()
  }
#  participants = c(1,2,3,4,5,6,7,8,9,10,11,12)


if(!use1400) {
  models = c(300, 301, 302, 306, 307, 308, 310, 311, 312, 313, 316, 317, 318, 319, 810, 850, 851, 852, 860, 861, 870, 880, 884, 990)
} else {
  models = c(1400, 1401, 1402, 1403, 1410, 1411, 1412, 1420, 1421, 1422, 1430, 1431, 1432, 1450, 1451, 1452, 1460, 1461, 1462, 1463, 1480, 1481, 1482, 1490, 1491, 1492)
}

  
  modelNum = 1
  text = 2

  modelFile = paste("att-",condition,"-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",models[modelNum],"-l1-nll", sep="")
  dataModel = readModelData(modelFile,text,"eyetrack")
  dataExternal = readExternalData(text,"eyetrack") 
  dataSurprisal = readSurprisalData(text,"eyetrack")
  data = merge(dataExternal,dataModel, by=c("JointPosition"))
  data = merge(data,dataSurprisal, by=c("JointPosition"))
  data$model = models[modelNum]
  data$TextNo = text

  for (text in texts) {
   for (modelNum in 1:length(models)) {
     if(!(text == 2 && modelNum == 1)) {

      modelFile = paste("att-",condition,"-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",models[modelNum],"-l1-nll", sep="")
#      if(file.exists(paste("forEvaluation/questions/annotation-textsonly/",modelFile,sep=""))) {
        cat(modelFile,sep="\n")

          dataExternal = readExternalData(text,"eyetrack")
          dataSurprisal = readSurprisalData(text,"eyetrack")

          dataModel = readModelData(modelFile,text,"eyetrack")

          dataNew = merge(dataExternal,dataModel, by=c("JointPosition"))
          dataNew = merge(dataNew,dataSurprisal, by=c("JointPosition"))
          dataNew$model = models[modelNum]
          dataNew$TextNo = text
          data <- rbind(data,dataNew)
 #     }
    }
   }
  }



  data$tokenID = 10000 * data$TextNo + data$JointPosition

  data = centerColumn(data, "HumanPosition") 
  data$Condition = condition



  data$IsPunctuation = (data$POS.Universal == ".")
  data$FunctionWord = (data$POS.Universal =="ADP"  | data$POS.Universal =="ADV" | data$POS.Universal =="CONJ" | data$POS.Universal =="DET" | data$POS.Universal =="PRON" | data$POS.Universal =="PRT")
  data = centerColumn(data, "IsPunctuation")
  data = centerColumn(data, "FunctionWord")
  data = centerColumn(data, "ExperimentTokenLength")
  data = centerColumn(data, "IsNamedEntity")
  data = centerColumn(data, "IsCorrectAnswer")
  data = centerColumn(data, "OccursInQuestion")
  data = centerColumn(data, "Surprisal")


  data = centerColumn(data, "PositionInHumanSentence")
  return(data)
}































##############################
##############################


evaluateAllModelsAgainstHumanDataCond = function() {
  sink("~/scr/results-against-human-cond.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + Condition.dummy.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model2 = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + Condition.dummy.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=TRUE)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}



evaluateAllModelsAgainstHumanDataInter = function() {
  sink("~/scr/results-against-human-inter.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered*Condition.dummy.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model2 = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + Condition.dummy.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}


evaluateAllModelsAgainstHumanDataLogTT = function() {
  sink("~/scr/results-against-human-log-tt.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[dataN$tt < 1000,]

dataN$tt.log = log(dataN$tt)

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt.log ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

model2 = lme4::lmer(tt.log ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}


evaluateAllModelsAgainstHumanDataTT1400 = function() {
  sink("~/scr/results-against-human-tt-1400.txt")
  models1400 = c(1400, 1401, 1402, 1403, 1410, 1411, 1412, 1420, 1421, 1422, 1430, 1431, 1432, 1450, 1451, 1452, 1460, 1461, 1462, 1463, 1480, 1481, 1482, 1490, 1491, 1492)

  for(j in (1:length(models1400))) {
    cat(j,sep="\n")
    i = models1400[j]
    data = initialize(i,i)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model2 = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }
  cat("Have finished the iteration")

  sink()

}




evaluateAllModelsAgainstHumanDataTT = function() {
  sink("~/scr/results-against-human-tt.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

model2 = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}

evaluateAllModelsAgainstHumanDataTTSlope = function() {
  sink("~/scr/results-against-human-tt-slope.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + (AttentionScore.Centered|Participant) + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

model2 = lme4::lmer(tt ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN, REML=FALSE)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}



evaluateAllModelsAgainstHumanData = function() {
  sink("~/scr/results-against-human.txt")
  for(i in 31:54) {
    data = initialize(i,i-14)


data$LogWordFreq = log(data$WordFreq)
data = centerColumn(data,"LogWordFreq")
dataN = data[data$fp>0,]

dataN = dataN[complete.cases(dataN[,c("AttentionScore")]),]

model = lme4::lmer(fp ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + AttentionScore.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

model2 = lme4::lmer(fp ~ PositionInHumanSentence.Centered + HumanPosition.Centered +  ExperimentTokenLength.Centered  + IsNamedEntity.Centered +  Surprisal.Centered + FunctionWord.Centered + LogWordFreq.Centered + (1|tokenID) + (1|Participant), data=dataN)

cat("Corrected ------------------\n")
cat(getModelFilesName("preview",i),sep="\n")
cat(AIC(model)-AIC(model2),sep="\n")
cat(capture.output(anova(model,model2)),sep="\n")




  }

  sink()

}



getModelFilesName = function(condition, model) {
  cat(paste(condition,model,sep=" "),sep="\n")

  if(model > 1399) {
    modelFile = paste("att-",condition,"-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-",model,"-l1-nll",sep="")
  } else if (condition == "preview" && model == 1) {
    modelFile = "att-pg-test-neat-qa-500-512-0.7-100-R-0.5again-qt-hard"
  } else if (condition == "preview" && model ==2) {
    modelFile = "att-pg-test-neat-qa-500-512-0.7-100-R-0.25again-qt-hard-2"
  } else if (condition == "preview" && model == 3) {
    modelFile = "att-pg-test-neat-qa-500-512-0.7-100-R-0.5again-qt-hard-REDONE"
  } else if (condition =="preview" && model == 4) {
    modelFile = "att-pg-test-neat-qa-500-512-0.7-100-R-0.8again-qt-hard-3"
  } else if(condition == "preview" && model == 5) {
    modelFile = "att-pg-test-neat-qa-500-512-0.7-100-R-0.8again-qt-hard-2-REDONE"
  } else if(condition == "preview" && model == 6) {
    modelFile = "att-pg-test-neat-qa-500-512-0.7-100-R-0.8cuny-qt-1"
  } else if(condition == "preview" && model == 7) {
    modelFile = "att-pg-test-neat-qa-500-512-0.7-100-R-0.8cuny-qt-2"
  } else if(condition == "preview" && model == 8) {
    modelFile = "att-pg-test-neat-qa-500-512-0.7-100-R-0.8cuny-qt-3"
  } else if(condition == "preview") {
    modelFile = c( "att-pg-test-neat-qa-1200-128-0.7-100-R-0.8-R2cuny-split-bcontinuing-2-pilotattention-8-nll","att-pg-test-neat-qa-1200-128-0.7-100-R-7-R2cuny-split-bcontinuing-2-pilotattention-9-nll","att-pg-test-neat-qa-1200-128-0.7-100-R-9-R2cuny-split-bcontinuing-2-pilotattention-9-nll","att-pg-test-neat-qa-500-128-0.7-100-R-0.5-R2cuny-split-bcontinuing-2-pilotattention-14","att-pg-test-neat-qa-500-128-0.7-100-R-0.8-R2cuny-split-bcontinuing-2-pilotattention-12","att-pg-test-neat-qa-500-128-0.7-100-R-0.8-R2cuny-split-bcontinuing-2-pilotattention-14","att-pg-test-neat-qa-500-128-0.7-100-R-3-R2cuny-split-bcontinuing-2-pilotattention-15-nll","att-pg-test-neat-qa-500-128-0.7-100-R-5-R2cuny-split-bcontinuing-2-pilotattention-16-nll","att-pg-test-neat-qa-500-128-0.7-100-R-8-R2cuny-split-bcontinuing-2-pilotattention-15-nll" , "att-pg-test-neat-qa-500-128-0.7-100-R-5-R2cuny-split-bcontinuing-2-pilotattention-21-nll","att-pg-test-neat-qa-500-128-0.7-100-R-5-R2cuny-split-bcontinuing-2-pilotattention-22-nll","att-pg-test-neat-qa-500-128-0.7-100-R-5-R2cuny-split-bcontinuing-2-pilotattention-32-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-83-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-84-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-85-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-86-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-87-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-88-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-89-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-103-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-112-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-113-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-300-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-301-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-302-l1-nll",
#"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-303-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-306-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-307-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-308-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-310-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-311-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-312-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-313-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-316-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-317-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-318-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-319-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-810-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-850-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-851-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-852-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-860-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-861-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-870-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-880-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-884-l1-nll",
#"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-900-l1-nll",
#"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-950-l1-nll",
"att-preview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-990-l1-nll"
  )[model-8]
  } else if (condition == "nopreview" && model == 1) {
    modelFile = "att-pg-test-neat-qa-500-512-0.7-100-R-0.5again-tq-hard"
  } else if (condition == "nopreview" && model == 2) {
    modelFile = "att-pg-test-neat-qa-500-512-0.7-100-R-0.25again-tq-hard-2" #for now, only this model is good
  } else if(condition == "nopreview") {
    modelFile = c( "att-pg-test-neat-qa-500-128-0.7-100-R-10-R2cuny-split-bcontinuing-2-pilotattention-18-nll-nopreview","att-pg-test-neat-qa-500-128-0.7-100-R-8-R2cuny-split-bcontinuing-2-pilotattention-17-nll-nopreview","att-pg-test-neat-qa-500-128-0.7-100-R-5-R2cuny-split-bcontinuing-2-pilotattention-20-nll-nopreview","att-pg-test-neat-qa-500-128-0.7-100-R-8-R2cuny-split-bcontinuing-2-pilotattention-19-nll-nopreview",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-83-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-84-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-85-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-86-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-87-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-88-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-89-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-103-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-112-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-113-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-300-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-301-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-302-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-306-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-307-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-308-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-310-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-311-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-312-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-313-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-316-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-317-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-318-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-319-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-810-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-850-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-851-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-852-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-860-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-861-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-870-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-880-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-884-l1-nll",
"att-nopreview-pg-test-neat-qa-500-128-0-100-R-6-R2cuny-split-bcontinuing-2-pilotattention-990-l1-nll"
)[model-2]
  } else {
    crash()
  }


  cat(modelFile,sep="\n")


  return(modelFile)

}


readSurprisalData = function(text, dataset) {
  modelFile = "surp-pg-test-langmod-100-256-0.7-100langmod-joint-february"
  if(dataset == "eyetrack") {
    return(read.csv(paste(  "forEvaluation/questions/annotation-textsonly/",modelFile,"/E-",text,".csv", sep=""),header=TRUE,sep="\t"))
  } else if(dataset == "sample") {
    crash()
  } else {
    crash()
  }
}


readExternalEntropy = function() {
   path = "/juicier/scr120/scr/mhahn/qp/run_on_eyetrack-with-info.txt"
#   path = "/juicier/scr120/scr/mhahn/qp/entropy-surprisal-eyetrack.csv"
    dataEnt = (read.csv(path,header=TRUE,sep="\t"))
  dataEnt$JointPosition = dataEnt$JointPosition+2
  return(dataEnt)
}
# data2 = merge(data, dataEnt, by=c("TextNo", "JointPosition"))
#  data2 = data2[!(as.character(data2$Token.TF) == "UnitedHealth"),]
# entropy is a strong predictor in addition to surprisal!
#
# summary(lmer(fp ~ Surprisal.TF + Info.0 + LogWordFreq + Condition + (1|tokenID) + (1|Participant), data=data2))
# `Info' does not seem to work well. But real one-word entropy is a good predictor
sandbox = function() {

 data2 = merge(data, dataEnt, by=c("TextNo", "JointPosition"))
  data2 = data2[!(as.character(data2$Token.TF) == "UnitedHealth"),]

 data2$Info.5 = (data2$Info.5 - mean(data2$Info.5)) / mean(data2$Info.5)
 data2$Info.6 = (data2$Info.6 - mean(data2$Info.6)) / mean(data2$Info.6)
 data2$Info.7 = (data2$Info.7 - mean(data2$Info.7)) / mean(data2$Info.7)
 data2$Info.8 = (data2$Info.8 - mean(data2$Info.8)) / mean(data2$Info.8)

 data2$Info.0.R = resid(lm(data2$Info.0 ~ data2$LogWordFreq + data2$Surprisal.TF, na.action=na.exclude))
 data2$Info.1.R = resid(lm(data2$Info.1 ~ data2$LogWordFreq +  data2$Surprisal.TF + data2$Info.0, na.action=na.exclude))
 data2$Info.2.R = resid(lm(data2$Info.2 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0+ data2$Info.1, na.action=na.exclude))
 data2$Info.3.R = resid(lm(data2$Info.3 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2, na.action=na.exclude))
 data2$Info.4.R = resid(lm(data2$Info.4 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2 + data2$Info.3, na.action=na.exclude))
 data2$Info.5.R = resid(lm(data2$Info.5 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2 + data2$Info.3 + data2$Info.4, na.action=na.exclude))
 data2$Info.6.R = resid(lm(data2$Info.6 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2 + data2$Info.3 + data2$Info.4 + data2$Info.5, na.action=na.exclude))
 data2$Info.7.R = resid(lm(data2$Info.7 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2 + data2$Info.3 + data2$Info.4 + data2$Info.5 + data2$Info.6, na.action=na.exclude))
 data2$Info.8.R = resid(lm(data2$Info.8 ~  data2$LogWordFreq + data2$Surprisal.TF + data2$Info.0 + data2$Info.1 + data2$Info.2 + data2$Info.3 + data2$Info.4 + data2$Info.5 + data2$Info.6 + data2$Info.7, na.action=na.exclude))

model = lmer(fp ~ Surprisal.TF + Info.0.R + Info.1.R + Info.2.R + Info.3.R + Info.4.R + Info.5.R + Info.6.R + Info.7.R + Info.8.R + LogWordFreq + Condition + (1|tokenID) + (1|Participant), data=data2)

summary(model)
}

readCoefficients = function(modelFile, text, dataset) {
  if(dataset == "eyetrack") {
    return(read.csv(paste("forEvaluation/questions/annotation-textsonly/",modelFile,"/E-",text,".coef.csv", sep=""),header=TRUE,sep="\t"))
  } else if(dataset == "sample") {
    return(read.csv(paste("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/annotation-textsonly/",modelFile,"/",text,".coef.csv", sep=""),header=TRUE,sep="\t"))
  }
  crash()
}

readModelData = function(modelFile, text, dataset) {
  if(dataset == "eyetrack") {
    return(read.csv(paste("forEvaluation/questions/annotation-textsonly/",modelFile,"/E-",text,".csv", sep=""),header=TRUE,sep="\t"))
  } else if(dataset == "sample") {
    cat(paste("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/annotation-textsonly/",modelFile,"/",text,".csv", sep=""),sep="\n")
    return(read.csv(paste("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/annotation-textsonly/",modelFile,"/",text,".csv", sep=""),header=TRUE,sep="\t"))
  } else {
    crash()
  }
}

readExternalData = function(text, dataset) {
  if(dataset == "eyetrack") {
    return(read.csv(paste("forEvaluation/mappingWithExternal/E-",text,".csv",sep = ""),header=TRUE,sep="\t"))
  } else if(dataset == "sample") {
    cat(paste("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/mappingWithExternal/",text,".csv", sep=""), sep="\n")
    return(read.csv(paste("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/mappingWithExternal/",text,".csv", sep=""),header=TRUE,sep="\t"))
  } else {
    crash()
  }
}


initializeModelOnlyForCondition = function(condition, model, dataset) {
  if(dataset == "sample") {
     modelFile = getModelFilesName(condition,model)
  dataModel = readModelData(modelFile,"merged","sample")
  dataExternal = readExternalData("merged","sample")

  data = merge(dataExternal,dataModel, by=c("AnonymizedPosition","TextID"))
  cat("Warning: HumanPosition, ExperimentToken are reliable only in one of the two tables because of an upstream bug.",sep="\n\n")



  data$IsPunctuation = (data$POS.Universal == ".")
  data$FunctionWord = (data$POS.Universal =="ADP"  | data$POS.Universal =="ADV" | data$POS.Universal =="CONJ" | data$POS.Universal =="DET" | data$POS.Universal =="PRON" | data$POS.Universal =="PRT")
  data = centerColumn(data, "IsPunctuation")
  data = centerColumn(data, "FunctionWord")

  data = centerColumn(data, "PositionInHumanSentence")
  #data = center(data)


data$Condition <- condition

 
  return(data)
   


  } else {
    crash()
  }
}


#  cat("(1) 17 was not recorded for participant 9 in NoPreview -> exclude", sep="\n")
  cat("(2) participants do not distinguish between conditions, has to be fixed", sep="\n")
#  crash()

initializeForCondition = function(condition, model, partition = "dev") {
#  cat("(1) 17 was not recorded for participant 9 in NoPreview -> exclude", sep="\n")
#  cat("(2) participants do not distinguish between conditions, has to be fixed", sep="\n")
#  crash()
  if(partition == "dev") {
    texts = c(2,4,6,8,10,12,14,16,18,20)
    participants = c(1,2,3,4,5,6,7,8,9,10,11,12)
  } else if(partition == "disjoint") {
    texts = c(1,3,5,7,9,11,13,15,17,19)
    participants = c(1,2,3,4,5,6,7,8,9,10,11,12)
  } else if(partition %in% c("full","total")) { # note that this is the whole set, NOT the test partition
    texts = c(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20)
    participants = c(1,2,3,4,5,6,7,8,9,10,11,12)
  }
  modelFile = getModelFilesName(condition,model)
  text = 2
  participant = 2 
  dataModel = readModelData(modelFile,text,"eyetrack")
  dataHuman = read.csv(paste("forEvaluation/mappingWithHuman/",condition,"/Participant-",participant,"/E-",text,".csv",sep = ""),header=TRUE,sep="\t")
  dataExternal = readExternalData(text,"eyetrack") 
  dataSurprisal = readSurprisalData(text,"eyetrack")

  data = merge(dataHuman, dataModel, by=c("JointPosition"))
  data = merge(dataExternal,data, by=c("JointPosition"))
  data = merge(data,dataSurprisal, by=c("JointPosition"))

  for (text in texts) {
    for (participant in participants) {
      if(!(text == texts[1] && participant == participant[1])) {
          # exclude nonnative participants
          if(TRUE) {
            if(participant == 9 && condition == "nopreview") { # participant 17
              next
            }
            if(participant == 3 && condition == "nopreview") { # participant 5
              next
            }
          }
          cat(paste("forEvaluation/questions/annotation-textsonly/",modelFile,"/E-",text,".csv", sep=""),sep="\n")
          dataModel = readModelData(modelFile,text,"eyetrack")
          cat( paste("forEvaluation/mappingWithHuman/",condition,"/Participant-",participant,"/E-",text,".csv",sep = ""),sep="\n")
          dataHuman = read.csv(paste("forEvaluation/mappingWithHuman/",condition,"/Participant-",participant,"/E-",text,".csv",sep = ""),header=TRUE,sep="\t")
          dataExternal = readExternalData(text,"eyetrack")
          dataSurprisal = readSurprisalData(text,"eyetrack")

  dataNew = merge(dataHuman, dataModel, by=c("JointPosition"))
  dataNew = merge(dataExternal,dataNew, by=c("JointPosition"))
  dataNew = merge(dataNew,dataSurprisal, by=c("JointPosition"))


          data <- rbind(data,dataNew)
# it should be here that ends of texts are removed

      }
    }
  }

 # now put into one large csv, across readers and texts (only for development set)

  data$tokenID = 10000 * data$TextNo + data$JointPosition

        data$FIXATED = pmin(data$fp, 1)
  data$IsPunctuation = (data$POS.Universal == ".")
  data$FunctionWord = (data$POS.Universal =="ADP"  | data$POS.Universal =="ADV" | data$POS.Universal =="CONJ" | data$POS.Universal =="DET" | data$POS.Universal =="PRON" | data$POS.Universal =="PRT")
  data = centerColumn(data, "IsPunctuation")
  data = centerColumn(data, "FunctionWord")

  data = centerColumn(data, "PositionInHumanSentence")
  #data = center(data)
 
  return(data)
#  data = processDataForEvaluation(data)
#  return(data)
}

centerColumn = function(data,columnName) {
  newName = (paste(columnName,"Centered",sep="."))
  data[,newName] <- data[,columnName] - mean(data[,columnName], na.rm = TRUE)
  data[,newName] <- data[,newName] / sd(data[,columnName], na.rm = TRUE)
  return(data)
}

centerColumnDiscrete = function(data,columnName) {
  newName = (paste(columnName,"Centered",sep="."))
  data[,newName] <- data[,columnName] - mean(data[,columnName], na.rm = TRUE)
  span = max(data[,newName]) - min(data[,newName])
  data[,newName] <- data[,newName] / span 
  return(data)
}



###############################################
###############################################
getHumanData = function(data) {
   data = data[!is.na(data$HumanPosition),]
   # http://stackoverflow.com/questions/26997586/r-shifting-a-vector
   comesBeforeBreak <-c(rep(TRUE,times=1), data$IsFirstInLine[1:(length(data$IsFirstInLine)-1)]) 
   comesBeforeBreak2 <-c(rep(TRUE,times=2), data$IsFirstInLine[1:(length(data$IsFirstInLine)-2)]) 
   comesAfterBreak <-c(data$IsFirstInLine[2:(length(data$IsFirstInLine))],   rep(TRUE,times=1)) 
   data$IsCloseToLinebreak = data$IsFirstInLine | comesBeforeBreak #| comesBeforeBreak2 | comesAfterBreak
   data = data[!data$IsCloseToLinebreak,]
   return(data)
}
##########################################
##########################################
extractNamedEntities = function(data) {
  dataNE = data[data$IsNamedEntity == TRUE,]
  return(dataNE)
}



##########################################
##########################################

doBasicPreprocessing = function(data) {
# ADDING SOME COLUMNS
	data$NotCentered.AttentionScore = data$AttentionScore
        data$FIXATED = pmin(data$fp, 1)

if(FALSE) {
##########################################
# Merging together the -1 rows to undo the tokenization

		auxColumn.Att = NA * data$AttentionScore
                auxColumn.Dec = NA * data$AttentionScore

		accumulator.Att = 0
                accumulator.Dec = 0
		lastRealOne = -1
		numberOfTokensInCurrentToken = 0
		for(i in (1:length(data$JointPosition))) {
cat(accumulator.Att, sep="\n")

			if(is.na(data$HumanPosition.x[i])){ #is not a human thing by itself (but possibly not an anonymized either)
                                    if(!is.na(data$AttentionScore[i])) { # now we know it's in the anonymized version but not the experimental version
					accumulator.Att = data$AttentionScore[i] + accumulator.Att
					accumulator.Dec = data$AttentionDecision[i] + accumulator.Dec

					numberOfTokensInCurrentToken = numberOfTokensInCurrentToken + 1
                                    }
			} else { # is a human token
				auxColumn.Att[lastRealOne] = accumulator.Att / numberOfTokensInCurrentToken #+ data$Attention[lastRealOne]
				auxColumn.Dec[lastRealOne] = accumulator.Dec #+ data$Attention[lastRealOne]

				lastRealOne = i
                                if(!is.na(data$AttentionScore[i])) { # quite possibly, it is not a token for the anonymized version
  				   accumulator.Att = data$AttentionScore[i]
  				   accumulator.Dec = data$AttentionDecision[i]
                                } else {
                                   accumulator.Att = 0
                                   accumulator.Dec = 0
                                }
				numberOfTokensInCurrentToken = 1
			}
		}
                # note that the last word in the text has not yet been assigned the attention score

		auxColumn.Att[lastRealOne] = accumulator.Att / numberOfTokensInCurrentToken #+ data$Attention[lastRealOne]
		auxColumn.Dec[lastRealOne] = accumulator.Dec #+ data$Attention[lastRealOne]

		data$CumulativeAttentionScore = auxColumn.Att
		data$CumulativeAttentionDecision = auxColumn.Dec

#                data$CumulativeAttentionDecisions = pmin(data$fp, 1)


		data2 = data[!is.na(data$HumanPosition.x),]
                
   # note that initial and final things have not yet been removed

	maxPosition = max(data2$HumanPosition.y)
	data2 <- data2[ which(data2$HumanPosition.y > 3 & data2$HumanPosition.y < maxPosition-2), ]

# now remove things that are not in the anonymized version
data2 = data2[!is.na(data2$LuaID),]
} else {
  data2 = data
}

	maxPosition = max(data2$HumanPosition.y)
	data2 <- data2[ which(data2$HumanPosition.y > 3 & data2$HumanPosition.y < maxPosition-2), ]


   
   return(data2)

}


##########################
##########################


removeTrackLoss = function(data) {
   data = data[!is.na(data$FIXATED),]
		if(TRUE){
			column = data$LuaID
				for(i in (1:length(column))) {
					`[`(column, i) = 1
				}
			for(i in (1:(length(column)-3))) {
				if(`[`(data$FIXATED, i) == 0 && `[`(data$FIXATED, i+1) == 0 && `[`(data$FIXATED, i+2) == 0 && `[`(data$FIXATED, i+3) == 0) {
					`[`(column, i)   = 0
						`[`(column, i+1) = 0
						`[`(column, i+2) = 0
						`[`(column, i+3) = 0
				}
			}
			data <- data[column == 1,]
		} else{
			data <- data
		}



return(data)
}






createUniqueItemIDs = function(data) {
   uniqueLabels = unique(data$tokenID)
   labelToPosition = integer(max(uniqueLabels))
   for(i in (1:(length(uniqueLabels)))) {
     labelToPosition[uniqueLabels[i]] = i
   }
   renumberedItemNumbers = data$tokenID
   for(i in (1:(length(renumberedItemNumbers)))) {
      renumberedItemNumbers[i] = labelToPosition[renumberedItemNumbers[i]]
   }
   data$tokenID.Renumbered <- renumberedItemNumbers
   return(data)
}








processDataForEvaluation = function(data, CSV_FILE, partNum)
{
########################################
#       PREPROCESSING
########################################



	data = doBasicPreprocessing(data)

# is 0 for skipped elements
		data$WLEN <- NULL


if(FALSE){ # TODO not yet adapted
	columnPREVFIX = data$Surprisal
		columnFRQLAST = data$Surprisal
		columnLASTFIX = data$Surprisal

		columnPREVFIX[1] = 0
		columnFRQLAST[1] = NA
		columnLASTFIX[1] = NA
		lastFix = NA
		for(i in (2:length(data$Surprisal))) {
			if((!is.na(data$Itemno[i])) && data$Itemno[i] == data$Itemno[i-1]) {
				if(data$FIXATED[i-1] == 1){
					columnPREVFIX[i] = 1
				} else {
					columnPREVFIX[i] = 0
				}
				columnFRQLAST[i] = data$WordFreq[i-1]
					columnLASTFIX[i] = i-lastFix
					if(!is.na(i-lastFix) && (i-lastFix > 3)) { # this would count as track loss
						columnLASTFIX[i] = NA
					}
			} else {
				columnPREVFIX[i] = NA
					lastFix = NA
					columnFRQLAST[i] = NA
					columnLASTFIX[i] = NA
			}
			if(data$FIXATED[i] == 1){
				lastFix = i
			}
		}
	data$PREVFIX = columnPREVFIX
		data$FRQLAST = columnFRQLAST
		data$LASTFIX = columnLASTFIX
}

#####################################
# model does not really deal with rare words

            if(FALSE) {
		data <- data[data$WordFreq > 0,]
		data <- data[data$WordFreq < 12,]


		if(FALSE){
			data = data[data$WordFreq < 12,]
				data = data[data$WordFreq > 0,]
		} else if(FALSE){
			data = data[data$WordFreq < 6,]
				data = data[data$WordFreq > 0,]
		}
           } else {
              data <- data[data$LuaID < 9000,]
           }
 



##########################################
# Create some additional measures





# PREVFIX: previous word fixated or not = ?TODO CREATE
#linear.Surp = lmer(C.FPASSD ~ C.WLEN+ C.FREQBIN + C.FRQLAST + C.TRACEIC + C.BACKTR + C.NBIGR + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + residSurprisal + (residSurprisal|C.ITEM) + (1|Participant) + (1|C.ITEM), data=data.Nozeros)# FRQLAST: frequency of previous word. TODO CREATE
# LASTFIX: distance of previous fixation in number of words. TODO CREATE




#########
#should still be like the Edinburgh version
return(data)
##########################################
# Removing regions with track loss (four successive non-fixated words)





#########################################




#cat("Full Data:\n")
#cat(capture.output(summary(data)), sep="\n")
#cat("\n\n")
#cat("Restricted Positions: \n")

#cat(capture.output(summary(data)), sep="\n")
#cat("\n\n")

#		data$C.ITEM <- 10000*data$C.FILE+data$C.WNUM

###### CLEANUP

		data <- data[data$tt < 2000,]
		data <- data[data$fp < 1000,]
		data <- data[data$fp == 0 || data$fp > 80,]


		return(data)

}



center = function(data) {

data$IsNamedEntity.Centered = data$IsNamedEntity - mean(data$IsNamedEntity,na.rm=TRUE)
data$OccursInQuestion.Centered = data$OccursInQuestion - mean(data$OccursInQuestion,na.rm=TRUE)
data$IsCorrectAnswer.Centered = data$IsCorrectAnswer - mean(data$IsCorrectAnswer,na.rm=TRUE)
		data$OccurrencesOfCorrectAnswerSoFar.Centered <- data$OccurrencesOfCorrectAnswerSoFar - mean(data$OccurrencesOfCorrectAnswerSoFar, na.rm = TRUE)



		data$ExperimentTokenLength.Centered <- data$ExperimentTokenLength - mean(data$ExperimentTokenLength, na.rm = TRUE)
		data$fp.Centered   <- data$fp  - mean(data$fp  , na.rm = TRUE)
		data$AttentionScore.Centered   <- data$AttentionScore  - mean(data$AttentionScore  , na.rm = TRUE)
		data$AttentionDecision.Centered   <- data$AttentionDecision  - mean(data$AttentionDecision  , na.rm = TRUE)


if(min(data$HumanPosition.x.1 == data$HumanPosition.x,na.rm=TRUE) == 0 || min(data$HumanPosition.x.1 == data$HumanPosition.y,na.rm=TRUE) == 0 || min(data$HumanPosition.y.1 == data$HumanPosition.y,na.rm=TRUE) == 0) {
  cat("ERROR 54815")
  return(data)
}
data$HumanPosition = data$HumanPosition.x

		data$HumanPosition.Centered   <- data$HumanPosition  - mean(data$HumanPosition  , na.rm = TRUE)
data$HumanPosition.Centered = data$HumanPosition.Centered / 300


if(mean(data$LuaID.x == data$LuaID.y,na.rm=TRUE) < 0.8) {
  cat("ERROR 558")
  return(data)
}
data$LuaID = data$LuaID.x

		data$LuaID.Centered   <- (data$LuaID  - mean(data$LuaID  , na.rm = TRUE)) / sd(data$LuaID, na.rm=TRUE)
#		data$   <- data$  - mean(data$  , na.rm = TRUE)
#		data$   <- data$  - mean(data$  , na.rm = TRUE)

		return(data)
}





##########################################
##########################################
getNozeros = function(data)
{

# remove where FDUR is NAN
	data.Nozeros <- data[data$fp > 0,]

#		data.Nozeros <- data.Nozeros[complete.cases(data.Nozeros[,1]), ]
#		data.Nozeros <- data.Nozeros[data.Nozeros$C.FPASSD < 1000,]
#		data.Nozeros <- data.Nozeros[data.Nozeros$C.FPASSD > 80,]


#		data.Nozeros$C.WLEN <- data.Nozeros$C.WLEN - mean(data.Nozeros$C.WLEN, na.rm = TRUE)
#		data.Nozeros$PREVFIX <- data.Nozeros$PREVFIX - mean(data.Nozeros$PREVFIX, na.rm = TRUE)
#		data.Nozeros$OBLP <- data.Nozeros$WDLP - mean(data.Nozeros$WDLP, na.rm = TRUE)
#		data.Nozeros$ID <- data.Nozeros$ID - mean(data.Nozeros$ID, na.rm = TRUE)
#		data.Nozeros$WordFreq <- data.Nozeros$WordFreq - mean(data.Nozeros$WordFreq, na.rm = TRUE)
#		data.Nozeros$FRQLAST <- data.Nozeros$FRQLAST - mean(data.Nozeros$FRQLAST, na.rm = TRUE)
##data.Nozeros$NBIGR <- data.Nozeros$NBIGR - mean(data.Nozeros$NBIGR, na.rm = TRUE)
##data.Nozeros$BACKTR <- data.Nozeros$BACKTR - mean(data.Nozeros$BACKTR, na.rm = TRUE)
#		data.Nozeros$LASTFIX <- data.Nozeros$LASTFIX - mean(data.Nozeros$LASTFIX, na.rm = TRUE)
#		data.Nozeros$Surprisal <- data.Nozeros$Surprisal - mean(data.Nozeros$Surprisal, na.rm = TRUE)
#		data.Nozeros$Attention <- data.Nozeros$Attention - mean(data.Nozeros$Attention, na.rm = TRUE)


		return(data.Nozeros)
}
##########################################
##########################################




































































#################
##########################
##########################


mse = function(a,b) {
   return(sum((a-b)^2) / length(a))
}

##########################
##########################



lastOnesVector = function(vec) {
	return(append(-1, vec[-c(length(vec))]))

}



##########################
##########################


performDiscretizedEvaluation =function(data) {
cat("\nA\n")
  predA = getThresholdPredictor(data, data$NotCentered.Attention)
  doDiscreteEvaluation(predA, data$C.FIXATED)
cat("\nS\n")
  predS = getThresholdPredictor(data, data$NotCentered.Surprisal)
  doDiscreteEvaluation(predS, data$C.FIXATED)


}



splitIntoReaders = function(data) {

	data$C.FIXATED = pmin(data$C.FIXNO, 1)

		da = data[data$Participant == "sa",]
		db = data[data$Participant == "sb",]
		dc = data[data$Participant == "sc",]
		dd = data[data$Participant == "sd",]
		de = data[data$Participant == "se",]
		df = data[data$Participant == "sf",]
		dg = data[data$Participant == "sg",]
		dh = data[data$Participant == "sh",]
		di = data[data$Participant == "si",]
		dj = data[data$Participant == "sj",]

		da = da[order(da$tokenID),]
		db = db[order(db$tokenID),]
		dc = dc[order(dc$tokenID),]
		dd = dd[order(dd$tokenID),]
		de = de[order(de$tokenID),]
		df = df[order(df$tokenID),]
		dg = dg[order(dg$tokenID),]
		dh = dh[order(dh$tokenID),]
		di = di[order(di$tokenID),]
		dj = dj[order(dj$tokenID),]

		datasets = list(da, db, dc, dd, de, df, dg, dh, di, dj)

		for(j in (1:length(datasets))) {
			dataset = datasets[[j]]
				for(i in 1:ncol(dataset)){
					dataset[is.na(dataset[,i]), i] <- mean(dataset[,i], na.rm = TRUE)
				}
			datasets[[j]] <- dataset
		}



	return(datasets)
}



##########################
##########################



# NOTE data will be altered
# Assumes -1s are already taken out
discreteEvaluationForLogisticModel = function(readers, trainer, tester) {

	da = readers[[trainer]]
		db = readers[[tester]]
		return(discreteEvaluationForLogisticModelOnOtherData(db,db))
}

discreteEvaluationForLogisticModelOnOtherData = function(da, db) {

	lm = glm(formula = C.FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST +   WordFreq + Attention, family = binomial("logit"), data=da)


		for(i in 1:ncol(db)){
			db[is.na(db[,i]), i] <- mean(db[,i], na.rm = TRUE)
		}


	resu = predict(lm, type="response", newdata =data.frame(Attention=db$Attention,  C.WLEN=db$C.WLEN, C.FREQBIN=db$C.FREQBIN , C.FRQLAST=db$C.FRQLAST ,    WordFreq=db$WordFreq), na.action=na.pass)


		doDiscreteEvaluation(db$C.FIXATED, getThresholdPredictor(db, resu))


		resu = (sign(resu - 0.5) + 1)/2
		doDiscreteEvaluation(db$C.FIXATED, resu)
		return(lm)
}



##########################
##########################




markLastFix = function(data) {
	attended = data$Attended
		data$PrevAttended <- append(-1, attended[-c(length(attended))])


		condProb = sum(data$Attended * data$PrevAttended) / sum(data$PrevAttended)
		marginalProb = mean(data$Attended)
		cat(condProb,"  ",marginalProb,"\n")

}



##########################
##########################



readStatisticalNumbersFromFiles = function(descriptor, partNum) {
	numberOfDataPoints = 6
		datapoints = list()

		files = system(paste(" ls /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "/annotation/perp-pg-test-",descriptor,sep=""), intern=TRUE)
		print(files)
		for(i in (1:length(files))) {
			data = read.csv(files[[i]], header=FALSE, sep="\t")
				for(j in (1:numberOfDataPoints)) {
					if(i == 1) {
						datapoints = append(datapoints, data[j,][1])
					} else {
						datapoints[[j]] = append(datapoints[[j]], data[j,][1])
					}
				}
		}
	print(datapoints)
		for(j in (1:numberOfDataPoints)) {
			cat("--",j,"\n")
				evalList(datapoints[[j]], 0)
		}
}



##########################
##########################



getGaussianVector = function(length) {
	gauvector = c(rnorm(length-1, mean=0, sd = 0.01), length)
		gauvector = pmax(pmin(gauvector, 1), -1)
		return(gauvector)
}


##########################
##########################



evaluateLanguageModelRuns = function() {


}



##########################
##########################



discreteEvaluationForRandom = function(fixationrate) {


	truePositives = fixationrate * fixationrate
		trueNegatives = (1-fixationrate) * (1-fixationrate)


		correctPredictions = truePositives+trueNegatives


		totalRealPositives = fixationrate
		totalRealNegatives = 1-fixationrate
		totalItems = 1

		accuracy = correctPredictions / totalItems
		fixPrecision = truePositives / fixationrate
		fixRecall = truePositives / fixationrate
		skipPrecision = trueNegatives / (1-fixationrate)
		skipRecall = trueNegatives / (1-fixationrate)

		f1Fix = 2 * (fixPrecision * fixRecall) / (fixPrecision + fixRecall)
		f1Skip = 2 * (skipPrecision * skipRecall) / (skipPrecision + skipRecall)


		cat("Accuracy   ", accuracy,"\n")
		cat("F1 Fix     ", f1Fix, "\n")
		cat("F1 Skip    ", f1Skip, "\n")

}


##########################
##########################




findThreshold = function(data, predictor) {
	return(sort(predictor)[floor((1-mean(data$C.FIXATED)) * length(data$C.FIXATED))])
}


##########################
##########################




predictWithThreshold = function(predictor, threshold) {
	return(sign(sign(predictor - threshold) +1))
}


##########################
##########################



# NOTE assumes C.FIXATED has not been centered
getThresholdPredictor = function(data, predictor) {
	threshold = findThreshold(data, predictor)
		binarizedPredictor = predictWithThreshold(predictor, threshold)
		return(binarizedPredictor)
}



##########################
##########################



compareHumans = function(data) {
	data$C.FIXATED = pmin(data$C.FIXNO, 1)

		da = data[data$Participant == "sa",]
		db = data[data$Participant == "sb",]
		dc = data[data$Participant == "sc",]
		dd = data[data$Participant == "sd",]
		de = data[data$Participant == "se",]
		df = data[data$Participant == "sf",]
		dg = data[data$Participant == "sg",]
		dh = data[data$Participant == "sh",]
		di = data[data$Participant == "si",]
		dj = data[data$Participant == "sj",]

		da = da[order(da$tokenID),]
		db = db[order(db$tokenID),]
		dc = dc[order(dc$tokenID),]
		dd = dd[order(dd$tokenID),]
		de = de[order(de$tokenID),]
		df = df[order(df$tokenID),]
		dg = dg[order(dg$tokenID),]
		dh = dh[order(dh$tokenID),]
		di = di[order(di$tokenID),]
		dj = dj[order(dj$tokenID),]

		ppa = getThresholdPredictor(da, (db$FIXATED + dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*da$FIXATED
		ppb = getThresholdPredictor(db, (da$FIXATED + dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*db$FIXATED
		ppc = getThresholdPredictor(dc, (da$FIXATED + db$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dc$FIXATED
		ppd = getThresholdPredictor(dd, (da$FIXATED + db$FIXATED+dc$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dd$FIXATED
		ppe = getThresholdPredictor(de, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*de$FIXATED
		ppf = getThresholdPredictor(df, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*df$FIXATED
		ppg = getThresholdPredictor(dg, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dh$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dg$FIXATED
		pph = getThresholdPredictor(dh, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+di$FIXATED+dj$FIXATED)) + 0*dh$FIXATED
		ppi = getThresholdPredictor(di, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+dj$FIXATED)) + 0*di$FIXATED
		ppj = getThresholdPredictor(dj, (da$FIXATED + db$FIXATED+dc$FIXATED+dd$FIXATED+de$FIXATED+df$FIXATED+dg$FIXATED+dh$FIXATED+di$FIXATED)) + 0*dj$FIXATED


		ta = da$FIXATED
		tb = db$FIXATED
		tc = dc$FIXATED
		td = dd$FIXATED
		te = de$FIXATED
		tf = df$FIXATED
		tg = dg$FIXATED
		th = dh$FIXATED
		ti = di$FIXATED
		tj = dj$FIXATED

		ta = ta[!is.na(ppa)]
		tb = tb[!is.na(ppb)]
		tc = tc[!is.na(ppc)]
		td = td[!is.na(ppd)]
		te = te[!is.na(ppe)]
		tf = tf[!is.na(ppf)]
		tg = tg[!is.na(ppg)]
		th = th[!is.na(pph)]
		ti = ti[!is.na(ppi)]
		tj = tj[!is.na(ppj)]



		ppa = ppa[!is.na(ppa)]
		ppb = ppb[!is.na(ppb)]
		ppc = ppc[!is.na(ppc)]
		ppd = ppd[!is.na(ppd)]
		ppe = ppe[!is.na(ppe)]
		ppf = ppf[!is.na(ppf)]
		ppg = ppg[!is.na(ppg)]
		pph = pph[!is.na(pph)]
		ppi = ppi[!is.na(ppi)]
		ppj = ppj[!is.na(ppj)]

		doDiscreteEvaluation(c(ta,tb,tc,td,te,tf,tg,th,ti,tj), c(ppa, ppb, ppc, ppd, ppe, ppf, ppg, pph, ppi, ppj))


		cat(mean(da$FIXATED == db$FIXATED, na.rm=TRUE),"\n")
		cat(mean(db$FIXATED == dc$FIXATED, na.rm=TRUE),"\n")
		cat(mean(dc$FIXATED == dd$FIXATED, na.rm=TRUE),"\n")
		cat(mean(dd$FIXATED == de$FIXATED, na.rm=TRUE),"\n")
		cat(mean(de$FIXATED == df$FIXATED, na.rm=TRUE),"\n")
		cat(mean(df$FIXATED == dg$FIXATED, na.rm=TRUE),"\n")
		cat(mean(dg$FIXATED == dh$FIXATED, na.rm=TRUE),"\n")


		runEvaluationOfHumanOnOther(da$FIXATED, db$FIXATED)
		runEvaluationOfHumanOnOther(db$FIXATED, dc$FIXATED)
		runEvaluationOfHumanOnOther(dc$FIXATED, dd$FIXATED)
		runEvaluationOfHumanOnOther(dd$FIXATED, de$FIXATED)
		runEvaluationOfHumanOnOther(de$FIXATED, df$FIXATED)
		runEvaluationOfHumanOnOther(df$FIXATED, dg$FIXATED)
		runEvaluationOfHumanOnOther(dg$FIXATED, dh$FIXATED)
		runEvaluationOfHumanOnOther(dh$FIXATED, di$FIXATED)
		runEvaluationOfHumanOnOther(di$FIXATED, dj$FIXATED)
		runEvaluationOfHumanOnOther(dj$FIXATED, da$FIXATED)



}



##########################
##########################




runEvaluationOfHumanOnOther = function(target, predictor) {
	target2 = target * sign(predictor+1)
		predictor2 = predictor * sign(target+1)

		target2 = target2[!is.na(target2)]
		predictor2 = predictor2[!is.na(predictor2)]


		doDiscreteEvaluation(target2, predictor2)
}


##########################
##########################

getHumanSplit = function(data) {


	data$C.ITEM <- 10000*data$C.FILE+data$C.WNUM



		data$C.FIXATED = pmin(data$C.FIXNO, 1)


		da = data[data$Participant == "sa",]
		db = data[data$Participant == "sb",]
		dc = data[data$Participant == "sc",]
		dd = data[data$Participant == "sd",]
		de = data[data$Participant == "se",]
		df = data[data$Participant == "sf",]
		dg = data[data$Participant == "sg",]
		dh = data[data$Participant == "sh",]
		di = data[data$Participant == "si",]
		dj = data[data$Participant == "sj",]

                da = da[order(da$tokenID),]
                db = db[order(db$tokenID),]
                dc = dc[order(dc$tokenID),]
                dd = dd[order(dd$tokenID),]
                de = de[order(de$tokenID),]
                df = df[order(df$tokenID),]
                dg = dg[order(dg$tokenID),]
                dh = dh[order(dh$tokenID),]
                di = di[order(di$tokenID),]
                dj = dj[order(dj$tokenID),]


FIXATED_COLUMN = 92 #90

		da[is.na(da[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(da[,FIXATED_COLUMN], na.rm = TRUE)
		db[is.na(db[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(db[,FIXATED_COLUMN], na.rm = TRUE)
		dc[is.na(dc[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dc[,FIXATED_COLUMN], na.rm = TRUE)
		dd[is.na(dd[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dd[,FIXATED_COLUMN], na.rm = TRUE)
		de[is.na(de[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(de[,FIXATED_COLUMN], na.rm = TRUE)
		df[is.na(df[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(df[,FIXATED_COLUMN], na.rm = TRUE)
		dg[is.na(dg[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dg[,FIXATED_COLUMN], na.rm = TRUE)
		dh[is.na(dh[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dh[,FIXATED_COLUMN], na.rm = TRUE)
		di[is.na(di[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(di[,FIXATED_COLUMN], na.rm = TRUE)
		dj[is.na(dj[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dj[,FIXATED_COLUMN], na.rm = TRUE)


		da$Attention = (da$C.FIXATED + db$C.FIXATED + dc$C.FIXATED + dd$C.FIXATED + de$C.FIXATED + df$C.FIXATED + dg$C.FIXATED + dh$C.FIXATED + di$C.FIXATED + dj$C.FIXATED)/10

return(da)

}


getSkippingRatesHumanForOneTest = function(data) {


	data$C.ITEM <- 10000*data$C.FILE+data$C.WNUM



		data$C.FIXATED = pmin(data$C.FIXNO, 1)


		da = data[data$Participant == "sa" & data$Itemno==3,]
		db = data[data$Participant == "sb" & data$Itemno==3,]
		dc = data[data$Participant == "sc" & data$Itemno==3,]
		dd = data[data$Participant == "sd" & data$Itemno==3,]
		de = data[data$Participant == "se" & data$Itemno==3,]
		df = data[data$Participant == "sf" & data$Itemno==3,]
		dg = data[data$Participant == "sg" & data$Itemno==3,]
		dh = data[data$Participant == "sh" & data$Itemno==3,]
		di = data[data$Participant == "si" & data$Itemno==3,]
		dj = data[data$Participant == "sj" & data$Itemno==3,]


FIXATED_COLUMN = 90

		da[is.na(da[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(da[,FIXATED_COLUMN], na.rm = TRUE)
		db[is.na(db[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(db[,FIXATED_COLUMN], na.rm = TRUE)
		dc[is.na(dc[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dc[,FIXATED_COLUMN], na.rm = TRUE)
		dd[is.na(dd[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dd[,FIXATED_COLUMN], na.rm = TRUE)
		de[is.na(de[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(de[,FIXATED_COLUMN], na.rm = TRUE)
		df[is.na(df[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(df[,FIXATED_COLUMN], na.rm = TRUE)
		dg[is.na(dg[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dg[,FIXATED_COLUMN], na.rm = TRUE)
		dh[is.na(dh[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dh[,FIXATED_COLUMN], na.rm = TRUE)
		di[is.na(di[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(di[,FIXATED_COLUMN], na.rm = TRUE)
		dj[is.na(dj[,FIXATED_COLUMN]), FIXATED_COLUMN] <- mean(dj[,FIXATED_COLUMN], na.rm = TRUE)


		da$Attention = (da$C.FIXATED + db$C.FIXATED + dc$C.FIXATED + dd$C.FIXATED + de$C.FIXATED + df$C.FIXATED + dg$C.FIXATED + dh$C.FIXATED + di$C.FIXATED + dj$C.FIXATED)/10

return(da)







}




printPOSCoordinates = function(aggr, factor) {
 pos = aggr[,1]
 vals = aggr[,2]
 # exclude punctuation
 for(i in (2:length(pos))) {
   cat(paste("(",pos[i],",",round(factor*vals[i]),") ",sep=""), sep = " ")
 }
 cat("\n")
}

##########################
##########################

#aggregateOverReaders = function(data)  {
#   dataAggregate = aggregate(data["AttentionScore"],by=data["Token"], mean, na.rm=TRUE)
#}

printHeatmap = function(data, low, high, only_condition, end, exclude, attention, textno) {
        data = data[data$TextNo == textno,]
        attention = data[,attention]
	itemno = data$tokenID
		words = data$ExperimentToken.x
		wordfreq = data$LuaID
                condition = data$Condition
		initializeColors()
                end = min(end,length(itemno))
		cat("\\definecolor{none}{rgb}{0.99,0.99,0.99}","\n",sep="")
		for(i in (1:end)) {
#			if(itemno[i] > 0 && itemno[i] != 3) {
#				break
#			}
			if(words[i] == -1) {
				next
			}
                        if(condition[i] != only_condition) {
                                next
                        }
                        if(words[i] == "") {
                          next
                        }
			if((exclude && (wordfreq[i] > 9999)) ||  is.na(attention[i]  )){
#    cat(" ",as.character(words[i]),sep="")
				cat(" \\colorbox{none}{",as.character(words[i]),"}",sep="")
			} else {
				colorID = min(18, max(3, floor((attention[i] - low) * 20 / (high - low))))
					cat(" \\colorbox{color", colorID,     "}{",as.character(words[i]),"}",sep="")
			}
			if(i %% 10 == 0) {
				cat("\n")
			}

		}

}



##########################
##########################



getRGB = function(i, lower, upper) {
	i = min(20, max(0, (i - lower) * 20 / (upper-lower)))

         if (FALSE) {
		blue = max(0,min(1, 1-i/20.0))
		green = max(0,min(1, 1- i / 20.0))
		red = 1
		return(c(red,green,blue))
} else if(TRUE) {
		blue = max(0,min(1, 2- i/10))
		red = max(0,min(1, 0+i/10))
		return(c(red,0.3,blue))
} else {

		blue = max(0,min(1,2 - i/5.0))
		if(i < 10) {
			green = max(0,min(1,0+i/5.0))
		} else {
			green = max(0,min(1, 4-i/5.0))
		}
	red = max(0,min(1,-2+i/5.0))
		return(c(red, green, blue))
}
}


##########################
##########################




initializeColors = function(data) {
	for(i in (0:20)) {
		color = getRGB(i, 0, 20)
			cat("\\definecolor{color",i,"}{rgb}{", color[1], ",", color[2], ",", color[3],  "}","\n",sep="")
	}

}


##########################
##########################



FULL_SURPRISAL_MODEL = "pg-test-combined-50-1000-0.7-100-R-5a0-5.0-entities-entropy5.0-1.0-combined-real-full"


##########################
##########################




getFullSurprisalData = function(partNum) {
	name = FULL_SURPRISAL_MODEL
		CSV_FILE = paste("att-surp-uni-dundee-",name,"_",name,".csv",sep="")
		if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE,sep=""))) {
			return(processDataForEvaluation(getData(CSV_FILE2), "2"))
		}
}



##########################
##########################



pos = list("ADJ" = c("JJ", "JJR", "JJS"),
		"ADP" = c("IN"),
		"ADV" = c("RB", "RBR", "RBS", "WRB"),
		"CONJ" = c("CC"),
		"DET" = c("DT","EX","PDT","WDT"),
		"NOUN" = c("NN","NNP","NNPS","NNS"),
		"NUM" = c("CD"),
		"PRON" = c("PRP", "PRP$", "WP", "WP$"),
		"PRT" = c("POS", "RP","TO"),
		"VERB" = c("MD", "VB", "VBD", "VBG", "VBN", "VBP", "VBZ"),
		"X" = c("FW", "LS", "SYM", "UH")
	  )


##########################
##########################




retrieveValuesPerPOS = function() {
	findValuesPerPOS("full","5.0")
		findValuesPerPOS("s","5.0")
		findValuesPerPOS("rs","5.0")
		findValuesPerPOS("ir","5.0")

		findValuesPerPOS("full","4.5")
		findValuesPerPOS("s","4.5")
		findValuesPerPOS("rs","4.5")
		findValuesPerPOS("ir","4.5")

}


##########################
##########################



findValuesPerPOSInDundee = function(CSV_FILE, partNum) {
	scores = list("ADJ" = {}, "ADP" = {}, "ADV" = {}, "CONJ" = {}, "DET" = {}, "NOUN"= {}, "NUM" = {}, "PRON"={}, "PRT"={}, "VERB"={}, "X"={})
		cat(paste("X: /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE,sep=""),"\n")
		data = processDataForEvaluation(getData(CSV_FILE), "1")
		for(tag in names(pos)) {
			scores[[tag]] = append(scores[[tag]], mean((data[data$CPOS %in% pos[[tag]],]$FIXATED)))
		}
	for(tag in names(pos)) {
		cat("---\n",tag,"\n")
			cat(scores[[tag]],"\n")
			evalList(scores[[tag]], 0)
			cat("\n")
	}
}


##########################
##########################


collectValuesPerPOSForColumn = function(data, scores, column) {
	for(tag in names(pos)) {
		scores[[tag]] = append(scores[[tag]], mean((column[data$CPOS %in% pos[[tag]]])))
	}
	return(unlist(scores))
}

collectValuesPerPOS = function(data, scores) {
	for(tag in names(pos)) {
		scores[[tag]] = append(scores[[tag]], mean((data[data$CPOS %in% pos[[tag]],]$NotCentered.Attention)))
	}
	return(unlist(scores))
}



##########################
##########################




findValuesPerPOS = function(abl, weight, encoderName, partNum) {
	cat("@@@@@@@@@@@\n",abl,"   ",weight,"\n")
		scores = list("ADJ" = {}, "ADP" = {}, "ADV" = {}, "CONJ" = {}, "DET" = {}, "NOUN"= {}, "NUM" = {}, "PRON"={}, "PRT"={}, "VERB"={}, "X"={})
		for(num in (1:10)) {
			name = getNameFromAblAndWeight(abl, weight,num, encoderName)
				CSV_FILE = paste("att-surp-uni-dundee-",name,"_",name,".csv",sep="")
				cat(paste("X: /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE,sep=""),"\n")
				if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE,sep=""))) {
					data = processDataForEvaluation(getData(CSV_FILE), "1")
						if(is.null(data)) {
							cat("FAIL\n")
								next
						}
					scores = collectValuesPerPOS(data, scores)

				}
		}
	for(tag in names(pos)) {
		cat("---\n",tag,"\n")
			cat(scores[[tag]],"\n")
			evalList(scores[[tag]], 0)
			cat("\n")
	}
}



##########################
##########################



percentageOfSignificant = function(xlist, threshold)
{
	return(sum(xlist > threshold) / length(xlist))
}



##########################
##########################



evalList = function(xlist, threshold)
{
	cat(percentageOfSignificant(xlist, threshold), mean(xlist),sd(xlist),min(xlist), which.min(xlist), " -- ",max(xlist),which.max(xlist),"\n", sep=" ")
}



##########################
##########################



logisticFitWithBaselines = function(data)
{


	lm = glm(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST +  C.PREVFIX +  C.OBJLPOS + WordFreq +  NotCentered.Attention, family = binomial("logit"), data=data)
		resu = predict(lm, type="response", newdata =data.frame(NotCentered.Attention=data$NotCentered.Attention,  C.WLEN=data$C.WLEN, C.FREQBIN=data$C.FREQBIN , C.FRQLAST=data$C.FRQLAST ,  C.PREVFIX=data$C.PREVFIX ,  C.OBJLPOS=data$C.OBJLPOS , WordFreq=data$WordFreq))
		return(resu)

}



##########################
##########################


maxLikelihoodInterpolatorWithBaselines = function(data)
{
	resu = logisticFitWithBaselines(data)
		resu = (sign(resu - 0.5) + 1)/2
		return(resu)
}



##########################
##########################





logisticFit = function(data)
{
	lm = glm(formula = FIXATED ~  NotCentered.Attention, family = binomial("logit"), data=data)
		resu = predict(lm, type="response", newdata =data.frame(NotCentered.Attention=data$NotCentered.Attention))
		return(resu)

}


##########################
##########################



maxLikelihoodInterpolator = function(data)
{
	resu = logisticFit(data)
		resu = (sign(resu - 0.5) + 1)/2
		return(resu)
}



##########################
##########################





##########################
##########################



doEvaluationForAllGroups = function()
{
	if(TRUE) {
		doEvaluationForGroup("ir","5.0", "-combined-real")
			doEvaluationForGroup("rs","5.0", "-combined-real")
			doEvaluationForGroup("s","5.0", "-combined-real")
			doEvaluationForGroup("full","5.0", "-combined-real")

			doEvaluationForGroup("ir","4.5", "-combined-real")
			doEvaluationForGroup("rs","4.5", "-combined-real")
			doEvaluationForGroup("full","4.5", "-combined-real")
			doEvaluationForGroup("s","4.5", "-combined-real")
	} else {

		doEvaluationForGroup("ir","5.0", "encoder-1")
			doEvaluationForGroup("rs","5.0", "encoder-1")
			doEvaluationForGroup("s","5.0", "encoder-1")
			doEvaluationForGroup("full","5.0", "encoder-1")

	}


	if(FALSE) {
		doEvalForPairs()
	}
}


doEvalForPairs = function() {
	compareTwoGroups("ir", "5.0", "rs", "5.0", "-combined-real")
		compareTwoGroups("ir", "5.0", "s", "5.0", "-combined-real")
		compareTwoGroups("ir", "5.0", "full", "5.0", "-combined-real")

#compareTwoGroups("rs", "5.0", "ir", "5.0", "-combined-real")
		compareTwoGroups("rs", "5.0", "s", "5.0", "-combined-real")
		compareTwoGroups("rs", "5.0", "full", "5.0", "-combined-real")

#compareTwoGroups("s", "5.0", "ir", "5.0", "-combined-real")
#compareTwoGroups("s", "5.0", "rs", "5.0", "-combined-real")
		compareTwoGroups("s", "5.0", "full", "5.0", "-combined-real")

#compareTwoGroups("full", "5.0", "ir", "5.0", "-combined-real")
#compareTwoGroups("full", "5.0", "rs", "5.0", "-combined-real")
#compareTwoGroups("full", "5.0", "s", "5.0", "-combined-real")



}



##########################
##########################



getProcessedData = function(CSV_FILE) {
  return(processDataForEvaluation(getData(CSV_FILE), 1))
}


#assumes that processDataForEvaluation has been run on them
compareTwoModelsLogistic = function(data1, data2)
{


	data = merge(data1, data2, by=c("C.ITEM", "Participant"))

		data$resid.Attention.NoSurp.x.Against = residuals(lm(Attention.x ~ C.WLEN.x + C.FREQBIN.x +   WordFreq.x + Attention.y, data=data, na.action=na.exclude))
		data$resid.Attention.NoSurp.y.Against = residuals(lm(Attention.y ~ C.WLEN.y + C.FREQBIN.y +   WordFreq.y + Attention.x, data=data, na.action=na.exclude))

		data$resid.Attention.x.Against = residuals(lm(Attention.x ~ C.WLEN.x + C.FREQBIN.x +   WordFreq.x + Surprisal.x + Attention.y, data=data, na.action=na.exclude))
		data$resid.Attention.y.Against = residuals(lm(Attention.y ~ C.WLEN.y + C.FREQBIN.y +   WordFreq.y + Surprisal.y + Attention.x, data=data, na.action=na.exclude))


#log.Full.x = try(glmer(formula = FIXATED.x ~ C.WLEN.x+ C.FREQBIN.x + C.FRQLAST.x + C.PREVFIX.x +  C.OBJLPOS.x + WordFreq.x  + Surprisal.x + Attention.y + resid.Attention.x.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))
		log.Att.x  = try(glmer(formula = FIXATED.x ~ C.WLEN.x+ C.FREQBIN.x + C.FRQLAST.x + C.PREVFIX.x +  C.OBJLPOS.x + WordFreq.x  +               Attention.y + resid.Attention.NoSurp.x.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))

#log.Full.y = try(glmer(formula = FIXATED.y ~ C.WLEN.y+ C.FREQBIN.y + C.FRQLAST.y + C.PREVFIX.y +  C.OBJLPOS.y + WordFreq.y  + Surprisal.y + Attention.x + resid.Attention.y.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))
		log.Att.y  = try(glmer(formula = FIXATED.y ~ C.WLEN.y+ C.FREQBIN.y + C.FRQLAST.y + C.PREVFIX.y +  C.OBJLPOS.y + WordFreq.y  +               Attention.x + resid.Attention.NoSurp.y.Against + (1|Participant) + (1|C.ITEM), family = binomial("logit"), data=data))



		return(list("att-x" = log.Att.x, "att-y" = log.Att.y))


}
##########################
##########################


compareTwoModelsLinear = function(data1, data2)
{
	data.Nozeros = merge(getNozeros(data1), getNozeros(data2), by=c("C.ITEM", "Participant"))

		data.Nozeros$residSurprisal.x.Against = residuals(lm(Surprisal.x ~ C.WLEN.x + WordFreq.x +  FRQLAST.x +    WordFreq.x + Surprisal.y, data=data.Nozeros, na.action=na.exclude))
		data.Nozeros$residSurprisal.y.Against = residuals(lm(Surprisal.y ~ C.WLEN.y + WordFreq.y +  FRQLAST.y +    WordFreq.y + Surprisal.x, data=data.Nozeros, na.action=na.exclude))

		linear.Surp.x = lmer(C.FPASSD.x ~ C.WLEN.x+ C.FREQBIN.x + C.FRQLAST.x + C.TRACEIC.x + C.BACKTR.x + C.NBIGR.x + C.PREVFIX.x + C.LDIST.x + C.OBJLPOS.x + WordFreq.x + Surprisal.y + residSurprisal.x.Against + (residSurprisal.x.Against|C.ITEM) + (1|Participant) + (1|C.ITEM), data=data.Nozeros)
		linear.Surp.y = lmer(C.FPASSD.y ~ C.WLEN.y+ C.FREQBIN.y + C.FRQLAST.y + C.TRACEIC.y + C.BACKTR.y + C.NBIGR.y + C.PREVFIX.y + C.LDIST.y + C.OBJLPOS.y + WordFreq.y + Surprisal.x + residSurprisal.y.Against + (residSurprisal.y.Against|C.ITEM) + (1|Participant) + (1|C.ITEM), data=data.Nozeros)

		return(list("x" = linear.Surp.x, "y" = linear.Surp.y))
}

##########################
##########################



compareTwoGroups = function(abl1, weight1, abl2, weight2, encoderName, partNum)
{
	x_over_y = {}
	y_over_x = {}

	library(lme4)
		for(num1 in (1:10)) {
			name1 = getNameFromAblAndWeight(abl1, weight1,num1, encoderName)
				CSV_FILE1 = paste("att-surp-uni-dundee-",name1,"_",name1,".csv",sep="")
				cat(paste("X: /disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE1,sep=""),"\n")
				if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE1,sep=""))) {
					data1 = processDataForEvaluation(getData(CSV_FILE1), "1")
						if(is.null(data1)) {
							next
						}

					if(runif(1,0,1) > 0.5){
						next
					}

					for(num2 in (1:10)) {
						name2 = getNameFromAblAndWeight(abl2, weight2,num2, encoderName)
							CSV_FILE2 = paste("att-surp-uni-dundee-",name2,"_",name2,".csv",sep="")
							cat("\n","----",paste("X: /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE1,sep=""),"\n")
							cat(paste("Y: /disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE2,sep=""),"\n")
							if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE2,sep=""))) {
								data2 = processDataForEvaluation(getData(CSV_FILE2), "2")
									if(is.null(data2)) {
										next
									}
								if(runif(1,0,1) > 0.5) {
									next
								}

								compareLog = compareTwoModelsLogistic(data1, data2)
									compareLin = compareTwoModelsLinear(data1, data2)


# cat(capture.output(summary(compareLog$"full-x")), sep="\n")
									cat(capture.output(summary(compareLog$"att-x")), sep="\n")
# cat(capture.output(summary(compareLog$"full-y")), sep="\n")
									cat(capture.output(summary(compareLog$"att-y")), sep="\n")

									cat(capture.output(summary(compareLin$"x")), sep="\n")
									cat(capture.output(summary(compareLin$"y")), sep="\n")

									x_over_y = append(x_over_y, coef(summary(compareLog$"att-x"))["resid.Attention.NoSurp.x.Against","z value"])
									y_over_x = append(y_over_x, coef(summary(compareLog$"att-y"))["resid.Attention.NoSurp.y.Against","z value"])



							}
					}
				}
		}
	cat("X OVER Y for ","X",abl1,weight1,"Y",abl2,weight2,"\n",sep=" ")
		cat(x_over_y,"\n")
		cat(evalList(x_over_y, 2),"\n")

		cat("Y OVER X for ","Y",abl2,weight2,"X",abl1,weight1,"\n",sep=" ")
		cat(y_over_x,"\n")
		cat(evalList(y_over_x, 2),"\n")


}

##########################
##########################

getNameFromAblAndWeight = function(abl, weight, num, encoderName, soft)
{
	if(as.numeric(weight) == floor(as.numeric(weight))) {
		weightR = floor(as.numeric(weight))
	} else {
		weightR = weight
	}
	if (! soft){
		return(paste("pg-test-combined-50-1000-0.7-100-R-",weightR,"a0-", weight, "-entities-entropy5.0-1.0",encoderName,"-",abl,"-re", num, sep=""))
	} else {
		crash()
	}
}

##########################
##########################



doEvaluationForGroup = function(abl, weight, encoderName, partNum)
{
	library(lme4)
		cat("\n\n",abl,weight,"---------","\n",sep="\n")
		l.tval.fix.full =      {}
	l.tval.fix.surp =      {}
	l.tval.fix.att =       {}

	l.cor.att.surp =      {}
	l.cor.att.wordfreq =  {}
	l.cor.att.wordlength = {}

	l.tval.fp.full =      {}
	l.tval.fp.surp =     {}
	l.tval.fp.att =        {}

	l.meansurp =          {}

	l.loglikelihood =    {}

	l.ratio =           {}

	disc.att.acc = {}
	disc.att.f1f = {}
	disc.att.f1s = {}

	disc.surp.acc = {} 
	disc.surp.f1f = {} 
	disc.surp.f1s = {} 


	for(num in (1:10)) {
		name = getNameFromAblAndWeight(abl, weight, num, encoderName)
			CSV_FILE = paste("att-surp-uni-dundee-",name,"_",name,".csv",sep="")
			cat(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE,sep=""),"\n")
			if(file.exists(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART",partNum,"Statistics/statisticsFinal/",CSV_FILE,sep=""))) {
				results = evaluate.Treebank(CSV_FILE)
					if(is.null(results)) {
						next
					}
				if(DO_LOGISTIC) {
					tval.fix.full =      coef(summary(results$"log.Full"))["C.residAttention","z value"]
						tval.fix.surp =      coef(summary(results$"log.Surp"))["C.residSurprisal","z value"]
						tval.fix.att =       coef(summary(results$"log.Att"))["C.residAttentionNoSurp","z value"]

						l.tval.fix.full  =   append(l.tval.fix.full    , tval.fix.full   )
						l.tval.fix.surp  =   append(l.tval.fix.surp    ,  tval.fix.surp  )
						l.tval.fix.att  =   append(l.tval.fix.att    , tval.fix.att   )


				}

				cor.att.surp =       results$"Att-Surp"
					cor.att.wordfreq =   results$"Att-log(WordFreq)"
					cor.att.wordlength = results$"Att-WordLength"

					if(DO_LINEAR) {
						tval.fp.full =       coef(summary(results$"FP.Full"))["residAttentionWithSurp","t value"]
							tval.fp.surp =       coef(summary(results$"FP.Surprisal"))["residSurprisal","t value"]
							tval.fp.att =        coef(summary(results$"FP.Att"))["residAttentionNoSurp","t value"]


							l.tval.fp.full  =   append(l.tval.fp.full    ,  tval.fp.full  )
							l.tval.fp.surp  =   append(l.tval.fp.surp    ,  tval.fp.surp  )
							l.tval.fp.att  =   append(l.tval.fp.att    ,  tval.fp.att  )


					}

				meansurp =           results$"meanSurp"

					loglikelihood =      results$"approxPerp"$"model"

					ratio =              results$"approxPerp"$"ratio"


					l.cor.att.surp  =   append(l.cor.att.surp    ,  cor.att.surp  )
					l.cor.att.wordfreq  =   append(l.cor.att.wordfreq    ,  cor.att.wordfreq  )
					l.cor.att.wordlength  =   append(l.cor.att.wordlength    ,  cor.att.wordlength  )
					l.meansurp  =   append(l.meansurp    ,  meansurp  )
					l.loglikelihood  =   append(l.loglikelihood    ,  loglikelihood  )
					l.ratio  =   append(l.ratio    ,  ratio  )


					disc.att = results$"Disc.Att"
					disc.surp = results$"Disc.Surp"
					disc.att.acc = append(disc.att.acc, disc.att$"Acc")
					disc.att.f1f = append(disc.att.f1f, disc.att$"F1 Fix")
					disc.att.f1s = append(disc.att.f1s, disc.att$"F1 Skip")

					disc.surp.acc = append(disc.surp.acc, disc.surp$"Acc")
					disc.surp.f1f = append(disc.surp.f1f, disc.surp$"F1 Fix")
					disc.surp.f1s = append(disc.surp.f1s, disc.surp$"F1 Skip")




#   return(list("Acc" = accuracy, "F1 Fix" = f1Fix, "F1 Skip" = f1Skip))



			}
	}
	cat("EVAL FOR ",abl,"  ",weight," ",encoderName,"\n")

		ModelName = getModelName(abl)

		buildRow(paste(ModelName," (Prob)"), list(100*disc.att.acc, 100*disc.att.f1f, 100*disc.att.f1s))


		buildRow(paste(ModelName," (Surp)"), list(100*disc.surp.acc, 100*disc.surp.f1f, 100*disc.surp.f1s))



		printEvalFeature(disc.att.acc, "disc.att.acc", 0.52)
		printEvalFeature(disc.att.f1f, "disc.att.f1f", 0.6)
		printEvalFeature(disc.att.f1s, "disc.att.f1s", 0.4)

		printEvalFeature(disc.surp.acc, "disc.surp.acc", 0.52)
		printEvalFeature(disc.surp.f1f, "disc.surp.f1f", 0.6)
		printEvalFeature(disc.surp.f1s, "disc.surp.f1s", 0.4)


		cat("         l.tval.fix.full ","\n")
		cat(         l.tval.fix.full ,"\n")
		cat(   evalList(l.tval.fix.full, 1.96 ))
		cat("\n")
		cat( "    l.tval.fix.surp ","\n")
		cat(     l.tval.fix.surp ,"\n")
		cat(  evalList(l.tval.fix.surp , 1.96))
		cat("\n")
		cat(  "       l.tval.fix.att ","\n")
		cat(         l.tval.fix.att ,"\n")
		cat(evalList(l.tval.fix.att ,1.96))
		cat("\n")
		cat(   "    l.cor.att.surp","\n")
		cat(       l.cor.att.surp,"\n")
		cat(  evalList(       l.cor.att.surp,0))
		cat("\n")
		cat(    " l.cor.att.wordfreq ","\n")
		cat(     l.cor.att.wordfreq ,"\n")
		cat(  evalList(   l.cor.att.wordfreq ,0))
		cat("\n")
		cat(     "    l.cor.att.wordlength ","\n")
		cat(         l.cor.att.wordlength ,"\n")
		cat(   evalList(      l.cor.att.wordlength ,0))
		cat("\n")
		cat(      "   l.tval.fp.full" ,"\n")
		cat(         l.tval.fp.full ,"\n")      
		cat(evalList(         l.tval.fp.full,2.0 ))
		cat("\n")
		cat("     l.tval.fp.surp  ","\n")
		cat(     l.tval.fp.surp  ,"\n")    
		cat(evalList(     l.tval.fp.surp ,0 ))
		cat("\n")
		cat( "        l.tval.fp.att    ","\n")
		cat(         l.tval.fp.att   ,"\n" )   
		cat(evalList(         l.tval.fp.att   ,0 ))
		cat("\n")
		cat(  "       l.meansurp      "  ,"\n")
		cat(         l.meansurp       ,"\n" )  
		cat(evalList(         l.meansurp      ,0  ))
		cat("\n")
		cat(   "      l.loglikelihood" ,"\n")
		cat(         l.loglikelihood ,"\n")
		cat( evalList(        l.loglikelihood,0 ))
		cat("\n")
		cat(    "     l.ratio       "  ,"\n" )
		cat(         l.ratio         ,"\n" )  
		cat(  evalList(       l.ratio          ,3))
		cat("\n")
}


##########################
##########################



printEvalFeature = function(feature, name, threshold) {


	cat("\t\t",name,"\t\t\n")
		cat(       feature ,"\n")
		cat(   evalList(feature, threshold ))
		cat("\n")
}


##########################
##########################



getModelName = function(ABL) {
	return(models[[ABL]])
}


##########################
##########################




buildRow = function(RowName, data) {
#buildRow(paste(ModelName," (Prob)"), c(disc.att.acc, disc.att.f1f, disc.att.f1s))
	cat(RowName,"  ")
		for(i in (1:length(data))) {
			datapoint = data[[i]]
				cat("&   ",round(mean(datapoint),2)," (",round(sd(datapoint),2),")  ",sep="")
		}
	cat("\\\\\n")
}



##########################
##########################



bootstrapping = function(CSV_FILE) {
	library(boot)
		data = processDataForEvaluation(getData(CSV_FILE), CSV_FILE)
		if(! is.data.frame(data)) {
			return(NULL)
		}
	results <- boot(data = data, statistic = getStatisticsForData, R = 10)
		return(results)

}



##########################
##########################



getStatisticsForData = function(data, indices){

	d <- data[indices,]
		return(computeStatisticsOnData(d,TRUE))
}


#models <- list("data" = data,               "log.Full" = log.Full,               "log.Surp" = log.Surp,               "log.Att" = log.Att,"Att-Surp" =cor(data$Surprisal, data$Attention), "Att-log(WordFreq)" = cor(data$Attention, data$WordFreq),"Att-WordLength" = cor(data$Attention,data$C.WLEN), "FP.Surprisal" = linear.Surp, "FP.Att" = linear.Att= "FP.Full" = linear.Full, "meanSurp" = mean(data.Nozeros$NotCentered.Surprisal), "approxPerp" = approxPerp)
#return list("model" = perpModel, "random" = perpRandom, "ratio" = (perpRandom - perpModel))


##########################
##########################



evaluatePerplexity = function(CSV_FILE) 
{
	data = evaluate.Treebank(CSV_FILE)
		computeApproxPerplexity(data)
		return(data)
}



##########################
##########################




computeApproxPerplexity = function(data)
{

perpModel = -(sum(data$C.FIXATED * log(data$NotCentered.Attention)) + sum((1-data$C.FIXATED) * log(1-data$NotCentered.Attention)))
# todo remove unknown words from this formula

	lexicalAttentions = aggregate(data["NotCentered.Attention"],by=data["Token"], mean, na.rm=TRUE)
		perpModel = 0
		perpRandom = 0
		perpLexical = 0
		attention = data$NotCentered.Attention
		wordfreq = data$WordFreq
		fixated = data$C.FIXNO
		tokens = data$Token
		randomProb = mean(data$NotCentered.Attention)
		numberOfItems = 0




		for(i in (1:nrow(data))) {
#cat(i,sep="\n")
			if(!is.na(fixated[i]) & fixated[i] > -1){
				if(wordfreq[i] > 0 && wordfreq[i] < 12) { # deal with unknown words (NOTE hard-coded that ~12 is the threshold)
					numberOfItems = numberOfItems + 1
#lexicalAttention = wordatts[wordatts[,1]==tokens[i],]$NotCentered.Attention
						if(fixated[i] > 0){
							perpModel = perpModel - log(attention[i])
								perpRandom = perpRandom - log(randomProb)
#perpLexical = perpLexical - log(lexicalAttention)
#cat(attention[i], wordfreq[i], sep = "\n")
						}else{
#cat("\n")
							perpModel = perpModel - log(1-attention[i])
								perpRandom = perpRandom - log(1-randomProb)
#perpLexical = perpLexical - log(1-lexicalAttention)
						}
				}
			}
		}



	cat("LogLikelihoods:","\n","MODEL      ",perpModel,"\nLEXICAL   ",perpLexical,"\nRANDOM    ",perpRandom,"\nRATIO      ",(perpRandom - perpModel),"\nAVG MODEL  ",(perpModel/numberOfItems),"\nAVG RAND   ",(perpRandom/numberOfItems),sep="")
		return(list("model" = perpModel, "random" = perpRandom, "ratio" = (perpRandom - perpModel)))
}








##########################
##########################




evaluateAll.Treebank = function(partNum)
{
	library(lme4)
#library(ggplot2)
		files = list.files(path="/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/")
		for(q in (1:length(files))) {
#sink()
#         cat(files[q], sep="\n")
			try(evaluate.Treebank(files[q]))
		}

}



##########################
##########################



#result = merge(data.Treebank, data.Corpus, by=0, all=TRUE)

buildRowNames.T = function(dataT)
{
	rowNames = row.names(dataT)
		for(q in (1:length(dataT$Surprisal))) {
			print(q)
				rowNames[q] <- paste(dataT$C.SUBJ[q], dataT$C.ITEM[q])
		}
	row.names(dataT) <- rowNames
		return(dataT)
}


##########################
##########################



buildRowNames.C = function(dataC)
{
	rowNames = row.names(dataC)
		for(q in (1:length(dataC$Surprisal))) {
			print(q)
				rowNames[q] <- paste(dataC$SUBJ[q], dataC$ITEM[q])
		}
	row.names(dataC) <- rowNames
		return(dataC)
}



##########################
##########################



randomlySampleData.Treebank = function(data, prob) {
	column = data$Surprisal
		for(q in (1:length(column))) {
			dice = runif(1,0.0,1.0)
				column[q] = dice - prob
		}

	data2 = data[column < 0,]


		m1 = try(glm(formula = FIXATED ~ C.WLEN+ C.PREVFIX + C.OBJLPOS + WordFreq+ C.FRQLAST +  Surprisal + Attention, family = binomial("logit"), data=data2))

		m2 = try(glm(formula = FIXATED ~  C.WLEN+ C.PREVFIX + C.OBJLPOS  + WordFreq + C.FRQLAST +   Surprisal, family = binomial("logit"), data=data2))

		m3 = try(glm(formula = FIXATED ~  C.WLEN+ C.PREVFIX + C.OBJLPOS +  WordFreq + C.FRQLAST,  family = binomial("logit"), data=data2))

		m4 = try(glm(formula = FIXATED ~  C.WLEN+ C.PREVFIX + C.OBJLPOS +  WordFreq + C.FRQLAST + Attention,  family = binomial("logit"), data=data2))



		cat("--- nFixations, RESID, LOGISTIC ---",sep="\n")
		try(cat(capture.output(m1), "\n", sep="\n"))

		cat("--- nFixations, RESID, LOGISTIC BASELINE ---",sep="\n")
		try(cat(capture.output(m2), "\n", sep="\n"))

		cat("--- nFixations, RESID, LOGISTIC BASELINE NO SURP ---",sep="\n")
		try(cat(capture.output(m3), "\n", sep="\n"))

		cat("--- nFixations, RESID, NO SURP ---",sep="\n")
		try(cat(capture.output(m4), "\n", sep="\n"))


		cat("--- LIKELIHOOD RATIO ---",sep="\n")
		try(cat(capture.output(lrtest(m1,m2)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m1,m3)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m2,m3)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m1,m4)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m2,m4)), "\n", sep="\n"))

		try(cat(capture.output(lrtest(m3,m4)), "\n", sep="\n"))

}



##########################
##########################




test.Treebank = function()
{
	library(lme4)
#library(ggplot2)
		evaluate.Treebank('att-surp-uni-dundee-pg-test-autoencoding-20-700-0.7-100-R-4.6a0-emb_pg-test-langmod-20-200-0.7-100-R-4.6e1.csv')
}

#data <- read.csv("/disk/scratch2/s1582047/dundee/PART1Statistics/statisticsFinal//att-surp-uni-dundee-pg-test-autoencoding-20-700-0.7-100-R-4.6a0-emb_pg-test-langmod-20-200-0.7-100-R-4.6e1.csv", header = TRUE, sep = "\t")


##########################
##########################




evaluate.Treebank = function(CSV_FILE)
{
	data = processDataForEvaluation(getData(CSV_FILE), CSV_FILE)
		return(computeStatisticsOnData(data, FALSE))
}



##########################
##########################


#########
##########################



doDiscreteEvaluation = function(target, predicted) {



	correctPredictions = sum(abs(predicted == target))
		truePositives = sum(predicted * target)
		trueNegatives = sum((1-predicted) * (1-target))
		totalRealPositives = sum(target)
		totalRealNegatives = sum(1-target)
		totalItems = length(predicted)

		accuracy = correctPredictions / totalItems
		fixPrecision = truePositives / sum(predicted)
		fixRecall = truePositives / sum(target)
		skipPrecision = trueNegatives / sum(1-predicted)
		skipRecall = trueNegatives / sum(1-target)

		f1Fix = 2 * (fixPrecision * fixRecall) / (fixPrecision + fixRecall)
		f1Skip = 2 * (skipPrecision * skipRecall) / (skipPrecision + skipRecall)

		cat("RATES      ", mean(target),"  ", mean(predicted),"\n")
		cat("Accuracy   ", accuracy,"\n")
		cat("F1 Fix     ", f1Fix, "\n")
		cat("F1 Skip    ", f1Skip, "\n")
		return(list("Acc" = accuracy, "F1 Fix" = f1Fix, "F1 Skip" = f1Skip))
}





##########################
##########################



computeStatisticsOnData = function(data, returnStatistics) {

	if(! is.data.frame(data)) {
		return(NULL)
	}

#################### DATA CLEANUP




#LDIST = LAUN
#OBLP=OBLP
#WDLP= WDLP
#FILE, Participant (encoded via which file from the original corpus is used)

#PREVFIX  = whether the previous word was fixated
#FIXNO = number of fixations on this word
#TOTDUR = total fixation duration
#FFIXDUR = first fixation duration
#FDUR = first pass times
#CPOS = CPOS tag according to charniak parser
#MPOS = CPOS tag according to MINIPAR parser
#WordFreq = frequency, log-transformed per billion words based on gigaword corpus
#FRQLAST= log frequency of previous word
#NBIGR = bigram forward transitional probability, also estimated from gigaword, log
#BACKTR = bigram backward transitional probability, also estimated from gigaword, log
#GOPAST = go-past times = ?
#SACCWD = saccade length measured in words (this is mainly as an additional cue to identify errors in the dundee corpus like the one described above)



#################### MIXED MODELS

# dependent variables:
# FDUR
# FIXATED: number of fixations

# independent variables:
## OLEN: object length (includes punctuation)	= .
# C.WLEN: word length (excludes punctuation) =.
# // LDIST: launch distance = ?
# PREVFIX: previous word fixated or not = ?TODO CREATE
# OBJLPOS: landing position on object = OBLP
## WDLPOS: landing position on word = WDLP
## POS: part of speech = CPOS (what's the difference to FPOS?)
# WNOS: word number	= ID
# FREQBIN: lexical frequency. replaced by WordFreq
# FRQLAST: frequency of previous word. TODO CREATE
# // NBIGR: bigram probability = ?
# // BACKTR: backwards transitional probability	 = ?
# LASTFIX: distance of previous fixation in number of words. TODO CREATE



####################  CENTERING ###########


	data$C.WLEN <- data$C.WLEN - mean(data$C.WLEN, na.rm = TRUE)
		data$C.LDIST <- data$C.LDIST - mean(data$C.LDIST, na.rm = TRUE)
		data$C.PREVFIX <- data$C.PREVFIX - mean(data$C.PREVFIX, na.rm = TRUE)
		data$C.OBJLPOS <- data$C.WDLPOS - mean(data$C.WDLPOS, na.rm = TRUE)
		data$C.WNOS <- data$C.WNOS - mean(data$C.WNOS, na.rm = TRUE)
		data$C.FREQBIN <- data$C.FREQBIN - mean(data$C.FREQBIN, na.rm = TRUE)
		data$C.FRQLAST <- data$C.FRQLAST - mean(data$C.FRQLAST, na.rm = TRUE)
		data$C.TRACEIC <- data$C.TRACEIC - mean(data$C.TRACEIC, na.rm = TRUE)
		data$C.NBIGR <- data$C.NBIGR - mean(data$C.NBIGR, na.rm = TRUE)
		data$C.BACKTR <- data$C.BACKTR - mean(data$C.BACKTR, na.rm = TRUE)
		data$C.LASTFIX <- data$C.LASTFIX - mean(data$C.LASTFIX, na.rm = TRUE)

		data$Surprisal <- data$Surprisal - mean(data$Surprisal, na.rm = TRUE)
		data$Attention <- data$Attention - mean(data$Attention, na.rm = TRUE)



		data$C.FIXNO <- data$C.FIXNO - mean(data$C.FIXNO, na.rm = TRUE)




		if(length(data$Surprisal) == 0) {
			return(NULL)
		}


	data$C.FIXATED = pmin(data$C.FIXNO, 1)


########### COMPUTE RESIDUALS ##############

		data$C.residAttention       = residuals(lm(Attention ~ C.WLEN + C.FREQBIN +  WordFreq + Surprisal, data=data, na.action=na.exclude))
		data$C.residSurprisal       = residuals(lm(Surprisal ~ C.WLEN + C.FREQBIN +   WordFreq, data=data, na.action=na.exclude))
		data$C.residAttentionNoSurp = residuals(lm(Attention ~ C.WLEN + C.FREQBIN +   WordFreq, data=data, na.action=na.exclude))




		cat("Logistic Models","\n")




# Logistic Models
# - full
		if(DO_LOGISTIC) {


			log.Baseline = try(glmer(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST + C.PREVFIX +  C.OBJLPOS + WordFreq  + (1|C.SUBJ) + (1|C.ITEM), family = binomial("logit"), data=data))


				log.Full = try(glmer(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST + C.PREVFIX +  C.OBJLPOS + WordFreq  + C.residSurprisal + C.residAttention + (1|C.SUBJ) + (1|C.ITEM), family = binomial("logit"), data=data))
#

				cat(1,"\n")

#cat(capture.output(n1), sep="\n")

				log.Surp = try(glmer(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST + C.PREVFIX +  C.OBJLPOS + WordFreq  + C.residSurprisal + (1|C.SUBJ) + (1|C.ITEM), family = binomial("logit"), data=data))

				cat(2,"\n")

# - only Attention
				log.Att  = try(glmer(formula = FIXATED ~ C.WLEN+ C.FREQBIN + C.FRQLAST +  C.PREVFIX +  C.OBJLPOS + WordFreq + C.residAttentionNoSurp + (1|C.SUBJ) + (1|C.ITEM), family = binomial("logit"), data=data))

#cat(capture.output(n2), sep="\n")

				cat(3,"\n")
		} else {
			log.Baseline = NULL
				log.Full = NULL
				log.Surp = NULL
				log.Att  = NULL
		}





############## Predicting FPASSDUR

######### CENTERING FOR FIXATED ITEMS




# remove where FDUR is NAN
	data.Nozeros <- data[data$C.FPASSD > 0,]


		data.Nozeros <- data.Nozeros[complete.cases(data.Nozeros[,1]), ]
		data.Nozeros <- data.Nozeros[data.Nozeros$C.FPASSD < 1000,]
		data.Nozeros <- data.Nozeros[data.Nozeros$C.FPASSD > 80,]


		data.Nozeros$C.WLEN <- data.Nozeros$C.WLEN - mean(data.Nozeros$C.WLEN, na.rm = TRUE)
		data.Nozeros$PREVFIX <- data.Nozeros$PREVFIX - mean(data.Nozeros$PREVFIX, na.rm = TRUE)
		data.Nozeros$OBLP <- data.Nozeros$WDLP - mean(data.Nozeros$WDLP, na.rm = TRUE)
		data.Nozeros$ID <- data.Nozeros$ID - mean(data.Nozeros$ID, na.rm = TRUE)
		data.Nozeros$WordFreq <- data.Nozeros$WordFreq - mean(data.Nozeros$WordFreq, na.rm = TRUE)
		data.Nozeros$FRQLAST <- data.Nozeros$FRQLAST - mean(data.Nozeros$FRQLAST, na.rm = TRUE)
#data.Nozeros$NBIGR <- data.Nozeros$NBIGR - mean(data.Nozeros$NBIGR, na.rm = TRUE)
#data.Nozeros$BACKTR <- data.Nozeros$BACKTR - mean(data.Nozeros$BACKTR, na.rm = TRUE)
		data.Nozeros$LASTFIX <- data.Nozeros$LASTFIX - mean(data.Nozeros$LASTFIX, na.rm = TRUE)
		data.Nozeros$Surprisal <- data.Nozeros$Surprisal - mean(data.Nozeros$Surprisal, na.rm = TRUE)
		data.Nozeros$Attention <- data.Nozeros$Attention - mean(data.Nozeros$Attention, na.rm = TRUE)


#return(data)


# TODO
# C.WLEN should now be Token.Length, should be be taken care of when removing the -1 things




# Demberg:
# C.WLEN .
# wfreq .
# prevwordfreq .
# prevwordfix .
# launchdist
# landingpos .
# wordnoinsentence .
# resodbigramprob
# wordlen:wordfreq
# wordlen:landpos


# LASTFIX, LDIST
# mixed model with all factors

		cat("FPASSDUR","\n")

		data.Nozeros$residAttentionWithSurp = residuals(lm(Attention ~ C.WLEN + C.FRQLAST +    WordFreq + Surprisal, data=data.Nozeros, na.action=na.exclude))

		data.Nozeros$residSurprisal = residuals(lm(Surprisal ~ C.WLEN +   C.FRQLAST +    WordFreq, data=data.Nozeros, na.action=na.exclude))

		data.Nozeros$residAttentionNoSurp = residuals(lm(Attention ~ C.WLEN +  C.FRQLAST +    WordFreq, data=data.Nozeros, na.action=na.exclude))



		if(DO_LINEAR){

# with more predictors
#linear.Baseline = lmer(C.FPASSD ~ C.WNOS + C.WLEN+ C.FREQBIN + C.FRQLAST + C.TRACEIC + C.BACKTR + C.NBIGR + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + (1|Participant) + (1|C.ITEM), data=data.Nozeros)
#linear.Surp = lmer(C.FPASSD ~ C.WNOS + C.WLEN+ C.FREQBIN + C.FRQLAST + C.TRACEIC + C.BACKTR + C.NBIGR + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + residSurprisal +  (1|Participant) + (1|C.ITEM), data=data.Nozeros)

	linear.Baseline = lmer(C.FPASSD ~ C.WNOS + C.WLEN+ C.FRQLAST + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + (1|Participant) + (1|C.ITEM), data=data.Nozeros)

	linear.Surp = lmer(C.FPASSD ~ C.WNOS + C.WLEN+  C.FRQLAST +  C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + residSurprisal +  (1|Participant) + (1|C.ITEM), data=data.Nozeros)

if(sd(data$Attention) > 0) {
				linear.Att = lmer(C.FPASSD ~ C.WNOS + C.WLEN+ C.FREQBIN + C.FRQLAST + C.TRACEIC + C.BACKTR + C.NBIGR + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + residAttentionNoSurp + (1|Participant) + (1|C.ITEM), data=data.Nozeros)

				linear.Full = lmer(C.FPASSD ~ C.WNOS + C.WLEN+ C.FREQBIN + C.FRQLAST + C.TRACEIC + C.BACKTR + C.NBIGR + C.PREVFIX + C.LDIST + C.OBJLPOS + WordFreq + Surprisal + residAttentionWithSurp  + (1|Participant) + (1|C.ITEM), data=data.Nozeros)
} else {
   linear.Att = NULL
   linear.Full = NULL
}
		} else {
			linear.Surp = NULL
				linear.Att = NULL
				linear.Full = NULL
                      l9inear.Baseline = NULL
		}



	cat("Starting output","\n")
################### EVALUATION OUTPUT ######################

		cat("Mean Surprisal:", mean(data.Nozeros$NotCentered.Surprisal), sep="\n")

		approxPerp = computeApproxPerplexity(data)

# Significance for Fixations

		if(DO_LOGISTIC) {
			cat("\n", "Baseline",capture.output(summary(log.Baseline)), sep="\n")
				cat("\n","Full",capture.output(summary(log.Full)), sep="\n")

				cat("\n","Surprisal",capture.output(summary(log.Surp)), sep="\n")

				cat("\n","Attention",capture.output(summary(log.Att)), sep="\n")
		}
# Correlations

	cat("Att-Surp          ",cor(data$Surprisal, data$Attention),"\n")
		cat("Att-log(WordFreq) ", cor(data$Attention, data$WordFreq),"\n")
		cat("Att-WordLength    ", cor(data$Attention,data$C.WLEN),"\n")

#return(data)


# Significance for FPASSDUR

		if(DO_LINEAR){
cat("\n", "Baseline", capture.output(summary(linear.Baseline)), sep="\n")
			cat("\n","Surprisal",capture.output(summary(linear.Surp)), sep="\n")

				cat("\n","Attention",capture.output(summary(linear.Att)), sep="\n")

				cat("\n","Full",capture.output(summary(linear.Full)), sep="\n")
		}


# COMPARISON OF MAXIMUM-LIKELIHOOD SEQUENCE

	cat("\nATTENTION\n")
		predicted = getThresholdPredictor(data, data$NotCentered.Attention)
		resuAttDiscrete = doDiscreteEvaluation(data$FIXATED, predicted)


		cat("\nSURPRISAL\n")
		predicted = getThresholdPredictor(data, data$NotCentered.Surprisal)
		resuSurpDiscrete = doDiscreteEvaluation(data$FIXATED, predicted)


		cat("\nModel Fixation Rate  ",mean(data$Attended),"\n\n")
#sink()


#cat(".....")

		models <- list("data" = data,
				"log.Full" = log.Full, 
				"log.Surp" = log.Surp,
				"log.Att" = log.Att,
				"Att-Surp" = cor(data$Surprisal, data$Attention), 
				"Att-log(WordFreq)" = cor(data$Attention, data$WordFreq),
				"Att-WordLength" = cor(data$Attention,data$C.WLEN), 
				"FP.Surprisal" = linear.Surp, 
				"FP.Att" = linear.Att, "FP.Full" = linear.Full, "meanSurp" = mean(data.Nozeros$NotCentered.Surprisal), "approxPerp" = approxPerp,
				"Disc.Surp" = resuSurpDiscrete, "Disc.Att" = resuAttDiscrete)

#return(models)


		if(FALSE){
			statisticsOfInterest <- c(
					coef(summary(models$log.Full))["C.residAttention","z value"],
					coef(summary(models$log.Surp))["C.residSurprisal","z value"],
					coef(summary(models$log.Att))["C.residAttentionNoSurp","z value"],

					models$"Att-Surp",
					models$"Att-log(WordFreq)",
					models$"Att-WordLength",

					coef(summary(models$"FP.Full"))["residAttentionWithSurp","t value"],
					coef(summary(models$"FP.Att"))["residAttentionNoSurp","t value"],
					coef(summary(models$"FP.Surprisal"))["residSurprisal","t value"],

					models$"meanSurp",
					models$"approxPerp"$model,
					models$"approxPerp"$ratio
					)
		}


	if(!returnStatistics){
		return(models)
	}else{
		return(statisticsOfInterest)
	}

	postags = unique(data[,c("CPOS")])


		for(i in (1:length(postags)))
		{
			posdata <- data[ which(data$CPOS == postags[i]), ]
				if(length(posdata$CPOS) > 300 && max(posdata$WordFreq) != min(posdata$WordFreq) && length(unique(posdata[,c("Token")])) > 50) {
					att = posdata$Attention
						fpassdur = posdata$FDUR
						fixated = posdata$C.FIXNO
						surp = posdata$Surprisal
						cat("\n")
						cat("+++++++++\n")
						cat(capture.output(postags[i])[1])
						cat("\n")
						cat(capture.output(summary(posdata)), sep="\n")
						cat("Correlation between attention and surprisal:  ")
						cat(cor(att, surp), sep="\n")
						cat("Correlation between attention and duration:  ")
						cat(cor(att, fpassdur), sep="\n")
						cat("Correlation between attention and fixation:  ")
						cat(cor(att, fixated), sep="\n")
						cat("Correlation between surprisal and duration:  ")
						cat(cor(surp, fpassdur), sep="\n")
						cat("Correlation between surprisal and fixation:  ")
						cat(cor(surp, fixated), sep="\n")
						cat("Correlation between wordfreq and duration:  ")
						cat(cor(fpassdur, posdata$WordFreq), sep="\n")
						cat("Correlation between wordfreq and surprisal:  ")
						cat(cor(surp, posdata$WordFreq), sep="\n")
						cat("Correlation between wordfreq and attention:  ")
						cat(cor(att, posdata$WordFreq), sep="\n")

						if(sd(surp, na.rm = TRUE) * sd(att, na.rm = TRUE) * sd(fixated, na.rm = TRUE) * sd(fpassdur, na.rm = TRUE) == 0) {

						} else {

							posdata$residAttentionPOS = residuals(lm(Attention ~ C.WLEN + WordFreq +  FRQLAST +    WordFreq + Surprisal, data=posdata))

								posdata$residSurprisalPOS = residuals(lm(Surprisal ~ C.WLEN + WordFreq +  FRQLAST +    WordFreq, data=posdata))

								try(cat(capture.output(summary(lmer(FDUR ~ C.WLEN +  PREVFIX + OBLP + ID + WordFreq + FRQLAST +     WordFreq + Surprisal + residAttentionPOS + (1|Participant) + (1|C.ITEM), data=posdata))), sep="\n"))

								try(cat(capture.output(summary(lmer(FDUR ~ C.WLEN +  PREVFIX + OBLP + ID + WordFreq + FRQLAST +     WordFreq + residSurprisalPOS + (1|Participant) + (1|C.ITEM), data=posdata))), sep="\n"))

								try(cat(capture.output(summary(lmer(C.FIXNO ~ C.WLEN +  PREVFIX + OBLP + ID + WordFreq + FRQLAST +     WordFreq + Surprisal + residAttentionPOS + (1|Participant) + (1|C.ITEM), data=posdata))), sep="\n"))

# removed C.WLEN
								m1 = try(glm(formula = FIXATED ~   PREVFIX + OBLP + ID + WordFreq + FRQLAST +     WordFreq + Surprisal + residAttentionPOS, family = binomial("logit"), data=posdata))

								m2 = try(glm(formula = FIXATED ~   PREVFIX + OBLP + ID + WordFreq + FRQLAST +     WordFreq + Surprisal, family = binomial("logit"), data=posdata))

								cat("--- nFixations, RESID, LOGISTIC ---",sep="\n")
								try(cat(capture.output(m1), sep="\n"))

								cat("--- nFixations, RESID, LOGISTIC BASELINE ---",sep="\n")
								try(cat(capture.output(m2), sep="\n"))

								cat("--- LIKELIHOOD RATIO ---",sep="\n")
								try(cat(capture.output(lrtest(m1,m2)), sep="\n"))



						}

				}
		}
#sink()

#print("..")

#tokens = matrix(unique(data[,c("Token")]))



## tokens$Correlation <- apply(tokens, 1, function(token)
## {


##    tokenData = data[ which(data$Token == token), ]
##    #print(summary(tokenData))
##    if(length(tokenData$CPOS) > 100) {
##       correlation = cor(tokenData$Attention, tokenData$Surprisal)
##       cat(token, sep="\t")
##       cat(" \t  ")
##       cat(length(tokenData$CPOS), sep="\t")
##       cat(" \t  ")
##       cat(correlation, sep="\n")
##       return(correlation)
##    } else {
##       return(0.0)
##    }
## } )


}




outputSkippingPattern = function(CSV_FILE, fileName, partNum){
	sink(fileName)
		data <- read.csv(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE, sep=""), header = TRUE, sep = "\t")
		data$C.FIXATED = pmin(data$C.FIXNO, 1)

		fixated = data$C.FIXATED
		fileName = data$C.FILE
		previousFile = -1
		for(i in (1:length(fixated))) {
			if(! is.na(fileName[i]) &( fileName[i] == previousFile)) {
				if(is.na(fixated[i])) {
					cat(-1, sep="\n")
				}else{
					cat(fixated[i], sep="\n")
				}
			}else{
				cat(-1, sep="\n")
			}
			if(!is.na(fileName[i])) {
				previousFile = fileName[i]
			}
		}

	sink()

}


outputSkippingPatternWithWords = function(CSV_FILE, fileName, column_name, partNum){
	sink(fileName)
		data <- read.csv(paste("/disk/scratch2/s1582047/dundeetreebank/parts/PART", partNum, "Statistics/statisticsFinal/",CSV_FILE, sep=""), header = TRUE, sep = "\t")
		fixated = pmin(data[, column_name], 1)

		fileName = data$C.FILE
		words = data$Word
		previousFile = -1
		for(i in (1:length(fixated))) {
			if(! is.na(fileName[i]) &( fileName[i] == previousFile)) {
				if(is.na(fixated[i])) {
					cat(paste(-1, words[i], sep=" "), sep="\n")
				}else{
					cat(paste(fixated[i], words[i],  sep=" "), sep="\n")
				}
			}else{
				cat(paste(-1, words[i], sep=" "), sep="\n")
			}
			if(!is.na(fileName[i])) {
				previousFile = fileName[i]
			}
		}

	sink()
}


sampleTextsNames = function() {

return(c("_u_nlp_data_deepmind-qa_cnn_train_83694", "_u_nlp_data_deepmind-qa_cnn_train_294135", "_u_nlp_data_deepmind-qa_cnn_train_209685", "_u_nlp_data_deepmind-qa_cnn_train_280106", "_u_nlp_data_deepmind-qa_cnn_train_42869", "_u_nlp_data_deepmind-qa_cnn_train_218796", "_u_nlp_data_deepmind-qa_cnn_train_150838", "_u_nlp_data_deepmind-qa_cnn_train_314052", "_u_nlp_data_deepmind-qa_cnn_train_218385", "_u_nlp_data_deepmind-qa_cnn_train_241012", "_u_nlp_data_deepmind-qa_cnn_train_198721", "_u_nlp_data_deepmind-qa_cnn_train_206516", "_u_nlp_data_deepmind-qa_cnn_train_320475", "_u_nlp_data_deepmind-qa_cnn_train_308283", "_u_nlp_data_deepmind-qa_cnn_train_52140", "_u_nlp_data_deepmind-qa_cnn_train_29307", "_u_nlp_data_deepmind-qa_cnn_train_255179", "_u_nlp_data_deepmind-qa_cnn_train_144723", "_u_nlp_data_deepmind-qa_cnn_train_206414", "_u_nlp_data_deepmind-qa_cnn_train_182020", "_u_nlp_data_deepmind-qa_cnn_train_5267", "_u_nlp_data_deepmind-qa_cnn_train_41399", "_u_nlp_data_deepmind-qa_cnn_train_162684", "_u_nlp_data_deepmind-qa_cnn_train_225629", "_u_nlp_data_deepmind-qa_cnn_train_227195", "_u_nlp_data_deepmind-qa_cnn_train_32437", "_u_nlp_data_deepmind-qa_cnn_train_149731", "_u_nlp_data_deepmind-qa_cnn_train_39770", "_u_nlp_data_deepmind-qa_cnn_train_231092", "_u_nlp_data_deepmind-qa_cnn_train_120207", "_u_nlp_data_deepmind-qa_cnn_train_165070", "_u_nlp_data_deepmind-qa_cnn_train_178757", "_u_nlp_data_deepmind-qa_cnn_train_105727", "_u_nlp_data_deepmind-qa_cnn_train_310968", "_u_nlp_data_deepmind-qa_cnn_train_54178", "_u_nlp_data_deepmind-qa_cnn_train_148537", "_u_nlp_data_deepmind-qa_cnn_train_245817", "_u_nlp_data_deepmind-qa_cnn_train_53450", "_u_nlp_data_deepmind-qa_cnn_train_300573", "_u_nlp_data_deepmind-qa_cnn_train_172856", "_u_nlp_data_deepmind-qa_cnn_train_161542", "_u_nlp_data_deepmind-qa_cnn_train_191663", "_u_nlp_data_deepmind-qa_cnn_train_127392", "_u_nlp_data_deepmind-qa_cnn_train_53778", "_u_nlp_data_deepmind-qa_cnn_train_82306", "_u_nlp_data_deepmind-qa_cnn_train_201944", "_u_nlp_data_deepmind-qa_cnn_train_235206", "_u_nlp_data_deepmind-qa_cnn_train_301305", "_u_nlp_data_deepmind-qa_cnn_train_122259", "_u_nlp_data_deepmind-qa_cnn_train_232968", "_u_nlp_data_deepmind-qa_dailymail_train_303137", "_u_nlp_data_deepmind-qa_dailymail_train_150844", "_u_nlp_data_deepmind-qa_dailymail_train_772659", "_u_nlp_data_deepmind-qa_dailymail_train_218875", "_u_nlp_data_deepmind-qa_dailymail_train_171558", "_u_nlp_data_deepmind-qa_dailymail_train_383397", "_u_nlp_data_deepmind-qa_dailymail_train_154945", "_u_nlp_data_deepmind-qa_dailymail_train_286330", "_u_nlp_data_deepmind-qa_dailymail_train_508290", "_u_nlp_data_deepmind-qa_dailymail_train_567848", "_u_nlp_data_deepmind-qa_dailymail_train_874605", "_u_nlp_data_deepmind-qa_dailymail_train_112425", "_u_nlp_data_deepmind-qa_dailymail_train_628070", "_u_nlp_data_deepmind-qa_dailymail_train_470411", "_u_nlp_data_deepmind-qa_dailymail_train_726820", "_u_nlp_data_deepmind-qa_dailymail_train_622954", "_u_nlp_data_deepmind-qa_dailymail_train_142157", "_u_nlp_data_deepmind-qa_dailymail_train_838255", "_u_nlp_data_deepmind-qa_dailymail_train_543415", "_u_nlp_data_deepmind-qa_dailymail_train_304239", "_u_nlp_data_deepmind-qa_dailymail_train_821635", "_u_nlp_data_deepmind-qa_dailymail_train_613194", "_u_nlp_data_deepmind-qa_dailymail_train_331796", "_u_nlp_data_deepmind-qa_dailymail_train_82972", "_u_nlp_data_deepmind-qa_dailymail_train_440282", "_u_nlp_data_deepmind-qa_dailymail_train_869250", "_u_nlp_data_deepmind-qa_dailymail_train_679352", "_u_nlp_data_deepmind-qa_dailymail_train_386834", "_u_nlp_data_deepmind-qa_dailymail_train_16677", "_u_nlp_data_deepmind-qa_dailymail_train_673722", "_u_nlp_data_deepmind-qa_dailymail_train_431402", "_u_nlp_data_deepmind-qa_dailymail_train_845534", "_u_nlp_data_deepmind-qa_dailymail_train_737459", "_u_nlp_data_deepmind-qa_dailymail_train_759762", "_u_nlp_data_deepmind-qa_dailymail_train_566294", "_u_nlp_data_deepmind-qa_dailymail_train_150877", "_u_nlp_data_deepmind-qa_dailymail_train_482982", "_u_nlp_data_deepmind-qa_dailymail_train_386571", "_u_nlp_data_deepmind-qa_dailymail_train_278689", "_u_nlp_data_deepmind-qa_dailymail_train_443191", "_u_nlp_data_deepmind-qa_dailymail_train_177648", "_u_nlp_data_deepmind-qa_dailymail_train_669559", "_u_nlp_data_deepmind-qa_dailymail_train_437088", "_u_nlp_data_deepmind-qa_dailymail_train_791819", "_u_nlp_data_deepmind-qa_dailymail_train_739752", "_u_nlp_data_deepmind-qa_dailymail_train_651810", "_u_nlp_data_deepmind-qa_dailymail_train_678637", "_u_nlp_data_deepmind-qa_dailymail_train_7924", "_u_nlp_data_deepmind-qa_dailymail_train_379552", "_u_nlp_data_deepmind-qa_dailymail_train_685567"))
}

